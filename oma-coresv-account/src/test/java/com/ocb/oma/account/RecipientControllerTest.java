/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RecipientType;
import com.ocb.oma.dto.UserToken;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class RecipientControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testGetRecipientList() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/recipient/getRecipientList";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")
				.param("filerTemplateType", "EXTERNAL").param("pageSize", "10")
				).andExpect(status().isOk()).andReturn();
		List<RecipientDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<RecipientDTO>>() {
				});
		System.out.println("payload of testGetRecipientList : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());
	}

	@Test
	public void testDeleteRecipient() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/recipient/deleteRecipient";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		RecipientDTO delete = new RecipientDTO();
		delete.setRecipientId("1");
		MvcResult result = mvc
				.perform(
						requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(delete)))
				.andExpect(status().isOk()).andReturn();
		Boolean response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<Boolean>() {
				});
		System.out.println("payload of testDeleteRecipient : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testAddRecipient() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/recipient/addRecipient";
		RecipientInputDTO dtoInput = new RecipientInputDTO();
		dtoInput.setRecipientType(PaymentType.InternalPayment.getPaymentTypeCode());
		dtoInput.setShortName("Ha Minh Lam");
		dtoInput.setAccountId("lam2008");
		dtoInput.setRemarks("Chuyen khoan thoi noi");
		dtoInput.setBeneficiary("24, Truong Tho, Thu Duc");
		dtoInput.setCreditAccount("08897443976434");
		dtoInput.setProvince("0024");
		dtoInput.setBankCode("OCB");
		dtoInput.setBranchCode("DEF");
		dtoInput.setCardNumber("220099");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(dtoInput)))
				.andExpect(status().isOk()).andReturn();
		AddRecipientOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				AddRecipientOutputDTO.class);

		System.out.println("*******result of testAddRecipient : " + result.getResponse().getContentAsString());
		assertNotNull(response);

	}

}
