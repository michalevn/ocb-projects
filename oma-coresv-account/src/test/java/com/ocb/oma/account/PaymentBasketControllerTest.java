/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.account.service.dev.DevGeneratedDataUtil;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.StatusesType;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PaymentItemStudentFeeDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class PaymentBasketControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testcountBasketPaymentToProcess() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/countBasketPaymentToProcess";
		// path = "/custom/agentHierarchy/";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		Integer response = objectMapper.readValue(result.getResponse().getContentAsString(), Integer.class);

		System.out.println("payload of testcountBasketPaymentToProcess : " + response);
		assertNotNull(response);
	}

	@Test
	public void testgetTransactionFee() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getTransactionFee";
		// TransactionFeeInputDTO inputDTO = new TransactionFeeInputDTO(debitAccount,
		// creditAccount, amount, currency, paymentType, province, bookingDate)
		TransactionFeeInputDTO inputDTO = new TransactionFeeInputDTO("0100100007826003", "0100100007826001", 1000000D,
				"vnd", PaymentType.LocalPayment.getPaymentTypeCode(), "01", new Date());

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		Double response = objectMapper.readValue(result.getResponse().getContentAsString(), Double.class);

		System.out.println("*******payload of testgetTransactionFee : " + response);
		assertNotNull(response);
	}

	@Test
	public void testpostPayment() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/postPayment";
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		// ck noi bo
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount(1000000L);
		paymentInfo.setCreditAccount("0101898177");
		paymentInfo.setRecipient("0101010101010110");
		paymentInfo.setRemarks("Note giao dich");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setcRefNum(DevGeneratedDataUtil.generateUUID());

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO(PaymentType.InternalPayment.getPaymentTypeCode(),
				paymentInfo);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		PostPaymentOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				PostPaymentOutputDTO.class);

		System.out.println("*******result of testpostPayment : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testPostPaymentLocalPayment() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/postPayment";
		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.LocalPayment.getPaymentTypeCode());
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAmount(100000L);
		paymentInfo.setAccountId("11");
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("987654321");
		paymentInfo.setRecipient("PHAM THANH AN");
		paymentInfo.setRemarks("123");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setCreditAccountBankBranchCode("83202001");
		paymentInfo.setCreditAccountBankCode("202");
		paymentInfo.setCreditAccountProvinceCode("83");

		inputDTO.setPaymentInfo(paymentInfo);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		PostPaymentOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				PostPaymentOutputDTO.class);

		System.out
				.println("*******result of testPostPaymentLocalPayment : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testPostPaymentAddBasket() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/postAddBasket";
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount((long) 2000);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0100100007826003");
		paymentInfo.setRecipient("Huynh Khac Nhan");
		paymentInfo.setRemarks("chuyen tien cho HKN");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setcRefNum("");

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO(PaymentType.InternalPayment.getPaymentTypeCode(),
				paymentInfo);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		PostAddBasketDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				PostAddBasketDTO.class);

		System.out.println("*******result of postPaymentAddBasket : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testPostPaymentAddBasketLocalPayment() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/postAddBasket";
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount((long) 2000000);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0100100007826003");
		paymentInfo.setRecipient("Huynh Khac Nhan");
		paymentInfo.setRemarks("chuyen tien cho HKN");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setcRefNum("");

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO(PaymentType.LocalPayment.getPaymentTypeCode(),
				paymentInfo);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		PostAddBasketDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				PostAddBasketDTO.class);

		System.out.println(
				"*******result of postPaymentAddBasketLocalPayment : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testGetListofBasket() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getListofBasket";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		// amountFrom=0&amountTo=10000000&customerId=&realizationDateFrom=1453185446&realizationDateTo=1608358090000&statuses=READY
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").param("amountFrom", "0")
						.param("amountTo", "10000000").param("customerId", "")
						.param("realizationDateFrom", "1453185446000").param("realizationDateTo", "1608358090000")
						.param("statuses", StatusesType.READY.getStatusesTypeCode()))
				.andExpect(status().isOk()).andReturn();

		List<PaymentBasketDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<PaymentBasketDTO>>() {
				});

		System.out.println("payload of getListofBasket : " + response);
		assertNotNull(response);
	}

	@Test
	public void testPostDeleteBasket() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/deleteBasket";
		String basketId = "1";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("basketId", basketId))
				.andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		System.out.println("*******result of testPostDeleteBasket : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testgetListOfBillTransactions() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getListOfBillTransactions";
		GetListOfBillTransactionsInputDTO inputDTO = new GetListOfBillTransactionsInputDTO(new Date(1517418000000L),
				new Date(1545547421693L));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		List<BillPaymentHistoryDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<BillPaymentHistoryDTO>>() {
				});

		System.out.println(
				"*******result of testgetListOfBillTransactions : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testgetListBillPaymentByService() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getListBillPaymentByService";
		String serviceCode = "PPMB";
		Integer duration = 180;

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("serviceCode", serviceCode)
				.param("duration", duration + "")).andExpect(status().isOk()).andReturn();
		List<BillPaymentServiceDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<BillPaymentServiceDTO>>() {
				});

		System.out.println(
				"*******result of testgetListBillPaymentByService : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testgetBill() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getBill";
		GetBillInputDTO inputDTO = new GetBillInputDTO(true, "1", "", "");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().isOk()).andReturn();
		BillPaymentServiceDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<BillPaymentServiceDTO>() {
				});

		System.out.println("*******result of getBill : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testgetListOfServiceProvider() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getListOfServiceProvider";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		List<ServiceProviderComposDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<ServiceProviderComposDTO>>() {
				});

		System.out.println(
				"*******result of testgetListOfServiceProvider : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testLoadEVNparameter() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/loadEVNparameter";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		List<EVNParameter> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<EVNParameter>>() {
				});

		System.out.println("*******result of testLoadEVNparameter : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testPostBaskets() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/postBaskets";

		List<String> transfersIds = Arrays.asList("NIB-BAS46261113111803aabc47324cc064");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(
				requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(transfersIds)))
				.andExpect(status().isOk()).andReturn();
		PostListBasketDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<PostListBasketDTO>() {
				});

		System.out.println("*******result of testPostBaskets : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testPostUpdateBasket() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/updateBasket";
		PostPaymentInputDTO input = new PostPaymentInputDTO();
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount(99999L);
		paymentInfo.setBillCode("NCLYD");
		String[] billCodeItemNoArr = { "009", "008" };
		paymentInfo.setBillCodeItemNo(billCodeItemNoArr);
		paymentInfo.setCreditAccount("11111111");
		paymentInfo.setCardNumber("00917389463457832");
		paymentInfo.setCreditAccountBankBranchCode("10000001");
		paymentInfo.setCreditAccountBankCode("999991");
		paymentInfo.setCreditAccountProvinceCode("0099");
		paymentInfo.setcRefNum("123456");
		paymentInfo.setCurrency("USA");
		paymentInfo.seteWalletPhoneNumber("84");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setMobilePhoneNumber("0973456782");

		List<PaymentItemStudentFeeDTO> paymentItemStudentFee = new ArrayList<>();

		PaymentItemStudentFeeDTO paymentItemStudentFeeDTO = new PaymentItemStudentFeeDTO();
		paymentItemStudentFeeDTO.setAmount(350D);
		paymentItemStudentFeeDTO.setDiscount(10D);
		paymentItemStudentFeeDTO.setDueDate(new Date());
		paymentItemStudentFeeDTO.setIsrequired(1);
		paymentItemStudentFeeDTO.setItemNameDescription("Ngo thua hao");
		paymentItemStudentFeeDTO.setItemTypeFeeType("");
		paymentItemStudentFeeDTO.setPaymentItemFeeCode("");
		paymentItemStudentFee.add(paymentItemStudentFeeDTO);

		paymentInfo.setPaymentItemStudentFee(paymentItemStudentFee);
		paymentInfo.setRecipient("");
		paymentInfo.setRemarks("Chuyen tien mung");
		paymentInfo.setServiceCode("OOOC");
		paymentInfo.setServiceProviderCode("PT");
		paymentInfo.setStudentCode("QCT");

		input.setPaymentInfo(paymentInfo);
		input.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		String transferId = "NIB-BAS0909030811183645bf14a8b8545e";
		input.setTransferId(transferId);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();

		System.out.println("*******result of testPostUpdateBasket : " + result.getResponse().getContentAsString());
	}

	@Test
	public void testGetPaymentsInFuture() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getPaymentsInFuture";
		PaymentsInFutureInputDTO input = new PaymentsInFutureInputDTO();
		Date realizationDateFrom = new Date(1453185446000L);
		Date realizationDateTo = new Date(1545201446000L);
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setRealizationDateFrom(realizationDateFrom);
		input.setRealizationDateTo(realizationDateTo);
		input.setStatusPaymentCriteria("waiting");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();

		List<PaymentsInFutureDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<PaymentsInFutureDTO>>() {
				});

		System.out.println("payload of testGetPaymentsInFuture : " + response);
		assertNotNull(response);
	}

	@Test
	public void test_GetDetailPaymentsInFuture() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/payment/getDetailPaymentsInFuture";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("idFuturePayment",
				"7d37f465-f365-4a35-bd04-1dd21915d292@waiting")).andExpect(status().isOk()).andReturn();

		PaymentsInFutureDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				PaymentsInFutureDTO.class);

		System.out.println(
				"*******result of test_GetDetailPaymentsInFuture : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}
}
