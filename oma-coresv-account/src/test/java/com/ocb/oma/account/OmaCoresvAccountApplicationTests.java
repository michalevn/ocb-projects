package com.ocb.oma.account;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.dto.UserToken;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class OmaCoresvAccountApplicationTests extends AccountConfigTest {

	@Autowired
	AuthenticationService authenService;

	@Test
	public void testLogin() {
		UserToken checkLogin = authenService.loginAndLoadUserInfo("ibtest9", "Abc@123", "deviceId");
		assertNotNull(checkLogin);
	}

}
