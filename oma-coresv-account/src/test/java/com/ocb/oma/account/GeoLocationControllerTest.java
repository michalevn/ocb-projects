/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.account.security.AccessTokenHolder;
import com.ocb.oma.dto.GeoLocationPoiDTO;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class GeoLocationControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void find() throws Exception {

		AccessTokenHolder.set(getAccessToken());

		String path = "/geo-locations/find";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, AccessTokenHolder.get());
		requestBuilder.contentType("application/json");
		requestBuilder.param("lat", "10.768546");
		requestBuilder.param("lng", "106.700959");
		requestBuilder.param("poiType", "BRANCH");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		GeoLocationPoiDTO response = objectMapper.readValue(responseText, GeoLocationPoiDTO.class);
		System.out.println("### GET /geo-locations/find");
		System.out.println(objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotNull(response.getPoiPositions());
		assertNotNull(response.getRequestedGpsPosition());
		assertNotEquals(0, response.getPoiPositions().size());
	}
}
