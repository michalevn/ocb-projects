/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoLstItems;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class OtpControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testsendAuthorizationSmsOtp() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/sendAuthorizationSmsOtp";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		AuthorizationResponse response = objectMapper.readValue(result.getResponse().getContentAsString(),
				AuthorizationResponse.class);
		System.out.println("payload of testsendAuthorizationSmsOtp : " + result.getResponse().getContentAsString());
		assertNotNull(response);
		assertNotNull(response.getAuthId());
	}

	@Test
	public void testVerifySmsOTP() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/verifyOTPToken";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		VerifyOtpToken data = new VerifyOtpToken();
		data.setAuthId("12345");
		data.setOtpValue("something");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		AuthorizationResponse response = objectMapper.readValue(result.getResponse().getContentAsString(),
				AuthorizationResponse.class);
		System.out.println("payload of testVerifySmsOTP : " + result.getResponse().getContentAsString());
		assertNotNull(response);
		assertNotNull(response.getOperationStatus());
	}

	@Test
	public void testVerifyHWOTP() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/verifyHWOTPToken";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		Map<String, String> data = new HashMap<>();
		data.put("tokenValue", "1234567890");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		AuthorizationResponse response = objectMapper.readValue(result.getResponse().getContentAsString(),
				AuthorizationResponse.class);
		System.out.println("payload of testVerifyHWOTP : " + result.getResponse().getContentAsString());
		assertNotNull(response);
		assertNotNull(response.getOperationStatus());
	}

	@Test
	public void testgetChallengeCodeSoftOTP() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/getChallengeCodeSoftOTP";
		GetChallengeCodeInputDTO input = new GetChallengeCodeInputDTO("cif", "creditAcc01", "debit01", 1000000D,
				PaymentType.LocalPayment.getPaymentTypeCode(), "vnd", "citadCode", "provinceCode");
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		System.out.println("****result of testgetChallengeCodeSoftOTP : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testGetAuthorizationPolicies() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/getAuthorizationPolicies";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path).param("userId", "123")
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		List<String> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<String>>() {
				});
		System.out.println("********** " + response);
		System.out
				.println("****result of test getAuthorizationPolicies : " + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

	@Test
	public void testGetQD630Policies() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/getQD630Policies";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path).param("cif", "801003")
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<QD630PoliciesDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<QD630PoliciesDTO>>() {
				});
		System.out.println("result of testGetQD630Policies : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());

	}

	@Test
	public void testGetListPrepaidPhone() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/getListPrepaidPhone";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<PrepaidPhoneDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<PrepaidPhoneDTO>>() {
				});
		System.out.println("result of testGetListPrepaidPhone : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());

	}

	@Test
	public void test_checkQRcodeInfo() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/otp/checkQRcodeInfo";
		QRcodeInfoInputDTO input = new QRcodeInfoInputDTO();
		QRcodeInfoDTO qrcodeInfo = new QRcodeInfoDTO();
		qrcodeInfo.setDebitAmount(100000L);
		qrcodeInfo.setPayType("payType");
		qrcodeInfo.setVoucherCode("voucherCode");
		List<QRcodeInfoLstItems> lstItems = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			QRcodeInfoLstItems infoLstItems = new QRcodeInfoLstItems();
			infoLstItems.setNote("note");
			infoLstItems.setQrInfor("qrInfor000" + i);
			infoLstItems.setQuantity(i * 10);
			lstItems.add(infoLstItems);
		}
		qrcodeInfo.setLstItems(lstItems);

		input.setQrcodeInfo(qrcodeInfo);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		QRcodeInfoOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<QRcodeInfoOutputDTO>() {
				});
		System.out.println("test_checkQRcodeInfo: " + objectMapper.writeValueAsString(response));

		assertNotNull(response);
	}
}
