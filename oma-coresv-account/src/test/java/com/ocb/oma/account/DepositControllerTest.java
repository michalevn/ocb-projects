/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class DepositControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testBreakDeposit() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/deposit/breakDeposit";
		String depositId = "1261";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("depositId", depositId))
				.andExpect(status().isOk()).andReturn();
		OutputBaseDTO output = objectMapper.readValue(result.getResponse().getContentAsString(), OutputBaseDTO.class);
		assertNotNull(output);
		System.out.println("*******result of testBreakDeposit : " + result.getResponse().getContentAsString());
	}

	@Test
	public void test_getDeposits() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/deposit/getDeposits";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		List<DepositDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<DepositDTO>>() {
				});
		assertNotNull(response);
		System.out.println("*******result of TESTGetDeposits : " + result.getResponse().getContentAsString());
	}

	@Test
	public void test_getDeposit() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String jwtToken = user.getOldAuthen().getJwtToken();

		String path = "/deposit/getDeposit?depositId=1409";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		requestBuilder.header(HttpHeaders.AUTHORIZATION, jwtToken);
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		DepositDTO depositDTO = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<DepositDTO>() {
				});
		assertNotNull(depositDTO);
		assertEquals(depositDTO.getDepositId(), "1409");
		System.out.println("*******result of test_getDeposit : " + result.getResponse().getContentAsString());
	}

	@Test
	public void test_getDepositProducts() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/deposit/depositProducts";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		List<DepositProductDTO> depositProductDTOs = objectMapper.readValue(response,
				new TypeReference<List<DepositProductDTO>>() {
				});
		System.out.println("*******test_getDepositProducts:" + result.getResponse().getContentAsString());
		assertNotNull(response);
		assertEquals(depositProductDTOs.size(), 1);
	}

	@Test
	public void test_getDepositInterestRates() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);

		String path = "/deposit/depositInterestRates";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("currency", "VND")
				.param("subProductCode", "TICHLUYDIENTU")).andExpect(status().isOk()).andReturn();

		String response = result.getResponse().getContentAsString();
		List<DepositInterestRateDTO> depositProductDTOs = objectMapper.readValue(response,
				new TypeReference<List<DepositInterestRateDTO>>() {
				});
		System.out.println("*******test_getDepositInterestRates:" + result.getResponse().getContentAsString());
		assertNotNull(response);
		// assertEquals(depositProductDTOs.size(), 20);
	}

	@Test
	public void test_createDeposit() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/deposit/createDeposit";

		String dataInput = "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"currency\":\"VND\",\"customName\":\"Ten So TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":1,\"openingAccountId\":\"11\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"11\",\"settlementAccountIntId\":\"11\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		CreateDepositDTO depositDTO = objectMapper.readValue(dataInput, CreateDepositDTO.class);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(depositDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		CreateDepositOutputDTO output = objectMapper.readValue(result.getResponse().getContentAsString(),
				CreateDepositOutputDTO.class);
		assertNotNull(output);
	}

	@Test
	public void testGetDepositOfferAcc() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/deposit/getDepositOfferAcc";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		DepositOfferAccountsDTO output = objectMapper.readValue(result.getResponse().getContentAsString(),
				DepositOfferAccountsDTO.class);
		System.out.println("*******getDepositOfferAcc:" + result.getResponse().getContentAsString());
		assertNotNull(response);
	}

}
