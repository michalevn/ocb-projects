/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.UserToken;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class CampaignControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testgetCampaignList() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/campaign/getCampaignList?customerCif=801004&username=ibtest9";
		//customerId=801004&userId=ibtest9
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk()).andReturn();

		List<CampaignInfo> response = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<CampaignInfo>>() {
		});

		System.out.println("payload of testgetCampaignList : " + objectMapper.writeValueAsString(response));

		assertNotNull(response);
		assertNotEquals(0, response.size());

	}
}
