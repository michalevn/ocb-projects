/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.account.security.AccessTokenHolder;
import com.ocb.oma.account.service.AccountService;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class AccountControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private AccountService accountService;

	@Test
	public void testGetAccountList() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getAccountList";

		String customerCif = user.getOldAuthen().getCustomerId();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("customerCif", customerCif))
				.andExpect(status().isOk()).andReturn();

		List<AccountInfo> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<AccountInfo>>() {
				});

		System.out.println("payload of testGetAccountList : " + response);
		assertNotNull(response);
		assertNotEquals(0, response.size());
		assertNotNull(response.get(0).getCurrency());

	}

	@Test
	public void test_findAccountDetail() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/findAccountDetail";

		String customerCif = user.getOldAuthen().getCustomerId();
		List<AccountInfo> accountInfos = accountService.getAccountList(customerCif, user.getOldAuthen().getJwtToken());

		AccountInfo accountInfo = accountInfos.get(0);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").param("accountNum", accountInfo.getAccountNo()))
				.andExpect(status().isOk()).andReturn();

		AccountDetails accountDetails = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<AccountDetails>() {
				});

		assertNotNull(accountDetails);

	}

	@Test
	public void testGetCustomerDetail() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getCustomerDetail";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		CustomerInfoDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<CustomerInfoDTO>() {
				});
		System.out.println("payload of testGetCustomerDetail : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotNull(response.getAuthType());
		assertNotNull(response.getCustomerType());
		assertNotNull(response.getPackageType());
		assertNotNull(response.getPhoneNumber());

	}

	@Test
	public void testUpdateAccountIcon() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/updateAccountAvatar";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		Map<String, String> data = new HashMap<>();
		data.put("customerCif", user.getOldAuthen().getCustomerId());
		data.put("accountNo", "123456789");
		data.put("imageLink", "www.google.com");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		Boolean response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<Boolean>() {
				});
		System.out.println("payload of testUpdateAccountIcon : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testUpdateAccountName() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/updateAccountName";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		Map<String, String> data = new HashMap<>();
		data.put("customerCif", user.getOldAuthen().getCustomerId());
		data.put("accountNo", "416");
		data.put("accountName", "www.google.com");
		System.out.println("payload " + objectMapper.writeValueAsString(data));
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		Boolean response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<Boolean>() {
				});
		System.out.println("payload of testUpdateAccountName : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testfindTransactionHistory() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/findTransactionHistory";
		List<String> lsttransactionCodes = new ArrayList<>();
		String sDate1 = "2016-10-10";
		Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
		String sDate2 = "2017-10-10";
		Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate2);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		TransHistoryRequestDTO data = new TransHistoryRequestDTO("8", null, new Date(1454259600000L),
				new Date(1580490000000L), 0D, 10000000D, 1, 10);
		System.out.println("payload " + objectMapper.writeValueAsString(data));
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		List<TransactionHistoryDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<TransactionHistoryDTO>>() {
				});
		System.out.println("payload of testfindTransactionHistory : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());
	}

	@Test
	public void testloadAccountSummarByCurrencies() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/loadAccountSummarByCurrencies";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path).param("accountId", "41")
				.param("summaryPeriod", "180").header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		AccountSummaryDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<AccountSummaryDTO>() {
				});
		System.out
				.println("payload of testloadAccountSummarByCurrencies : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);

	}

	@Test
	public void testGetAccountLimit() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getAccountLimit";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.param("paymentType", PaymentType.FastTransfer.getPaymentTypeCode())
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		AccountLimitDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<AccountLimitDTO>() {
				});
		System.out.println("payload of testGetAccountLimit : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);

	}

	@Test
	public void testGetAccountNameByAccountNo() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getAccountNameByAccountNo";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.param("accountNo", "0001100012283006")
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println(result.getResponse().getContentAsString());
		String response = result.getResponse().getContentAsString();
		System.out.println("payload of testGetAccountNameByAccountNo : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);

	}

	@Test
	public void testgetAccountNameNapas() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);

//		String urlByAccount = host + "/api/recipient/get/recipient_name_by_account?accountNumber=" + accountNo
//				+ "&bankCode=" + bankCode + "&debitAccount=" + debitAccount;
//		String urlByCard = host + "/api/recipient/get/recipient_name_by_card?cardNumber=" + cardNumber
//				+ "&debitAccount=" + debitAccount;

		String path = "/account/getAccountNameNapas";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.param("accountNo", "0001100012283006").param("bankCode", "").param("cardNumber", "")
				.param("debitAccount", "").header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println(result.getResponse().getContentAsString());
		String response = result.getResponse().getContentAsString();
		System.out.println("payload of testGetAccountNameByAccountNo : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);

	}

	@Test
	public void testgetUpcomingRepaymentDeposit() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getUpcomingRepaymentDeposit";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println("****" + result.getResponse().getContentAsString());
		UpcomingRepaymentDepositDTO output = objectMapper.readValue(result.getResponse().getContentAsString(),
				UpcomingRepaymentDepositDTO.class);
		System.out
				.println("***result  of testgetUpcomingRepaymentDeposit : " + objectMapper.writeValueAsString(output));
		assertNotNull(output);
		assertNotNull(output.getCreditCardInfo());

	}

	@Test
	public void testgetBlockedTransaction() throws Exception {

		UserToken user = getUserLogin("rb1589390", "Qaz@1234");// getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getBlockedTransaction";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path).param("accountId", "413")
				.param("currentPage", "1").param("pageSize", "30")
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<TransactionBlockedDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<TransactionBlockedDTO>>() {
				});
		System.out.println("***payload of testGetBlockedTransaction : " + objectMapper.writeValueAsString(response));

		System.out.println("result of testGetBlockedTransaction01 : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testGetAvatarInfo() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getAvatarInfo";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		AvatarInfoOutputDTO output = objectMapper.readValue(result.getResponse().getContentAsString(),
				AvatarInfoOutputDTO.class);
		System.out.println("***payload of testGetAvatarInfo : " + objectMapper.writeValueAsString(output));

		assertNotNull(output);

	}

	@Test
	public void testChangePassword() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/changePassword";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());

		Map<String, String> data = new HashMap<>();
		data.put("customerId", "801003");
		data.put("oldPassword", "Abc@123");
		data.put("newPassword", "Abc@1234");
		System.out.println("payload " + objectMapper.writeValueAsString(data));
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		ChangePasswordDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<ChangePasswordDTO>() {
				});

		System.out.println("payload of testChangePassword : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testGetCifByAccountNo() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String accountNo = "0300958900090";
		String path = "/account/getCifByAccountNo?accountNo=" + accountNo;
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("*******testGetCifByAccountNo: " + rs);
		assertNotNull(rs);

	}

	@Test
	public void test_getCIFFromAccountList() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);

		URIBuilder uriBuilder = new URIBuilder("/account/getCIFFromAccountList");

		String path = uriBuilder.toString();

		Map<String, Object> params = new HashMap<>();
		params.put("lstAccountNum", Arrays.asList("0001100012281003", "0001100012282007", "0037100003605005"));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.content(objectMapper.writeValueAsBytes(params))
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		String rs = result.getResponse().getContentAsString();
		Map<String, String> cifs = objectMapper.readValue(rs, new TypeReference<Map<String, String>>() {
		});

		System.out.println("test_getCIFFromAccountList: " + rs);

		assertNotNull(cifs);

	}

	@Test
	public void test_getPersonalInfo() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/getPersonalInfo";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		CustomerPersonalInfoDTO personalInfoDTO = objectMapper.readValue(response,
				new TypeReference<CustomerPersonalInfoDTO>() {
				});
		System.out.println("*******test_getPersonalInfo:" + result.getResponse().getContentAsString());
		assertNotNull(response);
		assertEquals(personalInfoDTO.getUsername(), "kimlongap");
	}

//	

	@Test
	public void test_uploadAvatar() throws Exception {
		String path = "/account/uploadAvatar";
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String fileinput = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADhAOEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK5Hx1qGqaNo6anps/lx28n+kK0asrIeMnPIwcdPU0m7K5pSpurUVNbvudVJIkab2ZVX1LYH51yWs/ELR9M+S3f7dP8A3YWGz8X6flmvHda8W6x4imlS9WLfGmLdowFDZOeefyNZcMjeX88Dbv73ITHYDpiuWeIe0T6jA8P03aWIlfyWi+//AIY9ePxb037REn9nXPkMP3khZcofYdCPfI+ldZpPivR9cu/s2n3YmlEXmsoUjaMgYORwckcV8+Wcqwy73gkk/wBnbxn1J6E0trqupaTqS6jZStb/ALwyM+8YRAB8pGTkHng8dKUa8lqzpxfD+GcP3N0/W6+Z9P0VwPw68eSeMo71LiKOOW32FWjBAdWyM4JODkfqK76uqMlJXR8fXozoVHTnugoooqjIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACsDxcLVvCeqR3s6Qwy27x+aykqhYYBOATgEjnHFb9ZWvFl0G92WK3/7k7rVmx5q4+ZQcHkjOOOTSexdN2mn5nyteGPTLmW0uHhu7VWPkywuCMj0b+6expkVwzxb/lhi9ftDHH55FOv4VS6uJodrWbSErFJl3iTPGTjB68/TNX4bKJ0R0VY/lx8vH5D8a4JRSR9thq9Scrf8P/n+JHaWi3jbnumk/wBpujfQdqk1VlsNPfbFC+3Hy7BjJ46VWNrKk7pE33f41bH4fWqt7uudto/mSStyvzcD3PqKlR1OqtiIqnLo7HtnwS05k0K/1aWNFku5hGoXAXag7AdOWP5V6vXA/COG5h+Hlit2qr88hjO3BdC2QTk89+cDgD6nvq9CEeWKR8Li6rq15Sf9W0CiiiqOYKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDg/EXw30TVJZr5fMsrhvmaSDGM9SSp4I78YNeW3eiwQ3SQ2morJAzeXG7KPpkgHgcivdPEep6Xp+lSpqs/lwTqYsKCWbIwcAfzrwae4g/tlLa3l8638wbZdpG4ZHODyP89a8/FNxa5TaOYYii1ySLA8DeIEukhiihklbhdsq4b/vrHoa6bRPhLqMl2j6vcW8Nru3yRwsWkf8A2c4AUe4J9vWozqmpatqn9nWX7qWBvNa73EbEUZJY9gPX9Oa7A6lf3UiP58yyso/dRlhzjptFRQrrk5pq5vPNMRUVmdnBDFbQJDEixxRqFVF4CgcAAelTVVsklhs4kuGLShfmLHPP171ar0k7o4gooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFc9qviuy0m7Nu8ckjr9/y8fLnnv1OKidSMFeTsJtLc6GiuSs/H2l3Nz5MqyW6s2FeTGPx9K1ru9vIVWWKCO6tmz/q85x+uRj0qY1oSV4u4KSex5V8Rvttlq6PqU8dx5qloUjz8ig4xg9P1rireVbm4+SBVRWB3bs/0ru/iDf215dWVxEs1vPt2yfN+7cAjGCO4yc5HcVxqeb5nzqrK3O7bg/pXmVmuZ21RyzdpaHsng7TLSbR5WaOPe83+kbVwZGGCNzdSACMAYH50ar4xsLKX7Jotst5fN8q+SnyA+mRyx9l/MV5nc+INZexS0SWNbXncq5G4nHLYPI4HGP516H8PZdNe2dEs1h1FVy7s24uueoOOBnHH061vh6vNFU1o7f1Y1VTmfLE63SX1B9OibUooo7vH7xYzkf8A1vzNaFFFeglZWNgooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAITxXjOrXsU3iDULlJf3TTHb33AcZH5flXrmoWf2/Tri08xo/OjKb16rnvXk2r+HbvR5NssfydFkXo309687MHLlWmhhXu0jHkaJ2+RWZv93/ABp9tc32no9zaTzW/fbHIR+YHBqa1hb7UibWbdwq7cljXa6L4Qklngub5DHFEwdYm6uRyMjsM+tcNCEqj9w54Qk3oOu/BEms6Lafbp1+2LGGZsEfMQM5POfc45xmue1zwium2q/ZPO2RqEmSTG8E9DkcbT6jvxXrtc94xjI8M3UqHbJGoKsv+8Mj6Yr06+Gi4Nrc7JU1JWPG3tGh+R19vZc+9a3hnWm0bU4rh13bcoyL/Ep649+/4V12u+GorbRX1OKXcsUIkaNl9hkgj/CsPwPbaXqGryzTxRtFBCZdzdM5AGc/U1yRpThUSW5yKnKM0j1W1uY7y2iuIs7JFDLuGODVisaLXLTzkt4Y28vhVZQAvpwPStmvVjJSWjO1MKKKKoYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUx0VxtZQw96fRQBCltDG25IY1b1VQKmoooSsAVkeJ4vO8Makn/Tu5/IZ/pWvVW+t2utPuLdW2mWMpn6jHepmrxaAzXM1z4Vt/Ji8xpbeMMvX5WUZ4PXiuTe1+xLLMlm0flL+8aOL0wcHaPpXQ+G7mTUtFs4kLRxwLsmbozEDhQR04wSev55q14os/O8K3ttAoX93kBeOAQxA/AGuacfaR512Imrq5H4ctrKbT4r5P3kjc/N/AfTHYj1roa8U8OeIbnRNZi2szWsnyzRe3XIH94f8A1u9ezQTRTwpNEytHIoZWXuD0NXh6kZRtEmlNSWhLRRRXQahRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXIeOpnSztYldl3sx9jgDqPxqKk+SLkTKXKrm7f61p2meUL26SLzfujk5x347VVTxXpLt/r2Cf32Xj/EflXlkp+X7zb/AMv5Urtsj+Rq8946TeiMHXPZba8trxN1vPHMP9hgas14tYqtyNkS7Z1wdyuUOM8gYBzxXeWvjPT4rXbcLJG0S4wuX3YHA55B+v4100sVGfxaGsJ8yuQ+FtStNM8NvNcPtEl1JtVerYwOB+FdJZajaarC/ktu7MjDBGfUV5Vp8LOHdGaTcxEa9duCeB9Tk13+gaHJamK9uG2y7T+624254GT9KzoVpyailoNSbdjzrXdDbRvEM67P3G7fCf7ykf05H4V2fgDWPPt5dLlb5oP3kPuh6j8Cf19q6PV9EsdbtvJvId3911OGX6H+h4rhb/S5PB2uWV9FK00G7+7zt4DA/gaTi6E+ZfCY8jpy5lsenUVHG6yRq6PuVlyp9QehqSu86QooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArn/E2gya5HarFOsLQMWyyk5yMdq6CipnBTjyvYTSaszy+bwZrHnbUgVv8App5o2/h3/DFUZtEv4W8prG48zjhUJ/EY6169RXG8DDozF0I9Dye38K37xvdyr9njXnMnyufYAjrTNZjg8l/lZZVwN397ngH1r03UtPTUYNjO0bLyrL6+471xninw+9n4avbhr7zGXyxGWTZty6g5IJyOfT86znhOVe6VGil7qMfwTq8VhqrwvA0jTriPb1U88D64/lXrNeQaOraP8QYod6qsczQSSScBgQen1OMV6/W+Db5Gn0Ci3azCql9Yw39u0My5HY91PqKt0V1NJqzNSGCFLaBIkG1I1Cr9BU1FFMAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACuT+IUcr+ELp4pSqxsjSIAPnG4DBPbBIP4V1lc345P/ABR+oL/CyhW+hIzUVGlBtgjkfF+nyvoel65v3XDRqs0qrgnd8yE49On4ivQNE1FdV0W1vh/y1jy3+8OD+oNQSaTDqXhdNLlZliktkj3LjIwBgj6YFSeHtJ/sPQ7fT/P87yt3z7cZyxPTt1rKnTcZ3WzX4kKNpXNWiiiugsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKxfFaK/hbUdw+7CSvsR0NbVZXiExf8I/e+a/lxeWdzbc8d+Kip8DAsaS2/RrJ/71vGf/HRV2qenxCDTrWFeVjhRA30AHSrlVHYAooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFYXjFtnhHUj/0xx+ZArdrmfHLN/wAIzLCF3efIkbfQnr+lRU+BjW5taYmzS7NP7sKD/wAdFXKQDilqxBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVy/j7jwlcS/8APKSOT8nFdRWH4vh87wlqSf8ATHd+RB/pUVPgY1ublFVNMm+06Vazf89YUf8ANRVuqTuIKKKKYBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVDV1WTRr1H+60Lj9DV+qGsts0a6P8A0zNRU+FjW4mix+Xolkn92BB+grQqjo6+Xolgn923jH/joq9TgrRSEFFFFUAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVl+If+QBef9c/6iiis6vwP0Y1uWNL/wCQRZf9e6f+girlFFWhBRRRTAKKKKACiiigAooooAKKKKACiiigAooooA//2Q==";
		Map<String, String> params = new HashMap<>();
		params.put("file", fileinput);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.content(objectMapper.writeValueAsBytes(params));
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		AvatarInfoOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<AvatarInfoOutputDTO>() {
				});

		System.out.println("payload of test_uploadAvatar : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void test_updateProductDefaultDirectly() throws Exception {

		AccessTokenHolder.set(getAccessToken());

		String path = "/account/updateProductDefaultDirectly";
		Map<String, String> requestBody = new HashMap<>();

		requestBody.put("INTERNAL_TRANSFER_FROM_LIST", "381");
		requestBody.put("FAST_INTERBANK_TRANSFER_FROM_LIST", "382");
		requestBody.put("MOBILE_TOPUP_FROM_LIST", "381");
		requestBody.put("BILL_PAYMENT_FROM_LIST", "382");
		requestBody.put("TRANSFER_OTHER_FROM_LIST", "381");
		requestBody.put("EXTERNAL_TRANSFER_FROM_LIST", "382");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, AccessTokenHolder.get());
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(requestBody));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		assertNotNull(responseText);
	}

	@Test
	public void test_transferAccounts() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/transferAccounts";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")
				.param("productList", "TRANSFER_FROM_LIST").param("restrictions", "ACCOUNT_RESTRICTION_DEBIT"))
				.andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		List<TransferAccountDTO> dtos = objectMapper.readValue(rs, new TypeReference<List<TransferAccountDTO>>() {
		});

		System.out.println("test_transferAccounts: " + rs);

		assertNotNull(dtos);
	}

	@Test
	public void test_changeUsername() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/account/changeUsername";
		ChangeUsernameInput changeUsername = new ChangeUsernameInput();
		changeUsername.setNewUsername("ibtest9");
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.content(objectMapper.writeValueAsBytes(changeUsername));
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		ChangeUsernameOutput response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<ChangeUsernameOutput>() {
				});

		System.out.println("payload of test_changeUsername : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}
}
