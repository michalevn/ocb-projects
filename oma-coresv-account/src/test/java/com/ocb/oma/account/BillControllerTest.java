/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class BillControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void test_getTransferAutoBills() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);

		URIBuilder uriBuilder = new URIBuilder("/bill/getTransferAutoBills");
		uriBuilder.addParameter("dateFrom", "1511664871421");
		uriBuilder.addParameter("dateTo", "1574736871421");

		String path = uriBuilder.toString();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		String rs = result.getResponse().getContentAsString();
		List<TransferAutoBillOutputDTO> dtos = objectMapper.readValue(rs,
				new TypeReference<List<TransferAutoBillOutputDTO>>() {
				});

		System.out.println("test_getTransferAutoBills: " + rs);

		assertNotNull(dtos);

	}

	@Test
	public void test_createAutoBillTransferDirectly() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);

		String dtoJson = "{\"frequencyPeriodCount\":2,\"paymentSetting\":\"FULL\",\"recurringPeriod\":\"NOLIMIT\",\"fromAccount\":\"321\",\"customerId\":\"801004\",\"serviceCode\":\"ADSL\",\"serviceProviderCode\":\"FPT\",\"amountLimit\":0,\"firstExecutionDate\":\"2018-12-10T03:53:29.150Z\",\"finishDate\":\"2019-12-10T03:53:29.150Z\",\"currency\":\"VND\",\"frequencyPeriodUnit\":\"W\",\"nextExecutionDate\":\"2018-12-10T03:53:29.150Z\",\"serviceProviderCodeName\":\"XYZ\",\"serviceCodeName\":\"ABC\"}";
		CreateAutoBillTransferDirectlyDTO dto = objectMapper.readValue(dtoJson,
				CreateAutoBillTransferDirectlyDTO.class);

		URIBuilder uriBuilder = new URIBuilder("/bill/createAutoBillTransferDirectly");
		String path = uriBuilder.toString();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder = requestBuilder.contentType("application/json");
		requestBuilder = requestBuilder.content(objectMapper.writeValueAsBytes(dto));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("test_createAutoBillTransferDirectly: " + rs);
		assertEquals(rs, "true");

	}

	@Test
	public void test_deleteAutoBillTransfer() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/deleteAutoBillTransfer";
		String orderNumber = "4054";
		Map<String, String> params = new HashMap<>();
		params.put("orderNumber", orderNumber);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.content(objectMapper.writeValueAsBytes(params));
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		DeleteAutoBillTransferOutputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<DeleteAutoBillTransferOutputDTO>() {
				});

		System.out.println("payload of deleteAutoBillTransfer : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void test_modifyAutoBillTransferDirectly() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/modifyAutoBillTransferDirectly";
		TransferAutoBillDTO input = new TransferAutoBillDTO();
		input.setOrderNumber("4054");
		input.setFrequencyPeriodCount(1);
		input.setFrequencyPeriodUnit("");
		input.setFirstExecutionDate(new Date(1544582663000L));
		input.setPaymentSetting(null);
		input.setRecurringPeriod(null);
		input.setAmountLimit(1000000D);
		input.setFinishDate(new Date(1607741063L));

		Map<String, Object> params = new HashMap<>();
		params.put("orderNumber", input.getOrderNumber());
		params.put("frequencyPeriodCount", input.getFrequencyPeriodCount());
		params.put("frequencyPeriodUnit", input.getFrequencyPeriodUnit());
		params.put("firstExecutionDate", input.getFirstExecutionDate());
		params.put("paymentSetting", input.getPaymentSetting());
		params.put("recurringPeriod", input.getRecurringPeriod());
		params.put("amountLimit", input.getAmountLimit());
		params.put("finishDate", input.getFinishDate());

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.content(objectMapper.writeValueAsBytes(params));
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});

		System.out.println(
				"payload of test_modifyAutoBillTransferDirectly : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void test_standingOrderList() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/standingOrderList";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		String rs = result.getResponse().getContentAsString();
		List<StandingOrderOutputDTO> dtos = objectMapper.readValue(rs,
				new TypeReference<List<StandingOrderOutputDTO>>() {
				});

		System.out.println("test_standingOrderList: " + rs);

		assertNotNull(dtos);
	}

	@Test
	public void test_createStandingOrder() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/createStandingOrder";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setShortName("Nguyen");
		input.setAmount(1000L);
		input.setBeneficiary(new String[] { "SHORTNAME-801045" });
		input.setCreditAccount("0100100007826003");
		input.setRemarks(new String[] { "des" });
		input.setDebitAccountId("11");
		input.setCurrency("VND");
		input.setEndDate(new Date(1593144937000L));
		input.setPeriodUnit("M");
		input.setPeriodCount("1");
		input.setDayOfMonth(21);
		input.setStartDate(new Date(1545365737000L));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});
		System.out.println("test_createStandingOrder: " + objectMapper.writeValueAsString(response));

		assertNotNull(response);
	}

	@Test
	public void test_removeStandingOrder() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/removeStandingOrder";
		Map<String, String> input = new HashMap<>();
		input.put("standingOrderId", "0100100006076008.4");
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});
		System.out.println("removeStandingOrder: " + objectMapper.writeValueAsString(response));

		assertNotNull(response);
	}

	@Test
	public void test_modifyStandingOrderDirectly() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/bill/modifyStandingOrderDirectly";
		StandingOrderInputDTO input = new StandingOrderInputDTO();

		input.setStandingOrderId("0100100007825007.2");
		input.setShortName("Nguyen");
		input.setAmount(100000000L);
		input.setBeneficiary(new String[] { "SHORTNAME-801045" });
		input.setCreditAccount("0100100007825007");
		input.setRemarks(new String[] { "des" });
		input.setDebitAccountId("321");
		input.setCurrency("USD");
		input.setEndDate(new Date(1548867600000L));
		input.setPeriodUnit("M");
		input.setPeriodCount("1");
		input.setNextDate(new Date(1644374800000L));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});
		System.out.println("test_modifyStandingOrderDirectly: " + objectMapper.writeValueAsString(response));

		assertNotNull(response);
	}

}
