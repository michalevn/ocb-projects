/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.account.security.AccessTokenHolder;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class CardControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testFindCard() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/findCard";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		String rs = result.getResponse().getContentAsString();
		List<CardInfoDTO> response = objectMapper.readValue(rs, new TypeReference<List<CardInfoDTO>>() {
		});
		System.out.println("FindCard Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testRestrictCard() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/restrictCard";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("cardId", "1")
				.param("cardRestrictionReason", "STOLEN")).andExpect(status().isOk()).andReturn();
		String response = result.getResponse().getContentAsString();
		System.out.println("getRestrictCard Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testFindCardBlockades() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/findCardBlockades";
		CardBlockadeInputDTO input = new CardBlockadeInputDTO();
		input.setId("302");
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductType("CARD");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		List<CardBlockade> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<CardBlockade>>() {
				});
		System.out.println("getFindCardBlockades Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testPostAuthActivateCard() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/postAuthActivateCard";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("cardId", "302"))
				.andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});
		System.out.println("getFindCardBlockades Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testModifyCardAccountAutoRepayment() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/modifyCardAccountAutoRepayment";

		CardAccountAutoRepaymentDTO dto = new CardAccountAutoRepaymentDTO();
		dto.setCardId("302");
		dto.setRemitterAccountId("381");
		dto.setAutoRepaymentType("MIN");

		System.out.println("===> testModifyCardAccountAutoRepayment: " + objectMapper.writeValueAsString(dto));
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> testModifyCardAccountAutoRepayment: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void testFindCardAccountEntry() throws Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/findCardAccountEntry";

		CardAccountEntryQueryDTO dto = new CardAccountEntryQueryDTO();
		dto.setOperationAmountFrom(BigDecimal.valueOf(10));
		dto.setOperationAmountTo(BigDecimal.valueOf(120));
		dto.setProductId("302_CARD");
		dto.setPageNumber(1);
		dto.setPageSize(10);
		dto.setOperationType("all");
		dto.setDateFrom(new Date(1528650000000L));
		dto.setDateTo(new Date(1547139600000L));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> testFindCardAccountEntry: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void test_getPaymentDetails() throws Exception {

		URIBuilder uriBuilder = new URIBuilder("/card/getPaymentDetails");
		uriBuilder.addParameter("paymentId", "d725458f-d475-4a0c-8fd9-b895346869ac@waiting");

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = uriBuilder.toString();
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> testFindCardAccountEntry: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void testPostpaymentCardAnother() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/postPaymentCardAnother";
		PaymentCardAnotherInputDTO input = new PaymentCardAnotherInputDTO();
		input.setAccountId("381");
		input.setAccountName("TGTT Ca Nhan");
		input.setAmount(10000D);
		input.setCardNumber("1098765432");
		input.setCreditCardAccount("1234567890");
		input.setCreditCardAccountName("Nguyen van an");
		input.setRemark("remark");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		PaymentCardAnotherOuputDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<PaymentCardAnotherOuputDTO>() {
				});
		System.out.println("testPostpaymentCardAnother Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testFindCardByCardId() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/findCardByCardId";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("card_id", "302"))
				.andExpect(status().isOk()).andReturn();
		CardDetailByCardIdDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<CardDetailByCardIdDTO>() {
				});
		System.out.println("testFindCardByCardId Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testCardRejected() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/cardRejected";
		CardRejectedInputDTO cardRejectedInput = new CardRejectedInputDTO();
		cardRejectedInput.setOperationType("all");
		cardRejectedInput.setPageNumber(1);
		cardRejectedInput.setPageSize(10);
		cardRejectedInput.setProductId("302");
		cardRejectedInput.setProductType("CARD");
		cardRejectedInput.setDateFrom(new Date(1544071598L));
		cardRejectedInput.setDateTo(new Date(1607229998L));

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json")
						.content(objectMapper.writeValueAsString(cardRejectedInput)))
				.andExpect(status().isOk()).andReturn();
		List<CardRejectedDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<CardRejectedDTO>>() {
				});
		System.out.println("testCardRejected Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	/**
	 * @author docv
	 * 
	 * @throws Exception
	 */
	@Test
	public void test_restrictCard() throws Exception {

		AccessTokenHolder.set(getAccessToken());

		String path = "/card/restrictCard";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, AccessTokenHolder.get());
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").param("cardId", "301")
				.param("cardRestrictionReason", "LOST")).andExpect(status().isOk()).andReturn();
		OutputBaseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<OutputBaseDTO>() {
				});
		System.out.println("### test_restrictCard: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void testCardStatements() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/cardStatements";
		CardStatementsInputDTO input = new CardStatementsInputDTO();
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductId("302_CARD");
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		List<CardStatementsDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<CardStatementsDTO>>() {
				});
		System.out.println("cardStatements Result: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

	@Test
	public void test_changeLimitDirectly() throws Exception {

		AccessTokenHolder.set(getAccessToken());

		String path = "/card/changeLimitDirectly";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, AccessTokenHolder.get());
		requestBuilder.contentType("application/json");

		List<ChangeLimitDTO> limits = new ArrayList<>();
		ChangeLimitDTO dto = new ChangeLimitDTO();
		dto.setChannelType("WWW");
		dto.setValue(BigDecimal.valueOf(RandomUtils.nextDouble()));
		limits.add(dto);
		requestBuilder.content(objectMapper.writeValueAsBytes(limits));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		assertEquals(Boolean.TRUE.toString(), responseText);
	}

	@Test
	public void test_configStatusEcommerceCard() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/configStatusEcommerceCard";
		ConfigStatusEcommerceCardInput input = new ConfigStatusEcommerceCardInput();
		input.setCardNumber("9080898989");
		input.setType("LOCK");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(input));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		ConfigStatusEcommerceCardOutput output = objectMapper.readValue(result.getResponse().getContentAsString(),
				ConfigStatusEcommerceCardOutput.class);
		assertNotNull(output);
	}

	@Test
	public void test_cardPaymentInfo() throws Exception {

		UserToken user = getUserLogin(defaultUser, defaultPassword);
		String path = "/card/cardPaymentInfo";
		CardPaymentInfoInput input = new CardPaymentInfoInput();
		input.setCardAccountNumber("1234567");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, user.getOldAuthen().getJwtToken());
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(input));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		CardPaymentInfoOutput output = objectMapper.readValue(result.getResponse().getContentAsString(),
				CardPaymentInfoOutput.class);
		System.out.println("FindCard Result: " + objectMapper.writeValueAsString(output));
		assertNotNull(output);
	}

//	@Test
	public void test_cardStatemenDownload() throws Exception {

		AccessTokenHolder.set(getAccessToken());

		String path = "/card/cardStatemenDownload";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.header(HttpHeaders.AUTHORIZATION, AccessTokenHolder.get());
		requestBuilder.contentType("application/json");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").param("linkStatementFile",
						"http://10.96.21.155:8090/SaoKe/201806/20180615801003.pdf"))
				.andExpect(status().isOk()).andReturn();
		Resource response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<Resource>() {
				});
		System.out.println("### cardStatemenDownload: " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
	}

}
