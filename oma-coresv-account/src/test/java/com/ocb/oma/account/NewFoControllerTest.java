/**
 * 
 */
package com.ocb.oma.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.config.AccountConfigTest;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvAccountApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = AccountConfigTest.ACTIVE_PROFILE)
public class NewFoControllerTest extends AccountConfigTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void test_registerBankingProductOnline() throws Exception {
		String jsonInput = "{\"transactionInfo\":{\"cRefNum\":\"NHS_4\",\"clientCode\":\"LandingPage\",\"pRefNum\":null,\"transactionReturn\":null,\"transactionReturnMsg\":null},\"customerInfo\":{\"fullname\":\"test test full\",\"birthDay\":\"19831213\",\"gender\":\"FEMALE\",\"maritalStatus\":\"2.00\",\"jobInfo\":{\"professionalCode\":\"11\"},\"IDInfo\":[{\"IDNum\":\"000999888\",\"IDIssuedDate\":\"20170201\",\"IDIssuedLocation\":\"20\",\"IDType\":\"CMND\",\"nationality\":\"VN\"}],\"contactAddress\":{\"address1\":\"21/12A khu phố 1, đường HT35\",\"ward\":\"Phường Hiệp Thành\",\"email\":[\"huytest123@gmail.com\"],\"district\":\"21\",\"province\":\"1\"},\"contactInfo\":{\"mobileNumber\":[\"0909654321\"]},\"residentialAddress\":{\"address1\":\"21/12A khu phố 1 đường HT35\",\"ward\":\" Phường Hiệp Thành\",\"district\":\"21\",\"province\":\"1\"},\"permanentAddress\":{\"address1\":\"21/12A khu phố 1, đường HT35, Phường Hiệp Thành, Huyện Cần Giờ, TP. Hồ Chí Minh\"}},\"onlineService\":{\"source\":\"OMA\",\"registerDate\":\"20181001024315\",\"isOpenAtm\":\"1\",\"houseOwnership\":\"258\",\"stayInMonth\":\"23\",\"hasCreditCard\":\"1\",\"creditCardIssBy\":\"141\",\"creditCardLimit\":\"50000000\",\"isPlatinum\":\"1\",\"hasLifeInsu\":\"1\",\"lifeInsuBy\":\"282\",\"incomePerMonth\":\"33333333\",\"incomeSource\":\"348\",\"professionalClassify\":\"107\",\"department\":\"111\",\"companyName\":\"công ty TNHH dịch vụ vận chuyển Test test\",\"domain\":\"59\",\"position\":\"73\",\"monthOfLabour\":\"35\",\"labourContractType\":\"77\",\"expensePerMonth\":\"11111111\",\"netIncome\":null,\"cardType\":\"1\",\"imageOnCard\":null,\"cardExpectedLimit\":\"50000000\",\"cardReceivePlace\":\"87\",\"cardReceiveAddress\":\"TP. Hồ Chí Minh,Chi nhánh Thủ Đức\",\"secureQuestion\":\"81\",\"secureAnwser\":\"Trả lời\",\"contactPersonInfo\":\"Tên Tham Chiếu Test\",\"contactPersonRel\":\"126\",\"contactPersonMobile\":\"0987654321\",\"personalProfile1\":\"http://shopee.ocb.vn/upload/userInfo/cmndthe-can-cuoc_0909654321_20181001094658399.jpg;http://shopee.ocb.vn/upload/userInfo/cmndthe-can-cuoc_0909654321_20181001094658917.jpg;\",\"personalProfile2\":null,\"personalProfile3\":null,\"residentProfile1\":\"http://shopee.ocb.vn/upload/userInfo/so-giay-tam-tru_0909654321_20181001094726545.jpg\",\"residentProfile2\":\"http://shopee.ocb.vn/upload/userInfo/so-giay-tam-tru_0909654321_20181001094727051.jpg;\",\"residentProfile3\":null,\"residentProfile4\":null,\"financialProfile1\":\"http://shopee.ocb.vn/upload/userInfo/the-tin-dung_0909654321_20181001094712001.jpg\",\"financialProfile2\":\"http://shopee.ocb.vn/upload/userInfo/the-tin-dung_0909654321_20181001094712504.jpg;\",\"financialProfile3\":null,\"financialProfile4\":null,\"financialProfile5\":null,\"custRequirement\":null,\"insurProfileLink\":null,\"loanPurpose\":null,\"loanCollateralType\":null,\"loanAmount\":null,\"loanTerm\":null,\"productType\":\"Card\"}}";
		try {
			NewFoBankingProductOnline newFoBankingProductOnline = objectMapper.readValue(jsonInput,
					new TypeReference<NewFoBankingProductOnline>() {
					});
			System.out.println(newFoBankingProductOnline);
			String cRefNum = UUID.randomUUID().toString();
			newFoBankingProductOnline.getTransactionInfo().setcRefNum(cRefNum);
			newFoBankingProductOnline.getOnlineService().setSource("OMA");

			String path = "/newfo/registerBankingProductOnline";

			MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
			requestBuilder.contentType("application/json");
			requestBuilder.content(objectMapper.writeValueAsBytes(newFoBankingProductOnline));

			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
			String responseString = result.getResponse().getContentAsString();
			NewFoTransactionInfo output = objectMapper.readValue(responseString, NewFoTransactionInfo.class);

			assertNotNull(output);
			assertEquals(output.getcRefNum(), cRefNum);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
