/**
 * 
 */
package com.ocb.oma.account.config;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.dto.UserToken;

/**
 * @author Phu Hoang
 *
 */
public class AccountConfigTest {

	public static String DEV_PROFILE = "dev";
	public static String PROD_PROFILE = "prod";
	public static final String ACTIVE_PROFILE = "prod";

	@Autowired
	protected AuthenticationService authenService;

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	/**
	 * ibtest9/Abc@1234 
	 * ibtest10/Abc@1234
	 */

	protected String defaultUser = "ibtest9";
	protected String defaultPassword = "Abc@1234";
	protected String defaultDeviceId = "device-test-0001";
	protected String ocpTokenDefault = "TokenFromLoginService123";

	@Autowired
	ApplicationContext appConext;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	public String login() throws JsonProcessingException, Exception {
		return login(defaultUser, defaultPassword);
	}

	protected String login(String username, String password) throws JsonProcessingException, Exception {

		String path = "/account/login";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", username);
		payload.put("password", password);
		payload.put("deviceId", defaultDeviceId);
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		UserToken response = objectMapper.readValue(result.getResponse().getContentAsString(), UserToken.class);
		return response.getOldAuthen().getJwtToken();
	}

	protected UserToken getUserLogin(String username, String password) throws JsonProcessingException, Exception {

		String path = "/account/login";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", username);
		payload.put("password", password);
		payload.put("deviceId", defaultDeviceId);
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.out.println("responseText: " + responseText);
		UserToken response = objectMapper.readValue(responseText, UserToken.class);
		assertNotNull(response.getOldAuthen());
		assertNotNull(response.getOldAuthen().getJwtToken());
		assertNotNull(response.getOldAuthen().getAuthorizationMethod());
		// assertNotNull(response.getOldAuthen().getSmsOTPMobileNumber());
		assertNotNull(response.getOldAuthen().getCustomerId());

		return response;
	}

	protected String getAccessToken() throws JsonProcessingException, Exception {
		UserToken user = getUserLogin(defaultUser, defaultPassword);
		return user.getOldAuthen().getJwtToken();
	}

	public MockHttpServletRequestBuilder processGetAuth(String path, String... cridentials) throws Exception {
		String username = null, password = null;
		if (cridentials == null || cridentials.length < 2) {
			username = defaultUser;
			password = defaultPassword;
		}
		String token = login(username, password);
		return MockMvcRequestBuilders.get(path).header(HttpHeaders.AUTHORIZATION, token);

	}

	public MockHttpServletRequestBuilder processPostAuth(String path, String... cridentials) throws Exception {
		String username = null, password = null;
		if (cridentials == null || cridentials.length < 2) {
			username = defaultUser;
			password = defaultPassword;
		}
		String token = login(username, password);
		System.out.println("*****Login Token " + token);
		return MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, token);

	}

	public String getUUID() {
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		return randomUUIDString;
	}
}
