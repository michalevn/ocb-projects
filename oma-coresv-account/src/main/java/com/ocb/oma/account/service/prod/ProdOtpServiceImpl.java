/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.OtpService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdOtpServiceImpl implements OtpService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#sendAuthorizationSmsOtp(com.ocb.oma.
	 * dto.input.SendAuthorizationSmsOtpDTO, java.lang.String)
	 */
	@Override
	public AuthorizationResponse sendAuthorizationSmsOtp(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/authorization_requests/actions/ocb-send-sms-otp";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HashMap tt = new HashMap();

			HttpEntity<HashMap> entity = new HttpEntity<>(tt, headers);
			AuthorizationResponse result = restTemplate.postForObject(url, entity, AuthorizationResponse.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("sendAuthorizationSmsOtp error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#verifyOTPHWToken(com.ocb.oma.dto.input
	 * .VerifyOTPHWToken, java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data, String token) {

//		if ("12345678".equals(data.getOtpValue())) {
//			AuthorizationResponse response = new AuthorizationResponse();
//			response.setOperationStatus(AuthorizationResponse.CORRECT);
//			return response;
//		}

		if (token == null || data == null || data.getAuthId() == null || data.getOtpValue() == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		String url = host + "/api/authorization_requests/actions/ocb-verify-sms-otp";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<VerifyOtpToken> entity = new HttpEntity<>(data, headers);
			AuthorizationResponse result = restTemplate.postForObject(url, entity, AuthorizationResponse.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("verifyOTPHWToken error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.OtpService#AuthorizationResponse(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifyHwOTPToken(String hwToken, String token) {
		if (token == null || hwToken == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

//		if ("12345678".equals(hwToken)) {
//			AuthorizationResponse response = new AuthorizationResponse();
//			response.setOperationStatus(AuthorizationResponse.CORRECT);
//			return response;
//		}

		String url = host + "/api/authorization_requests/actions/ocb-verify-hwtoken";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, String> data = new HashMap<String, String>();
			data.put("tokenValue", hwToken);
			HttpEntity<Map> entity = new HttpEntity<>(data, headers);
			AuthorizationResponse result = restTemplate.postForObject(url, entity, AuthorizationResponse.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("verifyOTPHWToken error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#getChallengeCodeSoftOTP(com.ocb.oma.
	 * dto.input.GetChallengeCodeInputDTO, java.lang.String)
	 */
	@Override
	public String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input, String token) {
		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#verifySoftOTP(com.ocb.oma.oomni.dto.
	 * PaymentInfoDTO, com.ocb.oma.dto.input.VerifyOtpToken, java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifySoftOTP(PaymentInfoDTO paymentInfo, VerifyOtpToken otpCode, String token) {
		throw new NotImplementedException();
	}

	@Override
	public List<String> getAuthorizationPolicies(String userId, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/user/get/authorization_policies?userId=" + userId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			if (response.containsKey("authorizationMethods")) {
				List<String> authorizationMethods = (List<String>) response.get("authorizationMethods");
				return authorizationMethods;
			}
			return new ArrayList<>();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.OtpService#verifySoftOTP(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<QD630PoliciesDTO> getQD630Policies(String cif, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/user/get/QD630_Policies?cif=" + cif;
//		if (true) {
//			List<QD630PoliciesDTO> lst = new ArrayList<QD630PoliciesDTO>();
//			QD630PoliciesDTO sms = new QD630PoliciesDTO();
//			sms.setFromAmount(20000);
//			sms.setToAmount(10000000);
//			sms.setRegisteredMethod("SMSOTP");
//			lst.add(sms);
//			QD630PoliciesDTO hw = new QD630PoliciesDTO();
//			hw.setFromAmount(10000001);
//			hw.setToAmount(999900000);
//			hw.setRegisteredMethod("HW");
//			lst.add(hw);
//			return lst;
//
//		}

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<QD630PoliciesDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<QD630PoliciesDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@Override
	public List<PrepaidPhoneDTO> getListPrepaidPhone(String token) {
		/// api/prepaid_phone
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/prepaid_phone";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<PrepaidPhoneDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<PrepaidPhoneDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getListPrepaidPhone has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public QRcodeInfoOutputDTO checkQRcodeInfo(String token, QRcodeInfoInputDTO input) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		String path = "/api/qr_code/get/check_qrcode_info";
		String url = host + path;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<QRcodeInfoInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<QRcodeInfoOutputDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<QRcodeInfoOutputDTO>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("checkQRcodeInfo has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

}
