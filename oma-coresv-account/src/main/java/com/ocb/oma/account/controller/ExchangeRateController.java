package com.ocb.oma.account.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.ExchangeRatesService;
import com.ocb.oma.dto.ExchangeRateDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/exchangeRates")
public class ExchangeRateController extends BaseController {

	@Autowired
	private ExchangeRatesService exchangeRatesService;

	@RequestMapping(value = "/getExchangeRates")
	public List<ExchangeRateDTO> getExchangeRates() {
		return exchangeRatesService.getExchangeRates();
	}

}
