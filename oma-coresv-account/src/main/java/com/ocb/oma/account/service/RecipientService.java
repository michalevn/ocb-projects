/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;

/**
 * @author phuhoang
 *
 */
public interface RecipientService {
	Logger LOGGER = LoggerFactory.getLogger(RecipientService.class);

	/**
	 * lay danh sach nguoi thu huong
	 * 
	 * @param customerCif
	 * @param token
	 * @param pageSize 
	 * @param filerTemplateType 
	 * @return
	 */
	public List<RecipientDTO> getRecipientList(String token, String filerTemplateType, int pageSize);

	/**
	 * xoa nguoi thu huong
	 * 
	 * @param token
	 * @param recipient
	 * @return
	 */
	public Boolean deleteRecipient(String token, RecipientDTO recipient);

	/**
	 * Ham them nguoi thu huong
	 * 
	 * @param token
	 * @param recipientInputDTO
	 * @return
	 */
	public AddRecipientOutputDTO addRecipient(String token, RecipientInputDTO recipientInputDTO);

}
