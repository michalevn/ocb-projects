/**
 * 
 */
package com.ocb.oma.account.service.dev;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.StudentTuitionFeeService;
import com.ocb.oma.dto.StudentTuitionFeeDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("dev")
public class DevStudentTuitionFeeServiceImpl extends DevServiceImpl implements StudentTuitionFeeService {

	@Override
	public StudentTuitionFeeDTO get(String studentCode, String universityCode) {
		String jsonInput = "{\"tuitionFee\":{\"tuitionAmount\":1000,\"tuitionDebtAmount\":2000,\"description\":\"xyz\"},\"student\":{\"studentCode\":\"DOCV\",\"studentName\":\"Do Cao\",\"birthday\":\"2018-12-10T18:50:24.739Z\",\"gender\":\"M\",\"placeOfBirth\":\"Quang Nam\",\"nationalID\":\"VN\",\"className\":\"ABC\",\"period\":\"123\",\"department\":\"XYZ\"},\"tuitionPayment\":[{\"itemCode\":\"AAA\",\"courseType\":\"BBB\",\"paymentCode\":\"CCC\",\"description\":\"This is a list of every mode in the distribution. \",\"amount\":1234,\"discount\":200,\"itemType\":\"RTS\",\"isRequired\":0,\"dueDate\":\"2018-12-10T18:50:24.739Z\",\"subjectId\":\"123\",\"subjectName\":\"OCV\"}],\"_links\":{}}";
		return toObject(jsonInput, StudentTuitionFeeDTO.class);
	}

}
