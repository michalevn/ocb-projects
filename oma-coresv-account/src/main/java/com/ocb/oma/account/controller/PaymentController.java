package com.ocb.oma.account.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.account.service.OtpService;
import com.ocb.oma.account.service.PaymentService;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/payment")
public class PaymentController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

	@Autowired
	private AuthenticationService authenticateService;

	private OtpService otpService;

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/countBasketPaymentToProcess")
	public Integer countBasketPaymentToProcess(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.countBasketPaymentToProcess(token);
	}

	@RequestMapping(value = "/getTransactionFee")
	public Double getTransactionFee(HttpServletRequest request, @RequestBody TransactionFeeInputDTO input) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.getTransactionFee(input, token);
	}

	@RequestMapping(value = "/postPayment")
	public PostPaymentOutputDTO postPayment(HttpServletRequest request, @RequestBody PostPaymentInputDTO input) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.postPayment(input, token);
	}

	@RequestMapping(value = "/postAddBasket")
	public PostAddBasketDTO postAddBasket(HttpServletRequest request, @RequestBody PostPaymentInputDTO input) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.postAddBasket(input, token);
	}

	@RequestMapping(value = "/getListofBasket")
	public List<PaymentBasketDTO> getListofBasket(HttpServletRequest request,
			@RequestParam("amountFrom") Double amountFrom, @RequestParam("amountTo") Double amountTo,
			@RequestParam("customerId") String customerId,
			@RequestParam("realizationDateFrom") String realizationDateFrom,
			@RequestParam("realizationDateTo") String realizationDateTo, @RequestParam("statuses") String statuses) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.getListofBasket(token, amountFrom, amountTo, customerId, realizationDateFrom,
				realizationDateTo, statuses);
	}

	@RequestMapping(value = "/deleteBasket")
	public String deleteBasket(HttpServletRequest request, @RequestParam("basketId") String basketId) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.deleteBasket(basketId, token);
	}

	@RequestMapping(value = "/getListOfBillTransactions")
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(@RequestBody GetListOfBillTransactionsInputDTO dto,
			HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getListOfBillTransactions(dto.getStartDate(), dto.getEndDate(), token);
	}

	@RequestMapping(value = "/getListBillPaymentByService")
	public List<BillPaymentServiceDTO> getListBillPaymentByService(HttpServletRequest request,
			@RequestParam("serviceCode") String serviceCode, @RequestParam("duration") Integer duration) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getListBillPaymentByService(serviceCode, duration, token);
	}

	@RequestMapping(value = "/getBill")
	public BillPaymentServiceDTO getBill(@RequestBody GetBillInputDTO input, HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getBill(input, token);
	}

	@RequestMapping(value = "/getListOfServiceProvider")
	public List<ServiceProviderComposDTO> getListOfServiceProvider(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getListOfServiceProvider(token);
	}

	@RequestMapping(value = "/loadEVNparameter")
	public List<EVNParameter> loadEVNparameter(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.loadEVNParameter(token);
	}

	@RequestMapping(value = "/updateBasket")
	public PostAddBasketDTO updateBasket(HttpServletRequest request,
			@RequestBody PostPaymentInputDTO inputPaymentInputDTO) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return paymentService.updateBasket(inputPaymentInputDTO, token);
	}

	@RequestMapping(value = "/postBaskets")
	public PostListBasketDTO postBaskets(@RequestBody List<String> transfersIds, HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		PostListBasketDTO rs = paymentService.postBaskets(transfersIds, token);
		return rs;
	}

	@RequestMapping(value = "/getPaymentsInFuture")
	public List<PaymentsInFutureDTO> getPaymentsInFuture(HttpServletRequest request,
			@RequestBody PaymentsInFutureInputDTO input) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getPaymentsInFuture(token, input);
	}

	@RequestMapping(value = "/getDetailPaymentsInFuture")
	public PaymentsInFutureDTO getDetailPaymentsInFuture(HttpServletRequest request,
			@RequestParam("idFuturePayment") String idFuturePayment) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return paymentService.getDetailPaymentsInFuture(token, idFuturePayment);
	}

}
