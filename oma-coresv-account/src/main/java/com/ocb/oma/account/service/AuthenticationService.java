package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;

public interface AuthenticationService {
	Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

	public boolean login(String username, String password);

	public UserToken loadUserInfo(String username);

	/**
	 * 
	 * @param username
	 * @param password
	 * @param mobileDevice
	 * @return
	 */
	UserToken loginAndLoadUserInfo(String username, String password, String mobileDevice);

	/**
	 * ham thay doi password
	 * 
	 * @param username
	 * @param newPass
	 * @param token
	 * @param token2
	 * @return
	 */
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId, String token);

	
}
