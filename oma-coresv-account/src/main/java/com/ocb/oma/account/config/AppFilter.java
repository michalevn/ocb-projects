package com.ocb.oma.account.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;

import com.ocb.oma.account.log.LogRefIdHolder;
import com.ocb.oma.account.security.AccessTokenHolder;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AppFilter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOGGER.info(">>> Initiating Filter: " + AppFilter.class.getName());
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String refId = request.getHeader("X-REF-ID");
		LogRefIdHolder.set(refId);
		LOGGER.info("X-REF-ID: " + request.getHeader("X-REF-ID"));

		String accessToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		AccessTokenHolder.set(accessToken);

		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		LOGGER.info("Filter Destroyed: " + getClass().getName());
	}

}
