/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.PaymentService;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentItem;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.ServiceForBillPaymentDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.oomni.dto.ServiceProviderForBillPaymentDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdPayementImpl implements PaymentService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	private static final Log LOG = LogFactory.getLog(ProdPayementImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentBasketService#countBasketPaymentToProcess(
	 * java.lang.String)
	 */
	@Override
	public Integer countBasketPaymentToProcess(String token) {
		// TODO Auto-generated method stub
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}

		String url = host + "/api/payments_basket/get/count_basket_payments_ready_to_process";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			Integer readyToProcess = (Integer) data.get("readyToProcess");
			return readyToProcess;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getTransactionFee(com.ocb.oma.dto.
	 * input.TransactionFeeInputDTO, java.lang.String)
	 */
	@Override
	public Double getTransactionFee(TransactionFeeInputDTO input, String token) {
		// Lay phi giao dich
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/payment_execution/get/transaction_fee";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			HttpEntity<TransactionFeeInputDTO> entity = new HttpEntity<>(input, headers);
			Map<String, Object> result = restTemplate.postForObject(url, entity, Map.class);
			String testS = result.get("amount").toString();
			Double resultData = Double.parseDouble(testS);
			return resultData;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("get transaction_fee has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@Override
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/payment_execution/actions/post_payment";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<PostPaymentInputDTO> entity = new HttpEntity<>(input, headers);
			PostPaymentOutputDTO result = restTemplate.postForObject(url, entity, PostPaymentOutputDTO.class);

			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("post_payment has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input, String token) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payment_execution/actions/add_basket";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<PostPaymentInputDTO> entity = new HttpEntity<>(input, headers);
			PostAddBasketDTO result = restTemplate.postForObject(url, entity, PostAddBasketDTO.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("Add to Basket has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListofBasket(java.lang.String)
	 */
	@Override
	public List<PaymentBasketDTO> getListofBasket(String token, Double amountFrom, Double amountTo, String customerId,
			String realizationDateFrom, String realizationDateTo, String statuses) {
		// ham lay danh sach gio giao dich
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		// ?amountFrom=0&amountTo=10000000&customerId=&realizationDateFrom=1453185446&realizationDateTo=1608358090000&statuses=READY
		String url = host + "/api/payments_basket?amountFrom=" + amountFrom + "&amountTo=" + amountTo + "&customerId="
				+ customerId + "&realizationDateFrom=" + realizationDateFrom + "&realizationDateTo=" + realizationDateTo
				+ "&statuses=" + statuses;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<PaymentBasketDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<PaymentBasketDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("get List of Basket has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public String deleteBasket(String basketId, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payment_execution/actions/remove_transfer_basket";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HashMap tt = new HashMap();
			tt.put("transferId", basketId);
			String resStr = "";
			HttpEntity<HashMap> entity = new HttpEntity<>(tt, headers);
			OutputBaseDTO result = restTemplate.postForObject(url, entity, OutputBaseDTO.class);
			if (result.getResultCode().equals("1")) {
				resStr = MessageConstant.OK_MESSAGE;
			} else {
				resStr = MessageConstant.CAN_NOT_REMOVE_TRANSFER_BASKET;
			}

			return resStr;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("deleteBasket error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListOfServiceProvider(java.lang
	 * .String)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<ServiceProviderComposDTO> getListOfServiceProvider(String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<ServiceProviderComposDTO> dtos = new ArrayList<>();
		String url = host + "/api/transfer/search/transfer_service_providers";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {

						String serviceCode = (String) map.get("serviceCode");
						String serviceName = (String) map.get("serviceName");

						if ("HOCPHI".equals(serviceCode)) {
							continue;
						}

						ServiceProviderComposDTO dto = new ServiceProviderComposDTO();
						ServiceForBillPaymentDTO service = new ServiceForBillPaymentDTO();
						service.setServiceCode(serviceCode);
						service.setServiceName(serviceName);
						dto.setService(service);

						List<ServiceProviderForBillPaymentDTO> providers = new ArrayList<>();
						if (map.containsKey("providers")) {
							List<Map> listProviders = (List<Map>) map.get("providers");
							for (Map listProvider : listProviders) {
								ServiceProviderForBillPaymentDTO serviceProviderForBillPaymentDTO = new ServiceProviderForBillPaymentDTO();
								serviceProviderForBillPaymentDTO
										.setProviderCode((String) listProvider.get("providerCode"));
								serviceProviderForBillPaymentDTO
										.setProviderName((String) listProvider.get("providerName"));
								providers.add(serviceProviderForBillPaymentDTO);
							}
						}
						dto.setProviders(providers);
						dtos.add(dto);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
		return dtos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListOfBillTransactions(java.
	 * util.Date, java.util.Date, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(Date startDate, Date toDate, String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<BillPaymentHistoryDTO> dtos = new ArrayList<>();

		try {

			Map<String, String> params = new HashMap<>();

			String url = host + "/api/transfer/search/transfer_bill_history";

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			params.put("fromDate", dateFormat.format(startDate));
			params.put("toDate", dateFormat.format(toDate));

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<Map<String, String>> request = new RequestEntity<>(params, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();

			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						BillPaymentHistoryDTO dto = objectMapper.convertValue(map, BillPaymentHistoryDTO.class);
						dtos.add(dto);
					}
				}
			}
			return dtos;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListBillPaymentByService(java.
	 * lang.String, java.lang.String)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override

	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration, String token) {
		// /cbp-ocb-service/api/transfer/get/beneficiary_bills
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<BillPaymentServiceDTO> dtos = new ArrayList<>();
		String url = host + "/api/transfer/get/beneficiary_bills";
		Map<String, Object> params = new HashMap<>();
		params.put("serviceCode", serviceCode);
		params.put("duration", duration);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						BillPaymentServiceDTO dto = objectMapper.convertValue(map, BillPaymentServiceDTO.class);
						if (map.containsKey("serviceProvider")) {

							Map<String, Object> serviceProvider = (Map<String, Object>) map.get("serviceProvider");

							ServiceProviderForBillPaymentDTO provider = new ServiceProviderForBillPaymentDTO();
							provider.setProviderCode((String) serviceProvider.get("providerCode"));
							provider.setProviderName((String) serviceProvider.get("providerName"));
							dto.setProvider(provider);

							if (serviceProvider.containsKey("service")) {
								Map<String, Object> service = (Map<String, Object>) serviceProvider.get("service");
								ServiceForBillPaymentDTO serviceForBillPaymentDTO = new ServiceForBillPaymentDTO();
								serviceForBillPaymentDTO.setServiceCode((String) service.get("serviceCode"));
								serviceForBillPaymentDTO.setServiceName((String) service.get("serviceName"));
								dto.setService(serviceForBillPaymentDTO);
							}
						}

						if (map.containsKey("billItem")) {
							List<Map> billItems = (List<Map>) map.get("billItem");
							List<BillPaymentItem> billPaymentItems = new ArrayList<>();
							for (Map billItem : billItems) {
								BillPaymentItem billPaymentItem = objectMapper.convertValue(billItem,
										BillPaymentItem.class);
								billPaymentItems.add(billPaymentItem);
							}
							dto.setBillItems(billPaymentItems);
						}

						dtos.add(dto);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
		return dtos;
	}

	/*
	 * (non-Javadoc) 1.48 Ham truy van bill - transfer_bill
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getBill(com.ocb.oma.dto.input.
	 * GetBillInputDTO, java.lang.String)
	 */
	@Override
	public BillPaymentServiceDTO getBill(GetBillInputDTO input, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/transfer/get/transfer_bill";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<GetBillInputDTO> entity = new HttpEntity<>(input, headers);
			Map<String, HashMap<String, Object>> mapResult = restTemplate.postForObject(url, entity, Map.class);

			RequestEntity<GetBillInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = responseEntity.getBody();

			if (data == null) {
				throw new OmaException(MessageConstant.BILL_NOT_FOUND);
			}

			BillPaymentServiceDTO dtoOutput = new BillPaymentServiceDTO();
			String address = (String) data.get("address");
			dtoOutput.setAddress(address);

			if (data.containsKey("amount")) {
				Map<String, Number> amountMap = (Map<String, Number>) data.get("amount");
				Number amountValue = amountMap.get("value");
				if (amountValue != null) {
					dtoOutput.setAmount(amountValue.doubleValue());
				}
			}

			String billCode;
			if (data.get("billCode") == null) {
				billCode = null;
			} else {
				billCode = data.get("billCode").toString();
			}
			dtoOutput.setBillCode(billCode);

			List<BillPaymentItem> lstBillPaymentItem = new ArrayList<>();
			List<Map<String, Object>> billItems = (List<Map<String, Object>>) data.get("billItem");

			for (Map<String, Object> billItem : billItems) {

				Double amountMonth = null;
				if (billItem.containsKey("amountMonth")) {
					Map<String, Number> amountMonthMap = (Map<String, Number>) billItem.remove("amountMonth");
					Number amountMonthValue = amountMonthMap.get("value");
					if (amountMonthValue != null) {
						amountMonth = amountMonthValue.doubleValue();
					}
				}

				BillPaymentItem billPayItem = objectMapper.convertValue(billItem, BillPaymentItem.class);
				if (amountMonth != null && amountMonth > 0) {
					billPayItem.setAmountMonth(amountMonth);
				}
				lstBillPaymentItem.add(billPayItem);
			}

			dtoOutput.setBillItems(lstBillPaymentItem);
			dtoOutput.setCustomerCode(getString(data, "customerCode"));
			dtoOutput.setCustomerName(getString(data, "customerName"));
			dtoOutput.setPaymentType(getString(data, "paymentType"));
			dtoOutput.setMeterNumber(getString(data, "meterNumber"));
			dtoOutput.getPhoneNumber(getString(data, "phoneNumber"));
			// serviceProvider
			ServiceProviderForBillPaymentDTO provider = new ServiceProviderForBillPaymentDTO();
			ServiceForBillPaymentDTO serviceForBillPayment = new ServiceForBillPaymentDTO();
			if (data.containsKey("serviceProvider")) {
				Map<String, Object> serviceProvider = (Map<String, Object>) data.get("serviceProvider");
				provider.setProviderCode(getString(serviceProvider, "providerCode"));
				provider.setProviderName(getString(serviceProvider, "providerName"));
				if (serviceProvider.containsKey("service")) {
					Map<String, Object> service = (Map<String, Object>) serviceProvider.get("service");
					serviceForBillPayment.setServiceCode(getString(service, "serviceCode"));
					serviceForBillPayment.setServiceName(getString(service, "serviceName"));
				}
			}
			dtoOutput.setService(serviceForBillPayment);
			dtoOutput.setProvider(provider);

			return dtoOutput;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("transfer_bill has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	private String getString(Map<String, ?> map, String key) {
		if (map.containsKey(key)) {
			return String.valueOf(map.get(key));
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.PaymentService#loadEVNParameter()
	 */
	@Override
	public List<EVNParameter> loadEVNParameter(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/partner_settings/get/evn_params";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			List<EVNParameter> result = new ArrayList<>();

			if (data.get("content") == null) {
				return result;
			}
			result = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<EVNParameter>>() {
					});

			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("get List of Basket has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#updateBasket(com.ocb.oma.dto.input
	 * .PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input, String token) {
		// Url: /cbp-ocb-service/api/payment_execution/actions/modify_transfer_basket
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payment_execution/actions/modify_transfer_basket";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<PostPaymentInputDTO> entity = new HttpEntity<>(input, headers);
			PostAddBasketDTO result = restTemplate.postForObject(url, entity, PostAddBasketDTO.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("Add to Basket has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc) Ham thuc hien thanh toan trong gio hang
	 * 
	 * @see com.ocb.oma.account.service.PaymentService#postBaskets(java.util.List)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public PostListBasketDTO postBaskets(List<String> transfersIds, String token) {
		// cbp-ocb-service/api/payment_execution/actions/post_basket_payment
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payment_execution/actions/post_basket_payment";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map params = new HashMap();
			params.put("transfersId", transfersIds);
			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));
			ResponseEntity<PostListBasketDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<PostListBasketDTO>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("sendAuthorizationSmsOtp error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<PaymentsInFutureDTO> getPaymentsInFuture(String token, PaymentsInFutureInputDTO input) {
		// : /cbp-ocb-service/api/payments
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payments";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<PaymentsInFutureInputDTO> entity = new HttpEntity<>(input, headers);

			RequestEntity<PaymentsInFutureInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = responseEntity.getBody();
			List<PaymentsInFutureDTO> result = new ArrayList<>();
			if (data.get("content") == null) {
				return result;
			}
			result = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<PaymentsInFutureDTO>>() {
					});

			return result;

		} catch (Exception e) {
			LOGGER.error("getPaymentsInFuture has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String token, String idFuturePayment) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/payments/" + idFuturePayment;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			PaymentsInFutureDTO dtoOutput = new PaymentsInFutureDTO();

			dtoOutput = objectMapper.readValue(objectMapper.writeValueAsString(data),
					new TypeReference<PaymentsInFutureDTO>() {
					});

			return dtoOutput;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error(" getDetailPaymentsInFuture has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

}
