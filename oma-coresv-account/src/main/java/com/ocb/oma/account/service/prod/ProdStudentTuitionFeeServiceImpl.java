/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.StudentTuitionFeeService;
import com.ocb.oma.dto.StudentDTO;
import com.ocb.oma.dto.StudentTuitionFeeDTO;
import com.ocb.oma.dto.TuitionPaymentDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdStudentTuitionFeeServiceImpl extends ProdServiceImpl implements StudentTuitionFeeService {

	@Override
	public StudentTuitionFeeDTO get(String studentCode, String universityCode) {
		String path = "/api/transfer/get/tuition_fee";
		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("studentCode", studentCode);
		requestBody.put("universityCode", universityCode);
		StudentTuitionFeeDTO studentTuitionFee = getPostResponse(path, requestBody, StudentTuitionFeeDTO.class);
		List<TuitionPaymentDTO> tuitionPayment = studentTuitionFee.getTuitionPayment();
		List<TuitionPaymentDTO> newTuitionPayment = new ArrayList<>();
		if (tuitionPayment != null) {
			for (TuitionPaymentDTO tuitionPaymentDTO : tuitionPayment) {
				if (StringUtils.isNotBlank(tuitionPaymentDTO.getSubjectId())) {
					newTuitionPayment.add(tuitionPaymentDTO);
				}
			}
		}
		studentTuitionFee.setTuitionPayment(newTuitionPayment);

		StudentDTO student = studentTuitionFee.getStudent();
		if (student != null) {
			if (StringUtils.isBlank(student.getStudentCode())) {
				return null;
			}
		}

		return studentTuitionFee;
	}

}
