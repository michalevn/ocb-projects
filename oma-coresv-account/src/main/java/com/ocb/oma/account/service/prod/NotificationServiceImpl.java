/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.ocb.oma.account.service.NotificationService;
import com.ocb.oma.dto.notification.NotificationMessageDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Lazy
public class NotificationServiceImpl implements NotificationService {

	@Value("${firebase.service-account.path}")
	private String fireBaserServiceKeyPath;

	@PostConstruct
	public void init() throws IOException {

		FileInputStream serviceAccount = new FileInputStream(fireBaserServiceKeyPath);

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(serviceAccount))
				.setDatabaseUrl("https://data01.firebaseio.com/").build();

		FirebaseApp.initializeApp(options);
	}

	@Override
	public void sendNotificationMessage(NotificationMessageDTO message, String sendType)
			throws FirebaseMessagingException {
		// TODO Auto-generated method stub
		com.google.firebase.messaging.Message.Builder builder = Message.builder();
		if (NotificationMessageDTO.SEND_TYPE_DEVICE.equalsIgnoreCase(sendType)) {
			builder.setToken(message.getTarget());
		} else if (NotificationMessageDTO.SEND_TYPE_TOPIC.equalsIgnoreCase(sendType)) {
			builder.setTopic(message.getTarget());
		}

		Message dto = builder.build();

		FirebaseMessaging.getInstance().send(dto);

	}

}
