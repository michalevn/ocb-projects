/**
 * 
 */
package com.ocb.oma.account.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.StudentTuitionFeeDTO;

/**
 * @author docv
 *
 */
public interface StudentTuitionFeeService {

	static final Logger LOGGER = LoggerFactory.getLogger(StudentTuitionFeeService.class);

	public StudentTuitionFeeDTO get(String studentCode, String universityCode);
}
