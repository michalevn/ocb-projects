/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;

/**
 * @author phuhoang
 *
 */
public interface AccountService {

	static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

	/**
	 * lay danh sach tai khoan
	 * 
	 * @param customerCif
	 * @param token
	 * @return
	 */
	List<AccountInfo> getAccountList(String customerCif, String token);

	/**
	 * lay thong tin cua customer
	 * 
	 * @param token
	 * @return
	 */
	CustomerInfoDTO getCustomerDetail(String token);

	/**
	 * update icon of account
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param token
	 * @param imageLink
	 * @return
	 */
	Boolean updateAccountIcon(String accountNo, String customerCif, String token, String imageLink);

	/**
	 * update account Name
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param token
	 * @param updatedName
	 * @return
	 */
	Boolean updateAccountName(String accountId, String customerCif, String token, String updatedName);

	/**
	 * tim kiem lich su transation
	 * 
	 * @param token
	 * @param transactionType
	 * @param fromDate
	 * @param toDate
	 * @param fromAmount
	 * @param toAmount
	 * @param page
	 * @param pageSize
	 * @return
	 */
	List<TransactionHistoryDTO> findTransactionHistory(String token, String accountNo, String transactionType,
			Date fromDate, Date toDate, Double fromAmount, Double toAmount, Integer page, Integer pageSize);

	/**
	 * lay summary account boi cif
	 * 
	 * @param token
	 * @param accountId
	 * @return
	 */
	AccountSummaryDTO loadAccountSummarByCurrencies(String token, String accountId, Integer summaryPeriod);

	/**
	 * lay limit cua account
	 * 
	 * @param paymentType
	 * @param token
	 * @return
	 */
	AccountLimitDTO getAccountLimnit(String paymentType, String token);

	/**
	 * lay ten account bang so tai khoan
	 * 
	 * @param accountNo
	 * @param token
	 * @return
	 */
	String getAccountNameByAccountNo(String accountNo, String token);

	/**
	 * getAccountNameNapas
	 * 
	 * @param accountNo
	 * @param bankCode
	 * @param cardNumber
	 * @param debitAccount
	 * @param token
	 * @return
	 */
	String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount, String token);

	/**
	 * 
	 * @param token
	 * @return
	 */
	UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token);

	/**
	 * Lay DS transactions dang bi block
	 * 
	 * @param accountId
	 * @param token
	 * @param currentPage
	 * @param pageSize
	 * @return List
	 */
	List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer pageNumber, Integer pageSize,
			String token);

	/**
	 * ham lay avatar cua User
	 * 
	 * @param omniToken
	 * @return
	 */
	AvatarInfoOutputDTO getAvatarInfo(String omniToken);

	/**
	 * Lay CIF tu accountNo
	 * 
	 * @param accountNo
	 * @param token
	 * @return
	 */
	String getCifByAccountNo(String accountNo, String token);

	/**
	 * Lấy ds CIF từ ds số TK
	 * 
	 * @param cifDTO
	 * @param token
	 * @return
	 */
	Map<String, String> getCIFFromAccountList(List<String> accountNos, String token);

	/**
	 * Lay thong tin personal cua khach hang
	 * 
	 * @param token
	 * @return
	 */
	CustomerPersonalInfoDTO getPersonalInfo(String token);


	/**
	 * Ham tai len anh dai dien
	 * 
	 * @param token
	 * @param file
	 * @return
	 */
	AvatarInfoOutputDTO uploadAvatar(String token, String file);

	/**
	 * Cập nhật lại tài khoản mặc định khi hiển thị cho các giao dịch thanh toán
	 * 
	 * @param map Mã loại sản phẩm => Id số tài khoản <br>
	 *            Mã loại sản phẩm:
	 *            <ul>
	 *            <li>INTERNAL_TRANSFER_FROM_LIST: chuyển tiền nội bộ</li>
	 *            <li>FAST_INTERBANK_TRANSFER_FROM_LIST: chuyển tiền nhanh</li>
	 *            <li>MOBILE_TOPUP_FROM_LIST: nạp tiền điện thoại</li>
	 *            <li>BILL_PAYMENT_FROM_LIST: thanh toán hóa đơn</li>
	 *            <li>TRANSFER_OTHER_FROM_LIST: khác</li>
	 *            <li>EXTERNAL_TRANSFER_FROM_LIST: chuyển tiền liên ngân hàng</li>
	 *            </ul>
	 *            <br>
	 *            Id số tài khoản: Lấy từ output trường accountId Hàm lấy danh sách
	 *            tài khoản được chuyển khoản và thanh toán
	 * @return
	 */
	Boolean updateProductDefaultDirectly(Map<String, String> map);

	/**
	 * Lay danh sach tai khoan dc chuyen khoan va thanh toan
	 * 
	 * @param productList
	 * @param restrictions
	 * @param token
	 * @return
	 */
	public List<TransferAccountDTO> transferAccounts(String productList, String restrictions, String token);

	/**
	 * change Username
	 * @param changeUsername
	 * @param token
	 * @return
	 */
	ChangeUsernameOutput changeUsername(ChangeUsernameInput changeUsername, String token);

	/**
	 * 1.12 Hàm lấy thông tin chi tiết một tài khoản
	 * 
	 * @param accountNum
	 * @return
	 */
	AccountDetails findAccountDetail(String accountNum);

}
