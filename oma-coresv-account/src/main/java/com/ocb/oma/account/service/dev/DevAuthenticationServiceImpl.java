/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;

/**
 * @author Phu Hoang
 *
 */
@Service
@Profile("dev")
public class DevAuthenticationServiceImpl implements AuthenticationService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#login(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean login(String username, String password) {

		LOGGER.info("dev profile going");
		LOGGER.debug("dev profile going");
		if (username.equals("fail3time")) {
			throw new OmaException(MessageConstant.ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL);
		}
		List<String> users = Arrays.asList("admin", "ibtest9", "ibtest10", "pentest01", "pentest02");
		if (users.contains(username) == false) {
			return false;
		}
		if (username.equals("pentest01") || username.equals("pentest02")) {
			if (password.equals("OcbPenTest@#$%") == false) {
				return false;
			}
		}
		return true;
	}

	@Override
	public UserToken loginAndLoadUserInfo(String username, String password, String mobileDevice) {

		boolean checkLogin = login(username, password);

		if (checkLogin) {
			return loadUserInfo(username);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#loadUserInfo(java.lang.
	 * String)
	 */
	@Override
	public UserToken loadUserInfo(String username) {
		// TODO Auto-generated method stub
		LOGGER.debug("dev profile going. Load user {}", username);
		UserToken userToken = new UserToken();
		userToken.setUsername(username);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801004";
		;
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, OtpAuthorizeMethod.SMSOTP.getCode(), "09737391833");
		userToken.setOldAuthen(oldOmni);

		return userToken;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#changePassword(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	@Override
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId, String token) {
		try {
			if (oldPass == null || oldPass.isEmpty() || newPass == null || newPass.isEmpty()) {
				throw new OmaException(MessageConstant.MISSING_PARAMETER);
			}
			ChangePasswordDTO dto = new ChangePasswordDTO();
			if(oldPass.equals(newPass)) {
				dto.setCorrectOldPassword(true);
				dto.setChanged(true);
				dto.setMessage(MessageConstant.OK_MESSAGE);
				dto.setRequiredActionAfterChangePassword("NEXT_TOKEN_CODE");
			}else {
				dto.setCorrectOldPassword(true);
				dto.setChanged(false);
				dto.setMessage("Not"+ MessageConstant.OK_MESSAGE);
				dto.setRequiredActionAfterChangePassword("NOTHING");
			}
			
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			throw new OmaException(MessageConstant.CAN_NOT_UPDATE_PASSWORD);
		}
	}
}
