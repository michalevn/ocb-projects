/**
 * 
 */
package com.ocb.oma.account.service;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.ocb.oma.dto.notification.NotificationMessageDTO;

/**
 * @author phuhoang
 *
 */
public interface NotificationService {

	public void sendNotificationMessage(NotificationMessageDTO message, String sendType) throws FirebaseMessagingException;
}
