/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.CardService;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.CardStatementHeader;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.MerchantAddressDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardAccountStatementHeaderInputDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardDetailByIdDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevCardServiceImpl extends DevServiceImpl implements CardService {

	private static final Log LOG = LogFactory.getLog(DevCardServiceImpl.class);

	@Autowired
	private ObjectMapper objectMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.CardService#findCard(java.lang.String)
	 */
	@Override
	public List<CardInfoDTO> findCard(String token) {
		LOG.info("findCard");

		List<CardInfoDTO> dtos = new ArrayList<>();
		String[] statusArr = { "ACTIVE", "INACTIVE", "BLOCKED", "RESTRICTED" };
		for (int i = 0; i < statusArr.length; i++) {
			CardInfoDTO dto = new CardInfoDTO();
			dto.setId("60" + i);
			dto.setName("Name00" + i);
			dto.setCardNo("08088038485399453" + i);
			dto.setAvailableFunds(200000000D * (i));
			dto.setAccountId(DevGeneratedDataUtil.generateAccountId());
			dto.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
			dto.setCardOwnerName("cardOwnerName");
			dto.setCardOwnerLastName("cardOwnerLastName");
			dto.setStatus(statusArr[i]);
			dto.setCardType(DevGeneratedDataUtil.generateCardType());
			dto.setCardIDType("" + i + 1);
			dto.setCardSubType("EXTRA");
			dto.setCurrency(DevGeneratedDataUtil.generateCurrency());
			dto.setBlockedFunds(null);
			dto.setDateExpirationEnd(new Date(1709161200000L));
			dto.setSettlmntDate(null);
			dto.setLimitLeft(1000000D * i);
			dto.setCardImage(null);
			dto.setLastOperationDate(null);
			dto.setBalance(null);
			dto.setCurrentUserOwner(false);
			dto.setCurrentUserHolder(false);
			dto.setResume(false);
			dto.setEmbossedName("NAME1037454");
			dto.setLimitInCycle(20000000D);
			dto.setEmbossedCompanyName("embossedCompanyName");
			dto.setLimitUsed(0D + i);
			dto.setCardTypeNumber(null);
			dto.setMinimumRepaymentAmount((0D + i) * i);
			dto.setRepaymentDueDate(null);

			dtos.add(dto);

		}

		return dtos;
	}

	@Override
	public OutputBaseDTO restrictCard(String token, String cardId, String restrictionReason) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (cardId.equals("301")) {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		} else if (cardId.equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		}

		return dto;
	}

	@Override
	public List<CardBlockade> findCardBlockades(String token, CardBlockadeInputDTO input) {
		List<CardBlockade> lstRes = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			CardBlockade blockade = new CardBlockade();
			blockade.setCardUser("cardUser00" + (i + 1));
			blockade.setCurrency(DevGeneratedDataUtil.generateCurrency());
			MerchantAddressDTO merchantAddress = new MerchantAddressDTO();
			merchantAddress.setCountry("Korea");
			merchantAddress.setPostCode("NEW0098");
			merchantAddress.setStreet("BuSan09");
			merchantAddress.setTown("town0" + i + 1);

			blockade.setMerchantAddress(merchantAddress);
			blockade.setMerchantName("Credit limit for Liability Contract");
			blockade.setOperationDate(new Date(1559008354));
			blockade.setRejectionReason("rejectionReason00" + i + 1);
			blockade.setTransactionAmount(1500000D * i);
			blockade.setTransactionDesc("Credit limit for Liability Contract");
			blockade.setTransactionType("transactionType0" + i);

			lstRes.add(blockade);
		}

		return lstRes;
	}

	@Override
	public void modifyCardAccountAutoRepayment(String authorization,
			CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		LOG.info("modifyCardAccountAutoRepayment");
		LOG.info("CardId: " + cardAccountAutoRepayment.getCardId());
		LOG.info("RemitterAccountId: " + cardAccountAutoRepayment.getRemitterAccountId());
		LOG.info("AutoRepaymentType: " + cardAccountAutoRepayment.getAutoRepaymentType());
	}

	@Override
	public List<CardAccountEntryDTO> findCardAccountEntry(String token,
			CardAccountEntryQueryDTO cardAccountEntryQuery) {
		List<CardAccountEntryDTO> items = new ArrayList<>();

		String jsonInput = "{\"id\":\"4938691\",\"bookingDate\":1544115600000,\"transactionDate\":1544115600000,\"embossedName\":null,\"embossedCompanyName\":null,\"transactionType\":\"\",\"amount\":500000,\"currency\":\"VND\",\"transactionNameLocation\":\"PGD NGUYEN THAI BINH\",\"counterParty\":\"\",\"cardNo\":\"9704 **** **** 2464\",\"cardAccount\":\"0037100007643009\",\"originalCurrency\":\"VND\",\"amountInOriginalCurrency\":null,\"details\":\"PGD NGUYEN THAI BINH, HCM, VNM\",\"fullName\":null,\"organizationExchangeRate\":null,\"bankExchangeRate\":null,\"organizationExchangeRateValue\":null,\"bankExchangeRateValue\":null,\"balanceAfter\":null,\"detailsAvailable\":null,\"billingType\":\"DEBIT\"}";
		try {
			CardAccountEntryDTO dto = objectMapper.readValue(jsonInput, CardAccountEntryDTO.class);
			items.add(dto);
		} catch (IOException e) {
			e.printStackTrace();
		}

		LOG.info("findCardAccountEntry => success");
		return items;
	}

	@Override
	public PaymentsInFutureDTO getPaymentDetails(String token, String paymentId) {
		PaymentsInFutureDTO paymentsInFutureDTO = new PaymentsInFutureDTO();
		String json = "{\"id\":\"d725458f-d475-4a0c-8fd9-b895346869ac@waiting\",\"accountNo\":\"0037100003605005\",\"accountCurrency\":null,\"accountId\":\"43\",\"accountName\":\"ACCOUNT801004\",\"recipientName\":[\"Huynh Khac Nhan\"],\"recipientAddress\":null,\"senderName\":null,\"senderAddress\":null,\"recipientAccountNo\":\"0100100007826003\",\"recipientAccountId\":null,\"amount\":1000000,\"currency\":\"VND\",\"title\":null,\"transferType\":\"INTERNAL\",\"paymentType\":\"INTERNAL_PAYMENT\",\"status\":null,\"realizationDate\":null,\"registrationDate\":null,\"paymentDetails\":{\"bankDetails\":null,\"fee\":\"0\",\"feeAccount\":\"0037100003605005\",\"creditAccountBankCode\":\"\",\"creditAccountBankBranchCode\":null,\"creditAccountProvinceCode\":null},\"cyclicDefinition\":null,\"isCyclic\":false,\"deliveryDate\":null,\"charges\":null,\"lastRealizationDesc\":null,\"realizationDateShiftReason\":null,\"templateId\":null,\"saveTemplate\":null,\"templateName\":null,\"owner\":\"GB NAME 1-801004\",\"operationStatus\":\"IN_PROCESSING\",\"transactionId\":null,\"originalCustomerId\":\"\",\"originalCustomerName\":null,\"transactionTypeDesc\":\"Chuyển khoản trong OCB\",\"description\":[\"Huynh Khac Nhan OCB\"],\"eWalletPhoneNumber\":null,\"addToBasket\":false,\"originalCustomerVisible\":false,\"_links\":{\"get_edit_zus_from_basket_additional_info\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting/get/get_edit_zus_from_basket_additional_info.json\"},\"self\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting\"},\"realize\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting/actions/realize.json\"}}}";
		try {
			paymentsInFutureDTO = objectMapper.readValue(json, PaymentsInFutureDTO.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paymentsInFutureDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.CardService#postpaymentCardAnother(java.lang.
	 * String, com.ocb.oma.dto.input.PaymentCardAnotherInputDTO)
	 */
	@Override
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(String token, PaymentCardAnotherInputDTO input) {
//		throw new NotImplementedException();
		PaymentCardAnotherOuputDTO dto = new PaymentCardAnotherOuputDTO();
		dto.setAuthErrorCause("authErrorCause");
		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.CardService#findCardByCardId(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public CardDetailByCardIdDTO findCardByCardId(String token, String cardId) {
		CardDetailByCardIdDTO dto = new CardDetailByCardIdDTO();
		dto.setId("302");
		dto.setName(null);
		dto.setCardNo("5209 **** **** 6675");
		dto.setAvailableFunds(30000000D);
		dto.setAccountId("383");
		dto.setAccountNo("601688010030001");
		dto.setCardOwnerName(null);
		dto.setCardOwnerLastName(null);
		dto.setStatus(null);
		dto.setCardType("CREDIT");
		dto.setCardIDType("CCMEM00501");
		dto.setCardSubType("MAIN");
		dto.setCurrency("VND");
		dto.setBlockedFunds(null);
		dto.setDateExpirationEnd(new Date(1709139600000L));
		dto.setSettlmntDate(null);
		dto.setLimitLeft(null);
		CardDetailByIdDTO cardDetails = new CardDetailByIdDTO();
		cardDetails.setDateExpirationStart(null);
		cardDetails.setDailyTrxLimit(500000000D);
		cardDetails.setDailyCashLimit(100000000D);
		cardDetails.setDailyTrxLimitCount(30);
		cardDetails.setDailyCashLimitCount(0);
		cardDetails.setMaxDailyTrxLimit(500000000D);
		cardDetails.setMaxDailyCashLimit(null);
		cardDetails.setMaxDailyTrxLimitCount(30);
		cardDetails.setMaxDailyCashLimitCount(0);
		cardDetails.setInternetLimit(0D);
		cardDetails.setMotoLimit(0D);
		cardDetails.setInternetUseLimit(0);
		cardDetails.setMotoUseLimit(0);
		cardDetails.setMinPayment(null);
		cardDetails.setMinPaymentDate(null);
		cardDetails.setLimitInCycle(30000000D);
		cardDetails.setDebt(null);
		cardDetails.setDebtPrevSettlmnt(null);
		cardDetails.setDebtPrevUnsettled(null);
		cardDetails.setMinDebtPrevSettlmnt(null);
		cardDetails.setAutomaticRepayment("INACTIVE");
		cardDetails.setBillingCycle("15");
		cardDetails.setCashTransactionsInterest(24D);
		cardDetails.setNonCashTransactionsInterest(24D);
		cardDetails.setAccountLimit(30000000D);
		cardDetails.setLimitUsed(0D);
		cardDetails.setMinDebtPrevSettlmntPlain(null);
		cardDetails.setStatementDistributionDesc(null);
		cardDetails.setInstallments(null);
		dto.setCardDetails(cardDetails);

		dto.setLastOperationDate(null);

		dto.setBalance(30000000D);
		dto.setHolders(null);
		dto.setHolders(null);
		dto.setCurrentUserOwner(false);
		dto.setCurrentUserHolder(false);
		dto.setResume(false);
		dto.setActions(null);

		dto.setCustomName("");
		dto.setHolders(null);
		dto.setLimitInCycle(30000000D);
		dto.setEmbossedCompanyName(null);
		dto.setCardImage(null);
		dto.setLimitUsed(null);
		dto.setCardTypeNumber(null);
		dto.setMinimumRepaymentAmount(null);
		dto.setRepaymentDueDate(null);

		return dto;
	}

	@Override
	public List<CardAutoRepaymentDTO> cardAutoRepayment(String token) {
		List<CardAutoRepaymentDTO> lst = new ArrayList<>();
		CardAutoRepaymentDTO dto = new CardAutoRepaymentDTO();
		dto.setAccountId(DevGeneratedDataUtil.generateAccountId());
		dto.setAccountName("TGTT Ca Nhan");
		dto.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
		dto.setCardAccountId("383");
		dto.setCardAccountName(null);
		dto.setCardAccountNo(null);
		dto.setCardId("302");
		dto.setCardNo("5209 **** **** 6675");
		dto.setCardRepaymentType("MIN");
		dto.setId("1");
		lst.add(dto);
		CardAutoRepaymentDTO dto1 = new CardAutoRepaymentDTO();
		dto1.setAccountId(DevGeneratedDataUtil.generateAccountId());
		dto1.setAccountName("TGTT Ca Nhan");
		dto1.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
		dto1.setCardAccountId("385");
		dto1.setCardAccountName(null);
		dto1.setCardAccountNo(null);
		dto1.setCardId("301");
		dto1.setCardNo("5209 **** **** 6098");
		dto1.setCardRepaymentType("TOTAL");
		dto1.setId("2");
		lst.add(dto1);
		return lst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.CardService#cardRejected(java.lang.String,
	 * com.ocb.oma.dto.input.CardRejectedInputDTO)
	 */
	@Override
	public List<CardRejectedDTO> cardRejected(String authorization, CardRejectedInputDTO cardRejectedInput) {
		List<CardRejectedDTO> lst = new ArrayList<>();
		List<String> transactionTypes = Arrays.asList("RutTienMat", "ChuyenKhoanTrong", "ChuyenKhoanNgoai",
				"ChuyenKhoanNoiBo", "ChuyenKhoan24/7");
		for (int i = 0; i < transactionTypes.size(); i++) {

			CardRejectedDTO dto = new CardRejectedDTO();
			dto.setOperationDate(new Date(1607229998000L));
			dto.setTransactionType(transactionTypes.get(i));
			dto.setTransactionAmount(300000D * i);
			dto.setCurrency(DevGeneratedDataUtil.generateCurrency());
			MerchantAddressDTO merchantAddress = new MerchantAddressDTO();
			merchantAddress.setCountry("VietNam");
			merchantAddress.setPostCode("P0000" + i + 1);
			merchantAddress.setStreet("street " + i + 9);
			merchantAddress.setTown("town " + i + 1);
			dto.setMerchantAddress(merchantAddress);
			dto.setTransactionDesc("transactionDesc" + i + 1);
			dto.setRejectionReason("rejectionReason" + i + 1);
			dto.setCardUser("cardUser" + i + 1);
			lst.add(dto);
		}
		return lst;
	}

	@Override
	public OutputBaseDTO postAuthActivateCard(String token, String cardId) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (cardId.equals("301")) {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (cardId.equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		return dto;
	}

	@Override
	public List<CardStatementsDTO> cardStatements(String token, CardStatementsInputDTO input) {
		List<CardStatementsDTO> lst = new ArrayList<>();
		CardStatementsDTO dto = new CardStatementsDTO();
		dto.setId("20180615601688010030001");
		dto.setTotalRepaymentAmount(253320D);
		dto.setMinimumRepaymentAmount(200000D);
		dto.setCycleStart(new Date(1526403600000L));
		dto.setCycleEnd(new Date(1528995600000L));
		dto.setNextPaymentDate(new Date(1531242000000L));
		dto.setOverDueAmount(0D);
		dto.setCardAccountNumber("601688010030001");
		dto.setDownloadLink("http://10.96.11.11:8083/saoke/201806/20180615601688010030001.pdf");
		lst.add(dto);

		CardStatementsDTO dto1 = new CardStatementsDTO();
		dto1.setId("20180615601688010030001");
		dto1.setTotalRepaymentAmount(358515D);
		dto1.setMinimumRepaymentAmount(300000D);
		dto1.setCycleStart(new Date(1529082000000L));
		dto1.setCycleEnd(new Date(1531587600000L));
		dto1.setNextPaymentDate(new Date(1533834000000L));
		dto1.setOverDueAmount(0D);
		dto1.setCardAccountNumber("601688010030001");
		dto1.setDownloadLink("http://10.96.11.11:8083/saoke/201807/20180715601688010030001.pdf");
		lst.add(dto1);

		CardStatementsDTO dto2 = new CardStatementsDTO();
		dto2.setId("20180615601688010030001");
		dto2.setTotalRepaymentAmount(575481D);
		dto2.setMinimumRepaymentAmount(880481D);
		dto2.setCycleStart(new Date(1534352400000L));
		dto2.setCycleEnd(new Date(1536944400000L));
		dto2.setNextPaymentDate(new Date(1539190800000L));
		dto2.setOverDueAmount(0D);
		dto2.setCardAccountNumber("601688010030001");
		dto2.setDownloadLink("http://10.96.11.11:8083/saoke/201806/20180615601688010030001.pdf");
		lst.add(dto2);

		CardStatementsDTO dto3 = new CardStatementsDTO();
		dto3.setId("20180615601688010030001");
		dto3.setTotalRepaymentAmount(465979D);
		dto3.setMinimumRepaymentAmount(500000D);
		dto3.setCycleStart(new Date(1531674000000L));
		dto3.setCycleEnd(new Date(1534266000000L));
		dto3.setNextPaymentDate(new Date(1536512400000L));
		dto3.setOverDueAmount(0D);
		dto3.setCardAccountNumber("601688010030001");
		dto3.setDownloadLink("http://10.96.11.11:8083/saoke/201807/20180715601688010030001.pdf");
		lst.add(dto3);

		return lst;
	}

	@Override
	public Boolean changeLimitDirectly(List<ChangeLimitDTO> limits) {
		return true;
	}

	@Override
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(String token,
			ConfigStatusEcommerceCardInput input) {

		ConfigStatusEcommerceCardOutput cardOutput = new ConfigStatusEcommerceCardOutput();
		cardOutput.setStatus("status");
		cardOutput.setErrorMsg("");
		cardOutput.setSuccess(false);
		return cardOutput;
		// TODO Auto-generated method stub
//		throw new NotImplementedException();
	}

	@Override
	public CardPaymentInfoOutput cardPaymentInfo(String token, CardPaymentInfoInput input) {
		// TODO Auto-generated method stub
//		throw new NotImplementedException();
		CardPaymentInfoOutput dto = new CardPaymentInfoOutput();
		dto.setAccountNumber("accountNumber");
		dto.setAccountStatus("accountStatus");
		dto.setBillCycleDate(new Date());
		dto.setCardNumber("cardNumber");
		return dto;
	}

	@Override
	public InputStream cardStatemenDownload(String token, String linkStatementFile) throws IOException {
		String basePath = getClass().getClassLoader().getResource("").getPath();
		String filePath = basePath + "20180715801003.pdf";
		File file = new File(filePath);
		System.out.println(">>>" + file.exists());
		FileInputStream fis = new FileInputStream(file);
		return fis;
	}
}
