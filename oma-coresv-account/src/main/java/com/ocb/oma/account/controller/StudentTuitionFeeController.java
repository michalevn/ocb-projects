package com.ocb.oma.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.StudentTuitionFeeService;
import com.ocb.oma.dto.StudentTuitionFeeDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/student-tuition-fee")
public class StudentTuitionFeeController extends BaseController {

	@Autowired
	private StudentTuitionFeeService studentTuitionFeeService;

	@RequestMapping(value = "/get")
	public StudentTuitionFeeDTO get(@RequestParam("studentCode") String studentCode,
			@RequestParam("universityCode") String universityCode) {
		return studentTuitionFeeService.get(studentCode, universityCode);
	}

}
