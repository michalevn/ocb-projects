/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.ocb.oma.account.service.AccountService;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositSubDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdAccountServiceImpl extends ProdServiceImpl implements AccountService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	private static final Log LOG = LogFactory.getLog(ProdAccountServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountList(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<AccountInfo> getAccountList(String customerCif, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/account?customerId=" + customerCif;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<AccountInfo> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<AccountInfo>>() {
					});

			if (lst != null && !lst.isEmpty()) {
				for (AccountInfo accountInfo : lst) {
					accountInfo.setAccountName(accountInfo.getCustomName());
				}
			}

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@Override
	public CustomerInfoDTO getCustomerDetail(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/customer/get/customer_details";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			String authType;
			if (data.get("authType") == null) {
				authType = null;
			} else {
				authType = data.get("authType").toString();
			}
			String customerType;
			if (data.get("customerType") == null) {
				customerType = null;
			} else {
				customerType = data.get("customerType").toString();
			}
			String packageType;
			if (data.get("packageType") == null) {
				packageType = null;
			} else {
				packageType = data.get("packageType").toString();
			}
			String phoneNumber;
			if (data.get("phoneNumber") == null) {
				phoneNumber = null;
			} else {
				phoneNumber = data.get("phoneNumber").toString();
			}
			CustomerInfoDTO result = new CustomerInfoDTO(customerType, packageType, authType, phoneNumber);
			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.AccountService#updateAccountIcon(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountIcon(String accountNo, String customerCif, String token, String imageLink) {
		// Bo, khong can
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.AccountService#updateAccountName(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountName(String accountId, String customerCif, String token, String updatedName) {

		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/product_settings/actions/update_product_custom_name";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HashMap data = new HashMap();
			data.put("id", "" + accountId);
			data.put("customName", updatedName);
			HttpEntity<HashMap> entity = new HttpEntity<>(data, headers);
			Map<String, String> mapResult = restTemplate.postForObject(url, entity, Map.class);
			if (mapResult != null && "OK".equalsIgnoreCase(mapResult.get("content")) == true) {
				return true;
			}
			return false;

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("updateAccountName error ", e);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#findTransactionHistory(java.lang.
	 * String, java.lang.String, java.lang.String, java.util.Date, java.util.Date,
	 * java.lang.Double, java.lang.Double, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<TransactionHistoryDTO> findTransactionHistory(String token, String accountNo, String transactionType,
			Date fromDate, Date toDate, Double fromAmount, Double toAmount, Integer page, Integer pageSize) {

		if (token == null || accountNo == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String toDateStr = df.format(toDate);
		String formDateStr = df.format(fromDate);

		HashMap postData = new HashMap();
		postData.put("accountId", accountNo);
		postData.put("dateFrom", formDateStr);
		postData.put("dateTo", toDateStr);
		postData.put("operationAmountFrom", fromAmount);
		postData.put("operationAmountTo", toAmount);
		postData.put("pageNumber", page);
		postData.put("pageSize", pageSize);
		String url = host + "/api/transaction/get/search_history";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<HashMap> entity = new HttpEntity<>(postData, headers);
			Map<String, HashMap<String, Object>> mapResult = restTemplate.postForObject(url, entity, Map.class);
			List<TransactionHistoryDTO> result = objectMapper.readValue(
					objectMapper.writeValueAsString(mapResult.get("list").get("content")),
					new TypeReference<List<TransactionHistoryDTO>>() {
					});

			return result;

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("findTransactionHistory has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#loadAcciybtSynnartByCurrencies(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public AccountSummaryDTO loadAccountSummarByCurrencies(String token, String accountId, Integer summaryPeriod) {
		
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		
		String url = host + "/api/account/get/account_summary?accountId=" + accountId + "&summaryPeriod="
				+ summaryPeriod;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			AccountSummaryDTO result = objectMapper.readValue(objectMapper.writeValueAsString(data),
					new TypeReference<AccountSummaryDTO>() {
					});
			result.setLastInComeCurrency(result.getLastDebitInfo().getCurrency());
			result.setLastIncome(result.getLastDebitInfo().getAmount());
			result.setLastIncomeDate(result.getLastDebitInfo().getDate());
			result.setLastOutComeCurrency(result.getLastCreditInfo().getCurrency());
			result.setLastOutCome(result.getLastCreditInfo().getAmount());
			result.setLastOutcomeDate(result.getLastCreditInfo().getDate());
			
			return result;

		} catch (Exception e) {
			LOGGER.error("loadAccountSummarByCurrencies has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountLimnit(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AccountLimitDTO getAccountLimnit(String paymentType, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/transaction_limit?paymentType=" + paymentType;// lay han muc giao dich kh
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			AccountLimitDTO result = objectMapper.readValue(objectMapper.writeValueAsString(data),
					new TypeReference<AccountLimitDTO>() {
					});

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getAccountLimnit has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountNameByAccountNo(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public String getAccountNameByAccountNo(String accountNo, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/account/get/account_reciepient_name";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			URIBuilder uriBuilder = new URIBuilder(url);
			uriBuilder.addParameter("accountId", accountNo);

			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					uriBuilder.build());

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data != null && data.containsKey("content") && data.get("content") instanceof String) {
				return (String) data.get("content");
			}
			throw new OmaException(MessageConstant.ACCOUNT_NOT_FOUND);
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}

	/*
	 * (non-Javadoc) bythaoctm
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getUpcomingRepaymentDeposit(java.
	 * lang.String)
	 */
	@Override
	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token) {
		// TODO Auto-generated method stub
		/// cbp-ocb-service/api/account/get/upcomming_repayment_deposit
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/account/get/upcomming_repayment_deposit";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo = objectMapper.readValue(
					objectMapper.writeValueAsString(data.get("minMaturityDateDepositInfo")),
					new TypeReference<UpcomingRepaymentDepositSubDTO>() {
					});

			UpcomingRepaymentDepositSubDTO loanRepaymentInfo = objectMapper.readValue(
					objectMapper.writeValueAsString(data.get("loanRepaymentInfo")),
					new TypeReference<UpcomingRepaymentDepositSubDTO>() {
					});
			UpcomingRepaymentDepositSubDTO creditCardInfo = objectMapper.readValue(
					objectMapper.writeValueAsString(data.get("creditCardInfo")),
					new TypeReference<UpcomingRepaymentDepositSubDTO>() {
					});
			Boolean creditCardWithoutStatement = (Boolean) data.get("creditCardWithoutStatement");
			UpcomingRepaymentDepositDTO result = new UpcomingRepaymentDepositDTO(minMaturityDateDepositInfo,
					loanRepaymentInfo, creditCardInfo, creditCardWithoutStatement);
			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getBlockedTransaction(java.lang.
	 * String, java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	public List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer currentPage, Integer pageSize,
			String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		/// api/blockades?accountId=”string”&pageNumber=”int”&pageSize=”int”
		String url = host + "/api/blockades?accountId=" + accountId + "&currentPage=" + currentPage + "&pageSize="
				+ pageSize;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			List<TransactionBlockedDTO> result = new ArrayList<>();

			if (data.get("content") == null) {
				return result;
			}
			result = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<TransactionBlockedDTO>>() {
					});
			List<TransactionBlockedDTO> lstresult = new ArrayList<>();
			for (TransactionBlockedDTO transactionBlockedDTO : result) {
				transactionBlockedDTO.setBlockadeDescription(transactionBlockedDTO.getRemark());
				transactionBlockedDTO.setBlockedAmount(transactionBlockedDTO.getAmount());
				transactionBlockedDTO.setBlockedBy(transactionBlockedDTO.getMerchant());
				transactionBlockedDTO.setCreatedDate(transactionBlockedDTO.getDateFrom());
				transactionBlockedDTO.setReason(transactionBlockedDTO.getType());
				lstresult.add(transactionBlockedDTO);
			}

			return lstresult;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getBlockedTransaction has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountNameNapas(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount,
			String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String urlByAccount = host + "/api/recipient/get/recipient_name_by_account?accountNumber=" + accountNo
				+ "&bankCode=" + bankCode + "&debitAccount=" + debitAccount;
		String urlByCard = host + "/api/recipient/get/recipient_name_by_card?cardNumber=" + cardNumber
				+ "&debitAccount=" + debitAccount;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			String result = "";
			if (accountNo.isEmpty() && bankCode.isEmpty() && !cardNumber.isEmpty()) {

				RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
						URI.create(urlByCard));
				ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
						new ParameterizedTypeReference<Map<String, Object>>() {
						});
				Map<String, Object> data = responseEntity.getBody();
				;
				result = (String) data.get("name");
			}
			if (!accountNo.isEmpty() && !bankCode.isEmpty() && cardNumber.isEmpty()) {

				RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
						URI.create(urlByAccount));
				ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
						new ParameterizedTypeReference<Map<String, Object>>() {
						});
				Map<String, Object> data = responseEntity.getBody();
				;
				String name;
				if (data.get("name") == null) {
					name = null;
				} else {
					name = data.get("name").toString();
				}
				result = name;
			}
			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getAccountNameNapas has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAvatarInfo(java.lang.String)
	 */
	@Override
	public AvatarInfoOutputDTO getAvatarInfo(String omniToken) {
		if (omniToken == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/avatar/get/my_avatar";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + omniToken);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			AvatarInfoOutputDTO dtoOutput = new AvatarInfoOutputDTO();

			dtoOutput = objectMapper.readValue(objectMapper.writeValueAsString(response),
					new TypeReference<AvatarInfoOutputDTO>() {
					});

			return dtoOutput;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getAvatarInfo has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public String getCifByAccountNo(String accountNo, String token) {
		Map<String, String> cifDTOs = this.getCIFFromAccountList(Arrays.asList(accountNo), token);
		return cifDTOs.get(accountNo);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, String> getCIFFromAccountList(List<String> accountNos, String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		Map<String, String> CIFs = new HashMap<>();
		String url = host + "/api/account/get/getCIFFromAccountList";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, List<String>> params = new HashMap<>();
			params.put("lstAccountNum", accountNos);
			RequestEntity<Map<String, List<String>>> request = new RequestEntity<>(params, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> result = response.getBody();
			if (result.containsKey("content")) {
				List<Map> list = (List<Map>) result.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						String nrbNumber = (String) map.get("nrbNumber");
						Map owner = (Map) map.get("owner");
						if (owner != null && owner.containsKey("globusId")) {
							String globusId = owner.get("globusId").toString();
							CIFs.put(nrbNumber, globusId);
						}
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
		return CIFs;
	}

	@Override
	public CustomerPersonalInfoDTO getPersonalInfo(String token) {
		String requestPath = "/api/customer/get/ocb_customer_personal_data";
		CustomerPersonalInfoDTO dto = getGetResponse(requestPath, null, CustomerPersonalInfoDTO.class);
		return dto;
	}

	@Override
	public AvatarInfoOutputDTO uploadAvatar(String token, String file) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/avatar/uploads/upload_avatar";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			//
			byte[] decode = Base64.decodeBase64(file);

			LinkedMultiValueMap<String, String> pdfHeaderMap = new LinkedMultiValueMap<>();
			pdfHeaderMap.add("Content-disposition", "form-data; name=file; filename=file.png");
			pdfHeaderMap.add("Content-type", "image/png");
			HttpEntity<byte[]> doc = new HttpEntity<byte[]>(decode, pdfHeaderMap);

			LinkedMultiValueMap<String, Object> multipartReqMap = new LinkedMultiValueMap<>();
			multipartReqMap.add("file", doc);

			HttpEntity<LinkedMultiValueMap<String, Object>> reqEntity = new HttpEntity<>(multipartReqMap, headers);

			ResponseEntity<AvatarInfoOutputDTO> resE = restTemplate.exchange(url, HttpMethod.POST, reqEntity,
					AvatarInfoOutputDTO.class);

			return resE.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public Boolean updateProductDefaultDirectly(Map<String, String> map) {
		Map<String, Object> requestBody = new HashMap<>();
		List<Map<String, String>> productListDefaults = new ArrayList<>();
		for (Entry<String, String> entry : map.entrySet()) {
			Map<String, String> productListDefault = new HashMap<>();
			productListDefault.put("productListId", entry.getKey());
			productListDefault.put("productId", entry.getValue());
			productListDefaults.add(productListDefault);
		}
		requestBody.put("productListDefaults", productListDefaults);
		String path = "/api/product_settings/actions/update_product_default_directly";
		OutputBaseDTO outputBase = getPostResponse(path, requestBody, OutputBaseDTO.class);
		if ("1".equals(outputBase.getResultCode())) {
			return true;
		}
		LOGGER.error("==> OCB_ERROR: [{}] {}", outputBase.getResultCode(), outputBase.getResultMsg());
		throw new OmaException(MessageConstant.SERVER_OCP_ERROR, outputBase.getResultMsg());
	}

	@Override
	public List<TransferAccountDTO> transferAccounts(String productList, String restrictions, String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<TransferAccountDTO> transferAccountDTOs = new ArrayList<>();
		String url = host + "/api/transfer/search/transfer_accounts?productList=" + productList + "&restrictions="
				+ restrictions;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						TransferAccountDTO transferAccountDTO = objectMapper.convertValue(map,
								TransferAccountDTO.class);
						transferAccountDTOs.add(transferAccountDTO);
					}
				}
			}
			return transferAccountDTOs;
		} catch (Exception e) {
			LOG.error("transferAccounts has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}

	@Override
	public ChangeUsernameOutput changeUsername(ChangeUsernameInput changeUsername, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/customer/actions/change_username";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<ChangeUsernameInput> request = new RequestEntity<>(changeUsername, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<ChangeUsernameOutput> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<ChangeUsernameOutput>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("restrictCard has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public AccountDetails findAccountDetail(String accountNum) {

		String path = "/api/account/get/findAccountDetail";

		Map<String, String> requestBody = new HashMap<>();
		requestBody.put("accountNum", accountNum);

		AccountDetails accountDetails = getPostResponse(path, requestBody, AccountDetails.class);

		return accountDetails;
	}
}
