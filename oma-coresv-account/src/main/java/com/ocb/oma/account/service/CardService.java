/**
 * 
 */
package com.ocb.oma.account.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * @author phuhoang
 *
 */
public interface CardService {

	/**
	 * tim danh sach the tin dung cua User
	 * 
	 * @param token
	 * @return
	 */
	public List<CardInfoDTO> findCard(String token);

	/**
	 * Khoa the
	 * 
	 * @param token
	 * @param cardNumber
	 * @param restrictionReason
	 * @return
	 */
	public OutputBaseDTO restrictCard(String token, String cardNumber, String restrictionReason);

	/**
	 * Giao dich the phong toa
	 * 
	 * @param token
	 * @param accountNumber
	 * @param cardNumber
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	public List<CardBlockade> findCardBlockades(String token, CardBlockadeInputDTO input);

	/**
	 * kich hoat the
	 * 
	 * @param authorization
	 */

	/**
	 * Dang ky/sua/xoa trich no tu dong
	 * 
	 * @param token
	 * @param cardAccountAutoRepayment
	 */
	public void modifyCardAccountAutoRepayment(String token, CardAccountAutoRepaymentDTO cardAccountAutoRepayment);

	/**
	 * 
	 * Liet ke danh sach lich su giao dich cua the credit/debit
	 * 
	 * @param token
	 * @param cardAccountEntryQuery
	 * @return
	 */
	public List<CardAccountEntryDTO> findCardAccountEntry(String token, CardAccountEntryQueryDTO cardAccountEntryQuery);

	/**
	 * Lay thong tin chi tiet cua giao dich trong tuong lai.
	 * 
	 * @param token
	 * @param paymentId id cua payment duoc lay thu getPaymentsInFuture
	 * @return
	 */
	public PaymentsInFutureDTO getPaymentDetails(String token, String paymentId);

	/**
	 * Ham thanh toan the tin dung cho nguoi khac(OCP)
	 * 
	 * @param token
	 * @param input
	 * @return
	 */
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(String token, PaymentCardAnotherInputDTO input);

	/**
	 * Ham lay thong tin chi tiet the
	 * 
	 * @param token
	 * @param cardId
	 * @return
	 */
	public CardDetailByCardIdDTO findCardByCardId(String token, String cardId);

	/**
	 * Lay danh sach the trich no tu dong
	 * 
	 * @param token
	 * @return
	 */
	public List<CardAutoRepaymentDTO> cardAutoRepayment(String token);

	/**
	 * lay danh sach giao dich the bi tu choi
	 * 
	 * @param authorization
	 * @param cardRejectedInput
	 * @return
	 */
	public List<CardRejectedDTO> cardRejected(String token, CardRejectedInputDTO cardRejectedInput);

	/**
	 * 1.75 Hàm kích hoạt thẻ - card_activation_directly
	 * 
	 * Kích hoạt thẻ
	 * 
	 * @param cardId
	 * @return
	 */
	public OutputBaseDTO postAuthActivateCard(String token, String cardId);

	/**
	 * Sao ke
	 * 
	 * @param token
	 * @param input
	 * @return
	 */
	public List<CardStatementsDTO> cardStatements(String token, CardStatementsInputDTO input);

	Boolean changeLimitDirectly(List<ChangeLimitDTO> limits);

	/**
	 * cap nhat trang thai lock/unlock giao dich online the tin dung 
	 * @param token
	 * @param input
	 * @return
	 */
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(String token,
			ConfigStatusEcommerceCardInput input);

	/**
	 * truy van thong tin the thanh toan - card_payment_info
	 * @param token
	 * @param input
	 * @return
	 */
	public CardPaymentInfoOutput cardPaymentInfo(String token, CardPaymentInfoInput input);

	/**
	 * tai file sao ke
	 * @param token
	 * @param linkStatementFile
	 * @return
	 * @throws IOException 
	 */
	public InputStream cardStatemenDownload(String token, String linkStatementFile) throws IOException;

}
