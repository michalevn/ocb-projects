package com.ocb.oma.account.service.dev;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.WebLimitService;
import com.ocb.oma.dto.WebLimitDTO;

@Service
@Profile("dev")
public class DevWebLimitServiceImpl extends DevServiceImpl implements WebLimitService {

	@Override
	public WebLimitDTO get() {
		String jsonInput = "{\"id\":43,\"type\":\"DAILY_INDIVIDUAL\",\"limitValue\":500000000,\"usedLimit\":0,\"companyGlobus\":null,\"_links\":{}}";
		return toObject(jsonInput, WebLimitDTO.class);
	}

}
