/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;

/**
 * @author phuhoang
 *
 */
public interface PaymentService {

	Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

	public Integer countBasketPaymentToProcess(String token);

	/**
	 * lay phi khi thuc hien thanh toan/chuyen khoan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public Double getTransactionFee(TransactionFeeInputDTO input, String token);

	/**
	 * ham thuc hien chuyen khoan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input, String token);

	/**
	 * ham thuc hien them giao dich vao gio hang
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input, String token);

	/**
	 * tim kiem danh sach cua user
	 * 
	 * @param token
	 * @return
	 */

	public List<PaymentBasketDTO> getListofBasket(String token, Double amountFrom, Double amountTo, String customerId, String realizationDateFrom,
			String realizationDateTo, String statuses);

	/**
	 * ham xoa don hang trong gio
	 * 
	 * @param basketId
	 * @param token
	 * @return
	 */
	public String deleteBasket(String basketId, String token);

	/**
	 * ham thanh toan gio hang
	 * 
	 * @param transfersIds
	 * @return
	 */
	public PostListBasketDTO postBaskets(List<String> transfersIds, String token);

	/**
	 * ham update item trong gio hang
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input, String token);

	/**
	 * ham lay toan bo danh sach dich vu & nha cung cap
	 * 
	 * @param token
	 * @return
	 */
	public List<ServiceProviderComposDTO> getListOfServiceProvider(String token);

	/**
	 * ham tiem kiem lich su giao dich payment
	 * 
	 * @param startDate
	 * @param toDate
	 * @param cif
	 * @param token
	 * @return
	 */
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(Date startDate, Date toDate, String token);

	/**
	 * Kiem tra va tra ve list Bill (da luu truoc day), neu truy van co DU NO cho ky
	 * nay thi hien thi so tien du no
	 *
	 * - Doi voi cac bill khong co du no thi van hien thi nhung de status: chua co
	 * du no trong ky nay
	 *
	 * @param serviceCode
	 * @return
	 */
	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration, String token);

	/**
	 * ham lay bill can thanh toan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public BillPaymentServiceDTO getBill(GetBillInputDTO input, String token);

	/**
	 * lay providerCodes cau hinh theo EVN Load quy dinh nhan dien ma ENV. Vi du -
	 * Bat dau bang PB.... --> ProviderCode: 004 - EVN Mien Nam - Bat dau bang
	 * PD.... --> ProviderCode: 005 - EVN HaNoi - Bat dau bang PA.... -->
	 * ProviderCode: xxx - EVN Mien Bac
	 * 
	 * @return
	 */
	public List<EVNParameter> loadEVNParameter(String token);
	
	/**
	 * Ham lay danh sach giao dich tuong lai
	 * 
	 * @param token
	 * @param input
	 * @return
	 */
	public List<PaymentsInFutureDTO> getPaymentsInFuture(String token, PaymentsInFutureInputDTO input);

	/**
	 * chi tiet giao dich tuong lai
	 * @param token
	 * @param idFuturePayment
	 * @return
	 */
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String token, String idFuturePayment);

}
