/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.BillService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.FrequencyDTO;
import com.ocb.oma.oomni.dto.StandingOrderDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevBillServiceImpl implements BillService {

	@Autowired
	private ObjectMapper objectMapper;

	public PaymentsInFutureDTO getPaymentDetails(String token, String paymentId) {
		PaymentsInFutureDTO paymentsInFutureDTO = new PaymentsInFutureDTO();
		String jsonInput = "{\"id\":\"d725458f-d475-4a0c-8fd9-b895346869ac@waiting\",\"accountNo\":\"0037100003605005\",\"accountCurrency\":null,\"accountId\":\"43\",\"accountName\":\"ACCOUNT801004\",\"recipientName\":[\"Huynh Khac Nhan\"],\"recipientAddress\":null,\"senderName\":null,\"senderAddress\":null,\"recipientAccountNo\":\"0100100007826003\",\"recipientAccountId\":null,\"amount\":1000000,\"currency\":\"VND\",\"title\":null,\"transferType\":\"INTERNAL\",\"paymentType\":\"INTERNAL_PAYMENT\",\"status\":null,\"realizationDate\":null,\"registrationDate\":null,\"paymentDetails\":{\"bankDetails\":null,\"fee\":\"0\",\"feeAccount\":\"0037100003605005\",\"creditAccountBankCode\":\"\",\"creditAccountBankBranchCode\":null,\"creditAccountProvinceCode\":null},\"cyclicDefinition\":null,\"isCyclic\":false,\"deliveryDate\":null,\"charges\":null,\"lastRealizationDesc\":null,\"realizationDateShiftReason\":null,\"templateId\":null,\"saveTemplate\":null,\"templateName\":null,\"owner\":\"GB NAME 1-801004\",\"operationStatus\":\"IN_PROCESSING\",\"transactionId\":null,\"originalCustomerId\":\"\",\"originalCustomerName\":null,\"transactionTypeDesc\":\"Chuyển khoản trong OCB\",\"description\":[\"Huynh Khac Nhan OCB\"],\"eWalletPhoneNumber\":null,\"addToBasket\":false,\"originalCustomerVisible\":false,\"_links\":{\"get_edit_zus_from_basket_additional_info\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting/get/get_edit_zus_from_basket_additional_info.json\"},\"self\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting\"},\"realize\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/payments/d725458f-d475-4a0c-8fd9-b895346869ac@waiting/actions/realize.json\"}}}";
		try {
			paymentsInFutureDTO = objectMapper.readValue(jsonInput, PaymentsInFutureDTO.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paymentsInFutureDTO;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String token, Date dateFrom, Date dateTo) {
		String data = "{\"pageNumber\":1,\"pageSize\":15,\"totalPages\":1,\"numberOfElements\":15,\"totalElements\":15,\"firstPage\":true,\"lastPage\":true,\"content\":[{\"orderNumber\":\"2888\",\"orderName\":\"Ðiện – Power bill-Điện lực TPHCM – HCM EVN\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PE01000098417\",\"firstExecutionDate\":1524502800000,\"nextExecutionDate\":1545584400000,\"serviceCode\":\"POWE\",\"serviceCodeName\":\"Điện lực TPHCM – HCM EVN\",\"serviceProviderCode\":\"EVNHCM\",\"serviceProviderCodeName\":\"Ðiện – Power bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":\"10000000\",\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4054\",\"orderName\":\"Ðiện – Power bill-Điện lực các tỉnh khác – EVN other provinces\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PD04010070772\",\"firstExecutionDate\":1542301200000,\"nextExecutionDate\":1544893200000,\"serviceCode\":\"POWE\",\"serviceCodeName\":\"Điện lực các tỉnh khác – EVN other provinces\",\"serviceProviderCode\":\"VNPAYEVN\",\"serviceProviderCodeName\":\"Ðiện – Power bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":500000},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542301200000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3994\",\"orderName\":\"Di động trả sau – Postpaid mobile bill-Viettel-Viettel\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"0985359522\",\"firstExecutionDate\":1541005200000,\"nextExecutionDate\":1543597200000,\"serviceCode\":\"PPMB\",\"serviceCodeName\":\"Viettel-Viettel\",\"serviceProviderCode\":\"VIETTEL\",\"serviceProviderCodeName\":\"Di động trả sau – Postpaid mobile bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":10000000},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1556125200000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3168\",\"orderName\":\"Di động trả sau – Postpaid mobile bill-Viettel-Viettel\",\"fromAccount\":\"0100100006533002\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PE126231212\",\"firstExecutionDate\":1525712400000,\"nextExecutionDate\":1544202000000,\"serviceCode\":\"PPMB\",\"serviceCodeName\":\"Viettel-Viettel\",\"serviceProviderCode\":\"VIETTEL\",\"serviceProviderCodeName\":\"Di động trả sau – Postpaid mobile bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":100000},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"1528\",\"orderName\":\"Di động trả sau – Postpaid mobile bill-Viettel-Viettel\",\"fromAccount\":\"0100100007825007\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PE1234\",\"firstExecutionDate\":1483203600000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"PPMB\",\"serviceCodeName\":\"Viettel-Viettel\",\"serviceProviderCode\":\"VIETTEL\",\"serviceProviderCodeName\":\"Di động trả sau – Postpaid mobile bill\",\"paymentSetting\":\"FULL\",\"amountLimit\":{\"value\":null},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3164\",\"orderName\":\"Di động trả sau – Postpaid mobile bill-Viettel-Viettel\",\"fromAccount\":\"0100100006533002\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PE126231211\",\"firstExecutionDate\":1525712400000,\"nextExecutionDate\":1544202000000,\"serviceCode\":\"PPMB\",\"serviceCodeName\":\"Viettel-Viettel\",\"serviceProviderCode\":\"VIETTEL\",\"serviceProviderCodeName\":\"Di động trả sau – Postpaid mobile bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":100000},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4094\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"1111212313\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4084\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"11112123\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4074\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"1111111\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4096\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"11112123131\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4092\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"111121231\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"4082\",\"orderName\":\"Vé máy bay – tàu bay-Tân Cảng Sài Gòn (SNP)\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"1111112\",\"firstExecutionDate\":1542733200000,\"nextExecutionDate\":1545325200000,\"serviceCode\":\"ATIC\",\"serviceCodeName\":\"Tân Cảng Sài Gòn (SNP)\",\"serviceProviderCode\":\"SNP\",\"serviceProviderCodeName\":\"Vé máy bay – tàu bay\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":1111111},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1542992400000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3013\",\"orderName\":\"Ðiện – Power bill-Điện lực TPHCM – HCM EVN\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"zzz\",\"firstExecutionDate\":1525021200000,\"nextExecutionDate\":1543510800000,\"serviceCode\":\"POWE\",\"serviceCodeName\":\"Điện lực TPHCM – HCM EVN\",\"serviceProviderCode\":\"EVNHCM\",\"serviceProviderCodeName\":\"Ðiện – Power bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":7575757},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3008\",\"orderName\":\"Ðiện – Power bill-Điện lực TPHCM – HCM EVN\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"xxx\",\"firstExecutionDate\":1525021200000,\"nextExecutionDate\":1543510800000,\"serviceCode\":\"POWE\",\"serviceCodeName\":\"Điện lực TPHCM – HCM EVN\",\"serviceProviderCode\":\"EVNHCM\",\"serviceProviderCodeName\":\"Ðiện – Power bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":4444444},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1528563600000,\"owner\":\"NAME801004\",\"_links\":{}},{\"orderNumber\":\"3716\",\"orderName\":\"Ðiện – Power bill-Điện lực TPHCM – HCM EVN\",\"fromAccount\":\"0037100003605005\",\"globusId\":null,\"frequencyPeriodCount\":1,\"frequencyPeriodUnit\":\"M\",\"customerId\":\"PE01000060922\",\"firstExecutionDate\":1533747600000,\"nextExecutionDate\":1544288400000,\"serviceCode\":\"POWE\",\"serviceCodeName\":\"Điện lực TPHCM – HCM EVN\",\"serviceProviderCode\":\"EVNHCM\",\"serviceProviderCodeName\":\"Ðiện – Power bill\",\"paymentSetting\":\"LIMITED\",\"amountLimit\":{\"value\":500000},\"recurringPeriod\":\"LIMITED\",\"finishDate\":1532970000000,\"owner\":\"NAME801004\",\"_links\":{}}],\"sortOrder\":null,\"sortDirection\":null,\"_links\":{\"selected\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_auto_bill?pageSize=15\"},\"self\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_auto_bill?pageSize=15&pageNumber=1\"},\"foreign_transfer_type\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/foreign_transfer_type.json\"},\"transfer_batch_by_id\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/transfer_batch_by_id.json\"},\"transfer_batch_types\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_batch_types.json\"},\"transfer_accounts\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_accounts.json\"},\"transfer_bill_history\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_bill_history.json\"},\"transfer_cost\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/transfer_cost.json\"},\"batch_template_excel\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/batch_template_excel.json\"},\"tuition_fee\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/tuition_fee.json\"},\"remove_suggested_bill\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/remove_suggested_bill.json\"},\"suggested_bills\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/suggested_bills.json\"},\"cb_transaction\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/cb_transaction.json\"},\"semesters\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/semesters.json\"},\"transfer\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer\"},\"universities\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/universities.json\"},\"transfer_bill\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/transfer_bill.json\"},\"transfer_auto_bill\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_auto_bill.json\"},\"beneficiary_bills\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/beneficiary_bills.json\"},\"transfer_service_providers\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/search/transfer_service_providers.json\"},\"current_limit\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/current_limit.json\"},\"validate_checker\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/get/validate_checker.json\"},\"modify_auto_bill_transfer\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/actions/modify_auto_bill_transfer.json\"},\"create_auto_bill_transfer\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/actions/create_auto_bill_transfer.json\"},\"delete_auto_bill_transfer\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/actions/delete_auto_bill_transfer.json\"},\"create_auto_bill_transfer_directly\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/transfer/actions/create_auto_bill_transfer_directly.json\"}}}";
		List<TransferAutoBillOutputDTO> dtos = new ArrayList<>();
		TransferAutoBillOutputDTO dto= new TransferAutoBillOutputDTO();
		dto.setCustomerId("customerId");
		dtos.add(dto);
		return dtos;
	}

	@Override
	public Boolean createAutoBillTransferDirectly(String token,
			CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.BillService#deleteAutoBillTransfer(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String token, String string) {
		DeleteAutoBillTransferOutputDTO dto = new DeleteAutoBillTransferOutputDTO();
//		"status": "INTERNAL_ERROR",
//	    "authErrorCause": "PROCESSING_ERROR",
//	    "referenceId": null,
//	    "coreRefNum2": null,
//	    "errorMsg": "",
//	    "success": false,
		dto.setStatus("EXECUTED");
		dto.setAuthErrorCause(null);
		dto.setReferenceId("4224");
		dto.setCoreRefNum2(null);
		dto.setErrorMsg(null);
		dto.setSuccess(true);
		return dto;
	}

	@Override
	public OutputBaseDTO modifyAutoBillTransferDirectly(String token, TransferAutoBillDTO input) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (input.getOrderNumber().equals("301")) {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (input.getOrderNumber().equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		return dto;
	}

	@Override
	public List<StandingOrderOutputDTO> standingOrderList(String token) {
		List<StandingOrderOutputDTO> lst = new ArrayList<>();
		StandingOrderOutputDTO dto = new StandingOrderOutputDTO();
		dto.setBankName(DevGeneratedDataUtil.generatedBankName());
		StandingOrderDTO standingOrder = new StandingOrderDTO();
		standingOrder.setProductOwner(801003D);
		standingOrder.setId(null);
		standingOrder.setShortName("Orange");
		standingOrder.setOrderingCustomer(null);
		standingOrder.setDebitAccount("0100100003486003");
		String[] beneficiary = { "SHORTNAME-910802" };
		standingOrder.setBeneficiary(beneficiary);
		standingOrder.setCreditAccount("0001100001761003");
		standingOrder.setCreditAccountBankName(null);
		standingOrder.setAmount(50000D);
		standingOrder.setAmountCalculation(null);
		standingOrder.setCurrency("VND");
		String[] remarks = { "Desc" };
		standingOrder.setRemarks(remarks);
		standingOrder.setClearingNetwork(null);
		standingOrder.setStartDate(new Date(1543510800000L));
		standingOrder.setEndDate(new Date(1553878800000L));
		FrequencyDTO frequency = new FrequencyDTO();
		frequency.setNextDate(new Date(1543510800000L));
		frequency.setPeriodUnit("M");
		frequency.setPeriodCount(1);
		frequency.setNextDate(null);
		standingOrder.setFrequency(frequency);
		standingOrder.setActiveFlag(false);
		standingOrder.setStandingOrderReferenceId("0100100003486003.2");
		standingOrder.setAlreadyDeleted(false);
		dto.setStandingOrder(standingOrder);
		lst.add(dto);

		StandingOrderOutputDTO dto1 = new StandingOrderOutputDTO();
		dto1.setBankName(DevGeneratedDataUtil.generatedBankName());
		StandingOrderDTO standingOrder1 = new StandingOrderDTO();
		standingOrder1.setProductOwner(801003D);
		standingOrder1.setId(null);
		standingOrder1.setShortName("801003");
		standingOrder1.setOrderingCustomer(null);
		standingOrder1.setDebitAccount("0100100006076008");
		standingOrder1.setDebitAccountName("ACCOUNT801003");
		String[] beneficiary1 = { "SHORTNAME-801003" };
		standingOrder1.setBeneficiary(beneficiary1);
		standingOrder1.setCreditAccount("0001600004224001");
		standingOrder1.setCreditAccountBankName(null);
		standingOrder1.setAmount(5000000D);
		standingOrder1.setAmountCalculation(null);
		standingOrder1.setCurrency("VND");
		String[] remarks1 = {};
		standingOrder1.setRemarks(remarks1);
		standingOrder1.setClearingNetwork(null);
		standingOrder1.setStartDate(new Date(1546275600000L));
		standingOrder1.setEndDate(new Date(1553878800000L));
		FrequencyDTO frequency1 = new FrequencyDTO();
		frequency1.setNextDate(new Date(1543510800000L));
		frequency1.setPeriodUnit("M");
		frequency1.setPeriodCount(1);
		frequency1.setNextDate(null);
		standingOrder1.setFrequency(frequency1);
		standingOrder1.setActiveFlag(false);
		standingOrder1.setStandingOrderReferenceId("0100100003486003.3");
		standingOrder1.setAlreadyDeleted(false);
		dto1.setStandingOrder(standingOrder1);
		lst.add(dto1);

		return lst;
	}

	@Override
	public OutputBaseDTO removeStandingOrder(String token, String standingOrderId) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (standingOrderId.equals("0")) {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (standingOrderId.equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		return dto;
	}

	@Override
	public OutputBaseDTO createStandingOrder(String token, StandingOrderInputDTO input) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (input.getAmount() == 0D) {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (input.getAmount() < 50000D) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		return dto;
	}

	@Override
	public OutputBaseDTO modifyStandingOrderDirectly(String token, StandingOrderInputDTO input) {
		OutputBaseDTO dto = new OutputBaseDTO();
		if (input.getAmount() == 0D) {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (input.getAmount() < 50000D) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		return dto;
	}

}
