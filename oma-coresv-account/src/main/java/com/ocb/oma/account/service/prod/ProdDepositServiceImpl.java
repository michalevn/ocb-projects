/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.DepositService;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.AccountCapitalDTO;
import com.ocb.oma.oomni.dto.AccountInterestDTO;
import com.ocb.oma.oomni.dto.AccountSourceDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdDepositServiceImpl extends ProdServiceImpl implements DepositService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	private static final Log LOG = LogFactory.getLog(ProdDepositServiceImpl.class);

	@Override
	public OutputBaseDTO breakDeposit(String depositId, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit/actions/break_deposit_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map params = new HashMap();
			params.put("depositId", depositId);
			HttpEntity<Map> entity = new HttpEntity<>(params, headers);
			return restTemplate.postForObject(url, entity, OutputBaseDTO.class);

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("break_deposit_directly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.DepositService#getDeposits(java.lang.String)
	 */
	@Override
	public List<DepositDTO> getDeposits(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<DepositDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<DepositDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("break_deposit_directly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<DepositProductDTO> getDepositProducts(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit/search/deposit_products?productListName=DEPOSIT_OPEN_NORMAL_DEPOSIT";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<DepositProductDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<DepositProductDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getDepositProducts has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<DepositInterestRateDTO> getDepositInterestRates(String currency, String subProductCode, String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit_interest_rates?currency=" + currency + "&subProductCode=" + subProductCode;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<DepositInterestRateDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<DepositInterestRateDTO>>() {
					});

			return lst;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("getDepositProducts has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public CreateDepositOutputDTO createDeposit(String token, CreateDepositDTO deposit) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		String path = "/api/deposit_order/actions/create_deposit_directly";
		String url = host + path;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<CreateDepositDTO> request = new RequestEntity<>(deposit, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<CreateDepositOutputDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<CreateDepositOutputDTO>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("restrictCard has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public DepositOfferAccountsDTO getDepositOfferAcc(String token) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit_offer/get/deposit_offer_accounts";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			DepositOfferAccountsDTO accountsDTO = new DepositOfferAccountsDTO();
			List<AccountCapitalDTO> accountCapitalList = new ArrayList<>();
			accountCapitalList = objectMapper.readValue(
					objectMapper.writeValueAsString(response.get("accountInterestList")),
					new TypeReference<List<AccountCapitalDTO>>() {
					});

			accountsDTO.setAccountCapitalList(accountCapitalList);
			List<AccountInterestDTO> accountInterestList = new ArrayList<>();
			accountInterestList = objectMapper.readValue(
					objectMapper.writeValueAsString(response.get("accountCapitalList")),
					new TypeReference<List<AccountInterestDTO>>() {
					});
			accountsDTO.setAccountInterestList(accountInterestList);

			List<AccountSourceDTO> accountSourceList = new ArrayList<>();
			accountSourceList = objectMapper.readValue(
					objectMapper.writeValueAsString(response.get("accountSourceList")),
					new TypeReference<List<AccountSourceDTO>>() {
					});
			accountsDTO.setAccountSourceList(accountSourceList);

			return accountsDTO;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("break_deposit_directly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public DepositDTO getDeposit(String token, String depositId) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit/" + depositId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url.trim()));
			ResponseEntity<DepositDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<DepositDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("break_deposit_directly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	public DepositDTO getDeposit_x(String token, String depositId) {
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String url = host + "/api/deposit/" + depositId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Map> responseEntity = restTemplate.exchange(request, new ParameterizedTypeReference<Map>() {
			});
			Map rs = responseEntity.getBody();
			System.out.println(rs);
			return new DepositDTO();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("break_deposit_directly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	

}
