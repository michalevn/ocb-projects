/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.CampaignService;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CampainSlot;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevCampaignServiceImpl implements CampaignService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.CampaignService#getCampaignList(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<CampaignInfo> getCampaignList(String customerCif, String username, String token) {
		// TODO Auto-generated method stub
		if (customerCif == null || token == null || username == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<CampaignInfo> lst = new ArrayList<>();
		CampaignInfo dto = new CampaignInfo();
		dto.setActivated(true);
		dto.setBannerText("banner Text ");
		dto.setDescription(DevGeneratedDataUtil.generateDescription());
		dto.setFromDate(new Date(2018, 02, 02));
		dto.setToDate(new Date(2019, 01, 02));
		dto.setLink("https://www.desicomments.com/wp-content/uploads/2017/01/Today-Is-A-Good-Day.jpg");
		dto.setPriority(1);
		List<CampainSlot> slots = new ArrayList<>();
		// 1 slot
		CampainSlot slot = new CampainSlot();
		slot.setImageLink("https://www.iloveshayari.in/wp-content/uploads/2018/01/good-morning-wallpapers-hd-1024x550.jpg");
		slot.setMiniApplication("Applicaton A");
		slot.setSlotName(DevGeneratedDataUtil.generateNameOfUser());
		slots.add(slot);

		dto.setSlots(slots);

		// campaign 1
		lst.add(dto);
		// campaign 2

		dto = new CampaignInfo();
		dto.setActivated(true);
		dto.setBannerText("banner Text 2");
		dto.setDescription(DevGeneratedDataUtil.generateDescription());
		dto.setFromDate(new Date(2018, 02, 02));
		dto.setToDate(new Date(2019, 01, 02));
		dto.setLink("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzddpeMlEahJ6jG7_3EUa7z37ofkdFWgpdsQHLlPVPZm3wdWvC");
		dto.setPriority(2);
		lst.add(dto);
		return lst;
	}

}
