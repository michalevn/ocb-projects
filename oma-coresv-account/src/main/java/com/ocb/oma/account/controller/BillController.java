package com.ocb.oma.account.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.BillService;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/bill")
public class BillController extends BaseController {

	private static final Log LOG = LogFactory.getLog(BillController.class);

	@Autowired
	private BillService billService;

	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value = "/getTransferAutoBills")
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestParam("dateFrom") String dateFrom, @RequestParam("dateTo") String dateTo) {
		LOG.info("getTransferAutoBills");
		List<TransferAutoBillOutputDTO> rs = billService.getTransferAutoBills(token,
				objectMapper.convertValue(dateFrom, Date.class), objectMapper.convertValue(dateTo, Date.class));
		return rs;
	}

	@RequestMapping(value = "/createAutoBillTransferDirectly")
	public Boolean createAutoBillTransferDirectly(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {
		LOG.info("createAutoBillTransferDirectly");
		Boolean rs = billService.createAutoBillTransferDirectly(token, createAutoBillTransferDirectlyDTO);
		return rs;
	}

	@RequestMapping(value = "/deleteAutoBillTransfer")
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(
			@RequestHeader(HttpHeaders.AUTHORIZATION) String token, @RequestBody Map<String, String> input) {
		LOG.info("deleteAutoBillTransfer");

		return billService.deleteAutoBillTransfer(token, input.get("orderNumber"));
	}

	@RequestMapping(value = "/modifyAutoBillTransferDirectly")
	public OutputBaseDTO modifyAutoBillTransferDirectly(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody TransferAutoBillDTO input) {
		LOG.info("modifyAutoBillTransferDirectly");

		return billService.modifyAutoBillTransferDirectly(token, input);
	}

	@RequestMapping(value = "/standingOrderList")
	public List<StandingOrderOutputDTO> standingOrderList(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
		LOG.info("standingOrderList");

		return billService.standingOrderList(token);
	}

	@RequestMapping(value = "/createStandingOrder")
	public OutputBaseDTO createStandingOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody StandingOrderInputDTO input) {
		LOG.info("createStandingOrder");
		return billService.createStandingOrder(token, input);
	}

	@RequestMapping(value = "/removeStandingOrder")
	public OutputBaseDTO removeStandingOrder(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody Map<String, String> input) {
		LOG.info("removeStandingOrder");
		String standingOrderId = input.get("standingOrderId");
		return billService.removeStandingOrder(token, standingOrderId);
	}
	
	@RequestMapping(value = "/modifyStandingOrderDirectly")
	public OutputBaseDTO modifyStandingOrderDirectly(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody StandingOrderInputDTO input) {
		LOG.info("modifyStandingOrderDirectly");
		return billService.modifyStandingOrderDirectly(token, input);
	}

}
