package com.ocb.oma.account.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.CreditService;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/credit")
public class CreditController extends BaseController {

	@Autowired
	private CreditService creditService;

	@RequestMapping(value = "/getCredits")
	public List<CreditDTO> getCredits() {
		return creditService.getCredits();
	}

	@RequestMapping(value = "/getInstalmentTransactionHistory")
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(
			@RequestParam("contractNumber") String contractNumber, @RequestParam("dateFrom") String dateFrom,
			@RequestParam("dateTo") String dateTo,
			@RequestParam(name = "operationAmountFrom", required = false) BigDecimal operationAmountFrom,
			@RequestParam(name = "operationAmountTo", required = false) BigDecimal operationAmountTo,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize) {
		return creditService.getInstalmentTransactionHistory(contractNumber,
				objectMapper.convertValue(dateFrom, Date.class), objectMapper.convertValue(dateTo, Date.class),
				operationAmountFrom, operationAmountTo, pageNumber, pageSize);
	}

}
