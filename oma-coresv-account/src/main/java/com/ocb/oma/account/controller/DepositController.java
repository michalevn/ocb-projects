package com.ocb.oma.account.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.DepositService;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * 
 * @author thaoctm
 *
 */
@RestController
@RequestMapping("/deposit")
public class DepositController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(DepositController.class);

	@Autowired
	private DepositService depositService;

	@RequestMapping(value = "/breakDeposit")
	public OutputBaseDTO breakDeposit(HttpServletRequest request, @RequestParam("depositId") String depositId) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return depositService.breakDeposit(depositId, token);
	}

	@RequestMapping(value = "/getDeposits")
	public List<DepositDTO> getDeposits(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return depositService.getDeposits(token);
	}

	@RequestMapping(value = "/getDeposit")
	public DepositDTO getDeposit(@RequestParam("depositId") String depositId,
			@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
		return depositService.getDeposit(token, depositId);
	}

	@RequestMapping(value = "/depositProducts")
	public List<DepositProductDTO> getDepositProducts(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
		return depositService.getDepositProducts(token);
	}

	@RequestMapping(value = "/depositInterestRates")
	public List<DepositInterestRateDTO> getDepositInterestRates(HttpServletRequest request,
			@RequestParam("currency") String currency, @RequestParam("subProductCode") String subProductCode) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return depositService.getDepositInterestRates(currency, subProductCode, token);
	}

	@RequestMapping(value = "/createDeposit")
	public CreateDepositOutputDTO createDeposit(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody CreateDepositDTO deposit) {
		return depositService.createDeposit(token, deposit);
	}

	@RequestMapping(value = "/getDepositOfferAcc")
	public DepositOfferAccountsDTO getDepositOfferAcc(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return depositService.getDepositOfferAcc(token);
	}

	
}
