/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.RecipientService;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RecipientType;
import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevRecipientServiceImpl implements RecipientService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.RecipientService#getRecipientList(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public List<RecipientDTO> getRecipientList(String token, String filerTemplateType, int pageSize) {
		// TODO Auto-generated method stub
		List<RecipientDTO> lst = new ArrayList<>();
		String recipientId;
		String recipientName;
		String accountNo;
		String beneficiaryAccountNo;
		String bankName;
		String bankCode;
		String paymentType;
		String provinceId;
		String branchCode;
		String cardNumber;
		String bankCodeMa;
		for (int i = 0; i < 10; i++) {
			recipientId = "" + i + 10;
			recipientName = DevGeneratedDataUtil.generateNameOfUser();
			accountNo = DevGeneratedDataUtil.generateAccountNo();
			beneficiaryAccountNo = DevGeneratedDataUtil.generateAccountNo();
			bankName = DevGeneratedDataUtil.generatedBankName();
			bankCode = DevGeneratedDataUtil.generateBankCode();
			paymentType = DevGeneratedDataUtil.generatePaymentType();
			provinceId = DevGeneratedDataUtil.generateProvinceCodes();
			branchCode = DevGeneratedDataUtil.generateBankCode();
			cardNumber = DevGeneratedDataUtil.generateCardNumber();
			bankCodeMa = DevGeneratedDataUtil.generatebankMACodes();
			if (paymentType.equals(PaymentType.InternalPayment.getPaymentTypeCode())) {

				cardNumber = null;
				branchCode = null;
				provinceId = null;
				bankCode = null;
				bankName = null;

			}
			if (paymentType.equals(PaymentType.LocalPayment.getPaymentTypeCode())) {
				cardNumber = null;
			}
			if (paymentType.equals(PaymentType.FastTransfer.getPaymentTypeCode())) {

				// cardNumber = null;
				branchCode = null;
				provinceId = null;
				bankCode = null;
				bankName = null;

			}
			RecipientDTO dto = new RecipientDTO(recipientId, recipientName, accountNo, bankName, bankCode, paymentType,
					provinceId, branchCode);
			dto.setRemitterAccountNo(DevGeneratedDataUtil.generateAccountNo());
			dto.setCardNumber(cardNumber);
			dto.setBeneficiaryAccountNo(beneficiaryAccountNo);
			dto.setBankCodeMA(bankCodeMa);
			lst.add(dto);
		}
		return lst;
	}

	@Override
	public Boolean deleteRecipient(String token, RecipientDTO recipient) {

		if (recipient.getRecipientId().toString().equals("6")) {
			throw new OmaException(MessageConstant.RECIPIENT_NOT_FOUND);
		}
		if (recipient.getRecipientId().toString().equals("1000")) {
			throw new OmaException(MessageConstant.CAN_NOT_DELETE_RECIPIENT_OF_OTHER_USER);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.RecipientService#addRecipient(java.lang.String,
	 * com.ocb.oma.dto.RecipientInputDTO)
	 */
	@Override
	public AddRecipientOutputDTO addRecipient(String token, RecipientInputDTO input) {
		AddRecipientOutputDTO output = new AddRecipientOutputDTO();
		List<String> lstPaymentType = new ArrayList<>();
		lstPaymentType.add(RecipientType.EXTERNAL.getRecipientTypeCode());
		lstPaymentType.add(RecipientType.INTERNAL.getRecipientTypeCode());
		lstPaymentType.add(RecipientType.FAST.getRecipientTypeCode());
		if (lstPaymentType.contains(input.getRecipientType()) && input.getBankCode().equals("OCB")) {

			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (input.getBankCode().equals("ACB")) {
			output.setResultCode("1000");
			output.setResultMsg("Invalid debit account");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}
		return output;
	}

}
