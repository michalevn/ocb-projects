package com.ocb.oma.account.service;

import com.ocb.oma.dto.WebLimitDTO;

public interface WebLimitService {

	WebLimitDTO get();

}
