/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.CreditService;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("dev")
public class DevCreditServiceImpl extends DevServiceImpl implements CreditService {

	@Override
	public List<CreditDTO> getCredits() {
		String jsonInput = "[{\"creditId\":\"LD1630900143\",\"creditType\":\"INSTALLMENT\",\"accountNo\":\"0100100003486003\",\"currency\":\"VND\",\"creditAmount\":617498000,\"creditName\":\"Cho vay co TSBD CBNV OCB\",\"maturedCapitalAmount\":null,\"unmaturedCapitalAmount\":null,\"nextInstallmentAmount\":null,\"nextInstallmentDate\":null,\"overpaymentAmount\":null,\"overpaymentCurrency\":null,\"outstandingLiabilitiesAmount\":5417000,\"outstandingLiabilitiesCurrency\":null,\"interestRate\":8.21,\"openDate\":null,\"closeDate\":null,\"ownersList\":[{\"customerId\":null,\"relationType\":\"OWNER\",\"name\":null,\"addressStreetPrefix\":null,\"addressStreet\":null,\"houseNo\":null,\"apartmentNo\":null,\"postCode\":null,\"town\":null,\"country\":null,\"fullName\":\"GB NAME 1-801003\",\"residence\":null}],\"lastTransactionBookingDate\":null,\"actions\":[\"HISTORIC_SCHEDULE\",\"HISTORY\"],\"creditCategories\":[\"BASIC_LOAN_LIST\",\"PERSONAL_LOAN_LIST\",\"CONSUMPTION_LOAN_LIST\"],\"flexAgreementNo\":\"\",\"flexProductId\":\"\",\"principalAmount\":617498000,\"contractDate\":1478192400000,\"disbursedTranchesAmount\":650000000,\"totalAmountDue\":null,\"totalPrincipialDue\":37919000,\"totalInterest\":4224715,\"totalPenaltyInterestDue\":0,\"nextRepaymentAmount\":5417000,\"nextRepaymentDate\":1550595600000,\"repaymentAccount\":\"0100100003486003\",\"efficientRepaymentDate\":1478192400000,\"valueDate\":1478192400000,\"finalDueDate\":1793725200000,\"wholeRepaymentBalance\":9641715,\"outstandingInterest\":4224715,\"totalInterestDue\":4224715,\"totalInstallment\":0,\"paidInstallments\":0,\"initialBalance\":650000000,\"totalPaidPrincipals\":32502000,\"totalPaidInterests\":0,\"_links\":{}},{\"creditId\":\"LD1601300115\",\"creditType\":\"INSTALLMENT\",\"accountNo\":\"0100100003486003\",\"currency\":\"VND\",\"creditAmount\":36935000,\"creditName\":\"Cho vay tin chap NV OCB\",\"maturedCapitalAmount\":null,\"unmaturedCapitalAmount\":null,\"nextInstallmentAmount\":null,\"nextInstallmentDate\":null,\"overpaymentAmount\":null,\"overpaymentCurrency\":null,\"outstandingLiabilitiesAmount\":1945000,\"outstandingLiabilitiesCurrency\":null,\"interestRate\":8.01,\"openDate\":null,\"closeDate\":null,\"ownersList\":[{\"customerId\":null,\"relationType\":\"OWNER\",\"name\":null,\"addressStreetPrefix\":null,\"addressStreet\":null,\"houseNo\":null,\"apartmentNo\":null,\"postCode\":null,\"town\":null,\"country\":null,\"fullName\":\"GB NAME 1-801003\",\"residence\":null}],\"lastTransactionBookingDate\":null,\"actions\":[\"HISTORIC_SCHEDULE\",\"HISTORY\"],\"creditCategories\":[\"BASIC_LOAN_LIST\",\"PERSONAL_LOAN_LIST\",\"CONSUMPTION_LOAN_LIST\"],\"flexAgreementNo\":\"\",\"flexProductId\":\"\",\"principalAmount\":36935000,\"contractDate\":1452618000000,\"disbursedTranchesAmount\":70000000,\"totalAmountDue\":null,\"totalPrincipialDue\":35010000,\"totalInterest\":246541,\"totalPenaltyInterestDue\":0,\"nextRepaymentAmount\":1945000,\"nextRepaymentDate\":1550595600000,\"repaymentAccount\":\"0100100003486003\",\"efficientRepaymentDate\":1452618000000,\"valueDate\":1452618000000,\"finalDueDate\":1547312400000,\"wholeRepaymentBalance\":2191541,\"outstandingInterest\":246541,\"totalInterestDue\":246541,\"totalInstallment\":0,\"paidInstallments\":0,\"initialBalance\":70000000,\"totalPaidPrincipals\":33065000,\"totalPaidInterests\":0,\"_links\":{}},{\"creditId\":\"LD1601300116\",\"creditType\":\"INSTALLMENT\",\"accountNo\":\"0100100003486003\",\"currency\":\"USD\",\"creditAmount\":36935000,\"creditName\":\"Cho vay tin chap NV OCB\",\"maturedCapitalAmount\":null,\"unmaturedCapitalAmount\":null,\"nextInstallmentAmount\":null,\"nextInstallmentDate\":null,\"overpaymentAmount\":null,\"overpaymentCurrency\":null,\"outstandingLiabilitiesAmount\":1945000,\"outstandingLiabilitiesCurrency\":null,\"interestRate\":8.01,\"openDate\":null,\"closeDate\":null,\"ownersList\":[{\"customerId\":null,\"relationType\":\"OWNER\",\"name\":null,\"addressStreetPrefix\":null,\"addressStreet\":null,\"houseNo\":null,\"apartmentNo\":null,\"postCode\":null,\"town\":null,\"country\":null,\"fullName\":\"GB NAME 1-801003\",\"residence\":null}],\"lastTransactionBookingDate\":null,\"actions\":[\"HISTORIC_SCHEDULE\",\"HISTORY\"],\"creditCategories\":[\"BASIC_LOAN_LIST\",\"PERSONAL_LOAN_LIST\",\"CONSUMPTION_LOAN_LIST\"],\"flexAgreementNo\":\"\",\"flexProductId\":\"\",\"principalAmount\":36935000,\"contractDate\":1452618000000,\"disbursedTranchesAmount\":70000000,\"totalAmountDue\":null,\"totalPrincipialDue\":35010000,\"totalInterest\":246541,\"totalPenaltyInterestDue\":0,\"nextRepaymentAmount\":1945000,\"nextRepaymentDate\":1550595600000,\"repaymentAccount\":\"0100100003486003\",\"efficientRepaymentDate\":1452618000000,\"valueDate\":1452618000000,\"finalDueDate\":1547312400000,\"wholeRepaymentBalance\":2191541,\"outstandingInterest\":246541,\"totalInterestDue\":246541,\"totalInstallment\":0,\"paidInstallments\":0,\"initialBalance\":70000000,\"totalPaidPrincipals\":33065000,\"totalPaidInterests\":0,\"_links\":{}}]";
		return toList(jsonInput, CreditDTO.class);
	}

	@Override
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String contractNumber, Date dateFrom,
			Date dateTo, BigDecimal operationAmountFrom, BigDecimal operationAmountTo, Integer pageNumber,
			Integer pageSize) {
		String jsonInput = "[{\"contractNumber\":\"LD1601300115\",\"currency\":\"VND\",\"type\":\"ABC\",\"typeDesc\":\"string\",\"eventDate\":\"2018-12-05T08:21:42.788Z\",\"principalIncrease\":640042.7417477826,\"valueDate\":\"2018-12-05T08:21:42.788Z\",\"bookingDate\":\"2018-12-05T08:21:42.788Z\",\"agreementFlex\":true,\"principalRepayment\":620023.5270888254,\"interestRepayment\":98958.0611881208,\"paymentAmount\":106843.27935693643,\"penaltyInterest\":796182.0227635548,\"chargeAmount\":701075.8905500651,\"feeAmount\":540264.0976377806,\"commisionAmount\":449343.75327946304,\"interestRate\":42395.65480922098,\"description\":\"xyz\",\"repaymentDate\":\"2018-12-05T08:21:42.788Z\"}]";
		List<InstalmentTransactionHistoryDTO> dtos = toList(jsonInput, InstalmentTransactionHistoryDTO.class);
		for (InstalmentTransactionHistoryDTO dto : dtos) {
			dto.setContractNumber(contractNumber);
		}
		return dtos;
	}
}
