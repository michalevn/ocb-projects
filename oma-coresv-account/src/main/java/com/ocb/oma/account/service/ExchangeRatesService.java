/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.ExchangeRateDTO;

/**
 * @author docv
 *
 */
public interface ExchangeRatesService {

	static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRatesService.class);

	public List<ExchangeRateDTO> getExchangeRates();
}
