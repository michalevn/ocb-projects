package com.ocb.oma.account.config;

import java.io.IOException;
import java.net.ConnectException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.ocb.oma.account.log.LogRefIdHolder;
import com.ocb.oma.account.security.AccessTokenHolder;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;

public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateInterceptor.class);

	@Value("${omni.url}")
	private String omniUrl;

	@Value("${newfo.url}")
	private String newfoUrl;

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		String requestUri = request.getURI().toString();

		boolean isConnectingToOcp = requestUri.startsWith(omniUrl);
		boolean isConnectingToNewFo = requestUri.startsWith(newfoUrl);

		HttpHeaders headers = request.getHeaders();
		headers.set("X-REF-ID", LogRefIdHolder.get());

		LOGGER.info(">>> [{}] {}", request.getMethod().name(), requestUri);

		if (isConnectingToOcp && !headers.containsKey(HttpHeaders.AUTHORIZATION)) {
			String accessToken = AccessTokenHolder.get();
			if (StringUtils.isNotBlank(accessToken)) {
				if (!accessToken.startsWith("Bearer ")) {
					accessToken = "Bearer " + accessToken;
				}
				headers.set(HttpHeaders.AUTHORIZATION, accessToken);
			}
		}

		ClientHttpResponse response = null;
		try {
			response = execution.execute(request, body);
			HttpStatus statusCode = response.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return response;
			} else {
				logError(request, response, body, null);
				String errorMessage = "Error calling API: " + requestUri;
				if (isConnectingToOcp) {
					throw new OmaException(MessageConstant.SERVER_OCP_ERROR, errorMessage);
				} else if (isConnectingToNewFo) {
					throw new OmaException(MessageConstant.SERVER_NEWFO_ERROR, errorMessage);
				} else {
					throw new OmaException(MessageConstant.SERVER_ERROR, errorMessage);
				}
			}
		} catch (ConnectException e) {
			logError(request, response, body, e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		} catch (Throwable e) {
			logError(request, response, body, e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	private void logError(HttpRequest request, ClientHttpResponse response, byte[] body, Throwable e)
			throws IOException {
		LOGGER.error("==================== REST CLIENT ERROR ====================");
		LOGGER.error(">>> Request URL: " + request.getURI().toString());
		LOGGER.error(">>> Request Method: " + request.getMethod().name());
		LOGGER.error(">>> Request Headers: ");
		HttpHeaders headers = request.getHeaders();
		for (Entry<String, List<String>> entry : headers.entrySet()) {
			String headerKey = entry.getKey();
			if (entry.getValue().size() == 1) {
				LOGGER.error(headerKey + " => " + entry.getValue().get(0));
			} else {
				for (String headerValue : entry.getValue()) {
					if (headerKey.equals(HttpHeaders.AUTHORIZATION)) {
						if (headerValue.length() > 5) {
							headerValue = "***" + headerValue.substring(headerValue.length() - 5);
						}
					}
					LOGGER.error("    " + headerValue);
				}
			}
		}
		LOGGER.error(">>> Request Body: \n");
		LOGGER.error(new String(body));
		if (response != null) {
			HttpStatus statusCode = response.getStatusCode();
			LOGGER.error(">>> Response Status: " + statusCode.value() + ", " + statusCode.getReasonPhrase());
			LOGGER.error(">>> Response: \n\n");
			char[] chars = IOUtils.toCharArray(response.getBody(), Charset.forName("UTF-8"));
			LOGGER.error(new String(chars));
			LOGGER.error("\n\n");
		}
	}
}
