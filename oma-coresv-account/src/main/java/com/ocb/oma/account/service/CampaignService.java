/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.CampaignInfo;

/**
 * @author phuhoang
 *
 */
public interface CampaignService {
	Logger LOGGER = LoggerFactory.getLogger(CampaignService.class);

	/**
	 * lay danh sach campaign cua user
	 * @param customerCif
	 * @param username
	 * @param token
	 * @return
	 */
	public List<CampaignInfo> getCampaignList(String customerCif, String username, String token);

}
