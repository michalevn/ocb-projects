/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.AccountService;
import com.ocb.oma.account.service.PaymentService;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AvatarInfoDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.StatusesAvatar;
import com.ocb.oma.dto.StatusesUpdateAvatar;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositSubDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevAccountServiceImpl extends DevServiceImpl implements AccountService {

	@Autowired
	private PaymentService paymentService;

	private AccountLimitDTO accountLimit;

	@Autowired
	private ObjectMapper objectMapper;

	@PostConstruct
	public void init() {
		accountLimit = new AccountLimitDTO(1000000D, 1000000000D, 1000000000D);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountList(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<AccountInfo> getAccountList(String customerCif, String token) {
		// TODO Auto-generated method stub
		if (customerCif == null || token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<AccountInfo> lst = new ArrayList<>();
		AccountInfo acc1 = new AccountInfo();
		acc1.setCurrency("usd");
		acc1.setCurrentBalance(85000000D);
		acc1.setAccountId("1");
		acc1.setAccountName(DevGeneratedDataUtil.generateNameOfUser());
		acc1.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
		acc1.setAccountRestrictFlag(true);
		acc1.setAllowedLimit(2000000D);
		acc1.setAllowedLimitCurrency("vnd");
		acc1.setBlockAssets(100000D);
		acc1.setOpenDate(new Date(2010, 10, 01));
		acc1.setCustomName(DevGeneratedDataUtil.generateNameOfUser());
		acc1.setAccessibleAssets(200000000D);
		lst.add(acc1);
		acc1 = new AccountInfo();
		acc1.setCurrency("vnd");
		acc1.setCurrentBalance(25000000D);
		acc1.setAccountId("2");
		acc1.setAccountName(DevGeneratedDataUtil.generateNameOfUser());
		acc1.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
		acc1.setAccountRestrictFlag(true);
		acc1.setAllowedLimit(1000000D);
		acc1.setAllowedLimitCurrency("vnd");
		acc1.setBlockAssets(200000D);
		acc1.setOpenDate(new Date(2010, 10, 01));
		acc1.setCustomName(DevGeneratedDataUtil.generateNameOfUser());
		acc1.setAccessibleAssets(85000000D);
		lst.add(acc1);
		return lst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#getCustomerDetail(java.lang
	 * .String)
	 */
	@Override
	public CustomerInfoDTO getCustomerDetail(String token) {
		// TODO Auto-generated method stub
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		CustomerInfoDTO dto = new CustomerInfoDTO("INDIVIDUAL", "GOLD", OtpAuthorizeMethod.SMSOTP.getCode(),
				"0972638123");
		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.AccountService#updateAccountIcon(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountIcon(String accountNo, String customerCif, String token, String imageLink) {
		// TODO Auto-generated method stub

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.AccountService#updateAccountName(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountName(String accountNo, String customerCif, String token, String updatedName) {
		// TODO Auto-generated method stub
		if (updatedName.equals("wrong"))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#findTransactionHistory(java.lang.
	 * String, java.lang.String, java.util.Date, java.util.Date, java.lang.Double,
	 * java.lang.Double, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<TransactionHistoryDTO> findTransactionHistory(String token, String accountNo, String transactionType,
			Date fromDate, Date toDate, Double fromAmount, Double toAmount, Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		List<TransactionHistoryDTO> result = new ArrayList<>();
		List<String> transactionTypes = Arrays.asList("RutTienMat", "ChuyenKhoanTrong", "ChuyenKhoanNgoai",
				"ChuyenKhoanNoiBo", "ChuyenKhoan24/7");
		List<String> currencies = Arrays.asList("usd", "vnd", "hk");
		List<String> transactionTypeDescList = Arrays.asList("Rút tiền mặt ", "Chuyển khoản trong hệ thống ",
				"Chuyển khoản ngoài hệ thống ", "Chuyển khoản nội bộ ", "Chuyển khoản 24/7");
		for (int i = 0; i < pageSize; i++) {
			double amount = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
			int transIndex = ThreadLocalRandom.current().nextInt(transactionTypes.size() - 1);
			int currencyIndex = ThreadLocalRandom.current().nextInt(2);
			String transactionId = RandomStringUtils.randomAlphabetic(10);
			String transactionNote = DevGeneratedDataUtil.generateDescription();
			Date transactionDate = new Date(
					ThreadLocalRandom.current().nextLong(fromDate.getTime() - 1000, toDate.getTime()));
			String receiverName = DevGeneratedDataUtil.generateNameOfUser();
			String receiverAccount = DevGeneratedDataUtil.generateAccountNo();
			TransactionHistoryDTO dto = new TransactionHistoryDTO(accountNo, amount, currencies.get(currencyIndex),
					transactionId, transactionTypes.get(transIndex), transactionNote,
					transactionTypeDescList.get(transIndex), transactionDate, receiverName, receiverAccount);
			result.add(dto);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#loadAcciybtSynnartByCurrencies(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public AccountSummaryDTO loadAccountSummarByCurrencies(String token, String accountId, Integer periodSummary) {

		List<String> currencies = Arrays.asList("usd", "vnd", "hk", "cad", "eu");

		double lastIncome = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
		double lastOutcome = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
		Date lastIncomeDate = new Date(2018, 5, 10);
		Date lastOutcomeDate = new Date(2018, 4, 10);

		int currencyIndex = ThreadLocalRandom.current().nextInt(3);
		String accountNo = DevGeneratedDataUtil.generateAccountNo();

		AccountSummaryDTO dto = new AccountSummaryDTO(accountNo, currencies.get(currencyIndex),
				currencies.get(currencyIndex), lastIncome, lastIncomeDate, lastOutcomeDate, lastOutcome);
		return dto;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountLimnit(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AccountLimitDTO getAccountLimnit(String paymentType, String token) {
		// TODO Auto-generated method stub
		PaymentType.validPaymentType(paymentType);
		return accountLimit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountNameByAccountNo(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public String getAccountNameByAccountNo(String accountNo, String token) {
		// TODO Auto-generated method stub
		return DevGeneratedDataUtil.generateNameOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getUpcomingRepaymentDeposit(java.
	 * lang.String)
	 */
	@Override
	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token) {
		// TODO Auto-generated method stub

		UpcomingRepaymentDepositDTO dto = new UpcomingRepaymentDepositDTO();
		UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo = new UpcomingRepaymentDepositSubDTO(new Date(),
				100000D, "", "vnd", true);
		UpcomingRepaymentDepositSubDTO loanRepaymentInfo = new UpcomingRepaymentDepositSubDTO(new Date(), 500000D, "",
				"vnd", true);
		UpcomingRepaymentDepositSubDTO creditCardInfo = new UpcomingRepaymentDepositSubDTO(new Date(), 100000D, "",
				"vnd", true);
		dto.setMinMaturityDateDepositInfo(minMaturityDateDepositInfo);
		dto.setLoanRepaymentInfo(loanRepaymentInfo);
		dto.setCreditCardWithoutStatement(true);
		dto.setCreditCardInfo(creditCardInfo);
		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getBlockedTransaction(java.lang.
	 * String, java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer pageNumber, Integer pageSize,
			String token) {

		List<TransactionBlockedDTO> lstDtoData = new ArrayList<>();
		String accountNo = DevGeneratedDataUtil.generateAccountNo();

		double blockedAmount = 0;

		for (int i = 0; i < pageSize; i++) {
			Date createdDate = new Date();
			String transactionRef = DevGeneratedDataUtil.generateUUID();
			String blockedBy = DevGeneratedDataUtil.generateNameOfUser();
			String reason = DevGeneratedDataUtil.generateDescription();
			TransactionBlockedDTO dto = new TransactionBlockedDTO();
			dto.setAccountId(accountId);
			dto.setAccountNo(accountNo);
			dto.setCreatedDate(createdDate);
			dto.setTransactionRef(transactionRef);
			dto.setBlockedBy(blockedBy);
			dto.setReason(reason);
			dto.setBlockedAmount(blockedAmount);
			lstDtoData.add(dto);

		}
		return lstDtoData;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAccountNameNapas(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount,
			String token) {
		if (accountNo.equals("123")) {
			return null;
		}
		return DevGeneratedDataUtil.generateNameOfUser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#getAvatarInfo(java.lang.String)
	 */
	@Override
	public AvatarInfoOutputDTO getAvatarInfo(String omniToken) {
		AvatarInfoOutputDTO dtoOutput = new AvatarInfoOutputDTO();
		AvatarInfoDTO avatarInfo = new AvatarInfoDTO();
		avatarInfo.setData("imageTestVoDien.jpg");
		avatarInfo.setFileName("avatarName");
		dtoOutput.setAvatarInfo(avatarInfo);
		dtoOutput.setMessage(MessageConstant.OK_MESSAGE);
		dtoOutput.setStatus(StatusesAvatar.OK.getStatusesAvatarCode());
		return dtoOutput;
	}

	@Override
	public String getCifByAccountNo(String accountNo, String token) {
		return "phuhns";
	}

	@Override
	public Map<String, String> getCIFFromAccountList(List<String> accountNos, String token) {
		Map<String, String> CIFs = new HashMap<>();
		for (String accountNo : accountNos) {
			CIFs.put(accountNo, "801004");
		}
		return CIFs;
	}

	@Override
	public CustomerPersonalInfoDTO getPersonalInfo(String token) {
		String jsonInput = "{\"username\":\"kimlongap\",\"fullName\":\"HOANG NGUYEN SY PHU\",\"birthDate\":587062800000,\"birthPlace\":null,\"legalIdentification1\":\"264283317\",\"legalIdentification2\":null,\"legalIdentification3\":null,\"issuedDate1\":1418058000000,\"issuedDate2\":null,\"issuedDate3\":null,\"issuedPlace1\":\"T. Ninh Thuan\",\"issuedPlace2\":null,\"issuedPlace3\":null,\"homeAddress\":{\"street\":\"75A TO HIEU::, P.HIEP TAN, Q. Tan Phu\",\"postCode\":\"\",\"town\":\"Tp. Ho Chi Minh\",\"country\":\"\",\"house\":\"\",\"residence\":\"\"},\"deliveryAddress\":{\"street\":\"75A TO HIEU::, P.HIEP TAN, Q. Tan Phu\",\"postCode\":\"\",\"town\":\"Tp. Ho Chi Minh\",\"country\":\"\",\"house\":\"\",\"residence\":\"\"},\"mobilePhoneNumber\":\"0398675430\",\"homePhoneNumber\":\"\",\"businessPhoneNumber\":\"0398675430\",\"smsOtpMobileNumber\":\"0398675430\",\"faxNumber\":\"\",\"email\":\"kimlongap@gmail.com\",\"context\":\"DETAL\",\"corporateInfo\":{\"cif\":1731994,\"companyName\":\"Tan Binh::Tan Binh\",\"companyAddress\":\"Tan Binh\",\"taxNumber\":\"0300852005-018\",\"openedDate\":1534093200000}}";
		CustomerPersonalInfoDTO customerPersonalInfoDTO = new CustomerPersonalInfoDTO();
		try {
			customerPersonalInfoDTO = objectMapper.readValue(jsonInput, CustomerPersonalInfoDTO.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return customerPersonalInfoDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AccountService#uploadAvatar(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AvatarInfoOutputDTO uploadAvatar(String token, String file) {
		AvatarInfoOutputDTO dtoOutput = new AvatarInfoOutputDTO();
		AvatarInfoDTO avatarInfo = new AvatarInfoDTO();
		avatarInfo.setData("imageTestVoDien.jpg");
		avatarInfo.setFileName("avatarName");
		dtoOutput.setAvatarInfo(avatarInfo);
		dtoOutput.setMessage(MessageConstant.OK_MESSAGE);
		dtoOutput.setStatus(StatusesUpdateAvatar.OK.getStatusesUpdateAvatarCode());
		return dtoOutput;
	}

	@Override
	public Boolean updateProductDefaultDirectly(Map<String, String> map) {
		return true;
	}

	@Override
	public List<TransferAccountDTO> transferAccounts(String productList, String restrictions, String token) {
		List<TransferAccountDTO> list = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			TransferAccountDTO dto = new TransferAccountDTO();
			dto.setAccountId("" + i + 1);
			list.add(dto);
		}
		return list;
	}

	@Override
	public ChangeUsernameOutput changeUsername(ChangeUsernameInput changeUsername, String token) {
		throw new NotImplementedException();
	}

	@Override
	public AccountDetails findAccountDetail(String accountNum) {
		String jsonValue = "{\"nrbNumber\":\"0015100003525008\",\"ibanNumber\":\"0015100003525008\",\"currency\":\"VND\",\"category\":1035,\"postingRestriction\":0,\"subProduct\":\"1035\",\"charges\":\"\",\"abbreviationName\":\"ACCOUNT.TITLE.1-0015100003525008\",\"openingDate\":\"2018-03-06\",\"workingBalance\":34209607,\"workingAvailableFunds\":34209607,\"valueDatedAvailableFunds\":{\"balance\":34209607,\"date\":\"2018-11-14\"},\"attachedCards\":[],\"accountInterests\":{\"creditIntCapitalisation\":{\"previousCapitalisation\":\"2018-10-26\",\"nextCapitalisation\":\"2017-06-26\",\"frequency\":{\"nextDate\":1498410000000,\"periodUnit\":\"M\",\"periodCount\":1,\"dayOfMonth\":26},\"interestType\":\"STANDARD\",\"totalAccruedInterests\":25}},\"statementGeneration\":{\"frequency\":[{\"periodUnit\":\"X\",\"periodCount\":0,\"dayOfMonth\":0}],\"delivery\":[{\"method\":\"NONE\",\"language\":\"\",\"address\":{\"street\":\"\",\"postCode\":\"\",\"town\":\"\",\"country\":\"\",\"house\":\"\",\"residence\":\"\",\"additionalInfo\":\"\",\"countryName\":\"\",\"apartmentNumber\":\"\"}}]},\"postingRestrict\":\"debit\",\"owner\":{\"globusId\":1589405,\"fullName\":\"GB NAME 1-1589405\",\"mobilePhoneNumbers\":[\"SMS-1589405\"],\"birthDate\":752544000000,\"birthPlace\":\"\",\"corporateDataRDto\":{\"taxNumber\":\"\"},\"customerType\":\"INDIVIDUAL\",\"legalIdentifications\":[{\"type\":1,\"id\":\"LEGAL ID-1589405\",\"issuedDate\":1277424000000,\"issuedPlace\":\"Quang Nam\",\"typeId\":\"1\"}],\"homePhoneNumbers\":[\"\"],\"businessPhoneNumbers\":[null],\"faxNumbers\":[\"\"],\"mobilePhoneNumber\":\"SMS-1589405\",\"paperStatementsDeliveryActive\":true,\"legalIdentificationId\":\"LEGAL ID-1589405\",\"homePhoneNumber\":\"\"},\"_links\":{}}";
		try {
			return objectMapper.readValue(jsonValue, AccountDetails.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
