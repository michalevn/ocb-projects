package com.ocb.oma.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.WebLimitService;
import com.ocb.oma.dto.WebLimitDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/webLimit")
public class WebLimitController extends BaseController {

	@Autowired
	private WebLimitService webLimitService;

	@RequestMapping(value = "/get")
	public WebLimitDTO getExchangeRates() {
		return webLimitService.get();
	}

}
