package com.ocb.oma.account.service.prod;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ocb.oma.account.service.NewFoService;
import com.ocb.oma.account.util.ProtectedConfigUtils;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;
import com.ocb.oma.exception.OmaException;

@Service
@Profile("prod")
public class ProdNewFoServiceImpl implements NewFoService {

	@Value("${file.config.encryptedKey}")
	private String encryptedKey;
	
	@Value("${newfo.url}")
	private String baseUrl;

	@Value("${newfo.account}")
	private String account;

	@Value("${newfo.password}")
	private String password;

	@Autowired
	private RestTemplate restTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProdNewFoServiceImpl.class);

	@Override
	public NewFoTransactionInfo registerBankingProductOnline(NewFoBankingProductOnline bankingProduct) {
		String url = baseUrl + "/product-combination/registerBankingProductOnline";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			String auth = account + ":" + ProtectedConfigUtils.decrypt(password, encryptedKey);
			byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
			String authHeader = "Basic " + new String(encodedAuth);
			headers.set(HttpHeaders.AUTHORIZATION, authHeader);
			HttpEntity<NewFoBankingProductOnline> entity = new HttpEntity<>(bankingProduct, headers);
			NewFoBankingProductOnline newBankingProduct = restTemplate.postForObject(url, entity,
					NewFoBankingProductOnline.class);
			return newBankingProduct.getTransactionInfo();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("registerBankingProductOnline has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_NEWFO_ERROR, e.getMessage());
		}
	}
}
