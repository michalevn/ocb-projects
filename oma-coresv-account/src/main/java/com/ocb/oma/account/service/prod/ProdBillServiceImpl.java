/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.BillService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdBillServiceImpl extends ProdServiceImpl implements BillService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RestTemplate restTemplate;

	private static final Log LOG = LogFactory.getLog(ProdBillServiceImpl.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String token, Date dateFrom, Date dateTo) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<TransferAutoBillOutputDTO> dtos = new ArrayList<>();
		String url = host + "/api/transfer/search/transfer_auto_bill";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, Long> params = new HashMap<>();
			params.put("dateFrom", dateFrom.getTime());
			params.put("dateTo", dateTo.getTime());
			RequestEntity<Map<String, Long>> request = new RequestEntity<>(params, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> result = response.getBody();
			if (result.containsKey("content")) {
				List<Map> list = (List<Map>) result.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						TransferAutoBillOutputDTO dto = objectMapper.convertValue(map, TransferAutoBillOutputDTO.class);
						dtos.add(dto);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
		return dtos;
	}

	@Override
	public Boolean createAutoBillTransferDirectly(String token,
			CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {

		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/transfer/actions/create_auto_bill_transfer_directly";
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		RequestEntity<CreateAutoBillTransferDirectlyDTO> request = new RequestEntity<>(
				createAutoBillTransferDirectlyDTO, headers, HttpMethod.POST, URI.create(url));
		ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<Map<String, Object>>() {
				});
		Map<String, Object> result = response.getBody();
		if (result.containsKey("resultCode")) {
			String resultCode = (String) result.get("resultCode");
			if ("1".equals(resultCode)) {
				return true;
			} else if ("1000".equals(resultCode)) {
				throw new OmaException(MessageConstant.SERVER_OCP_ERROR, MessageConstant.DEBIT_ACCOUNT_NOT_VALID);
			} else {
				LOG.info(result);
				throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
			}
		} else {
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}

	@Override
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String token, String input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/transfer/actions/delete_auto_bill_transfer";
		// http://10.96.23.146:9080/cbp-ocb-service/api/transfer/actions/delete_auto_bill_transfer
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, String> params = new HashMap<>();
			params.put("orderNumber", input);

			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));

			ResponseEntity<DeleteAutoBillTransferOutputDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<DeleteAutoBillTransferOutputDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardStatements has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public OutputBaseDTO modifyAutoBillTransferDirectly(String token, TransferAutoBillDTO input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/transfer/actions/modify_auto_bill_transfer_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			Map<String, Object> params = new HashMap<>();
			params.put("orderNumber", input.getOrderNumber());
			params.put("frequencyPeriodCount", input.getFrequencyPeriodCount());
			params.put("frequencyPeriodUnit", input.getFrequencyPeriodUnit());
			params.put("firstExecutionDate", input.getFirstExecutionDate());
			params.put("paymentSetting", input.getPaymentSetting());
			params.put("recurringPeriod", input.getRecurringPeriod());
			params.put("amountLimit", input.getAmountLimit());
			params.put("finishDate", input.getFinishDate());

			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));

			ResponseEntity<OutputBaseDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<OutputBaseDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("modifyAutoBillTransferDirectly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<StandingOrderOutputDTO> standingOrderList(String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		List<StandingOrderOutputDTO> standingOrderOutputDTOs = new ArrayList<>();
		String url = host + "/api/standing_order/search/standing_order_list";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						StandingOrderOutputDTO standingOrderOutputDTO = objectMapper.convertValue(map,
								StandingOrderOutputDTO.class);
						standingOrderOutputDTOs.add(standingOrderOutputDTO);
					}
				}
			}
			return standingOrderOutputDTOs;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("standingOrderList has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public OutputBaseDTO removeStandingOrder(String token, String input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/standing_order/actions/remove_standing_order_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<String> entity = new HttpEntity<>(input, headers);
			Map<String, String> params = new HashMap<>();
			params.put("standingOrderId", input);

			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));

			ResponseEntity<OutputBaseDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<OutputBaseDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("removeStandingOrder has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public OutputBaseDTO createStandingOrder(String token, StandingOrderInputDTO input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/standing_order/actions/create_standing_order_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = df.format(input.getStartDate());
			String endDate = df.format(input.getEndDate());

			Map<String, Object> params = new HashMap<>();
			params.put("shortName", input.getShortName());
			params.put("amount", input.getAmount());
			params.put("beneficiary", input.getBeneficiary());
			params.put("creditAccount", input.getCreditAccount());
			params.put("remarks", input.getRemarks());
			params.put("debitAccountId", input.getDebitAccountId());
			params.put("currency", input.getCurrency());
			params.put("endDate", endDate);
			params.put("periodUnit", input.getPeriodUnit());
			params.put("periodCount", input.getPeriodCount());
			params.put("dayOfMonth", input.getDayOfMonth());
			params.put("startDate", startDate);

			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));

			ResponseEntity<OutputBaseDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<OutputBaseDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			LOG.error("createStandingOrder has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}

	@Override
	public OutputBaseDTO modifyStandingOrderDirectly(String token, StandingOrderInputDTO input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/standing_order/actions/modify_standing_order_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<StandingOrderInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));

			ResponseEntity<OutputBaseDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<OutputBaseDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			LOG.error("modifyStandingOrderDirectly has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR);
		}
	}
}
