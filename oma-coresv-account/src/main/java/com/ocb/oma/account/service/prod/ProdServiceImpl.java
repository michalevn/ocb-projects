/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.security.AccessTokenHolder;
import com.ocb.oma.account.typeReferences.MapTypeReference;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;

/**
 * @author docv
 *
 */
@Profile("prod")
public class ProdServiceImpl {

	@Value("${omni.url}")
	protected String host;

	@Autowired
	protected RestTemplate restTemplate;

	@Autowired
	protected ObjectMapper objectMapper;

	protected final Log LOG = LogFactory.getLog(this.getClass());

	protected <T> T getGetResponse(String path, Map<String, Object> queryParams, Class<T> responseType) {
		HttpHeaders headers = getHeaders();
		URI uri = toUri(path, queryParams);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);
		ResponseEntity<T> responseEntity = restTemplate.exchange(request, responseType);
		T response = responseEntity.getBody();
		return response;
	}

	protected Map<String, Object> getGetResponse(String path, Map<String, Object> queryParams) {
		HttpHeaders headers = getHeaders();
		URI uri = toUri(path, queryParams);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);
		ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request, new MapTypeReference());
		Map<String, Object> response = responseEntity.getBody();
		return response;
	}

	protected <T> T getPostResponse(String path, Object requestBody, Class<T> responseType) {
		if (requestBody == null) {
			requestBody = new HashedMap();
		}
		HttpHeaders headers = getHeaders();
		URI uri = toUri(path, null);
		RequestEntity<?> request = new RequestEntity<>(requestBody, headers, HttpMethod.POST, uri);
		ResponseEntity<T> responseEntity = restTemplate.exchange(request, responseType);
		T response = responseEntity.getBody();
		return response;
	}

	protected Map<String, Object> getPostResponse(String path, Object requestBody) {
		if (requestBody == null) {
			requestBody = new HashedMap();
		}
		HttpHeaders headers = getHeaders();
		URI uri = toUri(path, null);
		RequestEntity<?> request = new RequestEntity<>(requestBody, headers, HttpMethod.POST, uri);
		ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request, new MapTypeReference());
		Map<String, Object> response = responseEntity.getBody();
		return response;
	}

	protected <T> T getGetContent(String path, Map<String, Object> queryParams, Class<T> responseType) {
		Map<String, Object> response = getGetResponse(path, queryParams);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof Map) {
			return toObject(responseType, content);
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> getGetContent(String path, Map<String, Object> queryParams) {
		Map<String, Object> response = getGetResponse(path, queryParams);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof Map) {
			return (Map<String, Object>) content;
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("rawtypes")
	protected <T> List<T> getGetContentAsList(String path, Map<String, Object> queryParams, Class<T> responseType) {
		Map<String, Object> response = getGetResponse(path, queryParams);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof List) {
			return toList(responseType, (List) content);
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("unchecked")
	protected Map<String, Object> getPostContent(String path, Object requestBody) {
		Map<String, Object> response = getPostResponse(path, requestBody);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof Map) {
			return (Map<String, Object>) content;
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	protected <T> T getPostContent(String path, Object requestBody, Class<T> responseType) {
		Map<String, Object> response = getPostResponse(path, requestBody);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof Map) {
			return toObject(responseType, content);
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("rawtypes")
	protected <T> List<T> getPostContentAsList(String path, Object requestBody, Class<T> responseType) {
		Map<String, Object> response = getPostResponse(path, requestBody);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof List) {
			return toList(responseType, (List) content);
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("unchecked")
	protected List<Map<String, Object>> getPostContentAsList(String path, Object requestBody) {
		Map<String, Object> response = getPostResponse(path, requestBody);
		if (!response.containsKey("content")) {
			return null;
		}
		Object content = response.get("content");
		if (content instanceof List) {
			return (List<Map<String, Object>>) content;
		} else {
			LOG.info(content);
			throw new OmaException(MessageConstant.INVALID_OCP_RESPONSE_FORMAT);
		}
	}

	@SuppressWarnings("unchecked")
	protected <T> T toObject(final Class<T> clazz, final Object src) {
		if (src != null) {
			if (src instanceof Map) {
				Map<String, Object> properties = (Map<String, Object>) src;
				return newInstance(clazz, properties);
			} else if (src.getClass().equals(clazz)) {
				T target = newInstance(clazz, null);
				try {
					BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
					beanUtils.getConvertUtils().register(false, false, 0);
					beanUtils.copyProperties(target, src);
				} catch (IllegalAccessException | InvocationTargetException e) {
					LOG.error(e.getMessage(), e);
					throw new OmaException(MessageConstant.SERVER_ERROR, e.getMessage());
				}
				return target;
			} else {
				try {
					throw new OmaException(MessageConstant.SERVER_ERROR, "Can not create an instance of "
							+ clazz.getName() + " from " + objectMapper.writeValueAsString(src));
				} catch (JsonProcessingException e) {
					throw new OmaException(MessageConstant.SERVER_ERROR,
							"Can not create an instance of " + clazz.getName());
				}
			}
		}
		return newInstance(clazz, null);
	}

	@SuppressWarnings("rawtypes")
	protected <T> List<T> toList(Class<T> clazz, List srcs) {
		List<T> list = new ArrayList<>();
		if (srcs == null || srcs.isEmpty()) {
			return list;
		}
		for (Object src : srcs) {
			T bean = toObject(clazz, src);
			list.add(bean);
		}
		return list;
	}

	private <T> T newInstance(final Class<T> clazz, Map<String, ? extends Object> properties) {
		if (properties == null) {
			properties = new HashMap<>();
		}
		return objectMapper.convertValue(properties, clazz);
	}

	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + AccessTokenHolder.get());
		return headers;
	}

	private URI toUri(String path, Map<String, Object> params) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(host + path);
		if (params != null && !params.isEmpty()) {
			for (Entry<String, Object> entry : params.entrySet()) {
				uriBuilder.queryParam(entry.getKey(), entry.getValue());
			}
		}
		return uriBuilder.build().toUri();
	}
}
