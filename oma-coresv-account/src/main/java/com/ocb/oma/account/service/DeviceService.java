package com.ocb.oma.account.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface DeviceService {

	static final Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

	/**
	 * kich hoat thiet bi
	 * 
	 * @param token
	 * @param cif
	 * @param deviceId
	 * @return
	 */
	Boolean activate(String token, String cif, String deviceId);

	/**
	 * ham ngat kich hoat thiet bi
	 * 
	 * @param token
	 * @param cif
	 * @param deviceId
	 * @return
	 */
	Boolean deactivate(String token, String cif, String deviceId);

}
