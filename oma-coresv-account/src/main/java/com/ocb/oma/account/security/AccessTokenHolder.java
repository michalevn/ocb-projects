package com.ocb.oma.account.security;

import org.apache.commons.lang.StringUtils;

import com.ocb.oma.account.util.CentralizedThreadLocal;

public class AccessTokenHolder {

	private static final ThreadLocal<String> ACCESS_TOKEN = new CentralizedThreadLocal<>(
			AccessTokenHolder.class.getName());

	public static String get() {
		String accessToken = ACCESS_TOKEN.get();
		if (StringUtils.isBlank(accessToken)) {
			accessToken = "";
		}
		return accessToken;
	}

	public static void set(String accessToken) {
		ACCESS_TOKEN.set(accessToken);
	}

}
