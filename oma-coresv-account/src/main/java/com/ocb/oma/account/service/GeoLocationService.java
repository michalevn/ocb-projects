package com.ocb.oma.account.service;

import com.ocb.oma.dto.GeoLocationPoiDTO;

public interface GeoLocationService {

	GeoLocationPoiDTO find(Double lat, Double lng, String poiType);

}
