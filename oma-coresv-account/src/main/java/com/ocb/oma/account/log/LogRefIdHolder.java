package com.ocb.oma.account.log;

import com.ocb.oma.account.util.CentralizedThreadLocal;

public class LogRefIdHolder {

	private static final ThreadLocal<String> LOG_REF_ID = new CentralizedThreadLocal<>(LogRefIdHolder.class.getName());

	public static String get() {
		String refId = LOG_REF_ID.get();
		return refId;
	}

	public static void set(String refId) {
		LOG_REF_ID.set(refId);
	}

}
