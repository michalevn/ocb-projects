/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.Date;
import java.util.List;

import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * @author docv
 *
 */
public interface BillService {

	/**
	 * lấy danh sách đăng ký hóa đơn tự động
	 * 
	 * @param token
	 * @param dateTo
	 * @param dateFrom
	 * @return
	 */
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String token, Date dateFrom, Date dateTo);

	/**
	 * Tạo giao dịch thanh toán hóa đơn tự động
	 * 
	 * @param token
	 * @param createAutoBillTransferDirectlyDTO
	 * @return
	 */
	public Boolean createAutoBillTransferDirectly(String token,
			CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO);

	/**
	 * xoa thanh toan hoa don tu dong 
	 * @param token
	 * @param string
	 * @return
	 */
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String token, String string);

	/**
	 * chinh sua thanh toan hoa don tu dong 
	 * @param token
	 * @param input
	 * @return
	 */
	public OutputBaseDTO modifyAutoBillTransferDirectly(String token, TransferAutoBillDTO input);

	/**
	 * Lay danh sach chuyen tien dinh ky
	 * @param token
	 * @return
	 */
	public List<StandingOrderOutputDTO> standingOrderList(String token);

	/**
	 * xoa chuyen tien dinh ky
	 * @param token
	 * @param standingOrderId
	 * @return
	 */
	public OutputBaseDTO removeStandingOrder(String token, String standingOrderId);

	/**
	 * tao moi chuyen tien dinh ky
	 * @param token
	 * @param input
	 * @return
	 */
	public OutputBaseDTO createStandingOrder(String token, StandingOrderInputDTO input);

	/**
	 * cap nhat chuyen tien dinh ky
	 * @param token
	 * @param input
	 * @return
	 */
	public OutputBaseDTO modifyStandingOrderDirectly(String token, StandingOrderInputDTO input);
	
}
