/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.CreditService;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdCreditServiceImpl extends ProdServiceImpl implements CreditService {

	@Override
	public List<CreditDTO> getCredits() {
		List<CreditDTO> dtos = getGetContentAsList("/api/credit", null, CreditDTO.class);
		return dtos;
	}

	@Override
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String contractNumber, Date dateFrom,
			Date dateTo, BigDecimal operationAmountFrom, BigDecimal operationAmountTo, Integer pageNumber,
			Integer pageSize) {
		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("contractNumber", contractNumber);
		requestBody.put("dateFrom", dateFrom);
		requestBody.put("dateTo", dateTo);
		requestBody.put("pageNumber", pageNumber);
		requestBody.put("pageSize", pageSize);
		requestBody.put("operationAmountFrom", operationAmountFrom);
		requestBody.put("operationAmountTo", operationAmountTo);

		List<InstalmentTransactionHistoryDTO> dtos = getPostContentAsList(
				"/api/transaction/search/instalment_transaction_history", requestBody,
				InstalmentTransactionHistoryDTO.class);
		return dtos;
	}
}
