/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.ocb.oma.dto.PaymentType;

/**
 * @author phuhoang
 *
 */
public class DevGeneratedDataUtil {

	private static List<String> firstNames = Arrays.asList("Phú ", "Thắng", "Tân", "Uyên", "Nhân", " Nam", "Bảo", "Nguyệt", "Thanh", "Phú", "Mai", "Lan",
			"Ngọc", "Điệp", "Thảo");
	private static List<String> middleNames = Arrays.asList("Mai ", "Thị", "Đậu", "Bùi", "Lâm", "Thanh", "Thái", "");

	private static List<String> lastNames = Arrays.asList("Hoàng ", "Nguyễn ", "Huỳnh ", "Phase ", "Lâm", "Thanh", "");
	private static List<String> provinceCodes = Arrays.asList("01", "79", "31", "38");
	private static List<String> bankNames = Arrays.asList("Ngân hàng Việtcombank  ", "Ngân hàng BIDV ", "Ngân hàng Sacombank ", "Ngân hàng OCB");

	private static List<String> bankCodes = Arrays.asList("101", "333", "201", "202");

	private static List<String> branchCodes = Arrays.asList("01101018", "79101001", "01101011", "87101001", "82101001", "83101001");

	private static List<String> accountNumbers = Arrays.asList("0101898177", "098765456", "3456787654", "456765432", "45678765", "5678765");

	private static List<String> accountIds = Arrays.asList("40", "41", "42", "42");

	private static List<String> cardNumbers = Arrays.asList("9876543456", "2345678765 ", "45676545623 ", "78973636363");

	private static List<String> phoneNumbers = Arrays.asList("0398675430", "0398675431 ", "0398675432 ", "0398675433");
	private static List<String> cardTypes = Arrays.asList("CREDIT", "DEBIT");

	private static List<String> bankMACodes = Arrays.asList("Bank1", "Bank2", "Bank3", "Bank33");

	private static List<String> serviceCodes = Arrays.asList("ADSL", "ATIC", "PPMB", "POWE", "TIVI", "WATE", "TAICHINH");

	private static List<String> currencies = Arrays.asList("VND", "USD", "EUR");

	private static List<String> paymentTypes = Arrays.asList(PaymentType.InternalPayment.getPaymentTypeCode(), PaymentType.LocalPayment.getPaymentTypeCode(),
			PaymentType.FastTransfer.getPaymentTypeCode());

	private static List<String> messages = Arrays.asList("Turn on Wi-Fi messaging, group chat options & more",
			"Chat features are only available for some phones and service providers, including carriers or Jibe Mobile from Google. For more info, contact your service provider.",
			"Note: To use these options, everyone in the Messages conversation needs chat features turned on.",
			"If your carrier and device aren’t automatically set up for chat features, you may see a notification to “Do more with Messages.”",
			"Add to, leave, or name a group conversation", "See when you're sending by mobile data, SMS, or MMS", "How chat features work",
			"Show others when you're typing in a conversation with them", "Let others know you've read their messages", "Choose how to resend messages",
			"How can we help you?", "Change your Messages notifications and settings",
			"You can choose whether your device plays sounds, vibrates, or sends reminders when you get a message. (Or, you can choose how your device sends pictures and videos.)",
			"You can also choose how Messages handles advanced tasks.",
			"Note: Some of these steps work only on Android 8.0 and up. Learn how to check your Android version.",
			"If you made changes to specific people already, these changes won't apply. Learn how to change options for specific people. ");

	private static List<String> transactionTypes = Arrays.asList("NONCASH", "ATM", "CARD_TRANSFER", "CREDIT", "REPAYMENT");

	public static String generateMessage() {
		int index = Math.abs(ThreadLocalRandom.current().nextInt(messages.size()));
		return messages.get(index);
	}

	public static String generateDescription() {
		int index = Math.abs(ThreadLocalRandom.current().nextInt(messages.size()));
		return messages.get(index);
	}

	public static String generateNameOfUser() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return lastNames.get(i % lastNames.size()) + " " + middleNames.get(i % middleNames.size()) + " " + firstNames.get(i % firstNames.size());
	}

	public static String generateAccountNo() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return accountNumbers.get(i % accountNumbers.size());
	}

	public static String generateCardNumber() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return cardNumbers.get(i % cardNumbers.size()).trim();
	}

	public static String generatedBankName() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return bankNames.get(i % bankNames.size());
	}

	/**
	 * @return
	 */
	public static String generateBankCode() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return bankCodes.get(i % bankCodes.size());
	}

	/**
	 * @return
	 */
	public static String generatePaymentType() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return paymentTypes.get(i % paymentTypes.size());
	}

	public static String generatebankMACodes() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return bankMACodes.get(i % bankMACodes.size());
	}

	/**
	 * @return
	 */
	public static String generateBranchCode() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return branchCodes.get(i % branchCodes.size());
	}

	/**
	 * @return
	 */
	public static String generateBranchName() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return bankNames.get(i % bankNames.size());
	}

	public static String generateAccountId() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return accountIds.get(i % accountIds.size());
	}

	public static final Double generateAmount() {
		return ThreadLocalRandom.current().nextDouble() * 150000000;
	}

	public static String generateProvinceCodes() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return provinceCodes.get(i % provinceCodes.size());
	}

	public static String generatePhoneNumbers() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return phoneNumbers.get(i % phoneNumbers.size());
	}
	public static String generatePhoneNames() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return firstNames.get(i % firstNames.size());
	}

	public static String generateUUID() {
		return UUID.randomUUID().toString();
	}

	public static Double nextDouble(double min, double max) {
		return ThreadLocalRandom.current().nextDouble(min, max);
	}

	public static Integer nextInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max);
	}

	public static Long nextLong(long min, long max) {
		return ThreadLocalRandom.current().nextLong(min, max);
	}

	public static Boolean nextBoolean() {
		return ThreadLocalRandom.current().nextBoolean();
	}

	public static Date nextDate(int dateadded) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, dateadded);
		return calendar.getTime();
	}

	public static String generateCardType() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return cardTypes.get(i % cardTypes.size());
	}

	public static String generateCurrency() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return currencies.get(i % currencies.size());
	}

	public static String generateCardExpirationDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, nextInt(365 / 2, 365 * 5));
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yy");
		return dateFormat.format(calendar.getTime());
	}

	public static String generateCardAccountNumber() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return cardNumbers.get(i % cardNumbers.size());
	}

	public static String generateTransactionType() {
		int i = Math.abs(ThreadLocalRandom.current().nextInt(100));
		return transactionTypes.get(i % transactionTypes.size());
	}
}
