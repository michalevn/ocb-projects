package com.ocb.oma.account.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.CardService;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/card")
public class CardController extends BaseController {

	private Logger LOG = LoggerFactory.getLogger(CardController.class);

	@Autowired
	private CardService cardService;

	@RequestMapping(value = "/findCard")
	public List<CardInfoDTO> findCard(HttpServletRequest request) {
		LOG.info("findCard");
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		List<CardInfoDTO> rs = cardService.findCard(token);
		return rs;
	}

	@RequestMapping(value = "/restrictCard")
	public OutputBaseDTO restrictCard(HttpServletRequest request, @RequestParam("cardId") String cardNumber,
			@RequestParam("cardRestrictionReason") String restrictionReason) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.restrictCard(token, cardNumber, restrictionReason);
	}

	@RequestMapping(value = "/findCardBlockades")
	public List<CardBlockade> findCardBlockades(HttpServletRequest request, @RequestBody CardBlockadeInputDTO input) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.findCardBlockades(token, input);
	}

	@RequestMapping(value = "/getPaymentDetails")
	public PaymentsInFutureDTO getPaymentDetails(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestParam("paymentId") String paymentId) {
		return cardService.getPaymentDetails(token, paymentId);
	}

	@RequestMapping(value = "/postAuthActivateCard")
	public OutputBaseDTO postAuthActivateCard(HttpServletRequest request, @RequestParam("cardId") String cardId) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.postAuthActivateCard(token, cardId);
	}

	@RequestMapping(value = "/modifyCardAccountAutoRepayment")
	public Boolean modifyCardAccountAutoRepayment(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
			@RequestBody CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		LOG.info("modifyCardAccountAutoRepayment");
		cardService.modifyCardAccountAutoRepayment(authorization, cardAccountAutoRepayment);
		return true;
	}

	@RequestMapping(value = "/findCardAccountEntry")
	public List<CardAccountEntryDTO> findCardAccountEntry(
			@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
			@RequestBody CardAccountEntryQueryDTO cardAccountEntryQuery) {
		LOG.info("findCardAccountEntry");
		return cardService.findCardAccountEntry(authorization, cardAccountEntryQuery);
	}

	@RequestMapping(value = "/postPaymentCardAnother")
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(HttpServletRequest request,
			@RequestBody PaymentCardAnotherInputDTO input) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.postpaymentCardAnother(token, input);
	}

	@RequestMapping(value = "/findCardByCardId")
	public CardDetailByCardIdDTO findCardByCardId(HttpServletRequest request, @RequestParam("card_id") String cardId) {
		LOG.info("findCardByCardId");
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		CardDetailByCardIdDTO rs = cardService.findCardByCardId(token, cardId);
		return rs;
	}

	@RequestMapping(value = "/cardAutoRepayment")
	public List<CardAutoRepaymentDTO> cardAutoRepayment(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.cardAutoRepayment(token);
	}

	@RequestMapping(value = "/cardRejected")
	public List<CardRejectedDTO> cardRejected(HttpServletRequest request,
			@RequestBody CardRejectedInputDTO cardRejectedInput) {
		LOG.info("cardRejected");
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.cardRejected(token, cardRejectedInput);
	}

	@RequestMapping(value = "/cardStatements")
	public List<CardStatementsDTO> cardStatements(HttpServletRequest request,
			@RequestBody CardStatementsInputDTO input) {
		LOG.info("cardRejected");
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.cardStatements(token, input);
	}

	@RequestMapping(value = "/changeLimitDirectly")
	public Boolean changeLimitDirectly(@RequestBody List<ChangeLimitDTO> limits) {
		return cardService.changeLimitDirectly(limits);
	}

	@RequestMapping(value = "/configStatusEcommerceCard")
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(HttpServletRequest request,
			@RequestBody ConfigStatusEcommerceCardInput input) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.configStatusEcommerceCard(token, input);
	}

	@RequestMapping(value = "/cardPaymentInfo")
	public CardPaymentInfoOutput cardPaymentInfo(HttpServletRequest request, @RequestBody CardPaymentInfoInput input) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return cardService.cardPaymentInfo(token, input);
	}

	@RequestMapping(value = "/cardStatemenDownload")
	public void cardStatemenDownload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("linkStatementFile") String linkStatementFile) throws IOException {

		String[] fileNameArr = linkStatementFile.split("/");
		String fileName = fileNameArr[fileNameArr.length - 1];
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		InputStream is = cardService.cardStatemenDownload(token, linkStatementFile);
		response.addHeader("Content-disposition", "attachment;filename=" + fileName);
		response.setContentType("application/pdf");
		IOUtils.copy(is, response.getOutputStream());
		response.flushBuffer();
	}

}
