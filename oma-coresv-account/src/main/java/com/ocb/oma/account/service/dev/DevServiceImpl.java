/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author docv
 *
 */
@Service
@Profile("dev")
public class DevServiceImpl {

	@Autowired
	protected ObjectMapper objectMapper;

	public <T> List<T> toList(String jsonInput, Class<T> clazz) {
		try {
			List<Map<String, Object>> mapList = objectMapper.readValue(jsonInput,
					new TypeReference<List<Map<String, Object>>>() {
					});
			List<T> dtos = new ArrayList<>();
			for (Map<String, Object> properties : mapList) {
				dtos.add(newInstance(clazz, properties));
			}
			return dtos;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public <T> T toObject(String jsonInput, Class<T> clazz) {
		try {
			return objectMapper.readValue(jsonInput, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Map<String, Object> toMap(String jsonInput) {
		try {
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
			};
			return objectMapper.readValue(jsonInput, typeRef);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private <T> T newInstance(final Class<T> clazz, Map<String, ? extends Object> properties) {
		if (properties == null) {
			properties = new HashMap<>();
		}
		return objectMapper.convertValue(properties, clazz);
	}
}
