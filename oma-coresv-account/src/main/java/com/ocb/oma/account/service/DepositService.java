/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * @author phuhoang
 *
 */
public interface DepositService {

	static final Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

	/**
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public OutputBaseDTO breakDeposit(String depositId, String token);

	/**
	 * lay danh sach so tiet kiem
	 * 
	 * @param token
	 * @return
	 */
	public List<DepositDTO> getDeposits(String token);

	/**
	 * 1.61	Hàm lấy thông tin chi tiết sổ tiết kiệm
	 * 
	 * @param token
	 * @return
	 */
	public DepositDTO getDeposit(String token, String depositId);

	/**
	 * Ham lay danh sach san pham so tiet kiem
	 * 
	 * @param token
	 * @return
	 */
	public List<DepositProductDTO> getDepositProducts(String token);

	/**
	 * Lay danh sach lai suat tien gui tiet kiem
	 * 
	 * @param token
	 * @param token2
	 * @param subProductCode
	 * @return
	 */
	public List<DepositInterestRateDTO> getDepositInterestRates(String currency, String subProductCode, String token);

	/**
	 * Tao so tiet kiem
	 * 
	 * @param token
	 * @param deposit
	 * @return
	 */
	public CreateDepositOutputDTO createDeposit(String token, CreateDepositDTO deposit);

	/**
	 * Ham lay danh sach tai khoan đuoc mo tien gui so tiet kiem
	 * 
	 * @param token
	 * @return
	 */
	public DepositOfferAccountsDTO getDepositOfferAcc(String token);

	
}
