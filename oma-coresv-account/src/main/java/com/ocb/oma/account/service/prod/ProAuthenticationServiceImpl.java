/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ocb.oma.account.service.AccountService;
import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;

/**
 * @author Phu Hoang
 *
 */
@Service
@Profile("prod")
public class ProAuthenticationServiceImpl implements AuthenticationService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private AccountService accountService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#login(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean login(String username, String password) {
		LOGGER.debug("production profile login with username {}", username);
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#loadUserInfo(java.lang.
	 * String)
	 */
	@Override
	public UserToken loadUserInfo(String username) {
		LOGGER.debug("production profile loadUserInfo with username {}", username);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#loginAndLoadUserInfo(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public UserToken loginAndLoadUserInfo(String username, String password, String mobileDevice) {

		Map<String, String> postData = new HashMap<>();
		postData.put("username", username);
		postData.put("password", password);

		String url = host + "/api/token/actions/login";
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Map> entity = new HttpEntity<>(postData, headers);
		Map<String, Object> returnData = restTemplate.postForObject(url, entity, Map.class);
		if (((Boolean) returnData.get("credentialsCorrect")) == true) {
			UserToken result = new UserToken();
			String jwtToken = (String) returnData.get("jwtToken");
			Date lastLoginDate = null;
			if (returnData.get("lastCorrectLogonDate") != null) {
				lastLoginDate = new Date((Long) returnData.get("lastCorrectLogonDate"));
			}
			// get customer cif
			Map<String, Object> profileDetail = (Map<String, Object>) returnData.get("accessProfileDetails");
			String customerId = (String) profileDetail.get("customerId");
			String firstName = (String) profileDetail.get("firstName");
			String lastName = (String) profileDetail.get("lastName");
			Boolean changePasswordRequired = false;
			if (returnData.get("changePasswordRequired") != null) {
				changePasswordRequired = (Boolean) returnData.get("changePasswordRequired");
			}
			// getCustomer Info
			CustomerInfoDTO cusInfo = accountService.getCustomerDetail(jwtToken);
			String authMethod = cusInfo.getAuthType();
			String phoneNumber = cusInfo.getPhoneNumber();
			// set customerInfo
			OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
					lastName, lastLoginDate, authMethod, phoneNumber);
			oldOmni.setChangePasswordRequired(changePasswordRequired);
			result.setOldAuthen(oldOmni);
			return result;
		} else {
			String authenticationInCorrectReason = (String) returnData.get("authenticationInCorrectReason");
			if (returnData.containsKey("accessBlocked") && (Boolean) returnData.get("accessBlocked")) {
				throw new OmaException(MessageConstant.ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL);
			}
			if ("BLOCKED_CONTEXT".equalsIgnoreCase(authenticationInCorrectReason)) {
				throw new OmaException(MessageConstant.ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL);
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.AuthenticationService#changePassword(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	@Override
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId, String token) {
		if (token == null || oldPass == null || newPass == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/user/actions/change_user_password";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, Object> input = new HashMap<String, Object>();
			input.put("customerId", customerId);
			input.put("oldPassword", oldPass);
			input.put("newPassword", newPass);

			HttpEntity<Map> entity = new HttpEntity<>(input, headers);

			ChangePasswordDTO dtoResult = restTemplate.postForObject(url, entity, ChangePasswordDTO.class);

			return dtoResult;

		} catch (Exception e) {
			LOGGER.error("changePassword has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

}
