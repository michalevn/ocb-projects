/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.math.BigDecimal;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.WebLimitService;
import com.ocb.oma.dto.WebLimitDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdWebLimitServiceImpl extends ProdServiceImpl implements WebLimitService {

	private static final Integer DEFAULT_LIMIT = 1000000000;

	@Override
	public WebLimitDTO get() {
		WebLimitDTO webLimit = getGetResponse("/api/limit/get/web_limit", null, WebLimitDTO.class);
		if (webLimit == null) {
			webLimit = new WebLimitDTO();
		}
		if (webLimit.getLimitValue() == null) {
			webLimit.setLimitValue(BigDecimal.valueOf(DEFAULT_LIMIT));
		}
		return webLimit;
	}
}
