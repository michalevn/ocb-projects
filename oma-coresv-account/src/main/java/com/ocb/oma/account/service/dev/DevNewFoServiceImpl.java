package com.ocb.oma.account.service.dev;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.NewFoService;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;

@Service
@Profile("dev")
public class DevNewFoServiceImpl implements NewFoService {

	@Autowired
	private ObjectMapper objectMapper;

	private static final Logger LOGGER = LoggerFactory.getLogger(DevNewFoServiceImpl.class);

	@Override
	public NewFoTransactionInfo registerBankingProductOnline(NewFoBankingProductOnline bankingProduct) {

		try {
			LOGGER.info(objectMapper.writeValueAsString(bankingProduct));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}

		String jsonInput = "{\"transactionInfo\":{\"cRefNum\":\"NHS_2\",\"pRefNum\":\"5c00d8448d73e0df1940d46bb6e93252\",\"transactionReturn\":\"1\",\"transactionReturnMsg\":\"Successful\",\"clientCode\":\"LandingPage\"}}";
		try {
			NewFoTransactionInfo newFoTransactionInfo = objectMapper.readValue(jsonInput,
					new TypeReference<NewFoTransactionInfo>() {
					});
			newFoTransactionInfo.setcRefNum(bankingProduct.getTransactionInfo().getcRefNum());
			return newFoTransactionInfo;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
