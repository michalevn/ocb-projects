/**
 * 
 */
package com.ocb.oma.account.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;

/**
 * @author docv
 *
 */
public interface CreditService {

	/**
	 * @author docv
	 * 
	 *         1.81 Hàm lấy danh sách khoản vay – credit Trả về danh sách khoản vay
	 *         mà người dùng có
	 * 
	 * @param token
	 * @return
	 */
	public List<CreditDTO> getCredits();

	/**
	 * 
	 * @author docv
	 * 
	 *         1.82 Hàm lấy lịch sử thanh toán vay - instalment_transaction_history
	 * 
	 *         Trả về danh sách lịch sử thanh toán vay
	 * 
	 * @return
	 */
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String contractNumber, Date dateFrom,
			Date dateTo, BigDecimal operationAmountFrom, BigDecimal operationAmountTo, Integer pageNumber,
			Integer pageSize);

}
