package com.ocb.oma.account.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.OtpService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/otp")
public class OtpController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(OtpController.class);

	@Autowired
	private OtpService otpService;;

	@RequestMapping(value = "/sendAuthorizationSmsOtp")
	public AuthorizationResponse sendAuthorizationSmsOtp(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		AuthorizationResponse response = otpService.sendAuthorizationSmsOtp(token);
		return response;
	}

	@RequestMapping(value = "/verifyOTPToken")
	public AuthorizationResponse verifyOTPToken(HttpServletRequest request, @RequestBody VerifyOtpToken data) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		AuthorizationResponse response = otpService.verifyOTPToken(data, token);
		return response;
	}

	@RequestMapping(value = "/verifyHWOTPToken")
	public AuthorizationResponse verifyHWOTPToken(HttpServletRequest request, @RequestBody Map<String, String> data) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String hwToken = data.get("tokenValue");
		AuthorizationResponse response = otpService.verifyHwOTPToken(hwToken, token);
		return response;
	}

	@RequestMapping(value = "/getChallengeCodeSoftOTP")
	public String getChallengeCodeSoftOTP(HttpServletRequest request, @RequestBody GetChallengeCodeInputDTO input) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		String response = otpService.getChallengeCodeSoftOTP(input, token);
		return response;
	}

	// @RequestMapping(value = "/verifyOTPToken")
	public AuthorizationResponse verifySofOTP(HttpServletRequest request, @RequestBody VerifyOtpToken data) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		// AuthorizationResponse response = otpService.verifySoftOTP(otpCode, token)
		// return response;
		return null;
	}

	@RequestMapping(value = "/getAuthorizationPolicies")
	public List<String> getAuthorizationPolicies(@RequestParam("userId") String userId, HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return otpService.getAuthorizationPolicies(userId, token);

	}

	@RequestMapping(value = "/getQD630Policies")
	public List<QD630PoliciesDTO> getQD630Policies(@RequestParam("cif") String cif, HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return otpService.getQD630Policies(cif, token);
	}

	@RequestMapping(value = "/getListPrepaidPhone")
	public List<PrepaidPhoneDTO> getListPrepaidPhone(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		List<PrepaidPhoneDTO> response = otpService.getListPrepaidPhone(token);
		return response;
	}

	@RequestMapping(value = "/checkQRcodeInfo")
	public QRcodeInfoOutputDTO checkQRcodeInfo(HttpServletRequest request, @RequestBody QRcodeInfoInputDTO input) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		QRcodeInfoOutputDTO response = otpService.checkQRcodeInfo(token, input);
		return response;
	}

}
