/**
 * 
 */
package com.ocb.oma.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
/**
 * @author phuhoang
 *
 */
public interface OtpService {
	Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

	/**
	 * ham init OTP boi SMS
	 * 
	 * @param token
	 * @return
	 */
	public AuthorizationResponse sendAuthorizationSmsOtp(String token);

	/**
	 * ham xac thuc otp
	 * 
	 * @param data
	 * @param token
	 * @return
	 */
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data, String token);

	/**
	 * xac thuc hardware token
	 * @param hwToken
	 * @param token
	 * @return
	 */
	public AuthorizationResponse verifyHwOTPToken(String hwToken, String token);


	/**
	 * lay challenge code
	 * @param input
	 * @param token
	 * @return
	 */
	String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input, String token);

	/**
	 * xac thuc hardware token
	 * @param hwToken
	 * @param token
	 * @return
	 */
	public AuthorizationResponse verifySoftOTP(PaymentInfoDTO paymentInfo, VerifyOtpToken otpCode, String token);

	/**
	 * DS phuong thuc xac thuc da dang ky
	 * @param userId
	 * @param token
	 * @return
	 */
	List<String> getAuthorizationPolicies(String userId, String token);

	/**
	 * DS quy dingh theo thong tu
	 * @param cif
	 * @param token
	 * @return
	 */
	public List<QD630PoliciesDTO> getQD630Policies(String cif, String token);
	

	/**
	 * Lay danh sach so dien thoai da luu truoc do
	 */
	public List<PrepaidPhoneDTO> getListPrepaidPhone(String token);

	//	Ham kiem tra thong tin ma QRCode 
	public QRcodeInfoOutputDTO checkQRcodeInfo(String token, QRcodeInfoInputDTO input);
	
}
