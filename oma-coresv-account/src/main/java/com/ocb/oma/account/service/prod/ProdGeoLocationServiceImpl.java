/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.GeoLocationService;
import com.ocb.oma.dto.GeoLocationDTO;
import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.dto.GpsPositionDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdGeoLocationServiceImpl extends ProdServiceImpl implements GeoLocationService {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public GeoLocationPoiDTO find(Double lat, Double lng, String poiType) {
		String path = "/api/geolocation_poi";
		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("lat", lat);
		requestBody.put("lng", lng);
		requestBody.put("poiType", poiType);
		Map<String, Object> response = getPostResponse(path, requestBody);
		GeoLocationPoiDTO geoLocationPoi = new GeoLocationPoiDTO();
		if (response.containsKey("poiPositions")) {
			Map<String, Object> poiPositionsMap = (Map<String, Object>) response.get("poiPositions");
			if (poiPositionsMap.containsKey("content")) {
				List<GeoLocationDTO> poiPositions = toList(GeoLocationDTO.class, (List) poiPositionsMap.get("content"));
				geoLocationPoi.setPoiPositions(poiPositions);
			}
		}
		GpsPositionDTO requestedGpsPosition = toObject(GpsPositionDTO.class, response.get("requestedGpsPosition"));
		geoLocationPoi.setRequestedGpsPosition(requestedGpsPosition);
		return geoLocationPoi;
	}

}
