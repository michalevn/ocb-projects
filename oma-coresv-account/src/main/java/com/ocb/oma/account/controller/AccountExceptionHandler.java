/**
 * @author Phu Hoang
 */
package com.ocb.oma.account.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OmaExceptionDTO;
import com.ocb.oma.exception.OmaException;

/**
 * @author Phu Hoang
 *
 */
@ControllerAdvice
@RestController
public class AccountExceptionHandler {

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<?> handleUnknowException(Exception ex, WebRequest request) {
		OmaExceptionDTO dto = new OmaExceptionDTO();
		dto.setErrorCode(MessageConstant.ACCOUNT_SERVICE_ERROR);
		dto.setErrorCode(ex.getMessage());
		LOGGER.error("Unkown exception : ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
	}

	@ExceptionHandler(OmaException.class)
	public final ResponseEntity<?> handleOmaException(OmaException ex, WebRequest request) {

		OmaExceptionDTO dto = new OmaExceptionDTO();
		dto.setDescription(ex.getDesc());
		dto.setErrorCode(ex.getMessage());
		LOGGER.error("Logic exception : ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
	}

}
