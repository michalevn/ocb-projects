/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.PaymentService;
import com.ocb.oma.dto.BasketTransferDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.ExecutedStatus;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentDTO;
import com.ocb.oma.dto.PaymentDetails;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.BillDetailDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentItem;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.ServiceForBillPaymentDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.oomni.dto.ServiceProviderForBillPaymentDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevPaymentServiceImpl implements PaymentService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentBasketService#countBasketPaymentToProcess(
	 * java.lang.String)
	 */
	@Override
	public Integer countBasketPaymentToProcess(String token) {
		// TODO Auto-generated method stub
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		return 20;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getTransactionFee(com.ocb.oma.dto.
	 * input.TransactionFeeInputDTO, java.lang.String)
	 */
	@Override
	public Double getTransactionFee(TransactionFeeInputDTO input, String token) {
		// TODO Auto-generated method stub

		return 0D;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#postPayment(com.ocb.oma.dto.input.
	 * PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input, String token) {
		// TODO Auto-generated method stub

		PostPaymentOutputDTO output = new PostPaymentOutputDTO();
		if (input.getPaymentInfo().getAmount() < 500000) {
			output.setStatus(ExecutedStatus.INTERNAL_ERROR.getCode());
			output.setAuthErrorCause("some errore Code");
			output.setErrorMsg("error");
			output.setSuccess(false);
		} else if (input.getPaymentInfo().getAmount() == 987654321) {
			output.setStatus(ExecutedStatus.REJECTED.getCode());
			output.setSuccess(false);
		} else {
			output.setStatus(ExecutedStatus.EXECUTED.getCode());
			output.setSuccess(true);
			output.setReferenceId(input.getPaymentInfo().getcRefNum());
			output.setReferenceId(DevGeneratedDataUtil.generateUUID());
			output.setCoreRefNum2(DevGeneratedDataUtil.generateUUID());
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#postPaymentAddBasket(com.ocb.oma.
	 * dto.input.PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input, String token) {
		PostAddBasketDTO output = new PostAddBasketDTO();
		List<String> lstPaymentType = new ArrayList<>();
		lstPaymentType.add(PaymentType.InternalPayment.getPaymentTypeCode());
		lstPaymentType.add(PaymentType.LocalPayment.getPaymentTypeCode());
		lstPaymentType.add(PaymentType.FastTransfer.getPaymentTypeCode());
		if (lstPaymentType.contains(input.getPaymentType()) && input.getPaymentInfo().getAmount() == 123456789) {

			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (input.getPaymentInfo().getAmount() == 987654321) {
			output.setResultCode("1000");
			output.setResultMsg("Invalid debit account");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListofBasket(java.lang.String)
	 */
	@Override
	public List<PaymentBasketDTO> getListofBasket(String token, Double amountFrom, Double amountTo, String customerId,
			String realizationDateFrom, String realizationDateTo, String statuses) {
		// TODO Auto-generated method stub
		List<PaymentBasketDTO> result = new ArrayList<>();
		Integer pagesize = 10;
		for (Integer i = 0; i < pagesize; i++) {
			PaymentBasketDTO dto = new PaymentBasketDTO();
			dto.setAccountId("" + i + 1);
			List<BasketTransferDTO> basketTransfers = new ArrayList<>();
			BasketTransferDTO basketTransferDTO = new BasketTransferDTO();
			PaymentDTO payment = new PaymentDTO();
			payment.setAccountId(DevGeneratedDataUtil.generateAccountId());
			basketTransferDTO.setPayment(payment);

			dto.setBasketTransfers(basketTransfers);
//			
//			String recipientName = DevGeneratedDataUtil.generateNameOfUser();
//			String accountId = DevGeneratedDataUtil.generateAccountId();
//			Double amount = DevGeneratedDataUtil.generateAmount();
//			String currency = "VND";
//			String accountNo = DevGeneratedDataUtil.generateAccountNo();
//			String beneficiaryAccountNo = DevGeneratedDataUtil.generateAccountNo();
//			String bankName = DevGeneratedDataUtil.generatedBankName();
//			String bankCode = DevGeneratedDataUtil.generateBankCode();
//			String paymentType = DevGeneratedDataUtil.generatePaymentType();
//			String provinceId = DevGeneratedDataUtil.generateProvinceCodes();
//			String branchCode = DevGeneratedDataUtil.generateBankCode();
//			String cardNumber = DevGeneratedDataUtil.generateCardNumber();
//			String bankCodeMa = DevGeneratedDataUtil.generatebankMACodes();
//			String remarks = DevGeneratedDataUtil.generateDescription();
//			Date executionDate = new Date();
//			Date createdDate = new Date();
//			if (paymentType.equals(PaymentType.InternalPayment.getPaymentTypeCode())) {
//
//				cardNumber = null;
//				branchCode = null;
//				provinceId = null;
//				bankCode = null;
//				bankName = null;
//
//			}
//			if (paymentType.equals(PaymentType.LocalPayment.getPaymentTypeCode())) {
//				cardNumber = null;
//			}
//			if (paymentType.equals(PaymentType.FastTransfer.getPaymentTypeCode())) {
//
//				// cardNumber = null;
//				branchCode = null;
//				provinceId = null;
//				bankCode = null;
//				bankName = null;
//
//			}
//			PaymentBasketDTO dto = new PaymentBasketDTO(paymentType, createdDate, accountId, beneficiaryAccountNo,
//					amount, currency, cardNumber, beneficiaryAccountNo, bankCode, branchCode, provinceId, recipientName,
//					remarks, executionDate);
//			dto.setBasketId(i.toString());
//			dto.setAccountNo(accountNo);
			result.add(dto);

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#deleteBasket(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String deleteBasket(String basketId, String token) {
		try {
			String res = "";
			if (basketId == null || basketId.isEmpty()) {
				throw new OmaException(MessageConstant.MISSING_PARAMETER);
			}
			res = MessageConstant.OK_MESSAGE.toString();
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			throw new OmaException(MessageConstant.CAN_NOT_UPDATE_OTHER_USER_DATA);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getListOfServiceProviders(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(Date startDate, Date toDate, String token) {
		List<BillPaymentHistoryDTO> lst = new ArrayList<>();
		BillPaymentHistoryDTO dto = new BillPaymentHistoryDTO();
		dto.setAmount(100000D);
		dto.setCreateDate("2018-10-15");
		dto.setCurrency("VND");
		BillDetailDTO detail = new BillDetailDTO();
		detail.setAccountNumber("0037100003605005");
		detail.setBillCode("PE01000060919");
		detail.setQty(1);
		dto.setDetail(detail);
		dto.setProviderCode("EVNHCM");
		dto.setProviderName("Điện lực TPHCM – HCM EVN");
		dto.setServiceCode("POWE");
		dto.setServiceName("Ðiện – Power bill");
		dto.setStatus("100");
		lst.add(dto);
		return lst;
//		throw new NotImplementedException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * com.ocb.oma.account.service.PaymentService#getListOfServiceForBillPayment(
	 * java.lang.String)
	 * com.ocb.oma.account.service.PaymentService#getListBillPaymentByService(java.
	 * lang.String)
	 */
	@Override
	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration, String token) {
		List<BillPaymentServiceDTO> lstres = new ArrayList<>();
		String[] billCodeArr = { "B009872", "B009874", "B009839", "B009679", "B006879", "B003879", "B009859",
				"B005679" };
		for (int j = 0; j < billCodeArr.length; j++) {
			BillPaymentServiceDTO billPaymentServiceDTO = new BillPaymentServiceDTO();
			billPaymentServiceDTO.setAddress("235, Hoang Hoa Tham");
			billPaymentServiceDTO.setAmount(DevGeneratedDataUtil.generateAmount());
			billPaymentServiceDTO.setBillCode(billCodeArr[j]);
			List<BillPaymentItem> billItems = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				BillPaymentItem billPaymentType = new BillPaymentItem();
				billPaymentType.setAmountMonth(25000000D / (i + 1));
				billPaymentType.setBillCodeItemNo("123");
				billPaymentType.setCustomerCode("00079");
				billPaymentType.setCustomerName("Nguyen Hoai Nam");
				billPaymentType.setDescription("Thanh toan no den han");
				billPaymentType.setFormulaRates("formulaRates");
				billPaymentType.setFromDate("1540260103");
				billPaymentType.setOrderId("O001");
				billPaymentType.setProductType("productType");
				billPaymentType.setQty(i + 500);
				billPaymentType.setToDate("1542938503");
				billItems.add(billPaymentType);
			}
			billPaymentServiceDTO.setBillItems(billItems);

			billPaymentServiceDTO.setCustomerCode("KH00009");
			billPaymentServiceDTO.setCustomerName("Hoang Nhut Nam");
			billPaymentServiceDTO.setMeterNumber("meterNumber");
			billPaymentServiceDTO.setPhoneNumber(DevGeneratedDataUtil.generatePhoneNumbers());
			ServiceProviderForBillPaymentDTO provider = new ServiceProviderForBillPaymentDTO();
			provider.setProviderCode(DevGeneratedDataUtil.generateProvinceCodes());
			provider.setProviderName("Ha Tinh");
			billPaymentServiceDTO.setProvider(provider);
			ServiceForBillPaymentDTO service = new ServiceForBillPaymentDTO();
			service.setServiceCode(DevGeneratedDataUtil.generateBranchCode());
			service.setServiceName("OB Trung Bo");
			billPaymentServiceDTO.setService(service);
			lstres.add(billPaymentServiceDTO);
		}
		return lstres;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see <<<<<<< HEAD
	 * com.ocb.oma.account.service.PaymentService#getListOfBillTransactions(java.
	 * util.Date, java.util.Date, java.lang.String, java.lang.String)
	 * com.ocb.oma.account.service.PaymentService#getBill(com.ocb.oma.dto.input.
	 * GetBillInputDTO, java.lang.String)
	 */
	@Override
	public BillPaymentServiceDTO getBill(GetBillInputDTO input, String token) {

		BillPaymentServiceDTO dto = new BillPaymentServiceDTO("bill01", "1", DevGeneratedDataUtil.generateNameOfUser(),
				DevGeneratedDataUtil.generateBranchCode(), DevGeneratedDataUtil.generateDescription(),
				DevGeneratedDataUtil.generatePhoneNumbers(), "meterNumber", DevGeneratedDataUtil.generateAmount(), "1",
				"billSourceData", null, null, null);
		ServiceForBillPaymentDTO service = new ServiceForBillPaymentDTO();
		service.setServiceCode("ADSL");
		service.setServiceName("Internet");
		ServiceProviderForBillPaymentDTO provider = new ServiceProviderForBillPaymentDTO("provider02",
				"Nha cung cap 2");
		List<BillPaymentItem> billItems = new ArrayList<>();
		String[] fromDateArr = { "1603849954", "1606528354", "1609120354", "1611798754", "1614477154", "1622166754",
				"1559008354" };
		for (int i = 0; i < fromDateArr.length; i++) {
			BillPaymentItem billPaymentItem = new BillPaymentItem();
			billPaymentItem.setAmountMonth(DevGeneratedDataUtil.generateAmount());
			billPaymentItem.setBillCodeItemNo("billCodeItem0" + (i + 1));
			billPaymentItem.setCustomerCode(DevGeneratedDataUtil.generateBranchCode());
			billPaymentItem.setCustomerName(DevGeneratedDataUtil.generateNameOfUser());
			billPaymentItem.setDescription(DevGeneratedDataUtil.generateDescription());
			billPaymentItem.setFormulaRates("formulaRates");
			billPaymentItem.setFromDate(fromDateArr[i]);
			billPaymentItem.setOrderId("0001");
			billPaymentItem.setProductType("productType");
			billPaymentItem.setQty(i + 15);
			billPaymentItem.setToDate("1669427904");
			billItems.add(billPaymentItem);
		}

		dto.setService(service);
		dto.setProvider(provider);
		dto.setBillItems(billItems);
		return dto;
	}

	@Override
	public List<ServiceProviderComposDTO> getListOfServiceProvider(String token) {
		List<ServiceProviderComposDTO> result = new ArrayList<>();
		for (int e = 0; e < 6; e++) {
			ServiceProviderComposDTO dto = new ServiceProviderComposDTO();
			String[] serviceCodeArr = { "ADSL", "FTEL", "POWE" };
			String[] serviceNameArr = { "ADSL – Internet ADSL bill", "Ðiện thoại cố định – Home phone bill",
					"Ðiện – Power bill" };
			for (int m = 0; m < serviceCodeArr.length; m++) {
				for (int n = 0; n < serviceNameArr.length; n++) {
					ServiceForBillPaymentDTO service = new ServiceForBillPaymentDTO();
					service.setServiceCode(serviceCodeArr[m]);
					service.setServiceName(serviceNameArr[n]);
					dto.setService(service);
					List<ServiceProviderForBillPaymentDTO> providers = new ArrayList<>();
					String[] providerCodeArr = { "FPT", "HTV-TMS", "SPT-HCM", "SPT-PMH" };
					String[] providerNameArr = { "FPT-FPT", "HTV-TMS",
							"SPT Bưu chính Viễn thông Sài Gòn – Sai Gon Postel", "SPT Phú Mỹ Hưng – Phu My Hung SPT" };
					for (int i = 0; i < providerCodeArr.length; i++) {
						for (int j = 0; j < providerNameArr.length; j++) {
							ServiceProviderForBillPaymentDTO serviceProviderForBillPaymentDTO = new ServiceProviderForBillPaymentDTO();
							serviceProviderForBillPaymentDTO.setProviderCode(providerCodeArr[i]);
							serviceProviderForBillPaymentDTO.setProviderName(providerNameArr[j]);
							providers.add(serviceProviderForBillPaymentDTO);
						}
					}
					dto.setProviders(providers);
				}
			}
			result.add(dto);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.PaymentService#loadEVNParameter()
	 */
	@Override
	public List<EVNParameter> loadEVNParameter(String token) {
		// TODO Auto-generated method stub
		// bo sung dac ta ham loadEVNparameter trong PaymentService
		/**
		 * lay providerCodes cau hinh theo EVN Load quy dinh nhan dien ma ENV. Vi du -
		 * Bat dau bang PB.... --> ProviderCode: 004 - EVN Mien Nam - Bat dau bang
		 * PD.... --> ProviderCode: 005 - EVN HaNoi - Bat dau bang PA.... -->
		 * ProviderCode: xxx - EVN Mien Bac
		 * 
		 * @return
		 */
		List<EVNParameter> lstResult = new ArrayList<>();
		EVNParameter dtoEVNParameter = new EVNParameter();
		String providerCode = "EVNHN";
		String providerName = "EVN Ha Noi";
		String[] prefix = { "PB1905", "PB0604", "PB1901", "PB1902", "PB1903", "PB1906", "PB1907", "PK0100", "PK0200",
				"PK0300", "PK0400", "PK0600", "PK0700", "PK0800", "PK0900", "PK1000", "PK1100", "PK9900", "PK0500",
				"PB1701", "PB1702", "PB1703", "PB1704", "PB1705", "PB1706", "PB1707", "PB1708", "PB1709", "PB1710",
				"PB1711", "PB2002", "PB2003", "PB2004", "PB2005", "PB2006", "PB2007", "PB2008", "PB2001", "PB0701",
				"PB0702", "PB0703", "PB0704", "PB0705", "PB0706", "PB0707", "PB0708", "PB0709", "PB0710", "PB0711",
				"PB0712", "PB1001", "PB1002", "PB1003", "PB1004", "PB1005", "PB1006", "PB1007", "PB1008", "PB1401",
				"PB1402", "PB1403", "PB1404", "PB1405", "PB1406", "PB1408", "PB1409", "PB1410", "PB1301", "PB1302",
				"PB1303", "PB1304", "PB1305", "PB1306", "PB1307", "PB1308", "PB1309", "PB1310", "PB1311", "PB1312",
				"PB0601", "PB0602", "PB0605", "PB0607", "PB0608", "PB0609", "PB0610", "PB0611", "PB0612", "PB0613",
				"PB0614", "PB0603", "PB0606", "PB1100", "PB1101", "PB1103", "PB1104", "PB1105", "PB1106", "PB1107",
				"PB1108", "PB1109", "PB1904", "PB1201", "PB1202", "PB1203", "PB1204", "PB1205", "PB1206", "PB1207",
				"PB1208", "PB1209", "PB1210" };
		dtoEVNParameter.setProviderCode(providerCode);
		dtoEVNParameter.setProviderName(providerName);
		dtoEVNParameter.setPrefix(prefix);
		lstResult.add(dtoEVNParameter);

		providerCode = "EVNMN";
		providerName = "EVN Mien Nam";
		String[] prefixs = { "PD01", "PD02", "PD03", "PD04", "PD05", "PD06", "PD07", "PD08", "PD09", "PD10", "PD11",
				"PD12", "PD13", "PD14", "PD15", "PD16", "PD17", "PD18", "PD19", "PD20", "PD21", "PD22", "PD23", "PD24",
				"PD25", "PD26", "PD27", "PD28", "PD29", "PD30" };
		dtoEVNParameter.setProviderCode(providerCode);
		dtoEVNParameter.setProviderName(providerName);
		dtoEVNParameter.setPrefix(prefixs);
		lstResult.add(dtoEVNParameter);

		return lstResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#updateBasket(com.ocb.oma.dto.input
	 * .PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input, String token) {
		PostAddBasketDTO output = new PostAddBasketDTO();
		List<String> lstPaymentType = new ArrayList<>();
		lstPaymentType.add(PaymentType.InternalPayment.getPaymentTypeCode());
		lstPaymentType.add(PaymentType.LocalPayment.getPaymentTypeCode());
		lstPaymentType.add(PaymentType.FastTransfer.getPaymentTypeCode());
		if (lstPaymentType.contains(input.getPaymentType()) && input.getPaymentInfo().getAmount() == 123456789) {

			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (input.getPaymentInfo().getAmount() == 123) {
			output.setResultCode("06");
			output.setResultMsg("can_not_find_data");
		} else if (input.getPaymentInfo().getAmount() == 987654321) {
			output.setResultCode("1000");
			output.setResultMsg("Invalid debit account");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.PaymentService#postBaskets(java.util.List)
	 */
	@Override
	public PostListBasketDTO postBaskets(List<String> transfersIds, String token) {
		PostListBasketDTO postListBasketDTO = new PostListBasketDTO();
		postListBasketDTO.setFailedTransactions(DevGeneratedDataUtil.nextInt(0, 20));
		List<String> messages = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			messages.add(DevGeneratedDataUtil.generateMessage());
		}
		postListBasketDTO.setMessages(messages);
		postListBasketDTO.setNotAcceptedTransactions(DevGeneratedDataUtil.nextInt(0, 20));
		postListBasketDTO.setReadyTransactions(DevGeneratedDataUtil.nextInt(0, 20));
		postListBasketDTO.setResultCode(RandomStringUtils.randomAlphanumeric(8));
		postListBasketDTO.setResultMsg(DevGeneratedDataUtil.generateMessage());
		postListBasketDTO.setTransactionsSubmited(DevGeneratedDataUtil.nextBoolean());
		return postListBasketDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.PaymentService#getPaymentsInFuture(java.lang.
	 * String, com.ocb.oma.dto.input.PaymentsInFutureInputDTO)
	 */
	@Override
	public List<PaymentsInFutureDTO> getPaymentsInFuture(String token, PaymentsInFutureInputDTO input) {
		List<PaymentsInFutureDTO> lst = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			PaymentsInFutureDTO dto = new PaymentsInFutureDTO();
			dto.setAccountCurrency(DevGeneratedDataUtil.generateCurrency());
			dto.setAccountId(DevGeneratedDataUtil.generateAccountId());
			dto.setAccountName("Hani");
			dto.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
			dto.setAddToBasket(DevGeneratedDataUtil.nextBoolean());
			dto.setAmount(DevGeneratedDataUtil.generateAmount());
			List<Map<String, ?>> charges = new ArrayList<>();
			dto.setCharges(charges);
			dto.setCurrency(DevGeneratedDataUtil.generateCurrency());
			Map<String, ?> cyclicDefinition = new HashMap<>();
			dto.setCyclicDefinition(cyclicDefinition);
			dto.setCyclicDefinition(cyclicDefinition);
			dto.setDeliveryDate("1480125504");
			String[] description = { DevGeneratedDataUtil.generateDescription() };
			dto.setDescription(description);
			dto.seteWalletPhoneNumber(DevGeneratedDataUtil.generatePhoneNumbers());
			dto.setId(DevGeneratedDataUtil.generateAccountId());
			dto.setIsCyclic(DevGeneratedDataUtil.nextBoolean());
			dto.setLastRealizationDesc("lastRealizationDesc");
			dto.setOperationStatus("operationStatus");
			dto.setOriginalCustomerId("originalCustomerId");
			dto.setOriginalCustomerName("originalCustomerName");
			dto.setOriginalCustomerVisible(DevGeneratedDataUtil.nextBoolean());
			dto.setOwner("owner");
			PaymentDetails paymentDetails = new PaymentDetails();
			paymentDetails.setBankDetails("bankDetails");
			paymentDetails.setCreditAccountBankBranchCode("creditAccountBankBranchCode");
			paymentDetails.setCreditAccountBankCode(DevGeneratedDataUtil.generateBranchCode());
			paymentDetails.setCreditAccountProvinceCode(DevGeneratedDataUtil.generateProvinceCodes());
			paymentDetails.setFee("fee");
			paymentDetails.setFeeAccount(DevGeneratedDataUtil.generateAccountNo());
			dto.setPaymentDetails(paymentDetails);
			dto.setPaymentType(DevGeneratedDataUtil.generatePaymentType());
			dto.setRealizationDate("1543197504");
			dto.setRealizationDateShiftReason("realizationDateShiftReason");
			dto.setRecipientAccountId(DevGeneratedDataUtil.generateAccountId());
			dto.setRecipientAccountNo("000987334");
			dto.setRecipientAddress("123, Ngô Tất Tố, P6, Q1 ");
			String[] recipientName = { "Nguyễn thị mỸ Nhân" };
			dto.setRecipientName(recipientName);
			dto.setRegistrationDate("1669427904");
			dto.setSaveTemplate(DevGeneratedDataUtil.nextBoolean());
			dto.setSenderAddress("564, Nam Kỳ Khởi Nghĩa");
			dto.setSenderName("Lý Thị Dần");
			dto.setStatus("status");
			dto.setTemplateId("templateId001");
			dto.setTemplateName("templateName001");
			dto.setTransactionId("transactionId01");
			dto.setTitle("title");
			dto.setTransactionTypeDesc("transactionTypeDesc");
			dto.setTransferType(DevGeneratedDataUtil.generatePaymentType());
			lst.add(dto);
		}
		return lst;
	}

	@Override
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String token, String idFuturePayment) {
		// TODO Auto-generated method stub
//		throw new NotImplementedException();
		PaymentsInFutureDTO dto= new PaymentsInFutureDTO();
		dto.setAccountCurrency("VND");
		dto.setAccountId("11");
		dto.setAccountName("ACCOUNT8010034");
		dto.setAccountNo("0100100003486003");
		return dto;
	}

}
