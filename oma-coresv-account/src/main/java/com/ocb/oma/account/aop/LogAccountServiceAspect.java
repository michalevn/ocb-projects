/**
 *@author  Phu Hoang
 */
package com.ocb.oma.account.aop;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.log.LogRefIdHolder;
import com.ocb.oma.dto.LogDTO;

/**
 * @author Phu Hoang
 *
 */
@Aspect
@Configuration
public class LogAccountServiceAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogAccountServiceAspect.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Around("execution(* com.ocb.oma.account.service.*.*(..))")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
		Object result = null;
		LogDTO logResult = new LogDTO();
		try {
			result = joinPoint.proceed();

		} catch (Exception e) {
			// TODO: handle exception
			logResult.setResult(LogDTO.RESULT_FAILED);
			throw e;
		} finally {
			long endTime = System.currentTimeMillis();
			long timeTaken = endTime - startTime;
			Object[] arguments = joinPoint.getArgs();
			MethodSignature methodSignature = (MethodSignature) joinPoint.getStaticPart().getSignature();
			Method method = methodSignature.getMethod();
			logResult.setMethodName(method.getName());
			Parameter[] params = method.getParameters();

			Map<String, Object> logParams = new HashMap<>();
			for (int i = 0; i < params.length; i++) {
				String paramName = params[i].getName();
				if (paramName.toLowerCase().contains("token") || paramName.toLowerCase().contains("password")) {
					logParams.put(paramName, "******");
					continue;
				}

				logParams.put(paramName, arguments[i]);

			}
			logResult.setParams(logParams);
			logResult.setTimeStarted(new Date(startTime));
			logResult.setTimeEnd(new Date(endTime));
			logResult.setDuration(timeTaken);
			logResult.setRefId(LogRefIdHolder.get());
			// logResult.setUsername(AppUtil.getCurrentUsernameLogin());
			LOGGER.info("AccountService  - LogInfo by {}  is returned    {}  ", joinPoint,
					objectMapper.writeValueAsString(logResult));
			if (result != null && !(result instanceof InputStream)) {
				LOGGER.info(">>> OUTPUT: {} => {}", LogRefIdHolder.get(), objectMapper.writeValueAsString(result));
			}
		}
		return result;

	}
}
