package com.ocb.oma.account.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.CampaignService;
import com.ocb.oma.dto.CampaignInfo;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/campaign")
public class CampaignController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(CampaignController.class);

	@Autowired
	private CampaignService campaignService;;

	@RequestMapping(value = "/getCampaignList")
	public List<CampaignInfo> countBasketPaymentToProcess(HttpServletRequest request, @RequestParam("customerCif") String customerCif,
			@RequestParam("username") String username) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);

		return campaignService.getCampaignList(customerCif, username, token);
	}

}
