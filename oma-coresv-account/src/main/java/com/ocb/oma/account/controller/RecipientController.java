package com.ocb.oma.account.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.RecipientService;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.exception.OmaException;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/recipient")
public class RecipientController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(RecipientController.class);

	@Autowired
	private RecipientService recipientService;

	@RequestMapping(value = "/getRecipientList")
	public List<RecipientDTO> getRecipientList(HttpServletRequest request,
			@RequestParam("filerTemplateType") String filerTemplateType, @RequestParam("pageSize") int pageSize) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		List<RecipientDTO> response = recipientService.getRecipientList(token, filerTemplateType, pageSize);
		return response;
	}

	@RequestMapping(value = "/deleteRecipient")
	public Boolean deleteRecipient(HttpServletRequest request, @RequestBody RecipientDTO recipientDTO) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		return recipientService.deleteRecipient(token, recipientDTO);
	}

	@RequestMapping(value = "/addRecipient")
	public AddRecipientOutputDTO addRecipient(HttpServletRequest request,
			@RequestBody RecipientInputDTO recipientInputDTO) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		return recipientService.addRecipient(token, recipientInputDTO);
	}

}
