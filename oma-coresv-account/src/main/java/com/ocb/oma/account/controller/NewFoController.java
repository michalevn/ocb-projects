package com.ocb.oma.account.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.NewFoService;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/newfo")
public class NewFoController extends BaseController {

	private Logger LOGGER = LoggerFactory.getLogger(NewFoController.class);

	@Autowired
	private NewFoService newFoService;

	@RequestMapping(value = "/registerBankingProductOnline")
	public NewFoTransactionInfo registerBankingProductOnline(@RequestBody NewFoBankingProductOnline bankingProduct) {
		LOGGER.info("registerBankingProductOnline");
		return newFoService.registerBankingProductOnline(bankingProduct);
	}
}
