package com.ocb.oma.account.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.AccountService;
import com.ocb.oma.account.service.AuthenticationService;
import com.ocb.oma.account.service.OtpService;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/account")
public class AccountController extends BaseController {

	@Autowired
	private AuthenticationService authenticateService;

	@Autowired
	private OtpService otpService;

	@Autowired
	private AccountService accountService;

	@RequestMapping(value = "/login")
	public UserToken login(@RequestBody Map<String, String> postData) {
		String username = postData.get("username");
		String password = postData.get("password");
		String deviceId = postData.get("deviceId");
		return authenticateService.loginAndLoadUserInfo(username, password, deviceId);
	}

	@RequestMapping(value = "/test")
	public ResponseEntity<?> testAccount(@RequestParam("name") String name) {
		return getFinalResponse("hello " + name);
	}

	@RequestMapping(value = "/sendAuthorizationSmsOtp")
	public AuthorizationResponse sendAuthorizationSmsOtp(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (token == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		AuthorizationResponse response = otpService.sendAuthorizationSmsOtp(token);
		return response;
	}

	// getAccountList

	@RequestMapping(value = "/getAccountList")
	public List<AccountInfo> getAccountList(@RequestParam("customerCif") String customerCif,
			HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getAccountList(customerCif, token);

	}

	@RequestMapping(value = "/findAccountDetail")
	public AccountDetails findAccountDetail(@RequestParam("accountNum") String accountNum) {
		return accountService.findAccountDetail(accountNum);

	}

	@RequestMapping(value = "/getCustomerDetail")
	public CustomerInfoDTO getCustomerDetail(HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getCustomerDetail(token);

	}

	@RequestMapping(value = "/updateAccountAvatar")
	public Boolean updateAccountAvatar(HttpServletRequest request, @RequestBody Map<String, String> data) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.updateAccountIcon(data.get("accountNo"), data.get("customerCif"), token,
				data.get("imageLink"));

	}

	@RequestMapping(value = "/updateAccountName")
	public Boolean updateAccountName(HttpServletRequest request, @RequestBody Map<String, String> data) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.updateAccountName(data.get("accountNo"), data.get("customerCif"), token,
				data.get("accountName"));
	}

	@RequestMapping(value = "/findTransactionHistory")
	public List<TransactionHistoryDTO> findTransactionHistory(HttpServletRequest request,
			@RequestBody TransHistoryRequestDTO transRequest) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.findTransactionHistory(token, transRequest.getAccountNo(), null,
				transRequest.getFromDate(), transRequest.getToDate(), transRequest.getFromAmount(),
				transRequest.getToAmount(), transRequest.getPage(), transRequest.getPageSize());
	}

	@RequestMapping(value = "/loadAccountSummarByCurrencies")
	public AccountSummaryDTO loadAccountSummarByCurrencies(@RequestParam("accountId") String accountId,
			@RequestParam("summaryPeriod") Integer summaryPeriod, HttpServletRequest request) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.loadAccountSummarByCurrencies(token, accountId, summaryPeriod);
	}

	@RequestMapping(value = "/getAccountLimit")
	public AccountLimitDTO getAccountLimit(HttpServletRequest request,
			@RequestParam("paymentType") String paymentType) {

		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getAccountLimnit(paymentType, token);
	}

	@RequestMapping(value = "/getAccountNameByAccountNo")
	public String getAccountNameByAccountNo(@RequestParam("accountNo") String accountNo, HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getAccountNameByAccountNo(accountNo, token);

	}
	// public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token)

	@RequestMapping(value = "/getUpcomingRepaymentDeposit")
	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getUpcomingRepaymentDeposit(token);

	}

	@RequestMapping(value = "/getBlockedTransaction")
	public List<TransactionBlockedDTO> getBlockedTransaction(@RequestParam("accountId") String accountId,
			@RequestParam("currentPage") Integer pageNumber, @RequestParam("pageSize") Integer pageSize,
			HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getBlockedTransaction(accountId, pageNumber, pageSize, token);

	}

	@RequestMapping(value = "/getAccountNameNapas")
	public String getAccountNameByAccountNo(@RequestParam(name = "accountNo", required = true) String accountNo,
			@RequestParam(name = "bankCode", required = true) String bankCode,
			@RequestParam(name = "cardNumber", required = true) String cardNumber,
			@RequestParam(name = "debitAccount", required = true) String debitAccount, HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getAccountNameNapas(accountNo, bankCode, cardNumber, debitAccount, token);

	}

	@RequestMapping(value = "/getAvatarInfo")
	public AvatarInfoOutputDTO getAvatarInfo(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getAvatarInfo(token);

	}

	@RequestMapping(value = "/changePassword")
	public ChangePasswordDTO changePassword(HttpServletRequest request, @RequestBody Map<String, String> data) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return authenticateService.changePassword(data.get("oldPassword"), data.get("newPassword"),
				data.get("customerId"), token);
	}

	@RequestMapping(value = "/getCifByAccountNo")
	public String getCifByAccountNo(HttpServletRequest request, @RequestParam("accountNo") String accountNo) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.getCifByAccountNo(accountNo, token);
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getCIFFromAccountList")
	public Map<String, String> getCIFFromAccountList(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
			@RequestBody Map<String, Object> params) {
		List<String> lstAccountNum = (List<String>) params.get("lstAccountNum");
		return accountService.getCIFFromAccountList(lstAccountNum, token);
	}

	@RequestMapping(value = "/getPersonalInfo")
	public CustomerPersonalInfoDTO getPersonalInfo(@RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
		return accountService.getPersonalInfo(token);
	}

	@RequestMapping(value = "/uploadAvatar")
	public AvatarInfoOutputDTO uploadAvatar(HttpServletRequest request, @RequestBody Map<String, String> data) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		String file = data.get("file");
		return accountService.uploadAvatar(token, file);

	}

	@RequestMapping(value = "/updateProductDefaultDirectly")
	public Boolean updateProductDefaultDirectly(@RequestBody Map<String, String> map) {
		return accountService.updateProductDefaultDirectly(map);
	}

	@RequestMapping(value = "/transferAccounts")
	public List<TransferAccountDTO> transferAccounts(HttpServletRequest request,
			@RequestParam("productList") String productList, @RequestParam("restrictions") String restrictions) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.transferAccounts(productList, restrictions, token);
	}

	@RequestMapping(value = "/changeUsername")
	public ChangeUsernameOutput changeUsername(HttpServletRequest request,
			@RequestBody ChangeUsernameInput changeUsername) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION);
		return accountService.changeUsername(changeUsername, token);
	}
}
