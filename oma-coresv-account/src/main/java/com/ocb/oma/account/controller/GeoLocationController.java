package com.ocb.oma.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.account.service.GeoLocationService;
import com.ocb.oma.dto.GeoLocationPoiDTO;

/**
 * 
 * @author docv
 *
 */
@RestController
@RequestMapping("/geo-locations")
public class GeoLocationController extends BaseController {

	@Autowired
	private GeoLocationService geoLocationService;

	@RequestMapping(value = "/find")
	public GeoLocationPoiDTO find(@RequestParam("lat") Double lat, @RequestParam("lng") Double lng,
			@RequestParam("poiType") String poiType) {
		return geoLocationService.find(lat, lng, poiType);
	}

}
