/**
 * 
 */
package com.ocb.oma.account.service;

import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;

/**
 * @author docv
 *
 */
public interface NewFoService {

	public NewFoTransactionInfo registerBankingProductOnline(NewFoBankingProductOnline bankingProduct);
}
