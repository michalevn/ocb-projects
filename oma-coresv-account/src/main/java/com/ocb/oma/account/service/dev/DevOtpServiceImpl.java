/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.OtpService;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.AmountDTO;
import com.ocb.oma.oomni.dto.BillInfoDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
import com.ocb.oma.oomni.dto.ServiceDTO;
import com.ocb.oma.oomni.dto.ServiceProviderDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevOtpServiceImpl implements OtpService {

	private String defaultOtpCode = "123456789";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#sendAuthorizationSmsOtp(com.ocb.oma.
	 * dto.input.SendAuthorizationSmsOtpDTO, java.lang.String)
	 */
	@Override
	public AuthorizationResponse sendAuthorizationSmsOtp(String token) {
		// TODO Auto-generated method stub

		// test data
		AuthorizationResponse response = new AuthorizationResponse();
		response.setAuthId(defaultOtpCode);
		response.setExpiryTime(1000L);
		response.setOperationDate(new Date());
		response.setOperationStatus(AuthorizationResponse.EXECUTED);
		// data.set
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#verifyOTPHWToken(com.ocb.oma.dto.input
	 * .VerifyOTPHWToken, java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data, String token) {
		// TODO Auto-generated method stub
		AuthorizationResponse response = new AuthorizationResponse();
		if (data.getOtpValue().length() == 8) {
			response.setOperationStatus(AuthorizationResponse.CORRECT);
		} else {
			response.setOperationStatus(AuthorizationResponse.INCORRECT);
		}

		// data.set
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.OtpService#AuthorizationResponse(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifyHwOTPToken(String hwToken, String token) {
		// TODO Auto-generated method stub
		AuthorizationResponse response = new AuthorizationResponse();
		if (hwToken.equals(defaultOtpCode)) {
			response.setOperationStatus(AuthorizationResponse.CORRECT);
		} else {
			response.setOperationStatus(AuthorizationResponse.INCORRECT);
		}
		// data.set
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#getChallengeCodeSoftOTP(com.ocb.oma.
	 * dto.input.GetChallengeCodeInputDTO, java.lang.String)
	 */
	@Override
	public String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input, String token) {
		// TODO Auto-generated method stub
		return defaultOtpCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.OtpService#verifySoftOTP(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public AuthorizationResponse verifySoftOTP(PaymentInfoDTO paymentInfo, VerifyOtpToken otpCode, String token) {
		AuthorizationResponse response = new AuthorizationResponse();
		if (otpCode.getOtpValue().equals(defaultOtpCode)) {
			response.setOperationStatus(AuthorizationResponse.CORRECT);
		} else {
			response.setOperationStatus(AuthorizationResponse.INCORRECT);
		}
		// data.set
		return response;
	}

	@Override
	public List<String> getAuthorizationPolicies(String userId, String token) {
		// TODO Auto-generated method stub
		List<String> lstStr = new ArrayList<>();
		lstStr.add(OtpAuthorizeMethod.NONE.getCode());
		lstStr.add(OtpAuthorizeMethod.SMSOTP.getCode());
		lstStr.add(OtpAuthorizeMethod.PKI.getCode());
		lstStr.add(OtpAuthorizeMethod.HW.getCode());
		return lstStr;
	}

	@Override
	public List<QD630PoliciesDTO> getQD630Policies(String cif, String token) {
		// TODO Auto-generated method stub
		List<QD630PoliciesDTO> lstResult = new ArrayList<>();
		QD630PoliciesDTO dto = new QD630PoliciesDTO();
		dto.setRegisteredMethod("SMSOTP");
		dto.setFromAmount(20000D);
		dto.setToAmount(10000000D);
		dto.setAllowChangingMethod(false);
		lstResult.add(dto);

		QD630PoliciesDTO dto1 = new QD630PoliciesDTO();
		dto.setRegisteredMethod("HW");
		dto1.setFromAmount(10000001D);
		dto1.setToAmount(20000000D);
		dto1.setAllowChangingMethod(false);
		lstResult.add(dto1);

		QD630PoliciesDTO dto2 = new QD630PoliciesDTO();
		dto2.setRegisteredMethod("PKI");
		dto2.setFromAmount(20000D);
		dto2.setToAmount(10000000D);
		dto2.setAllowChangingMethod(false);
		lstResult.add(dto2);

		QD630PoliciesDTO dto3 = new QD630PoliciesDTO();
		dto3.setRegisteredMethod("PKI");
		dto3.setFromAmount(20000001D);
		dto3.setToAmount(100000000D);
		dto3.setAllowChangingMethod(false);
		lstResult.add(dto3);

		QD630PoliciesDTO dto4 = new QD630PoliciesDTO();
		dto4.setRegisteredMethod("SOFTOTP");
		dto4.setFromAmount(100000001D);
		dto4.setAllowChangingMethod(false);
		lstResult.add(dto4);

		return lstResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.OtpService#getListPrepaidPhone(java.lang.String)
	 */
	@Override
	public List<PrepaidPhoneDTO> getListPrepaidPhone(String token) {

		List<PrepaidPhoneDTO> lst = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			PrepaidPhoneDTO dto = new PrepaidPhoneDTO();
			dto.setId(i);
			dto.setPhoneName(DevGeneratedDataUtil.generatePhoneNames());
			dto.setPhoneNumber(DevGeneratedDataUtil.generatePhoneNumbers());
			lst.add(dto);
		}
		return lst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.OtpService#checkQRcodeInfo(java.lang.String,
	 * com.ocb.oma.dto.input.QRcodeInfoInputDTO)
	 */
	@Override
	public QRcodeInfoOutputDTO checkQRcodeInfo(String token, QRcodeInfoInputDTO input) {
		// TODO Auto-generated method stub
//		throw new NotImplementedException();
		QRcodeInfoOutputDTO dto = new QRcodeInfoOutputDTO();
		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);
		BillInfoDTO billInfo = new BillInfoDTO();
		billInfo.setAdditionalData("additionalData");
		billInfo.setAddress("address");
		AmountDTO amount= new AmountDTO();
		amount.setValue(25000D);
		billInfo.setAmount(amount);
		billInfo.setCustomerName("customerName");
		billInfo.setPromotionValue(15000D);
		ServiceProviderDTO serviceProvider= new ServiceProviderDTO();
		serviceProvider.setProviderCode("providerCode");
		ServiceDTO service = new ServiceDTO();
		service.setServiceName("serviceName");
		serviceProvider.setService(service);
		billInfo.setServiceProvider(serviceProvider);
		dto.setBillInfo(billInfo);
		return dto;
	}

}
