/**
 * 
 */
package com.ocb.oma.account.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_MOB_BLOCKED")
public class OmMobileBlocked extends AbstractEntity implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "MOB_BLOCKED_ID", unique = true)
	@Id
	@SequenceGenerator(name = "MObileBlockedID", sequenceName = "SEQ_MObileBlockedID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MObileBlockedID")
	private Long mobileBlockedId;

	@Column(name = "mobile_device")
	private String mobileDevice;

	/**
	 * @return the mobileBlockedId
	 */
	public Long getMobileBlockedId() {
		return mobileBlockedId;
	}

	/**
	 * @param mobileBlockedId the mobileBlockedId to set
	 */
	public void setMobileBlockedId(Long mobileBlockedId) {
		this.mobileBlockedId = mobileBlockedId;
	}

	/**
	 * @return the mobileDevice
	 */
	public String getMobileDevice() {
		return mobileDevice;
	}

	/**
	 * @param mobileDevice the mobileDevice to set
	 */
	public void setMobileDevice(String mobileDevice) {
		this.mobileDevice = mobileDevice;
	}

}
