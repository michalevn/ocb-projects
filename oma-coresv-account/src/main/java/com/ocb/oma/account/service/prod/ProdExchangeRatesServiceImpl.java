/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ocb.oma.account.service.ExchangeRatesService;
import com.ocb.oma.dto.ExchangeRateDTO;

/**
 * @author docv
 *
 */
@Service
@Profile("prod")
public class ProdExchangeRatesServiceImpl extends ProdServiceImpl implements ExchangeRatesService {

	@Override
	public List<ExchangeRateDTO> getExchangeRates() {
		String path = "/api/exchange_rates/search/get_exchange_rates";
		return getGetContentAsList(path, null, ExchangeRateDTO.class);
	}

}
