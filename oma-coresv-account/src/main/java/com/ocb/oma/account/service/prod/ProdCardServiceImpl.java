/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.compiler.PluginProtos.CodeGeneratorResponse.File;
import com.ocb.oma.account.service.CardService;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdCardServiceImpl extends ProdServiceImpl implements CardService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RestTemplate restTemplate;

	private static final Log LOG = LogFactory.getLog(ProdCardServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.CardService#findCard(java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<CardInfoDTO> findCard(String token) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		List<CardInfoDTO> cardInfoDTOs = new ArrayList<>();
		String url = host + "/api/card";

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			if (data.containsKey("content")) {
				List<Map> list = (List<Map>) data.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						CardInfoDTO cardInfoDTO = objectMapper.convertValue(map, CardInfoDTO.class);
						cardInfoDTOs.add(cardInfoDTO);
					}
				}
			}
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("findCard has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
		return cardInfoDTOs;
	}

	@Override
	public OutputBaseDTO restrictCard(String token, String cardId, String cardRestrictionReason) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/actions/card_restriction_directly";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, Object> params = new HashMap<>();
			params.put("cardId", cardId);
			params.put("cardRestrictionReason", cardRestrictionReason);
			RequestEntity<Map> request = new RequestEntity<>(params, headers, HttpMethod.POST, URI.create(url));
			ResponseEntity<OutputBaseDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<OutputBaseDTO>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("restrictCard has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@Override
	public List<CardBlockade> findCardBlockades(String token, CardBlockadeInputDTO input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/search/card_blockades_compact";
		List<CardBlockade> dtos = new ArrayList<>();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<CardBlockadeInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> result = response.getBody();
			if (result.containsKey("content")) {
				List<Map> list = (List<Map>) result.get("content");
				if (!list.isEmpty()) {
					for (Map map : list) {
						CardBlockade dto = objectMapper.convertValue(map, CardBlockade.class);
						dtos.add(dto);
					}
				}
			}
			return dtos;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardStatements has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public void modifyCardAccountAutoRepayment(String token, CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		String path = "/api/card/actions/card_autorepayment_directly";
		OutputBaseDTO output = getPostResponse(path, cardAccountAutoRepayment, OutputBaseDTO.class);
		if (output != null && "1".equals(output.getResultCode())) {
			return;
		}
		throw new OmaException(MessageConstant.SERVER_OCP_ERROR, output.getResultMsg());
	}

	@Override
	public List<CardAccountEntryDTO> findCardAccountEntry(String token, CardAccountEntryQueryDTO query) {

		Map<String, Object> requestBody = new HashMap<>();

		requestBody.put("productId", query.getProductId());
		requestBody.put("pageNumber", query.getPageNumber());
		requestBody.put("pageSize", query.getPageSize());
		requestBody.put("operationType", query.getOperationType());
		requestBody.put("dateFrom", query.getDateFrom());
		requestBody.put("dateTo", query.getDateTo());
		requestBody.put("operationAmountFrom", query.getOperationAmountFrom());
		requestBody.put("operationAmountTo", query.getOperationAmountTo());

		String path = "/api/card_transaction_history";
		return getPostContentAsList(path, requestBody, CardAccountEntryDTO.class);
	}

	@Override
	public PaymentsInFutureDTO getPaymentDetails(String token, String paymentId) {
		if (StringUtils.isBlank(token)) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/payments/" + paymentId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<PaymentsInFutureDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<PaymentsInFutureDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(String token, PaymentCardAnotherInputDTO input) {
//		throw new NotImplementedException();
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/actions/card_payment";
		List<CardBlockade> dtos = new ArrayList<>();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<PaymentCardAnotherInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<PaymentCardAnotherOuputDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<PaymentCardAnotherOuputDTO>() {
					});
			return responseEntity.getBody();
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("postpaymentCardAnother has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}

	}

	@Override
	public CardDetailByCardIdDTO findCardByCardId(String token, String cardId) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/" + cardId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url.trim()));
			ResponseEntity<CardDetailByCardIdDTO> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<CardDetailByCardIdDTO>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("findCardByCardId has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<CardAutoRepaymentDTO> cardAutoRepayment(String token) {
		//
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card_auto_repayment";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<List<Map<String, Object>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> response = responseEntity.getBody();
			List<CardAutoRepaymentDTO> lst = new ArrayList<>();
			if (response.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(response.get("content")),
					new TypeReference<List<CardAutoRepaymentDTO>>() {
					});

			return lst;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardAutoRepayment has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public List<CardRejectedDTO> cardRejected(String token, CardRejectedInputDTO cardRejectedInput) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/search/card_rejected";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<CardRejectedInputDTO> request = new RequestEntity<>(cardRejectedInput, headers,
					HttpMethod.POST, URI.create(url));
			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			List<CardRejectedDTO> lst = new ArrayList<>();
			if (data.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<CardRejectedDTO>>() {
					});

			return lst;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardRejected has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public OutputBaseDTO postAuthActivateCard(String token, String cardId) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String path = "/api/card/actions/card_activation_directly";
		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("cardId", cardId);
		return getPostResponse(path, requestBody, OutputBaseDTO.class);
	}

	@Override
	public List<CardStatementsDTO> cardStatements(String token, CardStatementsInputDTO input) {
		if (token == null || input == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/search/card_statements";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			HttpEntity<CardStatementsInputDTO> entity = new HttpEntity<>(input, headers);

			RequestEntity<CardStatementsInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));

			ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = responseEntity.getBody();
			List<CardStatementsDTO> result = new ArrayList<>();
			if (data.get("content") == null) {
				return result;
			}
			result = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<CardStatementsDTO>>() {
					});

			return result;
		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardStatements has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public Boolean changeLimitDirectly(List<ChangeLimitDTO> limits) {
		Map<String, Object> requestBody = new HashMap<>();
		requestBody.put("limits", limits);
		String path = "/api/customer/actions/change_limit_directly";
		Map<String, Object> response = getPostResponse(path, requestBody);
		if (response.containsKey("resultCode") && response.get("resultCode").equals("1")) {
			return true;
		}
		LOG.info("=====> POST " + path);
		LOG.info(response);
		return false;
	}

	@Override
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(String token,
			ConfigStatusEcommerceCardInput input) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		String path = "/api/card/actions/config_status_ecommerce_card";
		String url = host + path;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<ConfigStatusEcommerceCardInput> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<ConfigStatusEcommerceCardOutput> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<ConfigStatusEcommerceCardOutput>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("configStatusEcommerceCard has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public CardPaymentInfoOutput cardPaymentInfo(String token, CardPaymentInfoInput input) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}

		String path = "/api/card/get/card_payment_info";
		String url = host + path;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<CardPaymentInfoInput> request = new RequestEntity<>(input, headers, HttpMethod.POST,
					URI.create(url));
			ResponseEntity<CardPaymentInfoOutput> responseEntity = restTemplate.exchange(request,
					new ParameterizedTypeReference<CardPaymentInfoOutput>() {
					});
			return responseEntity.getBody();

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardPaymentInfo has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public InputStream cardStatemenDownload(String token, String linkStatementFile) {
//		throw new NotImplementedException();
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/card/downloads/card_statement_download.json?downloadLink=" + linkStatementFile;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_PDF));
			headers.setContentType(MediaType.APPLICATION_PDF);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));
			ResponseEntity<Resource> responseEntity = restTemplate.exchange(request, Resource.class);
			InputStream responseInputStream;
			try {
				responseInputStream = responseEntity.getBody().getInputStream();
				return responseInputStream;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOG.error("cardAutoRepayment has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}


}
