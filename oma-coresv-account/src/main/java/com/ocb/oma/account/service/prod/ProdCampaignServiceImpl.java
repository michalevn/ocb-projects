/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.CampaignService;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdCampaignServiceImpl implements CampaignService {
	
	@Value("${omni.url}")
	private String host;
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.CampaignService#getCampaignList(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<CampaignInfo> getCampaignList(String customerCif, String username, String token) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if (customerCif == null || token == null || username == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/ocb_campaign/get/ocb_load_campaigns?customerId=" + customerCif + "&userId=" + username + "";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request, new ParameterizedTypeReference<Map<String, Object>>() {
			});
			Map<String, Object> data = response.getBody();
			List<CampaignInfo> lst = new ArrayList<>();
			if (data.get("content") == null) {
				return lst;
			}
			lst = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")), new TypeReference<List<CampaignInfo>>() {
			});
			return lst;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("login into omni has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
		// return null;
	}

}
