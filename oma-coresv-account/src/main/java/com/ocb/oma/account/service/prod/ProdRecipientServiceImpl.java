/**
 * 
 */
package com.ocb.oma.account.service.prod;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.RecipientService;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RecipientOCBDTO;
import com.ocb.oma.dto.RecipientType;
import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("prod")
public class ProdRecipientServiceImpl implements RecipientService {

	@Value("${omni.url}")
	private String host;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RestTemplate restTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.RecipientService#getRecipientList(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public List<RecipientDTO> getRecipientList(String token, String filerTemplateType, int pageSize) {
		if (token == null) {
			throw new OmaException(MessageConstant.PARAM_CAN_NOT_BE_NULL);
		}
		String url = "";
		if (filerTemplateType != null && filerTemplateType.isEmpty() == false) {
			if (filerTemplateType.equals(PaymentType.InternalPayment.getPaymentTypeCode())) {

				filerTemplateType = "INTERNAL";
			}
			if (filerTemplateType.equals(PaymentType.LocalPayment.getPaymentTypeCode())) {
				filerTemplateType = "EXTERNAL";
			}
			if (filerTemplateType.equals(PaymentType.FastTransfer.getPaymentTypeCode())) {
				filerTemplateType = "FAST";
			}
		}

		if (filerTemplateType.equals("") && pageSize == 0) {
			url = host + "/api/recipient";
		}
		if (pageSize != 0 && filerTemplateType == "") {
			url = host + "/api/recipient?pageSize=" + pageSize;
		}
		if (filerTemplateType != null && pageSize == 0) {
			url = host + "/api/recipient?filerTemplateType=" + filerTemplateType;
		}
		if (filerTemplateType != "" && pageSize != 0) {
			url = host + "/api/recipient?filerTemplateType=" + filerTemplateType + "&pageSize=" + pageSize;
		}

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);

			RequestEntity<List<Map<String, String>>> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(url));

			ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
					new ParameterizedTypeReference<Map<String, Object>>() {
					});
			Map<String, Object> data = response.getBody();
			List<RecipientOCBDTO> result = new ArrayList<>();
			List<RecipientDTO> resultEnd = new ArrayList<>();

			if (data.get("content") == null) {
				return resultEnd;
			}
			result = objectMapper.readValue(objectMapper.writeValueAsString(data.get("content")),
					new TypeReference<List<RecipientOCBDTO>>() {
					});
			String paymentType;
			for (RecipientOCBDTO recipientOCBDTO : result) {
				RecipientDTO dto = new RecipientDTO();
				dto.setAmount(recipientOCBDTO.getPaymentTemplates().get(0).getAmount());
				dto.setBankCode(recipientOCBDTO.getPaymentTemplates().get(0).getBankCode());
				dto.setBankName(recipientOCBDTO.getBankName());
				dto.setBeneficiaryAccountNo(recipientOCBDTO.getPaymentTemplates().get(0).getBeneficiaryAccountNo());
				dto.setBranchCode(recipientOCBDTO.getPaymentTemplates().get(0).getBranchCode());
				dto.setCardNumber(recipientOCBDTO.getPaymentTemplates().get(0).getCardNumber());
				dto.setCurrency(recipientOCBDTO.getPaymentTemplates().get(0).getCurrency());
				paymentType = recipientOCBDTO.getPaymentTemplates().get(0).getTemplateType();
				if (paymentType.equals("INTERNAL")) {
					paymentType = PaymentType.InternalPayment.getPaymentTypeCode();
				}
				if (paymentType.equals("EXTERNAL")) {
					paymentType = PaymentType.LocalPayment.getPaymentTypeCode();
				}
				if (paymentType.equals("FAST")) {
					paymentType = PaymentType.FastTransfer.getPaymentTypeCode();
				}
				dto.setPaymentType(paymentType);
				dto.setProvinceId(recipientOCBDTO.getPaymentTemplates().get(0).getProvince());
				if (recipientOCBDTO.getRecipientAddress() == null) {
					dto.setRecipientAddress(null);
				} else {
					dto.setRecipientAddress(recipientOCBDTO.getRecipientAddress()[0]);
				}

				dto.setRecipientId(recipientOCBDTO.getRecipientId());

				if (recipientOCBDTO.getRecipientName() == null) {
					dto.setRecipientName(null);

				} else {
					dto.setRecipientName(recipientOCBDTO.getRecipientName()[0]);
				}
				dto.setRemitterAccountNo(recipientOCBDTO.getPaymentTemplates().get(0).getRemitterAccountNo());

				resultEnd.add(dto);
			}

			return resultEnd;

		} catch (Exception e) {
			LOGGER.error("getRecipientList has occurred error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.RecipientService#deleteRecipient(java.lang.
	 * String, com.ocb.oma.dto.RecipientDTO)
	 */
	@Override
	public Boolean deleteRecipient(String token, RecipientDTO recipient) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/recipient/actions/remove_recipient";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			Map<String, Object> input = new HashMap<String, Object>();
			input.put("recipientId", recipient.getRecipientId());

			HttpEntity<Map> entity = new HttpEntity<>(input, headers);

			OutputBaseDTO result = new OutputBaseDTO();
			result = restTemplate.postForObject(url, entity, OutputBaseDTO.class);
			if (result.getResultCode().equals("1")) {

				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("deleteRecipient error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

	@Override
	public AddRecipientOutputDTO addRecipient(String token, RecipientInputDTO recipientInputDTO) {
		if (token == null) {
			throw new OmaException(MessageConstant.MISSING_PARAMETER);
		}
		String url = host + "/api/recipient/actions/add_recipient";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
			if (recipientInputDTO.getRecipientType().equals(PaymentType.InternalPayment.getPaymentTypeCode())) {
				recipientInputDTO.setRecipientType(RecipientType.INTERNAL.getRecipientTypeCode());
			}
			if (recipientInputDTO.getRecipientType().equals(PaymentType.LocalPayment.getPaymentTypeCode())) {
				recipientInputDTO.setRecipientType(RecipientType.EXTERNAL.getRecipientTypeCode());
			}
			if (recipientInputDTO.getRecipientType().equals(PaymentType.FastTransfer.getPaymentTypeCode())) {
				recipientInputDTO.setRecipientType(RecipientType.FAST.getRecipientTypeCode());
			}

			HttpEntity<RecipientInputDTO> entity = new HttpEntity<>(recipientInputDTO, headers);
			AddRecipientOutputDTO result = restTemplate.postForObject(url, entity, AddRecipientOutputDTO.class);

			return result;

		} catch (Exception e) {
			if (e instanceof OmaException) {
				throw (OmaException) e;
			}
			LOGGER.error("add_recipient error ", e);
			throw new OmaException(MessageConstant.SERVER_OCP_ERROR, e.getMessage());
		}
	}

}
