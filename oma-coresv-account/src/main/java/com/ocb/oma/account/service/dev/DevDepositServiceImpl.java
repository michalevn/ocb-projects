/**
 * 
 */
package com.ocb.oma.account.service.dev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.account.service.DepositService;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.OwnersDTO;
import com.ocb.oma.oomni.dto.AccountCapitalDTO;
import com.ocb.oma.oomni.dto.AccountInterestDTO;
import com.ocb.oma.oomni.dto.AccountSourceDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * @author phuhoang
 *
 */
@Service
@Profile("dev")
public class DevDepositServiceImpl implements DepositService {

	@Autowired
	private ObjectMapper objectMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.DepositService#breakDeposit(com.ocb.oma.dto.input
	 * .BreakDepositInputDTO, java.lang.String)
	 */
	@Override
	public OutputBaseDTO breakDeposit(String depositId, String token) {
		// TODO Auto-generated method stub
		OutputBaseDTO output = new OutputBaseDTO();
		if (depositId.substring(0, 3).equals("OCB") || depositId.length() == 16) {
			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (depositId.substring(0, 3).equals("VCB")) {
			output.setResultCode("06");
			output.setResultMsg("can_not_find_data");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}

		return output;
	}

	@Override
	public List<DepositDTO> getDeposits(String token) {
		List<DepositDTO> lst = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			DepositDTO depositDTO = new DepositDTO();
			depositDTO.setDepositId("78037" + i);
			depositDTO.setDepositName("Tich luy dien tu KHCN");
			depositDTO.setDescription(DevGeneratedDataUtil.generateDescription());
			depositDTO.setAccountNo(DevGeneratedDataUtil.generateAccountNo());
			depositDTO.setCurrency(DevGeneratedDataUtil.generateCurrency());
			depositDTO.setDepositBalance(DevGeneratedDataUtil.nextDouble(1000, 50000));
			depositDTO.setNextCapitalizationDate(new Date(1543309380L + i));
			depositDTO.setMaturityDate(new Date(1545757200000L + i));
			depositDTO.setOpeningAccountNo(DevGeneratedDataUtil.generateAccountNo());
			depositDTO.setSettlementAccountNo(DevGeneratedDataUtil.generateAccountNo());
			depositDTO.setSettlementAccountIntNo(DevGeneratedDataUtil.generateAccountId());
			depositDTO.setRenewalOptionType("renewalOptionType");
			depositDTO.setDepositPeriodType("DepositPeriodType");
			depositDTO.setPeriod(1L + i);
			depositDTO.setInterest(5.4 + (23 * i));
			depositDTO.setInterestRateType("interestRateType");
			depositDTO.setTotalInterestAmount(DevGeneratedDataUtil.generateAmount() * 13);
			depositDTO.setDepositMigrationName(DevGeneratedDataUtil.generateBranchName());
			depositDTO.setDepositMigrationContractNumber("1121234" + i);
			depositDTO.setAutoRolloverChangePos(DevGeneratedDataUtil.nextBoolean());
			depositDTO.setInterestPaymentMethod("InterestPaymentMethod" + i);
			depositDTO.setSubProductCode("TICHLUYDIENTU");
			depositDTO.setOpenDate(new Date(1543165200000L + i));
			depositDTO.setProductType("productType");
			depositDTO.setSurcharge(DevGeneratedDataUtil.nextBoolean());
			depositDTO.setNextSurchargeDate(new Date(1517043780L + i));
			depositDTO.setNextSurchargeAmount(DevGeneratedDataUtil.nextDouble(120000, 2500000));
			depositDTO.setLastSurchargeAmount(DevGeneratedDataUtil.nextDouble(2000, 89700));

			List<OwnersDTO> ownersList = new ArrayList<>();
			for (int j = 0; j < 5; j++) {
				OwnersDTO ownersDTO = new OwnersDTO();
				ownersDTO.setAddressStreet(DevGeneratedDataUtil.generateProvinceCodes());
				ownersDTO.setAddressStreetPrefix(DevGeneratedDataUtil.generateBranchName());
				ownersDTO.setApartmentNo("apartmentNo00" + i + 1);
				ownersDTO.setCountry("country0" + i + 1);
				ownersDTO.setCustomerId("customerId00" + 1 + i);
				ownersDTO.setFullName(DevGeneratedDataUtil.generateNameOfUser());
				ownersDTO.setHouseNo("HS0" + i + 3 * i);
				ownersDTO.setName(DevGeneratedDataUtil.generatePhoneNames());
				ownersDTO.setPostCode(DevGeneratedDataUtil.generateBranchCode());
				ownersDTO.setRelationType("relationType");
				ownersDTO.setResidence("residence");
				ownersDTO.setTown("town" + i + 1);
				ownersList.add(ownersDTO);
			}
			depositDTO.setOwnersList(ownersList);
			depositDTO.setUnfinishedDisposition(DevGeneratedDataUtil.nextBoolean());
			depositDTO.setTypeCapableToBreak(DevGeneratedDataUtil.nextBoolean());
			depositDTO.setLastTransactionBookingDate("1606528354");
			depositDTO.setDepositContractNumber("003760004037500" + i);
			depositDTO.setCustomName("Ten So TK " + i);
			depositDTO.setCustomIcon("DEPOSIT@78037@1731994001731994");
			depositDTO.setInstructionOnDispensation("instructionOnDispensation");
			depositDTO.setPeriodFrequency("periodFrequency");
			depositDTO.setDateFirstExecution(new Date(1606528354l));
			depositDTO.setWorkingAvailableFunds(123000D * i);
			depositDTO.setPrematureInterestRate(1000D + (i + 1));
			depositDTO.setIbRedeemDepositPos(DevGeneratedDataUtil.nextBoolean());
			depositDTO.setStatus("CUR");
			depositDTO.setDepositType("Online");
			depositDTO.setSurcharge(DevGeneratedDataUtil.nextBoolean());
			lst.add(depositDTO);
		}
		return lst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.DepositService#getDepositProducts(java.lang.
	 * String)
	 */
	@Override
	public List<DepositProductDTO> getDepositProducts(String token) {
		String jsonInput = "[{\"subProductCode\":\"TICHLUYDIENTU\",\"currency\":\"VND\",\"categoryCode\":\"6704\",\"description\":\"Tien gui trực tuyến\"}]";
		try {
			List<DepositProductDTO> depositProductDTOs = objectMapper.readValue(jsonInput,
					new TypeReference<List<DepositProductDTO>>() {
					});
			return depositProductDTOs;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.DepositService#getDepositInterestRates(java.lang.
	 * String)
	 */
	@Override
	public List<DepositInterestRateDTO> getDepositInterestRates(String currency, String subProductCode, String token) {
		String jsonInput = "[{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":7,\"periodUnit\":\"D\",\"currency\":\"VND\",\"interest\":1,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":14,\"periodUnit\":\"D\",\"currency\":\"VND\",\"interest\":1,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":21,\"periodUnit\":\"D\",\"currency\":\"VND\",\"interest\":1,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":1,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":5.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":2,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":5.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":3,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":5.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":4,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":5.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":5,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":5.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":6,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":6.6,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":7,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":6.7,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":8,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":6.8,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":9,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":6.9,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":10,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":11,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.1,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":12,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.5,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":18,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.7,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":21,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.7,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":24,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.8,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}},{\"subProductCode\":\"TICHLUYDIENTU\",\"accumulative\":false,\"description\":\"Tiền gửi trực tuyến\",\"term\":36,\"periodUnit\":\"M\",\"currency\":\"VND\",\"interest\":7.9,\"autoRenewal\":false,\"minAmount\":100000,\"maxAmount\":100000000000,\"interestType\":\"FIXED\",\"interestPaymentMethod\":\"IN_ADVANCE\",\"campaignId\":\"null\",\"initialAmountLimit\":100000,\"periodicAmountLimit\":100000,\"_links\":{}}]\"";
		try {
			List<DepositInterestRateDTO> depositInterestRates = objectMapper.readValue(jsonInput,
					new TypeReference<List<DepositInterestRateDTO>>() {
					});
			return depositInterestRates;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.account.service.DepositService#createDeposit(java.lang.String,
	 * com.ocb.oma.dto.DepositDTO)
	 */
	@Override
	public CreateDepositOutputDTO createDeposit(String token, CreateDepositDTO deposit) {
		CreateDepositOutputDTO dto = new CreateDepositOutputDTO();
//		1: thanh cong
//		1000: tai khoan debit ko hop le
//		Khac: Loi he thong

		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);
		dto.setNumOfSuccess(1);
		dto.setNumOfFailure(0);
		dto.setLstDepositNumber(new String[] { "0030080009768" });
		return dto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.account.service.DepositService#getDepositOfferAcc(java.lang.
	 * String)
	 */
	@Override
	public DepositOfferAccountsDTO getDepositOfferAcc(String token) {
		DepositOfferAccountsDTO accountsDTO = new DepositOfferAccountsDTO();
		List<AccountSourceDTO> accountSourceList = new ArrayList<>();
		for (int i = 1; i <= 3; i++) {
			AccountSourceDTO accountSourceDTO = new AccountSourceDTO();
			accountSourceDTO.setAccountId("4" + i);
			accountSourceDTO.setAccessibleAssets(1000000D * i);
			accountSourceDTO.setProductType("ACCOUNT");
			accountSourceDTO.setAccountName("TG Th.toan ATM");
			accountSourceDTO.setAccountNo("0037100003605005");
			accountSourceDTO.setAccessibleAssets(9480600684D);
			accountSourceDTO.setCurrentBalance(9480600684D);
			accountSourceDTO.setAllowedLimit(0D + i);
			accountSourceDTO.setAllowedLimitCurrency(DevGeneratedDataUtil.generateCurrency());
			accountSourceDTO.setDescription("description");
			accountSourceDTO.setOpenDate(null);
			accountSourceDTO.setBlockAssets(null);
			accountSourceDTO.setArrearAmount(null);
			accountSourceDTO.setOwnerName("ownerName");
			accountSourceDTO.setAccountInterest(0D + i);
			accountSourceDTO.setCreditInterest(null);
			accountSourceDTO.setCurrency(DevGeneratedDataUtil.generateCurrency());
			accountSourceDTO.setSettlementPeriodDtType(null);
			accountSourceDTO.setSettlementPeriodCtType("settlementPeriodCtType");
			accountSourceDTO.setSettlementPeriodDt(1000 * i);
			accountSourceDTO.setSettlementPeriodCt(1000 + i);
			accountSourceDTO.setNextCapitalizationDtDate(new Date());
			accountSourceDTO.setNextCapitalizationCtDate(new Date());
			accountSourceDTO.setLastTransactionBookingDate(new Date());
			accountSourceDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountSourceDTO.setOwnersList(ownersList);
			accountSourceDTO.setCategory(i + 1004);
			accountSourceDTO.setSubProduct("1004");
			accountSourceDTO.setRelation("OWNER");
			accountSourceDTO.setStatementDistributionType("statementDistributionType");
			String[] actions = {};
			accountSourceDTO.setActions(actions);

			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountSourceDTO.setAccountCategories(accountCategories);
			accountSourceDTO.setAccountRestrictFlag(DevGeneratedDataUtil.nextBoolean());
			String[] destAccountRestrictions = {};
			accountSourceDTO.setDestAccountRestrictions(destAccountRestrictions);
			accountSourceDTO.setOwnerContext("DETAL");
			accountSourceDTO.setAccountModifyStatus(null);
			accountSourceDTO.setExecutiveRestriction(false);
			accountSourceDTO.setOwnerGlobusId(801004000801004L);
			accountSourceDTO.setOpenBranch("Tan Binh " + i);
			accountSourceDTO.setCustomName("ACCOUNT801004");
			accountSourceDTO.setCustomIcon("");

			accountSourceList.add(accountSourceDTO);
		}
		accountsDTO.setAccountSourceList(accountSourceList);

		List<AccountCapitalDTO> accountCapitalList = new ArrayList<>();
		for (int i = 1; i <= 4; i++) {
			AccountCapitalDTO accountCapitalDTO = new AccountCapitalDTO();
			accountCapitalDTO.setAccountId("4" + i);
			accountCapitalDTO.setProductType("ACCOUNT");
			accountCapitalDTO.setAccountName("003710000360500" + i);
			accountCapitalDTO.setAccessibleAssets(9480600684D);
			accountCapitalDTO.setCurrentBalance(9480600684D);
			accountCapitalDTO.setAllowedLimit(null);
			accountCapitalDTO.setAllowedLimitCurrency("");
			accountCapitalDTO.setDescription("description");
			accountCapitalDTO.setOpenDate(null);
			accountCapitalDTO.setBlockAssets(null);
			accountCapitalDTO.setArrearAmount(null);
			accountCapitalDTO.setOwnerName(null);
			accountCapitalDTO.setAccountInterest(0D);
			accountCapitalDTO.setCreditInterest(null);
			accountCapitalDTO.setCurrency(DevGeneratedDataUtil.generateCurrency());
			accountCapitalDTO.setSettlementPeriodDtType(null);
			accountCapitalDTO.setSettlementPeriodCtType(null);
			accountCapitalDTO.setSettlementPeriodDt(0);
			accountCapitalDTO.setSettlementPeriodCt(i);
			accountCapitalDTO.setNextCapitalizationDtDate(null);
			accountCapitalDTO.setNextCapitalizationCtDate(null);
			accountCapitalDTO.setLastTransactionBookingDate(null);
			accountCapitalDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountCapitalDTO.setOwnersList(ownersList);
			accountCapitalDTO.setCategory(1004);
			accountCapitalDTO.setSubProduct("1004");
			accountCapitalDTO.setRelation("OWNER");
			accountCapitalDTO.setStatementDistributionType(null);
			accountCapitalDTO.setActions(null);
			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountCapitalDTO.setAccountCategories(accountCategories);
			accountCapitalDTO.setAccountRestrictFlag(false);
			accountCapitalDTO.setDestAccountRestrictions(null);
			accountCapitalDTO.setOwnerContext("DETAIL");
			accountCapitalDTO.setAccountModifyStatus(null);
			accountCapitalDTO.setExecutiveRestriction(false);
			accountCapitalDTO.setOwnerGlobusId(801004000801004L + i);
			accountCapitalDTO.setOpenBranch("TAn Binh " + i);
			accountCapitalDTO.setCustomName("ACCOUNT801004");
			accountCapitalDTO.setCustomIcon("");

			accountCapitalList.add(accountCapitalDTO);
		}
		accountsDTO.setAccountCapitalList(accountCapitalList);

		List<AccountInterestDTO> accountInterestList = new ArrayList<>();
		for (int i = 1; i <= 4; i++) {
			AccountInterestDTO accountInterestDTO = new AccountInterestDTO();
			accountInterestDTO.setAccountId("4" + i);
			accountInterestDTO.setProductType("ACCOUNT");
			accountInterestDTO.setAccountName("003710000360500" + i);
			accountInterestDTO.setAccessibleAssets(9480600684D);
			accountInterestDTO.setCurrentBalance(9480600684D);
			accountInterestDTO.setAllowedLimit(null);
			accountInterestDTO.setAllowedLimitCurrency("");
			accountInterestDTO.setDescription("description");
			accountInterestDTO.setOpenDate(null);
			accountInterestDTO.setBlockAssets(null);
			accountInterestDTO.setArrearAmount(null);
			accountInterestDTO.setOwnerName(null);
			accountInterestDTO.setAccountInterest(0D);
			accountInterestDTO.setCreditInterest(null);
			accountInterestDTO.setCurrency(DevGeneratedDataUtil.generateCurrency());
			accountInterestDTO.setSettlementPeriodDtType(null);
			accountInterestDTO.setSettlementPeriodCtType(null);
			accountInterestDTO.setSettlementPeriodDt(0);
			accountInterestDTO.setSettlementPeriodCt(i);
			accountInterestDTO.setNextCapitalizationDtDate(null);
			accountInterestDTO.setNextCapitalizationCtDate(null);
			accountInterestDTO.setLastTransactionBookingDate(null);
			accountInterestDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountInterestDTO.setOwnersList(ownersList);
			accountInterestDTO.setCategory(1004);
			accountInterestDTO.setSubProduct("1004");
			accountInterestDTO.setRelation("OWNER");
			accountInterestDTO.setStatementDistributionType(null);
			accountInterestDTO.setActions(null);
			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountInterestDTO.setAccountCategories(accountCategories);
			accountInterestDTO.setAccountRestrictFlag(false);
			accountInterestDTO.setDestAccountRestrictions(null);
			accountInterestDTO.setOwnerContext("DETAIL");
			accountInterestDTO.setAccountModifyStatus(null);
			accountInterestDTO.setExecutiveRestriction(false);
			accountInterestDTO.setOwnerGlobusId(801004000801004L + i);
			accountInterestDTO.setOpenBranch("TAn Binh " + i);
			accountInterestDTO.setCustomName("ACCOUNT801004");
			accountInterestDTO.setCustomIcon("");

			accountInterestList.add(accountInterestDTO);
		}

		accountsDTO.setAccountInterestList(accountInterestList);

		return accountsDTO;
	}

	@Override
	public DepositDTO getDeposit(String token, String depositId) {
		String jsonInput = "{\"depositId\":\"1409\",\"depositName\":null,\"description\":null,\"accountNo\":null,\"currency\":\"VND\",\"depositBalance\":10000000,\"nextCapitalizationDate\":null,\"maturityDate\":1557421200000,\"openingAccountNo\":\"0100100003486003\",\"settlementAccountNo\":\"0100100003486003\",\"settlementAccountIntNo\":\"0100100003486003\",\"renewalOptionType\":\"RENEWAL_WITH_INTEREST\",\"depositPeriodType\":null,\"period\":null,\"interest\":7.1,\"interestRateType\":\"FIXED\",\"totalInterestAmount\":350136.986301369863013698630136986301369,\"depositMigrationName\":null,\"depositMigrationContractNumber\":\"\",\"autoRolloverChangePos\":null,\"interestPaymentMethod\":null,\"category\":6704,\"subProductCode\":\"TICHLUYDIENTU\",\"openDate\":1541782800000,\"productType\":null,\"isSurcharge\":null,\"nextSurchargeDate\":null,\"nextSurchargeAmount\":null,\"lastSurchargeAmount\":null,\"ownersList\":[{\"customerId\":\"801003000801003\",\"relationType\":\"OWNER\",\"name\":null,\"addressStreetPrefix\":null,\"addressStreet\":\"WARD.2-801003, Q. 2\",\"houseNo\":null,\"apartmentNo\":null,\"postCode\":\"\",\"town\":\"Tp.Ho Chi Minh\",\"country\":null,\"fullName\":\"GB NAME 1-801003\",\"residence\":null}],\"unfinishedDisposition\":null,\"typeCapableToBreak\":true,\"lastTransactionBookingDate\":null,\"depositContractNumber\":\"0001600004155007\",\"customName\":null,\"customIcon\":null,\"instructionOnDispensation\":\"\",\"periodFrequency\":\"X\",\"dateFirstExecution\":1541782800000,\"workingAvailableFunds\":875269564,\"prematureInterestRate\":0,\"ibRedeemDepositPos\":true,\"status\":null,\"depositType\":null,\"surcharge\":null,\"_links\":{\"self\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409\"},\"deposit_download\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/downloads/deposit_download.json\"},\"realize_context_deposit\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/actions/realize_context_deposit.json\"},\"break_deposit\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/actions/break_deposit.json\"}}}";
		try {
			DepositDTO depositDTO = objectMapper.readValue(jsonInput, new TypeReference<DepositDTO>() {
			});
			depositDTO.setDepositId(depositId);
			return depositDTO;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	
}
