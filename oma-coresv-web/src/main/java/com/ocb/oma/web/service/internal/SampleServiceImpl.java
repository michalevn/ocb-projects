package com.ocb.oma.web.service.internal;

import org.springframework.stereotype.Service;

import com.ocb.oma.dto.SampleUserDTO;

@Service
public class SampleServiceImpl implements SampleService {

	@Override
	public String validate(SampleUserDTO value) {
		if (value.getEmail() != null) {
			if (!value.getEmail().endsWith("@ocb.com.vn")) {
				return "EMAIL_INVALID";
			}
		}
		return null;
	}

}
