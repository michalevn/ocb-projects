/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author Phu Hoang
 *
 */
@Configuration
public class ResourceConfigService {

	@Value("${token.expire.second}")
	Long EXPIRATIONTIME = 800000L;

	@Value("${token.secret.key}")
	String secretKey;

	@Value("${otp.sms.template}")
	String otpSmsTemplate;

	@Value("${otp.sms.expiretime}")
	Long otpSmsExpireTime;;

	public String getTokenSecret() {
		return secretKey;
	}

	public Long getTokenExpireTime() {
		return EXPIRATIONTIME;
	}

	public String getOtpSmsTemplate() {
		return otpSmsTemplate;
	}

	public Long getOtpSmsExpireTime() {
		return otpSmsExpireTime;
	}
}
