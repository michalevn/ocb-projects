package com.ocb.oma.web.util;

import java.util.concurrent.atomic.AtomicLong;

import org.hashids.Hashids;

import com.ocb.oma.web.config.LogFilter;

public class KeyHelper {

	private volatile static AtomicLong counter = new AtomicLong(System.currentTimeMillis());
	private static final Hashids HASH_IDS = new Hashids(LogFilter.class.getName());
	private static final Long INITIAL_VALUE = 1543424400000L;

	public static String next(String prefix) {
		return prefix + HASH_IDS.encode(counter.incrementAndGet() - INITIAL_VALUE);
	}
}
