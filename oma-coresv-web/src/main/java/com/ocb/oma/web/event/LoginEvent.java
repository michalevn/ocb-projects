/**
 * 
 */
package com.ocb.oma.web.event;

import org.springframework.mobile.device.Device;

import com.ocb.oma.dto.UserToken;

/**
 * @author phuhoang
 *
 */
public class LoginEvent extends OmniEvent {

	private static final long serialVersionUID = 3108366823808297671L;

	/**
	 * @param deviceInfo
	 * @param userDeviceToken
	 * @param source
	 */
	
	public LoginEvent(UserToken userToken, Device deviceInfo) {
		super(userToken, deviceInfo);
	}
}
