/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * @author docv
 *
 */
public interface WebBillService {
	/**
	 * Lấy ds đăng ký thanh toán HĐ tự động
	 * 
	 * @return
	 */
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String dateFrom, String dateTo);

	/**
	 * Post giao dịch đăng ký thanh toán HĐ tự động
	 * 
	 * @param createAutoBillTransferDirectlyDTO
	 * @return
	 */
	public Boolean createAutoBillTransferDirectly(CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO);

	/**
	 * xoa thanh toan hoa don tu dong
	 * 
	 * @param string
	 * @return
	 */
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String input);

	/**
	 * chinh sua thanh toan hoa don tu dong
	 * 
	 * @param input
	 * @return
	 */
	public OutputBaseDTO modifyAutoBillTransferDirectly(TransferAutoBillDTO input);

	/**
	 * Lay danh sach chuyen tien dinh ky
	 * 
	 * @return
	 */
	public List<StandingOrderOutputDTO> standingOrderList();

	/**
	 * xoa chuyen tien dinh ky
	 * 
	 * @param standingOrderId
	 * @return
	 */
	public OutputBaseDTO removeStandingOrder(String standingOrderId);

	/**
	 * tao chuyen tien dinh ky
	 * @param input
	 * @return
	 */
	public OutputBaseDTO createStandingOrder(StandingOrderInputDTO input);

	/**
	 * cap nhat chuyen tien dinh ky
	 * @param input
	 * @return
	 */
	public OutputBaseDTO modifyStandingOrderDirectly(StandingOrderInputDTO input);
}
