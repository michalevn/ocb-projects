/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebBillServiceImpl implements WebBillService {

	@Autowired
	private WebAccountSerivice webAccountService;

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String dateFrom, String dateTo) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getTransferAutoBills(omniToken, dateFrom, dateTo);
	}

	@Override
	public Boolean createAutoBillTransferDirectly(CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(createAutoBillTransferDirectlyDTO.getOtpMethod(),
				createAutoBillTransferDirectlyDTO.getOtpValue(), omniToken);
		return accountServiceExt.createAutoBillTransferDirectly(omniToken, createAutoBillTransferDirectlyDTO);
	}

	@Override
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.deleteAutoBillTransfer(omniToken, input);
	}

	@Override
	public OutputBaseDTO modifyAutoBillTransferDirectly(TransferAutoBillDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.modifyAutoBillTransferDirectly(omniToken, input);
	}

	@Override
	public List<StandingOrderOutputDTO> standingOrderList() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.standingOrderList(omniToken);
	}

	@Override
	public OutputBaseDTO removeStandingOrder(String standingOrderId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.removeStandingOrder(omniToken, standingOrderId);
	}

	@Override
	public OutputBaseDTO createStandingOrder(StandingOrderInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		return accountServiceExt.createStandingOrder(omniToken, input);
	}

	@Override
	public OutputBaseDTO modifyStandingOrderDirectly(StandingOrderInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.modifyStandingOrderDirectly(omniToken, input);
	}

}
