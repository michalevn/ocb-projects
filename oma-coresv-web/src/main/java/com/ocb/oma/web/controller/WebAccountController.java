/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.RegisterBankingProductOnlineDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.ChangePasswordInputDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.validation.AccountId;
import com.ocb.oma.validation.AccountName;
import com.ocb.oma.validation.AccountNo;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.PaymentType;
import com.ocb.oma.validation.ProductDefaultDirectlyValid;
import com.ocb.oma.validation.SummaryPeriod;
import com.ocb.oma.web.event.EventType;
import com.ocb.oma.web.event.LoginEvent;
import com.ocb.oma.web.event.OmniEvent;
import com.ocb.oma.web.service.internal.WebAccountSerivice;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/account")
//@Validated
public class WebAccountController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@RequestMapping(value = "/test")
	public ResponseEntity<?> testAccount(@RequestParam(name = "name") String name, Device device,
			HttpServletRequest request) {
		String type = "";
		if (device.isMobile()) {
			type = "mobile";
		} else if (device.isTablet()) {
			type = "tablet";
		} else if (device.isNormal()) {
			type = "normal";
		}
		Map<String, String> data = new HashMap<String, String>();
		data.put("type", type);

		data.put("simpleName", device.getDevicePlatform().getDeclaringClass().getSimpleName());
		data.put("name", device.getDevicePlatform().getDeclaringClass().getName());
		data.put("typeName", device.getDevicePlatform().getDeclaringClass().getTypeName());
		data.put("getCanonicalName", device.getDevicePlatform().getDeclaringClass().getCanonicalName());
		System.out.println(device.getDevicePlatform());
		data.put("data", accountService.doTest(name));
		// data.put("simpleName",device.getDevicePlatform().getDeclaringClass().get());

		try {
			Thread.currentThread().sleep(5 * 60 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return getFinalResponse(data);
	}

	@RequestMapping(value = "/getAccountList")
	public ResponseEntity<?> getAccountList(Device deviceInfo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		applicationEventPublisher.publishEvent(new OmniEvent(userToken, deviceInfo, EventType.getAccountList));
		return getFinalResponse(accountService.getAccountList());
	}

	@RequestMapping(value = "/findAccountDetail")
	public ResponseEntity<?> findAccountDetail(@RequestParam("accountNum") String accountNum) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.findAccountDetail(accountNum));
	}

	@RequestMapping(value = "/updateAccountAvatar", method = RequestMethod.POST)
	public ResponseEntity<?> updateAccountAvatar(@RequestParam("file") MultipartFile file,
			@AccountNo(message = "ACCOUNTNO_INVALID") @RequestParam("accountNo") String accountNo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}

		return getFinalResponse(accountService.updateAccountIcon(accountNo, file));
	}

	@RequestMapping(value = "/updateAccountName")
	public ResponseEntity<?> updateAccountName(
			@AccountName(message = "ACCOUNTNAME_INVALID") @RequestParam("accountName") String accountName,
			@AccountNo(message = "ACCOUNTNO_INVALID") @RequestParam("accountNo") String accountNo, Device deviceInfo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		applicationEventPublisher.publishEvent(new OmniEvent(userToken, deviceInfo, EventType.updateAccountName));
		return getFinalResponse(accountService.updateAccountName(accountNo, accountName));
	}

	@RequestMapping(value = "/findTransactionHistory", method = RequestMethod.POST)
	public ResponseEntity<?> findTransactionHistory(@RequestBody TransHistoryRequestDTO request, Device deviceInfo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		applicationEventPublisher.publishEvent(new OmniEvent(userToken, deviceInfo, EventType.updateAccountName));
		return getFinalResponse(accountService.findTransactionHistory(request));
	}

	@RequestMapping(value = "/loadAccountSummarByCurrencies")
	public ResponseEntity<?> loadAccountSummarByCurrencies(
			@AccountId(message = "ACCOUNTID_INVALID") @RequestParam("accountId") String accountId,
			@SummaryPeriod(message = "SUMMARYPERIOD_INVALID") @RequestParam("summaryPeriod") Integer summaryPeriod) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.loadAccountSummarByCurrencies(accountId, summaryPeriod));
	}

	@RequestMapping(value = "/accountLimit")
	public ResponseEntity<?> getAccountLimit(
			@PaymentType(message = "PAYMENTTYPE_INVALID") @RequestParam("paymentType") String paymentType) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAccountLimnit(paymentType));
	}

	@RequestMapping(value = "/getAccountNameByAccountNo")
	public ResponseEntity<?> getAccountNameByAccountNo(
			@AccountNo(message = "ACCOUNTNO_INVALID") @RequestParam("accountNo") String accountNo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAccountNameByAccountNo(accountNo));
	}

	@RequestMapping(value = "/getAccountNameNapas")
	public ResponseEntity<?> getAccountNameByAccountNo(
			@RequestParam(name = "accountNo", required = true) String accountNo,
			@RequestParam(name = "bankCode", required = true) String bankCode,
			@RequestParam(name = "cardNumber", required = true) String cardNumber,
			@RequestParam(name = "debitAccount", required = true) String debitAccount) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAccountNameNapas(accountNo, bankCode, cardNumber, debitAccount));
	}

	@RequestMapping(value = "/getUpcomingRepaymentDeposit")
	public ResponseEntity<?> getUpcomingRepaymentDeposit() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getUpcomingRepaymentDeposit());
	}

	@RequestMapping(value = "/getBlockedTransaction")
	public ResponseEntity<?> getBlockedTransaction(
			@AccountId(message = "ACCOUNTID_INVALID") @RequestParam("accountId") String accountId,
			@Digits(message = "PAGENUMBER_INVALID") @RequestParam("currentPage") Integer pageNumber,
			@Digits(message = "PAGESIZE_INVALID") @RequestParam("pageSize") Integer pageSize) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getBlockedTransaction(accountId, pageNumber, pageSize));
	}

	@RequestMapping(value = "/getAvatarInfo")
	public ResponseEntity<?> getAvatarInfo() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAvatarInfo());
	}

	@RequestMapping(value = "/sendGiftCard")
	public ResponseEntity<?> sendGiftCard(@RequestBody GiftCardInsertDTO giftCardInsertDTO) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.sendGiftCard(giftCardInsertDTO));
	}

	@RequestMapping(value = "/loginByToken")
	public ResponseEntity<?> loginByToken(@RequestBody Map<String, String> postData, Device deviceInfo) {

		String deviceId = postData.get("deviceId");
		String deviceToken = postData.get("deviceToken");

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		UserToken user = accountService.reLoginByEncryptData(userToken.getUsername(), userToken.getHashedData(),
				deviceId, deviceToken);
		if (user != null) {
			applicationEventPublisher.publishEvent(new LoginEvent(user, deviceInfo));
			return getFinalResponse(user);
		}
		return getFailureResponse(user, MessageConstant.LOGIN_FAIL_CODE);
	}

	@RequestMapping(value = "/changePassword")
	public ResponseEntity<?> changePassword(@RequestBody ChangePasswordInputDTO data) {
		String customerId = AppUtil.getCurrentCustomerCif();
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String oldPassword = data.getOldPassword();
		String newPassword = data.getNewPassword();
		return getFinalResponse(accountService.changePassword(oldPassword, newPassword, customerId));
	}

	@RequestMapping(value = "/getCIFFromAccountList")
	public ResponseEntity<?> getCIFFromAccountList(@RequestBody Map<String, Object> params) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<String> lstAccountNum = (List<String>) params.get("lstAccountNum");
		Map<String, String> rs = accountService.getCIFFromAccountList(lstAccountNum);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/getPersonalInfo")
	public ResponseEntity<?> getPersonalInfo() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		CustomerPersonalInfoDTO personalInfo = accountService.getPersonalInfo();
		return getFinalResponse(personalInfo);
	}

	@RequestMapping(value = "/registerBankingProductOnline")
	public ResponseEntity<?> registerBankingProductOnline(
			@RequestBody RegisterBankingProductOnlineDTO registerBankingProductOnlineDTO) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.registerBankingProductOnline(registerBankingProductOnlineDTO));
	}

	@RequestMapping(value = "/uploadAvatar")
	public ResponseEntity<?> uploadAvatar(@RequestBody Map<String, String> params) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		AvatarInfoOutputDTO dto = accountService.uploadAvatar(params.get("file"));
		return getFinalResponse(dto);
	}

	@RequestMapping(value = "/updateProductDefaultDirectly")
	public ResponseEntity<?> updateProductDefaultDirectly(
			@RequestBody @ProductDefaultDirectlyValid Map<String, String> map) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Boolean result = accountService.updateProductDefaultDirectly(map);
		return getFinalResponse(result);
	}

	@RequestMapping(value = "/transferAccounts")
	public ResponseEntity<?> transferAccounts(@RequestParam("productList") String productList,
			@RequestParam("restrictions") String restrictions) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.transferAccounts(productList, restrictions));
	}

	@RequestMapping(value = "/changeUsername")
	public ResponseEntity<?> changeUsername(@RequestBody ChangeUsernameInput changeUsername) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.changeUsername(changeUsername));
	}

}
