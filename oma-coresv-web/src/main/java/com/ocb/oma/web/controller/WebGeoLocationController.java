/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.WebGeoLocationService;

@RestController
@RequestMapping("/service/geo-locations")
public class WebGeoLocationController extends BaseController {

	@Autowired
	private WebGeoLocationService webGeoLocationService;

	@RequestMapping(value = "/find")
	public ResponseEntity<?> find(@RequestParam("lat") Double lat, @RequestParam("lng") Double lng,
			@RequestParam("poiType") String poiType) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		GeoLocationPoiDTO geoLocationPoi = webGeoLocationService.find(lat, lng, poiType);
		return getFinalResponse(geoLocationPoi);
	}
}
