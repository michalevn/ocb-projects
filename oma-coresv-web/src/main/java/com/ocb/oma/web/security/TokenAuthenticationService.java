package com.ocb.oma.web.security;

import static java.util.Collections.emptyList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.websocket.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.ResourceConfigService;
import com.ocb.oma.web.service.internal.TokenManagementService;
import com.ocb.oma.web.util.AppUtil;
import com.ocb.oma.web.util.ApplicationContextHolder;

class TokenAuthenticationService {
	static long EXPIRATIONTIME = -1;
	static String SECRET = null;
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	static ObjectMapper objectMapper = new ObjectMapper();
	static Logger LOGGER = LoggerFactory.getLogger(TokenAuthenticationService.class);

	private static String getSecretKey() {

		if (SECRET == null) {
			SECRET = ApplicationContextHolder.getBean(ResourceConfigService.class).getTokenSecret();
		}
		return SECRET;
	}

	private static Long getExpireTime() {

		if (EXPIRATIONTIME == -1) {
			EXPIRATIONTIME = ApplicationContextHolder.getBean(ResourceConfigService.class).getTokenExpireTime();
		}
		return EXPIRATIONTIME;
	}

	static void addAuthentication(HttpServletResponse res, String username) {
		//		Date expireDate = new Date(System.currentTimeMillis() + getExpireTime());
		//		Map<String, Object> extData = new HashMap();
		//		// extData.put("testKey", "123456123");
		//		String JWT = Jwts.builder().setClaims(extData).setSubject(username).setExpiration(expireDate)
		//				.signWith(SignatureAlgorithm.HS512, getSecretKey()).compact();
		//		res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
		//		UserToken userToken = new UserToken();
		//		userToken.setExpireTime(expireDate);
		//		userToken.setUsername(username);
		//		userToken.setToken(JWT);
		//		ResponseDTO out = ResponseDTO.getSuccessResponse();
		//		out.setData(userToken);
		//		try {
		//			res.getWriter().write(objectMapper.writeValueAsString(out));
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			// LOGGER.error("", e);
		//		}
		//		res.setContentType("application/json");
		//		addTokenToManage(JWT);

	}

	static Authentication getAuthentication(HttpServletRequest request) throws AuthenticationException {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			// check token is valid or not
			token = token.replace(TOKEN_PREFIX, "").trim();
			TokenManagementService tokenManager = ApplicationContextHolder.getBean(TokenManagementService.class);
			Boolean isValid = (Boolean) tokenManager.getTokenValue(token);

			if (isValid == null) {
				// throw new AccessDeniedException("token_invalid");
				// addTokenToManage(token);
				//	throw new AuthenticationException("invalid token");
			} else {
				if (isValid == false) {
					throw new AuthenticationException("invalid token");
					// return null;
				}
			}

			// parse the token.
			UserToken user = getUser(token);

			//			String testKey = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token)
			//					.getBody().get("testKey", String.class);
			//			System.out.println("****" + testKey);
			return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyList()) : null;
		}
		return null;
	}

	private static UserToken getUser(String token) {
		try {
			UserToken userToken = AppUtil.getUserToken(token, getSecretKey());
			return userToken;
			// return
			// Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody().getSubject();
		} catch (Exception e) {
			LOGGER.info("invalid token " + token);
			return null;
		}
	}
}