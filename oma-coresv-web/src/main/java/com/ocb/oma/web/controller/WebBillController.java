/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.web.service.internal.WebBillService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/bill")
public class WebBillController extends BaseController {

	private static final Log LOG = LogFactory.getLog(WebBillController.class);

	@Autowired
	private WebBillService billService;

	@RequestMapping(value = "/getTransferAutoBills")
	@DateRange(from = "dateFrom", to = "dateTo", message = "dateFrom.less.than.or.equal.to.dateTo")
	public ResponseEntity<?> getTransferAutoBills(@RequestParam("dateFrom") String dateFrom,
			@RequestParam("dateTo") String dateTo) {
		LOG.info("getTransferAutoBills");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<TransferAutoBillOutputDTO> rs = billService.getTransferAutoBills(dateFrom, dateTo);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/createAutoBillTransferDirectly")
	public ResponseEntity<?> createAutoBillTransferDirectly(
			@RequestBody CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {
		LOG.info("createAutoBillTransferDirectly");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Boolean rs = billService.createAutoBillTransferDirectly(createAutoBillTransferDirectlyDTO);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/deleteAutoBillTransfer")
	public ResponseEntity<?> deleteAutoBillTransfer(@RequestBody Map<String, String> input) {
		LOG.info("deleteAutoBillTransfer");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		DeleteAutoBillTransferOutputDTO rs = billService.deleteAutoBillTransfer(input.get("orderNumber"));
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/modifyAutoBillTransferDirectly")
	public ResponseEntity<?> modifyAutoBillTransferDirectly(@RequestBody TransferAutoBillDTO input) {
		LOG.info("deleteAutoBillTransfer");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		OutputBaseDTO rs = billService.modifyAutoBillTransferDirectly(input);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/standingOrderList")
	public ResponseEntity<?> standingOrderList() {
		LOG.info("standingOrderList");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<StandingOrderOutputDTO> rs = billService.standingOrderList();
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/createStandingOrder")
	public ResponseEntity<?> createStandingOrder(@RequestBody StandingOrderInputDTO input) {
		LOG.info("createStandingOrder");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		OutputBaseDTO rs = billService.createStandingOrder(input);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/removeStandingOrder")
	public ResponseEntity<?> removeStandingOrder(@RequestBody Map<String, String> input) {
		LOG.info("removeStandingOrder");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String standingOrderId = input.get("standingOrderId");
		OutputBaseDTO rs = billService.removeStandingOrder(standingOrderId);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/modifyStandingOrderDirectly")
	public ResponseEntity<?> modifyStandingOrderDirectly(@RequestBody StandingOrderInputDTO input) {
		LOG.info("modifyStandingOrderDirectly");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		OutputBaseDTO rs = billService.modifyStandingOrderDirectly(input);
		return getFinalResponse(rs);
	}

}
