package com.ocb.oma.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.web.security.JWTAuthenticationFilter;
import com.ocb.oma.web.security.JWTLoginFilter;
import com.ocb.oma.web.service.external.ResourceServiceExt;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private AuthenticationEntryPoint authEntryPoint;

	@Autowired
	private ResourceServiceExt resourceService;

	@Value("${allow.multiple.devices}")
	private Boolean allowMultipleDevices;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/").permitAll().antMatchers("/login").permitAll()
				.anyRequest()
				// .anonymous().and()
				.authenticated().and()
				// We filter the api/login requests
				.addFilterBefore(new JWTLoginFilter("/login", authenticationManager(), objectMapper, messageSource),
						UsernamePasswordAuthenticationFilter.class)
				// And filter other requests to check the presence of JWT in header
				.addFilterBefore(new JWTAuthenticationFilter(resourceService, allowMultipleDevices),
						UsernamePasswordAuthenticationFilter.class)
				.httpBasic().authenticationEntryPoint(authEntryPoint);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/webjars/**", "/swagger-resources/**", "/service/mobileDevices/registerDevice",
				"/loginService", "/service/account/test", "/loginService/loginByEncryptData", "/service/resources/**", "/logoutService", "/logoutService/invalidToken");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// Create a default account
		// auth.inMemoryAuthentication().passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder())
		// .withUser("admin").password("password").roles("ADMIN");
		UserBuilder usersBuilder = User.withDefaultPasswordEncoder();
		UserDetails admin = usersBuilder.username("admin").password("password").roles("USER", "ADMIN").build();
		auth.inMemoryAuthentication().withUser(admin);
		// User user =
		// users.username("user").password("password").roles("USER").build();
		// User admin = users.username("admin").password("password").roles("USER",
		// "ADMIN").build();

		// User.withDefaultPasswordEncoder().username("user").password("user").roles("USER").build();
	}

}