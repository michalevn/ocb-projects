/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import com.ocb.oma.dto.UserToken;

/**
 * @author Phu Hoang
 *
 */
public interface TokenManagementService {

	void addToken(UserToken token);

	void removeToken(String token);

	public Object getTokenValue(String token);

	public void invalidToken(String token);

	public Boolean isTokenOk(UserToken userToken);

}