/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.AddressDTO;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RegisterBankingProductOnlineDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostBasketsInput;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.dto.input.VetifyUserDeviceDTO;
import com.ocb.oma.dto.newFo.NewFoAddress;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoContactInfo;
import com.ocb.oma.dto.newFo.NewFoCustomerInfo;
import com.ocb.oma.dto.newFo.NewFoOnlineService;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.event.AddBasketEvent;
import com.ocb.oma.web.event.DeleteRecipientEvent;
import com.ocb.oma.web.event.OmniEvent;
import com.ocb.oma.web.event.PostPaymentEvent;
import com.ocb.oma.web.event.SendGiftCardEvent;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.util.AppUtil;
import com.ocb.oma.web.util.ApplicationContextHolder;
import com.ocb.oma.web.util.KeyHelper;

/**
 * @author Phu Hoang
 *
 */
@Service
public class WebAccountServiceImpl implements WebAccountSerivice {

	@Autowired
	private AccountServiceExt accountService;

	@Autowired
	private ResourceServiceExt resourceService;

	@Autowired
	private ResourceConfigService resourceConfigService;

	@Autowired
	private ObjectMapper objectMapper;

	protected Logger logger = Logger.getLogger(WebAccountServiceImpl.class.getName());

	private String DEVICEID_KEY = "DeviceIDKey";

	@Override
	public String doTest(String testParam) {
		return accountService.doTest(testParam);
	}

	@Autowired
	EncryptDataService encryptService;

	@Override
	public UserToken checkLogin(String username, String password, String deviceId, String deviceToken) {

		UserToken user = accountService.checkLogin(username, password, deviceId);

		if (user != null) {
			// generate token
			Date expireDate = new Date(System.currentTimeMillis() + resourceConfigService.getTokenExpireTime() * 1000);

			Map<String, Object> extData = new HashMap<String, Object>();
			extData.put(DEVICEID_KEY, deviceId);

			user.setExpireTime(expireDate);

			user.setDeviceId(deviceId);
			user.setDeviceToken(deviceToken);
			// encrypt Password
			String encryptPassword = encryptService.encrypt(password);
			String JWT = AppUtil.generateToken(deviceId, username, encryptPassword, expireDate,
					resourceConfigService.getTokenSecret(), user.getOldAuthen());
			user.setToken(JWT);
			user.setFirstName(user.getOldAuthen().getFirstName());
			user.setLastName(user.getOldAuthen().getLastName());
			// user.setHashedData(encryptPassword);
			// user.setOldAuthen(use);
			addTokenToManage(user);

			String cif = user.getOldAuthen().getCustomerId();
			UserDeviceToken userDevice = resourceService.activeUserDevice(cif, deviceId, deviceToken);
			if (userDevice == null) {
				user.setRequireOtp(false);
				userDevice = new UserDeviceToken();
				userDevice.setStatus(0);
				userDevice.setCif(cif);
				userDevice.setDeviceId(deviceId);
				userDevice.setDeviceToken(deviceToken);
				userDevice.setLastLoginDate(new Date());
				user.setIsNotRegistered(true);
				resourceService.registerUserDevice(userDevice);
			} else {
				if (userDevice.getStatus() == null || !userDevice.getStatus().equals(1)) {
					user.setRequireOtp(true);
				} else {
					user.setRequireOtp(false);
				}
				user.setIsNotRegistered(false);
			}
		}
		return user;
	}

	@Override
	public UserToken reLoginByEncryptData(String username, String encryptPassword, String deviceId,
			String deviceToken) {

		String decryptPassword = encryptService.decrypt(encryptPassword);
		UserToken userToken = checkLogin(username, decryptPassword, deviceId, deviceToken);
		if (userToken != null)
			userToken.setHashedData(encryptPassword);
		return userToken;
	}

	private static void addTokenToManage(UserToken token) {
		TokenManagementService tokenManager = ApplicationContextHolder.getBean(TokenManagementService.class);
		tokenManager.addToken(token);
	}

	@Override
	public AuthorizationResponse sendAuthorizationSmsOtp() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.sendAuthorizationSmsOtp(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#
	 * countBasketPaymentToProcess()
	 */
	@Override
	public Integer countBasketPaymentToProcess() {
		// TODO Auto-generated method stub
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.countBasketPaymentToProcess(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getAccountList(java.lang.
	 * String)
	 */
	@Override
	public List<AccountInfo> getAccountList() {
		// TODO Auto-generated method stub
		String customerCif = AppUtil.getCurrentCustomerCif();
		if (customerCif == null) {
			throw new OmaException(MessageConstant.CUSTOMER_CIF_INVALID);
		}
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getAccountList(customerCif, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#getCampaignList()
	 */
	@Override
	public List<CampaignInfo> getCampaignList() {
		// TODO Auto-generated method stub
		String customerCif = AppUtil.getCurrentCustomerCif();
		String username = AppUtil.getCurrentUsernameLogin();
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getCampaignList(customerCif, username, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#verifyOTPToken(com.ocb.
	 * oma.dto.input.VerifyOtpToken)
	 */
	@Override
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data) {
		// TODO Auto-generated method stub
		if (data.getAuthId() == null || data.getOtpValue() == null) {
			throw new OmaException(MessageConstant.PARAM_CAN_NOT_BE_NULL);
		}
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.verifyOTPToken(data, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#verifyHwOTPToken(java.
	 * lang.String)
	 */
	@Override
	public AuthorizationResponse verifyHwOTPToken(String hwToken) {
		if (hwToken == null) {
			throw new OmaException(MessageConstant.PARAM_CAN_NOT_BE_NULL);
		}
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.verifyHwOTPToken(hwToken, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#updateAccountName(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountName(String accountNo, String updatedName) {
		// TODO Auto-generated method stub
		String customerCif = AppUtil.getCurrentCustomerCif();
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.updateAccountName(accountNo, customerCif, updatedName, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#updateAccountIcon(java.
	 * lang.String, java.lang.String, java.io.InputStream)
	 */
	@Override
	public Boolean updateAccountIcon(String accountNo, MultipartFile image) {
		String omniToken = AppUtil.getCurrentCPBToken();
		String cif = AppUtil.getCurrentCustomerCif();
		String imageUrl = getImageLinkFromResource(image);
		return accountService.updateAccountIcon(accountNo, cif, imageUrl, omniToken);
	}

	private String getImageLinkFromResource(MultipartFile multipartFile) {

		return "somethingTest";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#findTransactionHistory(
	 * com.ocb.oma.dto.input.TransHistoryRequestDTO)
	 */
	@Override
	public List<TransactionHistoryDTO> findTransactionHistory(TransHistoryRequestDTO data) {
		// TODO Auto-generated method stub
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.findTransactionHistory(data, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getRecipientList(java.
	 * lang.String)
	 */
	@Override
	public List<RecipientDTO> getRecipientList(String filerTemplateType, int pageSize) {
		String omniToken = AppUtil.getCurrentCPBToken();
		List<RecipientDTO> result = accountService.getRecipientList(filerTemplateType, pageSize, omniToken);
		for (RecipientDTO recipientDTO : result) {
			String bankCodeMa = null;
			if (PaymentType.FastTransfer.getPaymentTypeCode().equals(recipientDTO.getPaymentType())
					&& recipientDTO.getBankCode() != null) {

				BankInfoDTO bankInfo = resourceService.getBankByCodeNapas(recipientDTO.getBankCode());
				recipientDTO.setBankName(bankInfo.getBankName());
				bankCodeMa = bankInfo.getBankCode();
			} else if (PaymentType.LocalPayment.getPaymentTypeCode().equals(recipientDTO.getPaymentType())) {
				bankCodeMa = resourceService.getByBankCitadId(recipientDTO.getBankCode());
			}

			recipientDTO.setBankCodeMA(bankCodeMa);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#deleteRecipient(com.ocb.
	 * oma.dto.RecipientDTO)
	 */
	@Override
	public Boolean deleteRecipient(RecipientDTO recipient) {
		String omniToken = AppUtil.getCurrentCPBToken();
		Boolean result = accountService.deleteRecipient(recipient, omniToken);
		publicEvent(new DeleteRecipientEvent(AppUtil.getCurrentUserLogin(), recipient));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#
	 * loadAccountSummarByCurrencies()
	 */
	@Override
	public AccountSummaryDTO loadAccountSummarByCurrencies(String accountId, Integer summaryPeriod) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.loadAccountSummarByCurrencies(accountId, summaryPeriod, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getAccountLimnit(java.
	 * lang.String)
	 */
	@Override
	public AccountLimitDTO getAccountLimnit(String paymentType) {
		// TODO Auto-generated method stub
		PaymentType.validPaymentType(paymentType);
		String omniToken = AppUtil.getCurrentCPBToken();
		;
		return accountService.getAccountLimnit(paymentType, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getAccountNameByAccountNo
	 * (java.lang.String)
	 */
	@Override
	public String getAccountNameByAccountNo(String accountNo) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getAccountNameByAccountNo(accountNo, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getTransactionFee(com.ocb
	 * .oma.dto.input.TransactionFeeInputDTO)
	 */
	@Override
	public Double getTransactionFee(TransactionFeeInputDTO input) {
		// TODO Auto-generated method stub
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getTransactionFee(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#
	 * getAuthorizeMethodByAmount(java.lang.Double, java.lang.String)
	 */
	@Override
	public String getAuthorizeMethodByAmount(Double amount, String paymentType) {
		// TODO Auto-generated method stub
		String omniToken = AppUtil.getCurrentCPBToken();
		String cif = AppUtil.getCurrentCustomerCif();
		String registeredMethod = "";
		// goi ham QD630 , kiem tra theo khoan tien nam trong muc nao ==> tra ve phuong
		// thuc tuong ung
		List<QD630PoliciesDTO> policies = getQD630Policies(cif);
		String registeredMethodCk = "";
		for (QD630PoliciesDTO qd630PoliciesDTO : policies) {
			registeredMethodCk = qd630PoliciesDTO.getRegisteredMethod();
			if (!registeredMethodCk.equals("PKI")) {
				Double frAmount = qd630PoliciesDTO.getFromAmount();
				Double toAmount = qd630PoliciesDTO.getToAmount();
				if (frAmount <= amount && toAmount >= amount) {
					registeredMethod = qd630PoliciesDTO.getRegisteredMethod();
					break;
				}
			}
		}
		if (registeredMethod.isEmpty() || registeredMethod == null) {
			throw new OmaException(MessageConstant.CAN_NOT_FIND_AUTHORIZE_METHOD);
		}

		return registeredMethod;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getChallengeCodeSoftOTP(
	 * com.ocb.oma.dto.input.GetChallengeCodeInputDTO)
	 */
	@Override
	public String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getChallengeCodeSoftOTP(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#postPayment(com.ocb.oma.
	 * dto.input.PostPaymentInputDTO)
	 */
	public void checkOTP(String otpMethod, VerifyOtpToken otpValue, String token) {
		if (otpMethod == null || otpValue == null || otpValue.getOtpValue() == null) {
			throw new OmaException(MessageConstant.OTP_INCORRECT);
		}
		if (OtpAuthorizeMethod.SMSOTP.getCode().equals(otpMethod)
				|| OtpAuthorizeMethod.SOFTOTP.getCode().equals(otpMethod)
				|| OtpAuthorizeMethod.SMS_TOKEN.getCode().equals(otpMethod)) {
			AuthorizationResponse verify = accountService.verifyOTPToken(otpValue, token);
			if (verify != null && verify.getOperationStatus().equals(AuthorizationResponse.CORRECT)) {
				return;
			}
			throw new OmaException("OTP_" + verify.getOperationStatus());
		}
		if (OtpAuthorizeMethod.HW.getCode().equals(otpMethod) || OtpAuthorizeMethod.HWTOKEN.getCode().equals(otpMethod)
				|| OtpAuthorizeMethod.HW_TOKEN.getCode().equals(otpMethod)) {
			AuthorizationResponse verify = accountService.verifyHwOTPToken(otpValue.getOtpValue(), token);
			if (verify != null && verify.getOperationStatus().equals(AuthorizationResponse.CORRECT)) {
				return;
			}
			throw new OmaException("OTP_" + verify.getOperationStatus());
		}
		// neu khong phai cac phuong thuc xac thuc tren thi nem loi
		throw new OmaException(MessageConstant.OTP_INCORRECT);

	}

	@Override
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		PaymentType.validPaymentType(input.getPaymentType());
		// verify OTP
		checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		PostPaymentOutputDTO result = accountService.postPayment(input, omniToken);
		if (result.getSuccess()) {
			PostPaymentEvent event = new PostPaymentEvent(AppUtil.getCurrentUserLogin(), input);
			publicEvent(event);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#
	 * getUpcomingRepaymentDeposit()
	 */
	@Override
	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getUpcomingRepaymentDeposit(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getBlockedTransaction(
	 * java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	public List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer currentPage, Integer pageSize) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getBlockedTransaction(accountId, currentPage, pageSize, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getAuthorizationPolicies(
	 * java.lang.String)
	 */
	@Override
	public List<String> getAuthorizationPolicies(String userId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getAuthorizationPolicies(userId, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getAccountNameNapas(java.
	 * lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getAccountNameNapas(accountNo, bankCode, cardNumber, debitAccount, omniToken);
	}

	@Override
	public List<QD630PoliciesDTO> getQD630Policies(String cif) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getQD630Policies(cif, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#postAddBasket(com.ocb.oma
	 * .dto.input.PostPaymentInputDTO)
	 */
	@Override
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();

		PostAddBasketDTO result = accountService.postAddBasket(input, omniToken);
		if (result.getResultCode().equals("1000")) {
			throw new OmaException(MessageConstant.DEBIT_ACCOUNT_NOT_VALID);
		} else if (result.getResultCode().equals("1")) {
			publicEvent(new AddBasketEvent(AppUtil.getCurrentUserLogin(), input));
			return result;
		} else {
			throw new OmaException(MessageConstant.SERVER_ERROR, result.getResultMsg());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#addRecipient(com.ocb.oma.
	 * dto.RecipientInputDTO)
	 */
	@Override
	public AddRecipientOutputDTO addRecipient(RecipientInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.addRecipient(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#getAvatarInfo()
	 */
	@Override
	public AvatarInfoOutputDTO getAvatarInfo() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getAvatarInfo(omniToken);
	}

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	protected void publicEvent(OmniEvent omniEvent) {
		applicationEventPublisher.publishEvent(omniEvent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#sendGiftCard(com.ocb.oma.
	 * dto.GiftCardInsertDTO)
	 */
	@Override
	public List<SendGiftCardOutputDTO> sendGiftCard(GiftCardInsertDTO giftCardInsertDTO) {
		String omniToken = AppUtil.getCurrentCPBToken();
		// verify otp
		checkOTP(giftCardInsertDTO.getOtpMethod(), giftCardInsertDTO.getOtpValue(), omniToken);

		// post payment
		List<SendGiftCardOutputDTO> result = accountService.sendGiftCard(giftCardInsertDTO, omniToken);
		UserToken userTokens = AppUtil.getCurrentUserLogin();

		publicEvent(new SendGiftCardEvent(userTokens, giftCardInsertDTO, result));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getListofBasket(com.ocb.
	 * oma.dto.UserToken, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<PaymentBasketDTO> getListofBasket(Double amountFrom, Double amountTo, String customerId,
			String realizationDateFrom, String realizationDateTo, String statuses) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getListofBasket(omniToken, amountFrom, amountTo, customerId, realizationDateFrom,
				realizationDateTo, statuses);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#deleteBasket(java.lang.
	 * String)
	 */
	@Override
	public String deleteBasket(String basketId) {
		String omniToken = AppUtil.getCurrentCPBToken();

		return accountService.deleteBasket(basketId, omniToken);
	}

	/*
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getListOfServiceProvider(
	 * )
	 */
	@Override
	public List<ServiceProviderComposDTO> getListOfServiceProvider() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getListOfServiceProvider(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getListOfBillTransactions
	 * (com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO)
	 */
	@Override
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(GetListOfBillTransactionsInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getListOfBillTransactions(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebAccountSerivice#
	 * getListBillPaymentByService(java.lang.String)
	 */
	@Override
	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getListBillPaymentByService(serviceCode, duration, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getBill(com.ocb.oma.dto.
	 * input.GetBillInputDTO)
	 */
	@Override
	public BillPaymentServiceDTO getBill(GetBillInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getBill(input, omniToken);
	}

	@Override
	public List<EVNParameter> loadEVNparameter() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.loadEVNparameter(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#updateBasket(com.ocb.oma.
	 * dto.input.PostPaymentInputDTO)
	 */
	@Override
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.updateBasket(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#postBaskets(java.util.
	 * List)
	 */
	@Override
	public PostListBasketDTO postBaskets(PostBasketsInput input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		return accountService.postBaskets(input.getTransfersIds(), omniToken);
	}

	@Override
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.changePassword(oldPass, newPass, customerId, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getPaymentsInFuture(com.
	 * ocb.oma.dto.input.PaymentsInFutureInputDTO)
	 */
	@Override
	public List<PaymentsInFutureDTO> getPaymentsInFuture(PaymentsInFutureInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getPaymentsInFuture(input, omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getListPrepaidPhone()
	 */
	@Override
	public List<PrepaidPhoneDTO> getListPrepaidPhone() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getListPrepaidPhone(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebAccountSerivice#getCIFFromAccountList(
	 * java.util.List)
	 */
	@Override
	public Map<String, String> getCIFFromAccountList(List<String> lstAccountNum) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getCIFFromAccountList(lstAccountNum, omniToken);
	}

	@Override
	public CustomerPersonalInfoDTO getPersonalInfo() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getPersonalInfo(omniToken);
	}

	@Override
	public NewFoTransactionInfo registerBankingProductOnline(
			RegisterBankingProductOnlineDTO registerBankingProductOnlineDTO) {

		String omniToken = AppUtil.getCurrentCPBToken();

		CustomerPersonalInfoDTO personalInfo = null;

		try {
			personalInfo = accountService.getPersonalInfo(omniToken);
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		if (personalInfo == null) {
			throw new OmaException(MessageConstant.ACCOUNT_NOT_REGISTERED_PERSONAL_INFO);
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

		NewFoBankingProductOnline bankingProductOnline = new NewFoBankingProductOnline();

		NewFoTransactionInfo transactionInfo = new NewFoTransactionInfo();
		transactionInfo.setcRefNum(KeyHelper.next("OMNI_"));
		transactionInfo.setClientCode("LandingPage");
		bankingProductOnline.setTransactionInfo(transactionInfo);

		NewFoCustomerInfo customerInfo = new NewFoCustomerInfo();
		customerInfo.setFullname(personalInfo.getFullName());
		if (personalInfo.getBirthDate() != null) {
			customerInfo.setBirthDay(dateFormat.format(personalInfo.getBirthDate()));
		}
		NewFoAddress contactAddress = new NewFoAddress();
		AddressDTO homeAddress = personalInfo.getHomeAddress();
		if (homeAddress != null) {
			// contactAddress.setProvince(homeAddress.getTown());
			StringBuilder address1 = new StringBuilder();
			if (StringUtils.isNotBlank(homeAddress.getHouse())) {
				address1.append(homeAddress.getHouse());
			}
			if (StringUtils.isNotBlank(homeAddress.getStreet())) {
				if (address1.length() > 0) {
					address1.append(", ");
				}
				address1.append(homeAddress.getHouse());
			}
			if (StringUtils.isNotBlank(homeAddress.getTown())) {
				if (address1.length() > 0) {
					address1.append(", ");
				}
				address1.append(homeAddress.getTown());
			}
			if (StringUtils.isNotBlank(homeAddress.getCountry())) {
				if (address1.length() > 0) {
					address1.append(", ");
				}
				address1.append(homeAddress.getCountry());
			}
			contactAddress.setAddress1(address1.toString());
		}
		if (StringUtils.isNotBlank(personalInfo.getEmail())) {
			contactAddress.setEmail(new String[] { personalInfo.getEmail() });
		}
		customerInfo.setContactAddress(contactAddress);

		NewFoContactInfo contactInfo = new NewFoContactInfo();
		String mobilePhoneNumber = personalInfo.getMobilePhoneNumber();
		if (StringUtils.isNotBlank(mobilePhoneNumber)) {
			contactInfo.setMobileNumber(new String[] { mobilePhoneNumber });
		}
		customerInfo.setContactInfo(contactInfo);

		bankingProductOnline.setCustomerInfo(customerInfo);

		NewFoOnlineService onlineService = new NewFoOnlineService();
		onlineService.setSource("OMNI");
		onlineService.setRegisterDate(dateFormat.format(new Date()));
		onlineService.setIsOpenAtm("1");
		onlineService.setProductType("Account");
		bankingProductOnline.setOnlineService(onlineService);
		onlineService.setCardExpectedLimit(String.valueOf(registerBankingProductOnlineDTO.getAmount()));
		// onlineService.setCreditCardLimit(String.valueOf(registerBankingProductOnlineDTO.getAmount()));
		onlineService.setCardType(registerBankingProductOnlineDTO.getCardTypeCode());
		onlineService.setContactPersonMobile(personalInfo.getMobilePhoneNumber());
		onlineService.setHasCreditCard("1");

		NewFoTransactionInfo newFoTransactionInfo = accountService.registerBankingProductOnline(omniToken,
				bankingProductOnline);

		return newFoTransactionInfo;
	}

	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis());
	}

	@Override
	public AvatarInfoOutputDTO uploadAvatar(String file) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.uploadAvatar(omniToken, file);
	}

	@Override
	public Boolean updateProductDefaultDirectly(Map<String, String> map) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.updateProductDefaultDirectly(omniToken, map);
	}

	@Override
	public List<TransferAccountDTO> transferAccounts(String productList, String restrictions) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.transferAccounts(omniToken, productList, restrictions);
	}

	@Override
	public QRcodeInfoOutputDTO checkQRcodeInfo(QRcodeInfoInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.checkQRcodeInfo(omniToken, input);
	}

	@Override
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String idFuturePayment) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.getDetailPaymentsInFuture(omniToken, idFuturePayment);
	}

	@Override
	public Boolean vetifyUserDevice(VetifyUserDeviceDTO verifyOtpToken) {
		String omniToken = AppUtil.getCurrentCPBToken();
		VerifyOtpToken otpToken = new VerifyOtpToken();
		otpToken.setAuthId(verifyOtpToken.getAuthId());
		otpToken.setOtpValue(verifyOtpToken.getOtpValue());
		checkOTP(verifyOtpToken.getOtpMethod(), otpToken, omniToken);
		String cif = AppUtil.getCurrentCustomerCif();
		resourceService.updateUserDeviceStatus(cif, verifyOtpToken.getDeviceId(), 1);
		return true;
	}

	@Override
	public ChangeUsernameOutput changeUsername(ChangeUsernameInput changeUsername) {
		String omniToken = AppUtil.getCurrentCPBToken();
		checkOTP(changeUsername.getOtpMethod(), changeUsername.getOtpValue(), omniToken);
		return accountService.changeUsername(omniToken, changeUsername);
	}

	@Override
	public AccountDetails findAccountDetail(String accountNum) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountService.findAccountDetail(omniToken, accountNum);
	}

}
