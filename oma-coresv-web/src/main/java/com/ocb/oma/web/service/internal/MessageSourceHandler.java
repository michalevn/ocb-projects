/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 * @author Phu Hoang
 *
 */
@Service
public class MessageSourceHandler {
	@Autowired
	MessageSource messageSource;

	public String getMessage(String code, Locale locale) {

		if (locale != null)
			return messageSource.getMessage(code, null, code, locale);
		return messageSource.getMessage(code, null, code, Locale.US);
	}
}
