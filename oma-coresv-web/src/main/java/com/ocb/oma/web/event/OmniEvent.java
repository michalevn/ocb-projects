/**
 * 
 */
package com.ocb.oma.web.event;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.mobile.device.Device;

import com.ocb.oma.dto.UserToken;

/**
 * @author phuhoang
 *
 */
public class OmniEvent extends ApplicationEvent {

	private static final Log LOG = LogFactory.getLog(OmniEvent.class);
	private UserToken userToken;
	private Device deviceInfo;

	private static final long serialVersionUID = 1L;

	/**
	 * @param source
	 */
	public OmniEvent(Object source) {
		super(source);
	}

	public OmniEvent(UserToken userToken, Device deviceInfo, Object source) {
		super(source);
		setUserToken(userToken);
		this.deviceInfo = deviceInfo;
	}

	/**
	 * @param userToken2
	 * @param source
	 */
	public OmniEvent(UserToken userToken, Object source) {
		super(source);
		setUserToken(userToken);
	}

	/**
	 * @return the userToken
	 */
	public UserToken getUserToken() {
		return userToken;
	}

	/**
	 * @param userToken the userToken to set
	 */
	public void setUserToken(UserToken userToken) {
		try {
			this.userToken = (UserToken) BeanUtils.cloneBean(userToken);
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (InstantiationException e) {
			LOG.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * @return the deviceInfo
	 */
	public Device getDeviceInfo() {
		return deviceInfo;
	}

	/**
	 * @param deviceInfo the deviceInfo to set
	 */
	public void setDeviceInfo(Device deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

}
