/**
 * 
 */
package com.ocb.oma.web.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.ocb.oma.web.event.LoginEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author phuhoang
 *
 */
@Component
public class LoginEventListener implements ApplicationListener<LoginEvent> {

	@Autowired
	private ResourceServiceExt resourceServiceExt;

	@Override
	public void onApplicationEvent(LoginEvent event) {

	}
}
