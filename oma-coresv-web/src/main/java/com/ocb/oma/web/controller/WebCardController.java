/**
	 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeLimitDirectlyInput;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PostAuthActivateCardInput;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.web.service.internal.WebCardService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/card")
public class WebCardController extends BaseController {

	private static final Log LOG = LogFactory.getLog(WebCardController.class);

	@Autowired
	private WebCardService cardService;

	@RequestMapping(value = "/findCard")
	public ResponseEntity<?> findCard() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<CardInfoDTO> rs = cardService.findCard();
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/restrictCard")
	public ResponseEntity<?> restrictCard(@RequestParam("cardId") String cardId,
			@RequestParam("cardRestrictionReason") String restrictionReason) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.restrictCard(cardId, restrictionReason));
	}

	@RequestMapping(value = "/findCardBlockades")
	public ResponseEntity<?> findCardBlockades(@RequestBody CardBlockadeInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.findCardBlockades(input));
	}

	@RequestMapping(value = "/postAuthActivateCard")
	public ResponseEntity<?> postAuthActivateCard(@RequestParam("cardId") String cardId,
			@RequestBody PostAuthActivateCardInput postAuthActivateCardInput) {

		LOG.info("postAuthActivateCard");

		if (StringUtils.isBlank(postAuthActivateCardInput.getCardId())) {
			postAuthActivateCardInput.setCardId(cardId);
		}

		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.postAuthActivateCard(postAuthActivateCardInput));
	}

	@RequestMapping(value = "/modifyCardAccountAutoRepayment")
	public ResponseEntity<?> modifyCardAccountAutoRepayment(
			@RequestBody CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		LOG.info("modifyCardAccountAutoRepayment");
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Boolean rs = cardService.modifyCardAccountAutoRepayment(cardAccountAutoRepayment);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/findCardAccountEntry")
	public ResponseEntity<?> findCardAccountEntry(@RequestBody CardAccountEntryQueryDTO cardAccountEntryQuery) {
		LOG.info("findCardAccountEntry");
		List<CardAccountEntryDTO> rs = cardService.findCardAccountEntry(cardAccountEntryQuery);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/getPaymentDetails")
	public PaymentsInFutureDTO getPaymentDetails(@RequestParam("paymentId") String paymentId) {
		return cardService.getPaymentDetails(paymentId);
	}

	@RequestMapping(value = "/postpaymentCardAnother")
	public ResponseEntity<?> postpaymentCardAnother(@RequestBody PaymentCardAnotherInputDTO input) {
		LOG.info("postpaymentCardAnother");
		if (input.getCurrency() == null || input.getCurrency().isEmpty()) {
			input.setCurrency("VND");
		}
		return getFinalResponse(cardService.postpaymentCardAnother(input));
	}

	@RequestMapping(value = "/findCardByCardId")
	public ResponseEntity<?> findCardByCardId(@RequestParam("card_id") String cardId) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.findCardByCardId(cardId));
	}

	@RequestMapping(value = "/cardAutoRepayment")
	public ResponseEntity<?> cardAutoRepayment() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.cardAutoRepayment());
	}

	@RequestMapping(value = "/cardRejected")
	public ResponseEntity<?> cardRejected(@RequestBody CardRejectedInputDTO cardRejectedInput) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.cardRejected(cardRejectedInput));
	}

	@RequestMapping(value = "/cardStatements")
	public ResponseEntity<?> cardStatements(@RequestBody CardStatementsInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(cardService.cardStatements(input));
	}

	@RequestMapping(value = "/changeLimitDirectly")
	public ResponseEntity<?> changeLimitDirectly(@RequestBody ChangeLimitDirectlyInput input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Boolean rs = cardService.changeLimitDirectly(input);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/configStatusEcommerceCard")
	public ResponseEntity<?> configStatusEcommerceCard(@RequestBody ConfigStatusEcommerceCardInput input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		ConfigStatusEcommerceCardOutput rs = cardService.configStatusEcommerceCard(input);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/cardPaymentInfo")
	public ResponseEntity<?> cardPaymentInfo(@RequestBody CardPaymentInfoInput input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		CardPaymentInfoOutput rs = cardService.cardPaymentInfo(input);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/cardStatemenDownload")
	ResponseEntity<?> cardStatemenDownload(@RequestParam("linkStatementFile") String linkStatementFile,
			HttpServletResponse response) throws IOException {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		InputStream is = cardService.cardStatemenDownload(linkStatementFile);
		String[] fileNameArr = linkStatementFile.split("/");
		String fileName = fileNameArr[fileNameArr.length - 1];
		response.addHeader("Content-disposition", "attachment;filename=" + fileName);
		response.setContentType("application/pdf");

		ByteArrayOutputStream t = new ByteArrayOutputStream();
		IOUtils.copy(is, t);
		// String finalString = new String();
		String finalString = Base64.getEncoder().encodeToString(t.toByteArray());
		return getFinalResponse(finalString);

	}

}
