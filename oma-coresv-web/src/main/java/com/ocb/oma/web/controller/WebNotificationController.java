/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/notifications")
public class WebNotificationController extends BaseController {

	@Autowired
	private ResourceServiceExt resourceServiceExt;

	@RequestMapping(value = "/push")
	public ResponseEntity<?> push(@RequestBody OmniNotification omniNotification) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Boolean rs = resourceServiceExt.pushNotification(omniNotification);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/maskAsRead")
	public ResponseEntity<?> maskAsRead(@RequestBody List<Long> notificationIds) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String cif = AppUtil.getCurrentCustomerCif();
		Boolean rs = resourceServiceExt.maskAsRead(cif, notificationIds);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/maskAsUnread")
	public ResponseEntity<?> maskAsUnread(@RequestBody List<Long> notificationIds) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String cif = AppUtil.getCurrentCustomerCif();
		Boolean rs = resourceServiceExt.maskAsUnread(cif, notificationIds);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/delete")
	public ResponseEntity<?> delete(@RequestBody List<Long> notificationIds) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String cif = AppUtil.getCurrentCustomerCif();
		Boolean rs = resourceServiceExt.deleteNotifications(cif, notificationIds);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/findByType")
	public ResponseEntity<?> findByType(@RequestParam(value = "query", required = false) String query,
			@RequestParam("type") String type, @RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Integer notiType = 0;
		if ("THONG_BAO".equals(type)) {
			notiType = OmniNotification.TYPE_THONG_BAO;
		} else if ("KHUYEN_MAI".equals(type)) {
			notiType = OmniNotification.TYPE_KHUYEN_MAI;
		} else if (OmniNotification.TYPE_THONG_BAO.toString().equals(type)
				|| OmniNotification.TYPE_KHUYEN_MAI.equals(type)) {
			notiType = Integer.valueOf(type);
		}

		String userId = userToken.getOldAuthen().getCustomerId();
		List<OmniNotification> rs = resourceServiceExt.findByUserIdAndType(query, userId, notiType, page, size);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/get")
	public ResponseEntity<?> get(@RequestParam("notificationId") Long notificationId) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		OmniNotification omniNotification = resourceServiceExt.get(notificationId);
		return getFinalResponse(omniNotification);
	}

	@RequestMapping(value = "/countUnread/{durationInDays}")
	public ResponseEntity<?> countUnread(@PathVariable("durationInDays") Integer durationInDays) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String cif = AppUtil.getCurrentCustomerCif();
		Integer unreadNo = resourceServiceExt.countUnreadNotifications(cif, durationInDays);
		return getFinalResponse(unreadNo);
	}
}
