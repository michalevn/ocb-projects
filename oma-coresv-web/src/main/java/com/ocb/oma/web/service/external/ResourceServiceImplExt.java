/**
* 
*/
package com.ocb.oma.web.service.external;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.CardTypeDTO;
import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.resources.model.OmniCardType;
import com.ocb.oma.resources.model.OmniGiftCardMsgTemplate;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author phuhoang
 *
 */
@Service
public class ResourceServiceImplExt implements ResourceServiceExt {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	@Autowired
	protected ObjectMapper objectMapper;

	private static final Log LOG = LogFactory.getLog(ResourceServiceImplExt.class);

	private String serviceUrl = "http://RESOURCES-SERVICE";

	private HttpHeaders getHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		return headers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.ResourceServiceExt#getBankInfos()
	 */
	@Override
	@Cacheable(value = "getBankInfos")
	public List<BankInfoDTO> getBankInfos() {
		HttpHeaders headers = getHeader();
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(serviceUrl + "/resources/bankList"));

		ResponseEntity<List<BankInfoDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<BankInfoDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#findListOfValue(java.lang
	 * .String)
	 */
	@Override
	public List<ListOfValueDTO> findListOfValue(String category) {
		HttpHeaders headers = getHeader();
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(serviceUrl + "/resources/listOfValue?lovType=" + category));

		ResponseEntity<List<ListOfValueDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<ListOfValueDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<LinkDTO> getLinksByGroup(String group) {
		HttpHeaders headers = getHeader();
		String url = serviceUrl + "/resources/getLinksByGroup";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
		uriBuilder.queryParam("group", group);
		RequestEntity<LinkDTO> request = new RequestEntity<>(headers, HttpMethod.GET, uriBuilder.build().toUri());
		ResponseEntity<List<LinkDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<LinkDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#searchBankCitad(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public List<BankInfoDTO> searchBankCitad(String bankCode, String provinceCode) {
		HttpHeaders headers = getHeader();
		String requestParam = "?provinceCode=";
		if (provinceCode != null) {
			requestParam += provinceCode;
		}
		requestParam += "&bankCode=";
		if (bankCode != null) {
			requestParam += bankCode;
		}
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(serviceUrl + "/resources/searchBankCitad" + requestParam));

		ResponseEntity<List<BankInfoDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<BankInfoDTO>>() {
				});
		return response.getBody();
	}

	@Cacheable(value = "getByBankCitadId")
	@Override
	public String getByBankCitadId(String bankCitadCode) {
		try {
			String requestURl = "/resources/getByBankCitadId?bankCitadCode=" + bankCitadCode.trim();
			HttpHeaders headers = getHeader();
			RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
					URI.create(serviceUrl + requestURl));

			ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
			});
			return response.getBody();
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info("error when get bankcode with  bankCitadCode " + bankCitadCode, e);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#findGiftCardMsgTemplate(
	 * java.lang.String)
	 */
	@Override
	public List<OmniGiftCardMsgTemplate> findGiftCardMsgTemplate(String cardType) {
		HttpHeaders headers = getHeader();

		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(serviceUrl + "/OmniGiftCardMsgTemplate/search/findCardInfoByCardType?cardType=" + cardType
						+ "&order=orderNumber,asc"));

		ResponseEntity<Map<String, Object>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<Map<String, Object>>() {
				});

		List<OmniGiftCardMsgTemplate> result = convertValue(response.getBody(),
				new TypeReference<List<OmniGiftCardMsgTemplate>>() {
				}, "OmniGiftCardMsgTemplate");
		// return response.getBody();
		return result;
	}

	public <T> T convertValue(Map<String, Object> data1, TypeReference<T> toValueTypeRef, String keyToGet) {

		Map<String, Object> data2 = (Map<String, Object>) (data1).get("_embedded");
		List<Object> data3 = (List<Object>) data2.get(keyToGet);
		T result = objectMapper.convertValue(data3, toValueTypeRef);

		return result;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#findUserDevices(java.
	 * lang.String)
	 */
	@Override
	public List<UserDeviceToken> findUserDevices(String cif) {
		HttpHeaders headers = getHeader();

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(serviceUrl + "/userDevices/findUserDevices?cif=" + cif));

		ResponseEntity<List<UserDeviceToken>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<UserDeviceToken>>() {
				});

		List<UserDeviceToken> userDeviceTokens = response.getBody();
		return userDeviceTokens;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#updateUserDeviceStatus(
	 * java.lang.String, java.lang.Long, java.lang.Boolean)
	 */
	@Override
	public Boolean updateUserDeviceStatus(String cif, String deviceId, Integer status) {

		HttpHeaders headers = getHeader();
		UserDeviceToken input = new UserDeviceToken();
		input.setCif(cif);
		input.setDeviceId(deviceId);
		input.setStatus(status);

		RequestEntity<UserDeviceToken> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(serviceUrl + "/userDevices/updateUserDeviceStatus"));

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#sendGiftCardResource(com.
	 * ocb.oma.dto.GiftCardInsertDTO)
	 */
	@Override
	public void sendGiftCardResource(GiftCardInsertDTO giftCardInsertDTO, String omniToken) {

		HttpHeaders headers = getHeader();
		headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + omniToken);

		RequestEntity<GiftCardInsertDTO> request = new RequestEntity<>(giftCardInsertDTO, headers, HttpMethod.POST,
				URI.create(serviceUrl + "/resources/sendGiftCard"));

		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		response.getBody();
	}

	@Override
	public Boolean pushNotification(OmniNotification omniNotification) {
		HttpHeaders headers = getHeader();
		RequestEntity<OmniNotification> request = new RequestEntity<>(omniNotification, headers, HttpMethod.POST,
				URI.create(serviceUrl + "/notifications/push"));
		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		return response.getBody();

	}

	@Override
	public List<OmniNotification> findByUserIdAndType(String query, String userId, Integer type, Integer page,
			Integer size) {
		HttpHeaders headers = getHeader();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(serviceUrl + "/notifications/findByUserIdAndType");
		uriBuilder.queryParam("query", query);
		uriBuilder.queryParam("userId", userId);
		uriBuilder.queryParam("type", type);
		uriBuilder.queryParam("page", page);
		uriBuilder.queryParam("size", size);
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<OmniNotification>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<OmniNotification>>() {
				});
		List<OmniNotification> rs = response.getBody();
		return rs;
	}

	@Override
	public OmniNotification get(Long notificationId) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + "/notifications/get");
		uriBuilder.queryParam("notificationId", notificationId);
		uriBuilder.queryParam("cif", AppUtil.getCurrentCustomerCif());
		HttpHeaders headers = getHeader();

		RequestEntity<Long> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<OmniNotification> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OmniNotification>() {
				});
		OmniNotification omniNotification = response.getBody();
		return omniNotification;
	}

	@Override
	public List<CardTypeDTO> findCardTypes(String group) {

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + "/cardTypes/find");
		uriBuilder.queryParam("group", group);

		HttpHeaders headers = getHeader();
		RequestEntity<Long> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<OmniCardType>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<OmniCardType>>() {
				});
		List<OmniCardType> omniCardTypes = response.getBody();
		List<CardTypeDTO> dtos = new ArrayList<>();
		TypeReference<List<Map<String, String>>> mapType = new TypeReference<List<Map<String, String>>>() {
		};
		for (OmniCardType omniCardType : omniCardTypes) {
			CardTypeDTO dto = new CardTypeDTO();
			dto.setCardTypeCode(omniCardType.getCardTypeCode());
			dto.setCardTypeName(omniCardType.getCardTypeName());
			dto.setCardTypeImage(omniCardType.getCardTypeImage());
			dto.setCardTypeGroup(omniCardType.getCardTypeGroup());
			if (StringUtils.isNotBlank(omniCardType.getSubCards())) {
				try {
					dto.setSubCards(objectMapper.readValue(omniCardType.getSubCards(), mapType));
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<TuitionFeeGroup> findTuitionFeeGroups() {
		HttpHeaders headers = getHeader();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + "/tuition-fee/findGroups");
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<TuitionFeeGroup>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TuitionFeeGroup>>() {
				});
		List<TuitionFeeGroup> rs = response.getBody();
		return rs;
	}

	@Override
	public List<TuitionFeeObject> findTuitionFeesByGroup(String groupCode, int page, int size) {
		HttpHeaders headers = getHeader();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + "/tuition-fee/findByGroup");
		uriBuilder.queryParam("groupCode", groupCode);
		uriBuilder.queryParam("page", page);
		uriBuilder.queryParam("size", size);
		RequestEntity<BankInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<TuitionFeeObject>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TuitionFeeObject>>() {
				});
		List<TuitionFeeObject> rs = response.getBody();
		return rs;
	}

	@Override
	public String creatForm(String omniToken, FormDTO formDTO) {
		HttpHeaders headers = getHeader();
		RequestEntity<FormDTO> request = new RequestEntity<>(formDTO, headers, HttpMethod.POST,
				URI.create(serviceUrl + "/forms"));
		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	@Override
	public FormDTO findFormByUuid(String uuid) {
		HttpHeaders headers = getHeader();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + "/forms/" + uuid);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<FormDTO> response = restTemplate.exchange(request, new ParameterizedTypeReference<FormDTO>() {
		});
		return response.getBody();
	}

	@Override
	public Boolean registerUserDevice(UserDeviceToken userDeviceToken) {

		HttpHeaders headers = getHeader();

		String path = "/userDevices/registerUserDevice";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<UserDeviceToken> request = new RequestEntity<>(userDeviceToken, headers, HttpMethod.POST, uri);
		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public Map<String, Object> getByBankInfoByBranchCode(String branchCode) {

		HttpHeaders headers = getHeader();

		String path = "/resources/getByBankInfoByBranchCode";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("branchCode", branchCode);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);
		ResponseEntity<Map> response = restTemplate.exchange(request, Map.class);
		return response.getBody();
	}

	@Override
	public Boolean maskAsRead(String cif, List<Long> notificationIds) {

		HttpHeaders headers = getHeader();

		String path = "/notifications/maskAsRead/" + cif;
		URI uri = URI.create(serviceUrl + path);

		RequestEntity<List<Long>> request = new RequestEntity<>(notificationIds, headers, HttpMethod.POST, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public Boolean maskAsUnread(String cif, List<Long> notificationIds) {

		HttpHeaders headers = getHeader();

		String path = "/notifications/maskAsUnread/" + cif;
		URI uri = URI.create(serviceUrl + path);

		RequestEntity<List<Long>> request = new RequestEntity<>(notificationIds, headers, HttpMethod.POST, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public Boolean deleteNotifications(String cif, List<Long> notificationIds) {

		HttpHeaders headers = getHeader();

		String path = "/notifications/delete/" + cif;
		URI uri = URI.create(serviceUrl + path);

		RequestEntity<List<Long>> request = new RequestEntity<>(notificationIds, headers, HttpMethod.POST, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public Boolean registerDevice(OmniMobileDevice mobileDevice) {

		HttpHeaders headers = getHeader();

		String path = "/mobileDevices/registerDevice";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<OmniMobileDevice> request = new RequestEntity<>(mobileDevice, headers, HttpMethod.POST, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public UserDeviceToken activeUserDevice(String cif, String deviceId, String deviceToken) {

		HttpHeaders headers = getHeader();

		String path = "/userDevices/activeUserDevice";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("cif", cif);
		uriBuilder.queryParam("deviceId", deviceId);
		uriBuilder.queryParam("deviceToken", deviceToken);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);

		ResponseEntity<UserDeviceToken> response = restTemplate.exchange(request, UserDeviceToken.class);
		return response.getBody();
	}

	@Override
	public Integer countUnreadNotifications(String cif, Integer durationInDays) {

		HttpHeaders headers = getHeader();

		String path = "/notifications/countUnread";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("cif", cif);
		uriBuilder.queryParam("durationInDays", durationInDays);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);

		ResponseEntity<Integer> response = restTemplate.exchange(request, Integer.class);
		return response.getBody();
	}

	@Override
	public Boolean deactivateUserDevice(String cif) {

		HttpHeaders headers = getHeader();

		String path = "/userDevices/deactivate";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("cif", cif);
		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public boolean isActiveUserDevice(String cif, String deviceId) {

		HttpHeaders headers = getHeader();

		String path = "/userDevices/isActiveUserDevice";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("cif", cif);
		uriBuilder.queryParam("deviceId", deviceId);

		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);

		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public UserDeviceToken getUserDevice(String cif, String deviceId) {
		HttpHeaders headers = getHeader();

		String path = "/userDevices/getUserDevice";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);
		uriBuilder.queryParam("cif", cif);
		uriBuilder.queryParam("deviceId", deviceId);
		URI uri = URI.create(uriBuilder.toUriString());
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);
		ResponseEntity<UserDeviceToken> response = restTemplate.exchange(request, UserDeviceToken.class);
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.ResourceServiceExt#getBankByCodeNapas(java.
	 * lang.String)
	 */
	@Override
	public BankInfoDTO getBankByCodeNapas(String codeNapas) {
		HttpHeaders headers = getHeader();

		String path = "/resources/getByCodeNapas?codeNapas=" + codeNapas;

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(serviceUrl + path);

		URI uri = URI.create(uriBuilder.toUriString());

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, uri);

		ResponseEntity<BankInfoDTO> response = restTemplate.exchange(request, BankInfoDTO.class);
		return response.getBody();
	}

	@Override
	public String sendMailForm(FormDTO formDTO) {
		HttpHeaders headers = getHeader();
		RequestEntity<FormDTO> request = new RequestEntity<>(formDTO, headers, HttpMethod.POST,
				URI.create(serviceUrl + "/forms/sendMailCus"));
		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();

	}

}