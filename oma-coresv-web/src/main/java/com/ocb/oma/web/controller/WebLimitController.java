/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.WebLimitDTO;
import com.ocb.oma.web.service.internal.WebLimitService;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/webLimit")
public class WebLimitController extends BaseController {

	@Autowired
	private WebLimitService webLimitService;

	@RequestMapping(value = "/get")
	public ResponseEntity<?> get() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		WebLimitDTO dto = webLimitService.get();
		return getFinalResponse(dto);
	}
}
