/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostBasketsInput;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/paymentBasket")
//@Validated
public class WebPaymentBasketController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	//
	@RequestMapping(value = "/countBasketPaymentToProcess")
	public ResponseEntity<?> countBasketPaymentToProcess() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.countBasketPaymentToProcess());
	}

	@RequestMapping(value = "/getTransactionFee")
	public ResponseEntity<?> getTransactionFee(@RequestBody TransactionFeeInputDTO input) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getTransactionFee(input));
	}

	@RequestMapping(value = "/postPayment")
	public ResponseEntity<?> postPayment(@RequestBody PostPaymentInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.postPayment(input));
	}

	@RequestMapping(value = "/postAddBasket")
	public ResponseEntity<?> postAddBasket(@RequestBody PostPaymentInputDTO input, Device deviceInfo) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		PostAddBasketDTO result = accountService.postAddBasket(input);

		return getFinalResponse(result);
	}

	@RequestMapping(value = "/getListofBasket")
	@DateRange(from = "realizationDateFrom", to = "realizationDateTo", message = "realizationDateFrom.must.be.less.than.or.equal.to.realizationDateTo")
	public ResponseEntity<?> getListofBasket(@RequestParam("amountFrom") Double amountFrom,
			@RequestParam("amountTo") Double amountTo, @RequestParam("customerId") String customerId,
			@RequestParam("realizationDateFrom") String realizationDateFrom,
			@RequestParam("realizationDateTo") String realizationDateTo, @RequestParam("statuses") String statuses) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getListofBasket(amountFrom, amountTo, customerId, realizationDateFrom,
				realizationDateTo, statuses));
	}

	@RequestMapping(value = "/deleteBasket")
	public ResponseEntity<?> deleteBasket(@RequestParam("basketId") String basketId) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.deleteBasket(basketId));
	}

	@RequestMapping(value = "/getListOfBillTransactions")
	public ResponseEntity<?> getListOfBillTransactions(@RequestBody GetListOfBillTransactionsInputDTO dto,
			HttpServletRequest request) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getListOfBillTransactions(dto));
	}

	@RequestMapping(value = "/getListBillPaymentByService")
	public ResponseEntity<?> getListBillPaymentByService(HttpServletRequest request,
			@RequestParam("serviceCode") String serviceCode, @Digits @RequestParam("duration") Integer duration) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getListBillPaymentByService(serviceCode, duration));
	}

	@RequestMapping(value = "/getBill")
	public ResponseEntity<?> getBill(@RequestBody GetBillInputDTO input, HttpServletRequest request) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getBill(input));
	}

	@RequestMapping(value = "/getListOfServiceProvider")
	public ResponseEntity<?> getListOfServiceProvider(HttpServletRequest request) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getListOfServiceProvider());
	}

	@RequestMapping(value = "/loadEVNparameter")
	public ResponseEntity<?> loadEVNparameter() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<EVNParameter> result = accountService.loadEVNparameter();

		return getFinalResponse(result);
	}

	@RequestMapping(value = "/updateBasket")
	public ResponseEntity<?> updateBasket(@RequestBody PostPaymentInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.updateBasket(input));
	}

	@RequestMapping(value = "/postBaskets")
	public ResponseEntity<?> postBaskets(@RequestBody PostBasketsInput input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}

		PostListBasketDTO result = accountService.postBaskets(input);
		return getFinalResponse(result);
	}

	@RequestMapping(value = "/getPaymentsInFuture")
	public ResponseEntity<?> getPaymentsInFuture(@RequestBody PaymentsInFutureInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getPaymentsInFuture(input));
	}
	
	@RequestMapping(value = "/getDetailPaymentsInFuture")
	public ResponseEntity<?> getDetailPaymentsInFuture(@RequestParam("idFuturePayment") String idFuturePayment) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getDetailPaymentsInFuture(idFuturePayment));
	}
}
