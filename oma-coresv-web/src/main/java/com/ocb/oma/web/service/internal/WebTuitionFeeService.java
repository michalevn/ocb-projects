/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;

/**
 * @author docv
 *
 */
public interface WebTuitionFeeService {

	List<TuitionFeeGroup> findGroups();

	List<TuitionFeeObject> findByGroup(String groupCode, int page, int size);

}
