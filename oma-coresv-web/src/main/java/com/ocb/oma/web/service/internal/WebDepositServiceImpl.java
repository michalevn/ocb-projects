/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.input.BreakDepositInputDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebDepositServiceImpl implements WebDepositService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Autowired
	private WebAccountSerivice webAccountService;

	@Override
	public OutputBaseDTO breakDeposit(BreakDepositInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		return accountServiceExt.breakDeposit(omniToken, input.getDepositId());
	}

	@Override
	public List<DepositDTO> getDeposits() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getDeposits(omniToken);
	}

	@Override
	public List<DepositProductDTO> getDepositProducts() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getDepositProducts(omniToken);
	}

	@Override
	public List<DepositInterestRateDTO> getDepositInterestRates(String currency, String subProductCode) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getDepositInterestRates(omniToken, currency, subProductCode);
	}

	@Override
	public CreateDepositOutputDTO createDeposit(CreateDepositDTO deposit) {
		String omniToken = AppUtil.getCurrentCPBToken();

		webAccountService.checkOTP(deposit.getOtpMethod(), deposit.getOtpValue(), omniToken);

		return accountServiceExt.createDeposit(omniToken, deposit);
	}

	@Override
	public DepositOfferAccountsDTO getDepositOfferAcc() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getDepositOfferAcc(omniToken);
	}

	@Override
	public DepositDTO getDeposit(String depositId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getDeposit(omniToken, depositId);
	}

}
