/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.web.event.LogoutEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.service.internal.TokenManagementService;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/logoutService")
public class LogoutController extends BaseController {

	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	private TokenManagementService tokenManagement;

	@Autowired
	private ResourceServiceExt resourceService;

	@RequestMapping(value = "")
	public ResponseEntity<?> logoutService(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			String cif = AppUtil.getCurrentCustomerCif();
			// tokenManagement.invalidToken(token.replace(TOKEN_PREFIX, "").trim());
			applicationEventPublisher.publishEvent(new LogoutEvent(token));
			resourceService.deactivateUserDevice(cif);
		}
		return getFinalResponse(true);
	}

	@RequestMapping(value = "/invalidToken")
	public ResponseEntity<?> invalidToken(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			tokenManagement.invalidToken(token.replace(TOKEN_PREFIX, "").trim());
		}
		return getFinalResponse(true);
	}
}
