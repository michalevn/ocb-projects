/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import com.ocb.oma.dto.GeoLocationPoiDTO;

public interface WebGeoLocationService {

	GeoLocationPoiDTO find(Double lat, Double lng, String poiType);
}
