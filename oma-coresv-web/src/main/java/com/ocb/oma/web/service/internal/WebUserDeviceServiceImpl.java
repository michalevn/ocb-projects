/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.event.DeactiveDeviceEvent;
import com.ocb.oma.web.event.OmniEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebUserDeviceServiceImpl implements WebUserDeviceService {

	@Autowired
	private ResourceServiceExt resourcesService;

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	protected void publicEvent(OmniEvent omniEvent) {
		applicationEventPublisher.publishEvent(omniEvent);
	}

	@Override
	public Boolean updateUserDeviceStatus(String cif, String deviceId, int enable) {
		Boolean result = resourcesService.updateUserDeviceStatus(cif, deviceId, enable);
		UserToken userTokens = AppUtil.getCurrentUserLogin();
		UserDeviceToken userDeviceToken = resourcesService.getUserDevice(cif, deviceId);
		OmniMobileDevice mobileDevice = userDeviceToken.getMobileDevice();
		String deviceName="";
		if(mobileDevice!=null) {
			deviceName = mobileDevice.getDeviceName();
		}
		publicEvent(new DeactiveDeviceEvent(userTokens, deviceName, enable));
		return result;
	}

}
