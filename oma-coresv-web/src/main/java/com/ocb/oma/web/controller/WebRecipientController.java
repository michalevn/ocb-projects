/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/recipient")
//@Validated
public class WebRecipientController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	//
	@RequestMapping(value = "/getRecipientList")
	public ResponseEntity<?> getRecipientList(
			@RequestParam(name = "filerTemplateType", required = false) String filerTemplateType,
			@RequestParam(name = "pageSize", required = false) Integer pageSize) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		if (filerTemplateType == null) {
			filerTemplateType = "";
		}
		if (pageSize == null) {
			pageSize = 0;
		}
		return getFinalResponse(accountService.getRecipientList(filerTemplateType, pageSize));
	}
	// verifyOTPToken

	@RequestMapping(value = "/deleteRecipient")
	public ResponseEntity<?> deleteRecipient(@RequestBody RecipientDTO data) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.deleteRecipient(data));
	}

	@RequestMapping(value = "/addRecipient")
	public ResponseEntity<?> addRecipient(@RequestBody RecipientInputDTO data) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.addRecipient(data));
	}

}
