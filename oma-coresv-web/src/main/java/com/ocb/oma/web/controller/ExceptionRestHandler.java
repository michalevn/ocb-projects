/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OmaExceptionDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.web.dto.ResponseDTO;

/**
 * @author Phu Hoang
 *
 */
@ControllerAdvice
@RestController
public class ExceptionRestHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionRestHandler.class);

	@ExceptionHandler(IllegalStateException.class)
	public final ResponseEntity<?> handleServiceUnavailable(IllegalStateException ex, WebRequest request) {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		errorDetails.setMessage(ex.getMessage());
		errorDetails.setErrorCode(MessageConstant.SERVICE_UNAVAILABLE);
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public final ResponseEntity<?> handleIllegalArgument(IllegalArgumentException ex, WebRequest request) {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		errorDetails.setMessage(ex.getMessage());
		errorDetails.setErrorCode(MessageConstant.MISSING_PARAMETER);
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Object> handleHttpRequestMethodNotSupported(WebRequest request) {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		errorDetails.setMessage(MessageConstant.SERVICE_NOT_FOUND);
		errorDetails.setErrorCode(MessageConstant.SERVER_ERROR);
		LOGGER.error("restError: ", request.getContextPath());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDetails);

	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public final ResponseEntity<?> handleValidations(MissingServletRequestParameterException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		errorDetails.setErrorCode(ex.getParameterName() + ".required");
		errorDetails.setMessage(ex.getMessage());
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDetails);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<?> handleValidations(ConstraintViolationException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
		if (constraintViolations != null) {
			for (ConstraintViolation<?> constraintViolation : constraintViolations) {
				errorDetails.setErrorCode(constraintViolation.getMessage());
				break;
			}
		} else {
			errorDetails.setErrorCode(ex.getMessage());
		}
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorDetails);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<?> handleValidations(MethodArgumentNotValidException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		BindingResult bindingResult = ex.getBindingResult();
		List<ObjectError> errors = bindingResult.getAllErrors();
		for (ObjectError objectError : errors) {
			FieldError fieldError = (FieldError) objectError;
			errorDetails.setErrorCode(fieldError.getDefaultMessage());
			break;
		}
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorDetails);
	}

	@ExceptionHandler(ValidationException.class)
	public final ResponseEntity<?> handleValidations(ValidationException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {
		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		if (ex.getCause() instanceof OmaException) {
			OmaException omaException = (OmaException) ex.getCause();
			errorDetails.setMessage(omaException.getDesc());
		} else {
			errorDetails.setMessage(ex.getMessage());
		}
		errorDetails.setErrorCode(MessageConstant.VALIDATION_ERROR);
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorDetails);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<?> handleUnknowException(Exception ex, WebRequest request) {

		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		// errorDetails.setMessage(messageSourceHandler.getMessage(MessageConstant.SERVER_ERROR,
		// request.getLocale()));
		errorDetails.setErrorCode(MessageConstant.SERVER_ERROR);
		errorDetails.setMessage(ex.getMessage());
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	}

	@ExceptionHandler(HttpServerErrorException.class)
	public final ResponseEntity<?> handleRestExceptionb(HttpServerErrorException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {

		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		// errorDetails.setMessage(messageSourceHandler.getMessage(MessageConstant.SERVER_ERROR,
		// request.getLocale()));
		// errorDetails.setErrorCode(ex.getResponseBodyAsString());
		byte[] responseText = ex.getResponseBodyAsByteArray();
		OmaExceptionDTO realEx = new Gson().fromJson(new String(responseText), OmaExceptionDTO.class);
		errorDetails.setErrorCode(realEx.getErrorCode());
		errorDetails.setMessage(realEx.getDescription());
		LOGGER.error("restError: ", realEx);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	}

	@ExceptionHandler(OmaException.class)
	public final ResponseEntity<?> handleOmaException(OmaException ex, WebRequest request)
			throws JsonParseException, JsonMappingException, IOException {

		ResponseDTO errorDetails = ResponseDTO.getFailureResponse();
		// errorDetails.setMessage(messageSourceHandler.getMessage(MessageConstant.SERVER_ERROR,
		// request.getLocale()));
		errorDetails.setErrorCode(ex.getMessage());
		errorDetails.setMessage(ex.getDesc());
		LOGGER.error("restError: ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDetails);
	}
}
