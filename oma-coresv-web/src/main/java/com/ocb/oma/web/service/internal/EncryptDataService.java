/**
 * @author phuhoang
 */
package com.ocb.oma.web.service.internal;

public interface EncryptDataService {

	public String encrypt(String toBeEncrypt);

	public String decrypt(String encrypted);
	
	String hash(Object input);
}
