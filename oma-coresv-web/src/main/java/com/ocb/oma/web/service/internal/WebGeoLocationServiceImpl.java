/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

@Service
public class WebGeoLocationServiceImpl implements WebGeoLocationService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public GeoLocationPoiDTO find(Double lat, Double lng, String poiType) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.findGeoLocationPois(omniToken, lat, lng, poiType);
	}

}
