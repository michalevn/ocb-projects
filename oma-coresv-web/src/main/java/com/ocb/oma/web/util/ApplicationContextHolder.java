/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.util;

import org.springframework.context.ApplicationContext;

/**
 * @author Phu Hoang
 *
 */
public class ApplicationContextHolder {

	private static ApplicationContext appContext;

	public static void setApplicationContext(ApplicationContext ctx) {
		appContext = ctx;
	}

	public static <T> T getBean(Class<T> x) {
		return appContext.getBean(x);
	}
}
