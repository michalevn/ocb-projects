/**
 * 
 */
package com.ocb.oma.web.listener;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.config.DeviceMagMessages;
import com.ocb.oma.web.event.DeactiveDeviceEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author phuhoang
 *
 */
@Component
public class DeactiveDeviceEventListener implements ApplicationListener<DeactiveDeviceEvent> {

	@Autowired
	private ResourceServiceExt resourceService;

	private static final Log LOG = LogFactory.getLog(PostPaymentEventListener.class);

	@Override
	public void onApplicationEvent(DeactiveDeviceEvent event) {
		int enable = event.getEnable();
		String title;
		String message;
		String userName = event.getUserToken().getUsername();
		String deviceName = event.getDeviceName();
		if (enable==1) {
			title = DeviceMagMessages.ENABLE_DEVICE_TITLE;
			Date datetime = new Date();
			message = DeviceMagMessages.ENABLE_DEVICE(userName, deviceName, datetime);
			pushNotification(title, message, event);
			System.out.println("test xong");
		} if (enable==0) {
			title = DeviceMagMessages.DISABLE_DEVICE_TITLE;
			Date datetime = new Date();
			message = DeviceMagMessages.DISABLE_DEVICE(userName, deviceName, datetime);
			pushNotification(title, message, event);
		}

	}

	private void pushNotification(String title, String message, DeactiveDeviceEvent event) {

		String cif = event.getUserToken().getOldAuthen().getCustomerId();

		try {

			Date executionDate = new Date();
			String description = Jsoup.parse(message).text();
			if (description.length() > 100) {
				description = description.substring(0, 100).concat("...");
			}
			OmniNotification omniNotification = new OmniNotification();
			omniNotification.setType(OmniNotification.TYPE_THONG_BAO.toString());
			omniNotification.setUserId(cif);
			int distributedType = OmniNotification.DISTRIBUTED_TYPE_APP | OmniNotification.DISTRIBUTED_TYPE_FIREBASE;
			omniNotification.setDistributedType(distributedType);
			omniNotification.setTitleVn(title);
			omniNotification.setTitleEl(title);
			omniNotification.setDescriptionVn(description);
			omniNotification.setDescriptionEl(description);
			omniNotification.setBodyVn(message);
			omniNotification.setBodyEl(message);
			omniNotification.setCreatedDate(executionDate);
			resourceService.pushNotification(omniNotification);

		} catch (Exception e) {
			LOG.error(e, e);
		}

	}
}
