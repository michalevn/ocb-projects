/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeLimitDirectlyInput;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PostAuthActivateCardInput;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebCardServiceImpl implements WebCardService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Autowired
	WebAccountSerivice webAccountService;

	@Override
	public List<CardInfoDTO> findCard() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.findCard(omniToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.internal.WebCardService#restrictCard(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	@Override
	public OutputBaseDTO restrictCard(String cardNumber, String restrictionReason) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.restrictCard(omniToken, cardNumber, restrictionReason);
	}

	@Override
	public List<CardBlockade> findCardBlockades(CardBlockadeInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.findCardBlockades(omniToken, input);
	}

	@Override
	public Boolean modifyCardAccountAutoRepayment(CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.modifyCardAccountAutoRepayment(omniToken, cardAccountAutoRepayment);
	}

	@Override
	public List<CardAccountEntryDTO> findCardAccountEntry(CardAccountEntryQueryDTO cardAccountEntryQuery) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.findCardAccountEntry(omniToken, cardAccountEntryQuery);
	}

	@Override
	public PaymentsInFutureDTO getPaymentDetails(String paymentId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getPaymentDetails(omniToken, paymentId);
	}

	@Override
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(PaymentCardAnotherInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		return accountServiceExt.postpaymentCardAnother(omniToken, input);
	}

	@Override
	public CardDetailByCardIdDTO findCardByCardId(String cardId) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.findCardByCardId(omniToken, cardId);
	}

	@Override
	public List<CardAutoRepaymentDTO> cardAutoRepayment() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.cardAutoRepayment(omniToken);
	}

	@Override
	public List<CardRejectedDTO> cardRejected(CardRejectedInputDTO cardRejectedInput) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.cardRejected(omniToken, cardRejectedInput);
	}

	@Override
	public OutputBaseDTO postAuthActivateCard(PostAuthActivateCardInput postAuthActivateCardInput) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(postAuthActivateCardInput.getOtpMethod(), postAuthActivateCardInput.getOtpValue(),
				omniToken);
		return accountServiceExt.postAuthActivateCard(omniToken, postAuthActivateCardInput.getCardId());
	}

	@Override
	public List<CardStatementsDTO> cardStatements(CardStatementsInputDTO input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.cardStatements(omniToken, input);
	}

	@Override
	public Boolean changeLimitDirectly(ChangeLimitDirectlyInput input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		webAccountService.checkOTP(input.getOtpMethod(), input.getOtpValue(), omniToken);
		return accountServiceExt.changeLimitDirectly(omniToken, input.getLimits());
	}

	@Override
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(ConfigStatusEcommerceCardInput input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.configStatusEcommerceCard(omniToken, input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.WebCardService#cardPaymentInfo(com.ocb.oma.
	 * dto.input.CardPaymentInfoInput)
	 */
	@Override
	public CardPaymentInfoOutput cardPaymentInfo(CardPaymentInfoInput input) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.cardPaymentInfo(omniToken, input);

	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public InputStream cardStatemenDownload(String linkStatementFile) throws IOException {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.cardPaymentInfo(omniToken, linkStatementFile);
	}
}
