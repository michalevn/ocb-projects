package com.ocb.oma.web.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class ProtectedConfigUtils {

	private static final byte[] SALT = new String("%}%,\\\"td4/LCXg^EHF7ESA%6qM?'jDn(wg[Vq_vA&6QkByDBZtMTTA4>Z^-X%Y>cz")
			.getBytes();

	public static String encrypt(String value, String password) {
		try {
			// Decreasing this speeds down startup time and can be useful during testing,
			// but it also makes it easier for brute force attackers
			int iterationCount = 40000;
			// Other values give me java.security.InvalidKeyException: Illegal key size or
			// default parameters
			int keyLength = 128;
			SecretKeySpec keySpec = createSecretKey(password.toCharArray(), SALT, iterationCount, keyLength);

			return encrypt(value, keySpec);

		} catch (Exception e) {
			return value;
		}
	}

	public static String decrypt(String encryptedValue, String encryptedKey) {

		try {
			// Decreasing this speeds down startup time and can be useful during testing,
			// but it also makes it easier for brute force attackers
			int iterationCount = 40000;
			// Other values give me java.security.InvalidKeyException: Illegal key size or
			// default parameters
			int keyLength = 128;
			SecretKeySpec keySpec = createSecretKey(encryptedKey.toCharArray(), SALT, iterationCount, keyLength);
			return decrypt(encryptedValue, keySpec);
		} catch (Exception e) {
			return encryptedValue;
		}

	}

	private static SecretKeySpec createSecretKey(char[] password, byte[] salt, int iterationCount, int keyLength)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
		PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
		SecretKey keyTmp = keyFactory.generateSecret(keySpec);
		return new SecretKeySpec(keyTmp.getEncoded(), "AES");
	}

	private static String encrypt(String property, SecretKeySpec key)
			throws GeneralSecurityException, UnsupportedEncodingException {
		Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		pbeCipher.init(Cipher.ENCRYPT_MODE, key);
		AlgorithmParameters parameters = pbeCipher.getParameters();
		IvParameterSpec ivParameterSpec = parameters.getParameterSpec(IvParameterSpec.class);
		byte[] cryptoText = pbeCipher.doFinal(property.getBytes("UTF-8"));
		byte[] iv = ivParameterSpec.getIV();
		return base64Encode(iv) + ":" + base64Encode(cryptoText);
	}

	private static String base64Encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	private static String decrypt(String encryptedValue, SecretKeySpec key)
			throws GeneralSecurityException, IOException {
		String[] parts = encryptedValue.split(":");
		if (parts.length != 2) {
			throw new IOException("Invalid Encrypted Value");
		}
		String iv = parts[0];
		String property = parts[1];
		Cipher pbeCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		pbeCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(base64Decode(iv)));
		return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
	}

	private static byte[] base64Decode(String property) throws IOException {
		return Base64.getDecoder().decode(property);
	}

}
