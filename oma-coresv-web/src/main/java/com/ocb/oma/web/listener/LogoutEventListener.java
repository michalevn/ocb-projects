/**
 * 
 */
package com.ocb.oma.web.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.ocb.oma.web.event.LogoutEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author docv
 *
 */
@Component
public class LogoutEventListener implements ApplicationListener<LogoutEvent> {

	@Autowired
	private ResourceServiceExt resourceService;

	private static final Log LOG = LogFactory.getLog(LogoutEventListener.class);

	@Override
	public void onApplicationEvent(LogoutEvent event) {
	}

}
