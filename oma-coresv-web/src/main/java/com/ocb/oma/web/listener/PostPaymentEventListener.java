/**
 * 
 */
package com.ocb.oma.web.listener;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.event.PostPaymentEvent;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author phuhoang
 *
 */
@Component
public class PostPaymentEventListener implements ApplicationListener<PostPaymentEvent> {

	private static final Log LOG = LogFactory.getLog(PostPaymentEventListener.class);

	@Autowired
	private ResourceServiceExt resourceService;

	@Autowired
	private AccountServiceExt accountService;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageSource messageSource;

	private static final Locale elLocal = new Locale("el");
	private static final Locale viLocal = new Locale("vi");
	private static final NumberFormat NUMBER_FORMAT = NumberFormat.getNumberInstance(Locale.GERMAN);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.
	 * springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(PostPaymentEvent event) {

		UserToken userToken = event.getUserToken();
		String omniToken = userToken.getOldAuthen().getJwtToken();
		String cif = userToken.getOldAuthen().getCustomerId();

		PostPaymentInputDTO data = event.getData();
		PaymentInfoDTO paymentInfo = data.getPaymentInfo();
		String paymentType = data.getPaymentType();

		String executionDate = formatDate(paymentInfo.getExecutionDate());
		String amount = formatNumber(paymentInfo.getAmount());
		String idNo = ""; // paymentInfo.get
		String studentCode = paymentInfo.getStudentCode();
		String customerCode = "";// paymentInfo.get
		String orderCode = paymentInfo.getBillCode();// paymentInfo.get;
		String phone = paymentInfo.getMobilePhoneNumber();

		String senderId = cif;
		LOG.info(">>> Sender: " + senderId);
		AccountInfo senderAccount = getAccountByAccountId(senderId, paymentInfo.getAccountId(), omniToken);
		String senderName = userToken.getOldAuthen().getFirstName();
		if (StringUtils.isBlank(senderName)) {
			senderName = "";
		}

		if (senderAccount == null) {
			LOG.error(">>> Sender Not Found: " + senderId);
			return;
		}

		String senderBalance = formatNumber(senderAccount.getCurrentBalance());
		String senderAccountNo = senderAccount.getAccountNo();

		String receiverAccountNo = paymentInfo.getCreditAccount();
		String receiverName = paymentInfo.getRecipient();

		String titleVi;
		String messageVi;
		String titleEl;
		String messageEl;

		// chuyển tiền trong hệ thống
		if (PaymentType.InternalPayment.getPaymentTypeCode().equals(paymentType)) {

			// nguoi gui

			String receiverId = getCif(receiverAccountNo, omniToken);

			titleVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.BODY_5", viLocal);
			String receiver = receiverAccountNo;
			if (StringUtils.isNotBlank(receiverName)) {
				receiver = receiver + ", " + receiverName;
			}
			messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, receiver, senderBalance);

			titleEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.TITLE", elLocal);
			messageEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.BODY_5", elLocal);
			messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, receiver, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);

			// nguoi nhan

			String senderInfo = senderAccountNo;
			if (StringUtils.isNotBlank(senderName)) {
				senderInfo = senderInfo + ", " + senderName;
			}

			titleVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.BODY_4", viLocal);
			messageVi = String.format(messageVi, receiverAccountNo, amount, executionDate, senderInfo);

			titleEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.TITLE", elLocal);
			messageEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.BODY_4", elLocal);
			messageEl = String.format(messageEl, receiverAccountNo, amount, executionDate, senderInfo);
			pushNotification(receiverId, titleVi, messageVi, titleEl, messageEl, event);
		}

		if (PaymentType.LocalPayment.getPaymentTypeCode().equals(paymentType)) {

			// chuyển tiền ngoài hệ thống

			// nguoi gui

			String receiver = receiverAccountNo;
			if (StringUtils.isNotBlank(receiverName)) {
				receiver = receiver + ", " + receiverName;
			}

			titleVi = getMessage("POST_PAYMENT.LOCAL_PAYMENT.SENDER.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.LOCAL_PAYMENT.SENDER.BODY_5", viLocal);
			messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, receiver, senderBalance);

			titleEl = getMessage("POST_PAYMENT.LOCAL_PAYMENT.SENDER.TITLE", elLocal);
			messageEl = getMessage("POST_PAYMENT.LOCAL_PAYMENT.SENDER.BODY_5", elLocal);
			messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, receiver, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);

		}

		if (PaymentType.FastTransfer.getPaymentTypeCode().equals(paymentType)) {

			// chuyển tiền 24/7

			if (StringUtils.isBlank(receiverAccountNo)) {
				receiverAccountNo = paymentInfo.getRecipientCardNumber();
			}

			String receiver = receiverAccountNo;
			if (StringUtils.isNotBlank(receiverName)) {
				receiver = receiver + ", " + receiverName;
			}

			titleVi = getMessage("POST_PAYMENT.FAST_TRANSFER.SENDER.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.FAST_TRANSFER.SENDER.BODY_5", viLocal);
			messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, receiver, senderBalance);

			titleEl = getMessage("POST_PAYMENT.FAST_TRANSFER.SENDER.TITLE", elLocal);
			messageEl = getMessage("POST_PAYMENT.FAST_TRANSFER.SENDER.BODY_5", elLocal);
			messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, receiver, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);

		}

		if (PaymentType.TuitionFee.getPaymentTypeCode().equals(paymentType)) {

			// thanh toán học phí

			String universityCode = event.getData().getPaymentInfo().getUniversityCode();

			if (StringUtils.isNotBlank(idNo)) {

				titleVi = getMessage("POST_PAYMENT.TUITION_FEE.MSSV.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.TUITION_FEE.CMND.BODY_6", viLocal);
				messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, universityCode, idNo,
						senderBalance);

				titleEl = getMessage("POST_PAYMENT.TUITION_FEE.CMND.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.TUITION_FEE.CMND.BODY_6", elLocal);
				messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, universityCode, idNo,
						senderBalance);

				pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);
			}

			if (StringUtils.isNotBlank(studentCode)) {

				titleVi = getMessage("POST_PAYMENT.TUITION_FEE.MSSV.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.TUITION_FEE.MSSV.BODY_6", viLocal);
				messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, universityCode,
						studentCode, senderBalance);

				titleEl = getMessage("POST_PAYMENT.TUITION_FEE.MSSV.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.TUITION_FEE.MSSV.BODY_6", elLocal);
				messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, universityCode,
						studentCode, senderBalance);

				pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);
			}

		}

		if (PaymentType.BillPayment.getPaymentTypeCode().equals(paymentType)) {

			// thanh toán hóa đơn

			String serviceProviderCode = paymentInfo.getServiceProviderCode();

			if (StringUtils.isNotBlank(customerCode)) {

				titleVi = getMessage("POST_PAYMENT.BILL_PAYMENT.CUSTOMER_CODE.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.BILL_PAYMENT.CUSTOMER_CODE.BODY_6", viLocal);
				messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, customerCode,
						serviceProviderCode, senderBalance);

				titleEl = getMessage("POST_PAYMENT.BILL_PAYMENT.CUSTOMER_CODE.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.BILL_PAYMENT.CUSTOMER_CODE.BODY_6", elLocal);
				messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, customerCode,
						serviceProviderCode, senderBalance);

				pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);
			}

			if (StringUtils.isNotBlank(orderCode)) {

				titleVi = getMessage("POST_PAYMENT.BILL_PAYMENT.ORDER_CODE.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.BILL_PAYMENT.ORDER_CODE.BODY_6", viLocal);
				messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, orderCode,
						serviceProviderCode, senderBalance);

				titleEl = getMessage("POST_PAYMENT.BILL_PAYMENT.ORDER_CODE.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.BILL_PAYMENT.ORDER_CODE.BODY_6", elLocal);
				messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, orderCode,
						serviceProviderCode, senderBalance);

				pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);
			}

		}

		if (PaymentType.eWallet.getPaymentTypeCode().equals(paymentType)) {
			// ví điện tử
		}

		if (PaymentType.TOPUPPayment.getPaymentTypeCode().equals(paymentType)) {

			// nạp tiền điện thoại

			titleVi = getMessage("POST_PAYMENT.TOPUP.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.TOPUP.BODY_5", viLocal);
			messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, phone, senderBalance);

			titleEl = getMessage("POST_PAYMENT.TOPUP.TITLE", viLocal);
			messageEl = getMessage("POST_PAYMENT.TOPUP.BODY_5", viLocal);
			messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, phone, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);

		}

		if (PaymentType.BillCardPayment.getPaymentTypeCode().equals(paymentType)) {

			// thanh toán hóa đơn sử dụng credit card

			String cardNumber = paymentInfo.getCardNumber();

			titleVi = getMessage("POST_PAYMENT.BILL_CARD.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.BILL_CARD.BODY_4", viLocal);
			messageVi = String.format(messageVi, cardNumber, amount, executionDate, senderBalance);

			titleEl = getMessage("POST_PAYMENT.BILL_CARD.TITLE", viLocal);
			messageEl = getMessage("POST_PAYMENT.BILL_CARD.BODY_4", viLocal);
			messageEl = String.format(messageEl, cardNumber, amount, executionDate, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);

		}

		if (PaymentType.QRCodePayment.getPaymentTypeCode().equals(paymentType)) {

			// thong bao giong ck noi bo

			titleVi = getMessage("POST_PAYMENT.QRCODE.TITLE", viLocal);
			messageVi = getMessage("POST_PAYMENT.QRCODE.BODY_4", viLocal);
			messageVi = String.format(messageVi, senderAccountNo, amount, executionDate, senderBalance);

			titleEl = getMessage("POST_PAYMENT.QRCODE.TITLE", elLocal);
			messageEl = getMessage("POST_PAYMENT.QRCODE.BODY_4", elLocal);
			messageEl = String.format(messageEl, senderAccountNo, amount, executionDate, senderBalance);

			pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, event);
		}
	}

	private String getMessage(String key, Locale locale) {
		return messageSource.getMessage(key, new Object[0], locale);
	}

	private void pushNotification(String cif, String titleVi, String messageVi, String titleEl, String messageEl,
			PostPaymentEvent event) {

		// String cif = event.getUserToken().getOldAuthen().getCustomerId();

		try {

			Date executionDate = event.getData().getPaymentInfo().getExecutionDate();
			String descriptionVi = Jsoup.parse(messageVi).text();
			if (descriptionVi.length() > 100) {
				descriptionVi = descriptionVi.substring(0, 100).concat("...");
			}
			String descriptionEl = Jsoup.parse(messageEl).text();
			if (descriptionEl.length() > 100) {
				descriptionEl = descriptionEl.substring(0, 100).concat("...");
			}
			OmniNotification omniNotification = new OmniNotification();
			omniNotification.setType(OmniNotification.TYPE_THONG_BAO.toString());
			omniNotification.setUserId(cif);
			int distributedType = OmniNotification.DISTRIBUTED_TYPE_APP | OmniNotification.DISTRIBUTED_TYPE_FIREBASE;
			omniNotification.setDistributedType(distributedType);
			omniNotification.setTitleVn(titleVi);
			omniNotification.setTitleEl(titleEl);
			omniNotification.setDescriptionVn(descriptionVi);
			omniNotification.setDescriptionEl(descriptionEl);
			omniNotification.setBodyVn(messageVi);
			omniNotification.setBodyEl(messageEl);
			omniNotification.setCreatedDate(executionDate);

			LOG.info(">>> pushNotification: " + objectMapper.writeValueAsString(omniNotification));

			resourceService.pushNotification(omniNotification);

		} catch (Exception e) {
			LOG.error(e, e);
		}

	}

	private static String formatDate(Date datetime) {
		if (datetime == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(datetime);
	}

	private static String formatNumber(Number number) {
		if (number == null) {
			return "";
		}
		return NUMBER_FORMAT.format(number);
	}

	private AccountInfo getAccountByAccountId(String cif, String accountId, String omniToken) {
		List<AccountInfo> accounts = accountService.getAccountList(cif, omniToken);
		for (AccountInfo accountInfo : accounts) {
			if (accountId.equals(accountInfo.getAccountId())) {
				return accountInfo;
			}
		}
		return null;
	}

	private String getCif(String accountNo, String omniToken) {
		Map<String, String> accountCifs = accountService.getCIFFromAccountList(Arrays.asList(accountNo), omniToken);
		String receiverId = accountCifs.get(accountNo);
		return receiverId;
	}
}
