/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.ExchangeRateDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

@Service
public class WebExchangeRatesServiceImpl implements WebExchangeRatesService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public List<ExchangeRateDTO> getExchangeRates() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getExchangeRates(omniToken);
	}

}
