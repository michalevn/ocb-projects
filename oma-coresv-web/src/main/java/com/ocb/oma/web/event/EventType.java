/**
 * 
 */
package com.ocb.oma.web.event;

/**
 * @author phuhoang
 *
 */
public enum EventType {

	LOGIN("LOGIN"), LOGOUT("LOGOUT"), getAccountList("getAccountList"), updateAccountName("updateAccountName"),
	findTransactionHistory("findTransactionHistory");

	private String code;

	/**
	 * 
	 */
	private EventType(String code) {
		// TODO Auto-generated constructor stub
		this.code = code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
}
