/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

/**
 * @author docv
 *
 */
public interface WebUserDeviceService {
	

	/**
	 * 
	 * @param cif
	 * @param deviceId
	 * @param enable
	 * @return
	 */
	Boolean updateUserDeviceStatus(String cif, String deviceId, int enable);


}
