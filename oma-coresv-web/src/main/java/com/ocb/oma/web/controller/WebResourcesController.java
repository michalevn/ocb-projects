/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/resources")
public class WebResourcesController extends BaseController {

	@Autowired
	private ResourceServiceExt resourcesService;

	//
	@RequestMapping(value = "/getBankList")
	public ResponseEntity<?> getBankList() {
		return getFinalResponse(resourcesService.getBankInfos());
	}

	@RequestMapping(value = "/getByBankInfoByBranchCode")
	public ResponseEntity<?> getByBankInfoByBranchCode(@RequestParam("branchCode") String branchCode) {
		return getFinalResponse(resourcesService.getByBankInfoByBranchCode(branchCode));
	}

	@RequestMapping(value = "/listOfValue")
	public ResponseEntity<?> listOfValue(@RequestParam("lovType") String lovType) {

		return getFinalResponse(resourcesService.findListOfValue(lovType));
	}

	@RequestMapping(value = "/getLinksByGroup")
	public ResponseEntity<?> getLinksByGroup(@RequestParam("group") String group) {
		return getFinalResponse(resourcesService.getLinksByGroup(group));
	}

	@RequestMapping(value = "/searchBankCitad")
	public ResponseEntity<?> searchBankCitad(@RequestParam("bankCode") String bankCode,
			@RequestParam("provinceCode") String provinceCode) {

		return getFinalResponse(resourcesService.searchBankCitad(bankCode, provinceCode));
	}

	@RequestMapping(value = "/findGiftCardMsgTemplate")
	public ResponseEntity<?> findGiftCardMsgTemplate(@RequestParam("cardType") String cardType) {

		return getFinalResponse(resourcesService.findGiftCardMsgTemplate(cardType));
	}

	// public List<OmniGiftCardMsgTemplate> findGiftCardMsgTemplate(String cardType)
}
