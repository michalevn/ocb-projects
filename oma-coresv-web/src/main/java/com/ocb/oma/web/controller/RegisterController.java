/**
 * 
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author phuhoang
 *
 */
@RestController
@RequestMapping("/service/register")
public class RegisterController extends BaseController {

	@Autowired
	private ResourceServiceExt resourceService;

}
