/**
 * 
 */
package com.ocb.oma.web.service.internal;

import java.util.Map;

/**
 * @author phuhoang
 *
 */
public interface TemplateFormatService {

	public String format(String messageCode, Map<String, Object> inputData);
}
