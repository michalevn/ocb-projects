/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.StudentTuitionFeeDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

@Service
public class StudentTuitionFeeServiceImpl implements StudentTuitionFeeService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public StudentTuitionFeeDTO get(String studentCode, String universityCode) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getStudentTuitionFee(omniToken, studentCode, universityCode);
	}

}
