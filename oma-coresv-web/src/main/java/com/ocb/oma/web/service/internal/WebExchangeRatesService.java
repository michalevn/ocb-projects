/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import com.ocb.oma.dto.ExchangeRateDTO;

public interface WebExchangeRatesService {

	List<ExchangeRateDTO> getExchangeRates();
}
