/**
 * 
 */
package com.ocb.oma.web.service.internal;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author phuhoang
 *
 */
@Service
public class EncryptDataServiceImpl implements EncryptDataService {

	static Logger LOGGER = LoggerFactory.getLogger(EncryptDataService.class);
	private static final String SECRET_KEY_1 = "ssdkF$HUy2A#D%kd";
	private static final String SECRET_KEY_2 = "weJiSEvR5yAC5ftB";

	private IvParameterSpec ivParameterSpec;
	private SecretKeySpec secretKeySpec;
	private Cipher cipher;

	@Autowired
	private ObjectMapper objectMapper;

	public EncryptDataServiceImpl()
			throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException {
		ivParameterSpec = new IvParameterSpec(SECRET_KEY_1.getBytes("UTF-8"));
		secretKeySpec = new SecretKeySpec(SECRET_KEY_2.getBytes("UTF-8"), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	}

	/**
	 * Encrypt the string with this internal algorithm.
	 *
	 * @param toBeEncrypt string object to be encrypt.
	 * @return returns encrypted string.
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	@Override
	public String encrypt(String toBeEncrypt) {
		try {
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
			byte[] encrypted = cipher.doFinal(toBeEncrypt.getBytes());
			return Base64.encodeBase64String(encrypted);
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("error when encrypt ", e);
			return null;
		}
	}

	/**
	 * Decrypt this string with the internal algorithm. The passed argument should
	 * be encrypted using {@link #encrypt(String) encrypt} method of this class.
	 *
	 * @param encrypted encrypted string that was encrypted using
	 *                  {@link #encrypt(String) encrypt} method.
	 * @return decrypted string.
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	@Override
	public String decrypt(String encrypted) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
			byte[] decryptedBytes = cipher.doFinal(Base64.decodeBase64(encrypted));
			return new String(decryptedBytes);
		} catch (Exception e) {
			LOGGER.error("error when decrypt ", e);
			return null;
		}

	}

	private Map<String, Object> sortMap(Map<String, Object> input) {
		TreeMap<String, Object> sortInput = new TreeMap<>();
		for (Entry<String, Object> entry : input.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (value instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<String, Object> sortedValue = sortMap((Map<String, Object>) value);
				sortInput.put(key, sortedValue);
			} else {
				sortInput.put(key, value);
			}
		}
		return input;
	}

	@SuppressWarnings("unchecked")
	public String hash(Object input) {
		if (input == null) {
			return "";
		}
		Map<String, Object> mapInput = objectMapper.convertValue(input, Map.class);
		Map<String, Object> sortedInput = sortMap(mapInput);
		sortedInput.remove("otpMethod");
		sortedInput.remove("otpValue");
		sortedInput.remove("certificate");
		sortedInput.put("secretKey", SECRET_KEY_1);
		String strInput;
		try {
			strInput = objectMapper.writeValueAsString(sortedInput);
			return DigestUtils.sha256Hex(strInput);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}
}
