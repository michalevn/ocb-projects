/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.Map;

import javax.cache.Cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author Phu Hoang
 *
 */
@Service
public class TokenManagementServiceImpl implements TokenManagementService {

	private static String tokenCache = "TokenConfig";
	private Map<String, String> blockedDevices;

	@Autowired
	private ResourceServiceExt resourceService;

	@Autowired
	// inject the cache manager
	private javax.cache.CacheManager cacheManager;

	private String getKey(UserToken userToken) {
		return userToken.getUsername() + "-" + userToken.getDeviceId();
	}

	@Override
	public void addToken(UserToken token) {
		String key = getKey(token);
		Cache<Object, Object> cache = cacheManager.getCache(tokenCache);
		cache.put(key, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.TokenManagementService#removeToken(java.lang.String)
	 */
	@Override
	public void removeToken(String token) {
		cacheManager.getCache(tokenCache).remove(token);
	}

	@Override
	public Object getTokenValue(String token) {
		Cache<Object, Object> cache = cacheManager.getCache(tokenCache);
		return cache.get(token);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.TokenManagementService#invalidToken(java.lang.String)
	 */
	@Override
	public void invalidToken(String token) {
		// TODO Auto-generated method stub
		cacheManager.getCache(tokenCache).put(token, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.internal.TokenManagementService#isTokenOk(com.ocb.oma
	 * .dto.UserToken)
	 */
	@Override
	public Boolean isTokenOk(UserToken userToken) {
		// TODO Auto-generated method stub
		String key = userToken.getUsername() + userToken.getDeviceId();
		Object check = cacheManager.getCache(tokenCache).get(key);
		if (check == null || (Boolean) check == true) {
			return true;
		}
		return false;
	}

	private Map<String, String> getBlockedDevices() {

		if (blockedDevices == null) {

		}
		return blockedDevices;
	}

}
