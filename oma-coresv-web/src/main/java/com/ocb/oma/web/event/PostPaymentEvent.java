/**
 * 
 */
package com.ocb.oma.web.event;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.PostPaymentInputDTO;

/**
 * @author phuhoang
 *
 */
public class PostPaymentEvent extends OmniEvent {

	private String senderId;
	private String receiverId;
	private PostPaymentInputDTO data;

	/**
	 * @param userToken2
	 * @param source
	 */
	public PostPaymentEvent(UserToken userToken2, PostPaymentInputDTO source) {
		super(userToken2, source);
		this.data = source;
		this.setUserToken(userToken2);
	}

	/**
	 * @return the data
	 */
	public PostPaymentInputDTO getData() {
		return data;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverId() {
		return receiverId;
	}
}
