package com.ocb.oma.web.config;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DeviceMagMessages {

	private static final String DISABLE_DEVICE = "Quý khách đã vô hiệu hóa thành công chức năng đăng nhập bằng tài khoản %s trên thiết bị có tên %s lúc %s! Thiết bị sẽ không thể thực hiện đăng nhập các lần tiếp theo.";
	public static final String DISABLE_DEVICE_TITLE = "Thiết bị đã bị vô hiệu hóa thành công";
	
	private static final String ENABLE_DEVICE = "Quý khách đã kích hoạt thành công chức năng đăng nhập bằng tài khoản %s trên thiết bị có tên %s lúc %s! Quý khách có thể vào chức năng Quản lý thiết bị để biết danh sách các thiết bị đang được sử dụng.";
	public static final String ENABLE_DEVICE_TITLE = "Thiết bị đã được kích hoạt đăng nhập thành công";

	private static String formatDate(Date datetime) {
		if (datetime == null) {
			return "";
		}
		return new SimpleDateFormat("dd-mm-yyyy hh:mm:ss").format(datetime);
	}

	public static String ENABLE_DEVICE(String userName, String deviceName, Date datetime) {
		return String.format(ENABLE_DEVICE, userName, deviceName, formatDate(datetime));
	}

	public static String DISABLE_DEVICE(String userName, String deviceName, Date datetime) {
		return String.format(DISABLE_DEVICE, userName, deviceName, formatDate(datetime));
	}
}
