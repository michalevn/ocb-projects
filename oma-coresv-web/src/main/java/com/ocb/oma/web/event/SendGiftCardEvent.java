/**
 * 
 */
package com.ocb.oma.web.event;

import java.util.List;

import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;

/**
 * @author phuhoang
 *
 */
public class SendGiftCardEvent extends OmniEvent {

	private static final long serialVersionUID = 3678751430567966105L;

	private GiftCardInsertDTO input;
	private List<SendGiftCardOutputDTO> output;

	/**
	 * @param userToken2
	 * @param source
	 */
	public SendGiftCardEvent(UserToken userToken2, GiftCardInsertDTO input, List<SendGiftCardOutputDTO> output) {
		super(userToken2, output);
		this.input = input;
		this.output = output;
	}

	public GiftCardInsertDTO getInput() {
		return input;
	}

	public void setInput(GiftCardInsertDTO input) {
		this.input = input;
	}

	public List<SendGiftCardOutputDTO> getOutput() {
		return output;
	}

	public void setOutput(List<SendGiftCardOutputDTO> output) {
		this.output = output;
	}

}
