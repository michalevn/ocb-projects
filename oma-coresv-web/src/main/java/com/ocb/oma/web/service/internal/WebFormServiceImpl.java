/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebFormServiceImpl implements WebFormService {

	@Autowired
	private ResourceServiceExt resourceServiceExt;

	@Override
	public String creatForm(FormDTO formDTO) {
		String omniToken = AppUtil.getCurrentCPBToken();
		String result = resourceServiceExt.creatForm(omniToken, formDTO);
		resourceServiceExt.sendMailForm(formDTO);
		return result;
	}

}
