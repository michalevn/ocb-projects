/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import com.ocb.oma.dto.WebLimitDTO;

/**
 * @author docv
 *
 */
public interface WebLimitService {

	public WebLimitDTO get();

}
