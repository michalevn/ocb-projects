/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebCreditServiceImpl implements WebCreditService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public List<CreditDTO> getCredits() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getCredits(omniToken);
	}

	@Override
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String contractNumber, Date dateFrom,
			Date dateTo, Integer pageNumber, Integer pageSize, Double operationAmountFrom, Double operationAmountTo) {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getInstalmentTransactionHistory(omniToken, contractNumber, dateFrom, dateTo,
				pageNumber, pageSize, operationAmountFrom,operationAmountTo);
	}

}
