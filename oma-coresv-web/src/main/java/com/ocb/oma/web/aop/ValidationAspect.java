/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.aop;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.ocb.oma.validation.Validation;
import com.ocb.oma.validation.impl.Validator;

/**
 * @author docv
 *
 */
@Aspect
@Configuration
public class ValidationAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationAspect.class);

	@Autowired
	private ApplicationContext applicationContext;

	public ValidationAspect() {
		LOGGER.info("init");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Around("execution(* com.ocb.oma.web.controller.*.*(..))")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		Class<?>[] parameterTypes = method.getParameterTypes();
		Object args[] = joinPoint.getArgs();
		Validation validation = method.getAnnotation(Validation.class);
		if (validation != null) {
			Class<? extends Validator<?>>[] validatorClasses = validation.validators();
			for (Class<? extends Validator<?>> validatorClass : validatorClasses) {
				List<Validator> validators = new ArrayList<>();
				Map<String, ? extends Validator<?>> validationBeans = applicationContext.getBeansOfType(validatorClass);
				if (validationBeans != null && validationBeans.size() > 0) {
					validators.addAll(validationBeans.values());
				}
				for (int i = 0; i < parameterTypes.length; i++) {
					Object arg = args[i];
					if (arg != null) {
						try {
							for (Validator validator : validators) {
								validator.getClass().getMethod("validate", parameterTypes[i]);
								String error = validator.validate(arg);
								if (!StringUtils.isBlank(error)) {
									throw new ConstraintViolationException(error, null);
								}
							}
						} catch (NoSuchMethodException e) {
							// SKIP, check next arg
						}
					}
				}
			}
		}
		return joinPoint.proceed();
	}

}
