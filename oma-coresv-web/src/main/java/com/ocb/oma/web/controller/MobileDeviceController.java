/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/mobileDevices")
public class MobileDeviceController extends BaseController {

	@Autowired
	private ResourceServiceExt resourceService;

	@RequestMapping(value = "/registerDevice")
	public ResponseEntity<?> registerDevice(@RequestBody OmniMobileDevice mobileDevice) {
		return getFinalResponse(resourceService.registerDevice(mobileDevice));
	}
}
