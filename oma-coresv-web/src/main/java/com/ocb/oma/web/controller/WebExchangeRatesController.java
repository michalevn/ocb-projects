/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.ExchangeRateDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.WebExchangeRatesService;

@RestController
@RequestMapping("/service/exchangeRates")
public class WebExchangeRatesController extends BaseController {

	@Autowired
	private WebExchangeRatesService exchangeRatesService;

	@RequestMapping(value = "/getExchangeRates")
	public ResponseEntity<?> getExchangeRates() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<ExchangeRateDTO> rs = exchangeRatesService.getExchangeRates();
		return getFinalResponse(rs);
	}
}
