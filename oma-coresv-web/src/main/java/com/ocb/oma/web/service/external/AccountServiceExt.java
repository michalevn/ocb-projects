/**
 * 
 */
package com.ocb.oma.web.service.external;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.ExchangeRateDTO;
import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.StudentTuitionFeeDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.WebLimitDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;

/**
 * 
 * @author Phu Hoang
 *
 */
public interface AccountServiceExt {

	String doTest(String testParam);

	// TestDTO doTestDTO(TestDTO testDTO);

	/**
	 * check login
	 * 
	 * @param username
	 * @param password
	 * @param deviceId
	 * @return return usertoken if the login is succcess , otherwise return null
	 */
	UserToken checkLogin(String username, String password, String deviceId);

	/**
	 * ham dung de khoi tao SMS OTP
	 * 
	 * @param data
	 * @return
	 */
	AuthorizationResponse sendAuthorizationSmsOtp(String omniToken);

	/**
	 * ham veriry otp token
	 * 
	 * @param data
	 * @return
	 */
	AuthorizationResponse verifyOTPToken(VerifyOtpToken data, String omniToken);

	/**
	 * lay so luong item co trong gio hang
	 * 
	 * @return count
	 */
	Integer countBasketPaymentToProcess(String omniToken);

	/**
	 * ham lay danh sach tai khoan cua khach hang
	 * 
	 * @param customerCif
	 * @return
	 */
	List<AccountInfo> getAccountList(String customerCif, String omniToken);

	/**
	 * 
	 * 
	 * @return
	 */
	List<CampaignInfo> getCampaignList(String customerCif, String username, String omniToken);

	/**
	 * xac thuc otp bang hardware token
	 * 
	 * @param hwToken
	 * @return
	 */
	AuthorizationResponse verifyHwOTPToken(String hwToken, String omniToken);

	/**
	 * update account Name
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param token
	 * @param updatedName
	 * @return
	 */
	Boolean updateAccountName(String accountNo, String customerCif, String updatedName, String omniToken);

	/**
	 * update icon
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param imageLink
	 * @return
	 */
	Boolean updateAccountIcon(String accountNo, String customerCif, String imageLink, String omniToken);

	/**
	 * 
	 * @param token
	 * @param accountNo
	 * @param transactionType
	 * @param fromDate
	 * @param toDate
	 * @param fromAmount
	 * @param toAmount
	 * @param page
	 * @param pageSize
	 * @return
	 */
	List<TransactionHistoryDTO> findTransactionHistory(TransHistoryRequestDTO request, String omniToken);

	/**
	 * lay danh sach nguoi thu huong
	 * 
	 * @param customerCif
	 * @param pageSize
	 * @param token
	 * @return
	 */
	List<RecipientDTO> getRecipientList(String filerTemplateType, int pageSize, String omniToken);

	/**
	 * xoa nguoi thu huong
	 * 
	 * @param token
	 * @param recipient
	 * @return
	 */
	Boolean deleteRecipient(RecipientDTO recipient, String omniToken);

	/**
	 * lay summary account boi cif
	 * 
	 * @param customerCif
	 * @return
	 */
	AccountSummaryDTO loadAccountSummarByCurrencies(String accountId, Integer summaryPeriod, String omniToken);

	/**
	 * lay han muc su dung cua nguoi dung
	 * 
	 * @param paymentType
	 * @param omniToken
	 * @return
	 */
	AccountLimitDTO getAccountLimnit(String paymentType, String omniToken);

	/**
	 * lay ten account theo tai khoan
	 * 
	 * @param accountNo
	 * @param token
	 * @return
	 */
	String getAccountNameByAccountNo(String accountNo, String token);

	/**
	 * lay phi khi thuc hien thanh toan/chuyen khoan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	Double getTransactionFee(TransactionFeeInputDTO input, String token);

	/**
	 * lay phuong thuc giao dich theo so tien
	 * 
	 * @param token
	 * @param amount
	 * @param paymentType
	 * @return
	 */
	String getAuthorizeMethodByAmount(String token, Double amount, String paymentType);

	/**
	 * lay challenge code
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input, String token);

	/**
	 * ham thuc hien chuyen khoan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	PostPaymentOutputDTO postPayment(PostPaymentInputDTO input, String token);

	/**
	 * 
	 * @param token
	 * @return
	 */
	UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token);

	/**
	 * Get List Transaction is Blocked
	 * 
	 * @param token
	 * @return
	 */
	List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer currentPage, Integer pageSize,
			String token);

	/**
	 * 
	 * @param userId
	 * @param omniToken
	 * @return
	 */
	List<String> getAuthorizationPolicies(String userId, String omniToken);

	/**
	 * Lay DS quy dinh theo thong tu QD630
	 * 
	 * @param cif
	 * @param omniToken
	 * @return
	 */
	List<QD630PoliciesDTO> getQD630Policies(String cif, String omniToken);

	/**
	 * them giao dich vao gio hang
	 * 
	 * @param input
	 * @param omniToken
	 * @return
	 */
	PostAddBasketDTO postAddBasket(PostPaymentInputDTO input, String omniToken);

	String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount, String token);

	/**
	 * Them nguoi thu huong
	 * 
	 * @param input
	 * @param omniToken
	 * @return
	 */
	AddRecipientOutputDTO addRecipient(RecipientInputDTO input, String omniToken);

	/**
	 * gET Avatar Info
	 * 
	 * @param omniToken
	 * @return
	 */
	AvatarInfoOutputDTO getAvatarInfo(String omniToken);

	/**
	 * sendGiftCard
	 * 
	 * @param giftCardInsertDTO
	 * @param omniToken
	 * @return
	 */
	List<SendGiftCardOutputDTO> sendGiftCard(GiftCardInsertDTO giftCardInsertDTO, String omniToken);

	/**
	 * getListofBasket
	 * 
	 * @param omniToken
	 * @param page
	 * @param pagesize
	 * @return
	 */
	List<PaymentBasketDTO> getListofBasket(String omniToken, Double amountFrom, Double amountTo, String customerId,
			String realizationDateFrom, String realizationDateTo, String statuses);

	/**
	 * deleteBasket
	 * 
	 * @param basketId
	 * @param omniToken
	 * @return
	 */
	String deleteBasket(String basketId, String omniToken);

	/**
	 * ham lay toan bo danh sach dich vu & nha cung cap
	 * 
	 * @param token
	 * @return
	 */
	List<ServiceProviderComposDTO> getListOfServiceProvider(String token);

	/**
	 * ham tiem kiem lich su giao dich payment
	 * 
	 * @param startDate
	 * @param endDage
	 * @param cif
	 * @param token
	 * @return
	 */
	List<BillPaymentHistoryDTO> getListOfBillTransactions(GetListOfBillTransactionsInputDTO input, String token);

	/**
	 * Kiem tra va tra ve list Bill (da luu truoc day), neu truy van co DU NO cho ky
	 * nay thi hien thi so tien du no
	 *
	 * - Doi voi cac bill khong co du no thi van hien thi nhung de status: chua co
	 * du no trong ky nay
	 *
	 * @param serviceCode
	 * @param duration
	 * @return
	 */
	List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration, String token);

	/**
	 * ham lay bill can thanh toan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	BillPaymentServiceDTO getBill(GetBillInputDTO input, String token);

	/**
	 * loadEVNparameter
	 * 
	 * @param omniToken
	 * @return
	 */
	List<EVNParameter> loadEVNparameter(String omniToken);

	/**
	 * Ham tim the tin dung
	 * 
	 * @param omniToken
	 * @return
	 */
	List<CardInfoDTO> findCard(String omniToken);

	/**
	 * updateBasket
	 * 
	 * @param input
	 * @param omniToken
	 * @return
	 */
	PostAddBasketDTO updateBasket(PostPaymentInputDTO input, String omniToken);

	/**
	 * ham thanh toan gio hang
	 * 
	 * @param transfersIds
	 * @param omniToken
	 * @return
	 */
	PostListBasketDTO postBaskets(List<String> transfersIds, String omniToken);

	/**
	 * ham thay doi pass
	 * 
	 * @param oldPass
	 * @param newPass
	 * @param omniToken
	 * @return
	 */
	ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId, String omniToken);

	String getCifByAccountNo(String accountNo, String omniToken);

	/**
	 * KHoa the
	 * 
	 * @param omniToken
	 * @param cardNumber
	 * @param expiryDate
	 * @return
	 */
	OutputBaseDTO restrictCard(String omniToken, String cardNumber, String expiryDate);

	/**
	 * @param omniToken
	 * @param accountNumber
	 * @param cardNumber
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	List<CardBlockade> findCardBlockades(String omniToken, CardBlockadeInputDTO input);

	/**
	 * Kich hoat the
	 * 
	 * @param omniToken
	 * @param cardId
	 * @return
	 */
	OutputBaseDTO postAuthActivateCard(String omniToken, String cardId);

	/**
	 * Dang ky/sua/xoa trich no tu dong
	 * 
	 * @param omniToken
	 * @param cardAccountAutoRepayment
	 * @return
	 */
	Boolean modifyCardAccountAutoRepayment(String omniToken, CardAccountAutoRepaymentDTO cardAccountAutoRepayment);

	/**
	 * Liet ke danh sach lich su giao dich cua the credit/debit
	 * 
	 * @param omniToken
	 * @param cardAccountEntryQuery
	 * @return
	 */
	List<CardAccountEntryDTO> findCardAccountEntry(String omniToken, CardAccountEntryQueryDTO cardAccountEntryQuery);

	/**
	 * Lay thong tin chi tiet cua giao dich trong tuong lai.
	 * 
	 * @param omniToken
	 * @param paymentId
	 * @return
	 */
	PaymentsInFutureDTO getPaymentDetails(String omniToken, String paymentId);

	/**
	 * lay danh sach giao dich tuong lai
	 * 
	 * @param input
	 * @param omniToken
	 * @return
	 */
	List<PaymentsInFutureDTO> getPaymentsInFuture(PaymentsInFutureInputDTO input, String omniToken);

	/**
	 * Lay ds đang ky thanh toan HĐ tu đong
	 * 
	 * @param omniToken
	 * @return
	 */
	List<TransferAutoBillOutputDTO> getTransferAutoBills(String omniToken, String dateFrom, String dateTo);

	/**
	 * lay danh sach so dien thoai da luu truoc do
	 * 
	 * @param omniToken
	 * @return
	 */
	List<PrepaidPhoneDTO> getListPrepaidPhone(String omniToken);

	/**
	 * Lấy ds CIF từ ds số TK
	 * 
	 * @param lstAccountNum
	 * @param omniToken
	 * @return
	 */
	Map<String, String> getCIFFromAccountList(List<String> lstAccountNum, String omniToken);

	/**
	 * Tat toan so tiet kiem
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	OutputBaseDTO breakDeposit(String omniToken, String depositId);

	/**
	 * Post giao dịch đăng ký thanh toán HĐ tự động
	 * 
	 * @param omniToken
	 * @param createAutoBillTransferDirectlyDTO
	 * @return
	 */
	Boolean createAutoBillTransferDirectly(String omniToken,
			CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO);

	/**
	 * Lay danh sach so tiet kiem
	 * 
	 * @param omniToken
	 * @return
	 */
	List<DepositDTO> getDeposits(String omniToken);

	/**
	 * Lay loaị sổ tiết kiêm
	 * 
	 * @param omniToken
	 * @return
	 */
	List<DepositProductDTO> getDepositProducts(String omniToken);

	/**
	 * Lấy kì hạn và lãi
	 * 
	 * @param omniToken
	 * @param subProductCode
	 * @param currency
	 * @return
	 */
	List<DepositInterestRateDTO> getDepositInterestRates(String omniToken, String currency, String subProductCode);

	/**
	 * Tạo deposit
	 * 
	 * @param omniToken
	 * @param deposit
	 * @return
	 */
	CreateDepositOutputDTO createDeposit(String omniToken, CreateDepositDTO deposit);

	/**
	 * Lay thong tin personal
	 * 
	 * @param omniToken
	 * @return
	 */
	CustomerPersonalInfoDTO getPersonalInfo(String omniToken);

	/**
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	PaymentCardAnotherOuputDTO postpaymentCardAnother(String omniToken, PaymentCardAnotherInputDTO input);

	/**
	 * Lay danh sach tai khoan dc mo tien gui tiet kiem
	 * 
	 * @param omniToken
	 * @return
	 */
	DepositOfferAccountsDTO getDepositOfferAcc(String omniToken);

	DepositDTO getDeposit(String omniToken, String depositId);

	/**
	 * ham lay chi tiet the theo cardid
	 * 
	 * @param omniToken
	 * @param cardId
	 * @return
	 */
	CardDetailByCardIdDTO findCardByCardId(String omniToken, String cardId);

	/**
	 * 1.83 Hàm lấy danh sách tỷ giá - get_exchange_rates
	 * 
	 * @return
	 */
	public List<ExchangeRateDTO> getExchangeRates(String omniToken);

	NewFoTransactionInfo registerBankingProductOnline(String omniToken, NewFoBankingProductOnline bankingProductOnline);

	/**
	 * ham lay danh sach the trich no tu dong
	 * 
	 * @param omniToken
	 * @return
	 */
	List<CardAutoRepaymentDTO> cardAutoRepayment(String omniToken);

	/**
	 * 1.80 Hàm lấy thông tin hạn mức trên web - web_limit
	 * 
	 * @param omniToken
	 * @return
	 */
	WebLimitDTO getWebLimit(String omniToken);

	/**
	 * 1.81 Hàm lấy danh sách khoản vay – credit
	 * 
	 * @param omniToken
	 * @return
	 */
	List<CreditDTO> getCredits(String omniToken);

	/**
	 * 1.82 Hàm lấy lịch sử thanh toán vay - instalment_transaction_history
	 * 
	 * @param omniToken
	 * @param contractNumber
	 * @param dateFrom
	 * @param dateTo
	 * @param pageNumber
	 * @param pageSize
	 * @param operationAmountTo
	 * @param operationAmountFrom
	 * @return
	 */
	List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String omniToken, String contractNumber,
			Date dateFrom, Date dateTo, Integer pageNumber, Integer pageSize, Double operationAmountFrom,
			Double operationAmountTo);

	/**
	 * Lay danh sach giao dich the bi tu choi
	 * 
	 * @param omniToken
	 * @param cardRejectedInput
	 * @return
	 */
	List<CardRejectedDTO> cardRejected(String omniToken, CardRejectedInputDTO cardRejectedInput);

	/**
	 * upload Avatar
	 * 
	 * @param omniToken
	 * @param file
	 * @return
	 */
	AvatarInfoOutputDTO uploadAvatar(String omniToken, String file);

	/**
	 * sao ke
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	List<CardStatementsDTO> cardStatements(String omniToken, CardStatementsInputDTO input);

	/**
	 * 
	 * @param omniToken
	 * @param lat
	 * @param lng
	 * @param poiType
	 * @return
	 */
	GeoLocationPoiDTO findGeoLocationPois(String omniToken, Double lat, Double lng, String poiType);

	/**
	 * 
	 * @param omniToken
	 * @param studentCode
	 * @param universityCode
	 * @return
	 */
	StudentTuitionFeeDTO getStudentTuitionFee(String omniToken, String studentCode, String universityCode);

	/**
	 * xoa thanh toan hoa don tu dong
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String omniToken, String input);

	Boolean changeLimitDirectly(String omniToken, List<ChangeLimitDTO> limits);

	/**
	 * chinh sua thanh toan hoa don tu dong
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	OutputBaseDTO modifyAutoBillTransferDirectly(String omniToken, TransferAutoBillDTO input);

	/**
	 * lay danh sach chuyen tien dinh ky
	 * 
	 * @param omniToken
	 * @return
	 */
	List<StandingOrderOutputDTO> standingOrderList(String omniToken);

	/**
	 * xoa chuyen tien dinh ky
	 * 
	 * @param omniToken
	 * @param standingOrderId
	 * @return
	 */
	OutputBaseDTO removeStandingOrder(String omniToken, String standingOrderId);

	/**
	 * tao chuyen tien dinh ky
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	OutputBaseDTO createStandingOrder(String omniToken, StandingOrderInputDTO input);

	/**
	 * Cập nhật lại tài khoản mặc định khi hiển thị cho các giao dịch thanh toán
	 * 
	 * @param omniToken
	 * @param map
	 * @return
	 */
	Boolean updateProductDefaultDirectly(String omniToken, Map<String, String> map);

	/**
	 * cap nhat thong tin giao dich chuyen tien dinh ky
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	OutputBaseDTO modifyStandingOrderDirectly(String omniToken, StandingOrderInputDTO input);

	/**
	 * Lay danh sach tai khoan dc chuyen khoan va thanh toan
	 * 
	 * @param omniToken
	 * @param productList
	 * @param restrictions
	 * @return
	 */
	List<TransferAccountDTO> transferAccounts(String omniToken, String productList, String restrictions);

	/**
	 * lay thong tin ma QRcode
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	QRcodeInfoOutputDTO checkQRcodeInfo(String omniToken, QRcodeInfoInputDTO input);

	/**
	 * chi tiet giao dich tuong lai
	 * 
	 * @param omniToken
	 * @param idFuturePayment
	 * @return
	 */
	PaymentsInFutureDTO getDetailPaymentsInFuture(String omniToken, String idFuturePayment);

	/**
	 * cap nhat trang thai lock/unlock giao dich online the tin dung
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	ConfigStatusEcommerceCardOutput configStatusEcommerceCard(String omniToken, ConfigStatusEcommerceCardInput input);

	/**
	 * Ham truy van thong tin the thanh toan - card_payment_info
	 * 
	 * @param omniToken
	 * @param input
	 * @return
	 */
	CardPaymentInfoOutput cardPaymentInfo(String omniToken, CardPaymentInfoInput input);

	/**
	 * 
	 * @param omniToken
	 * @param linkStatementFile
	 * @return
	 * @throws IOException
	 */
	InputStream cardPaymentInfo(String omniToken, String linkStatementFile) throws IOException;

	/**
	 * doi username
	 * 
	 * @param omniToken
	 * @param changeUsername
	 * @return
	 */
	ChangeUsernameOutput changeUsername(String omniToken, ChangeUsernameInput changeUsername);

	AccountDetails findAccountDetail(String omniToken, String accountNum);

}
