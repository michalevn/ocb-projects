/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.event.LoginEvent;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/loginService")
public class LoginController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	@Autowired
	private ResourceServiceExt resourceService;

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody Map<String, String> postData, Device deviceInfo) {

		String username = postData.get("username");
		String password = postData.get("password");
		String deviceId = postData.get("deviceId");
		String deviceToken = postData.get("deviceToken");

		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			throw new IllegalArgumentException(MessageConstant.MISSING_USERNAME_PASSWORD);
		}

		if (!username.matches("[0-9a-zA-Z\\.-_]+")) {
			throw new IllegalArgumentException(MessageConstant.USERNAME_INVALID);
		}

		if (StringUtils.isBlank(deviceId)) {
			// || StringUtils.isBlank(deviceToken)
			throw new IllegalArgumentException(MessageConstant.MISSING_DEVICE_INFO);
		}

		UserToken user = accountService.checkLogin(username, password, deviceId, deviceToken);

		if (user != null) {
			applicationEventPublisher.publishEvent(new LoginEvent(user, deviceInfo));
			return getFinalResponse(user);
		}

		return getFailureResponse(user, MessageConstant.LOGIN_FAIL_CODE);
	}

}
