/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.web.service.internal.WebCreditService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/credit")
public class WebCreditController extends BaseController {

	@Autowired
	private WebCreditService webCreditService;

	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value = "/getCredits")
	public ResponseEntity<?> get() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<CreditDTO> credits = webCreditService.getCredits();
		return getFinalResponse(credits);
	}

	@RequestMapping(value = "/getInstalmentTransactionHistory")
	@DateRange(from = "dateFrom", to = "dateTo")
	public ResponseEntity<?> getInstalmentTransactionHistory(@RequestParam("contractNumber") String contractNumber,
			@RequestParam("dateFrom") String dateFrom, @RequestParam("dateTo") String dateTo,
			@Digits @RequestParam("pageNumber") Integer pageNumber, @Digits @RequestParam("pageSize") Integer pageSize,
			@RequestParam("operationAmountFrom") Double operationAmountFrom,
			@RequestParam("operationAmountTo") Double operationAmountTo) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		Date dateFromDate = objectMapper.convertValue(dateFrom, Date.class);
		Date dateToDate = objectMapper.convertValue(dateTo, Date.class);
		List<InstalmentTransactionHistoryDTO> dtos = webCreditService.getInstalmentTransactionHistory(contractNumber,
				dateFromDate, dateToDate, pageNumber, pageSize, operationAmountFrom, operationAmountTo);
		return getFinalResponse(dtos);
	}
}
