/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.BreakDepositInputDTO;
import com.ocb.oma.validation.VND;
import com.ocb.oma.web.service.internal.WebDepositService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/deposit")
public class WebDepositController extends BaseController {

	private static final Log LOG = LogFactory.getLog(WebDepositController.class);

	@Autowired
	private WebDepositService depositService;

	@RequestMapping(value = "/breakDeposit")
	public ResponseEntity<?> breakDeposit(@RequestBody BreakDepositInputDTO input) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(depositService.breakDeposit(input));
	}

	@RequestMapping(value = "/getDeposits")
	public ResponseEntity<?> getDeposits() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(depositService.getDeposits());
	}

	@RequestMapping(value = "/getDeposit")
	public ResponseEntity<?> getDeposit(@RequestParam("depositId") String depositId) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(depositService.getDeposit(depositId));
	}

	@RequestMapping(value = "/depositProducts")
	public ResponseEntity<?> depositProducts() {

		LOG.info("depositProducts");

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<DepositProductDTO> depositProducts = depositService.getDepositProducts();
		return getFinalResponse(depositProducts);
	}

	@RequestMapping(value = "/depositInterestRates")
	public ResponseEntity<?> depositInterestRates(@VND @RequestParam("currency") String currency,
			@RequestParam("subProductCode") String subProductCode) {

		LOG.debug("depositInterestRates");

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<DepositInterestRateDTO> depositInterestRates = depositService.getDepositInterestRates(currency,
				subProductCode);
		return getFinalResponse(depositInterestRates);
	}

	@RequestMapping(value = "/createDeposit")
	public ResponseEntity<?> createDeposit(@RequestBody CreateDepositDTO deposit) {

		LOG.debug("createDeposit");

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		CreateDepositOutputDTO numOfDeposits = depositService.createDeposit(deposit);
		return getFinalResponse(numOfDeposits);
	}

	@RequestMapping(value = "/getDepositOfferAcc")
	public ResponseEntity<?> getDepositOfferAcc() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(depositService.getDepositOfferAcc());
	}

}
