/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RegisterBankingProductOnlineDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostBasketsInput;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.dto.input.VetifyUserDeviceDTO;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;

/**
 * @author Phu Hoang
 *
 */
public interface WebAccountSerivice {
	/**
	 * test function
	 * 
	 * @param testParam
	 * @return
	 */
	public String doTest(String testParam);

	/**
	 * test function
	 * 
	 * @param testDTO
	 * @return
	 */
	// public TestDTO doTestDTO(TestDTO testDTO);

	/**
	 * kiem tra login
	 * 
	 * @param username
	 * @param password
	 * @param deviceId
	 * @param deviceToken 
	 * @return tra ve null/exception neu login khong thanh cong. Thanh cong tra ve
	 *         thong tin user
	 */
	public UserToken checkLogin(String username, String password, String deviceId, String deviceToken);

	/**
	 * relogin & invalid token cu
	 * 
	 * @param userToken
	 * @return
	 */
	public UserToken reLoginByEncryptData(String username, String encryptPassword, String deviceId, String deviceToken);

	/**
	 * ham init goi xac thuc SMS Token
	 * 
	 * @return ket qua goi
	 */
	public AuthorizationResponse sendAuthorizationSmsOtp();

	/**
	 * ham verify otp token
	 * 
	 * @param data
	 * @param token
	 * @return
	 */
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data);

	/**
	 * lay so luong item co trong gio hang
	 * 
	 * @return count
	 */
	public Integer countBasketPaymentToProcess();

	/**
	 * ham lay danh sach tai khoan cua khach hang
	 * 
	 * @param customerCif
	 * @return
	 */
	public List<AccountInfo> getAccountList();

	/**
	 * lay danh sach campaign
	 * 
	 * @return
	 */
	public List<CampaignInfo> getCampaignList();

	/**
	 * ham xac thuc hardware token
	 * 
	 * @param hwToken
	 * @return
	 */
	public AuthorizationResponse verifyHwOTPToken(String hwToken);

	/**
	 * update account Name
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param updatedName
	 * @return
	 */
	public Boolean updateAccountName(String accountNo, String updatedName);

	/**
	 * update icon
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param imageLink
	 * @return
	 */
	public Boolean updateAccountIcon(String accountNo, MultipartFile image);

	/**
	 * ham tim lich su giao dich
	 * 
	 * @param data
	 * @return
	 */
	public List<TransactionHistoryDTO> findTransactionHistory(TransHistoryRequestDTO data);

	/**
	 * lay danh sach nguoi thu huong
	 * 
	 * @param pageSize
	 * @param filerTemplateType
	 * 
	 * @return
	 */
	public List<RecipientDTO> getRecipientList(String filerTemplateType, int pageSize);

	/**
	 * xoa nguoi thu huong
	 * 
	 * @param token
	 * @param recipient
	 * @return
	 */
	public Boolean deleteRecipient(RecipientDTO recipient);

	/**
	 * 
	 * @param accountId
	 * @return
	 */
	public AccountSummaryDTO loadAccountSummarByCurrencies(String accountId, Integer summaryPeriod);

	/**
	 * lay han muc cua tai khoan
	 * 
	 * @param paymentType
	 * @return
	 */
	public AccountLimitDTO getAccountLimnit(String paymentType);

	/**
	 * lay ten tai khoan theo account no
	 * 
	 * @param accountNo
	 * @return
	 */
	public String getAccountNameByAccountNo(String accountNo);

	/**
	 * lay phi khi thuc hien thanh toan/chuyen khoan
	 * 
	 * @param input
	 * @return
	 */
	public Double getTransactionFee(TransactionFeeInputDTO input);

	/**
	 * lay phuong thuc xac thuc theo so tien
	 * 
	 * @param amount
	 * @param paymentType
	 * @return
	 */
	public String getAuthorizeMethodByAmount(Double amount, String paymentType);

	/**
	 * lay challenge code
	 * 
	 * @param input
	 * @return
	 */
	String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input);

	/**
	 * ham thuc hien chuyen khoan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input);

	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit();

	/**
	 * Lay DS Transactions bi block
	 * 
	 * @param accountId
	 * @param currentPage
	 * @param pageSize
	 * @param token
	 * @return
	 */
	List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer currentPage, Integer pageSize);

	/**
	 * DS phuong thuc xac thuc user da dang ky
	 * 
	 * @param userId
	 * @return
	 */
	public List<String> getAuthorizationPolicies(String userId);

	/**
	 * Lay DS quy dinh theo thong tu
	 * 
	 * @param cif
	 * @return
	 */
	public List<QD630PoliciesDTO> getQD630Policies(String cif);

	/**
	 * Them giao dich vao gio hang
	 * 
	 * @param input
	 * @return
	 */
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input);

	/**
	 * lay ten account theo Napas
	 * 
	 * @param accountNo
	 * @param bankCode
	 * @param cardNumber
	 * @param debitAccount
	 * @param token
	 * @return
	 */
	public String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount);

	/**
	 * Them vao danh sach nguoi thu huong
	 * 
	 * @param data
	 * @return
	 */
	public AddRecipientOutputDTO addRecipient(RecipientInputDTO data);

	/**
	 * lay Avatar
	 * 
	 * @return
	 */
	public AvatarInfoOutputDTO getAvatarInfo();

	/**
	 * send li xi
	 * 
	 * @param giftCardInsertDTO
	 * @return
	 */
	public List<SendGiftCardOutputDTO> sendGiftCard(GiftCardInsertDTO giftCardInsertDTO);

	/**
	 * Lay List trong gio hang
	 * 
	 * @param userToken
	 * @param page
	 * @param pagesize
	 * @return
	 */
	public List<PaymentBasketDTO> getListofBasket(Double amountFrom, Double amountTo, String customerId,
			String realizationDateFrom, String realizationDateTo, String statuses);

	/**
	 * xOA GIAO DICH trong gio
	 * 
	 * @param basketId
	 * @return
	 */
	public String deleteBasket(String basketId);

	/**
	 * ham lay toan bo danh sach dich vu & nha cung cap
	 * 
	 * @param token
	 * @return
	 */
	public List<ServiceProviderComposDTO> getListOfServiceProvider();

	/**
	 * ham tiem kiem lich su giao dich payment
	 * 
	 * @param startDate
	 * @param endDage
	 * @param cif
	 * @param token
	 * @return
	 */
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(GetListOfBillTransactionsInputDTO input);

	/**
	 * Kiem tra va tra ve list Bill (da luu truoc day), neu truy van co DU NO cho ky
	 * nay thi hien thi so tien du no
	 *
	 * - Doi voi cac bill khong co du no thi van hien thi nhung de status: chua co
	 * du no trong ky nay
	 *
	 * @param serviceCode
	 * @param duration
	 * @return
	 */
	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration);

	/**
	 * ham lay bill can thanh toan
	 * 
	 * @param input
	 * @param token
	 * @return
	 */
	public BillPaymentServiceDTO getBill(GetBillInputDTO input);

	/**
	 * 
	 * @return
	 */
	public List<EVNParameter> loadEVNparameter();

	/**
	 * updateBasket
	 * 
	 * @param input
	 * @return
	 */
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input);

	/**
	 * postBaskets
	 * 
	 * @param transfersIds
	 * @return
	 */
	public PostListBasketDTO postBaskets(PostBasketsInput input);

	/**
	 * changePassword
	 * 
	 * @param userName
	 * @param newPass
	 * @return
	 */
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId);

	/**
	 * lay danh sach giao dich tuong lai
	 * 
	 * @param input
	 * @return
	 */
	public List<PaymentsInFutureDTO> getPaymentsInFuture(PaymentsInFutureInputDTO input);

	/**
	 * danh sach so dien thoai da luu truoc do
	 * 
	 * @return
	 */
	public List<PrepaidPhoneDTO> getListPrepaidPhone();

	/**
	 * Lấy ds CIF từ ds số TK
	 * 
	 * @param lstAccountNum
	 * 
	 * @return
	 */
	public Map<String, String> getCIFFromAccountList(List<String> lstAccountNum);

	/**
	 * Lay thong tin personal
	 * 
	 * @param token
	 * @return
	 */
	public CustomerPersonalInfoDTO getPersonalInfo();

	/**
	 * 
	 * 
	 * @param bankingProductOnline
	 * @return
	 */
	public NewFoTransactionInfo registerBankingProductOnline(
			RegisterBankingProductOnlineDTO registerBankingProductOnlineDTO);


	/**
	 * upload avatar
	 * 
	 * @param file
	 * @return
	 */
	public AvatarInfoOutputDTO uploadAvatar(String file);

	/**
	 * Cập nhật lại tài khoản mặc định khi hiển thị cho các giao dịch thanh toán
	 * 
	 * @param map
	 * @return
	 */
	public Boolean updateProductDefaultDirectly(Map<String, String> map);

	/**
	 * Lay danh sach tai khoan dc chuyen khoan va thanh toan
	 * 
	 * @param productList
	 * @param restrictions
	 * @return
	 */
	public List<TransferAccountDTO> transferAccounts(String productList, String restrictions);

	/**
	 * kiem tra thong tin ma QRCode
	 * 
	 * @param input
	 * @return
	 */
	public QRcodeInfoOutputDTO checkQRcodeInfo(QRcodeInfoInputDTO input);

	/**
	 * chi tiet giao dich tuong lai
	 * 
	 * @param idFuturePayment
	 * @return
	 */
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String idFuturePayment);
	
	/**
	 * verify OTP
	 * @param otpMethod
	 * @param otpValue
	 * @param token
	 */
	public void checkOTP(String otpMethod, VerifyOtpToken otpValue, String token);

	public Boolean vetifyUserDevice(VetifyUserDeviceDTO vetifyUserDeviceDTO);

	/**
	 * thay doi userName
	 * @param changeUsername
	 * @return
	 */
	public ChangeUsernameOutput changeUsername(ChangeUsernameInput changeUsername);

	/**
	 * 1.12 Hàm lấy thông tin chi tiết một tài khoản
	 * 
	 * @param accountNum
	 * @return
	 */
	public AccountDetails findAccountDetail(String accountNum);

}
