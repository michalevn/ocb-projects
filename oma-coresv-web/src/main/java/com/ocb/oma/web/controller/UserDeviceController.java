/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.VetifyUserDeviceDTO;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.service.internal.WebAccountSerivice;
import com.ocb.oma.web.service.internal.WebUserDeviceService;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/userDevices")
public class UserDeviceController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	@Autowired
	private ResourceServiceExt resourcesService;

	@Autowired
	private WebUserDeviceService userDeviceSService;

	@RequestMapping(value = "/findUserDevices")
	public ResponseEntity<?> findUserDevices() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String cif = AppUtil.getCurrentCustomerCif();
		return getFinalResponse(resourcesService.findUserDevices(cif));
	}

	@RequestMapping(value = "/registerUserDevice")
	public ResponseEntity<?> registerUserDevice(@RequestBody UserDeviceToken userDeviceToken) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}

		String cif = userToken.getOldAuthen().getCustomerId();
		userDeviceToken.setCif(cif);

		return getFinalResponse(resourcesService.registerUserDevice(userDeviceToken));
	}

	@RequestMapping(value = "/updateUserDeviceStatus")
	public ResponseEntity<?> updateUserDeviceStatus(@RequestParam("deviceId") String deviceId,
			@RequestParam("enable") Boolean enable) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}

		String cif = userToken.getOldAuthen().getCustomerId();
		Boolean res = userDeviceSService.updateUserDeviceStatus(cif, deviceId, enable ? 1 : 0);
		return getFinalResponse(res);

	}

	@RequestMapping(value = "/vetifyUserDevice")
	public ResponseEntity<?> verifyOTPToken(@RequestBody VetifyUserDeviceDTO vetifyUserDeviceDTO) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		accountService.vetifyUserDevice(vetifyUserDeviceDTO);
		return getFinalResponse(true);
	}
}
