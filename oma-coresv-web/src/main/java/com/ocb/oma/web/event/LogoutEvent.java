/**
 * 
 */
package com.ocb.oma.web.event;

/**
 * @author docv
 *
 */
public class LogoutEvent extends OmniEvent {

	private static final long serialVersionUID = 1L;

	private String token;

	public LogoutEvent(String token) {
		super(token);
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}
