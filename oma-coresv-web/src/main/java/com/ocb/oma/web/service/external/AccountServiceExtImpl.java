
package com.ocb.oma.web.service.external;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.MessageSource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.AccountDetails;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.CustomerPersonalInfoDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.ExchangeRateDTO;
import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.StudentTuitionFeeDTO;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.TransferAutoBillOutputDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.WebLimitDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.SendAuthorizationSmsOtpDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.dto.newFo.NewFoTransactionInfo;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.QRcodeInfoOutputDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;

@Service
public class AccountServiceExtImpl implements AccountServiceExt {

	@Autowired
	@LoadBalanced
	protected RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageSource messageSource;

	private static final Locale elLocal = new Locale("el");
	private static final Locale viLocal = new Locale("vi");

	protected Logger logger = Logger.getLogger(AccountServiceExtImpl.class.getName());

	private static final String ACCOUNT_SERVICE_URL = "http://ACCOUNTS-SERVICE";

	@Override
	public String doTest(String testParam) {

		logger.info("doTest() invoked: for " + testParam);
		return restTemplate.getForObject(ACCOUNT_SERVICE_URL + "/account/test?name={testName}", String.class,
				testParam);
	}

	@Override
	public UserToken checkLogin(String username, String password, String deviceId) {
		Map<String, String> postData = new HashMap<>();
		postData.put("username", username);
		postData.put("password", password);
		postData.put("deviceId", deviceId);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		HttpEntity<Map> entity = new HttpEntity<>(postData, headers);
		UserToken user = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/account/login", entity, UserToken.class);
		if (user == null || user.getOldAuthen() == null) {
			return null;
		}
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#sendAuthorizationSmsOtp(
	 * com.ocb.oma.dto.input.SendAuthorizationSmsOtpDTO)
	 */
	@Override
	public AuthorizationResponse sendAuthorizationSmsOtp(String omniToken) {

		HttpHeaders headers = getHeader(omniToken);
		HttpEntity<SendAuthorizationSmsOtpDTO> entity = new HttpEntity<>(headers);
		AuthorizationResponse result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/otp/sendAuthorizationSmsOtp",
				entity, AuthorizationResponse.class);
		return result;
	}

	private HttpHeaders getHeader(String omniToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		if (omniToken == null) {
			throw new OmaException(MessageConstant.INVALID_TOKEN);
		}
		headers.set(HttpHeaders.AUTHORIZATION, omniToken);
		return headers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.AccountServiceExt#
	 * countBasketPaymentToProcess(java.lang.String)
	 */
	@Cacheable(value = "countBasketPaymentToProcess", key = "#omniToken")
	@Override
	public Integer countBasketPaymentToProcess(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		HttpEntity<Integer> entity = new HttpEntity<Integer>(headers);
		Integer result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/payment/countBasketPaymentToProcess",
				entity, Integer.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getAccountList(java.lang.
	 * String, java.lang.String)
	 */
	@Cacheable(value = "getAccountList", key = "#omniToken")
	@Override
	public List<AccountInfo> getAccountList(String customerCif, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<AccountInfo>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getAccountList?customerCif=" + customerCif));

		ResponseEntity<List<AccountInfo>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<AccountInfo>>() {
				});
		return response.getBody();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.AccountServiceExt#getCampaignList()
	 */
	@Cacheable(value = "getCampaignList", key = "#omniToken")
	@Override
	public List<CampaignInfo> getCampaignList(String customerCif, String username, String omniToken) {
		// TODO Auto-generated method stub

		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<CampaignInfo>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/campaign/getCampaignList?customerCif=" + customerCif + "&username="
						+ username));

		ResponseEntity<List<CampaignInfo>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CampaignInfo>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#verifyOTPToken(com.ocb.oma
	 * .dto.input.VerifyOtpToken)
	 */

	@Override
	public AuthorizationResponse verifyOTPToken(VerifyOtpToken data, String omniToken) {

		HttpHeaders headers = getHeader(omniToken);
		HttpEntity<VerifyOtpToken> entity = new HttpEntity<>(data, headers);
		AuthorizationResponse result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/otp/verifyOTPToken", entity,
				AuthorizationResponse.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#verifyHwOTPToken(java.lang
	 * .String)
	 */
	@Override
	public AuthorizationResponse verifyHwOTPToken(String hwToken, String omniToken) {
		// TODO Auto-generated method stub

		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> data = new HashMap<>();
		data.put("tokenValue", hwToken);
		HttpEntity<Map> entity = new HttpEntity<>(data, headers);
		AuthorizationResponse result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/otp/verifyHWOTPToken", entity,
				AuthorizationResponse.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#updateAccountName(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */

	@Override
	public Boolean updateAccountName(String accountNo, String customerCif, String updatedName, String omniToken) {
		// TODO Auto-generated method stub

		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> data = new HashMap<>();
		data.put("customerCif", customerCif);
		data.put("accountNo", accountNo);
		data.put("accountName", updatedName);
		HttpEntity<Map> entity = new HttpEntity<>(data, headers);
		Boolean result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/account/updateAccountName", entity,
				Boolean.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#updateAccountIcon(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean updateAccountIcon(String accountNo, String customerCif, String imageLink, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> data = new HashMap<>();
		data.put("customerCif", customerCif);
		data.put("accountNo", accountNo);
		data.put("imageLink", imageLink);
		HttpEntity<Map> entity = new HttpEntity<>(data, headers);
		Boolean result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/account/updateAccountAvatar", entity,
				Boolean.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#findTransactionHistory(com
	 * .ocb.oma.dto.input.TransHistoryRequestDTO)
	 */
	@Cacheable(value = "findTransactionHistory")
	@Override
	public List<TransactionHistoryDTO> findTransactionHistory(TransHistoryRequestDTO data, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<TransHistoryRequestDTO> request = new RequestEntity<>(data, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/findTransactionHistory"));

		ResponseEntity<List<TransactionHistoryDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TransactionHistoryDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getRecipientList(java.lang
	 * .String)
	 */
//	@Cacheable(value = "getRecipientList", key = "#omniToken")
	@Override
	public List<RecipientDTO> getRecipientList(String filerTemplateType, int pageSize, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		String url = "";
		url = "/recipient/getRecipientList?filerTemplateType=" + filerTemplateType + "&pageSize=" + pageSize;
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + url));

		ResponseEntity<List<RecipientDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<RecipientDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public Boolean deleteRecipient(RecipientDTO recipient, String omniToken) {
		// TODO Auto-generated method stub
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(recipient, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/recipient/deleteRecipient"));

		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.AccountServiceExt#
	 * loadAccountSummarByCurrencies(java.lang.String)
	 */
	@Override
	@Cacheable(value = "loadAccountSummarByCurrencies", key = "#omniToken")
	public AccountSummaryDTO loadAccountSummarByCurrencies(String accountId, Integer summaryPeriod, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/loadAccountSummarByCurrencies?accountId=" + accountId
						+ "&summaryPeriod=" + summaryPeriod));

		ResponseEntity<AccountSummaryDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AccountSummaryDTO>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getAccountLimnit(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	@Cacheable(value = "getAccountLimnit", key = "#omniToken")
	public AccountLimitDTO getAccountLimnit(String paymentType, String omniToken) {
		// TODO Auto-generated method stub
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getAccountLimit?paymentType=" + paymentType));

		ResponseEntity<AccountLimitDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AccountLimitDTO>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getAccountNameByAccountNo(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public String getAccountNameByAccountNo(String accountNo, String omniToken) {
		// TODO Auto-generated method stub
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getAccountNameByAccountNo?accountNo=" + accountNo));

		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getTransactionFee(com.ocb.
	 * oma.dto.input.TransactionFeeInputDTO, java.lang.String)
	 */
	@Override
	public Double getTransactionFee(TransactionFeeInputDTO input, String token) {
		HttpHeaders headers = getHeader(token);
		HttpEntity<TransactionFeeInputDTO> entity = new HttpEntity<>(input, headers);
		Double result = restTemplate.postForObject(ACCOUNT_SERVICE_URL + "/payment/getTransactionFee", entity,
				Double.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getAuthorizeMethodByAmount
	 * (java.lang.String, java.lang.Double, java.lang.String)
	 */
	@Override
	@Cacheable(value = "getAuthorizeMethodByAmount", key = "#amount")
	public String getAuthorizeMethodByAmount(String token, Double amount, String paymentType) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(ACCOUNT_SERVICE_URL
				+ "/otp/getAuthorizeMethodByAmount?paymentType=" + paymentType + "&amount=" + amount));

		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getChallengeCodeSoftOTP(
	 * com.ocb.oma.dto.input.GetChallengeCodeInputDTO, java.lang.String)
	 */
	@Override
	public String getChallengeCodeSoftOTP(GetChallengeCodeInputDTO input, String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<GetChallengeCodeInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/otp/getChallengeCodeSoftOTP"));

		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#postPayment(com.ocb.oma.
	 * dto.input.PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostPaymentOutputDTO postPayment(PostPaymentInputDTO input, String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<PostPaymentInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/postPayment"));

		ResponseEntity<PostPaymentOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PostPaymentOutputDTO>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.AccountServiceExt#
	 * getUpcomingRepaymentDeposit(java.lang.String)
	 */
	@Override
	public UpcomingRepaymentDepositDTO getUpcomingRepaymentDeposit(String token) {
		// TODO Auto-generated method stub
		HttpHeaders headers = getHeader(token);
		RequestEntity<PostPaymentInputDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getUpcomingRepaymentDeposit"));

		ResponseEntity<UpcomingRepaymentDepositDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<UpcomingRepaymentDepositDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<TransactionBlockedDTO> getBlockedTransaction(String accountId, Integer currentPage, Integer pageSize,
			String omniToken) {
		// TODO Auto-generated method stub
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<TransactionBlockedDTO>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getBlockedTransaction?accountId=" + accountId
						+ "&currentPage=" + currentPage + "&pageSize=" + pageSize));

		ResponseEntity<List<TransactionBlockedDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TransactionBlockedDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<String> getAuthorizationPolicies(String userId, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<String>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/otp/getAuthorizationPolicies?userId=" + userId));
		ResponseEntity<List<String>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<String>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getAccountNameNapas(java.
	 * lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getAccountNameNapas(String accountNo, String bankCode, String cardNumber, String debitAccount,
			String token) {
		HttpHeaders headers = getHeader(token);
		String requestUrl = "/account/getAccountNameNapas?";
		if (accountNo != null) {
			requestUrl += "accountNo=" + accountNo;
		} else {
			requestUrl += "accountNo=";
		}
		if (bankCode != null) {
			requestUrl += "&bankCode=" + bankCode;
		} else {
			requestUrl += "&bankCode=";
		}
		if (cardNumber != null) {
			requestUrl += "&cardNumber=" + cardNumber;
		} else {
			requestUrl += "&cardNumber=";
		}
		if (debitAccount != null) {
			requestUrl += "&debitAccount=" + debitAccount;
		} else {
			requestUrl += "&debitAccount=";
		}
		RequestEntity<GetChallengeCodeInputDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + requestUrl));

		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	@Override
	public List<QD630PoliciesDTO> getQD630Policies(String cif, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/otp/getQD630Policies?cif=" + cif));

		ResponseEntity<List<QD630PoliciesDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<QD630PoliciesDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public PostAddBasketDTO postAddBasket(PostPaymentInputDTO input, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<PostPaymentInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/postAddBasket"));

		ResponseEntity<PostAddBasketDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PostAddBasketDTO>() {
				});
		return response.getBody();
	}

	@Override
	public AddRecipientOutputDTO addRecipient(RecipientInputDTO input, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/recipient/addRecipient"));

		ResponseEntity<AddRecipientOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AddRecipientOutputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public AvatarInfoOutputDTO getAvatarInfo(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<String>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/getAvatarInfo"));

		ResponseEntity<AvatarInfoOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AvatarInfoOutputDTO>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#sendGiftCard(com.ocb.oma.
	 * dto.GiftCardInsertDTO, java.lang.String)
	 */
	@Override
	public List<SendGiftCardOutputDTO> sendGiftCard(GiftCardInsertDTO giftCardInsertDTO, String omniToken) {

		HttpHeaders headers = getHeader(omniToken);

		// goi post pament o account
//		List<SendGiftCardOutputDTO> rs = postGiftCardPayment(giftCardInsertDTO, headers);

		List<SendGiftCardOutputDTO> lstSendGiftCardOutputDTO = new ArrayList<>();
		PostPaymentInputDTO postPaymentInputDTO = new PostPaymentInputDTO();

		postPaymentInputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		List<OmniGiftCardReceiver> receivers = new ArrayList<>();
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = giftCardInsertDTO.getReceivers();

		for (OmniGiftCardReceiver omniGiftCardReceiver : lstOmniGiftCardReceiver) {
			try {
				if (sendGiftCard(giftCardInsertDTO, omniToken, lstSendGiftCardOutputDTO, postPaymentInputDTO,
						omniGiftCardReceiver)) {
					omniGiftCardReceiver.setCreatedDate(new Date());
					receivers.add(omniGiftCardReceiver);

					logger.info("Add Receiver: " + objectMapper.writeValueAsString(omniGiftCardReceiver));

				} else {

					logger.info("Invalid Receiver: " + objectMapper.writeValueAsString(omniGiftCardReceiver));

				}
			} catch (Exception e) {
				try {
					logger.warning("Error sendGiftCard: " + objectMapper.writeValueAsString(omniGiftCardReceiver));
				} catch (JsonProcessingException jpe) {
				}
			}
		}
		giftCardInsertDTO.setReceivers(receivers);

		List<SendGiftCardOutputDTO> result = lstSendGiftCardOutputDTO;

		return result;
	}

	private boolean sendGiftCard(GiftCardInsertDTO giftCardInsertDTO, String omniToken,
			List<SendGiftCardOutputDTO> lstSendGiftCardOutputDTO, PostPaymentInputDTO postPaymentInputDTO,
			OmniGiftCardReceiver omniGiftCardReceiver) {

		OmniGiftCard giftCard = giftCardInsertDTO.getGiftCard();

		PostPaymentOutputDTO postPaymentOutputDTO = new PostPaymentOutputDTO();
		SendGiftCardOutputDTO sendGiftCardOutputDTO = new SendGiftCardOutputDTO();
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();

		paymentInfo.setAccountId(giftCard.getDebitAccountId());
		paymentInfo.setCurrency(giftCard.getCurrency());
		paymentInfo.setRemarks(getRemarks(giftCard));
		paymentInfo.setCreditAccount(omniGiftCardReceiver.getBeneficiary());
		paymentInfo.setAmount(omniGiftCardReceiver.getAmount().longValue());
		paymentInfo.setRecipient(omniGiftCardReceiver.getRecipient());
		paymentInfo.setExecutionDate(new Date());

		postPaymentInputDTO.setPaymentInfo(paymentInfo);
		postPaymentOutputDTO = postPayment(postPaymentInputDTO, omniToken);

		sendGiftCardOutputDTO.setBeneficiary(omniGiftCardReceiver.getBeneficiary());
		sendGiftCardOutputDTO.setDebitAccountId(giftCardInsertDTO.getGiftCard().getDebitAccountId());
		sendGiftCardOutputDTO.setPostPaymentOutputDTO(postPaymentOutputDTO);

		lstSendGiftCardOutputDTO.add(sendGiftCardOutputDTO);

		if (postPaymentOutputDTO.getSuccess()) {
			return true;
		}
		return false;
	}

	private String getRemarks(OmniGiftCard giftCard) {
		String cardType = giftCard.getCardType();
		if (StringUtils.isNotBlank(cardType)) {
			try {
				if ("el".equals(giftCard.getLocale()) || "en".equals(giftCard.getLocale())) {
					return messageSource.getMessage(cardType, new Object[0], elLocal);
				} else {
					return messageSource.getMessage(cardType, new Object[0], viLocal);
				}
			} catch (Exception e) {
			}
		}
		return giftCard.getShortMessage();
	}

	/**
	 * goi post payment
	 * 
	 * @param giftCardInsertDTO
	 * @param headers
	 * @return
	 */
	private List<SendGiftCardOutputDTO> postGiftCardPayment(GiftCardInsertDTO giftCardInsertDTO, HttpHeaders headers) {
		RequestEntity<GiftCardInsertDTO> request = new RequestEntity<>(giftCardInsertDTO, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/sendGiftCard"));

		ResponseEntity<List<SendGiftCardOutputDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<SendGiftCardOutputDTO>>() {
				});
		List<SendGiftCardOutputDTO> rs = response.getBody();
		return rs;
	}

	/**
	 * goi ham getListofBasket
	 */
	@Override
	public List<PaymentBasketDTO> getListofBasket(String omniToken, Double amountFrom, Double amountTo,
			String customerId, String realizationDateFrom, String realizationDateTo, String statuses) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<PaymentBasketDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getListofBasket?amountFrom=" + amountFrom + "&amountTo="
						+ amountTo + "&customerId=" + customerId + "&realizationDateFrom=" + realizationDateFrom
						+ "&realizationDateTo=" + realizationDateTo + "&statuses=" + statuses));

		ResponseEntity<List<PaymentBasketDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<PaymentBasketDTO>>() {
				});
		return response.getBody();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#deleteBasket(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public String deleteBasket(String basketId, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<List<PaymentBasketDTO>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/deleteBasket?basketId=" + basketId));

		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getListOfServiceProvider(
	 * java.lang.String)
	 */
	@Override
	public List<ServiceProviderComposDTO> getListOfServiceProvider(String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<PaymentBasketDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getListOfServiceProvider"));

		ResponseEntity<List<ServiceProviderComposDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<ServiceProviderComposDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getListOfBillTransactions(
	 * com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO, java.lang.String)
	 */
	@Override
	public List<BillPaymentHistoryDTO> getListOfBillTransactions(GetListOfBillTransactionsInputDTO input,
			String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<GetListOfBillTransactionsInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getListOfBillTransactions"));

		ResponseEntity<List<BillPaymentHistoryDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<BillPaymentHistoryDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.web.service.external.AccountServiceExt#
	 * getListBillPaymentByService(java.lang.String, java.lang.String)
	 */
	@Override
	public List<BillPaymentServiceDTO> getListBillPaymentByService(String serviceCode, Integer duration, String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<PaymentBasketDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getListBillPaymentByService?serviceCode=" + serviceCode
						+ "&duration=" + duration));

		ResponseEntity<List<BillPaymentServiceDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<BillPaymentServiceDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#getBill(com.ocb.oma.dto.
	 * input.GetBillInputDTO, java.lang.String)
	 */
	@Override
	public BillPaymentServiceDTO getBill(GetBillInputDTO input, String token) {
		HttpHeaders headers = getHeader(token);
		RequestEntity<GetBillInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getBill"));

		ResponseEntity<BillPaymentServiceDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<BillPaymentServiceDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<EVNParameter> loadEVNparameter(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<PaymentBasketDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/loadEVNparameter"));

		ResponseEntity<List<EVNParameter>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<EVNParameter>>() {
				});
		return response.getBody();
	}

	@Override
	public List<CardInfoDTO> findCard(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardInfoDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/card/findCard"));
		ResponseEntity<List<CardInfoDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardInfoDTO>>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#updateBasket(com.ocb.oma.
	 * dto.input.PostPaymentInputDTO, java.lang.String)
	 */
	@Override
	public PostAddBasketDTO updateBasket(PostPaymentInputDTO input, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<PostPaymentInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/updateBasket"));

		ResponseEntity<PostAddBasketDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PostAddBasketDTO>() {
				});
		PostAddBasketDTO rs = response.getBody();
		return rs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#postBaskets(java.util.
	 * List, java.lang.String)
	 */
	@Override
	public PostListBasketDTO postBaskets(List<String> transfersIds, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<List<String>> request = new RequestEntity<>(transfersIds, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/postBaskets"));
		ResponseEntity<PostListBasketDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PostListBasketDTO>() {
				});
		return response.getBody();
	}

	@Override
	public ChangePasswordDTO changePassword(String oldPass, String newPass, String customerId, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		HashMap input = new HashMap<>();
		input.put("oldPassword", oldPass);
		input.put("newPassword", newPass);
		input.put("customerId", customerId);

		RequestEntity<HashMap> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/changePassword"));
		ResponseEntity<ChangePasswordDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<ChangePasswordDTO>() {
				});
		return response.getBody();
	}

	@Override
	public String getCifByAccountNo(String accountNo, String omniToken) {

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/account/getCifByAccountNo");
		uriBuilder.queryParam("accountNo", accountNo);
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<PaymentBasketDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<String> response = restTemplate.exchange(request, new ParameterizedTypeReference<String>() {
		});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#restrictCard(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public OutputBaseDTO restrictCard(String omniToken, String cardId, String restrictionReason) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(ACCOUNT_SERVICE_URL
				+ "/card/restrictCard?cardId=" + cardId + "&cardRestrictionReason=" + restrictionReason));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<CardBlockade> findCardBlockades(String omniToken, CardBlockadeInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardBlockadeInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/card/findCardBlockades"));
		ResponseEntity<List<CardBlockade>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardBlockade>>() {
				});
		return response.getBody();
	}

	@Override
	public Boolean modifyCardAccountAutoRepayment(String omniToken,
			CardAccountAutoRepaymentDTO cardAccountAutoRepayment) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardAccountAutoRepaymentDTO> request = new RequestEntity<>(cardAccountAutoRepayment, headers,
				HttpMethod.POST, URI.create(ACCOUNT_SERVICE_URL + "/card/modifyCardAccountAutoRepayment"));
		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		return response.getBody();
	}

	@Override
	public List<CardAccountEntryDTO> findCardAccountEntry(String omniToken,
			CardAccountEntryQueryDTO cardAccountEntryQuery) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardAccountEntryQueryDTO> request = new RequestEntity<>(cardAccountEntryQuery, headers,
				HttpMethod.POST, URI.create(ACCOUNT_SERVICE_URL + "/card/findCardAccountEntry"));
		ResponseEntity<List<CardAccountEntryDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardAccountEntryDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public PaymentsInFutureDTO getPaymentDetails(String omniToken, String paymentId) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/card/getPaymentDetails");
		uriBuilder.queryParam("paymentId", paymentId);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<PaymentsInFutureDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PaymentsInFutureDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<PaymentsInFutureDTO> getPaymentsInFuture(PaymentsInFutureInputDTO input, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		input.setStatusPaymentCriteria("waiting");
		RequestEntity<PaymentsInFutureInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/payment/getPaymentsInFuture"));

		ResponseEntity<List<PaymentsInFutureDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<PaymentsInFutureDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<TransferAutoBillOutputDTO> getTransferAutoBills(String omniToken, String dateFrom, String dateTo) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/bill/getTransferAutoBills");
		uriBuilder.queryParam("dateFrom", dateFrom);
		uriBuilder.queryParam("dateTo", dateTo);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<TransferAutoBillOutputDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TransferAutoBillOutputDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<PrepaidPhoneDTO> getListPrepaidPhone(String omniToken) {

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/otp/getListPrepaidPhone"));
		ResponseEntity<List<PrepaidPhoneDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<PrepaidPhoneDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public Map<String, String> getCIFFromAccountList(List<String> lstAccountNum, String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/account/getCIFFromAccountList");

		Map<String, Object> params = new HashMap<>();
		params.put("lstAccountNum", lstAccountNum);

		RequestEntity<Map<String, Object>> request = new RequestEntity<>(params, headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<Map<String, String>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<Map<String, String>>() {
				});
		return response.getBody();
	}

	@Override
	public OutputBaseDTO breakDeposit(String omniToken, String depositId) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/deposit/breakDeposit?depositId=" + depositId));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();

	}

	@Override
	public Boolean createAutoBillTransferDirectly(String omniToken,
			CreateAutoBillTransferDirectlyDTO createAutoBillTransferDirectlyDTO) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/bill/createAutoBillTransferDirectly");

		RequestEntity<CreateAutoBillTransferDirectlyDTO> request = new RequestEntity<>(
				createAutoBillTransferDirectlyDTO, headers, HttpMethod.POST, URI.create(uriBuilder.toUriString()));
		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		return response.getBody();
	}

	@Override
	public List<DepositDTO> getDeposits(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/deposit/getDeposits"));
		ResponseEntity<List<DepositDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<DepositDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<DepositProductDTO> getDepositProducts(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/deposit/depositProducts");

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<DepositProductDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<DepositProductDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<DepositInterestRateDTO> getDepositInterestRates(String omniToken, String currency,
			String subProductCode) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ACCOUNT_SERVICE_URL
				+ "/deposit/depositInterestRates?currency=" + currency + "&subProductCode=" + subProductCode);

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<DepositInterestRateDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<DepositInterestRateDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public CreateDepositOutputDTO createDeposit(String omniToken, CreateDepositDTO deposit) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/deposit/createDeposit");

		RequestEntity<CreateDepositDTO> request = new RequestEntity<>(deposit, headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<CreateDepositOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<CreateDepositOutputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public CustomerPersonalInfoDTO getPersonalInfo(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/account/getPersonalInfo");

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<CustomerPersonalInfoDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<CustomerPersonalInfoDTO>() {
				});
		return response.getBody();
	}

	@Override
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(String omniToken, PaymentCardAnotherInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/card/postPaymentCardAnother");

		RequestEntity<PaymentCardAnotherInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<PaymentCardAnotherOuputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PaymentCardAnotherOuputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public DepositOfferAccountsDTO getDepositOfferAcc(String omniToken) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/deposit/getDepositOfferAcc"));
		ResponseEntity<DepositOfferAccountsDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<DepositOfferAccountsDTO>() {
				});
		return response.getBody();
	}

	@Override
	public DepositDTO getDeposit(String omniToken, String depositId) {

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/deposit/getDeposit");
		uriBuilder.queryParam("depositId", depositId);

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<DepositDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<DepositDTO>() {
				});
		return response.getBody();
	}

	@Override
	public CardDetailByCardIdDTO findCardByCardId(String omniToken, String cardId) {
		HttpHeaders headers = getHeader(omniToken);

		RequestEntity<List<String>> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/card/findCardByCardId?card_id=" + cardId));

		ResponseEntity<CardDetailByCardIdDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<CardDetailByCardIdDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<ExchangeRateDTO> getExchangeRates(String omniToken) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/exchangeRates/getExchangeRates");
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<ExchangeRateDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<ExchangeRateDTO>>() {
				});
		List<ExchangeRateDTO> omniCardTypes = response.getBody();
		return omniCardTypes;
	}

	@Override
	public NewFoTransactionInfo registerBankingProductOnline(String omniToken,
			NewFoBankingProductOnline bankingProductOnline) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/newfo/registerBankingProductOnline");
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<NewFoBankingProductOnline> request = new RequestEntity<>(bankingProductOnline, headers,
				HttpMethod.POST, URI.create(uriBuilder.toUriString()));
		ResponseEntity<NewFoTransactionInfo> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<NewFoTransactionInfo>() {
				});
		NewFoTransactionInfo newFoTransactionInfo = response.getBody();
		return newFoTransactionInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#cardAutoRepayment(java.
	 * lang.String)
	 */
	@Override
	public List<CardAutoRepaymentDTO> cardAutoRepayment(String omniToken) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/card/cardAutoRepayment");
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<CardAutoRepaymentDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardAutoRepaymentDTO>>() {
				});
		List<CardAutoRepaymentDTO> omniCardTypes = response.getBody();
		return omniCardTypes;
	}

	@Override
	public WebLimitDTO getWebLimit(String omniToken) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ACCOUNT_SERVICE_URL + "/webLimit/get");
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<WebLimitDTO> response = restTemplate.exchange(request, WebLimitDTO.class);
		return response.getBody();
	}

	@Override
	public List<CreditDTO> getCredits(String omniToken) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/credit/getCredits");
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<CreditDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CreditDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String omniToken,
			String contractNumber, Date dateFrom, Date dateTo, Integer pageNumber, Integer pageSize,
			Double operationAmountFrom, Double operationAmountTo) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/credit/getInstalmentTransactionHistory");

		uriBuilder.queryParam("contractNumber", contractNumber);
		uriBuilder.queryParam("dateFrom", dateFrom != null ? dateFrom.getTime() : "");
		uriBuilder.queryParam("dateTo", dateTo != null ? dateTo.getTime() : "");
		uriBuilder.queryParam("pageNumber", pageNumber);
		uriBuilder.queryParam("pageSize", pageSize);
		uriBuilder.queryParam("operationAmountFrom", operationAmountFrom);
		uriBuilder.queryParam("operationAmountTo", operationAmountTo);

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<InstalmentTransactionHistoryDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<InstalmentTransactionHistoryDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public List<CardRejectedDTO> cardRejected(String omniToken, CardRejectedInputDTO cardRejectedInput) {
		HttpHeaders headers = getHeader(omniToken);
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/card/cardRejected");

		RequestEntity<CardRejectedInputDTO> request = new RequestEntity<>(cardRejectedInput, headers, HttpMethod.POST,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<CardRejectedDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardRejectedDTO>>() {
				});
		return response.getBody();

	}

	@Override
	public OutputBaseDTO postAuthActivateCard(String omniToken, String cardId) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<String> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/card/postAuthActivateCard?cardId=" + cardId));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();

	}

	@Override
	public AvatarInfoOutputDTO uploadAvatar(String omniToken, String file) {
		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> input = new HashMap<>();
		input.put("file", file);
		RequestEntity<Map> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/uploadAvatar"));
		ResponseEntity<AvatarInfoOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AvatarInfoOutputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<CardStatementsDTO> cardStatements(String omniToken, CardStatementsInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardStatementsInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/card/cardStatements"));
		ResponseEntity<List<CardStatementsDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<CardStatementsDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public GeoLocationPoiDTO findGeoLocationPois(String omniToken, Double lat, Double lng, String poiType) {
		String path = "/geo-locations/find";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ACCOUNT_SERVICE_URL + path);
		uriBuilder.queryParam("lat", lat);
		uriBuilder.queryParam("lng", lng);
		uriBuilder.queryParam("poiType", poiType);
		String uriString = uriBuilder.toUriString();
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(uriString));
		ResponseEntity<GeoLocationPoiDTO> response = restTemplate.exchange(request, GeoLocationPoiDTO.class);
		return response.getBody();
	}

	@Override
	public StudentTuitionFeeDTO getStudentTuitionFee(String omniToken, String studentCode, String universityCode) {
		String path = "/student-tuition-fee/get";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ACCOUNT_SERVICE_URL + path);
		uriBuilder.queryParam("studentCode", studentCode);
		uriBuilder.queryParam("universityCode", universityCode);
		String uriString = uriBuilder.toUriString();
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, URI.create(uriString));
		ResponseEntity<StudentTuitionFeeDTO> response = restTemplate.exchange(request, StudentTuitionFeeDTO.class);
		return response.getBody();
	}

	@Override
	public DeleteAutoBillTransferOutputDTO deleteAutoBillTransfer(String omniToken, String input) {
		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> param = new HashMap<>();
		param.put("orderNumber", input);

		RequestEntity<Map> request = new RequestEntity<>(param, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/bill/deleteAutoBillTransfer"));
		ResponseEntity<DeleteAutoBillTransferOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<DeleteAutoBillTransferOutputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public Boolean changeLimitDirectly(String omniToken, List<ChangeLimitDTO> limits) {
		String path = "/card/changeLimitDirectly";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(ACCOUNT_SERVICE_URL + path);
		String uriString = uriBuilder.toUriString();
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<List<ChangeLimitDTO>> request = new RequestEntity<>(limits, headers, HttpMethod.POST,
				URI.create(uriString));
		ResponseEntity<Boolean> response = restTemplate.exchange(request, Boolean.class);
		return response.getBody();
	}

	@Override
	public OutputBaseDTO modifyAutoBillTransferDirectly(String omniToken, TransferAutoBillDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<TransferAutoBillDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/bill/modifyAutoBillTransferDirectly"));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();
	}

	@Override
	public List<StandingOrderOutputDTO> standingOrderList(String omniToken) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/bill/standingOrderList");

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(uriBuilder.toUriString()));
		ResponseEntity<List<StandingOrderOutputDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<StandingOrderOutputDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public OutputBaseDTO removeStandingOrder(String omniToken, String standingOrderId) {
		HttpHeaders headers = getHeader(omniToken);
		Map<String, String> input = new HashMap<String, String>();
		input.put("standingOrderId", standingOrderId);
		RequestEntity<Map> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/bill/removeStandingOrder"));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();
	}

	@Override
	public OutputBaseDTO createStandingOrder(String omniToken, StandingOrderInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<StandingOrderInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/bill/createStandingOrder"));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();
	}

	@Override
	public Boolean updateProductDefaultDirectly(String omniToken, Map<String, String> map) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<Map<String, String>> request = new RequestEntity<>(map, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/updateProductDefaultDirectly"));
		ResponseEntity<Boolean> response = restTemplate.exchange(request, new ParameterizedTypeReference<Boolean>() {
		});
		return response.getBody();
	}

	@Override
	public OutputBaseDTO modifyStandingOrderDirectly(String omniToken, StandingOrderInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<StandingOrderInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/bill/modifyStandingOrderDirectly"));
		ResponseEntity<OutputBaseDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<OutputBaseDTO>() {
				});
		return response.getBody();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.web.service.external.AccountServiceExt#transferAccounts(java.lang
	 * .String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<TransferAccountDTO> transferAccounts(String omniToken, String productList, String restrictions) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/account/transferAccounts?productList=" + productList
						+ "&restrictions=" + restrictions));

		ResponseEntity<List<TransferAccountDTO>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<TransferAccountDTO>>() {
				});
		return response.getBody();
	}

	@Override
	public QRcodeInfoOutputDTO checkQRcodeInfo(String omniToken, QRcodeInfoInputDTO input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<QRcodeInfoInputDTO> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/otp/checkQRcodeInfo"));
		ResponseEntity<QRcodeInfoOutputDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<QRcodeInfoOutputDTO>() {
				});
		return response.getBody();
	}

	@Override
	public PaymentsInFutureDTO getDetailPaymentsInFuture(String omniToken, String idFuturePayment) {

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET, URI
				.create(ACCOUNT_SERVICE_URL + "/payment/getDetailPaymentsInFuture?idFuturePayment=" + idFuturePayment));

		ResponseEntity<PaymentsInFutureDTO> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<PaymentsInFutureDTO>() {
				});
		return response.getBody();
	}

	@Override
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(String omniToken,
			ConfigStatusEcommerceCardInput input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<ConfigStatusEcommerceCardInput> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/card/configStatusEcommerceCard"));
		ResponseEntity<ConfigStatusEcommerceCardOutput> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<ConfigStatusEcommerceCardOutput>() {
				});
		return response.getBody();
	}

	@Override
	public CardPaymentInfoOutput cardPaymentInfo(String omniToken, CardPaymentInfoInput input) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<CardPaymentInfoInput> request = new RequestEntity<>(input, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/card/cardPaymentInfo"));
		ResponseEntity<CardPaymentInfoOutput> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<CardPaymentInfoOutput>() {
				});
		return response.getBody();
	}

	@Override
	public InputStream cardPaymentInfo(String omniToken, String linkStatementFile) throws IOException {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<RecipientDTO> request = new RequestEntity<>(headers, HttpMethod.GET,
				URI.create(ACCOUNT_SERVICE_URL + "/card/cardStatemenDownload?linkStatementFile=" + linkStatementFile));
		ResponseEntity<Resource> response = restTemplate.exchange(request, Resource.class);
		return response.getBody().getInputStream();
	}

	@Override
	public ChangeUsernameOutput changeUsername(String omniToken, ChangeUsernameInput changeUsername) {
		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<ChangeUsernameInput> request = new RequestEntity<>(changeUsername, headers, HttpMethod.POST,
				URI.create(ACCOUNT_SERVICE_URL + "/account/changeUsername"));
		ResponseEntity<ChangeUsernameOutput> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<ChangeUsernameOutput>() {
				});
		return response.getBody();
	}

	@Override
	public AccountDetails findAccountDetail(String omniToken, String accountNum) {

		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(ACCOUNT_SERVICE_URL + "/account/findAccountDetail");
		uriBuilder.queryParam("accountNum", accountNum);

		HttpHeaders headers = getHeader(omniToken);
		RequestEntity<ChangeUsernameInput> request = new RequestEntity<>(headers, HttpMethod.POST,
				uriBuilder.build().toUri());
		ResponseEntity<AccountDetails> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<AccountDetails>() {
				});
		return response.getBody();
	}

}