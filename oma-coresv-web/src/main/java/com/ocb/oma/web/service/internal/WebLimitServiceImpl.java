/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.WebLimitDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@Service
public class WebLimitServiceImpl implements WebLimitService {

	@Autowired
	private AccountServiceExt accountServiceExt;

	@Override
	public WebLimitDTO get() {
		String omniToken = AppUtil.getCurrentCPBToken();
		return accountServiceExt.getWebLimit(omniToken);
	}

}
