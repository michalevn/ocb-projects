/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import com.ocb.oma.dto.FormDTO;

/**
 * @author docv
 *
 */
public interface WebFormService {

	String creatForm(FormDTO formDTO);

}
