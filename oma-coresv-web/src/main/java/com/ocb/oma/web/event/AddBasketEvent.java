/**
 * 
 */
package com.ocb.oma.web.event;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.PostPaymentInputDTO;

/**
 * @author phuhoang
 *
 */
public class AddBasketEvent extends OmniEvent {

	/**
	 * @param userToken
	 * @param deviceInfo
	 * @param source
	 */
	public AddBasketEvent(UserToken userToken, PostPaymentInputDTO source) {
		super(userToken, source);
		// TODO Auto-generated constructor stub
	}

}
