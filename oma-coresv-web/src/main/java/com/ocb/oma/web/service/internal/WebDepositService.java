/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.DepositProductDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.input.BreakDepositInputDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;

/**
 * @author Phu Hoang
 *
 */
public interface WebDepositService {

	/**
	 * Tat toan
	 * 
	 * @param input
	 * @return
	 */
	OutputBaseDTO breakDeposit(BreakDepositInputDTO input);

	/**
	 * danh sach so tiet kiem
	 * 
	 * @return
	 */
	List<DepositDTO> getDeposits();

	/**
	 * Lay loaị sổ tiết kiêm
	 * 
	 * @return
	 */
	List<DepositProductDTO> getDepositProducts();

	/**
	 * Lấy kì hạn và lãi
	 * 
	 * @param subProductCode
	 * @param currency
	 * 
	 * @return
	 */
	List<DepositInterestRateDTO> getDepositInterestRates(String currency, String subProductCode);

	/**
	 * Tạo deposit
	 * 
	 * @param deposit
	 * @return
	 */
	CreateDepositOutputDTO createDeposit(CreateDepositDTO deposit);

	/**
	 * Lay danh sach tai khoan dc mo tien gui tiet kiem
	 * 
	 * @return
	 */
	DepositOfferAccountsDTO getDepositOfferAcc();

	DepositDTO getDeposit(String depositId);
}
