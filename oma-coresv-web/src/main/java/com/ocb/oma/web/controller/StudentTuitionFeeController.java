/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.StudentTuitionFeeDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.StudentTuitionFeeService;

@RestController
@RequestMapping("/service/student-tuition-fee")
public class StudentTuitionFeeController extends BaseController {

	@Autowired
	private StudentTuitionFeeService studentTuitionFeeService;

	@RequestMapping(value = "/get")
	public ResponseEntity<?> get(@RequestParam("studentCode") String studentCode,
			@RequestParam("universityCode") String universityCode) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		StudentTuitionFeeDTO rs = studentTuitionFeeService.get(studentCode, universityCode);
		return getFinalResponse(rs);
	}
}
