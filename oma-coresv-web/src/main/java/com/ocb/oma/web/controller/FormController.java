/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.service.internal.WebFormServiceImpl;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/forms")
public class FormController extends BaseController {

	@Autowired
	private ResourceServiceExt resourceServiceExt;
	@Autowired
	private WebFormServiceImpl formServiceImp;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody FormDTO formDTO) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		String uuid = formServiceImp.creatForm(formDTO);
		return getFinalResponse(uuid);
	}

	@RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
	public ResponseEntity<?> findByUuid(@PathVariable("uuid") String uuid) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		FormDTO formDTO = resourceServiceExt.findFormByUuid(uuid);
		return getFinalResponse(formDTO);
	}
}
