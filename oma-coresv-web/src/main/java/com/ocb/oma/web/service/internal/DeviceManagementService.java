/**
 * 
 */
package com.ocb.oma.web.service.internal;

/**
 * @author phuhoang
 *
 */
public interface DeviceManagementService {

	public String getTokenKey(String deviceId, String username);

	public String generateEncryptKey(String deviceId, String username);

}
