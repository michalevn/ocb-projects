/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author docv
 *
 */
@Service
public class WebTuitionFeeServiceImpl implements WebTuitionFeeService {

	@Autowired
	private ResourceServiceExt resourceServiceExt;

	@Override
	public List<TuitionFeeGroup> findGroups() {
		return resourceServiceExt.findTuitionFeeGroups();
	}

	@Override
	public List<TuitionFeeObject> findByGroup(String groupCode, int page, int size) {
		return resourceServiceExt.findTuitionFeesByGroup(groupCode, page, size);
	}

}
