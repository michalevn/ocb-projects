package com.ocb.oma.web.log;

import com.ocb.oma.web.util.CentralizedThreadLocal;

public class LogRefIdHolder {

	private static final ThreadLocal<String> LOG_REF_ID = new CentralizedThreadLocal<>(
			LogRefIdHolder.class + "._LOG_REF_ID");

	public static String get() {
		String refId = LOG_REF_ID.get();
		return refId;
	}

	public static void set(String refId) {
		LOG_REF_ID.set(refId);
	}

}
