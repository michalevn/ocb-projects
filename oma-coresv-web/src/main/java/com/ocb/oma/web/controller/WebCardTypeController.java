/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.CardTypeDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/cardTypes")
public class WebCardTypeController extends BaseController {

	@Autowired
	private ResourceServiceExt resourceServiceExt;

	@RequestMapping(value = "/find")
	public ResponseEntity<?> find(@RequestParam(name = "group", required = false) String group) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<CardTypeDTO> rs = resourceServiceExt.findCardTypes(group);
		return getFinalResponse(rs);
	}
}
