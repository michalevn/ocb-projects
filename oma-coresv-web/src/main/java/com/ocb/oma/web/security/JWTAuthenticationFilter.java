package com.ocb.oma.web.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.util.AppUtil;

public class JWTAuthenticationFilter extends GenericFilterBean {

	private ResourceServiceExt resourceService;
	private boolean allowMultipleDevices;

	public JWTAuthenticationFilter(ResourceServiceExt resourceService, Boolean allowMultipleDevices) {
		this.resourceService = resourceService;
		if (allowMultipleDevices == null) {
			this.allowMultipleDevices = false;
		} else {
			this.allowMultipleDevices = allowMultipleDevices.booleanValue();
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		Authentication authentication;
		HttpServletResponse reps = (HttpServletResponse) response;
		try {
			authentication = TokenAuthenticationService.getAuthentication((HttpServletRequest) request);
			UserToken userToken = getUserToken(authentication);
			if (userToken != null) {
				HttpServletRequest httpRequest = (HttpServletRequest) request;
				String requestURI = httpRequest.getRequestURI();
				boolean isLogginByToken = requestURI.endsWith("/loginByToken") || requestURI.endsWith("/invalidToken");
				if (isLogginByToken) {
					SecurityContextHolder.getContext().setAuthentication(authentication);
					filterChain.doFilter(request, response);
				} else {
					if (allowMultipleDevices) {
						SecurityContextHolder.getContext().setAuthentication(authentication);
						filterChain.doFilter(request, response);
					} else {
						String cif = userToken.getOldAuthen().getCustomerId();
						String deviceId = userToken.getDeviceId();
						if (!resourceService.isActiveUserDevice(cif, deviceId)) {
							HttpServletResponse httpServletResponse = (HttpServletResponse) response;
							AppUtil.accountSignedInAnotherDevice(httpServletResponse);
						} else {
							SecurityContextHolder.getContext().setAuthentication(authentication);
							filterChain.doFilter(request, response);
						}
					}
				}
			} else {
				AppUtil.buildLoginFailureResponse(reps);
			}
		} catch (AuthenticationException e) {
			// response.addHeader("WWW-Authenticate", "Basic realm=" +getRealmName());
			AppUtil.buildAccessDenyResponse(reps, e);
		}

	}

	public static UserToken getUserToken(Authentication authentication) {
		if (!(authentication instanceof AnonymousAuthenticationToken) && authentication != null
				&& authentication.getPrincipal() instanceof UserToken) {
			return (UserToken) authentication.getPrincipal();
		}
		return null;
	}
}