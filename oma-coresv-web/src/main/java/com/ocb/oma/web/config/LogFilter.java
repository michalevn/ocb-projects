package com.ocb.oma.web.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import com.ocb.oma.web.log.LogRefIdHolder;
import com.ocb.oma.web.util.KeyHelper;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LogFilter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOGGER.info(">>> Initiating LogFilter");
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		LogRefIdHolder.set(KeyHelper.next("REF_ID_"));

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		try {
			filterChain.doFilter(request, response);
		} catch (Exception e) {
			LOGGER.error(">>> ERROR: {}", e.getMessage());
			Map<String, String[]> parameterMap = request.getParameterMap();
			LOGGER.error(">>> REQUEST PARAMETERS");
			for (Entry<String, String[]> entry : parameterMap.entrySet()) {
				String[] values = entry.getValue();
				if (values != null) {
					if (values.length == 1) {
						LOGGER.error(">>> {} => {}", entry.getKey(), values[0]);
					} else {
						LOGGER.error(">>> {} => {}", entry.getKey(), Arrays.asList(values));
					}
				}
			}
		} finally {
			LOGGER.error(">>> RESPONSE STATUS: {}", response.getStatus());
		}

	}

	@Override
	public void destroy() {
	}

}
