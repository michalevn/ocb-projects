/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.TransactionInfo;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;
import com.ocb.oma.web.dto.ResponseDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Phu Hoang
 *
 */
public class AppUtil {

	private static final String DEVICEID_KEY = "DEVICEID_KEY";
	private static final String OMNI_TOKEN = "OMNI_TOKEN";
	private static final String CUSTOMER_CIF = "CUSTOMER_CIF";
	private static final String USER_NAME = "USERNAME";
	private static final String HASH_DATA = "HASHDATA";
	static ObjectMapper objectMapper = new ObjectMapper();
	static Logger LOGGER = LoggerFactory.getLogger(AppUtil.class);

	public static void buildAccessDenyResponse(HttpServletResponse response, Exception e) throws IOException {
		HttpServletResponse reps = response;
		reps.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		reps.setContentType("application/json");
		PrintWriter writer = response.getWriter();
		ResponseDTO out = ResponseDTO.getFailureResponse();
		out.setMessage(e.getMessage());
		out.setErrorCode(MessageConstant.INVALID_TOKEN);
		writer.println(objectMapper.writeValueAsString(out));
	}

	public static void accountSignedInAnotherDevice(HttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		PrintWriter writer = response.getWriter();
		ResponseDTO out = ResponseDTO.getFailureResponse();
		out.setErrorCode(MessageConstant.ACCOUNT_LOGGED_IN_ANOTHER_DEVICE);
		writer.println(objectMapper.writeValueAsString(out));
	}

	public static void buildLoginFailureResponse(HttpServletResponse response) throws IOException {
		buildLoginFailureResponse(response, null);
	}

	public static void buildLoginFailureResponse(HttpServletResponse response, AuthenticationException e)
			throws IOException {
		response.setContentType("application/json");
		// response.setStatus(HttpServletResponse.SC_OK);
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		PrintWriter writer = response.getWriter();
		ResponseDTO out = ResponseDTO.getFailureResponse();
		out.setErrorCode(MessageConstant.ACCESS_FORBIDDEN);

		writer.println(objectMapper.writeValueAsString(out));
	}

	public static String generateToken(Map<String, Object> extData, String username, Date expireDate,
			String secretKey) {
		return Jwts.builder().setClaims(extData).setSubject(username).setExpiration(expireDate)
				.signWith(SignatureAlgorithm.HS512, secretKey).compact();
	}

	public static String generateToken(String deviceId, String username, String hashData, Date expireDate,
			String secretKey, OldAuthenticateResponseDTO oldOmniAuthen) {
		Map<String, Object> extData = new HashMap();

		extData.put(DEVICEID_KEY, deviceId);
		extData.put(OMNI_TOKEN, oldOmniAuthen.getJwtToken());
		extData.put(CUSTOMER_CIF, oldOmniAuthen.getCustomerId());
		extData.put(USER_NAME, username);
		extData.put(HASH_DATA, hashData);
		return generateToken(extData, username, expireDate, secretKey);
	}

	public static UserToken getUserToken(String token, String secretKey) {
		try {
			Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
			String userName = claims.getSubject();
			Date expireDate = claims.getExpiration();
			String deviceId = claims.get(DEVICEID_KEY, String.class);
			String jwtOmniAuthen = claims.get(OMNI_TOKEN, String.class);
			String customerCif = claims.get(CUSTOMER_CIF, String.class);
			String hashData = claims.get(HASH_DATA, String.class);
			LOGGER.debug("token old authen ", jwtOmniAuthen);
			UserToken userToken = new UserToken();
			userToken.setUsername(userName);
			userToken.setToken(token);
			userToken.setDeviceId(deviceId);
			userToken.setExpireTime(expireDate);
			userToken.setHashedData(hashData);
			userToken.setDeviceId(deviceId);
			OldAuthenticateResponseDTO oldAuthen = new OldAuthenticateResponseDTO();
			oldAuthen.setJwtToken(jwtOmniAuthen);
			oldAuthen.setCustomerId(customerCif);
			userToken.setOldAuthen(oldAuthen);

			return userToken;
		} catch (Exception e) {
			LOGGER.info("invalid token ", e);
			return null;
		}
	}

	public static TransactionInfo generateTransactionInfo(String userId) {

		TransactionInfo data = new TransactionInfo();
		data.setUserId(userId);
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		data.setcRefNum(randomUUIDString);

		return data;
	}

	public static UserToken getCurrentUserLogin() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken) && authentication != null
				&& authentication.getPrincipal() instanceof UserToken) {
			UserToken userToken = (UserToken) authentication.getPrincipal();
			return userToken;
		}
		return null;
	}

	public static String getCurrentUsernameLogin() {
		UserToken token = getCurrentUserLogin();
		if (token == null)
			return null;
		return token.getUsername();
	}

	public static String getCurrentCPBToken() {
		UserToken token = getCurrentUserLogin();
		if (token == null)
			return null;
		if (token.getOldAuthen() == null) {
			return null;
		}
		return token.getOldAuthen().getJwtToken();
	}

	public static String getCurrentCustomerCif() {
		UserToken token = getCurrentUserLogin();
		if (token == null)
			return null;
		if (token.getOldAuthen() == null) {
			return null;
		}
		return token.getOldAuthen().getCustomerId();
	}
}
