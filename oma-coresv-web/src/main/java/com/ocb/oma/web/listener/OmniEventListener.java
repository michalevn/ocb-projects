/**
 * 
 */
package com.ocb.oma.web.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.ocb.oma.web.event.OmniEvent;

/**
 * @author phuhoang
 *
 */
@Component
public class OmniEventListener implements ApplicationListener<OmniEvent> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.
	 * springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(OmniEvent event) {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println("**********Omni Event" + event.getUserToken().getUsername());
	}

}
