/**
 * 
 */
package com.ocb.oma.web.event;

import com.ocb.oma.dto.UserToken;

/**
 * @author phuhoang
 *
 */
public class DeleteRecipientEvent extends OmniEvent {

	/**
	 * @param userToken2
	 * @param source
	 */
	public DeleteRecipientEvent(UserToken userToken2, Object source) {
		super(userToken2, source);
		// TODO Auto-generated constructor stub
	}

}
