/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.GroupCode;
import com.ocb.oma.web.service.internal.WebTuitionFeeService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/service/tuitionFee")
public class WebTuitionFeeController extends BaseController {

	@Autowired
	private WebTuitionFeeService tuitionFeeService;

	@RequestMapping(value = "/findGroups")
	public ResponseEntity<?> findGroups() {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<TuitionFeeGroup> rs = tuitionFeeService.findGroups();
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/findByGroup")
	public ResponseEntity<?> findByGroup(
			@GroupCode(message = "GROUPCODE_INVALID") @RequestParam("groupCode") String groupCode,
			@Digits @RequestParam("page") Integer page, @Digits @RequestParam("size") Integer size) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<TuitionFeeObject> rs = tuitionFeeService.findByGroup(groupCode, page, size);
		return getFinalResponse(rs);
	}
}
