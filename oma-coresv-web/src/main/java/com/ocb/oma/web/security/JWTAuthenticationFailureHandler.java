/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.security;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.web.dto.ResponseDTO;

/**
 * @author Phu Hoang
 *
 */

public class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {

	private ObjectMapper objectMapper;

	private MessageSource messageSource;

	public JWTAuthenticationFailureHandler(ObjectMapper objectMapper, MessageSource messageSource) {
		super();
		this.objectMapper = objectMapper;
		this.messageSource = messageSource;
	}

	public String getMessage(String code, Locale locale) {

		return messageSource.getMessage(code, null, code, locale);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.web.authentication.AuthenticationFailureHandler#
	 * onAuthenticationFailure(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse,
	 * org.springframework.security.core.AuthenticationException)
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
		response.setContentType("application/json");
		ResponseDTO data = ResponseDTO.getFailureResponse();
		data.setMessage(getMessage(MessageConstant.ACCESS_FORBIDDEN, request.getLocale()));
		data.setErrorCode(MessageConstant.ACCESS_FORBIDDEN);
		response.getWriter().write(objectMapper.writeValueAsString(data));
		// Map<String, V>

	}

}
