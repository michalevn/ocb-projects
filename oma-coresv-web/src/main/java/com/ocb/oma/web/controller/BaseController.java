/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.event.OmniEvent;

/**
 * @author Phu Hoang
 *
 */
public class BaseController {
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public ResponseEntity<?> getFinalResponse(Object input) {

		ResponseDTO result = ResponseDTO.getSuccessResponse();
		result.setData(input);
		return ResponseEntity.ok(result);
	}

	public ResponseEntity<?> getFailureResponse(Object input) {

		ResponseDTO result = ResponseDTO.getFailureResponse();
		result.setData(input);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
	}

	public ResponseEntity<?> getFailureResponse(Object input, String errorCode) {

		ResponseDTO result = ResponseDTO.getFailureResponse();
		result.setData(input);
		result.setErrorCode(errorCode);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
	}

	public UserToken getUserToken() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			UserToken currentUserName = (UserToken) authentication.getPrincipal();
			return currentUserName;
		}
		return null;
	}

	protected void publicEvent(OmniEvent omniEvent) {
		applicationEventPublisher.publishEvent(omniEvent);
	}
}
