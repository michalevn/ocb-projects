/**
 * 
 */
package com.ocb.oma.web.event;

import com.ocb.oma.dto.UserToken;

/**
 * @author phuhoang
 *
 */
public class DeactiveDeviceEvent extends OmniEvent {

	private int enable;
	private UserToken userToken;
	private String deviceName;

	public DeactiveDeviceEvent(UserToken userTokens, String deviceName, int result) {
		super(userTokens);
		this.setUserToken(userTokens);
		this.setDeviceName(deviceName);
		this.setEnable(result);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the userToken
	 */
	public UserToken getUserToken() {
		return userToken;
	}

	/**
	 * @param userToken the userToken to set
	 */
	public void setUserToken(UserToken userToken) {
		this.userToken = userToken;
	}

	/**
	 * @return the enable
	 */
	public int getEnable() {
		return enable;
	}

	/**
	 * @param enable the enable to set
	 */
	public void setEnable(int enable) {
		this.enable = enable;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
