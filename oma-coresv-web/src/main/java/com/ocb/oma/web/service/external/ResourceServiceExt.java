/**
 * 
 */
package com.ocb.oma.web.service.external;

import java.util.List;
import java.util.Map;

import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.CardTypeDTO;
import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.resources.model.OmniGiftCardMsgTemplate;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.model.UserDeviceToken;

/**
 * @author phuhoang
 *
 */
public interface ResourceServiceExt {

	List<BankInfoDTO> getBankInfos();

	List<ListOfValueDTO> findListOfValue(String category);

	/**
	 * lay danh sach ngan hang theo citad
	 * 
	 * @param bankCode
	 * @param provinceCode
	 * @return
	 */
	List<BankInfoDTO> searchBankCitad(String bankCode, String provinceCode);

	/**
	 * lay bankCode tu bankCidtadCode
	 * 
	 * @param bankCitadCode
	 * @return
	 */
	String getByBankCitadId(String bankCitadCode);;

	/**
	 * tim kiem
	 * 
	 * @param cardType
	 * @return
	 */
	List<OmniGiftCardMsgTemplate> findGiftCardMsgTemplate(String cardType);

	/**
	 * lay danh sach mobiler device cua user
	 * 
	 * @param username
	 * @return
	 */
	List<UserDeviceToken> findUserDevices(String cif);

	/**
	 * Kich hoat hay ngat kich hoat thiet bi theo nguoi dung
	 * 
	 * @param cif
	 * @param deviceId
	 * @param status
	 * @return
	 */
	Boolean updateUserDeviceStatus(String cif, String deviceId, Integer status);

	/**
	 * save DB sendFGiftCard
	 * 
	 * @param giftCardInsertDTO
	 * @return
	 */
	void sendGiftCardResource(GiftCardInsertDTO giftCardInsertDTO, String omniToken);

	/**
	 * Gui notification den nguoi dung
	 * 
	 * @param notification
	 * @return
	 */
	Boolean pushNotification(OmniNotification omniNotification);

	List<OmniNotification> findByUserIdAndType(String query, String userId, Integer type, Integer page, Integer size);

	OmniNotification get(Long notificationId);

	/**
	 * Hàm lấy danh sách loại thẻ để đăng ký
	 * 
	 * @param group
	 * 
	 * @return
	 */
	List<CardTypeDTO> findCardTypes(String group);

	List<TuitionFeeGroup> findTuitionFeeGroups();

	List<TuitionFeeObject> findTuitionFeesByGroup(String groupCode, int page, int size);


	FormDTO findFormByUuid(String uuid);

	Boolean registerUserDevice(UserDeviceToken userDeviceToken);

	/**
	 * Lay thong tin bank theo chi nhanh
	 * 
	 * @param branchCode
	 * @return
	 */
	Map<String, Object> getByBankInfoByBranchCode(String branchCode);

	/**
	 * Xoa cac notifications
	 * 
	 * @param cif
	 * @param notificationIds
	 * @return
	 */
	Boolean deleteNotifications(String cif, List<Long> notificationIds);

	/**
	 * Danh dau cac notifications la da doc
	 * 
	 * @param cif
	 * @param notificationIds
	 * @return
	 */
	Boolean maskAsRead(String cif, List<Long> notificationIds);

	/**
	 * Danh dau cac notifications la chua doc
	 * 
	 * @param cif
	 * @param notificationIds
	 * @return
	 */
	Boolean maskAsUnread(String cif, List<Long> notificationIds);

	/**
	 * Lay danh sach links theo group
	 * 
	 * @param group
	 * @return
	 */
	List<LinkDTO> getLinksByGroup(String group);

	/**
	 * Dang ky thiet bi
	 * 
	 * @param mobileDevice
	 * @return
	 */
	Boolean registerDevice(OmniMobileDevice mobileDevice);

	/**
	 * Lay UserDeviceToken cua nguoi dung hien tai theo thiet bi
	 * 
	 * @param cif
	 * @param deviceId
	 * @param deviceToken
	 * @return
	 */
	UserDeviceToken activeUserDevice(String cif, String deviceId, String deviceToken);

	/**
	 * Dem so notifications chua doc trong durationInDays ngay tinh tu ngay hien tai
	 * 
	 * @param cif
	 * @param durationInDays
	 * @return
	 */
	Integer countUnreadNotifications(String cif, Integer durationInDays);

	Boolean deactivateUserDevice(String cif);

	boolean isActiveUserDevice(String cif, String deviceId);

	UserDeviceToken getUserDevice(String cif, String deviceId);

	/**
	 * lay bank info tu napas code
	 * 
	 * @param codeNapas
	 * @return
	 */
	BankInfoDTO getBankByCodeNapas(String codeNapas);

	String creatForm(String omniToken, FormDTO formDTO);

	/**
	 * 
	 * @param formDTO
	 */
	String sendMailForm(FormDTO formDTO);

}
