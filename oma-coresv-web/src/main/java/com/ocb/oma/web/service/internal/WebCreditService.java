/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.util.Date;
import java.util.List;

import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;

/**
 * @author docv
 *
 */
public interface WebCreditService {

	public List<CreditDTO> getCredits();

	public List<InstalmentTransactionHistoryDTO> getInstalmentTransactionHistory(String contractNumber, Date dateFrom,
			Date dateTo, Integer pageNumber, Integer pageSize, Double operationAmountFrom, Double operationAmountTo);

}
