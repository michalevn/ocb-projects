/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.SampleUserDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.validation.BankCode;
import com.ocb.oma.validation.Validation;
import com.ocb.oma.validation.impl.SampleValidator;
import com.ocb.oma.web.service.internal.SampleService;

@RestController
@RequestMapping("/service/sample-validations")
@Validated
public class SampleValidationController extends BaseController implements ApplicationContextAware {

	@RequestMapping(value = "/validate-get-params")
	public ResponseEntity<?> func1(@Email(message = "EMAIL_INVALID") @RequestParam("email") String email,
			@Size(min = 8, max = 12, message = "NAME_LENGTH_INVALID") @RequestParam("name") String name) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse("OK");
	}

	@RequestMapping(value = "/validate-post-params")
	@BankCode
	public ResponseEntity<?> func2(@Valid @RequestBody SampleUserDTO dto) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse("OK");
	}

	@RequestMapping(value = "/validate-custom-params")
	@Validation(validators = { SampleService.class })
	public ResponseEntity<?> func3(@RequestBody SampleUserDTO dto) {
		UserToken userToken = getUserToken();
		if (userToken == null) {
			return getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse("OK");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		Map<String, SampleValidator> validators = applicationContext.getBeansOfType(SampleValidator.class);
		System.out.println(validators);
	}
}
