/**
 * 
 */
package com.ocb.oma.web.listener;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.event.SendGiftCardEvent;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author phuhoang
 *
 */
@Component
public class SendGiftCardEventListener implements ApplicationListener<SendGiftCardEvent> {

	@Autowired
	private ResourceServiceExt resourceService;

	@Autowired
	private AccountServiceExt accountService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ObjectMapper objectMapper;

	private static final Locale elLocal = new Locale("el");
	private static final Locale viLocal = new Locale("vi");
	private static final NumberFormat NUMBER_FORMAT = NumberFormat.getNumberInstance(Locale.GERMAN);

	private static final Log LOG = LogFactory.getLog(SendGiftCardEventListener.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.
	 * springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(SendGiftCardEvent event) {

		UserToken userToken = event.getUserToken();
		if (userToken == null || userToken.getOldAuthen() == null
				|| StringUtils.isBlank(userToken.getOldAuthen().getJwtToken())) {
			throw new OmaException(MessageConstant.ACCESS_FORBIDDEN);
		}

		String omniToken = event.getUserToken().getOldAuthen().getJwtToken();

		GiftCardInsertDTO giftCardInsertDTO = event.getInput();
		OmniGiftCard giftCard = giftCardInsertDTO.getGiftCard();

		String senderId = event.getUserToken().getOldAuthen().getCustomerId();
		String senderAccountId = giftCard.getDebitAccountId();
		AccountInfo senderAccount = getAccountByAccountId(senderId, senderAccountId, omniToken);
		if (senderAccount == null) {
			LOG.error(">>> Sender Not Found: " + senderId);
			return;
		}
		String senderBalance = formatNumber(senderAccount.getCurrentBalance());
		String senderAccountNo = senderAccount.getAccountNo();

		Date executionDate;

		resourceService.sendGiftCardResource(giftCardInsertDTO, omniToken);

		String titleVi;
		String messageVi;
		String titleEl;
		String messageEl;

		List<OmniGiftCardReceiver> receivers = giftCardInsertDTO.getReceivers();

		String senderName = userToken.getOldAuthen().getFirstName();
		if (StringUtils.isBlank(senderName)) {
			senderName = "";
		}

		if (receivers != null && !receivers.isEmpty()) {
			for (OmniGiftCardReceiver omniGiftCardReceiver : receivers) {

				executionDate = omniGiftCardReceiver.getCreatedDate();
				String strExecutionDate = formatDate(executionDate);
				String amount = formatNumber(omniGiftCardReceiver.getAmount());

				String receiverAccountNo = omniGiftCardReceiver.getBeneficiary();
				String receiverId = accountService.getCifByAccountNo(receiverAccountNo, omniToken);
				String receiverName = omniGiftCardReceiver.getRecipient();

				String receiver = receiverAccountNo;
				if (StringUtils.isNotBlank(receiverName)) {
					receiver = receiver + ", " + receiverName;
				}

				titleVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.BODY_5", viLocal);

				messageVi = String.format(messageVi, senderAccountNo, amount, strExecutionDate, receiver,
						senderBalance);

				titleEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.SENDER.BODY_5", elLocal);
				messageEl = String.format(messageEl, senderAccountNo, amount, strExecutionDate, receiver,
						senderBalance);

				pushNotification(senderId, titleVi, messageVi, titleEl, messageEl, executionDate);

				String senderInfo = senderAccountNo;
				if (StringUtils.isNotBlank(senderName)) {
					senderInfo = senderInfo + ", " + senderName;
				}

				titleVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.TITLE", viLocal);
				messageVi = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.BODY", viLocal);
				messageVi = String.format(messageVi, receiverAccountNo, amount, executionDate, senderInfo);

				titleEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.TITLE", elLocal);
				messageEl = getMessage("POST_PAYMENT.INTERNAL_PAYMENT.RECEIVER.BODY", elLocal);
				messageEl = String.format(messageEl, receiverAccountNo, amount, executionDate, senderInfo);

				pushNotification(receiverId, titleVi, messageVi, titleEl, messageEl, executionDate);

				String message = giftCard.getShort_message();

				pushNotification(receiverId, message, message, message, message, executionDate);

				omniGiftCardReceiver.setCif(receiverId);
			}
		}

	}

	private String getMessage(String key, Locale locale) {
		return messageSource.getMessage(key, new Object[0], locale);
	}

	private AccountInfo getAccountByAccountId(String cif, String accountId, String omniToken) {
		List<AccountInfo> accounts = accountService.getAccountList(cif, omniToken);
		for (AccountInfo accountInfo : accounts) {
			if (accountId.equals(accountInfo.getAccountId())) {
				return accountInfo;
			}
		}
		return null;
	}

	private static String formatDate(Date datetime) {
		if (datetime == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(datetime);
	}

	private static String formatNumber(Number number) {
		if (number == null) {
			return "";
		}
		return NUMBER_FORMAT.format(number);
	}

	private void pushNotification(String userId, String titleVi, String messageVi, String titleEl, String messageEl,
			Date executionDate) {

		// String cif = event.getUserToken().getOldAuthen().getCustomerId();

		try {

			String descriptionVi = Jsoup.parse(messageVi).text();
			if (descriptionVi.length() > 100) {
				descriptionVi = descriptionVi.substring(0, 100).concat("...");
			}
			String descriptionEl = Jsoup.parse(messageEl).text();
			if (descriptionEl.length() > 100) {
				descriptionEl = descriptionVi.substring(0, 100).concat("...");
			}
			OmniNotification omniNotification = new OmniNotification();
			omniNotification.setType(OmniNotification.TYPE_THONG_BAO.toString());
			omniNotification.setUserId(userId);
			int distributedType = OmniNotification.DISTRIBUTED_TYPE_APP | OmniNotification.DISTRIBUTED_TYPE_FIREBASE;
			omniNotification.setDistributedType(distributedType);
			omniNotification.setTitleVn(titleVi);
			omniNotification.setTitleEl(titleEl);
			omniNotification.setDescriptionVn(descriptionVi);
			omniNotification.setDescriptionEl(descriptionEl);
			omniNotification.setBodyVn(messageVi);
			omniNotification.setBodyEl(messageEl);
			omniNotification.setCreatedDate(executionDate);

			LOG.info(">>> pushNotification: " + objectMapper.writeValueAsString(omniNotification));

			resourceService.pushNotification(omniNotification);

		} catch (Exception e) {
			LOG.error(e, e);
		}

	}
}
