/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/campaign")
public class WebCaimpagnController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	//
	@RequestMapping(value = "/getCampaignList")
	public ResponseEntity<?> getCampaignList() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getCampaignList());
	}
}
