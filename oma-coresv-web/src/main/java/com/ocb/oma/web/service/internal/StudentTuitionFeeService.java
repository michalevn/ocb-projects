/**
 * @author docv
 */
package com.ocb.oma.web.service.internal;

import com.ocb.oma.dto.StudentTuitionFeeDTO;

public interface StudentTuitionFeeService {

	StudentTuitionFeeDTO get(String studentCode, String universityCode);
}
