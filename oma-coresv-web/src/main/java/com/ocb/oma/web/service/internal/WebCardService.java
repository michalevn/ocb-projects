/**
 * @author Phu Hoang
 */
package com.ocb.oma.web.service.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.CardInfoDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeLimitDirectlyInput;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PostAuthActivateCardInput;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardPaymentInfoOutput;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.oomni.dto.ConfigStatusEcommerceCardOutput;

/**
 * @author Phu Hoang
 *
 */
public interface WebCardService {
	/**
	 * Tim the tin dung
	 * 
	 * @return
	 */
	public List<CardInfoDTO> findCard();

	/**
	 * Khoa the
	 * 
	 * @param cardNumber
	 * @param expiryDate
	 * @param restrictionReason
	 * @return
	 */

	public OutputBaseDTO restrictCard(String cardNumber, String restrictionReason);

	/**
	 * PHONG TOA
	 * 
	 * @param accountNumber
	 * @param cardNumber
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	public List<CardBlockade> findCardBlockades(CardBlockadeInputDTO input);

	/**
	 * 
	 * Kich hoat the
	 * 
	 * @param authActivateCard
	 * @return
	 */
	public OutputBaseDTO postAuthActivateCard(PostAuthActivateCardInput postAuthActivateCardInput);

	/**
	 * Dang ky/sua/xoa trich no tu dong
	 * 
	 * @param cardAccountAutoRepayment
	 * @return
	 */
	public Boolean modifyCardAccountAutoRepayment(CardAccountAutoRepaymentDTO cardAccountAutoRepayment);

	/**
	 * Liet ke danh sach lich su giao dich cua the credit/debit
	 * 
	 * @param cardAccountEntryQuery
	 * @return
	 */
	public List<CardAccountEntryDTO> findCardAccountEntry(CardAccountEntryQueryDTO cardAccountEntryQuery);

	/**
	 * Lay thong tin chi tiet cua giao dich trong tuong lai.
	 * 
	 * @param paymentId id cua payment duoc lay thu getPaymentsInFuture
	 * @return
	 */
	public PaymentsInFutureDTO getPaymentDetails(String paymentId);

	/**
	 * thanh toan the tin dung cho nguoiw khac
	 * 
	 * @param input
	 * @return
	 */
	public PaymentCardAnotherOuputDTO postpaymentCardAnother(PaymentCardAnotherInputDTO input);

	/**
	 * lay thong tin chi tiet the theo cardid
	 * 
	 * @param cardId
	 * @return
	 */
	public CardDetailByCardIdDTO findCardByCardId(String cardId);

	/**
	 * ham lay danh sach the trich no tu dong
	 * 
	 * @return
	 */
	public List<CardAutoRepaymentDTO> cardAutoRepayment();

	/**
	 * Lay danh sach giao dich the bi tu choi
	 * 
	 * @param cardRejectedInput
	 * @return List<CardRejectedDTO>
	 */
	public List<CardRejectedDTO> cardRejected(CardRejectedInputDTO cardRejectedInput);

	/**
	 * Sao ke
	 * 
	 * @param input
	 * @return
	 */
	public List<CardStatementsDTO> cardStatements(CardStatementsInputDTO input);

	public Boolean changeLimitDirectly(ChangeLimitDirectlyInput limits);

	/**
	 * cap nhat trang thai lock/unlock giao dich online the tin dung
	 * 
	 * @param input
	 * @return
	 */
	public ConfigStatusEcommerceCardOutput configStatusEcommerceCard(ConfigStatusEcommerceCardInput input);

	/**
	 * 1.99	Ham truy van thong tin the thanh toan - card_payment_info
	 * @param input
	 * @return
	 */
	public CardPaymentInfoOutput cardPaymentInfo(CardPaymentInfoInput input);

	/**
	 * 
	 * @param linkStatementFile
	 * @return
	 * @throws IOException 
	 */
	public InputStream cardStatemenDownload(String linkStatementFile) throws IOException;
	
	
}
