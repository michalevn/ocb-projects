/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.validation.CIF;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.PaymentType;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

/**
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/service/otp")
public class WebOtpController extends BaseController {

	@Autowired
	private WebAccountSerivice accountService;

	//
	@RequestMapping(value = "/sendAuthorizationSmsOtp")
	public ResponseEntity<?> sendAuthorizationSmsOtp() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.sendAuthorizationSmsOtp());
	}
	// verifyOTPToken

	@RequestMapping(value = "/verifyOTPToken")
	public ResponseEntity<?> verifyOTPToken(@RequestBody VerifyOtpToken data) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.verifyOTPToken(data));
	}

	@RequestMapping(value = "/verifyHWOTPToken")
	public ResponseEntity<?> verifyHWOTPToken(@RequestBody Map<String, String> data) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.verifyHwOTPToken(data.get("tokenValue")));
	}

	@RequestMapping(value = "/getAuthorizeMethodByAmount")
	public ResponseEntity<?> getAuthorizeMethodByAmount(@Digits @RequestParam("amount") Double amount,
			@PaymentType @RequestParam("paymentType") String paymentType) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAuthorizeMethodByAmount(amount, paymentType));
	}

	@RequestMapping(value = "/getChallengeCodeSoftOTP")
	public ResponseEntity<?> getChallengeCodeSoftOTP(HttpServletRequest request,
			@RequestBody GetChallengeCodeInputDTO input) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getChallengeCodeSoftOTP(input));
	}

	@RequestMapping(value = "/getAuthorizationPolicies")
	public ResponseEntity<?> getAuthorizationPolicies(@RequestParam("userId") String userId) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getAuthorizationPolicies(userId));
	}

	@RequestMapping(value = "/getQD630Policies")
	public ResponseEntity<?> testGetQD630Policies(@CIF @RequestParam("cif") String cif) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		List<QD630PoliciesDTO> rs = accountService.getQD630Policies(cif);
		return getFinalResponse(rs);
	}

	@RequestMapping(value = "/getListPrepaidPhone")
	public ResponseEntity<?> testGetListPrepaidPhone() {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.getListPrepaidPhone());
	}

	@RequestMapping(value = "/checkQRcodeInfo")
	public ResponseEntity<?> checkQRcodeInfo(@RequestBody QRcodeInfoInputDTO input) {

		UserToken userToken = getUserToken();
		if (userToken == null) {
			getFailureResponse("", MessageConstant.INVALID_TOKEN);
		}
		return getFinalResponse(accountService.checkQRcodeInfo(input));
	}
}
