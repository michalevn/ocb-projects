package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class CreditControllerTest extends BaseTest {

	@Test
	public void test_getCredits() throws Exception {
		try {

			String path = "/service/credit/getCredits";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getInstalmentTransactionHistory() throws Exception {
		try {

			String path = "/service/credit/getInstalmentTransactionHistory?contractNumber=&dateFrom=1544091273782&dateTo=1544091273782&pageNumber=1&pageSize=10&operationAmountFrom=0&operationAmountTo=10000";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
