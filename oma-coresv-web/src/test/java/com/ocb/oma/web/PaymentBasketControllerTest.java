package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.BasketTransferDTO;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.ExecutedStatus;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentDTO;
import com.ocb.oma.dto.PaymentDetails;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.StatusesType;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.GetListOfBasketInputDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PaymentItemStudentFeeDTO;
import com.ocb.oma.oomni.dto.PostAddBasketDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.web.controller.WebAccountController;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.TokenManagementService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class PaymentBasketControllerTest extends BaseMockTest {

	WebAccountController controller;

	@Autowired
	TokenManagementService tokenManagementService;

	@Test
	public void testcountBasketPaymentToProcess() throws Exception {
		initLoginMock();
		Integer mockCount = 20;
		when(accountService.countBasketPaymentToProcess(anyString())).thenReturn(mockCount);
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		String path = "/service/paymentBasket/countBasketPaymentToProcess";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testcountBasketPaymentToProcess : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertEquals(mockCount, response.getData());
	}

	@Test
	public void testgetTransactionFee() throws Exception {
		initLoginMock();
		Double mockData = 1000D;
		when(accountService.getTransactionFee(ArgumentMatchers.anyObject(), anyString())).thenReturn(mockData);
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		TransactionFeeInputDTO inputDTO = new TransactionFeeInputDTO("010892111", "0129192911", 100000D, "vnd",
				PaymentType.InternalPayment.getPaymentTypeCode(), "provinceCode", new Date());

		String path = "/service/paymentBasket/getTransactionFee";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testgetTransactionFee : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertEquals(mockData, response.getData());
	}

	private PostPaymentOutputDTO initTestPostPayment() {
		PostPaymentOutputDTO output = new PostPaymentOutputDTO();

		output.setStatus(ExecutedStatus.EXECUTED.getCode());
		output.setSuccess(true);
		output.setReferenceId("GW182961415153625.01");
		output.setCoreRefNum2("FT171645F7M3");
		return output;
	}

	@Test
	public void testPostPayment() throws Exception {
		initLoginMock();
		PostPaymentOutputDTO mockData = initTestPostPayment();
		when(accountService.postPayment(ArgumentMatchers.any(), anyString())).thenReturn(mockData);
		// mock otp return
		AuthorizationResponse mockOtpResponse = new AuthorizationResponse();
		mockOtpResponse.setOperationStatus(AuthorizationResponse.CORRECT);
		when(accountService.verifyOTPToken(ArgumentMatchers.any(), anyString())).thenReturn(mockOtpResponse);
		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();

//		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		inputDTO.setPaymentType("InternalPayment");
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("11"); // ibtest10
		paymentInfo.setAmount(12345L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0037100003605005"); // ibtest9
		paymentInfo.setRecipient("Hoang Nguyen Sy Phu");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);
		inputDTO.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		VerifyOtpToken otpValue = new VerifyOtpToken();
		otpValue.setAuthId("12345");
		otpValue.setOtpValue("123");
		inputDTO.setOtpValue(otpValue);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		Thread.sleep(1000);
	}

	@Test
	public void testPostPayment_WRONG_OTP() throws Exception {
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		PostPaymentOutputDTO mockData = initTestPostPayment();
		when(accountService.postPayment(ArgumentMatchers.any(), anyString())).thenReturn(mockData);
		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();

//		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		inputDTO.setPaymentType("InternalPayment");
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("45");
		paymentInfo.setAmount(12345L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0100100007826003");
		paymentInfo.setRecipient("Hoang Nguyen Sy Phu");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);
		inputDTO.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());

		inputDTO.setOtpValue(otpValue2);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().is5xxServerError()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
		assertEquals(false, response.isSuccess());
	}

	@Test
	public void testPostPayment_NoOTP() throws Exception {
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		PostPaymentOutputDTO mockData = initTestPostPayment();
		when(accountService.postPayment(ArgumentMatchers.any(), anyString())).thenReturn(mockData);
		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();

//		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		inputDTO.setPaymentType("InternalPayment");
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("45");
		paymentInfo.setAmount(12345L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0100100007826003");
		paymentInfo.setRecipient("Hoang Nguyen Sy Phu");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);
		inputDTO.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());

		// inputDTO.setOtpValue(otpValue2);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().is5xxServerError()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
		assertEquals(false, response.isSuccess());
	}

	@Test
	public void testPostAddBasket() throws Exception {
		initLoginMock();
		PostAddBasketDTO mockData = new PostAddBasketDTO();
		mockData.setResultCode("1");
		mockData.setResultMsg("Thanh cong");
		when(accountService.postAddBasket(ArgumentMatchers.any(), anyString())).thenReturn(mockData);

		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		// ck noi bo
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount((long) 123456789);
		paymentInfo.setCreditAccount("0101898177");
		paymentInfo.setRecipient("0101010101010110");
		paymentInfo.setRemarks("Note giao dich");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setcRefNum("cRefNum");

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO(PaymentType.FastTransfer.getPaymentTypeCode(),
				paymentInfo);

		String path = "/service/paymentBasket/postAddBasket";
		System.out.println("***payload of testpostAddBasket " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testpostAddBasket : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testGetListofBasket() throws Exception {
		initLoginMock();
		List<PaymentBasketDTO> mockData = new ArrayList<>();
		Integer pagesize = 10;
		for (Integer i = 0; i < pagesize; i++) {
			PaymentBasketDTO dto = new PaymentBasketDTO();
			dto.setAccountId("" + i + 1);
			List<BasketTransferDTO> basketTransfers = new ArrayList<>();
			BasketTransferDTO basketTransferDTO = new BasketTransferDTO();
			PaymentDTO payment = new PaymentDTO();
			payment.setAccountId("accountId" + i + 1);
			basketTransferDTO.setPayment(payment);

			dto.setBasketTransfers(basketTransfers);
//			String recipientName = "Nguyen Huy Hoang";
//			String accountId = "456";
//			Double amount = 5000000D;
//			String currency = "VND";
//			String accountNo = "KHTT";
//			String beneficiaryAccountNo = "VIPP";
//			String bankName = "OOC";
//			String bankCode = "05";
//			String paymentType = PaymentType.InternalPayment.getPaymentTypeCode();
//			String provinceId = "004";
//			String branchCode = "3";
//			String cardNumber = "00033998776";
//			String bankCodeMa = "MHOH098778";
//			String remarks = "Chuyen tien nOi dia";
//			Date executionDate = new Date();
//			Date createdDate = new Date();
//			if (paymentType.equals(PaymentType.InternalPayment.getPaymentTypeCode())) {
//
//				cardNumber = null;
//				branchCode = null;
//				provinceId = null;
//				bankCode = null;
//				bankName = null;
//
//			}
//			if (paymentType.equals(PaymentType.LocalPayment.getPaymentTypeCode())) {
//				cardNumber = null;
//			}
//			if (paymentType.equals(PaymentType.FastTransfer.getPaymentTypeCode())) {
//
//				// cardNumber = null;
//				branchCode = null;
//				provinceId = null;
//				bankCode = null;
//				bankName = null;
//
//			}
//			PaymentBasketDTO dto = new PaymentBasketDTO(paymentType, createdDate, accountId, beneficiaryAccountNo,
//					amount, currency, cardNumber, beneficiaryAccountNo, bankCode, branchCode, provinceId, recipientName,
//					remarks, executionDate);
//			dto.setBasketId(i.toString());
//			dto.setAccountNo(accountNo);
			mockData.add(dto);

		}

		GetListOfBasketInputDTO input = new GetListOfBasketInputDTO();
		String realizationDateFrom = "2018-08-18T10:09:51.246Z";
		String realizationDateTo = "2018-09-17T10:09:51.246Z";
		input.setAmountFrom(0D);
		input.setAmountTo(100000000D);
		input.setCustomerId("801004");
		input.setRealizationDateFrom(realizationDateFrom);
		input.setRealizationDateTo(realizationDateTo);
		input.setStatuses(StatusesType.NEW.getStatusesTypeCode());

		String path = "/service/paymentBasket/getListofBasket";
		when(accountService.getListofBasket(anyString(), anyDouble(), anyDouble(), anyString(), anyString(),
				anyString(), anyString())).thenReturn(mockData);
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("amountFrom", "0")
						.param("amountTo", "120000000").param("customerId", "9998")
						.param("realizationDateFrom", realizationDateFrom).param("realizationDateTo", realizationDateTo)
						.param("statuses", StatusesType.NEW.getStatusesTypeCode()))
				.andExpect(status().isOk()).andReturn();
		System.out.println("***testgetTransactionFee : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testPostDeleteBasket() throws Exception {
		initLoginMock();
		String resultTest = "OK";
		when(accountService.deleteBasket(anyString(), anyString())).thenReturn(resultTest);
		String path = "/service/paymentBasket/deleteBasket";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("basketId", "1"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testPostDeleteBasket : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		String delCheck = (String) response.getData();
		assertEquals(resultTest, delCheck);
	}

	@Test
	public void testloadEVNparameter() throws Exception {
		initLoginMock();
		List<EVNParameter> lstResult = new ArrayList<>();
		EVNParameter dtoEVNParameter = new EVNParameter();
		String providerCode = "EVNHN";
		String providerName = "EVN Ha Noi";
		String[] prefix = { "PB1905", "PB0604", "PB1901", "PB1902", "PB1903", "PB1906", "PB1907", "PK0100", "PK0200",
				"PK0300", "PK0400", "PK0600", "PK0700", "PK0800", "PK0900", "PK1000", "PK1100", "PK9900", "PK0500",
				"PB1701", "PB1702", "PB1703", "PB1704", "PB1705", "PB1706", "PB1707", "PB1708", "PB1709", "PB1710",
				"PB1711", "PB2002", "PB2003", "PB2004", "PB2005", "PB2006", "PB2007", "PB2008", "PB2001", "PB0701",
				"PB0702", "PB0703", "PB0704", "PB0705", "PB0706", "PB0707", "PB0708", "PB0709", "PB0710", "PB0711",
				"PB0712", "PB1001", "PB1002", "PB1003", "PB1004", "PB1005", "PB1006", "PB1007", "PB1008", "PB1401",
				"PB1402", "PB1403", "PB1404", "PB1405", "PB1406", "PB1408", "PB1409", "PB1410", "PB1301", "PB1302",
				"PB1303", "PB1304", "PB1305", "PB1306", "PB1307", "PB1308", "PB1309", "PB1310", "PB1311", "PB1312",
				"PB0601", "PB0602", "PB0605", "PB0607", "PB0608", "PB0609", "PB0610", "PB0611", "PB0612", "PB0613",
				"PB0614", "PB0603", "PB0606", "PB1100", "PB1101", "PB1103", "PB1104", "PB1105", "PB1106", "PB1107",
				"PB1108", "PB1109", "PB1904", "PB1201", "PB1202", "PB1203", "PB1204", "PB1205", "PB1206", "PB1207",
				"PB1208", "PB1209", "PB1210" };
		dtoEVNParameter.setProviderCode(providerCode);
		dtoEVNParameter.setProviderName(providerName);
		dtoEVNParameter.setPrefix(prefix);
		lstResult.add(dtoEVNParameter);

		providerCode = "EVNMN";
		providerName = "EVN Mien Nam";
		String[] prefixs = { "PD01", "PD02", "PD03", "PD04", "PD05", "PD06", "PD07", "PD08", "PD09", "PD10", "PD11",
				"PD12", "PD13", "PD14", "PD15", "PD16", "PD17", "PD18", "PD19", "PD20", "PD21", "PD22", "PD23", "PD24",
				"PD25", "PD26", "PD27", "PD28", "PD29", "PD30" };
		dtoEVNParameter.setProviderCode(providerCode);
		dtoEVNParameter.setProviderName(providerName);
		dtoEVNParameter.setPrefix(prefixs);
		lstResult.add(dtoEVNParameter);

		when(accountService.loadEVNparameter(anyString())).thenReturn(lstResult);

		String path = "/service/paymentBasket/loadEVNparameter";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("***testloadEVNparameter : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testUpdateBasket() throws Exception {
		initLoginMock();
		PostAddBasketDTO mockData = new PostAddBasketDTO();
		mockData.setResultCode("123");
		mockData.setResultMsg("Mock Test");

		when(accountService.updateBasket(ArgumentMatchers.any(), anyString())).thenReturn(mockData);

		PostPaymentInputDTO input = new PostPaymentInputDTO();
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount(99999L);
		paymentInfo.setBillCode("NCLYD");
		String[] billCodeItemNoArr = { "009", "008" };
		paymentInfo.setBillCodeItemNo(billCodeItemNoArr);
		paymentInfo.setCardNumber("00917389463457832");
		paymentInfo.setCreditAccount("11111111");
		paymentInfo.setCreditAccountBankBranchCode("10000001");
		paymentInfo.setCreditAccountBankCode("999991");
		paymentInfo.setCreditAccountProvinceCode("0099");
		paymentInfo.setcRefNum("123456");
		paymentInfo.setCurrency("USA");
		paymentInfo.seteWalletPhoneNumber("84");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setMobilePhoneNumber("0973456782");

		List<PaymentItemStudentFeeDTO> paymentItemStudentFee = new ArrayList<>();

		PaymentItemStudentFeeDTO paymentItemStudentFeeDTO = new PaymentItemStudentFeeDTO();
		paymentItemStudentFeeDTO.setAmount(350D);
		paymentItemStudentFeeDTO.setDiscount(10D);
		paymentItemStudentFeeDTO.setDueDate(new Date());
		paymentItemStudentFeeDTO.setIsrequired(1);
		paymentItemStudentFeeDTO.setItemNameDescription("Ngo thua hao");
		paymentItemStudentFeeDTO.setItemTypeFeeType("");
		paymentItemStudentFeeDTO.setPaymentItemFeeCode("");
		paymentItemStudentFee.add(paymentItemStudentFeeDTO);

		paymentInfo.setPaymentItemStudentFee(paymentItemStudentFee);
		paymentInfo.setRecipient("");
		paymentInfo.setRemarks("Chuyen tien mung");
		paymentInfo.setServiceCode("OOOC");
		paymentInfo.setServiceProviderCode("PT");
		paymentInfo.setStudentCode("QCT");

		input.setPaymentInfo(paymentInfo);
		input.setPaymentType(PaymentType.FastTransfer.getPaymentTypeCode());

		String path = "/service/paymentBasket/updateBasket";
		System.out.println("***payload of testupdateBasket : " + objectMapper.writeValueAsString(input));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testGetPaymentsInFuture() throws Exception {
		initLoginMock();
		String[] accountIdArr = { "AccountId001", "AccountId002", "AccountId003", "AccountId004", "AccountId005",
				"AccountId006" };
		List<PaymentsInFutureDTO> lst = new ArrayList<>();
		for (int i = 0; i < accountIdArr.length; i++) {
			PaymentsInFutureDTO dto = new PaymentsInFutureDTO();
			dto.setAccountCurrency("VND");
			dto.setAccountId(accountIdArr[i]);
			dto.setAccountName("Hani");
			dto.setAccountNo("AccountNo001");
			dto.setAddToBasket(true);
			dto.setAmount(12000000D);
			List<Map<String, ?>> charges = new ArrayList<>();
			dto.setCharges(charges);
			dto.setCurrency("EUR");
			Map<String, ?> cyclicDefinition = new HashMap<>();
			dto.setCyclicDefinition(cyclicDefinition);
			dto.setCyclicDefinition(cyclicDefinition);
			dto.setDeliveryDate("1480125504");
			String[] description = { "Xin cho may ngan haha" };
			dto.setDescription(description);
			dto.seteWalletPhoneNumber("0198645237836");
			dto.setId("generateAccountId001");
			dto.setIsCyclic(true);
			dto.setLastRealizationDesc("lastRealizationDesc");
			dto.setOperationStatus("operationStatus");
			dto.setOriginalCustomerId("originalCustomerId");
			dto.setOriginalCustomerName("originalCustomerName");
			dto.setOriginalCustomerVisible(true);
			dto.setOwner("owner");
			PaymentDetails paymentDetails = new PaymentDetails();
			paymentDetails.setBankDetails("bankDetails");
			paymentDetails.setCreditAccountBankBranchCode("creditAccountBankBranchCode");
			paymentDetails.setCreditAccountBankCode("generateBranchCode");
			paymentDetails.setCreditAccountProvinceCode("PRV009");
			paymentDetails.setFee("fee");
			paymentDetails.setFeeAccount("NV0090983");
			dto.setPaymentDetails(paymentDetails);
			dto.setPaymentType(PaymentType.FastTransfer.getPaymentTypeCode());
			dto.setRealizationDate("1543197504");
			dto.setRealizationDateShiftReason("realizationDateShiftReason");
			dto.setRecipientAccountId("generateAccountId0001");
			dto.setRecipientAccountNo("000987334");
			dto.setRecipientAddress("123, Ngô Tất Tố, P6, Q1 ");
			String[] recipientName = { "Nguyễn thị mỸ Linh" };
			dto.setRecipientName(recipientName);
			dto.setRegistrationDate("1669427904");
			dto.setSaveTemplate(true);
			dto.setSenderAddress("564, Nam Kỳ Khởi Nghĩa");
			dto.setSenderName("Lý Thị Dần");
			dto.setStatus("status");
			dto.setTemplateId("templateId001");
			dto.setTemplateName("templateName001");
			dto.setTransactionId("transactionId01");
			dto.setTitle("title");
			dto.setTransactionTypeDesc("transactionTypeDesc");
			dto.setTransferType(PaymentType.InternalPayment.getPaymentTypeCode());
			lst.add(dto);
		}

		PaymentsInFutureInputDTO input = new PaymentsInFutureInputDTO();
		Date realizationDateFrom = new Date(1453185446L);
		Date realizationDateTo = new Date(1545201446L);
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setRealizationDateFrom(realizationDateFrom);
		input.setRealizationDateTo(realizationDateTo);
		input.setStatusPaymentCriteria(StatusesType.NEW.getStatusesTypeCode());

		when(accountService.getPaymentsInFuture(ArgumentMatchers.any(), anyString())).thenReturn(lst);

		String path = "/service/paymentBasket/getPaymentsInFuture";
		System.out.println("***payload of testGetPaymentsInFuture " + objectMapper.writeValueAsString(input));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();
		System.out.println("***testGetPaymentsInFuture : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}
}
