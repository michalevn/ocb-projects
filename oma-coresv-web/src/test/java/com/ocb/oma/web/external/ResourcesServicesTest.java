package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.resources.model.OmniGiftCardMsgTemplate;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.TokenManagementService;

/**
 * Dung de integration test voi account service rest Can phai start account
 * service test that su them
 * 
 * @author phuhoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ResourcesServicesTest extends BaseTest {

	@Autowired
	TokenManagementService tokenManagementService;

	@Test
	public void testGetBankList() throws Exception {
		String path = "/service/resources/getBankList";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetBankList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<BankInfoDTO> lst = (List<BankInfoDTO>) response.getData();
		assertNotNull(lst);
	}

	@Test
	public void testGetProvinceList() throws Exception {
		String path = "/service/resources/listOfValue?lovType=PROVINCE";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("***testGetProvinceList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<ListOfValueDTO> lst = (List<ListOfValueDTO>) response.getData();
		assertNotNull(lst);
	}

	@Test
	public void testsearchBankCitad() throws Exception {
		String path = "/service/resources/searchBankCitad?provinceCode=01&bankCode=Bank32";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testsearchBankCitad : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<BankInfoDTO> lst = (List<BankInfoDTO>) response.getData();
		assertNotNull(lst);
	}

	@Test
	public void testfindGiftCardMsgTemplate() throws Exception {
		String path = "/service/resources/findGiftCardMsgTemplate?cardType=GT_BIRTHDAY";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testfindGiftCardMsgTemplate : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<OmniGiftCardMsgTemplate> lst = (List<OmniGiftCardMsgTemplate>) response.getData();
		assertNotNull(lst);
		System.out.println("result of OmniGiftCardMsgTemplate " + objectMapper.writeValueAsString(response));
	}
}
