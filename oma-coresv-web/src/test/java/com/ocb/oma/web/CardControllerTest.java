package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.CardDetailByCardIdDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.MerchantAddressDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ChangeLimitDirectlyInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PostAuthActivateCardInput;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.WebCardService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class CardControllerTest extends BaseMockTest {

	@Test
	public void test_RestrictCard() throws Exception {

		initLoginMock();
		String cardId = "1";
		OutputBaseDTO dto = new OutputBaseDTO();
		if (cardId.equals("301")) {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		} else if (cardId.equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		}

		when(accountService.restrictCard(anyString(), anyString(), anyString())).thenReturn(dto);
		String path = "/service/card/restrictCard";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.param("cardId", "1").param("cardRestrictionReason", "LOST")).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of testRestrictCard : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void test_GetFindCardBlockades() throws Exception {

		initLoginMock();
		List<CardBlockade> lstRes = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			CardBlockade blockade = new CardBlockade();
			blockade.setCardUser("cardUser00" + (i + 1));
			blockade.setCurrency("VND");
			MerchantAddressDTO merchantAddress = new MerchantAddressDTO();
			merchantAddress.setCountry("Korea");
			merchantAddress.setPostCode("NEW0098");
			merchantAddress.setStreet("BuSan09");
			merchantAddress.setTown("town0" + i + 1);

			blockade.setMerchantAddress(merchantAddress);
			blockade.setMerchantName("Credit limit for Liability Contract");
			blockade.setOperationDate(new Date(1559008354));
			blockade.setRejectionReason("rejectionReason00" + i + 1);
			blockade.setTransactionAmount(1500000D * i);
			blockade.setTransactionDesc("Credit limit for Liability Contract");
			blockade.setTransactionType("transactionType0" + i);

			lstRes.add(blockade);
		}

		when(accountService.findCardBlockades(anyString(), ArgumentMatchers.any())).thenReturn(lstRes);
		String path = "/service/card/findCardBlockades";
		CardBlockadeInputDTO input = new CardBlockadeInputDTO();
		input.setId("302");
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductType("CARD");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardBlockade> realData = (List<CardBlockade>) response.getData();
		System.out.println("payload of testfindCardBlockades : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void test_PostpaymentCardAnother() throws Exception {

		// OTP

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		///
		initLoginMock();
		PaymentCardAnotherOuputDTO mockdata = new PaymentCardAnotherOuputDTO();
		mockdata.setAuthErrorCause("authErrorCause");
		mockdata.setCoreRefNum2("coreRefNum2");
		mockdata.setErrorMsg("errorMsg");
		mockdata.setReferenceId("referenceId");
		mockdata.setStatus("posted");
		mockdata.setSuccess(true);
		PaymentCardAnotherInputDTO input = new PaymentCardAnotherInputDTO();
		input.setAccountId("381");
		input.setAccountName("TGTT Ca Nhan");
		input.setAmount(10000D);
		input.setCardNumber("1098765432");
		input.setCreditCardAccount("1234567890");
		input.setCreditCardAccountName("Nguyen van an");
		input.setRemark("remark");
		when(accountService.postpaymentCardAnother(anyString(), ArgumentMatchers.any())).thenReturn(mockdata);
		String path = "/service/card/postpaymentCardAnother";
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue1);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		PaymentCardAnotherOuputDTO realData = objectMapper.convertValue(response.getData(),
				PaymentCardAnotherOuputDTO.class);
		System.out.println("payload of testPostpaymentCardAnother : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void test_PostpaymentCardAnother_OTP_ERROR() throws Exception {

		// OTP

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		///
		initLoginMock();
		PaymentCardAnotherOuputDTO mockdata = new PaymentCardAnotherOuputDTO();
		mockdata.setAuthErrorCause("authErrorCause");
		mockdata.setCoreRefNum2("coreRefNum2");
		mockdata.setErrorMsg("errorMsg");
		mockdata.setReferenceId("referenceId");
		mockdata.setStatus("posted");
		mockdata.setSuccess(true);
		PaymentCardAnotherInputDTO input = new PaymentCardAnotherInputDTO();
		input.setAccountId("381");
		input.setAccountName("TGTT Ca Nhan");
		input.setAmount(10000D);
		input.setCardNumber("1098765432");
		input.setCreditCardAccount("1234567890");
		input.setCreditCardAccountName("Nguyen van an");
		input.setRemark("remark");
		when(accountService.postpaymentCardAnother(anyString(), ArgumentMatchers.any())).thenReturn(mockdata);
		String path = "/service/card/postpaymentCardAnother";
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue2);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is5xxServerError()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		System.out.println("payload of testPostpaymentCardAnother : " + objectMapper.writeValueAsString(input));
		System.out.println("result of testPostpaymentCardAnother : " + objectMapper.writeValueAsString(response));
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
	}

	@Test
	public void test_FindCardByCardId() throws Exception {

		initLoginMock();

		CardDetailByCardIdDTO cardDetailDTO = new CardDetailByCardIdDTO();
		cardDetailDTO.setId("301");
		cardDetailDTO.setName(null);
		cardDetailDTO.setCardNo("5209 **** **** 6675");
		cardDetailDTO.setAvailableFunds(30000000D);
		cardDetailDTO.setAccountId("383");
		cardDetailDTO.setAccountNo("601688010030001");
		cardDetailDTO.setCardOwnerName(null);
		cardDetailDTO.setCardOwnerLastName(null);
		cardDetailDTO.setStatus(null);
		cardDetailDTO.setCardType("CREDIT");
		cardDetailDTO.setCardIDType("CCMEM00501");
		cardDetailDTO.setCardSubType("MAIN");
		cardDetailDTO.setCurrency(null);
		cardDetailDTO.setBlockedFunds(null);
		cardDetailDTO.setDateExpirationEnd(new Date(1709139600000L));
		cardDetailDTO.setSettlmntDate(null);
		cardDetailDTO.setLimitLeft(null);

		when(accountService.findCardByCardId(anyString(), anyString())).thenReturn(cardDetailDTO);
		String path = "/service/card/findCardByCardId";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("card_id", "301"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		CardDetailByCardIdDTO realData = objectMapper.convertValue(response.getData(), CardDetailByCardIdDTO.class);
		System.out.println("payload of testFindCardByCardId : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void test_CardAutoRepayment() throws Exception {

		initLoginMock();
		List<CardAutoRepaymentDTO> mockdata = new ArrayList<>();
		CardAutoRepaymentDTO dto = new CardAutoRepaymentDTO();
		dto.setAccountId("001");
		dto.setAccountName("TGTT Ca Nhan");
		dto.setAccountNo("00001");
		dto.setCardAccountId("383");
		dto.setCardAccountName(null);
		dto.setCardAccountNo(null);
		dto.setCardId("302");
		dto.setCardNo("5209 **** **** 6675");
		dto.setCardRepaymentType("CARDREPAYMENTTYPE");
		dto.setId("1");
		mockdata.add(dto);
		CardAutoRepaymentDTO dto1 = new CardAutoRepaymentDTO();
		dto1.setAccountId("002");
		dto1.setAccountName("TGTT Ca Nhan");
		dto1.setAccountNo("00002");
		dto1.setCardAccountId("385");
		dto1.setCardAccountName(null);
		dto1.setCardAccountNo(null);
		dto1.setCardId("301");
		dto1.setCardNo("5209 **** **** 6098");
		dto1.setCardRepaymentType("CARDREPAYMENTTYPE_1");
		dto1.setId("2");
		mockdata.add(dto1);
		when(accountService.cardAutoRepayment(anyString())).thenReturn(mockdata);
		String path = "/service/card/cardAutoRepayment";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardAutoRepaymentDTO> realData = (List<CardAutoRepaymentDTO>) response.getData();

		System.out.println("payload of testCardAutoRepayment : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	public void testCardRejected() throws Exception {

		initLoginMock();
		CardRejectedInputDTO cardRejectedInput = new CardRejectedInputDTO();
		cardRejectedInput.setOperationType("all");
		cardRejectedInput.setPageNumber(1);
		cardRejectedInput.setPageSize(10);
		cardRejectedInput.setProductId("302");
		cardRejectedInput.setProductType("CARD");
		cardRejectedInput.setDateFrom(new Date(1544071598L));
		cardRejectedInput.setDateTo(new Date(1607229998L));

		List<CardRejectedDTO> lst = new ArrayList<>();
		List<String> transactionTypes = Arrays.asList("RutTienMat", "ChuyenKhoanTrong", "ChuyenKhoanNgoai",
				"ChuyenKhoanNoiBo", "ChuyenKhoan24/7");
		for (int i = 0; i < transactionTypes.size(); i++) {

			CardRejectedDTO dto = new CardRejectedDTO();
			dto.setOperationDate(new Date(1607229998000L));
			dto.setTransactionType(transactionTypes.get(i));
			dto.setTransactionAmount(300000D * i);
			dto.setCurrency("VND");
			MerchantAddressDTO merchantAddress = new MerchantAddressDTO();
			merchantAddress.setCountry("VietNam");
			merchantAddress.setPostCode("P0000" + i + 1);
			merchantAddress.setStreet("street " + i + 9);
			merchantAddress.setTown("town " + i + 1);
			dto.setMerchantAddress(merchantAddress);
			dto.setTransactionDesc("transactionDesc" + i + 1);
			dto.setRejectionReason("rejectionReason" + i + 1);
			dto.setCardUser("cardUser" + i + 1);
			lst.add(dto);
		}
		when(accountService.cardRejected(anyString(), ArgumentMatchers.any())).thenReturn(lst);
		String path = "/service/card/cardRejected";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(cardRejectedInput)))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardRejectedDTO> realData = (List<CardRejectedDTO>) response.getData();

		System.out.println("payload of testCardRejected : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void testpostAuthActivateCard() throws Exception {

		initLoginMock();
		String cardId = "1";
		OutputBaseDTO dto = new OutputBaseDTO();
		if (cardId.equals("301")) {
			dto.setResultCode("1");
			dto.setResultMsg(MessageConstant.OK_MESSAGE);
		} else if (cardId.equals("1")) {
			dto.setResultCode("1000");
			dto.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			dto.setResultCode("0");
			dto.setResultMsg(MessageConstant.SERVER_ERROR);
		}

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("12345678");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		when(accountService.postAuthActivateCard(anyString(), anyString())).thenReturn(dto);

		String path = "/service/card/postAuthActivateCard";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.param("cardId", "301");

		VerifyOtpToken otpValue = new VerifyOtpToken();
		otpValue.setAuthId("12345");
		otpValue.setOtpValue("12345678");

		PostAuthActivateCardInput input = new PostAuthActivateCardInput();
		input.setOtpValue(otpValue);
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());

		requestBuilder.content(objectMapper.writeValueAsBytes(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of postAuthActivateCard : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void testCardStatements() throws Exception {

		initLoginMock();
		List<CardStatementsDTO> lst = new ArrayList<>();
		CardStatementsDTO dto = new CardStatementsDTO();
		dto.setId("20180615601688010030001");
		dto.setTotalRepaymentAmount(253320D);
		dto.setMinimumRepaymentAmount(200000D);
		dto.setCycleStart(new Date(1526403600000L));
		dto.setCycleEnd(new Date(1528995600000L));
		dto.setNextPaymentDate(new Date(1531242000000L));
		dto.setOverDueAmount(0D);
		dto.setCardAccountNumber("601688010030001");
		dto.setDownloadLink("http://10.96.11.11:8083/saoke/201806/20180615601688010030001.pdf");
		lst.add(dto);

		CardStatementsDTO dto1 = new CardStatementsDTO();
		dto1.setId("20180615601688010030001");
		dto1.setTotalRepaymentAmount(358515D);
		dto1.setMinimumRepaymentAmount(300000D);
		dto1.setCycleStart(new Date(1529082000000L));
		dto1.setCycleEnd(new Date(1531587600000L));
		dto1.setNextPaymentDate(new Date(1533834000000L));
		dto1.setOverDueAmount(0D);
		dto1.setCardAccountNumber("601688010030001");
		dto1.setDownloadLink("http://10.96.11.11:8083/saoke/201807/20180715601688010030001.pdf");
		lst.add(dto1);

		CardStatementsDTO dto2 = new CardStatementsDTO();
		dto2.setId("20180615601688010030001");
		dto2.setTotalRepaymentAmount(575481D);
		dto2.setMinimumRepaymentAmount(880481D);
		dto2.setCycleStart(new Date(1534352400000L));
		dto2.setCycleEnd(new Date(1536944400000L));
		dto2.setNextPaymentDate(new Date(1539190800000L));
		dto2.setOverDueAmount(0D);
		dto2.setCardAccountNumber("601688010030001");
		dto2.setDownloadLink("http://10.96.11.11:8083/saoke/201806/20180615601688010030001.pdf");
		lst.add(dto2);

		CardStatementsDTO dto3 = new CardStatementsDTO();
		dto3.setId("20180615601688010030001");
		dto3.setTotalRepaymentAmount(465979D);
		dto3.setMinimumRepaymentAmount(500000D);
		dto3.setCycleStart(new Date(1531674000000L));
		dto3.setCycleEnd(new Date(1534266000000L));
		dto3.setNextPaymentDate(new Date(1536512400000L));
		dto3.setOverDueAmount(0D);
		dto3.setCardAccountNumber("601688010030001");
		dto3.setDownloadLink("http://10.96.11.11:8083/saoke/201807/20180715601688010030001.pdf");
		lst.add(dto3);

		CardStatementsInputDTO input = new CardStatementsInputDTO();
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductId("302_CARD");

		when(accountService.cardStatements(anyString(), ArgumentMatchers.any())).thenReturn(lst);
		String path = "/service/card/cardStatements";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardStatementsDTO> realData = (List<CardStatementsDTO>) response.getData();
		System.out.println("payload of cardStatements : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void test_changeLimitDirectly() throws Exception {

		initLoginMock();

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		List<ChangeLimitDTO> limits = new ArrayList<>();
		ChangeLimitDTO dto = new ChangeLimitDTO();
		dto.setChannelType("WWW");
		dto.setValue(BigDecimal.valueOf(RandomUtils.nextDouble()));
		limits.add(dto);

		ChangeLimitDirectlyInput input = new ChangeLimitDirectlyInput();
		input.setLimits(limits);
		input.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		input.setOtpValue(otpValue1);
		when(accountService.changeLimitDirectly(anyString(), ArgumentMatchers.anyList())).thenReturn(true);

		String path = "/service/card/changeLimitDirectly";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsString(input));

		System.out.println("***payload of test_changeLimitDirectly " + objectMapper.writeValueAsString(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		System.out.println("result of test_changeLimitDirectly " + objectMapper.writeValueAsString(response));
		assertEquals(response.isSuccess(), true);
		assertNotNull(response.getData());
		assertEquals(response.getData() instanceof Boolean, true);
		assertEquals(response.getData(), Boolean.TRUE);

	}

	@Test
	public void test_changeLimitDirectly_NoOTP() throws Exception {

		initLoginMock();

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		List<ChangeLimitDTO> limits = new ArrayList<>();
		ChangeLimitDTO dto = new ChangeLimitDTO();
		dto.setChannelType("WWW");
		dto.setValue(BigDecimal.valueOf(RandomUtils.nextDouble()));
		limits.add(dto);

		ChangeLimitDirectlyInput input = new ChangeLimitDirectlyInput();
		input.setLimits(limits);
		input.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		// input.setOtpValue(otpValue1);
		when(accountService.changeLimitDirectly(anyString(), ArgumentMatchers.anyList())).thenReturn(true);

		String path = "/service/card/changeLimitDirectly";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsString(input));

		System.out.println("***payload of test_changeLimitDirectly " + objectMapper.writeValueAsString(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is5xxServerError()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		System.out.println("result of test_changeLimitDirectly " + objectMapper.writeValueAsString(response));
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());

	}

	@Test
	public void test_changeLimitDirectly_WrongOTP() throws Exception {

		initLoginMock();

		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		List<ChangeLimitDTO> limits = new ArrayList<>();
		ChangeLimitDTO dto = new ChangeLimitDTO();
		dto.setChannelType("WWW");
		dto.setValue(BigDecimal.valueOf(RandomUtils.nextDouble()));
		limits.add(dto);

		ChangeLimitDirectlyInput input = new ChangeLimitDirectlyInput();
		input.setLimits(limits);
		input.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		input.setOtpValue(otpValue2);
		when(accountService.changeLimitDirectly(anyString(), ArgumentMatchers.anyList())).thenReturn(true);

		String path = "/service/card/changeLimitDirectly";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsString(input));

		System.out.println("***payload of test_changeLimitDirectly " + objectMapper.writeValueAsString(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is5xxServerError()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		System.out.println("result of test_changeLimitDirectly " + objectMapper.writeValueAsString(response));
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());

	}
}
