/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author Phu Hoang
 *
 */
public class BaseMockTest extends BaseAccountMockTest {

	@MockBean
	@Autowired
	protected ResourceServiceExt resourceService;

}
