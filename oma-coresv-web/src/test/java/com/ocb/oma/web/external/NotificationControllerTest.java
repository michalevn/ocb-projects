package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class NotificationControllerTest extends BaseTest {

	@Autowired
	protected AccountServiceExt accountServiceExt;

//	@Test
	public void testPushNotification() throws Exception {

		String userId = "phuhns";
		OmniNotification omniNotification = new OmniNotification();
		omniNotification.setTitleVn("Xin chao");
		omniNotification.setTitleEl("Hello");
		omniNotification.setBodyVn("Chúc một ngày mới tốt đẹp!");
		omniNotification.setBodyEl("Have a nice day!");
		omniNotification.setDistributedType(3);
		omniNotification.setType(OmniNotification.TYPE_THONG_BAO.toString());
		omniNotification.setUserId(userId);

		String path = "/service/notifications/push";
		System.out.println("===> omniNotification: " + objectMapper.writeValueAsString(omniNotification));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(omniNotification)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("===> testPushNotification: " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());

	}

	@Test
	public void testFindNotificationsType() throws Exception {

		String type = OmniNotification.TYPE_THONG_BAO.toString();

		String path = "/service/notifications/findByType";
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(path).queryParam("type", type);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(uriBuilder.toUriString());
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("===> testFindNotifications: " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	// @Test
	public void testGet() throws Exception {

		Long notificationId = 1L;

		String path = "/service/notifications/get";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(path).queryParam("notificationId",
				notificationId);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(uriBuilder.toUriString());
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("===> testGet: " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void test_countUnread() throws Exception {

		String path = "/service/notifications/countUnread/10";

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(path);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(uriBuilder.toUriString());
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.out.println("===> test_countUnread: " + responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}
}
