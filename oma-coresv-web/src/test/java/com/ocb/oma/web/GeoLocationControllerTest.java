package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class GeoLocationControllerTest extends BaseMockTest {

	@Test
	public void test_find() throws Exception {

		initLoginMock();

		try {

			GeoLocationPoiDTO geoLocationPoi = new GeoLocationPoiDTO();

			when(accountService.findGeoLocationPois(anyString(), anyDouble(), anyDouble(), anyString()))
					.thenReturn(geoLocationPoi);

			String path = "/service/geo-locations/find";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			requestBuilder.param("lat", "10.768546");
			requestBuilder.param("lng", "106.700959");
			requestBuilder.param("poiType", "BRANCH");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
