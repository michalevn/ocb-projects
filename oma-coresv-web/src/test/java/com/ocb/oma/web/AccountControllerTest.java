package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AvatarInfoDTO;
import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.ExecutedStatus;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.StatusesAvatar;
import com.ocb.oma.dto.StatusesUpdateAvatar;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.TransactionType;
import com.ocb.oma.dto.TransferAccountDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositSubDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.ChangePasswordInputDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.PostBasketsInput;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.ChangeUsernameOutput;
import com.ocb.oma.oomni.dto.AvatarInfoOutputDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.PostPaymentOutputDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.web.controller.WebAccountController;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.event.OmniEvent;
import com.ocb.oma.web.service.internal.TokenManagementService;
import com.ocb.oma.web.service.internal.WebAccountServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class AccountControllerTest extends BaseMockTest {

	@Autowired
	WebAccountServiceImpl webaccountService;

	WebAccountController controller;

	@Autowired
	TokenManagementService tokenManagementService;

	@Test
	public void testLoginOk() throws Exception {

	}

	// @Test
	public void testGetAccount() throws Exception {
		initLoginMock();
		String name = "chaobannhe";
		// accountService = mock(classToMock)
		when(accountService.doTest(name)).thenReturn("hello " + name);
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		String path = "/service/account/test?name=" + name;
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetAccount : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		// assertEquals("hello " + name, response.getData());
	}

	// @Test
	public void testGetAccountWhenServiceDown() throws Exception {
		initLoginMock();
		String name = "chaobannhe";
		// accountService = mock(classToMock)
		when(accountService.doTest(anyString())).thenThrow(new IllegalStateException());
		String path = "/service/account/test?name=" + name;
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is5xxServerError()).andReturn();
		System.out.println("testGetAccountWhenServiceDown : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.SERVICE_UNAVAILABLE, response.getErrorCode());

	}

	// @Test
	public void testGetAccountWhenServiceSomethingWrong() throws Exception {
		initLoginMock();
		String name = "chaobannhe";
		// accountService = mock(classToMock)
		when(accountService.doTest(anyString())).thenThrow(new RuntimeException());
		String path = "/service/account/test?name=" + name;
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is5xxServerError()).andReturn();
		System.out.println("testGetAccountWhenServiceSomethingWrong : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.SERVER_ERROR, response.getErrorCode());

	}

	// @Test
	// public void testGetAccessTokenAfterLogin() throws Exception {
	// initLoginMock();
	// String tokenMock = "!@#$%^&*&^%$%";
	// // accountService = mock(classToMock)
	// when(accountService.getEncryptToken(this.defaultUser,
	// defaultDeviceId)).thenReturn(tokenMock);
	// String path = "/service/account/getEncryptToken";
	// MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
	// MvcResult result =
	// mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
	// System.out.println("testGetAccessTokenAfterLogin : " +
	// result.getResponse().getContentAsString());
	// ResponseDTO response =
	// objectMapper.readValue(result.getResponse().getContentAsString(),
	// ResponseDTO.class);
	//
	// assertEquals(true, response.isSuccess());
	// assertEquals(tokenMock, response.getData());
	// // assertEquals(MessageConstant.SERVER_ERROR, response.getErrorCode());
	//
	// }

	private void initTestGetAccountList() {
		List<AccountInfo> lst = new ArrayList<>();
		AccountInfo acc1 = new AccountInfo();
		acc1.setCurrency("usd");
		acc1.setCurrentBalance(85000000D);
		AccountInfo acc2 = new AccountInfo();
		acc2.setCurrency("vnd");
		acc2.setCurrentBalance(3000D);
		lst.add(acc1);
		lst.add(acc2);

		initLoginMock();
		when(accountService.getAccountList(anyString(), anyString())).thenReturn(lst);
	}

	@Test
	public void testupdateAccountName() throws Exception {
		initLoginMock();
		when(accountService.updateAccountName(anyString(), anyString(), anyString(), anyString())).thenReturn(true);
		String path = "/service/account/updateAccountName";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.param("accountName", "Nguyen Van A").param("accountNo", "123456789"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testupdateAccountName : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData());
	}

	@Test
	public void testGetAccountList() throws Exception {
		initTestGetAccountList();

		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		String path = "/service/account/getAccountList";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetAccountList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		List<AccountInfo> lst = (List<AccountInfo>) response.getData();
		assertEquals(2, lst.size());
	}

	private List<TransactionHistoryDTO> initTestfindTransactionHistory(TransHistoryRequestDTO input) {
		List<TransactionHistoryDTO> result = new ArrayList<>();
		List<String> transactionTypes = Arrays.asList("RutTienMat", "ChuyenKhoanTrong", "ChuyenKhoanNgoai",
				"ChuyenKhoanNoiBo", "ChuyenKhoan24/7");
		List<String> currencies = Arrays.asList("usd", "vnd", "hk");
		List<String> transactionTypeDescList = Arrays.asList("Rút tiền mặt ", "Chuyển khoản trong hệ thống ",
				"Chuyển khoản ngoài hệ thống ", "Chuyển khoản nội bộ ", "Chuyển khoản 24/7");
		for (int i = 0; i < input.getPageSize(); i++) {
			double amount = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
			int transIndex = ThreadLocalRandom.current().nextInt(transactionTypes.size() - 1);
			int currencyIndex = ThreadLocalRandom.current().nextInt(2);
			String transactionId = RandomStringUtils.randomAlphabetic(10);
			String transactionNote = RandomStringUtils.randomAlphabetic(10);
			Date transactionDate = new Date(ThreadLocalRandom.current().nextLong(input.getFromDate().getTime() - 1000,
					input.getToDate().getTime()));
			String receiverName = RandomStringUtils.randomAlphabetic(10);
			String receiverAccount = RandomStringUtils.randomNumeric(10);
			TransactionHistoryDTO dto = new TransactionHistoryDTO(input.getAccountNo(), amount,
					currencies.get(currencyIndex), transactionId, transactionTypes.get(transIndex), transactionNote,
					transactionTypeDescList.get(transIndex), transactionDate, receiverName, receiverAccount);
			result.add(dto);
		}
		return result;
	}

	@Test
	public void testfindTransactionHistory() throws Exception {

		initLoginMock();

		TransHistoryRequestDTO data = new TransHistoryRequestDTO("account12345", "ChuyenKhoanTrong", new Date(),
				new Date(), 1000000D, 5000000D, 1, 10);

		List<TransactionHistoryDTO> mockData = initTestfindTransactionHistory(data);
		when(accountService.findTransactionHistory(ArgumentMatchers.any(), anyString())).thenReturn(mockData);
		String path = "/service/account/findTransactionHistory";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(data))).andExpect(status().isOk()).andReturn();

		System.out.println("payload " + objectMapper.writeValueAsString(data));

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<TransactionHistoryDTO> realData = (List<TransactionHistoryDTO>) response.getData();
		System.out.println("payload of testfindTransactionHistory : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
		assertEquals(mockData.size(), realData.size());
	}

	private List<TransactionType> initTestGetTransactionType() {
		// TODO Auto-generated method stub
		List<String> transactionTypes = Arrays.asList("RutTienMat", "ChuyenKhoanTrong", "ChuyenKhoanNgoai",
				"ChuyenKhoanNoiBo", "ChuyenKhoan24/7");
		List<String> transactionTypeDescList = Arrays.asList("Rút tiền mặt ", "Chuyển khoản trong hệ thống ",
				"Chuyển khoản ngoài hệ thống ", "Chuyển khoản nội bộ ", "Chuyển khoản 24/7");

		List<TransactionType> lst = new ArrayList<>();
		for (int i = 0; i < transactionTypeDescList.size(); i++) {
			TransactionType type = new TransactionType(transactionTypes.get(i), transactionTypeDescList.get(i));
			lst.add(type);
		}
		return lst;
	}

	private AccountSummaryDTO initTestLoadAccountSummary() {

		List<String> currencies = Arrays.asList("usd", "vnd", "hk", "cad", "eu");

		double lastIncome = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
		double lastOutcome = ThreadLocalRandom.current().nextDouble(1000000D, 99000000D);
		Date lastIncomeDate = new Date(2018, 5, 10);
		Date lastOutcomeDate = new Date(2018, 4, 10);

		int currencyIndex = ThreadLocalRandom.current().nextInt(3);
		String accountNo = "0123123";

		AccountSummaryDTO dto = new AccountSummaryDTO(accountNo, currencies.get(currencyIndex),
				currencies.get(currencyIndex), lastIncome, lastIncomeDate, lastOutcomeDate, lastOutcome);
		return dto;

	}

	@Test
	public void testloadAccountSummarByCurrencies() throws Exception {

		initLoginMock();

		AccountSummaryDTO mockData = initTestLoadAccountSummary();
		when(accountService.loadAccountSummarByCurrencies(anyString(), anyInt(), anyString())).thenReturn(mockData);
		String path = "/service/account/loadAccountSummarByCurrencies?accountId=41&summaryPeriod=180";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AccountSummaryDTO realData = objectMapper.convertValue(response.getData(), AccountSummaryDTO.class);
		System.out
				.println("payload of testloadAccountSummarByCurrencies : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetAccountLimit() throws Exception {

		initLoginMock();

		AccountLimitDTO mockData = new AccountLimitDTO(1000000D, 1000000000D, 1000000000D);
		;
		when(accountService.getAccountLimnit(anyString(), anyString())).thenReturn(mockData);
		String path = "/service/account/accountLimit?paymentType=WrongValue";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is5xxServerError()).andReturn();
		System.out.println("***********Test getAccountLimit wrong value " + result.getResponse().getContentAsString());
		// test with right param
		path = "/service/account/accountLimit?paymentType=" + PaymentType.InternalPayment.getPaymentTypeCode();
		requestBuilder = processPostAuth(path);
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AccountLimitDTO realData = objectMapper.convertValue(response.getData(), AccountLimitDTO.class);
		System.out.println("payload of testGetAccountLimit : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
		assertEquals(mockData.getMaximumAmount(), realData.getMaximumAmount());
	}

	@Test
	public void testgetAccountNameByAccountNo() throws Exception {

		initLoginMock();

		String mockData = "Nguyen Van a";
		;
		when(accountService.getAccountNameByAccountNo(anyString(), anyString())).thenReturn(mockData);
		String path = "/service/account/getAccountNameByAccountNo?accountNo=0101982812";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String realData = objectMapper.convertValue(response.getData(), String.class);
		System.out
				.println("****payload of testgetAccountNameByAccountNo : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
		assertEquals(mockData, realData);
	}

	@Test
	public void testgetUpcomingRepaymentDeposit() throws Exception {

		initLoginMock();

		UpcomingRepaymentDepositDTO dto = new UpcomingRepaymentDepositDTO();
		UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo = new UpcomingRepaymentDepositSubDTO(new Date(),
				100000D, "", "vnd", true);
		UpcomingRepaymentDepositSubDTO loanRepaymentInfo = new UpcomingRepaymentDepositSubDTO(new Date(), 500000D, "",
				"vnd", true);
		UpcomingRepaymentDepositSubDTO creditCardInfo = new UpcomingRepaymentDepositSubDTO(new Date(), 100000D, "",
				"vnd", true);
		dto.setMinMaturityDateDepositInfo(minMaturityDateDepositInfo);
		dto.setLoanRepaymentInfo(loanRepaymentInfo);
		dto.setCreditCardWithoutStatement(true);
		dto.setCreditCardInfo(creditCardInfo);
		when(accountService.getUpcomingRepaymentDeposit(anyString())).thenReturn(dto);
		String path = "/service/account/getUpcomingRepaymentDeposit";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		UpcomingRepaymentDepositDTO realData = objectMapper.convertValue(response.getData(),
				UpcomingRepaymentDepositDTO.class);
		// System.out.println("***");
		System.out.println("****result of getUpcomingRepaymentDeposit : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
		assertEquals(dto.getCreditCardInfo().getAmount(), realData.getCreditCardInfo().getAmount());
	}

	@Test
	public void testGetBlockedTransaction() throws Exception {

		initLoginMock();
		List<TransactionBlockedDTO> data = new ArrayList<>();
		int pageSize = 10;
		String accountNo = "1234";
		for (int i = 0; i < pageSize; i++) {
			TransactionBlockedDTO dto = new TransactionBlockedDTO();
			dto.setAccountId("accountId");
			dto.setAccountNo(accountNo);
			dto.setBlockedAmount((double) (i + 234));
			dto.setBlockedBy("ABC");
			dto.setCreatedDate(new Date());
			dto.setReason("Reason");
			dto.setTransactionRef("transactionRef");
			data.add(dto);
		}
		when(accountService.getBlockedTransaction(anyString(), anyInt(), anyInt(), anyString())).thenReturn(data);
		String path = "/service/account/getBlockedTransaction?accountId=123&currentPage=3&pageSize=10";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<TransactionBlockedDTO> realData = (List<TransactionBlockedDTO>) response.getData();
		System.out.println("payload of getBlockedTransaction : " + objectMapper.writeValueAsString(response));
		System.out.println("payload of getBlockedTransaction : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
		// assertEquals(data.size(), realData.size());
	}

	@Test
	public void testgetAccountNameNapas() throws Exception {
		initLoginMock();
		String mockData = "ABC";
		when(accountService.getAccountNameNapas(anyString(), anyString(), anyString(), anyString(), anyString()))
				.thenReturn(mockData);
		String path = "/service/account/getAccountNameNapas?accountNo=0101982812&bankCode=ABC&cardNumber=&debitAccount=";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String realData = objectMapper.convertValue(response.getData(), String.class);
		System.out.println("****result of testgetAccountNameNapas : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
		assertEquals(mockData, realData);
	}

	@Test
	public void testGetAvatarInfo() throws Exception {
		initLoginMock();
		AvatarInfoOutputDTO dto = new AvatarInfoOutputDTO();
		AvatarInfoDTO avatarInfo = new AvatarInfoDTO();
		avatarInfo.setData("123456");
		avatarInfo.setFileName("avatarName123456");
		dto.setAvatarInfo(avatarInfo);
		dto.setMessage("OK");
		dto.setStatus(StatusesAvatar.INTERNAL_ERROR.getStatusesAvatarCode());

		when(accountService.getAvatarInfo(anyString())).thenReturn(dto);
		String path = "/service/account/getAvatarInfo";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AvatarInfoOutputDTO realData = objectMapper.convertValue(response.getData(), AvatarInfoOutputDTO.class);
		// System.out.println("***");
		System.out.println("****result of getAvatarInfo : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);

	}

	private PostPaymentOutputDTO initTestPostPayment() {
		PostPaymentOutputDTO output = new PostPaymentOutputDTO();

		output.setStatus(ExecutedStatus.EXECUTED.getCode());
		output.setSuccess(true);
		output.setReferenceId("GW182961415153625.01");
		output.setCoreRefNum2("FT171645F7M3");
		return output;
	}

	@Test
	public void testSendGiftCard() throws Exception {
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		GiftCardInsertDTO giftCardInsertDTO = new GiftCardInsertDTO();
		OmniGiftCard giftCard = new OmniGiftCard();
		giftCard.setDebitAccountId("0100100003486003");
		giftCard.setCurrency("VND");
		giftCard.setShortMessage("Have a nice day");
		giftCard.setFullMessage("Have a nice day");
		giftCardInsertDTO.setGiftCard(giftCard);
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = new ArrayList<>();
		String[] arrSetBeneficiary = { "0037100006151005", "0300958900300", "030095890999" };
		for (int i = 0; i < arrSetBeneficiary.length; i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = new OmniGiftCardReceiver();
			omniGiftCardReceiver.setBeneficiary(arrSetBeneficiary[i]);
			omniGiftCardReceiver.setRecipient("Nguyễn Văn A");
			omniGiftCardReceiver.setAmount((double) (3400 * (i + 1)));

			lstOmniGiftCardReceiver.add(omniGiftCardReceiver);
		}
		giftCardInsertDTO.setReceivers(lstOmniGiftCardReceiver);
		// set otp
		giftCardInsertDTO.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		giftCardInsertDTO.setOtpValue(otpValue1);

		List<SendGiftCardOutputDTO> lstSendGiftCardOutputDTO = new ArrayList<>();
		SendGiftCardOutputDTO sendGiftCardOutputDTO = new SendGiftCardOutputDTO();
		sendGiftCardOutputDTO.setBeneficiary("1234567890123");
		sendGiftCardOutputDTO.setDebitAccountId("0008000900700");
		PostPaymentOutputDTO mockData = initTestPostPayment();
		sendGiftCardOutputDTO.setPostPaymentOutputDTO(mockData);
		lstSendGiftCardOutputDTO.add(sendGiftCardOutputDTO);
		when(accountService.sendGiftCard(ArgumentMatchers.any(), anyString())).thenReturn(lstSendGiftCardOutputDTO);

		String path = "/service/account/sendGiftCard";
		System.out.println("***payload of testSendGiftCard " + objectMapper.writeValueAsString(giftCardInsertDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(giftCardInsertDTO)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("***testSendGiftCard : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());

		// Thread.sleep(100000);

	}

	@Test
	public void testPostBaskets() throws Exception {

		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		PostListBasketDTO resultMock = new PostListBasketDTO();
		when(accountService.postBaskets(ArgumentMatchers.any(), anyString())).thenReturn(resultMock);

		String path = "/service/paymentBasket/postBaskets";
		List<String> transfersIds = Arrays.asList("NIB-BAS46261113111803aabc47324cc064");
		PostBasketsInput input = new PostBasketsInput();
		input.setTransfersIds(transfersIds);
		input.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		input.setOtpValue(otpValue1);
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		System.out.println("*** payload of testPostBaskets " + objectMapper.writeValueAsString(input));
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		System.out.println("*******result of testPostBaskets : " + result.getResponse().getContentAsString());

	}

	@Test
	public void testPostBaskets_WRONGOTP() throws Exception {

		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);

		PostListBasketDTO resultMock = new PostListBasketDTO();
		when(accountService.postBaskets(ArgumentMatchers.any(), anyString())).thenReturn(resultMock);

		String path = "/service/paymentBasket/postBaskets";
		List<String> transfersIds = Arrays.asList("NIB-BAS46261113111803aabc47324cc064");
		PostBasketsInput input = new PostBasketsInput();
		input.setTransfersIds(transfersIds);
		input.setOtpMethod(OtpAuthorizeMethod.SMSOTP.getCode());
		input.setOtpValue(otpValue2);
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is5xxServerError()).andReturn();
		System.out.println("*** payload of testPostBaskets " + objectMapper.writeValueAsString(input));
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
		System.out.println("*******result of testPostBaskets : " + result.getResponse().getContentAsString());

	}

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	protected void publicEvent(OmniEvent omniEvent) {
		applicationEventPublisher.publishEvent(omniEvent);
	}

	@Test
	public void testchangePassword() throws Exception {
		initLoginMock();
		ChangePasswordInputDTO input = new ChangePasswordInputDTO();
		input.setNewPassword("Abc@123");
		input.setOldPassword("Abc@123");

		ChangePasswordDTO dto = new ChangePasswordDTO();
		dto.setCorrectOldPassword(true);
		dto.setChanged(true);
		dto.setMessage(MessageConstant.OK_MESSAGE);
		dto.setRequiredActionAfterChangePassword("NEXT_TOKEN_CODE");
		when(accountService.changePassword(anyString(), anyString(), anyString(), anyString())).thenReturn(dto);

		String path = "/service/account/changePassword";
		System.out.println("***payload of testchangePassword " + objectMapper.writeValueAsString(input));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();
		System.out.println("***testSendGiftCard : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		ChangePasswordDTO realData = objectMapper.convertValue(response.getData(), ChangePasswordDTO.class);

		assertNotNull(response);
	}

	@Test
	public void testUploadAvatar() throws Exception {

		initLoginMock();

		AvatarInfoOutputDTO dtoOutput = new AvatarInfoOutputDTO();
		AvatarInfoDTO avatarInfo = new AvatarInfoDTO();
		avatarInfo.setData("imageTestVoDien.jpg");
		avatarInfo.setFileName("avatarName");
		dtoOutput.setAvatarInfo(avatarInfo);
		dtoOutput.setMessage(MessageConstant.OK_MESSAGE);
		dtoOutput.setStatus(StatusesUpdateAvatar.OK.getStatusesUpdateAvatarCode());

		String fileinput = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADhAOEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK5Hx1qGqaNo6anps/lx28n+kK0asrIeMnPIwcdPU0m7K5pSpurUVNbvudVJIkab2ZVX1LYH51yWs/ELR9M+S3f7dP8A3YWGz8X6flmvHda8W6x4imlS9WLfGmLdowFDZOeefyNZcMjeX88Dbv73ITHYDpiuWeIe0T6jA8P03aWIlfyWi+//AIY9ePxb037REn9nXPkMP3khZcofYdCPfI+ldZpPivR9cu/s2n3YmlEXmsoUjaMgYORwckcV8+Wcqwy73gkk/wBnbxn1J6E0trqupaTqS6jZStb/ALwyM+8YRAB8pGTkHng8dKUa8lqzpxfD+GcP3N0/W6+Z9P0VwPw68eSeMo71LiKOOW32FWjBAdWyM4JODkfqK76uqMlJXR8fXozoVHTnugoooqjIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACsDxcLVvCeqR3s6Qwy27x+aykqhYYBOATgEjnHFb9ZWvFl0G92WK3/7k7rVmx5q4+ZQcHkjOOOTSexdN2mn5nyteGPTLmW0uHhu7VWPkywuCMj0b+6expkVwzxb/lhi9ftDHH55FOv4VS6uJodrWbSErFJl3iTPGTjB68/TNX4bKJ0R0VY/lx8vH5D8a4JRSR9thq9Scrf8P/n+JHaWi3jbnumk/wBpujfQdqk1VlsNPfbFC+3Hy7BjJ46VWNrKk7pE33f41bH4fWqt7uudto/mSStyvzcD3PqKlR1OqtiIqnLo7HtnwS05k0K/1aWNFku5hGoXAXag7AdOWP5V6vXA/COG5h+Hlit2qr88hjO3BdC2QTk89+cDgD6nvq9CEeWKR8Li6rq15Sf9W0CiiiqOYKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDg/EXw30TVJZr5fMsrhvmaSDGM9SSp4I78YNeW3eiwQ3SQ2morJAzeXG7KPpkgHgcivdPEep6Xp+lSpqs/lwTqYsKCWbIwcAfzrwae4g/tlLa3l8638wbZdpG4ZHODyP89a8/FNxa5TaOYYii1ySLA8DeIEukhiihklbhdsq4b/vrHoa6bRPhLqMl2j6vcW8Nru3yRwsWkf8A2c4AUe4J9vWozqmpatqn9nWX7qWBvNa73EbEUZJY9gPX9Oa7A6lf3UiP58yyso/dRlhzjptFRQrrk5pq5vPNMRUVmdnBDFbQJDEixxRqFVF4CgcAAelTVVsklhs4kuGLShfmLHPP171ar0k7o4gooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFc9qviuy0m7Nu8ckjr9/y8fLnnv1OKidSMFeTsJtLc6GiuSs/H2l3Nz5MqyW6s2FeTGPx9K1ru9vIVWWKCO6tmz/q85x+uRj0qY1oSV4u4KSex5V8Rvttlq6PqU8dx5qloUjz8ig4xg9P1rireVbm4+SBVRWB3bs/0ru/iDf215dWVxEs1vPt2yfN+7cAjGCO4yc5HcVxqeb5nzqrK3O7bg/pXmVmuZ21RyzdpaHsng7TLSbR5WaOPe83+kbVwZGGCNzdSACMAYH50ar4xsLKX7Jotst5fN8q+SnyA+mRyx9l/MV5nc+INZexS0SWNbXncq5G4nHLYPI4HGP516H8PZdNe2dEs1h1FVy7s24uueoOOBnHH061vh6vNFU1o7f1Y1VTmfLE63SX1B9OibUooo7vH7xYzkf8A1vzNaFFFeglZWNgooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAITxXjOrXsU3iDULlJf3TTHb33AcZH5flXrmoWf2/Tri08xo/OjKb16rnvXk2r+HbvR5NssfydFkXo309687MHLlWmhhXu0jHkaJ2+RWZv93/ABp9tc32no9zaTzW/fbHIR+YHBqa1hb7UibWbdwq7cljXa6L4Qklngub5DHFEwdYm6uRyMjsM+tcNCEqj9w54Qk3oOu/BEms6Lafbp1+2LGGZsEfMQM5POfc45xmue1zwium2q/ZPO2RqEmSTG8E9DkcbT6jvxXrtc94xjI8M3UqHbJGoKsv+8Mj6Yr06+Gi4Nrc7JU1JWPG3tGh+R19vZc+9a3hnWm0bU4rh13bcoyL/Ep649+/4V12u+GorbRX1OKXcsUIkaNl9hkgj/CsPwPbaXqGryzTxRtFBCZdzdM5AGc/U1yRpThUSW5yKnKM0j1W1uY7y2iuIs7JFDLuGODVisaLXLTzkt4Y28vhVZQAvpwPStmvVjJSWjO1MKKKKoYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUx0VxtZQw96fRQBCltDG25IY1b1VQKmoooSsAVkeJ4vO8Makn/Tu5/IZ/pWvVW+t2utPuLdW2mWMpn6jHepmrxaAzXM1z4Vt/Ji8xpbeMMvX5WUZ4PXiuTe1+xLLMlm0flL+8aOL0wcHaPpXQ+G7mTUtFs4kLRxwLsmbozEDhQR04wSev55q14os/O8K3ttAoX93kBeOAQxA/AGuacfaR512Imrq5H4ctrKbT4r5P3kjc/N/AfTHYj1roa8U8OeIbnRNZi2szWsnyzRe3XIH94f8A1u9ezQTRTwpNEytHIoZWXuD0NXh6kZRtEmlNSWhLRRRXQahRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXIeOpnSztYldl3sx9jgDqPxqKk+SLkTKXKrm7f61p2meUL26SLzfujk5x347VVTxXpLt/r2Cf32Xj/EflXlkp+X7zb/AMv5Urtsj+Rq8946TeiMHXPZba8trxN1vPHMP9hgas14tYqtyNkS7Z1wdyuUOM8gYBzxXeWvjPT4rXbcLJG0S4wuX3YHA55B+v4100sVGfxaGsJ8yuQ+FtStNM8NvNcPtEl1JtVerYwOB+FdJZajaarC/ktu7MjDBGfUV5Vp8LOHdGaTcxEa9duCeB9Tk13+gaHJamK9uG2y7T+624254GT9KzoVpyailoNSbdjzrXdDbRvEM67P3G7fCf7ykf05H4V2fgDWPPt5dLlb5oP3kPuh6j8Cf19q6PV9EsdbtvJvId3911OGX6H+h4rhb/S5PB2uWV9FK00G7+7zt4DA/gaTi6E+ZfCY8jpy5lsenUVHG6yRq6PuVlyp9QehqSu86QooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArn/E2gya5HarFOsLQMWyyk5yMdq6CipnBTjyvYTSaszy+bwZrHnbUgVv8App5o2/h3/DFUZtEv4W8prG48zjhUJ/EY6169RXG8DDozF0I9Dye38K37xvdyr9njXnMnyufYAjrTNZjg8l/lZZVwN397ngH1r03UtPTUYNjO0bLyrL6+471xninw+9n4avbhr7zGXyxGWTZty6g5IJyOfT86znhOVe6VGil7qMfwTq8VhqrwvA0jTriPb1U88D64/lXrNeQaOraP8QYod6qsczQSSScBgQen1OMV6/W+Db5Gn0Ci3azCql9Yw39u0My5HY91PqKt0V1NJqzNSGCFLaBIkG1I1Cr9BU1FFMAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACuT+IUcr+ELp4pSqxsjSIAPnG4DBPbBIP4V1lc345P/ABR+oL/CyhW+hIzUVGlBtgjkfF+nyvoel65v3XDRqs0qrgnd8yE49On4ivQNE1FdV0W1vh/y1jy3+8OD+oNQSaTDqXhdNLlZliktkj3LjIwBgj6YFSeHtJ/sPQ7fT/P87yt3z7cZyxPTt1rKnTcZ3WzX4kKNpXNWiiiugsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKxfFaK/hbUdw+7CSvsR0NbVZXiExf8I/e+a/lxeWdzbc8d+Kip8DAsaS2/RrJ/71vGf/HRV2qenxCDTrWFeVjhRA30AHSrlVHYAooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFYXjFtnhHUj/0xx+ZArdrmfHLN/wAIzLCF3efIkbfQnr+lRU+BjW5taYmzS7NP7sKD/wAdFXKQDilqxBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVy/j7jwlcS/8APKSOT8nFdRWH4vh87wlqSf8ATHd+RB/pUVPgY1ublFVNMm+06Vazf89YUf8ANRVuqTuIKKKKYBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVDV1WTRr1H+60Lj9DV+qGsts0a6P8A0zNRU+FjW4mix+Xolkn92BB+grQqjo6+Xolgn923jH/joq9TgrRSEFFFFUAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVl+If+QBef9c/6iiis6vwP0Y1uWNL/wCQRZf9e6f+girlFFWhBRRRTAKKKKACiiigAooooAKKKKACiiigAooooA//2Q==";
		Map<String, String> params = new HashMap<>();
		params.put("file", fileinput);

		when(accountService.uploadAvatar(anyString(), anyString())).thenReturn(dtoOutput);

		String path = "/service/account/uploadAvatar";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(params))).andExpect(status().isOk()).andReturn();
		System.out.println("***uploadAvatar : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AvatarInfoOutputDTO realData = objectMapper.convertValue(response.getData(), AvatarInfoOutputDTO.class);
		System.out.println("***uploadAvatar : " + realData);
		assertNotNull(response);
	}

	@Test
	public void test_updateProductDefaultDirectly() throws Exception {

		initLoginMock();

		String path = "/service/account/updateProductDefaultDirectly";
		Map<String, String> requestBody = new HashMap<>();

		requestBody.put("INTERNAL_TRANSFER_FROM_LIST", "381");
		requestBody.put("FAST_INTERBANK_TRANSFER_FROM_LIST", "382");
		requestBody.put("MOBILE_TOPUP_FROM_LIST", "381");
		requestBody.put("BILL_PAYMENT_FROM_LIST", "382");
		requestBody.put("TRANSFER_OTHER_FROM_LIST", "381");
		requestBody.put("EXTERNAL_TRANSFER_FROM_LIST", "382");

		when(accountService.updateProductDefaultDirectly(anyString(), ArgumentMatchers.anyMap())).thenReturn(true);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(requestBody));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		assertNotNull(responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		assertTrue(response.isSuccess());
		assertEquals(true, response.getData());
	}

	@Test
	public void test_transferAccounts() throws Exception {

		initLoginMock();
		String path = "/service/account/transferAccounts";
		List<TransferAccountDTO> list = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			TransferAccountDTO dto = new TransferAccountDTO();
			dto.setAccountId("" + i + 1);
			list.add(dto);
		}

		when(accountService.transferAccounts(anyString(), anyString(), anyString())).thenReturn(list);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.param("productList", "TRANSFER_FROM_LIST").param("restrictions", "ACCOUNT_RESTRICTION_DEBIT"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<TransferAccountDTO> realData = (List<TransferAccountDTO>) response.getData();
		System.out.println("payload of TransferAccountDTO : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testupdateUserDeviceLasttime() throws Exception {
		initLoginMock();
		login();
		// Thread.sleep(20000L);

		// assertEquals(mocklst.size(), lst.size());

	}

	@Test
	public void test_createDeposit() throws Exception {

		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		CreateDepositOutputDTO mockData = new CreateDepositOutputDTO();
		when(accountService.createDeposit(anyString(), ArgumentMatchers.any())).thenReturn(mockData);

		String path = "/service/deposit/createDeposit";

		String dataInput = "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"currency\":\"VND\",\"customName\":\"Ten So TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":1,\"openingAccountId\":\"11\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"11\",\"settlementAccountIntId\":\"11\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		// String dataInput =
		// "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"credentials\":\"23633186\",\"currency\":\"VND\",\"customName\":\"Ten
		// So
		// TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":2,\"openingAccountId\":\"53864\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"53864\",\"settlementAccountIntId\":\"53864\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		CreateDepositDTO depositDTO = objectMapper.readValue(dataInput, CreateDepositDTO.class);
		depositDTO.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		depositDTO.setOtpValue(otpValue1);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(depositDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("*** payload of test_createDeposit " + objectMapper.writeValueAsString(depositDTO));
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		CreateDepositOutputDTO realData = objectMapper.convertValue(response.getData(), CreateDepositOutputDTO.class);
		System.out.println("*******test_createDeposit:" + realData);
		assertNotNull(realData);

	}

	@Test
	public void test_createDeposit_NoOTP() throws Exception {

		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		CreateDepositOutputDTO mockData = new CreateDepositOutputDTO();
		when(accountService.createDeposit(anyString(), ArgumentMatchers.any())).thenReturn(mockData);

		String path = "/service/deposit/createDeposit";

		String dataInput = "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"currency\":\"VND\",\"customName\":\"Ten So TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":1,\"openingAccountId\":\"11\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"11\",\"settlementAccountIntId\":\"11\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		// String dataInput =
		// "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"credentials\":\"23633186\",\"currency\":\"VND\",\"customName\":\"Ten
		// So
		// TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":2,\"openingAccountId\":\"53864\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"53864\",\"settlementAccountIntId\":\"53864\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		CreateDepositDTO depositDTO = objectMapper.readValue(dataInput, CreateDepositDTO.class);
		// depositDTO.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		// depositDTO.setOtpValue(otpValue1);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(depositDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is5xxServerError()).andReturn();
		System.out.println("*** payload of test_createDeposit " + objectMapper.writeValueAsString(depositDTO));
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());

	}

	@Test
	public void test_changeUsername() throws Exception {

		initLoginMock();

		String path = "/service/account/changeUsername";
		ChangeUsernameInput changeUsername = new ChangeUsernameInput();
		changeUsername.setNewUsername(defaultUser + "");
		ChangeUsernameOutput output = new ChangeUsernameOutput();
		output.setStatus("status");

		when(accountService.changeUsername(anyString(), ArgumentMatchers.any())).thenReturn(output);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(changeUsername));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		assertNotNull(responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		assertTrue(response.isSuccess());
	}

}
