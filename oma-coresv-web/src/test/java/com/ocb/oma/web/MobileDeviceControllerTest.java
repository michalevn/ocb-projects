package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class MobileDeviceControllerTest extends BaseMockTest {

	@Test
	public void test_registerDevice() throws Exception {

		initLoginMock();

		String deviceId = "65d32952f937475b";

		String path = "/service/mobileDevices/registerDevice";

		OmniMobileDevice omniMobileDevice = new OmniMobileDevice();
		omniMobileDevice.setActive(true);
		omniMobileDevice.setDeviceBrand("samsung");
		omniMobileDevice.setOsType("android");
		omniMobileDevice.setDeviceId(deviceId);
		omniMobileDevice.setDeviceName("Galaxy Note9");
		omniMobileDevice.setDeviceModal("SM-N960F");
		omniMobileDevice.setVersion("27");
		omniMobileDevice.setDeviceToken(
				"fZ3giHxqW2s:APA91bGtejlEOCuKDDtJyAGXqSAJ9pWVv-7Vd_nzGj9V139S-6vz6sjVPVUy4jkNM1akEv8e1z3KVimLt97374kvYcJ992iLmSQ1gm1n0vbE1GO0pA4CSfLGrCCQ5SN4IRfiPEnL1u9O");

		System.out.println(objectMapper.writeValueAsString(omniMobileDevice));

		try {

			when(resourceService.registerDevice(ArgumentMatchers.any())).thenReturn(true);

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			requestBuilder.content(objectMapper.writeValueAsBytes(omniMobileDevice));
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
