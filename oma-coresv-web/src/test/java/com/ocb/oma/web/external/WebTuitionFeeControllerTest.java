package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.ResourceServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WebTuitionFeeControllerTest extends BaseTest {

	@Autowired
	protected ResourceServiceExt resourceServiceExt;

	@Test
	public void test_findGroups() throws Exception {
		try {

			List<TuitionFeeGroup> tuitionFeeGroups = resourceServiceExt.findTuitionFeeGroups();

			String path = "/service/tuitionFee/findGroups";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(tuitionFeeGroups.size(), data.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_findByGroup() throws Exception {

		try {

			String groupCode = "DH";
			int page = 0;
			int size = 10;

			List<TuitionFeeObject> tuitionFeeObjects = resourceServiceExt.findTuitionFeesByGroup(groupCode, page, size);

			String path = "/service/tuitionFee/findByGroup";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			requestBuilder.param("groupCode", groupCode);
			requestBuilder.param("page", page + "");
			requestBuilder.param("size", size + "");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(tuitionFeeObjects.size(), data.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
