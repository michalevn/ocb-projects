package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class UserDeviceControllerTest extends BaseTest {

	private String cif = "801003";
	private String deviceId = defaultDeviceId;

	@Test
	public void test_findUserDevices() throws Exception {

		String path = "/service/userDevices/findUserDevices";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.param("cif", cif);

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.out.println("findUserDevices : " + responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());

	}

	@Test
	public void test_registerUserDevice() throws Exception {

		String cif = "2005";

		String path = "/service/userDevices/registerUserDevice";

		UserDeviceToken userDeviceToken = new UserDeviceToken();
		userDeviceToken.setDeviceId(defaultDeviceId);
		userDeviceToken.setCif(cif);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.content(objectMapper.writeValueAsString(userDeviceToken));
		requestBuilder.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.out.println("test_registerUserDevice : " + responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData());

	}

	@Test
	public void test_updateUserDeviceStatus() throws Exception {

		String path = "/service/userDevices/updateUserDeviceStatus";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.param("deviceId", deviceId);
		requestBuilder.param("enable", "true");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("updateUserDeviceStatus : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData() instanceof Boolean);
		assertEquals(true, response.getData());
	}
}
