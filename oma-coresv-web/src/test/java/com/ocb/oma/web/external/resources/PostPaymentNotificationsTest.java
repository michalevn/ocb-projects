package com.ocb.oma.web.external.resources;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.web.BaseTest;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.internal.WebAccountSerivice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class PostPaymentNotificationsTest extends BaseTest {

	@Autowired
	@MockBean
	private WebAccountSerivice webAccountSerivice;

	@MockBean
	@Autowired
	protected AccountServiceExt accountService;

	private String cif = "801004";

	protected void initMock() {

		UserToken userToken = new UserToken();
		userToken.setUsername(defaultUser);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801003";
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, "SMSOTP", "09763652512");
		userToken.setOldAuthen(oldOmni);

		when(webAccountSerivice.checkLogin(anyString(), anyString(), anyString(), anyString())).thenReturn(userToken);

		when(accountService.getCifByAccountNo(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
				.thenReturn(cif);

		when(webAccountSerivice.vetifyUserDevice(ArgumentMatchers.any())).thenReturn(true);

		UserToken a = webAccountSerivice.checkLogin("", "", "", "");
		System.out.println(a);
	}

	//@Test
	public void test_postPayment() throws Exception {

		initMock();

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("11"); // ibtest10
		paymentInfo.setAmount(100000L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0037100003605005"); // ibtest9
		paymentInfo.setRecipient("Hoang Nguyen Sy Phu");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testPostPayment " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());
	}

}
