package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.dto.CardBlockade;
import com.ocb.oma.dto.CardBlockadeInputDTO;
import com.ocb.oma.dto.ChangeLimitDTO;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.PaymentCardAnotherOuputDTO;
import com.ocb.oma.dto.input.CardPaymentInfoInput;
import com.ocb.oma.dto.input.CardRejectedInputDTO;
import com.ocb.oma.dto.input.CardStatementsInputDTO;
import com.ocb.oma.dto.input.ConfigStatusEcommerceCardInput;
import com.ocb.oma.dto.input.PaymentCardAnotherInputDTO;
import com.ocb.oma.dto.input.PostAuthActivateCardInput;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.CardRejectedDTO;
import com.ocb.oma.oomni.dto.CardAccountAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardAccountEntryQueryDTO;
import com.ocb.oma.oomni.dto.CardAutoRepaymentDTO;
import com.ocb.oma.oomni.dto.CardStatementsDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class CardControllerTest extends BaseTest {

	@Test
	public void testFindCard() throws Exception {
		String path = "/service/card/findCard";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("testFindCard : " + rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testPostAuthActivateCard() throws Exception {

		String path = "/service/card/postAuthActivateCard";

		VerifyOtpToken otpValue = new VerifyOtpToken();
		otpValue.setAuthId("12345");
		otpValue.setOtpValue("12345678");

		PostAuthActivateCardInput input = new PostAuthActivateCardInput();
		input.setOtpValue(otpValue);
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.param("cardId", "301");
		
		requestBuilder.content(objectMapper.writeValueAsBytes(input));
		
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), new TypeReference<OutputBaseDTO>() {
		});

		System.out.println("*******result of postAuthActivateCard : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testModifyCardAccountAutoRepayment() throws Exception {
		String path = "/service/card/modifyCardAccountAutoRepayment";

		CardAccountAutoRepaymentDTO dto = new CardAccountAutoRepaymentDTO();
		dto.setCardId("302");
		dto.setRemitterAccountId("3002100000339009");
		dto.setAutoRepaymentType("MIN");

		System.out.println("===> omniNotification: " + objectMapper.writeValueAsString(dto));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> testModifyCardAccountAutoRepayment: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void testFindCardAccountEntry() throws Exception {
		String path = "/service/card/findCardAccountEntry";

		CardAccountEntryQueryDTO dto = new CardAccountEntryQueryDTO();
		dto.setOperationAmountFrom(BigDecimal.valueOf(0));
		dto.setOperationAmountTo(BigDecimal.valueOf(1000));

		System.out.println("===> omniNotification: " + objectMapper.writeValueAsString(dto));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> testFindCardAccountEntry: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void test_getPaymentDetails() throws Exception {
		String path = "/service/card/getPaymentDetails?paymentId=d725458f-d475-4a0c-8fd9-b895346869ac@waiting";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("===> test_getPaymentDetails: " + result.getResponse().getContentAsString());
		assertNotNull(rs);
	}

	@Test
	public void testPostpaymentCardAnother() throws Exception {
		String path = "/service/card/postpaymentCardAnother";
		PaymentCardAnotherInputDTO input = new PaymentCardAnotherInputDTO();
		input.setAccountId("381");
		input.setAccountName("TGTT Ca Nhan");
		input.setAmount(10000D);
		input.setCardNumber("1098765432");
		input.setCreditCardAccount("1234567890");
		input.setCreditCardAccountName("Nguyen van an");
		input.setRemark("remark");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		PaymentCardAnotherOuputDTO realData = objectMapper.convertValue(response.getData(),
				new TypeReference<PaymentCardAnotherOuputDTO>() {
				});

		System.out
				.println("*******result of testPostpaymentCardAnother : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testfindCardByCardId() throws Exception {
		String path = "/service/card/findCardByCardId?card_id=301";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("testfindCardByCardId : " + rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testCardAutoRepayment() throws Exception {
		String path = "/service/card/cardAutoRepayment";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardAutoRepaymentDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<CardAutoRepaymentDTO>>() {
				});
		System.out.println("*******result of testCardAutoRepayment : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_GetFindCardBlockades() throws Exception {
		String path = "/service/card/findCardBlockades";
		CardBlockadeInputDTO input = new CardBlockadeInputDTO();
		input.setId("302");
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductType("CARD");
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardBlockade> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<CardBlockade>>() {
				});
		System.out.println("*******result of findCardBlockades : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_restrictCard() throws Exception {

		String path = "/service/card/restrictCard?cardId=301&cardRestrictionReason=LOST";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.err.println("############# responseText: " + responseText);
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), new TypeReference<OutputBaseDTO>() {
		});

		System.out.println("*******result of restrictCard : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testCardRejected() throws Exception {
		String path = "/service/card/cardRejected";
		CardRejectedInputDTO cardRejectedInput = new CardRejectedInputDTO();
		cardRejectedInput.setOperationType("all");
		cardRejectedInput.setPageNumber(1);
		cardRejectedInput.setPageSize(10);
		cardRejectedInput.setProductId("302");
		cardRejectedInput.setProductType("CARD");
		cardRejectedInput.setDateFrom(new Date(1544071598L));
		cardRejectedInput.setDateTo(new Date(1607229998L));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(cardRejectedInput)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardRejectedDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<CardRejectedDTO>>() {
				});
		System.out.println("*******result of testCardRejected : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testcardStatements() throws Exception {
		String path = "/service/card/cardStatements";
		CardStatementsInputDTO input = new CardStatementsInputDTO();
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setProductId("302_CARD");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<CardStatementsDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<CardStatementsDTO>>() {
				});
		System.out.println("*******result of testcardStatements : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_changeLimitDirectly() throws Exception {

		String path = "/service/card/changeLimitDirectly";

		List<ChangeLimitDTO> limits = new ArrayList<>();
		ChangeLimitDTO dto = new ChangeLimitDTO();
		dto.setChannelType("WWW");
		dto.setValue(BigDecimal.valueOf(RandomUtils.nextDouble()));
		limits.add(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(limits));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();

		assertNotNull(rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_configStatusEcommerceCard() throws Exception {

		String path = "/service/card/configStatusEcommerceCard";
		ConfigStatusEcommerceCardInput input = new ConfigStatusEcommerceCardInput();
		input.setCardNumber("9080898989");
		input.setType("UNLOCK");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(input));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();

		assertNotNull(rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		System.out.println("*******result of configStatusEcommerceCard : " + objectMapper.writeValueAsString(response));
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_cardPaymentInfo() throws Exception {

		String path = "/service/card/cardPaymentInfo";
		CardPaymentInfoInput input = new CardPaymentInfoInput();
		input.setCardAccountNumber("1234567");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(input));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();

		assertNotNull(rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		System.out.println("*******result of cardPaymentInfo : " + objectMapper.writeValueAsString(response));
		assertTrue(response.isSuccess());
	}
}
