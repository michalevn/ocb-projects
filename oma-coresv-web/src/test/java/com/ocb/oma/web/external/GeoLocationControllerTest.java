package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.dto.GeoLocationPoiDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class GeoLocationControllerTest extends BaseTest {

	@Test
	public void test_find() throws Exception {
		String path = "/service/geo-locations/find";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.contentType(MediaType.APPLICATION_JSON_UTF8);

		requestBuilder.param("lat", "10.768546");
		requestBuilder.param("lng", "106.700959");
		requestBuilder.param("poiType", "BRANCH");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		System.out.println("===> findGeoLocationPois: " + responseContent);
		ResponseDTO response = objectMapper.readValue(responseContent, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData() instanceof Map);
		Map map = (Map) response.getData();
		GeoLocationPoiDTO locationPoi = objectMapper.convertValue(map, GeoLocationPoiDTO.class);
		assertNotNull(locationPoi.getPoiPositions());
		assertNotNull(locationPoi.getRequestedGpsPosition());
		assertNotEquals(0, locationPoi.getPoiPositions().size());
	}
}
