package com.ocb.oma.web;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class OmaCoresvRegistryApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void testLoginFail() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String path = "/login";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", "admin");
		payload.put("password", "password1");
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).content(objectMapper.writeValueAsString(payload)))
				.andExpect(status().is4xxClientError()).andReturn();
		//	UserToken userToken = objectMapper.readValue(result.getResponse().getContentAsString(), UserToken.class);
		//	assertNotNull(userToken.getToken());
		System.out.println("*********************" + objectMapper.writeValueAsString(result.getResponse().getContentAsString()));
	}

}
