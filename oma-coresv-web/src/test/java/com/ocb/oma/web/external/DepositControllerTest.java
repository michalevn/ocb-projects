package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.dto.CreateDepositDTO;
import com.ocb.oma.dto.CreateDepositOutputDTO;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.DepositInterestRateDTO;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class DepositControllerTest extends BaseTest {

	@Test
	public void testBreakDeposit() throws Exception {
		String path = "/service/deposit/breakDeposit";
		String depositId = "1234567890123456";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(
				requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("depositId", depositId))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("testBreakDeposit : " + realData);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testGetDeposits() throws Exception {
		String path = "/service/deposit/getDeposits";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<DepositDTO> realData = (List<DepositDTO>) response.getData();
		System.out.println("testGetDeposits : " + realData);
		assertNotNull(realData);
	}

	@Test
	public void test_getDeposit() throws Exception {

		String path = "/service/deposit/getDeposit?depositId=1409";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		Map data = (Map) response.getData();

		assertNotNull(data);
		assertEquals(data.get("depositId"), "1409");
	}

	@Test
	public void test_getDepositProducts() throws Exception {

		String path = "/service/deposit/depositProducts";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("*******test_getDepositProducts:" + rs);
		assertNotNull(rs);

		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void test_getDepositInterestRates() throws Exception {

		String path = "/service/deposit/depositInterestRates";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.param("currency", "VND").param("subProductCode", "TICHLUYDIENTU"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<DepositInterestRateDTO> realData = (List<DepositInterestRateDTO>) response.getData();
		System.out.println("*******test_getDepositInterestRates:" + realData);
		assertNotNull(realData);
	}

	@Test
	public void test_createDeposit() throws Exception {

		String path = "/service/deposit/createDeposit";

		String dataInput = "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"currency\":\"VND\",\"customName\":\"Ten So TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":1,\"openingAccountId\":\"11\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"11\",\"settlementAccountIntId\":\"11\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		//String dataInput = "{\"amount\":\"100000\",\"autoCapitalisation\":true,\"autoRenewal\":true,\"campaignId\":\"null\",\"credentials\":\"23633186\",\"currency\":\"VND\",\"customName\":\"Ten So TK\",\"iconId\":\"10\",\"interest\":5.4,\"interestPaymentMethod\":\"IN_ADVANCE\",\"interestRateType\":\"FIXED\",\"numberOfDeposits\":2,\"openingAccountId\":\"53864\",\"period\":1,\"periodUnit\":\"M\",\"settlementAccountId\":\"53864\",\"settlementAccountIntId\":\"53864\",\"subProductCode\":\"TICHLUYDIENTU\"}";
		CreateDepositDTO depositDTO = objectMapper.readValue(dataInput, CreateDepositDTO.class);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(depositDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		CreateDepositOutputDTO realData = objectMapper.convertValue(response.getData(), CreateDepositOutputDTO.class);
		System.out.println("*******test_createDeposit:" + realData);
		assertNotNull(realData);

	}

	@Test
	public void testGetDepositOfferAcc() throws Exception {
		String path = "/service/deposit/getDepositOfferAcc";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		DepositOfferAccountsDTO realData = objectMapper.convertValue(response.getData(), DepositOfferAccountsDTO.class);
		System.out.println("testGetDepositOfferAcc : " + realData);
		assertNotNull(realData);
	}
}
