package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.util.AppUtil;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class UserDeviceControllerTest extends BaseMockTest {

	@SuppressWarnings("unchecked")
	@Test
	public void test_findUserDevices() throws Exception {

		initLoginMock();

		String cif = "2005";

		List<UserDeviceToken> userDeviceTokens = new ArrayList<>();
		userDeviceTokens.add(new UserDeviceToken());
		userDeviceTokens.get(0).setCif(cif);
		userDeviceTokens.get(0).setDeviceId(defaultDeviceId);

		when(resourceService.findUserDevices(anyString())).thenReturn(userDeviceTokens);

		String path = "/service/userDevices/findUserDevices?cif=" + cif;
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		System.out.println("findUserDevices : " + contentAsString);
		ResponseDTO response = objectMapper.readValue(contentAsString, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<UserDeviceToken> userDeviceTokens2 = (List<UserDeviceToken>) response.getData();
		assertNotNull(userDeviceTokens2);
		assertEquals(userDeviceTokens.size(), userDeviceTokens2.size());

	}

	@Test
	public void test_registerUserDevice() throws Exception {

		initLoginMock();

		String cif = "2005";

		String path = "/service/userDevices/registerUserDevice";

		UserDeviceToken userDeviceToken = new UserDeviceToken();
		userDeviceToken.setDeviceId(defaultDeviceId);
		userDeviceToken.setCif(cif);

		when(resourceService.registerUserDevice(ArgumentMatchers.any())).thenReturn(true);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.content(objectMapper.writeValueAsString(userDeviceToken));
		requestBuilder.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("test_registerUserDevice : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData());

	}

	@Test
	public void test_updateUserDeviceStatus() throws Exception {

		initLoginMock();

		String path = "/service/userDevices/updateUserDeviceStatus";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.param("deviceId", defaultDeviceId);
		requestBuilder.param("enable", "true");

		UserDeviceToken userDevice = new UserDeviceToken();
		userDevice.setDeviceId(defaultDeviceId);
		userDevice.setCif(AppUtil.getCurrentCustomerCif());

		when(resourceService.updateUserDeviceStatus(anyString(), anyString(), ArgumentMatchers.anyInt()))
				.thenReturn(true);

		when(resourceService.getUserDevice(anyString(), anyString())).thenReturn(userDevice);

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("updateUserDeviceStatus : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData() instanceof Boolean);
		assertEquals(true, response.getData());
	}
}
