package com.ocb.oma.web.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.BaseMockTest;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.service.external.ResourceServiceExt;
import com.ocb.oma.web.service.internal.TokenManagementService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class TokenManagementServiceTest extends BaseMockTest {

	@Autowired
	TokenManagementService tokenManagementService;

	@Autowired
	ResourceServiceExt resourceServiceExt;

	@Test
	public void testGetAccount() throws Exception {

		String tokenSample = "123-xyz";
		String username = "123";
		String deviceId = "xyz";
		String deviceToken = "5422";
		UserToken userToken = new UserToken();
		userToken.setUsername(username);
		userToken.setDeviceToken(deviceToken);
		userToken.setDeviceId(deviceId);
		userToken.setToken(tokenSample);
		Boolean flag = false;
		flag = (Boolean) tokenManagementService.getTokenValue(tokenSample);
		assertNull(flag);
		tokenManagementService.addToken(userToken);
		flag = (Boolean) tokenManagementService.getTokenValue(tokenSample);
		assertEquals(Boolean.TRUE, flag);
	}

	// @Test
	public void testExpire() throws InterruptedException {
		String tokenSample = "123-xyz";
		String username = "123";
		String deviceToken = "5422";
		String deviceId = "xyz";
		UserToken userToken = new UserToken();
		userToken.setUsername(username);
		userToken.setDeviceToken(deviceToken);
		userToken.setToken(tokenSample);
		userToken.setDeviceId(deviceId);
		Boolean flag;
		tokenManagementService.addToken(userToken);
		flag = (Boolean) tokenManagementService.getTokenValue(tokenSample);
		assertEquals(Boolean.TRUE, flag);
		Thread.sleep(5000L);
		flag = (Boolean) tokenManagementService.getTokenValue(tokenSample);
		assertNull(flag);
	}

}
