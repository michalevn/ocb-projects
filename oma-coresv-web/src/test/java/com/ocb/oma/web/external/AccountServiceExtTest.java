package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountLimitDTO;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.EVNParameter;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.PaymentBasketDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.PaymentsInFutureDTO;
import com.ocb.oma.dto.PostListBasketDTO;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RecipientType;
import com.ocb.oma.dto.StatusesType;
import com.ocb.oma.dto.TransactionBlockedDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;
import com.ocb.oma.dto.UpcomingRepaymentDepositDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.ChangePasswordInputDTO;
import com.ocb.oma.dto.input.ChangeUsernameInput;
import com.ocb.oma.dto.input.GetBillInputDTO;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.GetListOfBillTransactionsInputDTO;
import com.ocb.oma.dto.input.PaymentsInFutureInputDTO;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoDTO;
import com.ocb.oma.dto.input.QRcodeInfoInputDTO;
import com.ocb.oma.dto.input.QRcodeInfoLstItems;
import com.ocb.oma.dto.input.TransHistoryRequestDTO;
import com.ocb.oma.dto.input.TransactionFeeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.dto.newFo.NewFoBankingProductOnline;
import com.ocb.oma.oomni.dto.BillPaymentHistoryDTO;
import com.ocb.oma.oomni.dto.BillPaymentServiceDTO;
import com.ocb.oma.oomni.dto.ChangePasswordDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.oomni.dto.PaymentItemStudentFeeDTO;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.oomni.dto.SendGiftCardOutputDTO;
import com.ocb.oma.oomni.dto.ServiceProviderComposDTO;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.event.OmniEvent;
import com.ocb.oma.web.event.SendGiftCardEvent;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.internal.TokenManagementService;
import com.ocb.oma.web.util.AppUtil;

/**
 * Dung de integration test voi account service rest Can phai start account
 * service test that su them
 * 
 * @author phuhoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class AccountServiceExtTest extends BaseTest {

	@Autowired
	TokenManagementService tokenManagementService;

	@Autowired
	AccountServiceExt accountService;

	@Test
	public void testLoginOk() throws Exception {

		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", this.defaultUser);
		payload.put("password", defaultPassword);
		payload.put("deviceId", "xxx-xxx-xxx");
		payload.put("deviceToken", "xxx-xxx-xxx");
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		System.out.println("login result ");
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		// UserToken userToken = (UserToken) response.getData();
		System.out.println(
				"result of testLogin " + objectMapper.writeValueAsString(result.getResponse().getContentAsString()));

		assertNotNull(response.getData());

		// UserToken userToken = objectmap
	}

	@Test
	public void testsendAuthorizationSmsOtpOk() throws Exception {
		String path = "/service/otp/sendAuthorizationSmsOtp";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println("sendAuthorizationSmsOtp : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		AuthorizationResponse authResponse = objectMapper.convertValue(response.getData(), AuthorizationResponse.class);

		// assertEquals(tokenMock, response.getData());

	}

	@Test
	public void testVerifyOTP() throws Exception {
		String path = "/service/otp/verifyOTPToken";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		VerifyOtpToken data = new VerifyOtpToken();
		data.setAuthId("12345");
		data.setOtpValue("something");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testVerifyOTP  : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());

		// assertEquals(tokenMock, response.getData());

	}

	@Test
	public void testVerifyHWOTP() throws Exception {
		String path = "/service/otp/verifyHWOTPToken";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		Map<String, String> data = new HashMap<>();
		data.put("tokenValue", "123456789");
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testVerifyHWOTP  : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());

		// assertEquals(tokenMock, response.getData());

	}

	@Test
	public void testcountBasketPaymentToProcess() throws Exception {
		String path = "/service/paymentBasket/countBasketPaymentToProcess";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println("testcountBasketPaymentToProcess : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		// assertEquals(tokenMock, response.getData());

	}

	@Test
	public void testGetAccountList() throws Exception {

		String path = "/service/account/getAccountList";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetAccountList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		List<AccountInfo> lst = (List<AccountInfo>) response.getData();
		assertEquals(2, lst.size());
	}

	@Test
	public void testGetCompagnList() throws Exception {
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		String path = "/service/campaign/getCampaignList";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetCompagnList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<CampaignInfo> lst = (List<CampaignInfo>) response.getData();
		assertEquals(2, lst.size());

	}

	@Test
	public void testfindTransactionHistory() throws Exception {

		String path = "/service/account/findTransactionHistory";

		TransHistoryRequestDTO data = new TransHistoryRequestDTO("8", null, new Date(1454259600000L),
				new Date(1580490000000L), 0D, 10000000D, 1, 10);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(data))).andExpect(status().isOk()).andReturn();

		System.out.println("payload " + objectMapper.writeValueAsString(data));

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<TransactionHistoryDTO> realData = (List<TransactionHistoryDTO>) response.getData();
		System.out.println("payload of testfindTransactionHistory : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testupdateAccountName() throws Exception {
		String path = "/service/account/updateAccountName";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.param("accountName", "Nguyen Van A").param("accountNo", "123456789"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testupdateAccountName : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(true, response.getData());
	}

	@Test
	public void testGetRecipientList() throws Exception {
		String path = "/service/recipient/getRecipientList?pageSize=100";//filerTemplateType=EXTERNAL&
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetRecipientList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<RecipientDTO> lst = (List<RecipientDTO>) response.getData();
		System.out.println("testGetRecipientList : " + objectMapper.writeValueAsString(lst));
		assertNotNull(lst);

	}

	@Test
	public void testGetRecipientListNOparam() throws Exception {
		String path = "/service/recipient/getRecipientList";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetRecipientList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<RecipientDTO> lst = (List<RecipientDTO>) response.getData();
		assertNotNull(lst);

	}

	@Test
	public void testDeleteRecipient() throws Exception {
		RecipientDTO data = new RecipientDTO();
		data.setRecipientId("1");
		data.setRecipientName("Nguyen Ngoc Anh");
		data.setToAccount("0039393828");
		data.setBankCode("JSGJJ");
		String path = "/service/recipient/deleteRecipient";
		System.out.println("payload of testDeleteRecipient" + objectMapper.writeValueAsString(data));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(data))).andExpect(status().isOk()).andReturn();
		System.out.println("testDeleteRecipient : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		Boolean delCheck = (Boolean) response.getData();
		assertNotNull(delCheck);

	}

	@Test
	public void testloadAccountSummarByCurrencies() throws Exception {
		String path = "/service/account/loadAccountSummarByCurrencies?accountId=41&summaryPeriod=180";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AccountSummaryDTO realData = objectMapper.convertValue(response.getData(), AccountSummaryDTO.class);
		System.out
				.println("payload of testloadAccountSummarByCurrencies : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetAccountLimit() throws Exception {

		String path = "/service/account/accountLimit?paymentType=WrongValue";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is5xxServerError()).andReturn();
		System.out.println("***********Test getAccountLimit wrong value " + result.getResponse().getContentAsString());
		// test with right param
		path = "/service/account/accountLimit?paymentType=" + PaymentType.InternalPayment.getPaymentTypeCode();
		requestBuilder = processPostAuth(path);
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AccountLimitDTO realData = objectMapper.convertValue(response.getData(), AccountLimitDTO.class);
		System.out.println("payload of testGetAccountLimit : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testgetAccountNameByAccountNo() throws Exception {

		String path = "/service/account/getAccountNameByAccountNo?accountNo=0101982812";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String realData = objectMapper.convertValue(response.getData(), String.class);
		System.out
				.println("****payload of testgetAccountNameByAccountNo : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testgetAccountNameNapas() throws Exception {

		String path = "/service/account/getAccountNameNapas?accountNo=0101982812&bankCode=ABC&cardNumber=&debitAccount=";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String realData = objectMapper.convertValue(response.getData(), String.class);
		System.out.println("****result of testgetAccountNameNapas : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testgetTransactionFee() throws Exception {
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		TransactionFeeInputDTO inputDTO = new TransactionFeeInputDTO("010892111", "0129192911", 100000D, "vnd",
				PaymentType.InternalPayment.getPaymentTypeCode(), "provinceCode", new Date());

		String path = "/service/paymentBasket/getTransactionFee";
		System.out.println("***payload of getTransactionFee " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testgetTransactionFee : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
	}

	@Test
	public void testgetChallengeCodeSoftOTP() throws Exception {
		GetChallengeCodeInputDTO input = new GetChallengeCodeInputDTO("cif", "creditAcc01", "debit01", 1000000D,
				PaymentType.LocalPayment.getPaymentTypeCode(), "vnd", "citadCode", "provinceCode");
		String path = "/service/otp/getChallengeCodeSoftOTP";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		System.out.println("***Payload of testgetChallengeCodeSoftOTP" + objectMapper.writeValueAsString(input));
		System.out.println("***testgetChallengeCodeSoftOTP : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());

	}

	@Test
	public void testPostPayment() throws Exception {

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("11"); // ibtest10
		paymentInfo.setAmount(100000L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0037100003605005"); // ibtest9
		paymentInfo.setRecipient("Hoang Nguyen Sy Phu");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testPostPayment " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testPostPaymentLocalPayment() throws Exception {

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.LocalPayment.getPaymentTypeCode());
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAmount(100000L);
		paymentInfo.setAccountId("11");
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("987654321");
		paymentInfo.setRecipient("PHAM THANH AN");
		paymentInfo.setRemarks("123");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setCreditAccountBankBranchCode("83202001");
		paymentInfo.setCreditAccountBankCode("202");
		paymentInfo.setCreditAccountProvinceCode("83");

		inputDTO.setPaymentInfo(paymentInfo);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testPostPayment " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testgetUpcomingRepaymentDeposit() throws Exception {

		String path = "/service/account/getUpcomingRepaymentDeposit";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		UpcomingRepaymentDepositDTO realData = objectMapper.convertValue(response.getData(),
				UpcomingRepaymentDepositDTO.class);
		// System.out.println("***");
		System.out.println("****result of getUpcomingRepaymentDeposit : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testgetListOfBillTransactions() throws Exception {

		String path = "/service/paymentBasket/getListOfBillTransactions";
		GetListOfBillTransactionsInputDTO inputDTO = new GetListOfBillTransactionsInputDTO(new Date(1517418000000L),
				new Date(1545547421693L));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<BillPaymentHistoryDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<BillPaymentHistoryDTO>>() {
				});

		System.out.println(
				"*******result of testgetListOfBillTransactions : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testgetListBillPaymentByService() throws Exception {

		String serviceCode = "PPMB";
		Integer duration = 180;

		String path = "/service/paymentBasket/getListBillPaymentByService";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.param("serviceCode", serviceCode).param("duration", duration + ""))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<BillPaymentServiceDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<BillPaymentServiceDTO>>() {
				});
		System.out.println(
				"*******result of testgetListBillPaymentByService : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testgetBill() throws Exception {

		////////
		String path = "/service/paymentBasket/getBill";
		GetBillInputDTO inputDTO = new GetBillInputDTO(false, "BillCode01", "Water01", "Water");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(inputDTO)))
				.andExpect(status().is2xxSuccessful()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		BillPaymentServiceDTO realData = objectMapper.convertValue(response.getData(),
				new TypeReference<BillPaymentServiceDTO>() {
				});

		System.out.println("*******result of testgetBill : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testgetListOfServiceProvider() throws Exception {

		String path = "/service/paymentBasket/getListOfServiceProvider";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<ServiceProviderComposDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<ServiceProviderComposDTO>>() {
				});
		System.out.println("*******result of getListOfServiceProvider : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testPostBaskets() throws Exception {

		String path = "/service/paymentBasket/postBaskets";
		List<String> transfersIds = Arrays.asList("NIB-BAS572606100119949b50d97d28a800");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(transfersIds)))
				.andExpect(status().is2xxSuccessful()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		PostListBasketDTO realData = objectMapper.convertValue(response.getData(),
				new TypeReference<PostListBasketDTO>() {
				});

		System.out.println("*******result of testPostBaskets : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	protected void publicEvent(OmniEvent omniEvent) {
		applicationEventPublisher.publishEvent(omniEvent);
	}

	@Test
	public void testSendGiftCardNoMockEventResoucre() throws Exception {
		GiftCardInsertDTO giftCardInsertDTO = new GiftCardInsertDTO();
		OmniGiftCard giftCard = new OmniGiftCard();
		giftCard.setDebitAccountId("1009233800303");
		giftCard.setCurrency("VND");
		giftCard.setShortMessage("Have a nice day");
		giftCardInsertDTO.setGiftCard(giftCard);
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = new ArrayList<>();
		String[] arrSetBeneficiary = { "0300958900090", "0300958900300", "030095890999" };
		for (int i = 0; i < arrSetBeneficiary.length; i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = new OmniGiftCardReceiver();
			omniGiftCardReceiver.setBeneficiary(arrSetBeneficiary[i]);
			omniGiftCardReceiver.setRecipient(
					"c2yJi_v8Sh8:APA91bEMk1voFMm0DNHY-WKPQULpaGrWWEY_nY7fikrRYxeOlI328JM4gpEqxEC0eKR3_y2FfNKL96XBfx4f3D54hjWTX-TNHUq95GloLDo8mWIGRQnz_94kivIbj6G5vkcn3iF-PaN75");
			omniGiftCardReceiver.setAmount((double) (3400 * (i + 1)));

			lstOmniGiftCardReceiver.add(omniGiftCardReceiver);
		}
		giftCardInsertDTO.setReceivers(lstOmniGiftCardReceiver);

		String path = "/service/account/sendGiftCard";
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		System.out.println("***payload of testSendGiftCard " + objectMapper.writeValueAsString(giftCardInsertDTO));

		// resourceServiceExt.sendGiftCardResource(giftCardInsertDTO);// chi test save
		List<SendGiftCardOutputDTO> result = accountService.sendGiftCard(giftCardInsertDTO, jwtToken);
		publicEvent(new SendGiftCardEvent(AppUtil.getCurrentUserLogin(), giftCardInsertDTO, result));
		assertNotNull(result);

	}

	// @Test //Muon test phai command @MockBean
	public void testSendGiftCardNoMockpostpayment() throws Exception {
		GiftCardInsertDTO giftCardInsertDTO = new GiftCardInsertDTO();
		OmniGiftCard giftCard = new OmniGiftCard();
		giftCard.setDebitAccountId("1009233800303");
		giftCard.setCurrency("VND");
		giftCard.setShortMessage("Have a nice day");
		giftCard.setFullMessage("Have a nice day");
		giftCardInsertDTO.setGiftCard(giftCard);
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = new ArrayList<>();
		String[] arrSetBeneficiary = { "0300958900090", "0300958900300", "030095890999" };
		for (int i = 0; i < arrSetBeneficiary.length; i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = new OmniGiftCardReceiver();
			omniGiftCardReceiver.setBeneficiary(arrSetBeneficiary[i]);
			omniGiftCardReceiver.setRecipient(
					"c2yJi_v8Sh8:APA91bEMk1voFMm0DNHY-WKPQULpaGrWWEY_nY7fikrRYxeOlI328JM4gpEqxEC0eKR3_y2FfNKL96XBfx4f3D54hjWTX-TNHUq95GloLDo8mWIGRQnz_94kivIbj6G5vkcn3iF-PaN75");
			omniGiftCardReceiver.setAmount((double) (3400 * (i + 1)));

			lstOmniGiftCardReceiver.add(omniGiftCardReceiver);
		}
		giftCardInsertDTO.setReceivers(lstOmniGiftCardReceiver);
		String path = "/service/account/sendGiftCard";
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		System.out.println("***payload of testSendGiftCard " + objectMapper.writeValueAsString(giftCardInsertDTO));

		List<SendGiftCardOutputDTO> result = accountService.sendGiftCard(giftCardInsertDTO, jwtToken);

		assertNotNull(result);
	}

	@Test
	public void testUpdateBasket() throws Exception {

		PostPaymentInputDTO input = new PostPaymentInputDTO();
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("41");
		paymentInfo.setAmount(99999L);
		paymentInfo.setBillCode("NCLYD");
		String[] billCodeItemNoArr = { "009", "008" };
		paymentInfo.setBillCodeItemNo(billCodeItemNoArr);
		paymentInfo.setCardNumber("00917389463457832");
		paymentInfo.setCreditAccount("11111111");
		paymentInfo.setCreditAccountBankBranchCode("10000001");
		paymentInfo.setCreditAccountBankCode("999991");
		paymentInfo.setCreditAccountProvinceCode("0099");
		paymentInfo.setcRefNum("123456");
		paymentInfo.setCurrency("USA");
		paymentInfo.seteWalletPhoneNumber("84");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setMobilePhoneNumber("0973456782");

		List<PaymentItemStudentFeeDTO> paymentItemStudentFee = new ArrayList<>();

		PaymentItemStudentFeeDTO paymentItemStudentFeeDTO = new PaymentItemStudentFeeDTO();
		paymentItemStudentFeeDTO.setAmount(350D);
		paymentItemStudentFeeDTO.setDiscount(10D);
		paymentItemStudentFeeDTO.setDueDate(new Date());
		paymentItemStudentFeeDTO.setIsrequired(1);
		paymentItemStudentFeeDTO.setItemNameDescription("Ngo thua hao");
		paymentItemStudentFeeDTO.setItemTypeFeeType("");
		paymentItemStudentFeeDTO.setPaymentItemFeeCode("");
		paymentItemStudentFee.add(paymentItemStudentFeeDTO);

		paymentInfo.setPaymentItemStudentFee(paymentItemStudentFee);
		paymentInfo.setRecipient("");
		paymentInfo.setRemarks("Chuyen tien mung");
		paymentInfo.setServiceCode("OOOC");
		paymentInfo.setServiceProviderCode("PT");
		paymentInfo.setStudentCode("QCT");

		input.setPaymentInfo(paymentInfo);
		input.setPaymentType(PaymentType.FastTransfer.getPaymentTypeCode());

		String path = "/service/paymentBasket/updateBasket";
		System.out.println("***payload of testUpdateBasket " + objectMapper.writeValueAsString(input));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();
		System.out.println("***testUpdateBasket : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testUpdateBasket " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());

	}

	@Test
	public void testGetAuthorizeMethodByAmount() throws Exception {
		Double amount = 20000000D;
		String paymentType = PaymentType.InternalPayment.getPaymentTypeCode();
		String path = "/service/otp/getAuthorizeMethodByAmount?paymentType=" + paymentType + "&amount=" + amount;
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("***testGetAuthorizeMethodByAmount : " + result.getResponse().getContentAsString());
		String response = result.getResponse().getContentAsString();
		System.out.println("***result of testGetAuthorizeMethodByAmount " + response);
		assertNotNull(response);
	}

	@Test
	public void testchangePassword() throws Exception {
		ChangePasswordInputDTO input = new ChangePasswordInputDTO();
		input.setNewPassword("Abc@123");
		input.setOldPassword("Abc@123");
		String path = "/service/account/changePassword";
		System.out.println("***payload of testchangePassword " + objectMapper.writeValueAsString(input));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();
		System.out.println("***testchangePassword : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		ChangePasswordDTO realData = objectMapper.convertValue(response.getData(), ChangePasswordDTO.class);
		System.out.println("***result of testchangePassword: " + realData);
		assertNotNull(response);
	}

	@Test
	public void testGetBlockedTransaction() throws Exception {

		String path = "/service/account/getBlockedTransaction?accountId=123&currentPage=3&pageSize=10";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<TransactionBlockedDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<TransactionBlockedDTO>>() {
				});
		System.out.println("*******result of testGetBlockedTransaction : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testAddRecipient() throws Exception {

		String path = "/service/recipient/addRecipient";
		RecipientInputDTO dtoInput = new RecipientInputDTO();
		dtoInput.setRecipientType(RecipientType.EXTERNAL.getRecipientTypeCode());
		dtoInput.setShortName("Ha Minh Lam");
		dtoInput.setAccountId("lam2008");
		dtoInput.setRemarks("Chuyen khoan thoi noi");
		dtoInput.setBeneficiary("24, Truong Tho, Thu Duc");
		dtoInput.setCreditAccount("08897443976434");
		dtoInput.setProvince("0024");
		dtoInput.setBankCode("OCB");
		dtoInput.setBranchCode("DEF");
		dtoInput.setCardNumber("220099");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(dtoInput)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		AddRecipientOutputDTO realData = objectMapper.convertValue(response.getData(), AddRecipientOutputDTO.class);
		System.out.println("*******result of testGetBlockedTransaction : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testLoadEVNparameter() throws Exception {

		String path = "/service/paymentBasket/loadEVNparameter";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<EVNParameter> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<EVNParameter>>() {
				});
		System.out.println("*******result of loadEVNparameter : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testDeleteBasket() throws Exception {
		String path = "/service/paymentBasket/deleteBasket";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("basketId", "1"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testDeleteBasket : " + result.getResponse().getContentAsString());
		String response = result.getResponse().getContentAsString();
		assertNotNull(response);

	}

	@Test
	public void testGetListofBasket() throws Exception {
		String path = "/service/paymentBasket/getListofBasket";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").param("amountFrom", "0")
						.param("amountTo", "10000000").param("customerId", "")
						.param("realizationDateFrom", "1453185446").param("realizationDateTo", "1608358090000")
						.param("statuses", StatusesType.READY.getStatusesTypeCode()))
				.andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("getListofBasket : " + result.getResponse().getContentAsString());
		List<PaymentBasketDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<PaymentBasketDTO>>() {
				});
		System.out.println("result of getListofBasket : " + objectMapper.writeValueAsString(realData));
		assertNotNull(response);

	}

	@Test
	public void testPostDeleteBasket() throws Exception {

		String path = "/service/paymentBasket/deleteBasket";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("basketId", "1"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testdeleteBasket : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		String delCheck = (String) response.getData();
		assertNotNull(delCheck);

	}

	@Test
	public void testGetPaymentsInFuture() throws Exception {

		String path = "/service/paymentBasket/getPaymentsInFuture";

		PaymentsInFutureInputDTO input = new PaymentsInFutureInputDTO();
		Date realizationDateFrom = new Date(1453185446L);
		Date realizationDateTo = new Date(1545201446L);
		input.setPageNumber(1);
		input.setPageSize(10);
		input.setRealizationDateFrom(realizationDateFrom);
		input.setRealizationDateTo(realizationDateTo);
		input.setStatusPaymentCriteria(StatusesType.NEW.getStatusesTypeCode());
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<PaymentsInFutureDTO> realData = (List<PaymentsInFutureDTO>) response.getData();
		System.out.println("*******result of testGetPaymentsInFuture : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testGetListPrepaidPhone() throws Exception {

		String path = "/service/otp/getListPrepaidPhone";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<PrepaidPhoneDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<PrepaidPhoneDTO>>() {
				});
		System.out.println("*******result of testGetListPrepaidPhone : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_getCIFFromAccountList() throws Exception {

		URIBuilder uriBuilder = new URIBuilder("/service/account/getCIFFromAccountList");

		String path = uriBuilder.toString();

		Map<String, Object> params = new HashMap<>();
		params.put("lstAccountNum", Arrays.asList("0001100012281003", "0001100012282007", "0037100003605005"));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsBytes(params)))
				.andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();

		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);

		System.out.println("test_getCIFFromAccountList: " + rs);

		assertTrue(response.isSuccess());
	}

	@Test
	public void test_getPersonalInfo() throws Exception {

		String path = "/service/account/getPersonalInfo";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("*******test_getPersonalInfo:" + result.getResponse().getContentAsString());
		assertNotNull(rs);

		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_registerBankingProductOnline() throws Exception {

		String jsonInput = "{\"transactionInfo\":{\"cRefNum\":\"NHS_4\",\"clientCode\":\"LandingPage\",\"pRefNum\":null,\"transactionReturn\":null,\"transactionReturnMsg\":null},\"customerInfo\":{\"fullname\":\"test test full\",\"birthDay\":\"19831213\",\"gender\":\"FEMALE\",\"maritalStatus\":\"2.00\",\"jobInfo\":{\"professionalCode\":\"11\"},\"IDInfo\":[{\"IDNum\":\"000999888\",\"IDIssuedDate\":\"20170201\",\"IDIssuedLocation\":\"20\",\"IDType\":\"CMND\",\"nationality\":\"VN\"}],\"contactAddress\":{\"address1\":\"21/12A khu phố 1, đường HT35\",\"ward\":\"Phường Hiệp Thành\",\"email\":[\"huytest123@gmail.com\"],\"district\":\"21\",\"province\":\"1\"},\"contactInfo\":{\"mobileNumber\":[\"0909654321\"]},\"residentialAddress\":{\"address1\":\"21/12A khu phố 1 đường HT35\",\"ward\":\" Phường Hiệp Thành\",\"district\":\"21\",\"province\":\"1\"},\"permanentAddress\":{\"address1\":\"21/12A khu phố 1, đường HT35, Phường Hiệp Thành, Huyện Cần Giờ, TP. Hồ Chí Minh\"}},\"onlineService\":{\"source\":\"OMA\",\"registerDate\":\"20181001024315\",\"isOpenAtm\":\"1\",\"houseOwnership\":\"258\",\"stayInMonth\":\"23\",\"hasCreditCard\":\"1\",\"creditCardIssBy\":\"141\",\"creditCardLimit\":\"50000000\",\"isPlatinum\":\"1\",\"hasLifeInsu\":\"1\",\"lifeInsuBy\":\"282\",\"incomePerMonth\":\"33333333\",\"incomeSource\":\"348\",\"professionalClassify\":\"107\",\"department\":\"111\",\"companyName\":\"công ty TNHH dịch vụ vận chuyển Test test\",\"domain\":\"59\",\"position\":\"73\",\"monthOfLabour\":\"35\",\"labourContractType\":\"77\",\"expensePerMonth\":\"11111111\",\"netIncome\":null,\"cardType\":\"1\",\"imageOnCard\":null,\"cardExpectedLimit\":\"50000000\",\"cardReceivePlace\":\"87\",\"cardReceiveAddress\":\"TP. Hồ Chí Minh,Chi nhánh Thủ Đức\",\"secureQuestion\":\"81\",\"secureAnwser\":\"Trả lời\",\"contactPersonInfo\":\"Tên Tham Chiếu Test\",\"contactPersonRel\":\"126\",\"contactPersonMobile\":\"0987654321\",\"personalProfile1\":\"http://shopee.ocb.vn/upload/userInfo/cmndthe-can-cuoc_0909654321_20181001094658399.jpg;http://shopee.ocb.vn/upload/userInfo/cmndthe-can-cuoc_0909654321_20181001094658917.jpg;\",\"personalProfile2\":null,\"personalProfile3\":null,\"residentProfile1\":\"http://shopee.ocb.vn/upload/userInfo/so-giay-tam-tru_0909654321_20181001094726545.jpg\",\"residentProfile2\":\"http://shopee.ocb.vn/upload/userInfo/so-giay-tam-tru_0909654321_20181001094727051.jpg;\",\"residentProfile3\":null,\"residentProfile4\":null,\"financialProfile1\":\"http://shopee.ocb.vn/upload/userInfo/the-tin-dung_0909654321_20181001094712001.jpg\",\"financialProfile2\":\"http://shopee.ocb.vn/upload/userInfo/the-tin-dung_0909654321_20181001094712504.jpg;\",\"financialProfile3\":null,\"financialProfile4\":null,\"financialProfile5\":null,\"custRequirement\":null,\"insurProfileLink\":null,\"loanPurpose\":null,\"loanCollateralType\":null,\"loanAmount\":null,\"loanTerm\":null,\"productType\":\"Card\"}}";
		try {
			NewFoBankingProductOnline newFoBankingProductOnline = objectMapper.readValue(jsonInput,
					new TypeReference<NewFoBankingProductOnline>() {
					});
			String path = "/service/account/registerBankingProductOnline";
			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.content(objectMapper.writeValueAsBytes(newFoBankingProductOnline));
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
			String rs = result.getResponse().getContentAsString();
			System.out.println("*******test_registerBankingProductOnline:" + result.getResponse().getContentAsString());
			assertNotNull(rs);
			ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
			assertTrue(response.isSuccess());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testUploadAvatar() throws Exception {
		String path = "/service/account/uploadAvatar";
		String fileinput = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADhAOEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK5Hx1qGqaNo6anps/lx28n+kK0asrIeMnPIwcdPU0m7K5pSpurUVNbvudVJIkab2ZVX1LYH51yWs/ELR9M+S3f7dP8A3YWGz8X6flmvHda8W6x4imlS9WLfGmLdowFDZOeefyNZcMjeX88Dbv73ITHYDpiuWeIe0T6jA8P03aWIlfyWi+//AIY9ePxb037REn9nXPkMP3khZcofYdCPfI+ldZpPivR9cu/s2n3YmlEXmsoUjaMgYORwckcV8+Wcqwy73gkk/wBnbxn1J6E0trqupaTqS6jZStb/ALwyM+8YRAB8pGTkHng8dKUa8lqzpxfD+GcP3N0/W6+Z9P0VwPw68eSeMo71LiKOOW32FWjBAdWyM4JODkfqK76uqMlJXR8fXozoVHTnugoooqjIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACsDxcLVvCeqR3s6Qwy27x+aykqhYYBOATgEjnHFb9ZWvFl0G92WK3/7k7rVmx5q4+ZQcHkjOOOTSexdN2mn5nyteGPTLmW0uHhu7VWPkywuCMj0b+6expkVwzxb/lhi9ftDHH55FOv4VS6uJodrWbSErFJl3iTPGTjB68/TNX4bKJ0R0VY/lx8vH5D8a4JRSR9thq9Scrf8P/n+JHaWi3jbnumk/wBpujfQdqk1VlsNPfbFC+3Hy7BjJ46VWNrKk7pE33f41bH4fWqt7uudto/mSStyvzcD3PqKlR1OqtiIqnLo7HtnwS05k0K/1aWNFku5hGoXAXag7AdOWP5V6vXA/COG5h+Hlit2qr88hjO3BdC2QTk89+cDgD6nvq9CEeWKR8Li6rq15Sf9W0CiiiqOYKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDg/EXw30TVJZr5fMsrhvmaSDGM9SSp4I78YNeW3eiwQ3SQ2morJAzeXG7KPpkgHgcivdPEep6Xp+lSpqs/lwTqYsKCWbIwcAfzrwae4g/tlLa3l8638wbZdpG4ZHODyP89a8/FNxa5TaOYYii1ySLA8DeIEukhiihklbhdsq4b/vrHoa6bRPhLqMl2j6vcW8Nru3yRwsWkf8A2c4AUe4J9vWozqmpatqn9nWX7qWBvNa73EbEUZJY9gPX9Oa7A6lf3UiP58yyso/dRlhzjptFRQrrk5pq5vPNMRUVmdnBDFbQJDEixxRqFVF4CgcAAelTVVsklhs4kuGLShfmLHPP171ar0k7o4gooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFc9qviuy0m7Nu8ckjr9/y8fLnnv1OKidSMFeTsJtLc6GiuSs/H2l3Nz5MqyW6s2FeTGPx9K1ru9vIVWWKCO6tmz/q85x+uRj0qY1oSV4u4KSex5V8Rvttlq6PqU8dx5qloUjz8ig4xg9P1rireVbm4+SBVRWB3bs/0ru/iDf215dWVxEs1vPt2yfN+7cAjGCO4yc5HcVxqeb5nzqrK3O7bg/pXmVmuZ21RyzdpaHsng7TLSbR5WaOPe83+kbVwZGGCNzdSACMAYH50ar4xsLKX7Jotst5fN8q+SnyA+mRyx9l/MV5nc+INZexS0SWNbXncq5G4nHLYPI4HGP516H8PZdNe2dEs1h1FVy7s24uueoOOBnHH061vh6vNFU1o7f1Y1VTmfLE63SX1B9OibUooo7vH7xYzkf8A1vzNaFFFeglZWNgooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAITxXjOrXsU3iDULlJf3TTHb33AcZH5flXrmoWf2/Tri08xo/OjKb16rnvXk2r+HbvR5NssfydFkXo309687MHLlWmhhXu0jHkaJ2+RWZv93/ABp9tc32no9zaTzW/fbHIR+YHBqa1hb7UibWbdwq7cljXa6L4Qklngub5DHFEwdYm6uRyMjsM+tcNCEqj9w54Qk3oOu/BEms6Lafbp1+2LGGZsEfMQM5POfc45xmue1zwium2q/ZPO2RqEmSTG8E9DkcbT6jvxXrtc94xjI8M3UqHbJGoKsv+8Mj6Yr06+Gi4Nrc7JU1JWPG3tGh+R19vZc+9a3hnWm0bU4rh13bcoyL/Ep649+/4V12u+GorbRX1OKXcsUIkaNl9hkgj/CsPwPbaXqGryzTxRtFBCZdzdM5AGc/U1yRpThUSW5yKnKM0j1W1uY7y2iuIs7JFDLuGODVisaLXLTzkt4Y28vhVZQAvpwPStmvVjJSWjO1MKKKKoYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUx0VxtZQw96fRQBCltDG25IY1b1VQKmoooSsAVkeJ4vO8Makn/Tu5/IZ/pWvVW+t2utPuLdW2mWMpn6jHepmrxaAzXM1z4Vt/Ji8xpbeMMvX5WUZ4PXiuTe1+xLLMlm0flL+8aOL0wcHaPpXQ+G7mTUtFs4kLRxwLsmbozEDhQR04wSev55q14os/O8K3ttAoX93kBeOAQxA/AGuacfaR512Imrq5H4ctrKbT4r5P3kjc/N/AfTHYj1roa8U8OeIbnRNZi2szWsnyzRe3XIH94f8A1u9ezQTRTwpNEytHIoZWXuD0NXh6kZRtEmlNSWhLRRRXQahRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXIeOpnSztYldl3sx9jgDqPxqKk+SLkTKXKrm7f61p2meUL26SLzfujk5x347VVTxXpLt/r2Cf32Xj/EflXlkp+X7zb/AMv5Urtsj+Rq8946TeiMHXPZba8trxN1vPHMP9hgas14tYqtyNkS7Z1wdyuUOM8gYBzxXeWvjPT4rXbcLJG0S4wuX3YHA55B+v4100sVGfxaGsJ8yuQ+FtStNM8NvNcPtEl1JtVerYwOB+FdJZajaarC/ktu7MjDBGfUV5Vp8LOHdGaTcxEa9duCeB9Tk13+gaHJamK9uG2y7T+624254GT9KzoVpyailoNSbdjzrXdDbRvEM67P3G7fCf7ykf05H4V2fgDWPPt5dLlb5oP3kPuh6j8Cf19q6PV9EsdbtvJvId3911OGX6H+h4rhb/S5PB2uWV9FK00G7+7zt4DA/gaTi6E+ZfCY8jpy5lsenUVHG6yRq6PuVlyp9QehqSu86QooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArn/E2gya5HarFOsLQMWyyk5yMdq6CipnBTjyvYTSaszy+bwZrHnbUgVv8App5o2/h3/DFUZtEv4W8prG48zjhUJ/EY6169RXG8DDozF0I9Dye38K37xvdyr9njXnMnyufYAjrTNZjg8l/lZZVwN397ngH1r03UtPTUYNjO0bLyrL6+471xninw+9n4avbhr7zGXyxGWTZty6g5IJyOfT86znhOVe6VGil7qMfwTq8VhqrwvA0jTriPb1U88D64/lXrNeQaOraP8QYod6qsczQSSScBgQen1OMV6/W+Db5Gn0Ci3azCql9Yw39u0My5HY91PqKt0V1NJqzNSGCFLaBIkG1I1Cr9BU1FFMAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACuT+IUcr+ELp4pSqxsjSIAPnG4DBPbBIP4V1lc345P/ABR+oL/CyhW+hIzUVGlBtgjkfF+nyvoel65v3XDRqs0qrgnd8yE49On4ivQNE1FdV0W1vh/y1jy3+8OD+oNQSaTDqXhdNLlZliktkj3LjIwBgj6YFSeHtJ/sPQ7fT/P87yt3z7cZyxPTt1rKnTcZ3WzX4kKNpXNWiiiugsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKxfFaK/hbUdw+7CSvsR0NbVZXiExf8I/e+a/lxeWdzbc8d+Kip8DAsaS2/RrJ/71vGf/HRV2qenxCDTrWFeVjhRA30AHSrlVHYAooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFYXjFtnhHUj/0xx+ZArdrmfHLN/wAIzLCF3efIkbfQnr+lRU+BjW5taYmzS7NP7sKD/wAdFXKQDilqxBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVy/j7jwlcS/8APKSOT8nFdRWH4vh87wlqSf8ATHd+RB/pUVPgY1ublFVNMm+06Vazf89YUf8ANRVuqTuIKKKKYBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVDV1WTRr1H+60Lj9DV+qGsts0a6P8A0zNRU+FjW4mix+Xolkn92BB+grQqjo6+Xolgn923jH/joq9TgrRSEFFFFUAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVl+If+QBef9c/6iiis6vwP0Y1uWNL/wCQRZf9e6f+girlFFWhBRRRTAKKKKACiiigAooooAKKKKACiiigAooooA//2Q==";
		Map<String, String> params = new HashMap<>();
		params.put("file", fileinput);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(params))).andExpect(status().isOk()).andReturn();
		System.out.println("testUploadAvatar : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
	}

	@Test
	public void test_transferAccounts() throws Exception {

		String path = "/service/account/transferAccounts";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("productList", "TRANSFER_FROM_LIST")
				.param("restrictions", "ACCOUNT_RESTRICTION_DEBIT");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("*******test_transferAccounts:" + result.getResponse().getContentAsString());
		assertNotNull(rs);

		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_updateProductDefaultDirectly() throws Exception {

		String path = "/service/account/updateProductDefaultDirectly";
		Map<String, String> requestBody = new HashMap<>();

		requestBody.put("INTERNAL_TRANSFER_FROM_LIST", "381");
		requestBody.put("FAST_INTERBANK_TRANSFER_FROM_LIST", "382");
		requestBody.put("MOBILE_TOPUP_FROM_LIST", "381");
		requestBody.put("BILL_PAYMENT_FROM_LIST", "382");
		requestBody.put("TRANSFER_OTHER_FROM_LIST", "381");
		requestBody.put("EXTERNAL_TRANSFER_FROM_LIST", "382");

		System.out.println(objectMapper.writeValueAsString(requestBody));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(requestBody));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		assertNotNull(responseText);
		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);
		assertTrue(response.isSuccess());
		assertEquals(true, response.getData());
	}

	@Test
	public void testsendGiftCard() throws Exception {
		String path = "/service/account/sendGiftCard";
		GiftCardInsertDTO giftCardInsertDTO = new GiftCardInsertDTO();
		OmniGiftCard giftCard = new OmniGiftCard();
		giftCard.setDebitAccountId("1009233800303");
		giftCard.setCurrency("VND");
		giftCard.setShortMessage("Have a nice day");
		giftCardInsertDTO.setGiftCard(giftCard);
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = new ArrayList<>();
		String[] arrSetBeneficiary = { "0300958900090", "0300958900300", "030095890999" };
		for (int i = 0; i < arrSetBeneficiary.length; i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = new OmniGiftCardReceiver();
			omniGiftCardReceiver.setBeneficiary(arrSetBeneficiary[i]);
			omniGiftCardReceiver.setRecipient("Nguyễn Văn A");
			omniGiftCardReceiver.setAmount((double) (3400 * (i + 1)));

			lstOmniGiftCardReceiver.add(omniGiftCardReceiver);
		}
		giftCardInsertDTO.setReceivers(lstOmniGiftCardReceiver);
//		List<SendGiftCardOutputDTO> lstSendGiftCardOutputDTO = new ArrayList<>();
//		SendGiftCardOutputDTO sendGiftCardOutputDTO = new SendGiftCardOutputDTO();
//		sendGiftCardOutputDTO.setBeneficiary("1234567890123");
//		sendGiftCardOutputDTO.setDebitAccountId("0008000900700");
//		PostPaymentOutputDTO mockData = initTestPostPayment();
//		sendGiftCardOutputDTO.setPostPaymentOutputDTO(mockData);
//		lstSendGiftCardOutputDTO.add(sendGiftCardOutputDTO);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(giftCardInsertDTO)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<SendGiftCardOutputDTO> realData = objectMapper.convertValue(response.getData(),
				new TypeReference<List<SendGiftCardOutputDTO>>() {
				});
		System.out.println("*******result of sendGiftCard : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_checkQRcodeInfo() throws Exception {

		String path = "/service/otp/checkQRcodeInfo";
		QRcodeInfoInputDTO input = new QRcodeInfoInputDTO();
		QRcodeInfoDTO qrcodeInfo = new QRcodeInfoDTO();
		qrcodeInfo.setDebitAmount(100000L);
		qrcodeInfo.setPayType("payType");
		qrcodeInfo.setVoucherCode("voucherCode");
		List<QRcodeInfoLstItems> lstItems = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			QRcodeInfoLstItems infoLstItems = new QRcodeInfoLstItems();
			infoLstItems.setNote("note");
			infoLstItems.setQrInfor("qrInfor000" + i);
			infoLstItems.setQuantity(i * 10);
			lstItems.add(infoLstItems);
		}
		qrcodeInfo.setLstItems(lstItems);

		input.setQrcodeInfo(qrcodeInfo);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("*******result of test_checkQRcodeInfo : " + objectMapper.writeValueAsString(response));
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_getDetailPaymentsInFuture() throws Exception {

		String path = "/service/paymentBasket/getDetailPaymentsInFuture";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json").param("idFuturePayment",
						"7d37f465-f365-4a35-bd04-1dd21915d292@waiting"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println(
				"*******result of test_getDetailPaymentsInFuture : " + objectMapper.writeValueAsString(response));
		assertTrue(response.isSuccess());
	}

	@Test
	public void test_changeUsername() throws Exception {

		String path = "/service/account/changeUsername";
		ChangeUsernameInput changeUsername = new ChangeUsernameInput();
		changeUsername.setNewUsername(defaultUser + "");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(changeUsername)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("*******result of test_changeUsername : " + objectMapper.writeValueAsString(response));
		assertTrue(response.isSuccess());
	}
}
