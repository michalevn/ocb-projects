package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.FormDetailsDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class FormControllerTest extends BaseTest {

	@Test
	public void test_create() throws Exception {

		String path = "/service/forms";

		FormDTO formDTO = new FormDTO();
		formDTO.setTitle("Tiêu đề 01");
		formDTO.setDescription("Mô tả 01");
		formDTO.setUserEmail("docao.hcm@gmail.com");
		formDTO.setUserName("Đô Cao");
		formDTO.setUserPhone("0903657884");

		List<FormDetailsDTO> formDetailsDTOs = new ArrayList<>();

		FormDetailsDTO image01 = new FormDetailsDTO();
		image01.setFieldName("image");
		image01.setFieldType("IMAGE");
		image01.setFieldValue("data:image/png;xxx");
		formDetailsDTOs.add(image01);

		FormDetailsDTO image02 = new FormDetailsDTO();
		image02.setFieldName("image");
		image02.setFieldType("IMAGE");
		image02.setFieldValue("data:image/png;base64,zzz");
		formDetailsDTOs.add(image02);

		formDTO.setFormDetails(formDetailsDTOs);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(formDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String uuid = (String) response.getData();
		assertEquals("2147b1f6-92a3-431e-a3c8-a4e7162d3e64".length(), uuid.length());

		test_findByUuid(uuid);
	}

	public void test_findByUuid(String uuid) throws Exception {

		String path = "/service/forms/" + uuid;

		MockHttpServletRequestBuilder requestBuilder = processGetAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		FormDTO formDTO = objectMapper.convertValue(response.getData(), FormDTO.class);
		assertEquals(uuid, formDTO.getUuid());
	}
}
