package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WebTuitionFeeControllerTest extends BaseMockTest {

	@Test
	public void test_findGroups() throws Exception {

		initLoginMock();

		try {

			List<TuitionFeeGroup> tuitionFeeGroups = new ArrayList<>();
			TuitionFeeGroup tuitionFeeGroup = new TuitionFeeGroup();
			tuitionFeeGroup.setCode("123");
			tuitionFeeGroups.add(tuitionFeeGroup);

			when(resourceService.findTuitionFeeGroups()).thenReturn(tuitionFeeGroups);

			String path = "/service/tuitionFee/findGroups";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(1, data.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_findByGroup() throws Exception {

		initLoginMock();

		try {

			String groupCode = "DH";
			int page = 0;
			int size = 10;

			List<TuitionFeeObject> tuitionFeeObjects = new ArrayList<>();
			TuitionFeeObject tuitionFeeObject = new TuitionFeeObject();
			tuitionFeeObject.setCode("123");
			tuitionFeeObjects.add(tuitionFeeObject);

			when(resourceService.findTuitionFeesByGroup(groupCode, page, size)).thenReturn(tuitionFeeObjects);

			String path = "/service/tuitionFee/findByGroup";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			requestBuilder.param("groupCode", groupCode);
			requestBuilder.param("page", page + "");
			requestBuilder.param("size", size + "");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(1, data.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
