package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.dto.CampaignInfo;
import com.ocb.oma.dto.CampainSlot;
import com.ocb.oma.web.controller.WebAccountController;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.TokenManagementService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class CampaignControllerTest extends BaseMockTest {

	WebAccountController controller;

	@Autowired
	TokenManagementService tokenManagementService;

	private List<CampaignInfo> initCampaignMock() {
		List<CampaignInfo> lst = new ArrayList<>();
		CampaignInfo dto = new CampaignInfo();
		dto.setActivated(true);
		dto.setBannerText("banner Text ");
		dto.setDescription("This is campain test 1");
		dto.setFromDate(new Date(2018, 02, 02));
		dto.setToDate(new Date(2019, 01, 02));
		dto.setLink("https://www.desicomments.com/wp-content/uploads/2017/01/Today-Is-A-Good-Day.jpg");
		dto.setPriority(1);
		List<CampainSlot> slots = new ArrayList<>();
		// 1 slot
		CampainSlot slot = new CampainSlot();
		slot.setImageLink("https://www.iloveshayari.in/wp-content/uploads/2018/01/good-morning-wallpapers-hd-1024x550.jpg");
		slot.setMiniApplication("Applicaton A");
		slot.setSlotName("Slot name");
		slots.add(slot);

		dto.setSlots(slots);

		// campaign 1
		lst.add(dto);
		// campaign 2

		dto = new CampaignInfo();
		dto.setActivated(true);
		dto.setBannerText("banner Text 2");
		dto.setDescription("This is campain test 2 ");
		dto.setFromDate(new Date(2018, 02, 02));
		dto.setToDate(new Date(2019, 01, 02));
		dto.setLink("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzddpeMlEahJ6jG7_3EUa7z37ofkdFWgpdsQHLlPVPZm3wdWvC");
		dto.setPriority(2);
		lst.add(dto);
		return lst;
	}

	@Test
	public void testGetCompagnList() throws Exception {
		initLoginMock();
		when(accountService.getCampaignList(anyString(), anyString(), anyString())).thenReturn(initCampaignMock());
		// Swhen(restTemplate.postForObject(anyString(), anyString(),null));
		String path = "/service/campaign/getCampaignList";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetCompagnList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<CampaignInfo> lst = (List<CampaignInfo>) response.getData();
		assertEquals(2, lst.size());

	}

}
