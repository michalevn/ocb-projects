package com.ocb.oma.web.validation.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.web.BaseMockTest;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.internal.TokenManagementService;

/**
 * Dung de integration test voi account service rest Can phai start account
 * service test that su them
 * 
 * @author phuhoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class AccountServiceValTest extends BaseMockTest {

	@Autowired
	TokenManagementService tokenManagementService;

	@Autowired
	AccountServiceExt accountService;

	@Test
	public void testLoginOk() throws Exception {
		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", this.defaultUser);
		payload.put("password", defaultPassword);
		payload.put("deviceId", "xxx-xxx-xxx");
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		System.out.println("login result ");
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		// UserToken userToken = (UserToken) response.getData();
		System.out.println(
				"result of testLogin " + objectMapper.writeValueAsString(result.getResponse().getContentAsString()));

		assertNotNull(response.getData());

		// UserToken userToken = objectmap
	}

	@Test
	public void testsendAuthorizationSmsOtpOk() throws Exception {
		String path = "/service/otp/sendAuthorizationSmsOtp";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		System.out.println("sendAuthorizationSmsOtp : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		AuthorizationResponse authResponse = objectMapper.convertValue(response.getData(), AuthorizationResponse.class);

		// assertEquals(tokenMock, response.getData());

	}

}
