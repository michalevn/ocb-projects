/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;

/**
 * @author Phu Hoang
 *
 */
public class BaseAccountMockTest extends BaseTest {

	@MockBean
	@Autowired
	protected AccountServiceExt accountService;

	protected void initLoginMock() {
		UserToken userToken = new UserToken();
		userToken.setUsername(defaultUser);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801004";
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, "SMSOTP", "09763652512");
		userToken.setOldAuthen(oldOmni);

		when(accountService.checkLogin(anyString(), anyString(), anyString())).thenReturn(userToken);
	}

}
