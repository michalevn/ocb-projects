package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.QD630PoliciesDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.GetChallengeCodeInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.PrepaidPhoneDTO;
import com.ocb.oma.web.controller.WebAccountController;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.TokenManagementService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class OtpControllerTest extends BaseMockTest {

	WebAccountController controller;

	@Autowired
	TokenManagementService tokenManagementService;

	private AuthorizationResponse initOtpOk() {
		AuthorizationResponse responseMock = new AuthorizationResponse();
		responseMock.setAuthId("TYUIO^&*(");
		responseMock.setOperationDate(new Date());
		responseMock.setExpiryTime(1000L);
		responseMock.setOperationStatus(AuthorizationResponse.EXECUTED);

		return responseMock;
	}

	@Test
	public void testsendAuthorizationSmsOtpOk() throws Exception {
		initLoginMock();
		AuthorizationResponse responseMock = initOtpOk();
		when(accountService.sendAuthorizationSmsOtp(anyString())).thenReturn(responseMock);
		String path = "/service/otp/sendAuthorizationSmsOtp";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json").content("{}"))
				.andExpect(status().isOk()).andReturn();
		System.out.println("sendAuthorizationSmsOtp : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		AuthorizationResponse authResponse = objectMapper.convertValue(response.getData(), AuthorizationResponse.class);
		assertNotNull(authResponse);
		assertNotNull(authResponse.getAuthId());
		assertEquals(responseMock.getAuthId(), authResponse.getAuthId());

		// assertEquals(tokenMock, .getData());
		// assertEquals(MessageConstant.SERVER_ERROR, response.getErrorCode());

	}

	@Test
	public void testVerifySmSToken() throws Exception {
		initLoginMock();
		AuthorizationResponse responseMock = new AuthorizationResponse();
		responseMock.setOperationStatus(AuthorizationResponse.CORRECT);
		String path = "/service/otp/verifyOTPToken";
		VerifyOtpToken data = new VerifyOtpToken();
		data.setAuthId("12345");
		data.setOtpValue("123456789");
		// mocking
		when(accountService.verifyOTPToken(ArgumentMatchers.any(), anyString())).thenReturn(responseMock);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testVerifySmSToken : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		AuthorizationResponse authResponse = objectMapper.convertValue(response.getData(), AuthorizationResponse.class);
		assertNotNull(authResponse);
		assertEquals(responseMock.getOperationStatus(), authResponse.getOperationStatus());

	}

	@Test
	public void testVerifyHWToken() throws Exception {
		initLoginMock();
		AuthorizationResponse responseMock = new AuthorizationResponse();
		responseMock.setOperationStatus(AuthorizationResponse.CORRECT);

		String path = "/service/otp/verifyHWOTPToken";
		Map<String, String> data = new HashMap<>();
		data.put("tokenValue", "123456789");
		// mocking
		when(accountService.verifyHwOTPToken(anyString(), anyString())).thenReturn(responseMock);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(data)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("testVerifyHWToken : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		AuthorizationResponse authResponse = objectMapper.convertValue(response.getData(), AuthorizationResponse.class);
		assertNotNull(authResponse);
		assertEquals(responseMock.getOperationStatus(), authResponse.getOperationStatus());

	}

	@Test
	public void testgetChallengeCodeSoftOTP() throws Exception {
		initLoginMock();
		String mockData = "12313";
		GetChallengeCodeInputDTO input = new GetChallengeCodeInputDTO("cif", "creditAcc01", "debit01", 1000000D,
				PaymentType.LocalPayment.getPaymentTypeCode(), "vnd", "citadCode", "provinceCode");
		when(accountService.getChallengeCodeSoftOTP(ArgumentMatchers.any(), anyString())).thenReturn(mockData);
		String path = "/service/otp/getChallengeCodeSoftOTP";
		when(accountService.getAuthorizeMethodByAmount(anyString(), anyDouble(), anyString())).thenReturn(mockData);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.contentType("application/json").content(objectMapper.writeValueAsString(input)))
				.andExpect(status().isOk()).andReturn();
		System.out.println("***payload of testgetChallengeCodeSoftOTP " + objectMapper.writeValueAsString(input));
		System.out.println("***testgetChallengeCodeSoftOTP : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(true, response.isSuccess());
		assertNotNull(response.getData());
		assertEquals(mockData, response.getData());

	}

	@Test
	public void testGetAuthorizationPolicies() throws Exception {

		initLoginMock();
		List<String> data = new ArrayList<>();
		data.add(OtpAuthorizeMethod.NONE.getCode());
		data.add(OtpAuthorizeMethod.SMSOTP.getCode());
		data.add(OtpAuthorizeMethod.PKI.getCode());
		data.add(OtpAuthorizeMethod.HW.getCode());
		when(accountService.getAuthorizationPolicies(anyString(), anyString())).thenReturn(data);
		String path = "/service/otp/getAuthorizationPolicies?userId=123";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		System.out.println("payload " + objectMapper.writeValueAsString(data));

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<String> realData = (List<String>) response.getData();
		System.out.println("payload of getAuthorizationPolicies : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetQD630Policies() throws Exception {

		initLoginMock();
		List<QD630PoliciesDTO> lstResult = new ArrayList<>();
		String[] arrRegisteredMethod = { "SMSOTP", "HW", "SOFTOTP", "PKI" };
		for (int i = 0; i < arrRegisteredMethod.length; i++) {
			QD630PoliciesDTO dto = new QD630PoliciesDTO();
			if (arrRegisteredMethod[i].equals("SMSOTP")) {
				dto.setFromAmount(20000D);
				dto.setFromAmount(500000D);
			}
			if (arrRegisteredMethod[i].equals("HW")) {
				dto.setFromAmount(50000D);
				dto.setFromAmount(5000000D);
			}
			if (arrRegisteredMethod[i].equals("SOFTOTP")) {
				dto.setFromAmount(0D);
				dto.setFromAmount(500000000D);
			}
			if (arrRegisteredMethod[i].equals("PKI")) {
				dto.setFromAmount(1000000D);
				dto.setFromAmount(5000000000D);
			}

			dto.setRegisteredMethod(arrRegisteredMethod[i]);
			dto.setAllowChangingMethod(true);
			lstResult.add(dto);
		}
		when(accountService.getQD630Policies(anyString(), anyString())).thenReturn(lstResult);
		String path = "/service/otp/getQD630Policies?cif=123";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		System.out.println("payload " + objectMapper.writeValueAsString(lstResult));

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<QD630PoliciesDTO> realData = (List<QD630PoliciesDTO>) response.getData();
		System.out.println("payload of getQD630Policies : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetAuthorizeMethodByAmount() throws Exception {

		initLoginMock();
		// init qd 630

		List<QD630PoliciesDTO> data = new ArrayList<>();
		String[] arrRegisteredMethod = { "SMSOTP", "HW", "SOFTOTP", "PKI" };
		for (int i = 0; i < arrRegisteredMethod.length; i++) {
			QD630PoliciesDTO dto = new QD630PoliciesDTO();
			dto.setFromAmount(i * 10000);
			dto.setToAmount((i + 1) * 10000);
			dto.setRegisteredMethod(arrRegisteredMethod[i]);
			dto.setAllowChangingMethod(true);
			data.add(dto);
		}
		when(accountService.getQD630Policies(anyString(), anyString())).thenReturn(data);
		String mockData = "ABCDEF";
		Double amount = 123D;
		String paymentType = PaymentType.InternalPayment.getPaymentTypeCode();
		when(accountService.getAuthorizeMethodByAmount(anyString(), anyDouble(), anyString())).thenReturn(mockData);
		String path = "/service/otp/getAuthorizeMethodByAmount?paymentType=" + paymentType + "&amount=" + amount;

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		String realData = (String) response.getData();
		System.out.println("payload of getAuthorizeMethodByAmount : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetListPrepaidPhone() throws Exception {

		initLoginMock();
		List<PrepaidPhoneDTO> data = new ArrayList<>();
		String[] phoneNameArr = { "A", "B", "C", "D" };
		for (int i = 0; i < phoneNameArr.length; i++) {
			PrepaidPhoneDTO dto = new PrepaidPhoneDTO();
			dto.setId(i);
			dto.setPhoneName(phoneNameArr[i]);
			dto.setPhoneNumber("0167894567" + i);
			data.add(dto);
		}
		when(accountService.getListPrepaidPhone(anyString())).thenReturn(data);
		String path = "/service/otp/getListPrepaidPhone";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		System.out.println("payload " + objectMapper.writeValueAsString(data));

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<PrepaidPhoneDTO> realData = (List<PrepaidPhoneDTO>) response.getData();
		System.out.println("payload of testGetListPrepaidPhone : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

}
