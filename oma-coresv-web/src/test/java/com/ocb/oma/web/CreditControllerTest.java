package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.CreditDTO;
import com.ocb.oma.dto.InstalmentTransactionHistoryDTO;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.internal.WebCreditService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class CreditControllerTest extends BaseMockTest {

	@MockBean
	@Autowired
	private WebCreditService webCreditService;

	@Test
	public void test_getCredits() throws Exception {

		initLoginMock();

		try {

			int size = 4;

			List<CreditDTO> credits = new ArrayList<>();

			for (int i = 0; i < 4; i++) {
				CreditDTO credit = new CreditDTO();
				credit.setAccountNo("123-" + i);
				credits.add(credit);
			}

			when(webCreditService.getCredits()).thenReturn(credits);

			String path = "/service/credit/getCredits";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(data.size(), size);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getInstalmentTransactionHistory() throws Exception {

		initLoginMock();

		try {

			int size = 4;

			List<InstalmentTransactionHistoryDTO> dtos = new ArrayList<>();

			for (int i = 0; i < 4; i++) {
				InstalmentTransactionHistoryDTO dto = new InstalmentTransactionHistoryDTO();
				dto.setEventDate(new Date());
				dtos.add(dto);
			}

			when(webCreditService.getInstalmentTransactionHistory(anyString(), any(Date.class), any(Date.class),
					any(Integer.class), any(Integer.class), any(Double.class), any(Double.class))).thenReturn(dtos);

			String path = "/service/credit/getInstalmentTransactionHistory?contractNumber=&dateFrom=1544091273782&dateTo=1544091273782&pageNumber=1&pageSize=10&operationAmountFrom=0&operationAmountTo=1000000";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
			MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

			String responseText = result.getResponse().getContentAsString();

			ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

			assertEquals(response.isSuccess(), true);
			assertNotNull(response.getData());
			assertEquals(response.getData() instanceof List, true);
			List data = (List) response.getData();
			assertEquals(data.size(), size);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
