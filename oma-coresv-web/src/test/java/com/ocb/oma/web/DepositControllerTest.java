package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.DepositDTO;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.OwnersDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.BreakDepositInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.AccountCapitalDTO;
import com.ocb.oma.oomni.dto.AccountInterestDTO;
import com.ocb.oma.oomni.dto.AccountSourceDTO;
import com.ocb.oma.oomni.dto.DepositOfferAccountsDTO;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class DepositControllerTest extends BaseMockTest {

	@Test
	public void testBreakDeposit() throws Exception {

		// OTP
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		///
		initLoginMock();
		String depositId = "1234567890123456";
		OutputBaseDTO output = new OutputBaseDTO();
		if (depositId.substring(0, 3).equals("OCB") || depositId.length() == 16) {
			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (depositId.substring(0, 3).equals("VCB")) {
			output.setResultCode("06");
			output.setResultMsg("can_not_find_data");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}
		when(accountService.breakDeposit(anyString(), anyString())).thenReturn(output);
		String path = "/service/deposit/breakDeposit";

		BreakDepositInputDTO input = new BreakDepositInputDTO();
		input.setDepositId("123");
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue1);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of testBreakDeposit : " + objectMapper.writeValueAsString(input));
		assertNotNull(realData);
	}

	@Test
	public void testBreakDeposit_OTP_WRONG() throws Exception {

		// OTP
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		///
		initLoginMock();
		String depositId = "1234567890123456";
		OutputBaseDTO output = new OutputBaseDTO();
		if (depositId.substring(0, 3).equals("OCB") || depositId.length() == 16) {
			output.setResultCode("1");
			output.setResultMsg("Success");
		} else if (depositId.substring(0, 3).equals("VCB")) {
			output.setResultCode("06");
			output.setResultMsg("can_not_find_data");
		} else {
			output.setResultCode("0");
			output.setResultMsg("System Error");
		}
		when(accountService.breakDeposit(anyString(), anyString())).thenReturn(output);
		String path = "/service/deposit/breakDeposit";

		BreakDepositInputDTO input = new BreakDepositInputDTO();
		input.setDepositId("123");
		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue2);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().is5xxServerError()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("payload of testBreakDeposit : " + objectMapper.writeValueAsString(input));
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
	}

	@Test
	public void testGetDeposits() throws Exception {

		initLoginMock();
		List<DepositDTO> lst = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			DepositDTO depositDTO = new DepositDTO();
			depositDTO.setDepositId("78037" + i);
			depositDTO.setDepositName("Tich luy dien tu KHCN");
			depositDTO.setDescription("Description");
			depositDTO.setAccountNo("AccountNo001");
			depositDTO.setCurrency("VND");
			depositDTO.setDepositBalance(50000D + i);
			depositDTO.setNextCapitalizationDate(new Date(1543309380L + i));
			depositDTO.setMaturityDate(new Date(1545757200000L + i));
			depositDTO.setOpeningAccountNo("OpeningAccountNo001");
			depositDTO.setSettlementAccountNo("");
			depositDTO.setSettlementAccountIntNo("AccountId");
			depositDTO.setRenewalOptionType("renewalOptionType");
			depositDTO.setDepositPeriodType("DepositPeriodType");
			depositDTO.setPeriod(1L + i);
			depositDTO.setInterest(5.4 + (23 * i));
			depositDTO.setInterestRateType("interestRateType");
			depositDTO.setTotalInterestAmount(30000D * 13);
			depositDTO.setDepositMigrationName("");
			depositDTO.setDepositMigrationContractNumber("1121234" + i);
			depositDTO.setAutoRolloverChangePos(true);
			depositDTO.setInterestPaymentMethod("InterestPaymentMethod" + i);
			depositDTO.setSubProductCode("TICHLUYDIENTU");
			depositDTO.setOpenDate(new Date(1543165200000L + i));
			depositDTO.setProductType("productType");
			depositDTO.setSurcharge(true);
			depositDTO.setNextSurchargeDate(new Date(1517043780L + i));
			depositDTO.setNextSurchargeAmount(2500000D);
			depositDTO.setLastSurchargeAmount(89700D);

			List<OwnersDTO> ownersList = new ArrayList<>();
			for (int j = 0; j < 5; j++) {
				OwnersDTO ownersDTO = new OwnersDTO();
				ownersDTO.setAddressStreet("Vo Văn Ngân");
				ownersDTO.setAddressStreetPrefix("VVNTD");
				ownersDTO.setApartmentNo("apartmentNo00" + i + 1);
				ownersDTO.setCountry("country0" + i + 1);
				ownersDTO.setCustomerId("customerId00" + 1 + i);
				ownersDTO.setFullName("hoàng nguyên sĩ phú");
				ownersDTO.setHouseNo("HS0" + i + 3 * i);
				ownersDTO.setName("0367685656");
				ownersDTO.setPostCode("TayHoa");
				ownersDTO.setRelationType("relationType");
				ownersDTO.setResidence("residence");
				ownersDTO.setTown("town" + i + 1);
				ownersList.add(ownersDTO);
			}
			depositDTO.setOwnersList(ownersList);
			depositDTO.setUnfinishedDisposition(true);
			depositDTO.setTypeCapableToBreak(true);
			depositDTO.setLastTransactionBookingDate("1606528354");
			depositDTO.setDepositContractNumber("003760004037500" + i);
			depositDTO.setCustomName("Ten So TK " + i);
			depositDTO.setCustomIcon("DEPOSIT@78037@1731994001731994");
			depositDTO.setInstructionOnDispensation("instructionOnDispensation");
			depositDTO.setPeriodFrequency("periodFrequency");
			depositDTO.setDateFirstExecution(new Date(1606528354l));
			depositDTO.setWorkingAvailableFunds(123000D * i);
			depositDTO.setPrematureInterestRate(1000D + (i + 1));
			depositDTO.setIbRedeemDepositPos(false);
			depositDTO.setStatus("CUR");
			depositDTO.setDepositType("Online");
			depositDTO.setSurcharge(false);
			lst.add(depositDTO);
		}
		when(accountService.getDeposits(anyString())).thenReturn(lst);
		String path = "/service/deposit/getDeposits";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<DepositDTO> realData = (List<DepositDTO>) response.getData();
		System.out.println("payload of testBreakDeposit : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void testGetDepositOfferAcc() throws Exception {

		initLoginMock();
		DepositOfferAccountsDTO accountsDTO = new DepositOfferAccountsDTO();
		List<AccountSourceDTO> accountSourceList = new ArrayList<>();
		for (int i = 1; i <= 3; i++) {
			AccountSourceDTO accountSourceDTO = new AccountSourceDTO();
			accountSourceDTO.setAccountId("4" + i);
			accountSourceDTO.setAccessibleAssets(1000000D * i);
			accountSourceDTO.setProductType("ACCOUNT");
			accountSourceDTO.setAccountName("TG Th.toan ATM");
			accountSourceDTO.setAccountNo("0037100003605005");
			accountSourceDTO.setAccessibleAssets(9480600684D);
			accountSourceDTO.setCurrentBalance(9480600684D);
			accountSourceDTO.setAllowedLimit(0D + i);
			accountSourceDTO.setAllowedLimitCurrency("USD");
			accountSourceDTO.setDescription("description");
			accountSourceDTO.setOpenDate(null);
			accountSourceDTO.setBlockAssets(null);
			accountSourceDTO.setArrearAmount(null);
			accountSourceDTO.setOwnerName("ownerName");
			accountSourceDTO.setAccountInterest(0D + i);
			accountSourceDTO.setCreditInterest(null);
			accountSourceDTO.setCurrency("VND");
			accountSourceDTO.setSettlementPeriodDtType(null);
			accountSourceDTO.setSettlementPeriodCtType("settlementPeriodCtType");
			accountSourceDTO.setSettlementPeriodDt(1000 * i);
			accountSourceDTO.setSettlementPeriodCt(1000 + i);
			accountSourceDTO.setNextCapitalizationDtDate(new Date());
			accountSourceDTO.setNextCapitalizationCtDate(new Date());
			accountSourceDTO.setLastTransactionBookingDate(new Date());
			accountSourceDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountSourceDTO.setOwnersList(ownersList);
			accountSourceDTO.setCategory(i + 1004);
			accountSourceDTO.setSubProduct("1004");
			accountSourceDTO.setRelation("OWNER");
			accountSourceDTO.setStatementDistributionType("statementDistributionType");
			String[] actions = {};
			accountSourceDTO.setActions(actions);

			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountSourceDTO.setAccountCategories(accountCategories);
			accountSourceDTO.setAccountRestrictFlag(false);
			String[] destAccountRestrictions = {};
			accountSourceDTO.setDestAccountRestrictions(destAccountRestrictions);
			accountSourceDTO.setOwnerContext("DETAL");
			accountSourceDTO.setAccountModifyStatus(null);
			accountSourceDTO.setExecutiveRestriction(false);
			accountSourceDTO.setOwnerGlobusId(801004000801004L);
			accountSourceDTO.setOpenBranch("Tan Binh " + i);
			accountSourceDTO.setCustomName("ACCOUNT801004");
			accountSourceDTO.setCustomIcon("");

			accountSourceList.add(accountSourceDTO);
		}
		accountsDTO.setAccountSourceList(accountSourceList);

		List<AccountCapitalDTO> accountCapitalList = new ArrayList<>();
		for (int i = 1; i <= 4; i++) {
			AccountCapitalDTO accountCapitalDTO = new AccountCapitalDTO();
			accountCapitalDTO.setAccountId("4" + i);
			accountCapitalDTO.setProductType("ACCOUNT");
			accountCapitalDTO.setAccountName("003710000360500" + i);
			accountCapitalDTO.setAccessibleAssets(9480600684D);
			accountCapitalDTO.setCurrentBalance(9480600684D);
			accountCapitalDTO.setAllowedLimit(null);
			accountCapitalDTO.setAllowedLimitCurrency("");
			accountCapitalDTO.setDescription("description");
			accountCapitalDTO.setOpenDate(null);
			accountCapitalDTO.setBlockAssets(null);
			accountCapitalDTO.setArrearAmount(null);
			accountCapitalDTO.setOwnerName(null);
			accountCapitalDTO.setAccountInterest(0D);
			accountCapitalDTO.setCreditInterest(null);
			accountCapitalDTO.setCurrency("NDT");
			accountCapitalDTO.setSettlementPeriodDtType(null);
			accountCapitalDTO.setSettlementPeriodCtType(null);
			accountCapitalDTO.setSettlementPeriodDt(0);
			accountCapitalDTO.setSettlementPeriodCt(i);
			accountCapitalDTO.setNextCapitalizationDtDate(null);
			accountCapitalDTO.setNextCapitalizationCtDate(null);
			accountCapitalDTO.setLastTransactionBookingDate(null);
			accountCapitalDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountCapitalDTO.setOwnersList(ownersList);
			accountCapitalDTO.setCategory(1004);
			accountCapitalDTO.setSubProduct("1004");
			accountCapitalDTO.setRelation("OWNER");
			accountCapitalDTO.setStatementDistributionType(null);
			accountCapitalDTO.setActions(null);
			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountCapitalDTO.setAccountCategories(accountCategories);
			accountCapitalDTO.setAccountRestrictFlag(false);
			accountCapitalDTO.setDestAccountRestrictions(null);
			accountCapitalDTO.setOwnerContext("DETAIL");
			accountCapitalDTO.setAccountModifyStatus(null);
			accountCapitalDTO.setExecutiveRestriction(false);
			accountCapitalDTO.setOwnerGlobusId(801004000801004L + i);
			accountCapitalDTO.setOpenBranch("TAn Binh " + i);
			accountCapitalDTO.setCustomName("ACCOUNT801004");
			accountCapitalDTO.setCustomIcon("");

			accountCapitalList.add(accountCapitalDTO);
		}
		accountsDTO.setAccountCapitalList(accountCapitalList);

		List<AccountInterestDTO> accountInterestList = new ArrayList<>();
		for (int i = 1; i <= 4; i++) {
			AccountInterestDTO accountInterestDTO = new AccountInterestDTO();
			accountInterestDTO.setAccountId("4" + i);
			accountInterestDTO.setProductType("ACCOUNT");
			accountInterestDTO.setAccountName("003710000360500" + i);
			accountInterestDTO.setAccessibleAssets(9480600684D);
			accountInterestDTO.setCurrentBalance(9480600684D);
			accountInterestDTO.setAllowedLimit(null);
			accountInterestDTO.setAllowedLimitCurrency("");
			accountInterestDTO.setDescription("description");
			accountInterestDTO.setOpenDate(null);
			accountInterestDTO.setBlockAssets(null);
			accountInterestDTO.setArrearAmount(null);
			accountInterestDTO.setOwnerName(null);
			accountInterestDTO.setAccountInterest(0D);
			accountInterestDTO.setCreditInterest(null);
			accountInterestDTO.setCurrency("EUR");
			accountInterestDTO.setSettlementPeriodDtType(null);
			accountInterestDTO.setSettlementPeriodCtType(null);
			accountInterestDTO.setSettlementPeriodDt(0);
			accountInterestDTO.setSettlementPeriodCt(i);
			accountInterestDTO.setNextCapitalizationDtDate(null);
			accountInterestDTO.setNextCapitalizationCtDate(null);
			accountInterestDTO.setLastTransactionBookingDate(null);
			accountInterestDTO.setAccountStatementDetails(null);
			List<OwnersDTO> ownersList = new ArrayList<>();
			String[] fullNameArr = { "GB NAME 1-801004", "GB NAME 1-801067" };
			for (int j = 0; j < fullNameArr.length; j++) {
				OwnersDTO dto = new OwnersDTO();
				dto.setCustomerId("80100400080100" + i);
				dto.setRelationType("OWNER");
				dto.setName(null);
				dto.setAddressStreetPrefix("addressStreetPrefix");
				dto.setAddressStreet("addressStreet");
				dto.setHouseNo("HNo" + j + i);
				dto.setApartmentNo("ANo" + j);
				dto.setPostCode("PCode" + j);
				dto.setTown("Tp.Ho Chi Minh");
				dto.setCountry("Viet nam");
				dto.setFullName(fullNameArr[j]);
				dto.setResidence("Viet Nam");
				dto.setCurrent(true);
				ownersList.add(dto);
			}
			accountInterestDTO.setOwnersList(ownersList);
			accountInterestDTO.setCategory(1004);
			accountInterestDTO.setSubProduct("1004");
			accountInterestDTO.setRelation("OWNER");
			accountInterestDTO.setStatementDistributionType(null);
			accountInterestDTO.setActions(null);
			String[] accountCategories = { "ACCOUNT_HISTORY_FROM_LIST", "MAIN_ACCOUNT_LIST", "ACCOUNT_MAIN_LIST",
					"INTERNAL_TRANSFER_FROM_LIST", "DEPOSIT_OPEN_INTEREST_LIST", "TRANSFER_OWN_FROM_LIST",
					"TRANSFER_FROM_LIST", "TRANSFER_US_FROM_LIST", "BASIC_ACCOUNT_LIST", "CARD_REPAYMENT_FROM_LIST",
					"DEPOSIT_OPEN_CAPITAL_LIST", "TRANSFER_ZUS_FROM_LIST", "CARD_AUTO_REPAYMENT_FROM_LIST",
					"BILL_PAYMENT_FROM_LIST", "TRANSFER_BLX_FROM_LIST", "LOAN_REPAYMENT_FROM_LIST",
					"BENEFICIARY_CREATE_FROM_LIST", "CURRENCY_SELL_FROM_LIST", "STANDING_ORDER_CREATE_FROM_LIST",
					"TRANSFER_OWN_TO_LIST", "TRANSFER_OTHER_FROM_LIST", "EXTERNAL_TRANSFER_FROM_LIST",
					"ACCOUNT_UNCLEARED_FROM_LIST", "CURRENCY_SELL_TO_LIST", "MOBILE_TOPUP_FROM_LIST",
					"CURRENCY_BUY_TO_LIST", "DEPOSIT_OPEN_FROM_LIST", "CURRENCY_BUY_FROM_LIST",
					"TRANSFER_INVOOBILL_FROM_LIST", "FAST_INTERBANK_TRANSFER_FROM_LIST" };
			accountInterestDTO.setAccountCategories(accountCategories);
			accountInterestDTO.setAccountRestrictFlag(false);
			accountInterestDTO.setDestAccountRestrictions(null);
			accountInterestDTO.setOwnerContext("DETAIL");
			accountInterestDTO.setAccountModifyStatus(null);
			accountInterestDTO.setExecutiveRestriction(false);
			accountInterestDTO.setOwnerGlobusId(801004000801004L + i);
			accountInterestDTO.setOpenBranch("TAn Binh " + i);
			accountInterestDTO.setCustomName("ACCOUNT801004");
			accountInterestDTO.setCustomIcon("");

			accountInterestList.add(accountInterestDTO);
		}
		accountsDTO.setAccountInterestList(accountInterestList);

		when(accountService.getDepositOfferAcc(anyString())).thenReturn(accountsDTO);
		String path = "/service/deposit/getDepositOfferAcc";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		DepositOfferAccountsDTO realData = objectMapper.convertValue(response.getData(), DepositOfferAccountsDTO.class);
		System.out.println("payload of testGetDepositOfferAcc : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void test_getDeposit() throws Exception {

		initLoginMock();

		String jsonInput = "{\"depositId\":\"1409\",\"depositName\":null,\"description\":null,\"accountNo\":null,\"currency\":\"VND\",\"depositBalance\":10000000,\"nextCapitalizationDate\":null,\"maturityDate\":1557421200000,\"openingAccountNo\":\"0100100003486003\",\"settlementAccountNo\":\"0100100003486003\",\"settlementAccountIntNo\":\"0100100003486003\",\"renewalOptionType\":\"RENEWAL_WITH_INTEREST\",\"depositPeriodType\":null,\"period\":null,\"interest\":7.1,\"interestRateType\":\"FIXED\",\"totalInterestAmount\":350136.986301369863013698630136986301369,\"depositMigrationName\":null,\"depositMigrationContractNumber\":\"\",\"autoRolloverChangePos\":null,\"interestPaymentMethod\":null,\"category\":6704,\"subProductCode\":\"TICHLUYDIENTU\",\"openDate\":1541782800000,\"productType\":null,\"isSurcharge\":null,\"nextSurchargeDate\":null,\"nextSurchargeAmount\":null,\"lastSurchargeAmount\":null,\"ownersList\":[{\"customerId\":\"801003000801003\",\"relationType\":\"OWNER\",\"name\":null,\"addressStreetPrefix\":null,\"addressStreet\":\"WARD.2-801003, Q. 2\",\"houseNo\":null,\"apartmentNo\":null,\"postCode\":\"\",\"town\":\"Tp.Ho Chi Minh\",\"country\":null,\"fullName\":\"GB NAME 1-801003\",\"residence\":null}],\"unfinishedDisposition\":null,\"typeCapableToBreak\":true,\"lastTransactionBookingDate\":null,\"depositContractNumber\":\"0001600004155007\",\"customName\":null,\"customIcon\":null,\"instructionOnDispensation\":\"\",\"periodFrequency\":\"X\",\"dateFirstExecution\":1541782800000,\"workingAvailableFunds\":875269564,\"prematureInterestRate\":0,\"ibRedeemDepositPos\":true,\"status\":null,\"depositType\":null,\"surcharge\":null,\"_links\":{\"self\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409\"},\"deposit_download\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/downloads/deposit_download.json\"},\"realize_context_deposit\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/actions/realize_context_deposit.json\"},\"break_deposit\":{\"href\":\"http://10.96.23.146:9080/cbp-ocb-service/api/deposit/1409/actions/break_deposit.json\"}}}";
		try {
			DepositDTO depositDTO = objectMapper.readValue(jsonInput, new TypeReference<DepositDTO>() {
			});
			when(accountService.getDeposit(anyString(), anyString())).thenReturn(depositDTO);
			String path = "/service/deposit/getDeposit?depositId=1409";

			MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
			MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
					.andExpect(status().isOk()).andReturn();

			ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
			Map data = (Map) response.getData();
			System.out.println("payload of test_getDeposit : " + objectMapper.writeValueAsString(response));
			assertNotNull(data);
			assertEquals(data.get("depositId"), depositDTO.getDepositId());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
