/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.util.ApplicationContextHolder;

/**
 * @author Phu Hoang
 *
 */
public class BaseTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;
	// protected String defaultUser = "admin";
	// protected String defaultPassword = "password";
	protected String defaultUser = "ibtest10";
	protected String defaultPassword = "Ocb@1234";
	protected String defaultDeviceId = "65d32952f937475b";
	protected String ocpTokenDefault = "TokenFromLoginService123";

	@Autowired
	protected ApplicationContext appConext;

	@Before
	public void init() {
		ApplicationContextHolder.setApplicationContext(appConext);
		MockitoAnnotations.initMocks(this);
	}

	public String login(String username, String password) throws JsonProcessingException, Exception {

		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", username);
		payload.put("password", password);
		payload.put("deviceId", defaultDeviceId);
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		UserToken userToken = objectMapper.convertValue(response.getData(), UserToken.class);
		assertNotNull(userToken.getToken());
		assertNotNull(userToken.getOldAuthen());
		assertNotNull(userToken.getOldAuthen().getAuthorizationMethod());
		assertNotNull(userToken.getOldAuthen().getJwtToken());
		assertNotNull(userToken.getOldAuthen().getCustomerId());
		return userToken.getToken();
		// Map<String, Object> dataMap = (Map<String, Object>) response.getData();
		//
		// return (String) dataMap.get("token");
	}

	protected UserToken loginToken(String username, String password) throws JsonProcessingException, Exception {

		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", username);
		payload.put("password", password);
		payload.put("deviceId", defaultDeviceId);
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		UserToken userToken = objectMapper.convertValue(response.getData(), UserToken.class);
		assertNotNull(userToken.getToken());
		assertNotNull(userToken.getOldAuthen());
		assertNotNull(userToken.getOldAuthen().getAuthorizationMethod());
		assertNotNull(userToken.getOldAuthen().getJwtToken());
		assertNotNull(userToken.getOldAuthen().getCustomerId());
		return userToken;
		// Map<String, Object> dataMap = (Map<String, Object>) response.getData();
		//
		// return (String) dataMap.get("token");
	}

	protected String login() throws JsonProcessingException, Exception {

		return login(defaultUser, defaultPassword);
	}

	public MockHttpServletRequestBuilder processGetAuth(String path, String... cridentials) throws Exception {
		String username = null, password = null;
		if (cridentials == null || cridentials.length < 2) {
			username = defaultUser;
			password = defaultPassword;
		}
		String token = login(username, password);
		return MockMvcRequestBuilders.get(path).header(HttpHeaders.AUTHORIZATION, "Bearer " + token);

	}

	public MockHttpServletRequestBuilder processPostAuth(String path, String... cridentials) throws Exception {
		String username = null, password = null;
		if (cridentials == null || cridentials.length < 2) {
			username = defaultUser;
			password = defaultPassword;
		} else {
			username = cridentials[0];
			password = cridentials[1];
		}
		String token = login(username, password);
		System.out.println("*****Login Token " + token);
		return MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, "Bearer " + token);

	}
}
