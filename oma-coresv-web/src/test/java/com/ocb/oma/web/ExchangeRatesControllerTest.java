package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.ExchangeRateDTO;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ExchangeRatesControllerTest extends BaseMockTest {

	@SuppressWarnings("rawtypes")
	@Test
	public void test_getExchangeRates() throws Exception {

		initLoginMock();

		int size = 4;

		List<ExchangeRateDTO> exchangeRates = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			ExchangeRateDTO exchangeRate = new ExchangeRateDTO();
			exchangeRate.setAverageRate(BigDecimal.valueOf(i));
			exchangeRates.add(exchangeRate);
		}

		when(accountService.getExchangeRates(anyString())).thenReturn(exchangeRates);

		String path = "/service/exchangeRates/getExchangeRates";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

		assertEquals(response.isSuccess(), true);
		assertNotNull(response.getData());
		assertEquals(response.getData() instanceof List, true);
		List data = (List) response.getData();
		assertEquals(size, data.size());
	}
}
