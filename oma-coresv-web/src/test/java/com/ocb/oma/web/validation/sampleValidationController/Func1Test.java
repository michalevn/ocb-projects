package com.ocb.oma.web.validation.sampleValidationController;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;
import com.ocb.oma.web.BaseMockTest;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class Func1Test extends BaseMockTest {

	@MockBean
	@Autowired
	protected AccountServiceExt accountService;

	@Before
	public void doLogin() {
		UserToken userToken = new UserToken();
		userToken.setUsername(defaultUser);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801004";
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, "SMSOTP", "09763652512");
		userToken.setOldAuthen(oldOmni);

		when(accountService.checkLogin(anyString(), anyString(), anyString())).thenReturn(userToken);
	}

	@Test
	public void test_ok() throws Exception {

		String path = "/service/sample-validations/validate-get-params";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");

		requestBuilder.param("email", "docv.hcm@gmail.com");
		requestBuilder.param("name", "Đô Cao 2018");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		System.out.println("responseText:" + responseText);

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

		assertEquals(response.isSuccess(), true);
	}

	@Test
	public void test_email_invalid() throws Exception {

		String path = "/service/sample-validations/validate-get-params";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");

		requestBuilder.param("email", "docv.hcm"); // invalid email
		requestBuilder.param("name", "Đô Cao 2018");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
				.andReturn();

		String responseText = result.getResponse().getContentAsString();

		System.out.println("responseText:" + responseText);

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

		assertEquals(response.isSuccess(), false);
		assertEquals(response.getErrorCode(), "EMAIL_INVALID");
	}
	
	@Test
	public void test_name_invalid() throws Exception {

		String path = "/service/sample-validations/validate-get-params";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json");

		requestBuilder.param("email", "docv.hcm@gmail.com"); // invalid email
		requestBuilder.param("name", "Đô Cao");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
				.andReturn();

		String responseText = result.getResponse().getContentAsString();

		System.out.println("responseText:" + responseText);

		ResponseDTO response = objectMapper.readValue(responseText, ResponseDTO.class);

		assertEquals(response.isSuccess(), false);
		assertEquals(response.getErrorCode(), "NAME_LENGTH_INVALID");
	}
}
