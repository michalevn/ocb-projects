package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.AddRecipientOutputDTO;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.RecipientDTO;
import com.ocb.oma.dto.RecipientInputDTO;
import com.ocb.oma.dto.RecipientType;
import com.ocb.oma.web.controller.WebAccountController;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class RecipientControllerTest extends BaseMockTest {

	WebAccountController controller;

	private List<RecipientDTO> initRecipientMock() {
		List<RecipientDTO> lst = new ArrayList<>();

		for (int i = 0; i < 2; i++) {
			String recipientId = "942";
			String recipientName = RandomStringUtils.randomAlphabetic(10);
			String accountNo = RandomStringUtils.randomAlphanumeric(10);
			String bankName = RandomStringUtils.randomAlphabetic(4);
			String bankCode = "970406";
			String paymentType = PaymentType.FastTransfer.getPaymentTypeCode();
			String provinceId = RandomStringUtils.randomAlphabetic(10);
			String branchCode = RandomStringUtils.randomAlphabetic(10);
			RecipientDTO dto = new RecipientDTO(recipientId, recipientName, accountNo, bankName, bankCode, paymentType,
					provinceId, branchCode);
			lst.add(dto);
		}
		return lst;
	}

	@Test
	public void testGetRecipientList() throws Exception {
		initLoginMock();
		List<RecipientDTO> mockResponse = initRecipientMock();
		BankInfoDTO bankInfo = new BankInfoDTO();
		bankInfo.setBankCode("070406");
		bankInfo.setBankName("Dong A Bank");
		when(resourceService.getBankByCodeNapas(anyString())).thenReturn(bankInfo);
		when(accountService.getRecipientList(anyString(), anyInt(), anyString())).thenReturn(mockResponse);
		String path = "/service/recipient/getRecipientList?filerTemplateType=EXTERNAL&pageSize=10";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testGetRecipientList : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		List<RecipientDTO> lst = (List<RecipientDTO>) response.getData();
		System.out.println("*** result of testGetRecipientList " + objectMapper.writeValueAsString(lst));
		assertEquals(mockResponse.size(), lst.size());

	}

	@Test
	public void testDeleteRecipient() throws Exception {
		initLoginMock();
		when(accountService.deleteRecipient(ArgumentMatchers.any(), anyString())).thenReturn(true);
		RecipientDTO data = new RecipientDTO();
		data.setRecipientId("1");
		String recipienName = "Hoang thanh Thuy";
		data.setRecipientName(recipienName);
		data.setToAccount("0039393828");
		data.setBankCode("JSGJJ");
		String path = "/service/recipient/deleteRecipient";
		System.out.println("payload of testDeleteRecipient" + objectMapper.writeValueAsString(data));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(data))).andExpect(status().isOk()).andReturn();
		System.out.println("testDeleteRecipient : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
		Boolean delCheck = (Boolean) response.getData();
		assertNotNull(delCheck);

	}

	@Test
	public void testAddRecipient() throws Exception {
		initLoginMock();
		AddRecipientOutputDTO mockData = new AddRecipientOutputDTO();
		mockData.setResultCode("1");
		mockData.setResultMsg("Thanh cong");
		when(accountService.addRecipient(ArgumentMatchers.any(), anyString())).thenReturn(mockData);

		RecipientInputDTO dtoInput = new RecipientInputDTO();
		dtoInput.setRecipientType(RecipientType.EXTERNAL.getRecipientTypeCode());
		dtoInput.setShortName("Ha Minh Lam");
		dtoInput.setAccountId("lam2008");
		dtoInput.setRemarks("Chuyen khoan thoi noi");
		dtoInput.setBeneficiary("24, Truong Tho, Thu Duc");
		dtoInput.setCreditAccount("08897443976434");
		dtoInput.setProvince("0024");
		dtoInput.setBankCode("OCB");
		dtoInput.setBranchCode("DEF");
		dtoInput.setCardNumber("220099");

		String path = "/service/recipient/addRecipient";
		System.out.println("***payload of testpostaddRecipient " + objectMapper.writeValueAsString(dtoInput));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(dtoInput))).andExpect(status().isOk()).andReturn();
		System.out.println("***testpostaddRecipient : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

}
