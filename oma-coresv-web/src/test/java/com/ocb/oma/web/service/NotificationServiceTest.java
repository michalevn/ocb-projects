package com.ocb.oma.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.web.BaseMockTest;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.service.external.ResourceServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class NotificationServiceTest extends BaseMockTest {

	@Autowired
	ResourceServiceExt resourceServiceExt;
	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void testInsertNotification() throws Exception {
		OmniNotification message = new OmniNotification();
		message.setBodyVn("som test");
		message.setUserId("admin");
		message.setCreatedBy("phuhns");
		message.setType("type1");
		message.setDistributedType(3);
		System.out.println(objectMapper.writeValueAsString(message));
		resourceServiceExt.pushNotification(message);
	}

}
