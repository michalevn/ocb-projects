package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.HttpServerErrorException;

import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OmaExceptionDTO;
import com.ocb.oma.dto.UserToken;
import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;
import com.ocb.oma.web.dto.ResponseDTO;
import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.internal.EncryptDataService;
import com.ocb.oma.web.service.internal.TokenManagementService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "dev")
public class ServiceTest extends BaseMockTest {

	@MockBean
	protected AccountServiceExt accountService;

	@Autowired
	protected TokenManagementService tokenManagementService;

	@Test
	public void testLoginOk() throws Exception {
		initLoginMockOk();
		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", this.defaultUser);
		payload.put("password", defaultPassword);
		payload.put("deviceId", "xxx-xxx-xxx");
		payload.put("deviceToken", "xxx-xxx-xxx");
		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
				.content(objectMapper.writeValueAsString(payload))).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		Map<String, Object> dataMap = (Map<String, Object>) response.getData();

		assertNotNull(dataMap);
		assertNotNull(dataMap.get("token"));
		assertEquals(defaultUser, dataMap.get("username"));
		System.out.println(response);
		// UserToken userToken = objectmap

	}

	protected void initLoginMockFail() {
		UserToken tokenMock = null;
		// tokenMock.setUsername(defaultUser);
		when(accountService.checkLogin(anyString(), anyString(), anyString())).thenReturn(tokenMock);
	}

	protected void initLoginMockOk() {
		UserToken userToken = new UserToken();
		userToken.setUsername(defaultUser);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801004";
		;
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, "SMSOTP", "09763652512");
		userToken.setOldAuthen(oldOmni);

		when(accountService.checkLogin(anyString(), anyString(), anyString())).thenReturn(userToken);
	}

	@Test
	public void testLoginFailure() throws Exception {
		initLoginMockFail();
		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", "userwrong");
		payload.put("password", defaultPassword);
		payload.put("deviceId", "xxx-xxx-xxx");
		MvcResult result = mvc
				.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
						.content(objectMapper.writeValueAsString(payload)))
				.andExpect(status().is5xxServerError()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.LOGIN_FAIL_CODE, response.getErrorCode());

	}

	// @Test
	public void testLogoutSuccess() throws Exception {
		initLoginMockOk();
		// accountService = mock(classToMock)
		// when(accountService.doTest(name)).thenReturn("hello " + name);
		String path = "/logout/";
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		System.out.println("testLogoutSuccess : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());

	}

	@Test
	public void testLogoutFailure() throws Exception {
		initLoginMockOk();
		// accountService = mock(classToMock)
		// when(accountService.doTest(name)).thenReturn("hello " + name);
		String path = "/logout/";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is4xxClientError()).andReturn();
		System.out.println("testLogoutSuccess : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.ACCESS_FORBIDDEN, response.getErrorCode());

	}

	//@Test  // hien logout ko co invalid token
	public void testInvalidToken_AfterLogout() throws Exception {
		initLoginMockOk();
		String token = login();

		String path = "/logoutService";
		// MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, "Bearer "
		// + token);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(true, response.isSuccess());

		List<AccountInfo> accountInfos = new ArrayList<>();
		when(accountService.getAccountList(anyString(), anyString())).thenReturn(accountInfos);
		
		path = "/service/account/getAccountList";
		requestBuilder = MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		result = mvc.perform(requestBuilder).andExpect(status().is4xxClientError()).andReturn();
		response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);

		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.INVALID_TOKEN, response.getErrorCode());
		System.out.println("testInvalidToken : " + result.getResponse().getContentAsString());

	}

	@Test
	public void testInvalidToken() throws Exception {

		String token = "something";

		String path = "/logout/";
		// MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, "Bearer "
		// + token);
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		MvcResult result;
		ResponseDTO response;

		// path = "/service/something";
		requestBuilder = MockMvcRequestBuilders.post(path).header(HttpHeaders.AUTHORIZATION, "Bearer " + token);
		result = mvc.perform(requestBuilder).andExpect(status().is4xxClientError()).andReturn();
		response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.ACCESS_FORBIDDEN, response.getErrorCode());
		System.out.println("testInvalidToken : " + result.getResponse().getContentAsString());

	}

	@Test
	public void testResponseWithMissingParameter() throws Exception {
		initLoginMockOk();
		;
		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", "userwrong");
		// payload.put("password", defaultPassword);
		MvcResult result = mvc
				.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
						.content(objectMapper.writeValueAsString(payload)))
				.andExpect(status().is5xxServerError()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.MISSING_PARAMETER, response.getErrorCode());

		// missing imeicodr
		payload.put("username", "userwrong");
		payload.put("password", defaultPassword);
		payload.remove("deviceId");
		result = mvc
				.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
						.content(objectMapper.writeValueAsString(payload)))
				.andExpect(status().is5xxServerError()).andReturn();
		response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.MISSING_PARAMETER, response.getErrorCode());
	}

	@Test
	public void testLoginFailureThreeTime() throws Exception {
		initLoginMockFail();
		UserToken tokenMock = null;
		// tokenMock.setUsername(defaultUser);
		OmaExceptionDTO exceptionDtO = new OmaExceptionDTO();
		exceptionDtO.setErrorCode(MessageConstant.ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL);

		HttpServerErrorException error = new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, null,
				objectMapper.writeValueAsBytes(exceptionDtO), null);
		// tokenMock.setUsername(defaultUser);
		when(accountService.checkLogin(anyString(), anyString(), anyString())).thenThrow(error);
		String path = "/loginService";
		// login
		// MvcResult result =
		// mvc.perform(MockMvcRequestBuilders.get(path)).andExpect(status().isOk()).andReturn();
		Map<String, String> payload = new HashMap<>();
		payload.put("username", "userwrong");
		payload.put("password", defaultPassword);
		payload.put("deviceId", "xxx-xxx-xxx");
		MvcResult result = mvc
				.perform(MockMvcRequestBuilders.post(path).contentType("application/json")
						.content(objectMapper.writeValueAsString(payload)))
				.andExpect(status().is5xxServerError()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL, response.getErrorCode());

	}

	@Test
	public void testReloginOk() throws Exception {
		// loginByEncryptData
		initLoginMockOk();
		// UserToken loginToken = loginToken(defaultUser, defaultPassword);
		// System.out.println("***login " +
		// objectMapper.writeValueAsString(loginToken));
		// String encryptPassword = loginToken.getHashedData();
		String path = "/service/account/loginByToken";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		UserToken userToken = objectMapper.convertValue(response.getData(), UserToken.class);
		assertNotNull(userToken.getToken());
		assertNotNull(userToken.getOldAuthen());
		assertNotNull(userToken.getOldAuthen().getAuthorizationMethod());
		assertNotNull(userToken.getOldAuthen().getJwtToken());
		assertNotNull(userToken.getOldAuthen().getCustomerId());
	}

	@Autowired
	EncryptDataService encryptService;

//	@Test
	public void testReloginWhenPasswordChanged() throws Exception {
		String username = "admin";
		String password = "123";
		UserToken tokenMock = null;
		UserToken userToken = new UserToken();
		userToken.setUsername(defaultUser);
		String jwtToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOltdLCJpZGVudGl0eSI6eyJvcmdfdW5pdCI6IiIsImFjY2Vzc19wcm9maWxlX2lkIjoiZ2ZkNDM1OSIsInJvbGUiOiJDVVNUT01FUiIsIm1lbWJlcl9ncm91cHMiOlsiUk9MRV9BRE1JTiJdLCJsYXN0X25hbWUiOiJpYnRlc3Q5IiwibG9naW4iOiJpYnRlc3Q5IiwiZmlyc3RfbmFtZSI6IklIQ0RMR1lUQlEiLCJwZXJzb25faWQiOiI4MDEwMDQifSwidXNlcl9uYW1lIjoiaWJ0ZXN0OSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJpc3MiOiIiLCJjbGllbnRfc2Vzc2lvbl9pZCI6IjIxMzEyMzEyMzEyMyIsImV4cCI6MTUzNjczNDk4NzQ0MSwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImp0aSI6IjhhNmNmYjVjLTdlZWMtNGNkNC1hZGIzLWFmZDk";
		String customerId = "801004";
		;
		String firstName = "Tran";
		String lastName = "Nguyen";
		Date lastLoginDate = new Date();
		OldAuthenticateResponseDTO oldOmni = new OldAuthenticateResponseDTO(true, jwtToken, customerId, firstName,
				lastName, lastLoginDate, "SMSOTP", "09763652512");
		userToken.setOldAuthen(oldOmni);

		// gia lap
		when(accountService.checkLogin(ArgumentMatchers.eq(username), ArgumentMatchers.eq(password), anyString()))
				.thenReturn(userToken);
		String path = "/service/account/loginByToken";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path, username, password);
		when(accountService.checkLogin(ArgumentMatchers.eq(username), ArgumentMatchers.eq(password), anyString()))
				.thenReturn(null);
		MvcResult result = mvc.perform(requestBuilder.contentType("application/json"))
				.andExpect(status().is5xxServerError()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		// UserToken userToken = objectMapper.convertValue(response.getData(),
		// UserToken.class);
		assertEquals(false, response.isSuccess());
		assertEquals(MessageConstant.LOGIN_FAIL_CODE, response.getErrorCode());

	}
}
