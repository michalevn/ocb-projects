/**
 *@author  Phu Hoang
 */
package com.ocb.oma.web.external;

import org.springframework.beans.factory.annotation.Autowired;

import com.ocb.oma.web.service.external.AccountServiceExt;
import com.ocb.oma.web.service.external.ResourceServiceExt;

/**
 * @author Phu Hoang
 *
 */
public class BaseTest extends com.ocb.oma.web.BaseTest {

	@Autowired
	protected AccountServiceExt accountService;

	@Autowired
	protected ResourceServiceExt resourceService;

}
