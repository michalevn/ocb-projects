package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WebLimitControllerTest extends BaseTest {

	@Test
	public void test_get() throws Exception {
		
		String path = "/service/webLimit/get";
		
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder.contentType(MediaType.APPLICATION_JSON_UTF8);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		System.out.println("===> test_get: " + responseContent);
		ResponseDTO response = objectMapper.readValue(responseContent, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}
}
