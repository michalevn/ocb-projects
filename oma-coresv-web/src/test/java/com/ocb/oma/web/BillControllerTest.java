package com.ocb.oma.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OtpAuthorizeMethod;
import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.input.AuthorizationResponse;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.FrequencyDTO;
import com.ocb.oma.oomni.dto.StandingOrderDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class BillControllerTest extends BaseMockTest {

	@Test
	public void testDeleteAutoBillTransfer() throws Exception {

		initLoginMock();
		String path = "/service/bill/deleteAutoBillTransfer";
		DeleteAutoBillTransferOutputDTO dto = new DeleteAutoBillTransferOutputDTO();
		dto.setStatus("EXECUTED");
		dto.setAuthErrorCause(null);
		dto.setReferenceId("4224");
		dto.setCoreRefNum2(null);
		dto.setErrorMsg(null);
		dto.setSuccess(true);

		String orderNumber = "4054";
		Map<String, String> params = new HashMap<>();
		params.put("orderNumber", orderNumber);

		when(accountService.deleteAutoBillTransfer(anyString(), ArgumentMatchers.any())).thenReturn(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(params))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		DeleteAutoBillTransferOutputDTO realData = objectMapper.convertValue(response.getData(),
				DeleteAutoBillTransferOutputDTO.class);
		System.out.println("payload of deleteAutoBillTransfer : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void testmodifyAutoBillTransferDirectly() throws Exception {

		initLoginMock();
		String path = "/service/bill/modifyAutoBillTransferDirectly";

		TransferAutoBillDTO input = new TransferAutoBillDTO();
		input.setOrderNumber("4054");
		input.setFrequencyPeriodCount(1);
		input.setFrequencyPeriodUnit("");
		input.setFirstExecutionDate(new Date(1544582663000L));
		input.setPaymentSetting(null);
		input.setRecurringPeriod(null);
		input.setAmountLimit(2000000D);
		input.setFinishDate(new Date(1607741063L));

		OutputBaseDTO baseDTO = new OutputBaseDTO();
		if (input.getOrderNumber().equals("301")) {
			baseDTO.setResultCode("0");
			baseDTO.setResultMsg(MessageConstant.SERVER_ERROR);
		} else if (input.getOrderNumber().equals("1")) {
			baseDTO.setResultCode("1000");
			baseDTO.setResultMsg(MessageConstant.ACCESS_DENIED);
		} else {
			baseDTO.setResultCode("1");
			baseDTO.setResultMsg(MessageConstant.OK_MESSAGE);
		}

		when(accountService.modifyAutoBillTransferDirectly(anyString(), ArgumentMatchers.any())).thenReturn(baseDTO);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of modifyAutoBillTransferDirectly : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void teststandingOrderList() throws Exception {

		initLoginMock();
		String path = "/service/bill/standingOrderList";
		List<StandingOrderOutputDTO> lst = new ArrayList<>();
		StandingOrderOutputDTO dto = new StandingOrderOutputDTO();
		dto.setBankName("OCEAN");
		StandingOrderDTO standingOrder = new StandingOrderDTO();
		standingOrder.setProductOwner(801003D);
		standingOrder.setId(null);
		standingOrder.setShortName("Orange");
		standingOrder.setOrderingCustomer(null);
		standingOrder.setDebitAccount("0100100003486003");
		String[] beneficiary = { "SHORTNAME-910802" };
		standingOrder.setBeneficiary(beneficiary);
		standingOrder.setCreditAccount("0001100001761003");
		standingOrder.setCreditAccountBankName(null);
		standingOrder.setAmount(50000D);
		standingOrder.setAmountCalculation(null);
		standingOrder.setCurrency("VND");
		String[] remarks = { "Desc" };
		standingOrder.setRemarks(remarks);
		standingOrder.setClearingNetwork(null);
		standingOrder.setStartDate(new Date(1543510800000L));
		standingOrder.setEndDate(new Date(1553878800000L));
		FrequencyDTO frequency = new FrequencyDTO();
		frequency.setNextDate(new Date(1543510800000L));
		frequency.setPeriodUnit("M");
		frequency.setPeriodCount(1);
		frequency.setNextDate(null);
		standingOrder.setFrequency(frequency);
		standingOrder.setActiveFlag(false);
		standingOrder.setStandingOrderReferenceId("0100100003486003.3");
		standingOrder.setAlreadyDeleted(false);
		dto.setStandingOrder(standingOrder);
		lst.add(dto);

		when(accountService.standingOrderList(anyString())).thenReturn(lst);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<StandingOrderOutputDTO> realData = (List<StandingOrderOutputDTO>) response.getData();
		System.out.println("payload of teststandingOrderList : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void test_createStandingOrder() throws Exception {

		// OTP
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		////
		initLoginMock();
		OutputBaseDTO dto = new OutputBaseDTO();
		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);

		String path = "/service/bill/createStandingOrder";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setShortName("Han Thai Trinh");
		input.setAmount(100000000L);
		String[] beneficiary = { "Ten Nguoi Nhan 1" };
		input.setBeneficiary(beneficiary);
		input.setCreditAccount("0100100007826003");
		String[] remarks = { "Noi dung mo ta" };
		input.setRemarks(remarks);
		input.setDebitAccountId("11");
		input.setCurrency("VND");
		input.setEndDate(new Date(1546077437L));
		input.setPeriodUnit("M");
		input.setPeriodCount("2");
		input.setDayOfMonth(21);
		input.setStartDate(new Date(1545386237L));

		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue1);
		when(accountService.createStandingOrder(anyString(), ArgumentMatchers.any())).thenReturn(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of test_createStandingOrder : " + objectMapper.writeValueAsString(input));
		System.out.println("result of test_createStandingOrder : " + objectMapper.writeValueAsString(realData));
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void test_createStandingOrder_OTP_WRONG() throws Exception {

		// OTP
		initLoginMock();
		// otp mock
		VerifyOtpToken otpValue1 = new VerifyOtpToken();
		otpValue1.setAuthId("12345");
		otpValue1.setOtpValue("123");

		VerifyOtpToken otpValue2 = new VerifyOtpToken();
		otpValue2.setAuthId("99999");
		otpValue2.setOtpValue("456");

		// mock otp return
		AuthorizationResponse mockOtpResponseOK = new AuthorizationResponse();
		mockOtpResponseOK.setOperationStatus(AuthorizationResponse.CORRECT);
		AuthorizationResponse mockOtpResponseNoOK = new AuthorizationResponse();
		mockOtpResponseNoOK.setOperationStatus(AuthorizationResponse.INCORRECT);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue1), anyString())).thenReturn(mockOtpResponseOK);

		when(accountService.verifyOTPToken(ArgumentMatchers.eq(otpValue2), anyString()))
				.thenReturn(mockOtpResponseNoOK);
		////
		initLoginMock();
		OutputBaseDTO dto = new OutputBaseDTO();
		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);

		String path = "/service/bill/createStandingOrder";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setShortName("Han Thai Trinh");
		input.setAmount(100000000L);
		String[] beneficiary = { "Ten Nguoi Nhan 1" };
		input.setBeneficiary(beneficiary);
		input.setCreditAccount("0100100007826003");
		String[] remarks = { "Noi dung mo ta" };
		input.setRemarks(remarks);
		input.setDebitAccountId("11");
		input.setCurrency("VND");
		input.setEndDate(new Date(1546077437L));
		input.setPeriodUnit("M");
		input.setPeriodCount("2");
		input.setDayOfMonth(21);
		input.setStartDate(new Date(1545386237L));

		input.setOtpMethod(OtpAuthorizeMethod.SMS_TOKEN.getCode());
		input.setOtpValue(otpValue2);
		when(accountService.createStandingOrder(anyString(), ArgumentMatchers.any())).thenReturn(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().is5xxServerError()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of test_createStandingOrder : " + objectMapper.writeValueAsString(input));
		System.out.println("result of test_createStandingOrder : " + objectMapper.writeValueAsString(realData));
		assertEquals(false, response.isSuccess());
		assertEquals("OTP_" + AuthorizationResponse.INCORRECT, response.getErrorCode());
	}

	@Test
	public void testremoveStandingOrder() throws Exception {

		initLoginMock();
		OutputBaseDTO dto = new OutputBaseDTO();
		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);

		String path = "/service/bill/removeStandingOrder";
		Map<String, String> input = new HashMap<>();
		input.put("standingOrderId", "0100100006076008.4");

		when(accountService.removeStandingOrder(anyString(), ArgumentMatchers.any())).thenReturn(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of deleteAutoBillTransfer : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

	@Test
	public void test_modifyStandingOrderDirectly() throws Exception {

		initLoginMock();
		OutputBaseDTO dto = new OutputBaseDTO();
		dto.setResultCode("1");
		dto.setResultMsg(MessageConstant.OK_MESSAGE);

		String path = "/service/bill/modifyStandingOrderDirectly";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setStandingOrderId("0100100007825007.2");
		input.setShortName("Nguyen");
		input.setAmount(100000000L);
		input.setBeneficiary(new String[] { "SHORTNAME-801045" });
		input.setCreditAccount("0100100007825007");
		input.setRemarks(new String[] { "des" });
		input.setDebitAccountId("321");
		input.setCurrency("USD");
		input.setEndDate(new Date(1548867600000L));
		input.setPeriodUnit("M");
		input.setPeriodCount("1");
		input.setNextDate(new Date(1644374800000L));

		when(accountService.modifyStandingOrderDirectly(anyString(), ArgumentMatchers.any())).thenReturn(dto);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(input))).andExpect(status().isOk()).andReturn();

		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("payload of modifyStandingOrderDirectly : " + objectMapper.writeValueAsString(realData));
		assertNotNull(realData);
	}

}
