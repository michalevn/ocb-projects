package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.ocb.oma.dto.OutputBaseDTO;
import com.ocb.oma.dto.TransferAutoBillDTO;
import com.ocb.oma.dto.input.StandingOrderInputDTO;
import com.ocb.oma.oomni.dto.CreateAutoBillTransferDirectlyDTO;
import com.ocb.oma.oomni.dto.DeleteAutoBillTransferOutputDTO;
import com.ocb.oma.oomni.dto.StandingOrderOutputDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class BillControllerTest extends BaseTest {

	@Test
	public void test_getTransferAutoBills() throws Exception {

		URIBuilder uriBuilder = new URIBuilder("/service/bill/getTransferAutoBills");
		uriBuilder.addParameter("dateFrom", "1511664871421");
		uriBuilder.addParameter("dateTo", "1574736871421");

		String path = uriBuilder.toString();
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("test_getTransferAutoBills : " + rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void test_createAutoBillTransferDirectly() throws Exception {

		URIBuilder uriBuilder = new URIBuilder("/service/bill/createAutoBillTransferDirectly");
		String path = uriBuilder.toString();

		String dtoJson = "{\"frequencyPeriodCount\":2,\"paymentSetting\":\"FULL\",\"recurringPeriod\":\"NOLIMIT\",\"fromAccount\":\"321\",\"customerId\":\"801004\",\"serviceCode\":\"ADSL\",\"serviceProviderCode\":\"FPT\",\"amountLimit\":0,\"firstExecutionDate\":\"2018-12-10T03:53:29.150Z\",\"finishDate\":\"2019-12-10T03:53:29.150Z\",\"currency\":\"VND\",\"frequencyPeriodUnit\":\"W\",\"nextExecutionDate\":\"2018-12-10T03:53:29.150Z\",\"serviceProviderCodeName\":\"XYZ\",\"serviceCodeName\":\"ABC\"}";
		CreateAutoBillTransferDirectlyDTO dto = objectMapper.readValue(dtoJson,
				CreateAutoBillTransferDirectlyDTO.class);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		requestBuilder = requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(dto));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String rs = result.getResponse().getContentAsString();
		System.out.println("test_createAutoBillTransferDirectly : " + rs);
		ResponseDTO response = objectMapper.readValue(rs, ResponseDTO.class);
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void testdeleteAutoBillTransfer() throws Exception {
		String path = "/service/bill/deleteAutoBillTransfer";
		String orderNumber = "4054";
		Map<String, String> params = new HashMap<>();
		params.put("orderNumber", orderNumber);

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(params)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		DeleteAutoBillTransferOutputDTO realData = objectMapper.convertValue(response.getData(),
				DeleteAutoBillTransferOutputDTO.class);
		System.out.println("*******result of deleteAutoBillTransfer : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testmodifyAutoBillTransferDirectly() throws Exception {
		String path = "/service/bill/modifyAutoBillTransferDirectly";
		TransferAutoBillDTO input = new TransferAutoBillDTO();
		input.setOrderNumber("4054");
		input.setFrequencyPeriodCount(1);
		input.setFrequencyPeriodUnit("");
		input.setFirstExecutionDate(new Date(1544582663000L));
		input.setPaymentSetting(null);
		input.setRecurringPeriod(null);
		input.setAmountLimit(25000000D);
		input.setFinishDate(new Date(1607741063L));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println(
				"*******result of modifyAutoBillTransferDirectly : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void teststandingOrderList() throws Exception {
		String path = "/service/bill/standingOrderList";

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json"))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		List<StandingOrderOutputDTO> realData = (List<StandingOrderOutputDTO>) response.getData();
		System.out.println("payload of teststandingOrderList : " + objectMapper.writeValueAsString(response));
		assertNotNull(realData);
	}

	@Test
	public void test_createStandingOrder() throws Exception {
		String path = "/service/bill/createStandingOrder";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setShortName("Han Thai Trinh");
		input.setAmount(1000L);
		input.setBeneficiary(new String[] { "SHORTNAME-801045" });
		input.setCreditAccount("0100100007826003");
		input.setRemarks(new String[] { "des" });
		input.setDebitAccountId("11");
		input.setCurrency("VND");
		input.setEndDate(new Date(1593144937000L));
		input.setPeriodUnit("M");
		input.setPeriodCount("1");
		input.setDayOfMonth(22);
		input.setStartDate(new Date(1545365737000L));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("*******result of test_createStandingOrder : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void testremoveStandingOrder() throws Exception {
		String path = "/service/bill/removeStandingOrder";
		Map<String, String> input = new HashMap<>();
		input.put("standingOrderId", "0100100006076008.4");

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out.println("*******result of testremoveStandingOrder : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

	@Test
	public void test_modifyStandingOrderDirectly() throws Exception {
		String path = "/service/bill/modifyStandingOrderDirectly";
		StandingOrderInputDTO input = new StandingOrderInputDTO();
		input.setStandingOrderId("0100100007825007.2");
		input.setShortName("Nguyen");
		input.setAmount(100000000L);
		input.setBeneficiary(new String[] { "SHORTNAME-801045" });
		input.setCreditAccount("0100100007825007");
		input.setRemarks(new String[] { "des" });
		input.setDebitAccountId("321");
		input.setCurrency("USD");
		input.setEndDate(new Date(1548867600000L));
		input.setPeriodUnit("M");
		input.setPeriodCount("1");
		input.setNextDate(new Date(1644374800000L));

		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(input)))
				.andExpect(status().is2xxSuccessful()).andReturn();
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		OutputBaseDTO realData = objectMapper.convertValue(response.getData(), OutputBaseDTO.class);
		System.out
				.println("*******result of modifyStandingOrderDirectly : " + result.getResponse().getContentAsString());
		assertNotNull(realData);
	}

}
