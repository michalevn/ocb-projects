package com.ocb.oma.web.external;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.PaymentType;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.web.OmaCoresvWebApplication;
import com.ocb.oma.web.dto.ResponseDTO;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvWebApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class PostPaymentControllerTest extends BaseTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void test_postPayment() throws Exception {

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.InternalPayment.getPaymentTypeCode());
//		inputDTO.setPaymentType("InternalPaymentssss");
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAccountId("12");
		paymentInfo.setAmount(50000L);
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("0037100006151005");
		paymentInfo.setRecipient("ibtest10_ibtest09");
		paymentInfo.setRemarks("Remark chuyen tien");
		paymentInfo.setExecutionDate(new Date());
		inputDTO.setPaymentInfo(paymentInfo);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testPostPayment " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());
	}

	@Test
	public void test_postPayment_LocalPayment() throws Exception {

		PostPaymentInputDTO inputDTO = new PostPaymentInputDTO();
		inputDTO.setPaymentType(PaymentType.LocalPayment.getPaymentTypeCode());
		PaymentInfoDTO paymentInfo = new PaymentInfoDTO();
		paymentInfo.setAmount(100000L);
		paymentInfo.setAccountId("11");
		paymentInfo.setCurrency("VND");
		paymentInfo.setCreditAccount("987654321");
		paymentInfo.setRecipient("PHAM THANH AN");
		paymentInfo.setRemarks("123");
		paymentInfo.setExecutionDate(new Date());
		paymentInfo.setCreditAccountBankBranchCode("83202001");
		paymentInfo.setCreditAccountBankCode("202");
		paymentInfo.setCreditAccountProvinceCode("83");

		inputDTO.setPaymentInfo(paymentInfo);

		String path = "/service/paymentBasket/postPayment";
		System.out.println("***payload of testPostPayment " + objectMapper.writeValueAsString(inputDTO));
		MockHttpServletRequestBuilder requestBuilder = processPostAuth(path);
		MvcResult result = mvc.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
				.content(objectMapper.writeValueAsString(inputDTO))).andExpect(status().isOk()).andReturn();
		System.out.println("***testPostPayment : " + result.getResponse().getContentAsString());
		ResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), ResponseDTO.class);
		System.out.println("***result of testPostPayment " + objectMapper.writeValueAsString(response));
		assertEquals(true, response.isSuccess());
	}

}
