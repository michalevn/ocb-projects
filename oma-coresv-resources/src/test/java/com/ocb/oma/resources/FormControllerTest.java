/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.FormDetailsDTO;
import com.ocb.oma.resources.repository.OmniCardTyleRepository;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class FormControllerTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected OmniCardTyleRepository repo;

	@Test
	public void test_create() throws Exception {

		FormDTO formDTO = new FormDTO();
		formDTO.setTitle("Tiêu đề 01");
		formDTO.setDescription("Mô tả 01");
		formDTO.setUserEmail("docv@ocb.com.vn");// docao.hcm@gmail.com
		formDTO.setUserName("Đô Cao");
		formDTO.setUserPhone("0903657884");

		List<FormDetailsDTO> formDetailsDTOs = new ArrayList<>();

		FormDetailsDTO image01 = new FormDetailsDTO();
		image01.setFieldName("image");
		image01.setFieldType("IMAGE");
		image01.setFieldValue("data:image/png;zzz");
		formDetailsDTOs.add(image01);

		FormDetailsDTO image02 = new FormDetailsDTO();
		image02.setFieldName("image");
		image02.setFieldType("IMAGE");
		image02.setFieldValue("data:image/png;base64,xxx");
		formDetailsDTOs.add(image02);

		formDTO.setFormDetails(formDetailsDTOs);

		System.out.println(objectMapper.writeValueAsString(formDTO));

		String path = "/forms/sendMailCus";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(formDTO));
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String uuid = result.getResponse().getContentAsString();
		System.out.println("uuid: " + uuid);

		test_findByUuid(uuid);
	}

	public void test_findByUuid(String uuid) throws Exception {
		String path = "/forms/" + uuid;
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path);
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();
		String responseText = result.getResponse().getContentAsString();
		System.out.println("responseText: " + responseText);
		FormDTO formDTO = objectMapper.readValue(responseText, FormDTO.class);
		assertEquals(uuid, formDTO.getUuid());
	}
}
