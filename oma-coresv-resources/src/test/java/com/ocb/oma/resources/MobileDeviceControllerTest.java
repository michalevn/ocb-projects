/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.repository.OmniMobileDeviceRepository;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class MobileDeviceControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private OmniMobileDeviceRepository repo;

	@Test
	public void test_registerDevice() throws Exception {

		String deviceId = "123";

		String path = "/mobileDevices/registerDevice";

		OmniMobileDevice omniMobileDevice = new OmniMobileDevice();
		omniMobileDevice.setActive(true);
		omniMobileDevice.setDeviceBrand("sony");
		omniMobileDevice.setOsType("android");
		omniMobileDevice.setDeviceId(deviceId);
		omniMobileDevice.setDeviceName("Sony Xperia ZR");
		omniMobileDevice.setDeviceModal("SS-N960F");
		omniMobileDevice.setVersion("27");
		omniMobileDevice.setDeviceToken("xyz");

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(omniMobileDevice));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();
		System.out.println("*** test_registerDevice : " + contentAsString);

		assertNotNull(contentAsString);

		OmniMobileDevice mobileDevice = repo.findByDeviceId(deviceId);
		assertNotNull(mobileDevice);

	}
}
