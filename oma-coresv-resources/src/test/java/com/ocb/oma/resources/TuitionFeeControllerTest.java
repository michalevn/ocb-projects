/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.service.TuitionFeeService;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class TuitionFeeControllerTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected TuitionFeeService tuitionFeeService;

	@Test
	public void test_findGroups() throws Exception {

		List<TuitionFeeGroup> tuitionFeeGroups = tuitionFeeService.findGroups();

		String path = "/tuition-fee/findGroups";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String content = result.getResponse().getContentAsString();

		assertNotNull(content);

		List<TuitionFeeGroup> results = objectMapper.readValue(content, new TypeReference<List<TuitionFeeGroup>>() {
		});

		assertEquals(tuitionFeeGroups.size(), results.size());
	}

	@Test
	public void test_findByGroup() throws Exception {

		String groupCode = "DH";
		int page = 0;
		int size = 10;

		List<TuitionFeeObject> tuitionFeeObjects = tuitionFeeService.findByGroup(groupCode, page, size);

		String path = "/tuition-fee/findByGroup";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.param("groupCode", groupCode);
		requestBuilder.param("page", String.valueOf(page));
		requestBuilder.param("size", String.valueOf(size));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String content = result.getResponse().getContentAsString();

		assertNotNull(content);

		List<TuitionFeeObject> results = objectMapper.readValue(content, new TypeReference<List<TuitionFeeObject>>() {
		});

		assertEquals(tuitionFeeObjects.size(), results.size());
	}
}
