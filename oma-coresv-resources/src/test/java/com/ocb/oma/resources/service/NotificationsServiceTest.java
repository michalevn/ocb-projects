/**
 * 
 */
package com.ocb.oma.resources.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.OmaCoresvResourcesApplication;
import com.ocb.oma.resources.model.OmniNotification;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class NotificationsServiceTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	private NotificationsService notificationService;

	@Test
	public void test_push() throws Exception {
		OmniNotification notification = new OmniNotification();
		notification.setTitleEl("Hello");
		notification.setTitleVn("Xin Chào");
		notification.setDescriptionEl("I wish you have a good day!!!");
		notification.setDescriptionVn("Chúc bạn ngày mới tốt lành!!!");
		notification.setDistributedType(OmniNotification.DISTRIBUTED_TYPE_FIREBASE);
		notification.setType(OmniNotification.TYPE_THONG_BAO.toString());
		notification.setUserId("801004");
		notificationService.push(notification);
	}
}
