/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.resources.repository.OmniGiftCardReceiverRepository;
import com.ocb.oma.resources.repository.OmniGiftCardRepository;

/**
 * @author Phu Hoang
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class ResourcesControllerTest {

	@Autowired
	private MockMvc mvc;
	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	OmniGiftCardRepository giftCardRepos;

	@Autowired
	OmniGiftCardReceiverRepository giftcardReceiverRepos;

	@Test
	public void testGetBankInfo() throws Exception {

		String path = "/resources/bankList";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<BankInfoDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<BankInfoDTO>>() {
				});

		System.out.println("payload of testGetBankInfo : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());

	}

	@Test
	public void searchBankCitad() throws Exception {

		String path = "/resources/searchBankCitad?provinceCode=01&bankCode=Bank44";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<BankInfoDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<BankInfoDTO>>() {
				});

		System.out.println("payload of searchBankCitad : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());

	}

	@Test
	public void testGetProvinceCodeList() throws Exception {

		String path = "/resources/listOfValue?lovType=PROVINCE";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		List<ListOfValueDTO> response = objectMapper.readValue(result.getResponse().getContentAsString(),
				new TypeReference<List<ListOfValueDTO>>() {
				});

		System.out.println("payload of testGetProvinceCodeList : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		assertNotEquals(0, response.size());

	}

	@Test
	public void getByBankCitadId() throws Exception {

		String path = "/resources/getByBankCitadId?bankCitadCode=101";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc.perform(requestBuilder.contentType("application/json")).andExpect(status().isOk())
				.andReturn();

		String response = result.getResponse().getContentAsString();

		System.out.println("payload of getByBankCitadId : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);

	}

	@Test
	public void testSendGiftCard() throws Exception {
		long countGiftCardRepos = giftCardRepos.count();
		long countGiftcardReceiverRepos = giftcardReceiverRepos.count();

		GiftCardInsertDTO giftCardInsertDTO = new GiftCardInsertDTO();
		OmniGiftCard giftCard = new OmniGiftCard();
		giftCard.setDebitAccountId("1009233800303");
		giftCard.setCurrency("VND");
		giftCard.setShortMessage("Have a nice day");
		giftCardInsertDTO.setGiftCard(giftCard);
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = new ArrayList<>();
		String[] arrSetBeneficiary = { "0300958900090", "0300958900300", "030095890999" };
		for (int i = 0; i < arrSetBeneficiary.length; i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = new OmniGiftCardReceiver();
			omniGiftCardReceiver.setBeneficiary(arrSetBeneficiary[i]);
			omniGiftCardReceiver.setRecipient(
					"c2yJi_v8Sh8:APA91bEMk1voFMm0DNHY-WKPQULpaGrWWEY_nY7fikrRYxeOlI328JM4gpEqxEC0eKR3_y2FfNKL96XBfx4f3D54hjWTX-TNHUq95GloLDo8mWIGRQnz_94kivIbj6G5vkcn3iF-PaN75");
			omniGiftCardReceiver.setAmount((double) (3400 * (i + 1)));

			lstOmniGiftCardReceiver.add(omniGiftCardReceiver);
		}
		giftCardInsertDTO.setReceivers(lstOmniGiftCardReceiver);

		String path = "/resources/sendGiftCard";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);

		MvcResult result = mvc
				.perform(requestBuilder.header(HttpHeaders.CONTENT_TYPE, "application/json")
						.content(objectMapper.writeValueAsString(giftCardInsertDTO)))
				.andExpect(status().isOk()).andReturn();

		String response = result.getResponse().getContentAsString();

		System.out.println("payload of sendGiftCard : " + objectMapper.writeValueAsString(response));
		assertNotNull(response);
		long countGiftCardReposAf = giftCardRepos.count();
		long countGiftcardReceiverReposAf = giftcardReceiverRepos.count();
		assertEquals(countGiftCardRepos + 1, countGiftCardReposAf);
		assertEquals(countGiftcardReceiverRepos + arrSetBeneficiary.length, countGiftcardReceiverReposAf);
	}

}
