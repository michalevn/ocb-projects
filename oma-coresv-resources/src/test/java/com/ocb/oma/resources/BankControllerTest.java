/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.resources.model.OmniBankCitad;
import com.ocb.oma.resources.model.OmniBankInfo;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class BankControllerTest {

	@Autowired
	private MockMvc mvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void test_getByBankInfoByBranchCode() throws Exception {

		String branchCode = "01307004";
		String path = "/resources/getByBankInfoByBranchCode?branchCode=" + branchCode;

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path);
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String responseText = result.getResponse().getContentAsString();

		System.out.println(responseText);
		System.out.println("");

		OmniBankCitad bankCitad = objectMapper.readValue(responseText, OmniBankCitad.class);

		assertNotNull(bankCitad);
		assertEquals(branchCode, bankCitad.getBankBranchCode());
		assertNotNull(bankCitad.getBank());
		OmniBankInfo bank = bankCitad.getBank();
		System.out.println(bank.getBankName());

	}

	@Test
	public void testGetBankByNapasCode() throws Exception {

		String napasCode ="970406";
		String path = "/resources/getByCodeNapas?codeNapas=" +napasCode;

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(path);
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String responseText = result.getResponse().getContentAsString();
		
		System.out.println(responseText);
		System.out.println("");

		BankInfoDTO bankinfo   = objectMapper.readValue(responseText, BankInfoDTO.class);

		assertNotNull(bankinfo);
		assertEquals(napasCode, bankinfo.getCodeNapas());
		assertNotNull(bankinfo.getBankName());
		System.out.println(bankinfo.getBankName());
		

	}
}
