package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.repository.OmniNotificationsRepository;
import com.ocb.oma.resources.service.NotificationsService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class NotificationsServiceTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private NotificationsService notificationService;

	@Autowired
	private OmniNotificationsRepository notificationsRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void testGet() throws Exception {

		String userId = "2005";

		OmniNotification notification = new OmniNotification();
		notification.setTitleVn("Xin chao");
		notification.setTitleEl("Hello");
		notification.setBodyVn("Chúc một ngày mới tốt đẹp!");
		notification.setBodyEl("Have a nice day!");
		notification.setDistributedType(3);
		notification.setType(OmniNotification.TYPE_THONG_BAO.toString());
		notification.setUserId(userId);
		notificationsRepository.save(notification);

		String path = "/notifications/get";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.param("notificationId", notification.getId().toString());

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String responseText = result.getResponse().getContentAsString();

		assertNotNull(responseText);

		OmniNotification responsedNotification = objectMapper.readValue(responseText, OmniNotification.class);
		assertNotNull(responsedNotification);
		assertEquals(responsedNotification.getId(), notification.getId());
	}

	@Test
	public void testFind() throws Exception {

		String query = "in";
		String userId = "801004";
		Integer type = OmniNotification.TYPE_THONG_BAO;

		for (int i = 0; i < 23; i++) {
			OmniNotification omniNotification1 = new OmniNotification();
			omniNotification1.setTitleVn("Xin chao " + i);
			omniNotification1.setTitleEl("Hello " + i);
			omniNotification1.setBodyVn("Chúc một ngày mới tốt đẹp! " + i);
			omniNotification1.setBodyEl("Have a nice day!" + i);
			omniNotification1.setDistributedType(3);
			omniNotification1.setType(type.toString());
			omniNotification1.setUserId(userId);
			notificationsRepository.save(omniNotification1);
		}

		int size = 10;

		List<OmniNotification> notifications = notificationService.findByUserIdAndType(query, userId, type, 0, size);
		assertEquals(notifications.size(), 10);

		notifications = notificationService.findByUserIdAndType(query, userId, type, 1, size);
		assertEquals(notifications.size(), 10);

		notifications = notificationService.findByUserIdAndType(query, userId, type, 2, size);
		assertEquals(notifications.size(), 3);

		notifications = notificationService.findByUserIdAndType(query, userId, type, 3, size);
		assertEquals(notifications.size(), 0);

		String path = "/notifications/findByUserIdAndType";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.param("query", "");
		requestBuilder.param("userId", userId);
		requestBuilder.param("type", type.toString());
		requestBuilder.param("page", "0");
		requestBuilder.param("size", "10");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String responseText = result.getResponse().getContentAsString();

		assertNotNull(responseText);

		List<OmniNotification> omniNotifications = objectMapper.readValue(responseText,
				new TypeReference<List<OmniNotification>>() {
				});

		assertNotNull(omniNotifications);

		assertEquals(10, omniNotifications.size());
	}

	@Test
	public void test_countUnread() throws Exception {

		String userId = "2005";

		OmniNotification omniNotification = new OmniNotification();
		omniNotification.setTitleVn("Xin chao");
		omniNotification.setTitleEl("Hello");
		omniNotification.setBodyVn("Chúc một ngày mới tốt đẹp!");
		omniNotification.setBodyEl("Have a nice day!");
		omniNotification.setDistributedType(3);
		omniNotification.setType(OmniNotification.TYPE_THONG_BAO.toString());
		omniNotification.setUserId(userId);
		notificationsRepository.save(omniNotification);

		String path = "/notifications/countUnread";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.param("cif", userId);
		requestBuilder.param("durationInDays", "10");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String responseText = result.getResponse().getContentAsString();

		assertNotNull(responseText);

		Integer count = objectMapper.readValue(responseText, Integer.class);

		assertNotNull(count);

		assertEquals(true, count.equals(1));
	}
}
