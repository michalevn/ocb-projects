/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.resources.repository.OmniMobileDeviceRepository;
import com.ocb.oma.resources.repository.UserDeviceTokenRepository;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class UserDeviceControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private OmniMobileDeviceRepository mobileDeviceRepository;

	@Autowired
	private UserDeviceTokenRepository userDeviceTokenRepository;

	private String deviceId = "65d32952f937475b";
	private String cif = "801003";

	@Test
	public void test_findUserDevices() throws Exception {

		String path = "/userDevices/findUserDevices";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.param("cif", cif);
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		System.out.println("*** test_findUserDevices : " + contentAsString);

		assertNotNull(contentAsString);

		List<UserDeviceToken> userDeviceTokens = objectMapper.readValue(contentAsString,
				new TypeReference<List<UserDeviceToken>>() {
				});

		assertEquals(1, userDeviceTokens.size());
	}

	@Test
	public void test_registerUserDevice() throws Exception {

		OmniMobileDevice mobileDevice = mobileDeviceRepository.findByDeviceId(deviceId);

		String path = "/userDevices/registerUserDevice";

		UserDeviceToken input = new UserDeviceToken();
		input.setCif(cif);
		input.setDeviceId(mobileDevice.getDeviceId());
		input.setDeviceToken(mobileDevice.getDeviceToken());
		input.setStatus(0);

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		requestBuilder.content(objectMapper.writeValueAsBytes(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		System.out.println("*** registerUserDevice: " + contentAsString);

		UserDeviceToken userDeviceToken = userDeviceTokenRepository.findByCifAndDeviceId(cif, deviceId);
		assertNotNull(userDeviceToken);
	}

	@Test
	public void test_activeUserDevice() throws Exception {

		String path = "/userDevices/activeUserDevice";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.param("cif", cif);
		requestBuilder.param("deviceId", deviceId);
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		System.out.println("*** activeUserDevice: " + contentAsString);

		assertNotNull(contentAsString);

		UserDeviceToken userDeviceToken2 = objectMapper.readValue(contentAsString, UserDeviceToken.class);

		assertNotNull(userDeviceToken2);
	}

	@Test
	public void test_updateUserDeviceStatus() throws Exception {

		test_registerUserDevice();

		String path = "/userDevices/updateUserDeviceStatus";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");

		UserDeviceToken input = new UserDeviceToken();
		input.setCif(cif);
		input.setDeviceId(deviceId);
		input.setStatus(1);
		requestBuilder.content(objectMapper.writeValueAsBytes(input));

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		System.out.println("*** updateUserDeviceStatus: " + contentAsString);
		assertEquals(Boolean.TRUE.toString(), contentAsString);

		UserDeviceToken userDeviceToken = userDeviceTokenRepository.findByCifAndDeviceId(cif, deviceId);
		assertEquals(true, userDeviceToken.getStatus() != null && userDeviceToken.getStatus().equals(1));

	}

	@Test
	public void test_getUserDevice() throws Exception {

		String path = "/userDevices/getUserDevice";

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.param("cif", cif);
		requestBuilder.param("deviceId", deviceId);
		requestBuilder.contentType("application/json");

		MvcResult result = mvc.perform(requestBuilder).andExpect(status().is2xxSuccessful()).andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		System.out.println("*** getUserDevice: " + contentAsString);

		assertNotNull(contentAsString);

		UserDeviceToken userDeviceToken2 = objectMapper.readValue(contentAsString, UserDeviceToken.class);

		assertNotNull(userDeviceToken2);
	}

}
