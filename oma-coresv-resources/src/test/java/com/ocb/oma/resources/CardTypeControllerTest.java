/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniCardType;
import com.ocb.oma.resources.repository.OmniCardTyleRepository;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class CardTypeControllerTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected OmniCardTyleRepository repo;

	@Test
	public void test_find() throws Exception {

		String path = "/cardTypes/find";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String content = result.getResponse().getContentAsString();
		List<OmniCardType> cardTypeDTOs = objectMapper.readValue(content, new TypeReference<List<OmniCardType>>() {
		});

		assertNotNull(cardTypeDTOs);
	}
}
