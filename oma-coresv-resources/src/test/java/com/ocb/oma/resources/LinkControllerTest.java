/**
 * 
 */
package com.ocb.oma.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.resources.model.OmniLink;
import com.ocb.oma.resources.repository.OmniLinkRepository;
import com.ocb.oma.resources.service.LinkService;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class LinkControllerTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected OmniLinkRepository linkRepo;

	@Autowired
	protected LinkService linkService;

	@Test
	public void test_getLinksByGroup() throws Exception {

		String group = "BANNER";

		OmniLink omniLink = new OmniLink();
		omniLink.setGroup(group);
		omniLink.setActive(true);
		linkRepo.save(omniLink);

		List<LinkDTO> links = linkService.getLinksByGroup(group);

		String path = "/resources/getLinksByGroup";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(path);
		requestBuilder.param("group", group);

		requestBuilder.contentType("application/json");
		MvcResult result = mvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

		String content = result.getResponse().getContentAsString();

		assertNotNull(content);

		List<LinkDTO> dtos = objectMapper.readValue(content, new TypeReference<List<LinkDTO>>() {
		});

		assertEquals(links.size(), 1);
		assertEquals(dtos.size(), 1);
	}
}
