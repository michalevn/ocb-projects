/**
 * 
 */
package com.ocb.oma.resources.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.OmaCoresvResourcesApplication;
import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.repository.TuitionFeeGroupRepository;
import com.ocb.oma.resources.repository.TuitionFeeObjectRepository;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class TuitionFeeServiceTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

	@Autowired
	protected TuitionFeeObjectRepository tuitionFeeObjectRepository;

	@Autowired
	protected TuitionFeeGroupRepository tuitionFeeGroupRepository;

	@Autowired
	protected TuitionFeeService tuitionFeeService;

	@Test
	public void test_findGroups() throws Exception {
		List<TuitionFeeGroup> tuitionFeeGroups = tuitionFeeService.findGroups();
		System.out.println(objectMapper.writeValueAsString(tuitionFeeGroups));
		assertNotNull(tuitionFeeGroups);
		assertNotEquals(0, tuitionFeeGroups.size());
	}

	@Test
	public void test_findByGroup() throws Exception {
		String groupCode = "DH";
		int page = 0;
		int size = 10;
		List<TuitionFeeObject> tuitionFeeObjects = tuitionFeeService.findByGroup(groupCode, page, size);
		assertNotNull(tuitionFeeObjects);
		assertNotEquals(0, tuitionFeeObjects.size());
		for (TuitionFeeObject tuitionFeeObject : tuitionFeeObjects) {
			assertEquals(groupCode, tuitionFeeObject.getGroupCode());
		}
	}
}
