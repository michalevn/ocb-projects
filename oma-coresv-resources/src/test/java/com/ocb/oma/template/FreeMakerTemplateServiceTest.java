package com.ocb.oma.template;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ocb.oma.oomni.dto.Notification.NotificationType;
import com.ocb.oma.resources.OmaCoresvResourcesApplication;
import com.ocb.oma.resources.service.TemplateService;

/**
 * @author docv
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OmaCoresvResourcesApplication.class)
@AutoConfigureMockMvc
public class FreeMakerTemplateServiceTest {

	@Autowired
	private TemplateService templateService;
	
	public static void main(String[] args) {
		File file = new File("/data/templates");
		System.out.println(file.getAbsolutePath());
	}

	@Test
	public void doTest1() {
		System.out.println("======================================");
		Map<String, String> params = new HashMap<>();
		params.put("user", "docv");
		String rs = templateService.format("hello", params, NotificationType.FIREBASE.name());
		System.out.println(rs);
		System.out.println("======================================");
	}

	@Test
	public void doTest2() {
		System.out.println("======================================");
		Map<String, String> params = new HashMap<>();
		params.put("user", "docv");
		String rs = templateService.format("<h1>Hello ${user}</h1>", params, NotificationType.FIREBASE.name());
		System.out.println(rs);
		System.out.println("======================================");
	}

}
