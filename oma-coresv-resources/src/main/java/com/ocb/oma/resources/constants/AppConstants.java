package com.ocb.oma.resources.constants;

public interface AppConstants {

	static final String BLANK = "";
	static final String FREE_MAKER_TEMPLATE = "FREE_MAKER_TEMPLATE";
	static final String UTF_8 = "UTF-8";
	static final String TEMPLATE_PATH = "/templates";
	
	static final Integer DURATION_IN_DAYS = 7;

}
