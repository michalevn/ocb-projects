/**
 * 
 */
package com.ocb.oma.resources.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.service.TuitionFeeService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/tuition-fee")
public class TuitionFeeController extends BaseController {

	@Autowired
	private TuitionFeeService tuitionFeeService;

	@RequestMapping(value = "/findGroups")
	public List<TuitionFeeGroup> findGroups() {
		return tuitionFeeService.findGroups();
	}

	@RequestMapping(value = "/findByGroup")
	public List<TuitionFeeObject> findByGroup(@RequestParam("groupCode") String groupCode,
			@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
		return tuitionFeeService.findByGroup(groupCode, page, size);
	}

}
