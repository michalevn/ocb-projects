/**
 * 
 */
package com.ocb.oma.resources.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.service.NotificationsService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/notifications")
public class NotificationController extends BaseController {

	@Autowired
	private NotificationsService notificationService;

	@RequestMapping(value = "/push")
	public Boolean push(@RequestBody OmniNotification omniNotification) {
		notificationService.push(omniNotification);
		return true;
	}

	@RequestMapping(value = "/maskAsRead/{cif}")
	public Boolean maskAsRead(@PathVariable("cif") String cif, @RequestBody List<Long> notificationIds) {
		notificationService.maskAsRead(cif, notificationIds);
		return true;
	}

	@RequestMapping(value = "/maskAsUnread/{cif}")
	public Boolean maskAsUnread(@PathVariable("cif") String cif, @RequestBody List<Long> notificationIds) {
		notificationService.maskAsUnread(cif, notificationIds);
		return true;
	}

	@RequestMapping(value = "/delete/{cif}")
	public Boolean delete(@PathVariable("cif") String cif, @RequestBody List<Long> notificationIds) {
		notificationService.delete(cif, notificationIds);
		return true;
	}

	@RequestMapping(value = "/findByUserIdAndType")
	public List<OmniNotification> findByUserIdAndType(@RequestParam("query") String query,
			@RequestParam("userId") String userId, @RequestParam("type") Integer type,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size) {
		if (page == null) {
			page = 0;
		}
		if (size == null) {
			size = 10;
		}
		return notificationService.findByUserIdAndType(query, userId, type, page.intValue(), size.intValue());
	}

	@RequestMapping(value = "/get")
	public OmniNotification get(@RequestParam("notificationId") Long notificationId, @RequestParam("cif") String cif) {
		return notificationService.get(notificationId, cif);
	}

	@RequestMapping(value = "/countUnread")
	public Integer countUnread(@RequestParam("cif") String cif,
			@RequestParam("durationInDays") Integer durationInDays) {
		Calendar calendar = Calendar.getInstance();
		Date toDate = calendar.getTime();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - durationInDays);
		Date fromDate = calendar.getTime();
		return notificationService.countUnread(cif, fromDate, toDate);
	}
}
