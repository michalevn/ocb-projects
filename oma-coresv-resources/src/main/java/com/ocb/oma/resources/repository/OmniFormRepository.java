package com.ocb.oma.resources.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniForm;

@RepositoryRestResource(collectionResourceRel = "app-forms", path = "app-forms")
public interface OmniFormRepository extends PagingAndSortingRepository<OmniForm, Long> {

	@RestResource(path = "findByUUID")
	@Query("SELECT o FROM OmniForm o WHERE o.uuid=:uuid")
	OmniForm findByUUID(@Param("uuid") String uuid);

}
