package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.TuitionFeeObject;

@RepositoryRestResource(collectionResourceRel = "tuitionFeeObject", path = "tuitionFeeObject")
public interface TuitionFeeObjectRepository extends PagingAndSortingRepository<TuitionFeeObject, Long> {

	@RestResource(path = "findByGroupAndActive")
	@Query("SELECT o FROM TuitionFeeObject o WHERE o.groupCode=:groupCode and o.active=:active")
	List<TuitionFeeObject> findByGroupAndActive(@Param("groupCode") String groupCode, @Param("active") Boolean active,
			Pageable pageable);

}
