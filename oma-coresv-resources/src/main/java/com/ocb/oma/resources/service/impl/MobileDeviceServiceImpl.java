/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.repository.OmniMobileDeviceRepository;
import com.ocb.oma.resources.service.MobileDeviceService;

/**
 * @author docv
 *
 */
@Service
@Transactional
public class MobileDeviceServiceImpl implements MobileDeviceService {

	@Autowired
	private OmniMobileDeviceRepository mobileDeviceRepo;

	@Override
	public Boolean registerDevice(OmniMobileDevice mobileDevice) {
		OmniMobileDevice omniMobileDevice = mobileDeviceRepo.findByDeviceId(mobileDevice.getDeviceId());
		if (omniMobileDevice == null) {
			mobileDeviceRepo.save(mobileDevice);
		} else {
			omniMobileDevice.setDeviceBrand(mobileDevice.getDeviceBrand());
			omniMobileDevice.setDeviceModal(mobileDevice.getDeviceModal());
			omniMobileDevice.setDeviceName(mobileDevice.getDeviceName());
			omniMobileDevice.setDeviceToken(mobileDevice.getDeviceToken());
			omniMobileDevice.setOsType(mobileDevice.getOsType());
			omniMobileDevice.setVersion(mobileDevice.getVersion());
			mobileDeviceRepo.save(omniMobileDevice);
		}
		return true;
	}
}
