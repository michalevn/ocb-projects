package com.ocb.oma.resources.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniMobileDevice;

@RepositoryRestResource(collectionResourceRel = "omniMobileDevice", path = "omniMobileDevice")
public interface OmniMobileDeviceRepository extends PagingAndSortingRepository<OmniMobileDevice, Long> {

	@RestResource(path = "findByDeviceId")
	@Query("SELECT o FROM OmniMobileDevice o WHERE o.deviceId = :deviceId")
	OmniMobileDevice findByDeviceId(String deviceId);

	@RestResource(path = "findByDeviceIds")
	@Query("SELECT o FROM OmniMobileDevice o WHERE o.deviceId IN :deviceIds")
	List<OmniMobileDevice> findByDeviceIds(Collection<String> deviceIds);

}
