package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ocb.oma.resources.model.OmniBankInfo;

@RepositoryRestResource(collectionResourceRel = "wellcomeBanner", path = "wellcomeBanner")
public interface OmniBankInfoRepository extends PagingAndSortingRepository<OmniBankInfo, Long> {

	@Query("select p from OmniBankInfo p order by p.orderNumber asc ")
	List<OmniBankInfo> findAllByOrder();

	@Query("SELECT P.bankCode FROM OmniBankInfo P WHERE P.citadCode = :citadCode")
	String findBankCodeFromCitadCode(@Param("citadCode") String citadCode);

	@Query("SELECT P FROM OmniBankInfo P WHERE P.bankCode = :bankCode")
	OmniBankInfo findByBankCode(String bankCode);
	
	@Query("SELECT P FROM OmniBankInfo P WHERE P.napasCode = :codeNapas")
	OmniBankInfo findByCodeNapas(@Param("codeNapas") String codeNapas);

}
