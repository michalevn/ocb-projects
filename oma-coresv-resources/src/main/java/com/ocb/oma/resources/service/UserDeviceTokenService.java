/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.List;

import com.ocb.oma.resources.model.UserDeviceToken;

/**
 * @author phuhoang
 *
 */
public interface UserDeviceTokenService {

	/**
	 * @param cif
	 * @param mobileDevice
	 * @param enable
	 * @return
	 */
	Boolean updateUserDeviceStatus(String cif, String deviceId, Integer status);

	/**
	 * dang ky 1 device
	 * 
	 * @param userDeviceToken
	 * @return
	 */
	void registerUserDevice(UserDeviceToken userDeviceToken);

	/**
	 * Lay danh sach UserDeviceToken theo cif
	 * 
	 * @param cif
	 * @return
	 */
	List<UserDeviceToken> findUserDevices(String cif);

	/**
	 * Lay thong tin nguoi dung theo device
	 * 
	 * @param cif
	 * @param deviceId
	 * @return
	 */
	UserDeviceToken activeUserDevice(String cif, String deviceId, String deviceToken);

	Boolean deactivate(String cif);

	Boolean isActiveUserDevice(String cif, String deviceId);

	UserDeviceToken getUserDevice(String cif, String deviceId);
}
