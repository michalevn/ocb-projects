/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.resources.model.OmniBankCitad;
import com.ocb.oma.resources.model.OmniBankInfo;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.resources.repository.OmniBankCitadRepository;
import com.ocb.oma.resources.repository.OmniBankInfoRepository;
import com.ocb.oma.resources.repository.OmniGiftCardReceiverRepository;
import com.ocb.oma.resources.repository.OmniGiftCardRepository;
import com.ocb.oma.resources.service.BankService;

/**
 * @author phuhoang
 *
 */
@Service
public class BankServiceImpl implements BankService {

	@Autowired
	private OmniBankInfoRepository bankInfoRepos;

	@Autowired
	private OmniBankCitadRepository citadRepository;

	@Autowired
	private OmniGiftCardRepository giftCardRepository;

	@Autowired
	private OmniGiftCardReceiverRepository giftCardReceiverRepository;

	@Autowired
	private ObjectMapper objectMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.resources.service.BankService#getBankInfos()
	 */
	@Override
	public List<BankInfoDTO> getBankInfos() {
		// TODO Auto-generated method stub
		List<BankInfoDTO> result = new ArrayList<>();
		List<OmniBankInfo> lst = bankInfoRepos.findAllByOrder();
		for (OmniBankInfo omniBankInfo : lst) {
			BankInfoDTO dto = new BankInfoDTO();
			// dto.setBankId(omniBankInfo.getBankId());
			dto.setBankName(omniBankInfo.getBankName());
			dto.setCodeCitad(omniBankInfo.getCitadCode());
			dto.setCodeNapas(omniBankInfo.getNapasCode());
			dto.setBankCode(omniBankInfo.getBankCode());
			dto.setIsInternal(omniBankInfo.getInternalBank());
			result.add(dto);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.resources.service.BankService#searchBankCitad(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<BankInfoDTO> searchBankCitad(String bankCode, String provinceCode) {
		// TODO Auto-generated method stub
		List<BankInfoDTO> results = new ArrayList<>();
		List<OmniBankCitad> lst = null;
		if (provinceCode != null && provinceCode.isEmpty() == false && (bankCode == null || bankCode.isEmpty())) {
			lst = citadRepository.findByProvinceCode(provinceCode);
		}
		if (provinceCode != null && provinceCode.isEmpty() == false
				&& (bankCode != null && bankCode.isEmpty() == false)) {
			lst = citadRepository.findByProvinceCodeAndBankCode(provinceCode, bankCode);
		}
		if (lst == null) {
			return results;
		}
		for (OmniBankCitad entity : lst) {
			BankInfoDTO dto = new BankInfoDTO(entity.getBankCode(), entity.getBank().getBankName(),
					entity.getCitadCode(), null, entity.getProvinceCode(), entity.getBankBranchCode(),
					entity.getBankBranchName(), false);
			results.add(dto);

		}
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ocb.oma.resources.service.BankService#findByBankCitadId(java.lang.Long)
	 */
	@Override
	public String getByBankCitadId(Long bankCitadCode) {
		String bankCode = bankInfoRepos.findBankCodeFromCitadCode(bankCitadCode.toString());
		return bankCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.resources.service.BankService#sendGiftCard(com.ocb.oma.dto.
	 * GiftCardInsertDTO)
	 */
	@Override
	public void sendGiftCard(GiftCardInsertDTO giftCardInsertDTO) {
		OmniGiftCard persisted = giftCardRepository.save(giftCardInsertDTO.getGiftCard());
		List<OmniGiftCardReceiver> lstOmniGiftCardReceiver = giftCardInsertDTO.getReceivers();
		for (int i = 0; i < lstOmniGiftCardReceiver.size(); i++) {
			OmniGiftCardReceiver omniGiftCardReceiver = lstOmniGiftCardReceiver.get(i);
			omniGiftCardReceiver.setGiftCardId(persisted.getGiftCardId());
			giftCardReceiverRepository.save(omniGiftCardReceiver);
		}
	}

	@Override
	public OmniBankCitad getByBankInfoByBranchCode(String branchCode) {
		OmniBankCitad bankCitad = citadRepository.getByBankInfoByBranchCode(branchCode);
		if (bankCitad == null) {
			return null;
		}
		OmniBankInfo bank = bankCitad.getBank();
		OmniBankInfo newBank = new OmniBankInfo();
		try {
			BeanUtilsBean.getInstance().copyProperties(newBank, bank);
		} catch (Exception e) {
		}
		newBank.setBankCitads(new ArrayList<>());
		bankCitad.setBank(newBank);
		return bankCitad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ocb.oma.resources.service.BankService#getBankByCodeNapas(java.lang.
	 * String)
	 */
	@Override
	public BankInfoDTO getBankByCodeNapas(String codeNapas) {
		OmniBankInfo omniBankInfo = bankInfoRepos.findByCodeNapas(codeNapas);
		BankInfoDTO dto = new BankInfoDTO();
		dto.setBankName(omniBankInfo.getBankName());
		dto.setCodeCitad(omniBankInfo.getCitadCode());
		dto.setCodeNapas(omniBankInfo.getNapasCode());
		dto.setBankCode(omniBankInfo.getBankCode());
		dto.setIsInternal(omniBankInfo.getInternalBank());
		return dto;
	}

}
