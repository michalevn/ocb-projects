package com.ocb.oma.resources.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.resources.model.OmniBankCitad;
import com.ocb.oma.resources.service.BankService;
import com.ocb.oma.resources.service.LovService;
import com.ocb.oma.resources.service.LinkService;

/**
 * 
 * @author Phu Hoang
 *
 */
@RestController
@RequestMapping("/resources")
public class ResourcesController extends BaseController {

	@Autowired
	private BankService bankService;

	@Autowired
	private LovService lovService;

	@Autowired
	private LinkService linkService;

	@RequestMapping(value = "/bankList")
	public List<BankInfoDTO> getBankList(HttpServletRequest request) {
		return bankService.getBankInfos();
	}

	@RequestMapping(value = "/listOfValue")
	public List<ListOfValueDTO> listOfValue(HttpServletRequest request, @RequestParam("lovType") String lovType) {
		return lovService.findListOfValue(lovType);
	}

	@RequestMapping(value = "/getLinksByGroup")
	public List<LinkDTO> getLinksByGroup(@RequestParam("group") String group) {
		return linkService.getLinksByGroup(group);
	}

	@RequestMapping(value = "/searchBankCitad")
	public List<BankInfoDTO> searchBankCitad(HttpServletRequest request, @RequestParam("bankCode") String bankCode,
			@RequestParam("provinceCode") String provinceCode) {
		return bankService.searchBankCitad(bankCode, provinceCode);
	}

	@RequestMapping(value = "/getByBankCitadId")
	public String getByBankCitadId(HttpServletRequest request, @RequestParam("bankCitadCode") Long bankCitadCode) {
		return bankService.getByBankCitadId(bankCitadCode);
	}

	@RequestMapping(value = "/getByCodeNapas")
	public BankInfoDTO getByCodeNapas(HttpServletRequest request, @RequestParam("codeNapas") String codeNapas) {
		return bankService.getBankByCodeNapas(codeNapas);
	}

	@RequestMapping(value = "/getByBankInfoByBranchCode")
	public OmniBankCitad getByBankInfoByBranchCode(@RequestParam("branchCode") String branchCode) {
		return bankService.getByBankInfoByBranchCode(branchCode);
	}

	/**
	 * sendGiftCard
	 * 
	 * @param request
	 * @param giftCardInsertDTO
	 */
	@RequestMapping(value = "/sendGiftCard")
	public void sendGiftCard(HttpServletRequest request, @RequestBody GiftCardInsertDTO giftCardInsertDTO) {
		bankService.sendGiftCard(giftCardInsertDTO);
	}

}
