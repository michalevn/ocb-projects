/**
 * 
 */
package com.ocb.oma.resources.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.resources.service.UserDeviceTokenService;

/**
 * @author phuhoang
 *
 */
@RestController
@RequestMapping("/userDevices")
public class UserDeviceController extends BaseController {

	@Autowired
	private UserDeviceTokenService userDeviceTokenService;

	@RequestMapping(value = "/findUserDevices")
	public List<UserDeviceToken> findUserDevices(@RequestParam("cif") String cif) {
		return userDeviceTokenService.findUserDevices(cif);
	}

	@RequestMapping(value = "/registerUserDevice")
	public Boolean registerUserDevice(@RequestBody UserDeviceToken userDeviceToken) {
		userDeviceTokenService.registerUserDevice(userDeviceToken);
		return true;
	}

	@RequestMapping(value = "/updateUserDeviceStatus")
	public Boolean updateUserDeviceStatus(@RequestBody UserDeviceToken input) {
		return userDeviceTokenService.updateUserDeviceStatus(input.getCif(), input.getDeviceId(), input.getStatus());
	}

	@RequestMapping(value = "/deactivate")
	public Boolean deactivate(@RequestParam("cif") String cif) {
		return userDeviceTokenService.deactivate(cif);
	}

	@RequestMapping(value = "/activeUserDevice")
	public UserDeviceToken activeUserDevice(@RequestParam("cif") String cif, @RequestParam("deviceId") String deviceId,
			@RequestParam(name = "deviceToken", required = false) String deviceToken) {
		return userDeviceTokenService.activeUserDevice(cif, deviceId, deviceToken);
	}

	@RequestMapping(value = "/isActiveUserDevice")
	public Boolean isActiveUserDevice(@RequestParam("cif") String cif, @RequestParam("deviceId") String deviceId) {
		return userDeviceTokenService.isActiveUserDevice(cif, deviceId);
	}

	@RequestMapping(value = "/getUserDevice")
	public UserDeviceToken getUserDevice(@RequestParam("cif") String cif, @RequestParam("deviceId") String deviceId) {
		return userDeviceTokenService.getUserDevice(cif, deviceId);
	}

}
