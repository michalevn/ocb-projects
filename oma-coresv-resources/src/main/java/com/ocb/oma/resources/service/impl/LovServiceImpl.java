/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.ListOfValueDTO;
import com.ocb.oma.resources.constants.AppConstants;
import com.ocb.oma.resources.model.OmniLOV;
import com.ocb.oma.resources.repository.OmniBankLovRepository;
import com.ocb.oma.resources.service.LovService;

/**
 * @author phuhoang
 *
 */
@Service
public class LovServiceImpl implements LovService {

	@Autowired
	private OmniBankLovRepository lovRepository;

	/*
	 * @see
	 * com.ocb.oma.resources.service.LovService#findListOfValue(java.lang.String)
	 */
	@Override
	public List<ListOfValueDTO> findListOfValue(String category) {
		List<OmniLOV> lst = lovRepository.findByType(category);
		List<ListOfValueDTO> result = new ArrayList<>();
		for (OmniLOV omniLOV : lst) {
			ListOfValueDTO dto = new ListOfValueDTO(omniLOV.getLovId(), omniLOV.getLovType(), omniLOV.getLovCode(), omniLOV.getLovNameVn(),
					omniLOV.getLovNameEl(), omniLOV.getOrderNumber());
			result.add(dto);
		}

		return result;
	}

	@Override
	public String findTemplate(String templateCode) {
		OmniLOV lov = lovRepository.findByTypeAndCode(AppConstants.FREE_MAKER_TEMPLATE, templateCode);
		if (lov == null) {
			return AppConstants.BLANK;
		}
		String strTemplate = lov.getLovNameVn();
		if (strTemplate == null) {
			return AppConstants.BLANK;
		}
		return strTemplate.trim();
	}

}
