
package com.ocb.oma.resources.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseController {

	@Autowired
	private MessageSource messageSource;

	public String getMessage(String code) {

		return messageSource.getMessage(code, null, code, Locale.ENGLISH);
	}

	@Autowired
	private ObjectMapper objectMapper;

	public ResponseEntity<?> getFinalResponse(Object input) {

		try {
			return ResponseEntity.ok(objectMapper.writeValueAsString(input));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
