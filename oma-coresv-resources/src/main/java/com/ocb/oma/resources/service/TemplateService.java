package com.ocb.oma.resources.service;

import java.util.Map;

public interface TemplateService {

	/**
	 * Xu ly template voi cac tham so params
	 * 
	 * @param template noi dung can xu ly. Co the la
	 *                 <ul>
	 *                 <li>ten file chua noi dung template can duoc xu ly, file nay
	 *                 nam o thu muc duoc cau hinh boi key: ${freemaker.templatesPath},
	 *                 vi du: template = hello hay template = hello.ftl</li>
	 *                 <li>LOV ung voi type = FREE_MAKER_TEMPLATE va lovCode =
	 *                 $template</li>
	 *                 <li>freemaker template, vi du: &lt;h1&gt;Hello
	 *                 ${user}&lt;/h1&gt</li>
	 *                 </ul>
	 * 
	 * @param params   cac tham so duoc dung trong template
	 * @param type     duong dan luu tru templaye
	 * @return ket qua duoc xu ly
	 */
	String format(String template, Map<String, ? extends Object> params, String type);

}
