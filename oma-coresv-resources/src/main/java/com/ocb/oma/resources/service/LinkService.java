/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.List;

import com.ocb.oma.dto.LinkDTO;

/**
 * @author docv
 *
 */
public interface LinkService {

	List<LinkDTO> getLinksByGroup(String group);
	
}
