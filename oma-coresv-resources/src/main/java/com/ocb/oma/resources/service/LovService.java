/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.List;

import com.ocb.oma.dto.ListOfValueDTO;

/**
 * @author phuhoang
 *
 */
public interface LovService {

	List<ListOfValueDTO> findListOfValue(String category);

	String findTemplate(String templateCode);
}
