package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ocb.oma.resources.model.OmniBankCitad;

@RepositoryRestResource(collectionResourceRel = "bankCitad", path = "bankCitad")
public interface OmniBankCitadRepository extends PagingAndSortingRepository<OmniBankCitad, Long> {

	@Query("select p from OmniBankCitad p where  p.provinceCode=:provinceCode ")
	List<OmniBankCitad> findByProvinceCode(@Param("provinceCode") String provinceCode);

	@Query("select p from OmniBankCitad p where p.provinceCode=:provinceCode and p.bankCode=:bankCode order by p.bank.orderNumber ")
	List<OmniBankCitad> findByProvinceCodeAndBankCode(@Param("provinceCode") String provinceCode,
			@Param("bankCode") String bankCode);

	@Query("select p from OmniBankCitad p where p.bankBranchCode=:bankBranchCode")
	OmniBankCitad getByBankInfoByBranchCode(@Param("bankBranchCode") String branchCode);

}
