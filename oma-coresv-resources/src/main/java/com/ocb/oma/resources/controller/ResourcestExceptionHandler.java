/**
 * @author Phu Hoang
 */
package com.ocb.oma.resources.controller;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.OmaExceptionDTO;
import com.ocb.oma.exception.OmaException;

/**
 * @author Phu Hoang
 *
 */
@ControllerAdvice
@RestController
public class ResourcestExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourcestExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<?> handleUnknowException(Exception ex, WebRequest request) {
		OmaExceptionDTO dto = new OmaExceptionDTO();
		dto.setErrorCode(MessageConstant.RESOURCE_SERVICE_ERROR);
		dto.setDescription(ex.getMessage());
		LOGGER.error("Unkown exception : ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
	}

	// TransactionSystemException

	@ExceptionHandler(TransactionSystemException.class)
	public final ResponseEntity<?> handleTransactionSystemException(TransactionSystemException ex, WebRequest request) {
		Throwable rootCause = ex.getRootCause();

		OmaExceptionDTO dto = new OmaExceptionDTO();
		dto.setErrorCode(MessageConstant.RESOURCE_SERVICE_ERROR);
		dto.setDescription(ex.getMessage());
		LOGGER.error("TransactionSystemException : ", ex);
		if (rootCause instanceof ValidationException) {
			String message = ((ValidationException) rootCause).getMessage();
			dto.setDescription(message);
			dto.setErrorCode(MessageConstant.CONSTRAIN_DATABASE_ERROR);
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
	}

	@ExceptionHandler(OmaException.class)
	public final ResponseEntity<?> handleOmaException(OmaException ex, WebRequest request) {

		OmaExceptionDTO dto = new OmaExceptionDTO();
		dto.setErrorCode(ex.getMessage());
		dto.setDescription(ex.getDesc());
		LOGGER.error("Logic exception : ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dto);
	}

}
