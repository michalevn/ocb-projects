/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;
import com.ocb.oma.resources.repository.TuitionFeeGroupRepository;
import com.ocb.oma.resources.repository.TuitionFeeObjectRepository;
import com.ocb.oma.resources.service.TuitionFeeService;

/**
 * @author phuhoang
 *
 */
@Service
public class TuitionFeeServiceImpl implements TuitionFeeService {

	@Autowired
	private TuitionFeeGroupRepository tuitionFeeGroupRepository;

	@Autowired
	private TuitionFeeObjectRepository tuitionFeeObjectRepository;

	@Override
	public List<TuitionFeeGroup> findGroups() {
		return tuitionFeeGroupRepository.findByActive(true);
	}

	@Override
	public List<TuitionFeeObject> findByGroup(String groupCode, int page, int size) {
		Pageable pageable = PageRequest.of(page, size);
		return tuitionFeeObjectRepository.findByGroupAndActive(groupCode, true, pageable);
	}

}
