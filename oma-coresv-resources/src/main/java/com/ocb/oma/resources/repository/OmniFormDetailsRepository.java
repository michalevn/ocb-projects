package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniFormDetails;

@RepositoryRestResource(collectionResourceRel = "app-forms", path = "app-forms")
public interface OmniFormDetailsRepository extends PagingAndSortingRepository<OmniFormDetails, Long> {

	@RestResource(path = "findByFormId")
	@Query("SELECT o FROM OmniFormDetails o WHERE o.formId = :formId")
	List<OmniFormDetails> findByFormId(@Param("formId") Long formId);

}
