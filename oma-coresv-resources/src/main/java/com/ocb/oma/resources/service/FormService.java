/**
 * 
 */
package com.ocb.oma.resources.service;

import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.resources.model.OmniForm;

/**
 * @author docv
 *
 */
public interface FormService {

	/**
	 * Tao form
	 * 
	 * @param form
	 * @return Form UUID
	 */
	OmniForm create(FormDTO formDTO);
	/**
	 * 
	 * Lay form theo Uuid
	 * 
	 * @param uuid
	 * @return
	 */
	FormDTO findByUuid(String uuid);

}
