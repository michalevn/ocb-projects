/**
 * 
 */
package com.ocb.oma.resources.service;

import javax.mail.MessagingException;

import com.ocb.oma.dto.MailMessageDTO;

/**
 * @author phuhoang
 *
 */
public interface MailService {

	String send(MailMessageDTO messageDTO) throws MessagingException;

}
