/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.List;

import com.ocb.oma.dto.BankInfoDTO;
import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.resources.model.OmniBankCitad;

/**
 * @author phuhoang
 *
 */
public interface BankService {

	List<BankInfoDTO> getBankInfos();

	List<BankInfoDTO> searchBankCitad(String bankCode, String provinceCode);

	String getByBankCitadId(Long bankCitadCode);
	
	BankInfoDTO getBankByCodeNapas(String codeNapas);

	void sendGiftCard(GiftCardInsertDTO giftCardInsertDTO);

	OmniBankCitad getByBankInfoByBranchCode(String branchCode);
}
