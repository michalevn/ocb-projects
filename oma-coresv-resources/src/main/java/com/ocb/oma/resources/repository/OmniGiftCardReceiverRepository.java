package com.ocb.oma.resources.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ocb.oma.resources.model.OmniGiftCardReceiver;

@RepositoryRestResource(collectionResourceRel = "OmniGiftCardReceiver", path = "OmniGiftCardReceiver")
public interface OmniGiftCardReceiverRepository extends PagingAndSortingRepository<OmniGiftCardReceiver, Long> {

}
