package com.ocb.oma.resources.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniActivity;

@RepositoryRestResource(collectionResourceRel = "app-activities", path = "app-activities")
public interface OmniActivityRepository extends PagingAndSortingRepository<OmniActivity, Long> {

	@RestResource(path = "findByUserActionTarget")
	@Query("SELECT a FROM OmniActivity a WHERE a.userId = :userId AND a.action = :action AND a.targetType = :targetType AND a.target = :target")
	OmniActivity findByUserActionTarget(@Param("userId") String userId, @Param("action") Integer action,
			@Param("targetType") Integer targetType, @Param("target") Long target);

	@RestResource(path = "findByUserActionTargets")
	@Query("SELECT a FROM OmniActivity a WHERE a.userId = :userId AND a.action = :action AND a.targetType = :targetType AND a.target IN :targets")
	List<OmniActivity> findByUserActionTargets(@Param("userId") String userId, @Param("action") Integer action,
			@Param("targetType") Integer targetType, @Param("targets") List<Long> targets);

	@RestResource(path = "findByUserActionTargets")
	@Query("SELECT COUNT(a) FROM OmniActivity a WHERE a.userId = :userId AND a.action = :action AND a.targetType = :targetType AND a.createdDate BETWEEN :fromDate AND :toDate")
	Integer countByUserAction(@Param("userId") String userId, @Param("action") Integer action,
			@Param("targetType") Integer targetType, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
}
