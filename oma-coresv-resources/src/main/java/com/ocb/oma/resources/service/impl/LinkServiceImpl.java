/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.LinkDTO;
import com.ocb.oma.resources.model.OmniLink;
import com.ocb.oma.resources.repository.OmniLinkRepository;
import com.ocb.oma.resources.service.LinkService;

/**
 * @author phuhoang
 *
 */
@Service
public class LinkServiceImpl implements LinkService {

	@Autowired
	private OmniLinkRepository repository;

	@Override
	public List<LinkDTO> getLinksByGroup(String group) {
		List<LinkDTO> dtos = new ArrayList<>();
		List<OmniLink> omniLinks = null;
		if (StringUtils.isBlank(group)) {
			omniLinks = repository.getLinks();
		} else {
			omniLinks = repository.getLinksByGroup(group);
		}
		if (omniLinks != null) {
			for (OmniLink omniLink : omniLinks) {
				dtos.add(new LinkDTO(omniLink));
			}
		}
		return dtos;
	}

}
