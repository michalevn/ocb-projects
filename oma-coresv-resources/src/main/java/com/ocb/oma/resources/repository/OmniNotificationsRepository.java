package com.ocb.oma.resources.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.model.projection.OmniNotificationProjection;

@RepositoryRestResource(collectionResourceRel = "app-notifications", path = "app-notifications", excerptProjection = OmniNotificationProjection.class)
public interface OmniNotificationsRepository extends PagingAndSortingRepository<OmniNotification, Long> {

	@RestResource(path = "findByUserIdAndType")
	@Query("SELECT n FROM OmniNotification n WHERE n.titleVn like LOWER(:query) AND n.userId=:userId AND n.type=:type ORDER BY n.createdDate DESC")
	List<OmniNotification> findByUserIdAndType(@Param("query") String query, @Param("userId") String userId,
			@Param("type") String type, Pageable pageable);

	@RestResource(path = "findByUserIdAndType")
	@Query("SELECT n FROM OmniNotification n WHERE n.userId=:userId AND n.type=:type ORDER BY n.createdDate DESC")
	List<OmniNotification> findByUserIdAndType(@Param("userId") String userId, @Param("type") String type,
			Pageable pageable);

	@RestResource(path = "count")
	@Query("SELECT COUNT(n) FROM OmniNotification n WHERE n.userId=:userId AND n.type=:type AND n.createdDate BETWEEN :fromDate AND :toDate")
	Integer count(@Param("userId") String userId, @Param("type") String type, Date fromDate, Date toDate);
}
