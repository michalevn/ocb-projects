/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.Date;
import java.util.List;

import com.ocb.oma.resources.model.OmniNotification;

/**
 * @author docv
 *
 */
public interface NotificationsService {

	/**
	 * Day notifications den nguoi dung.
	 * 
	 * @param notification
	 */
	void push(OmniNotification notification);

	/**
	 * Lay notification theo ID
	 * 
	 * @param notificationId
	 * @param cif
	 * @return OmniNotification
	 */
	OmniNotification get(Long notificationId, String cif);

	/**
	 * Tim cac notifications theo type va nguoi dung
	 * 
	 * @param type
	 * @param userId
	 * @param type2
	 * @return
	 */
	List<OmniNotification> findByUserIdAndType(String query, String userId, Integer type, int page, int size);

	/**
	 * Danh dau da doc
	 * 
	 * @param cif
	 * @param notificationIds
	 */
	void maskAsRead(String cif, List<Long> notificationIds);

	/**
	 * Danh dau chua doc
	 * 
	 * @param cif
	 * @param notificationIds
	 */
	void maskAsUnread(String cif, List<Long> notificationIds);

	/**
	 * Xoa thong bao
	 * 
	 * @param cif
	 * @param notificationIds
	 */
	void delete(String cif, List<Long> notificationIds);

	/**
	 * Dem so luong
	 * 
	 * @param cif
	 * @return
	 */
	Integer count(String cif, Date fromDate, Date toDate);

	/**
	 * Dem so luong chua doc
	 * 
	 * @param cif
	 * @return
	 */
	Integer countUnread(String cif, Date fromDate, Date toDate);
}
