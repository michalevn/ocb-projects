/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.List;

import com.ocb.oma.resources.model.TuitionFeeGroup;
import com.ocb.oma.resources.model.TuitionFeeObject;

/**
 * @author docv
 *
 */
public interface TuitionFeeService {

	List<TuitionFeeGroup> findGroups();

	List<TuitionFeeObject> findByGroup(String groupCode, int page, int size);

}
