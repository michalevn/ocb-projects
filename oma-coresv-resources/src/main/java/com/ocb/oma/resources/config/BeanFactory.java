package com.ocb.oma.resources.config;

import org.springframework.context.annotation.Configuration;

import com.ocb.oma.resources.OmaCoresvResourcesApplication;
import com.ocb.oma.resources.constants.AppConstants;

import freemarker.template.TemplateExceptionHandler;

@Configuration
public class BeanFactory {

	// @Bean("freemarkerConfig")
	public freemarker.template.Configuration getFreeMakerConfiguration() {
		freemarker.template.Configuration configuration = new freemarker.template.Configuration(
				freemarker.template.Configuration.VERSION_2_3_28);
		configuration.setClassForTemplateLoading(OmaCoresvResourcesApplication.class, AppConstants.TEMPLATE_PATH);
		configuration.setDefaultEncoding(AppConstants.UTF_8);
		configuration.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
		return configuration;
	}

}
