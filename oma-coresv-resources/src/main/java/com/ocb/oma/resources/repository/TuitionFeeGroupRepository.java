package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.TuitionFeeGroup;

@RepositoryRestResource(collectionResourceRel = "tuitionFeeGroup", path = "tuitionFeeGroup")
public interface TuitionFeeGroupRepository extends PagingAndSortingRepository<TuitionFeeGroup, Long> {

	@RestResource(path = "findByActive")
	@Query("SELECT o FROM TuitionFeeGroup o WHERE o.active=:active")
	List<TuitionFeeGroup> findByActive(@Param("active") Boolean active);

}
