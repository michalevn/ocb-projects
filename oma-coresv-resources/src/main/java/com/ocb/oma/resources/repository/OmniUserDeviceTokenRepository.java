package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniCardType;

@RepositoryRestResource(collectionResourceRel = "app-cardTypes", path = "app-cardTypes")
public interface OmniUserDeviceTokenRepository extends PagingAndSortingRepository<OmniCardType, Long> {

	@RestResource(path = "findByActive")
	@Query("SELECT oct FROM OmniCardType oct WHERE oct.active=:active")
	List<OmniCardType> findByActive(@Param("active") Boolean active);

}
