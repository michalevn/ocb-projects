/**
 * 
 */
package com.ocb.oma.resources.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.MailMessageDTO;
import com.ocb.oma.resources.model.OmniForm;
import com.ocb.oma.resources.service.FormService;
import com.ocb.oma.resources.service.MailService;

/**
 * @author docv
 *
 */
@Controller
@RestController
@RequestMapping("/forms")
public class FormController extends BaseController {

	@Autowired
	protected FormService formService;

	@Autowired
	protected MailService mailService;

	@Autowired
	protected ObjectMapper objectMapper;

	@Value("${omni.mailFrom}")
	private String mailFrom;

	@Value("${omni.mailTo}")
	private String mailTo;

//	@Value("mail.support.receivers")
//	private String receivers;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(@RequestBody FormDTO formDTO) {
		OmniForm form = formService.create(formDTO);

		return form.getUuid();
	}

	@RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
	public FormDTO findByUuid(@PathVariable("uuid") String uuid) {
		return formService.findByUuid(uuid);
	}

	@RequestMapping(value = "sendMailCus", method = RequestMethod.POST)
	public String sendMailCus(@RequestBody FormDTO formDTO) throws MessagingException {

		MailMessageDTO messageDTO = new MailMessageDTO();
		messageDTO.setSubject(formDTO.getTitle());
		messageDTO.setFrom(mailFrom);
		messageDTO.setTo(mailTo);// mailTo
		messageDTO.setContents(formDTO.getDescription());
		messageDTO.setRepllyTo(formDTO.getUserEmail());
		String resurl = mailService.send(messageDTO);
		return resurl;
	}
}
