package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniGiftCard;

@RepositoryRestResource(collectionResourceRel = "OmniGiftCard", path = "OmniGiftCard")
public interface OmniGiftCardRepository extends PagingAndSortingRepository<OmniGiftCard, Long> {

	@RestResource(path = "findOmniGiftCard")
	@Query("select p from OmniGiftCard p where  p.giftCardId=:giftCardId order by p.giftCardId ")
	List<OmniGiftCard> findOmniGiftCard(@Param("giftCardId") String giftCardId);
	
}
