package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.resources.model.projection.UserDeviceTokenProjection;

@RepositoryRestResource(collectionResourceRel = "userDeviceToken", path = "userDeviceToken", excerptProjection = UserDeviceTokenProjection.class)
public interface UserDeviceTokenRepository extends PagingAndSortingRepository<UserDeviceToken, Long> {

	@RestResource(path = "findByDeviceToken")
	@Query("SELECT o FROM UserDeviceToken o WHERE o.deviceToken = :deviceToken")
	List<UserDeviceToken> findByDeviceToken(@Param("deviceToken") String deviceToken);

	@RestResource(path = "findByCif")
	@Query("SELECT p FROM UserDeviceToken p WHERE p.cif = :cif")
	List<UserDeviceToken> findByCif(@Param("cif") String cif);

	@RestResource(path = "findByCifAndDeviceId")
	@Query("SELECT p FROM UserDeviceToken p WHERE p.cif = :cif AND p.deviceId = :deviceId")
	UserDeviceToken findByCifAndDeviceId(@Param("cif") String cif, @Param("deviceId") String deviceId);

	@RestResource(path = "findMobileDeviceByDeviceId")
	@Query("SELECT p FROM OmniMobileDevice p WHERE p.deviceId = :deviceId")
	OmniMobileDevice findMobileDeviceByDeviceId(@Param("deviceId") String deviceId);

	@RestResource(path = "findByCifOrDeviceToken")
	@Query("SELECT ud FROM UserDeviceToken ud WHERE ud.cif = :cif OR ud.deviceId = :deviceId")
	List<UserDeviceToken> findByCifOrDeviceId(@Param("cif") String cif, @Param("deviceId") String deviceId);

}
