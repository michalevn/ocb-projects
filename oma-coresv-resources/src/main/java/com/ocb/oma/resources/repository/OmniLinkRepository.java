package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniLink;

@RepositoryRestResource(collectionResourceRel = "omniLink", path = "omniLink")
public interface OmniLinkRepository extends PagingAndSortingRepository<OmniLink, Long> {

	@RestResource(path = "getLinksByGroup")
	@Query("SELECT o FROM OmniLink o WHERE o.group = :group AND o.active = true")
	List<OmniLink> getLinksByGroup(@Param("group") String group);
	
	@RestResource(path = "getLinks")
	@Query("SELECT o FROM OmniLink o WHERE o.active = true")
	List<OmniLink> getLinks();

}
