/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.resources.repository.OmniMobileDeviceRepository;
import com.ocb.oma.resources.repository.UserDeviceTokenRepository;
import com.ocb.oma.resources.service.UserDeviceTokenService;

/**
 * @author docv
 *
 */
@Service
@Transactional
public class UserDeviceTokenServiceImpl implements UserDeviceTokenService {

	@Autowired
	private UserDeviceTokenRepository userDeviceRepository;

	@Autowired
	private OmniMobileDeviceRepository mobileDeviceRepository;

	private static final Log LOG = LogFactory.getLog(UserDeviceTokenServiceImpl.class);

	@Override
	public List<UserDeviceToken> findUserDevices(String cif) {
		List<UserDeviceToken> userDevices = userDeviceRepository.findByCif(cif);
		if (userDevices != null && !userDevices.isEmpty()) {
			Map<String, UserDeviceToken> userDeviceMap = new HashMap<>();
			for (UserDeviceToken userDeviceToken : userDevices) {
				userDeviceMap.put(userDeviceToken.getDeviceId(), userDeviceToken);
			}
			List<OmniMobileDevice> mobileDevices = mobileDeviceRepository.findByDeviceIds(userDeviceMap.keySet());
			for (OmniMobileDevice mobileDevice : mobileDevices) {
				String deviceId = mobileDevice.getDeviceId();
				if (userDeviceMap.containsKey(deviceId)) {
					userDeviceMap.get(mobileDevice.getDeviceId()).setMobileDevice(mobileDevice);
				}
			}
		}
		return userDevices;
	}

	@Override
	public UserDeviceToken activeUserDevice(String cif, String deviceId, String deviceToken) {
		UserDeviceToken userDevice = userDeviceRepository.findByCifAndDeviceId(cif, deviceId);
		activeUserDevice(userDevice);
		return userDevice;
	}

	private void activeUserDevice(UserDeviceToken userDevice) {
		if (userDevice != null) {

			OmniMobileDevice mobileDevice = mobileDeviceRepository.findByDeviceId(userDevice.getDeviceId());
			userDevice.setMobileDevice(mobileDevice);

			List<UserDeviceToken> userDevices = userDeviceRepository.findByCifOrDeviceId(userDevice.getCif(),
					userDevice.getDeviceId());
			for (UserDeviceToken udt : userDevices) {
				if (!udt.getCif().equals(userDevice.getCif()) || !udt.getDeviceId().equals(userDevice.getDeviceId())) {
					udt.setActive(false);
					udt.setDeviceToken("");
					userDeviceRepository.save(udt);
				}
			}

			userDevice.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
			userDevice.setUpdatedBy(userDevice.getCif());
			userDevice.setLastLoginDate(new Date());
			userDevice.setActive(true);

			if (StringUtils.isBlank(userDevice.getDeviceToken())) {
				userDevice.setDeviceToken(mobileDevice.getDeviceToken());
			}

			userDeviceRepository.save(userDevice);
		}
	}

	@Override
	public Boolean updateUserDeviceStatus(String cif, String deviceId, Integer status) {
		UserDeviceToken userDeviceToken = userDeviceRepository.findByCifAndDeviceId(cif, deviceId);
		if (userDeviceToken == null) {
			throw new OmaException(MessageConstant.USER_DEVICE_NOT_FOUND);
		}
		if (!userDeviceToken.getCif().equals(cif)) {
			throw new OmaException(MessageConstant.CAN_NOT_UPDATE_OTHER_USER_DATA);
		}
		userDeviceToken.setStatus(status);
		userDeviceRepository.save(userDeviceToken);
		return true;
	}

	@Override
	public void registerUserDevice(UserDeviceToken userDevice) {

		if (StringUtils.isBlank(userDevice.getDeviceId())) {
			throw new OmaException(MessageConstant.MOBILE_DEVICE_REQUIRED);
		}

		OmniMobileDevice mobileDevice = mobileDeviceRepository.findByDeviceId(userDevice.getDeviceId());
		if (mobileDevice == null) {
			mobileDevice = new OmniMobileDevice();
			mobileDevice.setDeviceId(userDevice.getDeviceId());
			mobileDevice.setDeviceToken(userDevice.getDeviceToken());
			mobileDevice.setActive(true);
			mobileDeviceRepository.save(mobileDevice);
			// throw new OmaException(MessageConstant.MOBILE_DEVICE_NOT_REGISTERED);
		}

		Timestamp now = new Timestamp(System.currentTimeMillis());

		UserDeviceToken udt = userDeviceRepository.findByCifAndDeviceId(userDevice.getCif(), userDevice.getDeviceId());
		if (udt == null) {
			udt = new UserDeviceToken();
			copyProperties(udt, userDevice);
			udt.setCreatedDate(now);
			udt.setStatus(1);
		}

		activeUserDevice(udt);

	}

	@Override
	public Boolean deactivate(String cif) {
		List<UserDeviceToken> userDevices = userDeviceRepository.findByCif(cif);
		for (UserDeviceToken userDevice : userDevices) {
			userDevice.setDeviceToken("");
			userDevice.setActive(false);
			userDeviceRepository.save(userDevice);
		}
		return true;
	}

	@Override
	public Boolean isActiveUserDevice(String cif, String deviceId) {
		UserDeviceToken userDevice = userDeviceRepository.findByCifAndDeviceId(cif, deviceId);
		if (userDevice == null || userDevice.getActive() == null) {
			return false;
		}
		return userDevice.getActive().booleanValue();
	}

	private void copyProperties(UserDeviceToken dest, UserDeviceToken orig) {
		try {
			BeanUtilsBean.getInstance().copyProperties(dest, orig);
		} catch (IllegalAccessException | InvocationTargetException e) {
			LOG.error(e, e);
		}
	}

	@Override
	public UserDeviceToken getUserDevice(String cif, String deviceId) {
		UserDeviceToken userDevice = userDeviceRepository.findByCifAndDeviceId(cif, deviceId);
		userDevice.setMobileDevice(userDeviceRepository.findMobileDeviceByDeviceId(deviceId));
		return userDevice;
	}
}
