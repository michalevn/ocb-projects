/**
 * 
 */
package com.ocb.oma.resources.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ocb.oma.resources.model.OmniMobileDevice;
import com.ocb.oma.resources.service.MobileDeviceService;

/**
 * @author docv
 *
 */
@RestController
@RequestMapping("/mobileDevices")
public class MobileDeviceController extends BaseController {

	@Autowired
	private MobileDeviceService mobileDeviceService;

	@RequestMapping(value = "/registerDevice")
	public Boolean registerDevice(@RequestBody OmniMobileDevice input) {
		return mobileDeviceService.registerDevice(input);
	}
}
