/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.FormDTO;
import com.ocb.oma.dto.FormDetailsDTO;
import com.ocb.oma.resources.model.OmniForm;
import com.ocb.oma.resources.model.OmniFormDetails;
import com.ocb.oma.resources.repository.OmniFormDetailsRepository;
import com.ocb.oma.resources.repository.OmniFormRepository;
import com.ocb.oma.resources.service.FormService;

/**
 * @author docv
 *
 */
@Service
public class FormServiceImpl implements FormService {

	@Autowired
	private OmniFormRepository formRepo;

	@Autowired
	private OmniFormDetailsRepository formDetailsRepo;

	@Override
	public OmniForm create(FormDTO formDTO) {
		OmniForm form = new OmniForm();
		form.setCreatedBy(formDTO.getCreatedBy());
		form.setCreatedDate(formDTO.getCreatedDate());
		form.setDescription(formDTO.getDescription());
		form.setTitle(formDTO.getTitle());
		form.setUpdatedBy(formDTO.getUpdatedBy());
		if (formDTO.getUpdatedDate() != null) {
			form.setUpdatedDate(new Timestamp(formDTO.getUpdatedDate().getTime()));
		}
		form.setUserEmail(formDTO.getUserEmail());
		form.setUserName(formDTO.getUserName());
		form.setUserPhone(formDTO.getUserPhone());

		form.setUuid(UUID.randomUUID().toString());

		formRepo.save(form);

		List<FormDetailsDTO> formDetailsDTOs = formDTO.getFormDetails();
		if (formDetailsDTOs != null) {
			for (FormDetailsDTO formDetailsDTO : formDetailsDTOs) {
				OmniFormDetails formDetails = new OmniFormDetails();
				formDetails.setFormId(form.getFormId());
				formDetails.setFieldName(formDetailsDTO.getFieldName());
				formDetails.setFieldType(formDetailsDTO.getFieldType());
				formDetails.setFieldValue(formDetailsDTO.getFieldValue());
				formDetailsRepo.save(formDetails);
			}
		}

		return form;
	}

	@Override
	public FormDTO findByUuid(String uuid) {
		if (uuid == null || (uuid = uuid.trim()).isEmpty()) {
			return null;
		}
		OmniForm form = formRepo.findByUUID(uuid);
		if (form == null) {
			return null;
		}

		FormDTO formDTO = new FormDTO();

		formDTO.setUuid(form.getUuid());
		formDTO.setTitle(form.getTitle());
		formDTO.setCreatedBy(form.getCreatedBy());
		formDTO.setCreatedDate(form.getCreatedDate());
		formDTO.setDescription(form.getDescription());
		formDTO.setUpdatedBy(form.getUpdatedBy());
		formDTO.setUpdatedDate(form.getUpdatedDate());
		formDTO.setUserEmail(form.getUserEmail());
		formDTO.setUserName(form.getUserName());
		formDTO.setUserPhone(form.getUserPhone());

		List<FormDetailsDTO> formDetailsDtos = new ArrayList<>();
		List<OmniFormDetails> formDetails = formDetailsRepo.findByFormId(form.getFormId());
		if (formDetails != null) {
			for (OmniFormDetails formDetail : formDetails) {
				FormDetailsDTO formDetailsDTO = new FormDetailsDTO();
				formDetailsDTO.setFieldName(formDetail.getFieldName());
				formDetailsDTO.setFieldType(formDetail.getFieldType());
				formDetailsDTO.setFieldValue(formDetail.getFieldValue());
				formDetailsDtos.add(formDetailsDTO);
			}
		}

		formDTO.setFormDetails(formDetailsDtos);

		return formDTO;
	}

}
