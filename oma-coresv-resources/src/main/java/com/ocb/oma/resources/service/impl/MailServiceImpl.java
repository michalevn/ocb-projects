/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.ocb.oma.dto.MailMessageDTO;
import com.ocb.oma.oomni.dto.Notification.NotificationType;
import com.ocb.oma.resources.service.MailService;
import com.ocb.oma.resources.service.TemplateService;

/**
 * @author docv
 *
 */
@Service
public class MailServiceImpl implements MailService {

	@Value("${freemaker.mailForm}")
	private String mailForm;

	@Autowired
	TemplateService freeMakerTemplateServiceImpl;

	@Autowired
	public JavaMailSender emailSender;

	//@Value("mail.support.receivers")
	private String[] receivers;

//	@Override
//	public String sendTest(MailMessageDTO messageDTO) {
//
//		// Create a Simple MailMessage.
//		SimpleMailMessage message = new SimpleMailMessage();
//
//		String[] bcc = {};
//		message.setBcc(bcc);
//		String[] cc = {};// docv@ocb.com.vn
//		message.setCc(cc);
//		message.setFrom(messageDTO.getFrom());
//		message.setReplyTo(messageDTO.getFrom());
//		Date sentDate = new Date();
//		message.setSentDate(sentDate);
//		message.setSubject(messageDTO.getSubject());
//		message.setText(messageDTO.getContents());
//		message.setTo(messageDTO.getTo());
//
//		// Send Message!
//		emailSender.send(message);
//		return "Send mail!";
//
//	}

	private static String formatDate(Date datetime) {
		if (datetime == null) {
			return "";
		}
		return new SimpleDateFormat("dd-mm-yyyy hh:mm:ss").format(datetime);
	}

	@Override
	public String send(MailMessageDTO messageDTO) throws MessagingException {

		String datetime = formatDate(new Date());
		String title = messageDTO.getSubject();
		String content = messageDTO.getContents();
		String attachment = "KHÔNG";
		String repllyTo = messageDTO.getRepllyTo();

		MimeMessage message = emailSender.createMimeMessage();

		boolean multipart = true;

		MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dateTime", datetime);
		params.put("title", title);
		params.put("contents", content);
		params.put("attachment", attachment);
		params.put("mailUser", repllyTo);
		String htmlMsg = freeMakerTemplateServiceImpl.format(mailForm, params, "mails");

		message.setContent(htmlMsg, "text/html");
		message.setFrom(messageDTO.getFrom());
		helper.setTo(messageDTO.getTo());

		helper.setSubject("OCB OMNI_Hỏi đáp_" + title);

		this.emailSender.send(message);

		return "Email Sent!";
	}

}
