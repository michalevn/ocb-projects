package com.ocb.oma.resources.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.ocb.oma.resources.OmaCoresvResourcesApplication;
import com.ocb.oma.resources.constants.AppConstants;
import com.ocb.oma.resources.service.LovService;
import com.ocb.oma.resources.service.TemplateService;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * @author docv
 *
 */
@Service
public class FreeMakerTemplateServiceImpl implements TemplateService, InitializingBean {

	private static final Log LOG = LogFactory.getLog(FreeMakerTemplateServiceImpl.class);

	@Autowired
	private Configuration freeMarkerConfig;

	@Autowired
	private LovService lovService;

	@Value("${freemaker.templatesPath}")
	private String templatesPath = "/data/templates";

	@Override
	public String format(String strTemplate, Map<String, ? extends Object> params, String templateType) {
		try {
			if (params == null) {
				params = new HashMap<>();
			}
			Template template = findTemplate(strTemplate, templateType);
			if (template != null) {
				return FreeMarkerTemplateUtils.processTemplateIntoString(template, params);
			}
		} catch (IOException e) {
			LOG.error(e, e);
		} catch (TemplateException e) {
			LOG.error(e, e);
		}
		return AppConstants.BLANK;
	}

	private boolean isTemplateCode(String value) {
		return value.length() <= 72 && value.matches("[a-zA-Z0-9-_\\\\.]+");
	}

	private File getTemplateFile(String value, String type) {
		File templateFile;
		int index = value.lastIndexOf('.');
		if (index == -1) {
			templateFile = new File(templatesPath + File.separator + type.toLowerCase() + File.separator + value + ".ftl");
		} else {
			templateFile = new File(templatesPath + File.separator + type.toLowerCase() + File.separator + value);
		}
		if (templateFile.exists()) {
			return templateFile;
		}
		return null;
	}

	private Template findTemplate(String strTemplate, String type) throws MalformedTemplateNameException, ParseException, IOException {
		String inlineTemplate = AppConstants.BLANK;
		if (isTemplateCode(strTemplate)) {
			File templateFile = getTemplateFile(strTemplate, type.toLowerCase());
			if (templateFile != null) {
				return freeMarkerConfig.getTemplate(templateFile.getName());
			} else {
				inlineTemplate = lovService.findTemplate(strTemplate);
			}
		}
		if (inlineTemplate.isEmpty()) {
			inlineTemplate = strTemplate;
		}
		StringReader strTemplateReader = new StringReader(inlineTemplate);
		try {
			return new Template(UUID.randomUUID().toString(), strTemplateReader, freeMarkerConfig);
		} catch (IOException e) {
			LOG.error(e, e);
		}
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		File templatesFolder = new File(templatesPath);
		if (!templatesFolder.exists()) {
			templatesFolder.mkdirs();
		}
		templatesPath = templatesFolder.getCanonicalPath();
		freeMarkerConfig.setClassForTemplateLoading(OmaCoresvResourcesApplication.class, AppConstants.TEMPLATE_PATH);
		freeMarkerConfig.setDefaultEncoding(AppConstants.UTF_8);
		freeMarkerConfig.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
	}

}
