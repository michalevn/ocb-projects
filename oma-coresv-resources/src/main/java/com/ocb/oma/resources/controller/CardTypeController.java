/**
 * 
 */
package com.ocb.oma.resources.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.resources.model.OmniCardType;
import com.ocb.oma.resources.repository.OmniCardTyleRepository;

/**
 * @author docv
 *
 */
@Controller
@RestController
@RequestMapping("/cardTypes")
public class CardTypeController extends BaseController {

	private static final Log LOG = LogFactory.getLog(CardTypeController.class);

	@Autowired
	protected OmniCardTyleRepository repo;

	@Autowired
	protected ObjectMapper objectMapper;

	@RequestMapping(value = "/find")
	public List<OmniCardType> find(@RequestParam(name = "group", required = false) String group) {
		LOG.info("find");
		if (StringUtils.isBlank(group)) {
			return repo.findByActive(true);
		}
		return repo.findByGroupAndActive(group, true);
	}
}
