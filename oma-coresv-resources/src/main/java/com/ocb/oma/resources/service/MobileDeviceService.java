/**
 * 
 */
package com.ocb.oma.resources.service;

import com.ocb.oma.resources.model.OmniMobileDevice;

/**
 * @author docv
 *
 */
public interface MobileDeviceService {

	/**
	 * Dang ky thiet bi
	 * 
	 * @param mobileDevice
	 * @return true neu dang ky thanh cong, false neu khong
	 */
	Boolean registerDevice(OmniMobileDevice mobileDevice);

}
