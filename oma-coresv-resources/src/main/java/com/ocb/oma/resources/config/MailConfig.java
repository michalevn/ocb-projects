package com.ocb.oma.resources.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.ocb.oma.resources.util.ProtectedConfigUtils;

@Configuration
public class MailConfig {

	@Value("${omni.mailUserLogin}")
	private String mailUserLogin;

	@Value("${omni.mailPassLogin}")
	private String mailPassLogin;

	@Value("${file.config.encryptedKey}")
	private String encryptedKey;

	
	@Bean
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("mail.ocb.com.vn");
		mailSender.setPort(587);

		mailSender.setUsername(mailUserLogin);
		mailSender.setPassword(ProtectedConfigUtils.decrypt(mailPassLogin, encryptedKey));

		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");

		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.host", "smtp.mailtrap.io");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.ssl.trust", "smtp.mailtrap.io");

		return mailSender;
	}

}
