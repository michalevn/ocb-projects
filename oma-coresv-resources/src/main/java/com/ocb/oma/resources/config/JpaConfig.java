package com.ocb.oma.resources.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ocb.oma.resources.util.ProtectedConfigUtils;

/**
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.ocb.oma.resources.repository")
@EnableTransactionManagement
public class JpaConfig {

	@Autowired
	private Environment env;

	@Value("${spring.datasource.driverClassName}")
	private String driverClassName;

	@Value("${spring.datasource.jdbc-url}")
	private String jdbcUrl;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String encryptedPassword;

	@Value("${spring.jpa.properties.hibernate.dialect}")
	private String dialect;

	@Value("${file.config.encryptedKey}")
	private String encryptedKey;

// 
	@Primary
	@Bean
	public DataSource dataSource() {

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUrl(jdbcUrl);
		dataSource.setUsername(username);
		String password = ProtectedConfigUtils.decrypt(encryptedPassword, encryptedKey);
		dataSource.setPassword(password);

		return dataSource;

	}

//	@Bean
//	@ConfigurationProperties(prefix = "spring.datasource")
//	public DataSource dataSource() {
//		DataSource dataSource = DataSourceBuilder.create().build();
//		return dataSource;
//	}

	@Bean
	public EntityManagerFactory entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		Properties properties = new Properties();

		vendorAdapter.setDatabasePlatform(dialect);
		properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

		factory.setPackagesToScan("com.ocb.oma.resources.model");

		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setDataSource(dataSource());
		factory.afterPropertiesSet();
		factory.setJpaProperties(properties);

		return factory.getObject();

	}

	@Bean
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}

}
