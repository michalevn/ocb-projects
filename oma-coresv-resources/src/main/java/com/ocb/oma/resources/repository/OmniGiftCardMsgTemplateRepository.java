package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniGiftCardMsgTemplate;

@RepositoryRestResource(collectionResourceRel = "OmniGiftCardMsgTemplate", path = "OmniGiftCardMsgTemplate")
public interface OmniGiftCardMsgTemplateRepository extends PagingAndSortingRepository<OmniGiftCardMsgTemplate, Long> {

	@RestResource(path = "findCardInfoByCardType")
	@Query("select p from OmniGiftCardMsgTemplate p where  p.cardType=:cardType   order by p.orderNumber")
	List<OmniGiftCardMsgTemplate> findCardInfoByCardType(@Param("cardType") String cardType);

}
