package com.ocb.oma.resources.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ocb.oma.resources.model.OmniLOV;

@RepositoryRestResource(collectionResourceRel = "OmniLOV", path = "OmniLOV")
public interface OmniBankLovRepository extends PagingAndSortingRepository<OmniLOV, Long> {

	@RestResource(path = "findByType")
	@Query("SELECT p FROM OmniLOV p WHERE p.lovType=:type ORDER BY p.orderNumber")
	List<OmniLOV> findByType(@Param("type") String type);

	@RestResource(path = "findByTypeAndCode")
	@Query("SELECT p FROM OmniLOV p WHERE p.lovType=:type AND p.lovCode=:code")
	OmniLOV findByTypeAndCode(@Param("type") String type, @Param("code") String code);

}
