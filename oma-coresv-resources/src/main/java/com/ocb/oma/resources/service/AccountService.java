/**
 * 
 */
package com.ocb.oma.resources.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ocb.oma.dto.AccountInfo;
import com.ocb.oma.dto.AccountSummaryDTO;
import com.ocb.oma.dto.CustomerInfoDTO;
import com.ocb.oma.dto.TransactionHistoryDTO;

/**
 * @author phuhoang
 *
 */
public interface AccountService {
	Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

	/**
	 * lay danh sach tai khoan
	 * 
	 * @param customerCif
	 * @param token
	 * @return
	 */
	public List<AccountInfo> getAccountList(String customerCif, String token);

	/**
	 * lay thong tin cua customer
	 * 
	 * @param token
	 * @return
	 */
	public CustomerInfoDTO getCustomerDetail(String token);

	/**
	 * update icon of account
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param token
	 * @param imageLink
	 * @return
	 */
	public Boolean updateAccountIcon(String accountNo, String customerCif, String token, String imageLink);

	/**
	 * update account Name
	 * 
	 * @param accountNo
	 * @param customerCif
	 * @param token
	 * @param updatedName
	 * @return
	 */
	public Boolean updateAccountName(String accountId, String customerCif, String token, String updatedName);

	/**
	 * tim kiem lich su transation
	 * 
	 * @param token
	 * @param transactionType
	 * @param fromDate
	 * @param toDate
	 * @param fromAmount
	 * @param toAmount
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<TransactionHistoryDTO> findTransactionHistory(String token, String accountNo, String transactionType,
			Date fromDate, Date toDate, Double fromAmount, Double toAmount, Integer page, Integer pageSize);

	/**
	 * lay summary account boi cif
	 * 
	 * @param token
	 * @param accountId
	 * @return
	 */
	AccountSummaryDTO loadAccountSummarByCurrencies(String token, String accountId);

}
