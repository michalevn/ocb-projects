/**
 * 
 */
package com.ocb.oma.resources.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.ocb.oma.resources.model.OmniNotification;

/**
 * @author phuhoang
 *
 */
@Projection(name = "OmniNotificationMessage", types = { OmniNotification.class })
public interface OmniNotificationProjection {

	
    @Value("#{target.messageId}")
	public Long getMessageId();

	public String getUserId();

	/**
	 * @return the subject
	 */
	public String getSubject();

	/**
	 * @return the body
	 */
	public String getBody();

	/**
	 * @return the messageType
	 */
	public String getMessageType();

	/**
	 * @return the isRead
	 */
	public Boolean getIsRead();

	/**
	 * @return the isEmail
	 */
	public Boolean getIsEmail();

	public Boolean getIsSMS();

}
