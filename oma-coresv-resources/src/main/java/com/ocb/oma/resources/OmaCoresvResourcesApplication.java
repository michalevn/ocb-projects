package com.ocb.oma.resources;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.ocb.oma.resources.config.RestTemplateInterceptor;

@EnableWebMvc
@SpringBootApplication
@EnableAutoConfiguration
@EnableDiscoveryClient
public class OmaCoresvResourcesApplication {

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "resources-server");
		SpringApplication.run(OmaCoresvResourcesApplication.class, args);
	}

	@Bean
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Collections.singletonList(new RestTemplateInterceptor()));
		return restTemplate;
	}
}
