/**
 * 
 */
package com.ocb.oma.resources.model.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.ocb.oma.resources.model.UserDeviceToken;

/**
 * @author phuhoang
 *
 */
@Projection(name = "UserDeviceToken", types = { UserDeviceToken.class })
public interface UserDeviceTokenProjection {

	@Value("#{target.userDeviceTokenId}")
	public Long getUserDeviceTokenId() ;
	/**
	 * @return the enable
	 */
	public Boolean getEnable() ;
	/**
	 * @return the userDeviceTokenId
	 */


	/**
	 * @return the cif
	 */
	public String getCif() ;
	/**
	 * @return the mobileDevice
	 */
	public String getMobileDevice() ;

}
