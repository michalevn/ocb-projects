/**
 * 
 */
package com.ocb.oma.resources.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.resources.model.OmniActivity;
import com.ocb.oma.resources.model.OmniNotification;
import com.ocb.oma.resources.model.UserDeviceToken;
import com.ocb.oma.resources.repository.OmniActivityRepository;
import com.ocb.oma.resources.repository.OmniNotificationsRepository;
import com.ocb.oma.resources.repository.UserDeviceTokenRepository;
import com.ocb.oma.resources.service.NotificationsService;

/**
 * @author docv
 *
 */
@Service
@Transactional
public class NotificationsServiceImpl implements NotificationsService {

	private static final Log LOG = LogFactory.getLog(NotificationsServiceImpl.class);

	@Autowired
	private OmniNotificationsRepository notificationsRepository;

	@Autowired
	private UserDeviceTokenRepository userDeviceTokenRepository;

	@Autowired
	private OmniActivityRepository activityRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Value("${firebase.url}")
	private String firebaseUrl = "https://fcm.googleapis.com/fcm/send";

	@Value("${firebase.serverKey}")
	private String serverKey;

	@Override
	public void maskAsRead(String cif, List<Long> notificationIds) {

		Iterable<OmniNotification> notifications = notificationsRepository.findAllById(notificationIds);
		List<OmniActivity> activities = activityRepository.findByUserActionTargets(cif, OmniActivity.ACTION_VIEW,
				OmniActivity.TARGET_TYPE_NOTIFICATION, notificationIds);
		Map<Long, OmniActivity> activityMap = new HashMap<>();
		if (activities != null && !activities.isEmpty()) {
			for (OmniActivity omniActivity : activities) {
				activityMap.put(omniActivity.getTarget(), omniActivity);
			}
		}
		if (notifications != null) {
			Iterator<OmniNotification> iterator = notifications.iterator();
			while (iterator.hasNext()) {
				OmniNotification omniNotification = iterator.next();
				OmniActivity omniActivity;
				if (activityMap.containsKey(omniNotification.getId())) {
					omniActivity = activityMap.get(omniNotification.getId());
				} else {
					omniActivity = new OmniActivity();
					omniActivity.setAction(OmniActivity.ACTION_VIEW);
					omniActivity.setTargetType(OmniActivity.TARGET_TYPE_NOTIFICATION);
					omniActivity.setCreatedDate(new Date());
					omniActivity.setUserId(cif);
					omniActivity.setTarget(omniNotification.getId());
				}
				omniActivity.setValue(1D);
				activityRepository.save(omniActivity);
			}
		}
	}

	@Override
	public void maskAsUnread(String cif, List<Long> notificationIds) {
		List<OmniActivity> activities = activityRepository.findByUserActionTargets(cif, OmniActivity.ACTION_VIEW,
				OmniActivity.TARGET_TYPE_NOTIFICATION, notificationIds);
		if (activities != null && !activities.isEmpty()) {
			for (OmniActivity omniActivity : activities) {
				omniActivity.setValue(0D);
				activityRepository.save(omniActivity);
			}
		}
	}

	@Override
	public void delete(String cif, List<Long> notificationIds) {
		List<OmniActivity> activities = activityRepository.findByUserActionTargets(cif, OmniActivity.ACTION_VIEW,
				OmniActivity.TARGET_TYPE_NOTIFICATION, notificationIds);
		if (activities != null && !activities.isEmpty()) {
			for (OmniActivity omniActivity : activities) {
				activityRepository.delete(omniActivity);
			}
		}

		Iterable<OmniNotification> notifications = notificationsRepository.findAllById(notificationIds);
		if (notifications != null) {
			Iterator<OmniNotification> iterator = notifications.iterator();
			while (iterator.hasNext()) {
				OmniNotification omniNotification = iterator.next();
				if (cif.equals(omniNotification.getUserId())) {
					notificationsRepository.delete(omniNotification);
				}
			}
		}
	}

	@Override
	public void push(OmniNotification omniNotification) {

		validate(omniNotification);

		Integer distributedType = omniNotification.getDistributedType();
		if (distributedType == null) {
			distributedType = 0;
		}

		if (contains(distributedType, OmniNotification.DISTRIBUTED_TYPE_APP)) {
			save(omniNotification);
		}

		if (contains(distributedType, OmniNotification.DISTRIBUTED_TYPE_FIREBASE)) {
			pushNotificationToFireBase(omniNotification);
		}

		if (contains(distributedType, OmniNotification.DISTRIBUTED_TYPE_SMS)) {
			throw new OmaException(MessageConstant.NOTIFICATION_SMS_NOT_SUPPORTED);
		}

	}

	@Override
	public List<OmniNotification> findByUserIdAndType(String query, String userId, Integer type, int page, int size) {

		if (!OmniNotification.TYPE_THONG_BAO.equals(type) && !OmniNotification.TYPE_KHUYEN_MAI.equals(type)) {
			return new ArrayList<>();
		}

		Pageable pageable = PageRequest.of(page, size);

		List<OmniNotification> notifications = null;

		if (StringUtils.isNotBlank(query)) {
			notifications = notificationsRepository.findByUserIdAndType("%" + query.trim() + "%", userId,
					type.toString(), pageable);
		} else {
			if (OmniNotification.TYPE_THONG_BAO.equals(type)) {
				notifications = notificationsRepository.findByUserIdAndType(userId, type.toString(), pageable);
			} else {
				notifications = notificationsRepository.findByUserIdAndType("0", type.toString(), pageable);
			}
		}
		if (notifications != null && !notifications.isEmpty()) {
			List<Long> targets = new ArrayList<>();
			Map<Long, OmniNotification> map = new HashMap<>();
			for (OmniNotification omniNotification : notifications) {
				omniNotification.setRead(false);
				targets.add(omniNotification.getId());
				map.put(omniNotification.getId(), omniNotification);
			}
			List<OmniActivity> activities = activityRepository.findByUserActionTargets(userId, OmniActivity.ACTION_VIEW,
					OmniActivity.TARGET_TYPE_NOTIFICATION, targets);
			if (activities != null && !activities.isEmpty()) {
				for (OmniActivity omniActivity : activities) {
					if (omniActivity.getValue().doubleValue() == 1) {
						map.get(omniActivity.getTarget()).setRead(true);
					}
				}
			}
		}
		return notifications;
	}

	@Override
	public OmniNotification get(Long notificationId, String cif) {
		Optional<OmniNotification> rs = notificationsRepository.findById(notificationId);
		if (rs == null) {
			return null;
		}
		OmniNotification notification = rs.get();
		OmniActivity activity = activityRepository.findByUserActionTarget(cif, OmniActivity.ACTION_VIEW,
				OmniActivity.TARGET_TYPE_NOTIFICATION, notification.getId());
		if (activity == null) {
			activity = new OmniActivity();
			activity.setAction(OmniActivity.ACTION_VIEW);
			activity.setTargetType(OmniActivity.TARGET_TYPE_NOTIFICATION);
			activity.setTarget(notification.getId());
			activity.setCreatedDate(new Date());
			activity.setUserId(cif);
			activity.setValue(1D);
			activityRepository.save(activity);
		}
		return notification;

	}

	@Override
	public Integer count(String cif, Date fromDate, Date toDate) {
		Integer count1 = notificationsRepository.count(cif, OmniNotification.TYPE_THONG_BAO.toString(), fromDate,
				toDate);
		Integer count2 = notificationsRepository.count("0", OmniNotification.TYPE_KHUYEN_MAI.toString(), fromDate,
				toDate);
		return count1 + count2;
	}

	@Override
	public Integer countUnread(String cif, Date fromDate, Date toDate) {
		Integer count = count(cif, fromDate, toDate);
		if (count == null || count.equals(0)) {
			return 0;
		}
		Integer readValue = activityRepository.countByUserAction(cif, OmniActivity.ACTION_VIEW,
				OmniActivity.TARGET_TYPE_NOTIFICATION, fromDate, toDate);
		if (readValue == null) {
			return count.intValue();
		}
		return count.intValue() - readValue.intValue();
	}

	private boolean contains(int value, int base) {
		return (value & base) == base;
	}

	private void save(OmniNotification notification) {
		if (notification.getCreatedDate() == null) {
			notification.setCreatedDate(new Date());
		}
		notificationsRepository.save(notification);
	}

	/**
	 * @param notification
	 * @return
	 */
	private void pushNotificationToFireBase(OmniNotification omniNotification) {
		List<String> deviceTokens = findDeviceTokens(omniNotification);
		if (deviceTokens.isEmpty()) {
			try {
				throw new OmaException(MessageConstant.NOTIFICATION_DEVICE_TOKEN_REQUIRED,
						"omniNotification: " + objectMapper.writeValueAsString(omniNotification));
			} catch (JsonProcessingException e) {
				throw new OmaException(MessageConstant.NOTIFICATION_DEVICE_TOKEN_REQUIRED,
						"omniNotification: " + e.getMessage());
			}
		}
		for (String deviceToken : deviceTokens) {
			JSONObject message = buildMessage(omniNotification, deviceToken);
			pushNotificationToFireBase(message);
		}
	}

	/**
	 * @see https://firebase.google.com/docs/cloud-messaging/http-server-ref
	 * @param message
	 */
	private void pushNotificationToFireBase(JSONObject message) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(firebaseUrl);
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("Authorization", "key=" + serverKey);

		LOG.info("Push Message To FireBase");
		LOG.info(message);

		HttpResponse response = null;

		try {

			httpPost.setEntity(new StringEntity(message.toString(), "UTF-8"));
			response = client.execute(httpPost);

			// if (LOG.isDebugEnabled()) {
			LOG.info("================== response ==================");
			LOG.info(response);
			// }

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new OmaException(MessageConstant.FIREBASE_ERROR,
						"FireBase Status Code Responded: " + response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e) {
			LOG.error("Firebase Error: " + e.getMessage(), e);
			throw new OmaException(MessageConstant.FIREBASE_ERROR, e.getMessage());
		} catch (IOException e) {
			LOG.error("The connection to FireBase is refused.", e);
			throw new OmaException(MessageConstant.FIREBASE_ERROR, "The connection to FireBase is refused.");
		}
	}

	@SuppressWarnings("unchecked")
	private JSONObject buildMessage(OmniNotification omniNotification, String deviceToken) {
		JSONObject message = new JSONObject();
		try {

			message.put("to", deviceToken);
			message.put("priority", "high");
			message.put("content_available", true);

			// JSONObject notification = new JSONObject();
			// notification.put("title", omniNotification.getTitleVn());
			// notification.put("body", omniNotification.getBodyVn());
			// message.put("notification", notification);

			Map<String, Object> properties = objectMapper.convertValue(omniNotification, Map.class);
			JSONObject data = new JSONObject(properties);

			message.put("data", data);

			return message;

		} catch (JSONException e) {
			throw new OmaException(MessageConstant.DATA_INVALID, e.getMessage());
		}
	}

	private List<String> findDeviceTokens(OmniNotification omniNotification) {
		String userId = omniNotification.getUserId();
		List<UserDeviceToken> userDeviceTokens = userDeviceTokenRepository.findByCif(userId);
		List<String> deviceTokens = new ArrayList<>();
		if (userDeviceTokens != null) {
			for (UserDeviceToken userDeviceToken : userDeviceTokens) {
				String deviceToken = userDeviceToken.getDeviceToken();
				if (/* userDeviceToken.getActive() && */ StringUtils.isNotBlank(deviceToken)) {
					deviceTokens.add(userDeviceToken.getDeviceToken());
				}
			}
		}
		if (deviceTokens.isEmpty()) {
			LOG.info(">>> No Device Tokens for user: " + userId);
		} else {
			LOG.info(">>> Pushing notification to: " + deviceTokens);
		}
		return deviceTokens;
	}

	private void validate(OmniNotification notification) {
		if (StringUtils.isBlank(notification.getUserId())) {
			throw new OmaException(MessageConstant.NOTIFICATION_USER_ID_REQUIRED);
		}
	}
}
