package com.ocb.oma.constants;

public enum PostPaymentErrors {

	REQUEST_BODY_MISSING("Required request body content is missing."), //
	PAYMENT_TYPE_INVALID(
			"paymentType must be one of the following values: InternalPayment, LocalPayment, FastTransfer, TuitionFee, BillPayment, eWallet, TOPUPPayment, BillCardPayment"), //
	PAYMENTINFO_REQUIRED("The parameter paymentInfo must be set."), //
	PAYMENTINFO_ACCOUNTID_REQUIRED("The parameter paymentInfo.accountId must be set."), //
	PAYMENTINFO_ACCOUNTID_ONLY_ACCEPT_NUMBERIC_CHARACTERS(
			"The parameter paymentInfo.accountId only accept numberic characters."),//
	;

	private String message;

	PostPaymentErrors(String message) {
		this.message = message;
	}

	public String message() {
		return message;
	}
}