package com.ocb.oma.constants;

public interface OmniConstants {

	/**
	 * Transaction Types
	 */
	String TRANSACTION_TYPE_NONCASH = "NONCASH";
	String TRANSACTION_TYPE_ATM = "ATM";
	String TRANSACTION_TYPE_CARD_TRANSFER = "CARD_TRANSFER";
	String TRANSACTION_TYPE_CREDIT = "CREDIT";
	String TRANSACTION_TYPE_REPAYMENT = "REPAYMENT";
	/**
	 * auto repayment equal to minimum repayment amount from credit card statement
	 */
	String REPAYMENT_TYPE_MIN = "MIN";
	/**
	 * full repayment of last statement balance
	 */
	String REPAYMENT_TYPE_TOTAL = "TOTAL";

}
