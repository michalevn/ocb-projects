/**
 *@author  Phu Hoang
 */
package com.ocb.oma.dto;

/**
 * @author Phu Hoang
 *
 */
public class MessageConstant {

	public static final String LOGIN_FAIL_CODE = "LOGIN_FAIL";
	public static final String ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL = "ACCOUNT_LOCK_CAUSE_OF_LOGIN_FAIL";
	public static final String MISSING_PARAMETER = "MISSING_PARAMS";
	// server bi thieu thong tin khi goi service
	public static final String MISSING_PARAMETER_SERVER = "MISSING_PARAMS_SERVER";
	public static final String SERVICE_UNAVAILABLE = "SERVICE_UNAVAILABLE";
	public static final String INVALID_TOKEN = "INVALID_TOKEN";
	public static final String PARAM_CAN_NOT_BE_NULL = "PARAM_CAN_NOT_BE_NULL";
	public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
	public static final String SERVER_ERROR = "SERVER_ERROR";
	public static final String VALIDATION_ERROR = "VALIDATION_ERROR";
	public static final String ACCESS_DENIED = "ACCESS_DENIED";
	public static final String UNAUTHORIZED = "UNAUTHORIZED";
	public static final String SERVER_OCP_ERROR = "SERVER_OCP_ERROR";
	public static final String SERVER_NEWFO_ERROR = "SERVER_NEWFO_ERROR";
	public static final String SERVICE_NOT_FOUND = "SERVICE_NOT_FOUND";
	public static final String ILLEGAL_ARGUMENT_PARAMETER = "ILLEGAL_ARGUMENT_PARAM";
	//// LOGIN
	public static final String MISSING_USERNAME_PASSWORD = "MISSING_USERNAME_OR_PASSWORD";
	public static final String MISSING_DEVICE_INFO = "MISSING_DEVICE_INFO";
	public static final String USERNAME_INVALID = "USERNAME_INVALID";
	public static final String ACCESS_FORBIDDEN = "ACCESS_FORBIDDEN";
	public static final String ACCOUNT_LOGGED_IN_ANOTHER_DEVICE = "ACCOUNT_LOGGED_IN_ANOTHER_DEVICE";
	public static final String CUSTOMER_CIF_INVALID = "CUSTOMER_CIF_INVALID";

	public static final String ACCOUNT_SERVICE_ERROR = "ACCOUNT_SERVICE_ERROR";

	public static final String RESOURCE_SERVICE_ERROR = "RESOURCE_SERVICE_ERROR";
	public static final String PAYMENT_TYPE_INVALID = "PAYMENT_TYPE_INVALID";
	public static final String RECIPIENT_TYPE_INVALID = "RECIPIENT_TYPE_INVALID";
	public static final String TEMPLATE_TYPE_INVALID = "TEMPLATE_TYPE_INVALID";
	public static final String STATUSES_TYPE_INVALID = "STATUSES_TYPE_INVALID";
	public static final String INVALID_OCP_RESPONSE_FORMAT = "INVALID_OCP_RESPONSE_FORMAT";

	public static final String CONSTRAIN_DATABASE_ERROR = "CONSTRAIN_DATABASE_ERROR";

	/**
	 * tai khoan debit khong hop le
	 */
	public static final String DEBIT_ACCOUNT_NOT_VALID = "DEBIT_ACCOUNT_NOT_VALID";

	/**
	 * loi khong tim thay nguoi thu huong
	 */
	public static final String RECIPIENT_NOT_FOUND = "RECIPIENT_NOT_FOUND";

	/**
	 * 
	 */
	public static final String CAN_NOT_DELETE_RECIPIENT_OF_OTHER_USER = "CAN_NOT_DELETE_RECIPIENT_OF_OTHER_USER";

	public static final String USER_DEVICE_NOT_FOUND = "USER_DEVICE_NOT_FOUND";
	/**
	 * loi khi update data cua user khac
	 */
	public static final String CAN_NOT_UPDATE_OTHER_USER_DATA = "CAN_NOT_UPDATE_OTHER_USER_DATA";

	/**
	 * khong the tim thay token cua thiet bi
	 */
	public static final String CAN_NOT_FIND_DEVICE_TOKEN = "CAN_NOT_FIND_DEVICE_TOKEN";

	public static final String CAN_NOT_FIND_AUTHORIZE_METHOD = "CAN_NOT_FIND_AUTHORIZE_METHOD";
	public static final String CAN_NOT_FIND_RECIPIENT = "CAN_NOT_FIND_RECIPIENT";
	public static final String CAN_NOT_DELETE_RECIPIENT_IN_LIST = "CAN_NOT_DELETE_RECIPIENT_IN_LIST";

	public static final String OK_MESSAGE = "SUCCESS";
	public static final String CAN_NOT_REMOVE_TRANSFER_BASKET = "CAN_NOT_REMOVE_TRANSFER_BASKET";
	/**
	 * loi update password
	 */
	public static final String CAN_NOT_UPDATE_PASSWORD = "CAN_NOT_UPDATE_PASSWORD";

	/**
	 * Account
	 */
	public static final String ACCOUNT_NOT_FOUND = "ACCOUNT_NOT_FOUND";
	public static final String ACCOUNT_NOT_REGISTERED_PERSONAL_INFO = "ACCOUNT_NOT_REGISTERED_PERSONAL_INFO";
	/**
	 * Notification
	 */

	public static final String FIREBASE_ERROR = "FIREBASE_ERROR";
	public static final String NOTIFICATION_USER_ID_REQUIRED = "NOTIFICATION_USER_ID_REQUIRED";
	public static final String NOTIFICATION_TITLE_REQUIRED = "NOTIFICATION_TITLE_REQUIRED";
	public static final String NOTIFICATION_BODY_REQUIRED = "NOTIFICATION_BODY_REQUIRED";
	public static final String NOTIFICATION_DEVICE_TOKEN_REQUIRED = "NOTIFICATION_DEVICE_TOKEN_REQUIRED";

	/**
	 * loi khi dang ky 1 device da duoc ghi nhan
	 */
	public static final String MOBILE_DEVICE_NOT_FOUND = "MOBILE_DEVICE_NOT_FOUND";
	public static final String MOBILE_DEVICE_REQUIRED = "MOBILE_DEVICE_REQUIRED";
	public static final String MOBILE_DEVICE_NOT_REGISTERED = "MOBILE_DEVICE_NOT_REGISTERED";
	public static final String DEVICE_HAS_ALDREADY_REGISTERED = "DEVICE_HAS_ALDREADY_REGISTERED";
	/**
	 * Loi khi gui gift card ko hop le
	 */
	public static final String GIFT_CARD_INVALID = "GIFT_CARD_INVALID";
	public static final String NOTIFICATION_SMS_NOT_SUPPORTED = "NOTIFICATION_SMS_NOT_SUPPORTED";
	public static final String DATA_INVALID = "DATA_INVALID";
	public static final String OPERATIONSTATUS_INVALID = "OPERATIONSTATUS_INVALID";
	public static final String BILL_NOT_FOUND = "BILL_NOT_FOUND";

	public static final String OTP_INCORRECT = "OTP_INCORRECT";
}
