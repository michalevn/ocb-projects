package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoBankingProductOnline implements Serializable {

	private static final long serialVersionUID = 4408784913927697353L;

	private NewFoTransactionInfo transactionInfo;
	private NewFoCustomerInfo customerInfo;
	private NewFoOnlineService onlineService;

	public NewFoTransactionInfo getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(NewFoTransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	public NewFoCustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(NewFoCustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public NewFoOnlineService getOnlineService() {
		return onlineService;
	}

	public void setOnlineService(NewFoOnlineService onlineService) {
		this.onlineService = onlineService;
	}

}
