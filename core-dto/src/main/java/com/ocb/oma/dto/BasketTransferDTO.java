package com.ocb.oma.dto;

import java.util.Date;

public class BasketTransferDTO {
	private PaymentDTO payment;
	private String signedStatus;
	private Date addedToBasketDate;
	private String actionHistoryList;
	private String[] actions;
	private String operationType;
	private String referenceId;
	

	/**
	 * @return the payment
	 */
	public PaymentDTO getPayment() {
		return payment;
	}



	/**
	 * @param payment the payment to set
	 */
	public void setPayment(PaymentDTO payment) {
		this.payment = payment;
	}



	/**
	 * @return the signedStatus
	 */
	public String getSignedStatus() {
		return signedStatus;
	}



	/**
	 * @param signedStatus the signedStatus to set
	 */
	public void setSignedStatus(String signedStatus) {
		this.signedStatus = signedStatus;
	}



	/**
	 * @return the addedToBasketDate
	 */
	public Date getAddedToBasketDate() {
		return addedToBasketDate;
	}



	/**
	 * @param addedToBasketDate the addedToBasketDate to set
	 */
	public void setAddedToBasketDate(Date addedToBasketDate) {
		this.addedToBasketDate = addedToBasketDate;
	}



	/**
	 * @return the actionHistoryList
	 */
	public String getActionHistoryList() {
		return actionHistoryList;
	}



	/**
	 * @param actionHistoryList the actionHistoryList to set
	 */
	public void setActionHistoryList(String actionHistoryList) {
		this.actionHistoryList = actionHistoryList;
	}



	/**
	 * @return the actions
	 */
	public String[] getActions() {
		return actions;
	}



	/**
	 * @param actions the actions to set
	 */
	public void setActions(String[] actions) {
		this.actions = actions;
	}



	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}



	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}



	/**
	 * @return the referenceId
	 */
	public String getReferenceId() {
		return referenceId;
	}



	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}



	public BasketTransferDTO() {
		// TODO Auto-generated constructor stub
	}

}
