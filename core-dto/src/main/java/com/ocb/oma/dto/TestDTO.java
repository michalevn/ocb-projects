package com.ocb.oma.dto;
/**
 * 
 * @author Phu Hoang
 *
 */
public class TestDTO {

	private String name;

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
