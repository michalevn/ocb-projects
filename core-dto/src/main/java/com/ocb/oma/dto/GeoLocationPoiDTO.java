package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;

public class GeoLocationPoiDTO implements Serializable {

	private static final long serialVersionUID = -2513888611306772956L;

	private List<GeoLocationDTO> poiPositions;

	private GpsPositionDTO requestedGpsPosition;

	public List<GeoLocationDTO> getPoiPositions() {
		return poiPositions;
	}

	public void setPoiPositions(List<GeoLocationDTO> poiPositions) {
		this.poiPositions = poiPositions;
	}

	public GpsPositionDTO getRequestedGpsPosition() {
		return requestedGpsPosition;
	}

	public void setRequestedGpsPosition(GpsPositionDTO requestedGpsPosition) {
		this.requestedGpsPosition = requestedGpsPosition;
	}

}
