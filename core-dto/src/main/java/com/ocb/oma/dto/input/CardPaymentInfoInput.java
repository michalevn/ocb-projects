package com.ocb.oma.dto.input;

public class CardPaymentInfoInput {
	private String cardAccountNumber;

	/**
	 * @return the cardAccountNumber
	 */
	public String getCardAccountNumber() {
		return cardAccountNumber;
	}

	/**
	 * @param cardAccountNumber the cardAccountNumber to set
	 */
	public void setCardAccountNumber(String cardAccountNumber) {
		this.cardAccountNumber = cardAccountNumber;
	}

	public CardPaymentInfoInput() {
		// TODO Auto-generated constructor stub
	}

}
