package com.ocb.oma.dto.input;

public class CriteriaAssignDTO {
	private String assignCode;

	/**
	 * @return the assignCode
	 */
	public String getAssignCode() {
		return assignCode;
	}

	/**
	 * @param assignCode the assignCode to set
	 */
	public void setAssignCode(String assignCode) {
		this.assignCode = assignCode;
	}

	public CriteriaAssignDTO() {
		// TODO Auto-generated constructor stub
	}

}
