package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoOnlineService implements Serializable {

	private static final long serialVersionUID = 7221847710959906637L;

	private String source;
	private String registerDate;
	private String isOpenAtm;
	private String houseOwnership;
	private String stayInMonth;
	private String hasCreditCard;
	private String creditCardIssBy;
	private String creditCardLimit;
	private String isPlatinum;
	private String hasLifeInsu;
	private String lifeInsuBy;
	private String incomePerMonth;
	private String incomeSource;
	private String professionalClassify;
	private String department;
	private String companyName;
	private String domain;
	private String position;
	private String monthOfLabour;
	private String labourContractType;
	private String expensePerMonth;
	private String netIncome;
	private String cardType;
	private String imageOnCard;
	private String cardExpectedLimit;
	private String cardReceivePlace;
	private String cardReceiveAddress;
	private String secureQuestion;
	private String secureAnwser;
	private String contactPersonInfo;;
	private String contactPersonRel;
	private String contactPersonMobile;
	private String personalProfile1;
	private String personalProfile2;
	private String personalProfile3;
	private String residentProfile1;
	private String residentProfile2;
	private String residentProfile3;
	private String residentProfile4;
	private String financialProfile1;
	private String financialProfile2;
	private String financialProfile3;
	private String financialProfile4;
	private String financialProfile5;
	private String custRequirement;
	private String insurProfileLink;
	private String loanPurpose;
	private String loanCollateralType;
	private String loanAmount;
	private String loanTerm;
	private String productType;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getIsOpenAtm() {
		return isOpenAtm;
	}

	public void setIsOpenAtm(String isOpenAtm) {
		this.isOpenAtm = isOpenAtm;
	}

	public String getHouseOwnership() {
		return houseOwnership;
	}

	public void setHouseOwnership(String houseOwnership) {
		this.houseOwnership = houseOwnership;
	}

	public String getStayInMonth() {
		return stayInMonth;
	}

	public void setStayInMonth(String stayInMonth) {
		this.stayInMonth = stayInMonth;
	}

	public String getHasCreditCard() {
		return hasCreditCard;
	}

	public void setHasCreditCard(String hasCreditCard) {
		this.hasCreditCard = hasCreditCard;
	}

	public String getCreditCardIssBy() {
		return creditCardIssBy;
	}

	public void setCreditCardIssBy(String creditCardIssBy) {
		this.creditCardIssBy = creditCardIssBy;
	}

	public String getCreditCardLimit() {
		return creditCardLimit;
	}

	public void setCreditCardLimit(String creditCardLimit) {
		this.creditCardLimit = creditCardLimit;
	}

	public String getIsPlatinum() {
		return isPlatinum;
	}

	public void setIsPlatinum(String isPlatinum) {
		this.isPlatinum = isPlatinum;
	}

	public String getHasLifeInsu() {
		return hasLifeInsu;
	}

	public void setHasLifeInsu(String hasLifeInsu) {
		this.hasLifeInsu = hasLifeInsu;
	}

	public String getLifeInsuBy() {
		return lifeInsuBy;
	}

	public void setLifeInsuBy(String lifeInsuBy) {
		this.lifeInsuBy = lifeInsuBy;
	}

	public String getIncomePerMonth() {
		return incomePerMonth;
	}

	public void setIncomePerMonth(String incomePerMonth) {
		this.incomePerMonth = incomePerMonth;
	}

	public String getIncomeSource() {
		return incomeSource;
	}

	public void setIncomeSource(String incomeSource) {
		this.incomeSource = incomeSource;
	}

	public String getProfessionalClassify() {
		return professionalClassify;
	}

	public void setProfessionalClassify(String professionalClassify) {
		this.professionalClassify = professionalClassify;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getMonthOfLabour() {
		return monthOfLabour;
	}

	public void setMonthOfLabour(String monthOfLabour) {
		this.monthOfLabour = monthOfLabour;
	}

	public String getLabourContractType() {
		return labourContractType;
	}

	public void setLabourContractType(String labourContractType) {
		this.labourContractType = labourContractType;
	}

	public String getExpensePerMonth() {
		return expensePerMonth;
	}

	public void setExpensePerMonth(String expensePerMonth) {
		this.expensePerMonth = expensePerMonth;
	}

	public String getNetIncome() {
		return netIncome;
	}

	public void setNetIncome(String netIncome) {
		this.netIncome = netIncome;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getImageOnCard() {
		return imageOnCard;
	}

	public void setImageOnCard(String imageOnCard) {
		this.imageOnCard = imageOnCard;
	}

	public String getCardExpectedLimit() {
		return cardExpectedLimit;
	}

	public void setCardExpectedLimit(String cardExpectedLimit) {
		this.cardExpectedLimit = cardExpectedLimit;
	}

	public String getCardReceivePlace() {
		return cardReceivePlace;
	}

	public void setCardReceivePlace(String cardReceivePlace) {
		this.cardReceivePlace = cardReceivePlace;
	}

	public String getCardReceiveAddress() {
		return cardReceiveAddress;
	}

	public void setCardReceiveAddress(String cardReceiveAddress) {
		this.cardReceiveAddress = cardReceiveAddress;
	}

	public String getSecureQuestion() {
		return secureQuestion;
	}

	public void setSecureQuestion(String secureQuestion) {
		this.secureQuestion = secureQuestion;
	}

	public String getSecureAnwser() {
		return secureAnwser;
	}

	public void setSecureAnwser(String secureAnwser) {
		this.secureAnwser = secureAnwser;
	}

	public String getContactPersonInfo() {
		return contactPersonInfo;
	}

	public void setContactPersonInfo(String contactPersonInfo) {
		this.contactPersonInfo = contactPersonInfo;
	}

	public String getContactPersonRel() {
		return contactPersonRel;
	}

	public void setContactPersonRel(String contactPersonRel) {
		this.contactPersonRel = contactPersonRel;
	}

	public String getContactPersonMobile() {
		return contactPersonMobile;
	}

	public void setContactPersonMobile(String contactPersonMobile) {
		this.contactPersonMobile = contactPersonMobile;
	}

	public String getPersonalProfile1() {
		return personalProfile1;
	}

	public void setPersonalProfile1(String personalProfile1) {
		this.personalProfile1 = personalProfile1;
	}

	public String getPersonalProfile2() {
		return personalProfile2;
	}

	public void setPersonalProfile2(String personalProfile2) {
		this.personalProfile2 = personalProfile2;
	}

	public String getPersonalProfile3() {
		return personalProfile3;
	}

	public void setPersonalProfile3(String personalProfile3) {
		this.personalProfile3 = personalProfile3;
	}

	public String getResidentProfile1() {
		return residentProfile1;
	}

	public void setResidentProfile1(String residentProfile1) {
		this.residentProfile1 = residentProfile1;
	}

	public String getResidentProfile2() {
		return residentProfile2;
	}

	public void setResidentProfile2(String residentProfile2) {
		this.residentProfile2 = residentProfile2;
	}

	public String getResidentProfile3() {
		return residentProfile3;
	}

	public void setResidentProfile3(String residentProfile3) {
		this.residentProfile3 = residentProfile3;
	}

	public String getResidentProfile4() {
		return residentProfile4;
	}

	public void setResidentProfile4(String residentProfile4) {
		this.residentProfile4 = residentProfile4;
	}

	public String getFinancialProfile1() {
		return financialProfile1;
	}

	public void setFinancialProfile1(String financialProfile1) {
		this.financialProfile1 = financialProfile1;
	}

	public String getFinancialProfile2() {
		return financialProfile2;
	}

	public void setFinancialProfile2(String financialProfile2) {
		this.financialProfile2 = financialProfile2;
	}

	public String getFinancialProfile3() {
		return financialProfile3;
	}

	public void setFinancialProfile3(String financialProfile3) {
		this.financialProfile3 = financialProfile3;
	}

	public String getFinancialProfile4() {
		return financialProfile4;
	}

	public void setFinancialProfile4(String financialProfile4) {
		this.financialProfile4 = financialProfile4;
	}

	public String getFinancialProfile5() {
		return financialProfile5;
	}

	public void setFinancialProfile5(String financialProfile5) {
		this.financialProfile5 = financialProfile5;
	}

	public String getCustRequirement() {
		return custRequirement;
	}

	public void setCustRequirement(String custRequirement) {
		this.custRequirement = custRequirement;
	}

	public String getInsurProfileLink() {
		return insurProfileLink;
	}

	public void setInsurProfileLink(String insurProfileLink) {
		this.insurProfileLink = insurProfileLink;
	}

	public String getLoanPurpose() {
		return loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public String getLoanCollateralType() {
		return loanCollateralType;
	}

	public void setLoanCollateralType(String loanCollateralType) {
		this.loanCollateralType = loanCollateralType;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

}
