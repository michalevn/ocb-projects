/**
 * 
 */
package com.ocb.oma.dto.notification;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class NotificationMessageDTO implements Serializable {

	public static final String SEND_TYPE_DEVICE = "device";
	public static final String SEND_TYPE_TOPIC = "topic";
	public static final String SEND_TYPE_ALL = "all";
	/**
	 * 
	 */
	private static final long serialVersionUID = 2292452823822404233L;
	private String messageContent;
	private String messageLabel;
	private String target;

	/**
	 * @return the messageContent
	 */
	public String getMessageContent() {
		return messageContent;
	}

	/**
	 * @param messageContent the messageContent to set
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * @return the messageLabel
	 */
	public String getMessageLabel() {
		return messageLabel;
	}

	/**
	 * @param messageLabel the messageLabel to set
	 */
	public void setMessageLabel(String messageLabel) {
		this.messageLabel = messageLabel;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

}
