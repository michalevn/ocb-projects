/**
 *@author  Phu Hoang
 */
package com.ocb.oma.dto;

import java.util.Date;

/**
 * @author Phu Hoang
 *
 */
public class ResponseDTO {

	public static boolean SUCCESS_RETURN = true;
	public static boolean FAILURE_RETURN = false;
	private boolean success;
	private Object data;
	private Date date;
	private String message;
	private String errorCode;
	

	private ResponseDTO() {

	}

	public static ResponseDTO getSuccessResponse() {
		ResponseDTO dto = new ResponseDTO();
		dto.setSuccess(true);
		dto.setDate(new Date());
		return dto;
	}

	public static ResponseDTO getFailureResponse() {
		ResponseDTO dto = new ResponseDTO();
		dto.setSuccess(false);
		dto.setDate(new Date());
		return dto;
	}

	/**
	 * @param date the date to set
	 */
	private void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
