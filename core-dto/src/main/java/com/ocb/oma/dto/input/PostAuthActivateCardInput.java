/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

/**
 * @author docv
 *
 */
public class PostAuthActivateCardInput implements Serializable {

	private static final long serialVersionUID = -3596801873128558406L;

	private String cardId;

	private VerifyOtpToken otpValue;
	private String otpMethod;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
