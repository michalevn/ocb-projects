package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class TuitionFeeDTO implements Serializable {

	private static final long serialVersionUID = 5169760698420467212L;

	private BigDecimal tuitionAmount;
	private BigDecimal tuitionDebtAmount;
	private String description;

	public BigDecimal getTuitionAmount() {
		return tuitionAmount;
	}

	public void setTuitionAmount(BigDecimal tuitionAmount) {
		this.tuitionAmount = tuitionAmount;
	}

	public BigDecimal getTuitionDebtAmount() {
		return tuitionDebtAmount;
	}

	public void setTuitionDebtAmount(BigDecimal tuitionDebtAmount) {
		this.tuitionDebtAmount = tuitionDebtAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
