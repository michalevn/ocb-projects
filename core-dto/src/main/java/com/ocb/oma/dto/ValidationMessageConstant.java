/**
 *@author  Phu Hoang
 */
package com.ocb.oma.dto;

/**
 * @author Phu Hoang
 *
 */
public class ValidationMessageConstant {

	// Sai format password
	public static final String INPUT_PASSWORD_FORMAT_INVALID = "INPUT_PASSWORD_FORMAT_INVALID";
	// chuyen tien noi bo thi tk nguon va tk dich phai l� VND
	public static final String CURRENCY_MUST_BE_VND = "CURRENCY_MUST_BE_VND";
	// chuyen tien noi bo ma so tk gui va tk nhan khong duoc giong nhau
	public static final String DEBITACCOUNT_AND_CREDITACCOUNT_MUST_BE_DIFFERENT = "DEBITACCOUNT_AND_CREDITACCOUNT_MUST_BE_DIFFERENT";
	// phai lai so
	public static final String MUST_BE_NUMBER = "MUST_BE_NUMBER";

}
