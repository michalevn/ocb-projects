/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class CustomerInfoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2390143811877706105L;
	private String customerType;
	private String packageType;
	private String authType;
	private String phoneNumber;

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the packageType
	 */
	public String getPackageType() {
		return packageType;
	}

	/**
	 * @param packageType the packageType to set
	 */
	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	/**
	 * @return the authType
	 */
	public String getAuthType() {
		return authType;
	}

	/**
	 * @param authType the authType to set
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}

	 
	/**
	 * @param customerType
	 * @param packageType
	 * @param authType
	 * @param phoneNumber
	 */
	public CustomerInfoDTO(String customerType, String packageType, String authType, String phoneNumber) {
		super();
		this.customerType = customerType;
		this.packageType = packageType;
		this.authType = authType;
		this.phoneNumber = phoneNumber;
	}

	/**
	 * 
	 */
	public CustomerInfoDTO() {
		// TODO Auto-generated constructor stub
	}

}
