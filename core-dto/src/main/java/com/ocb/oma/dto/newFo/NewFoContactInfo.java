package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoContactInfo implements Serializable {

	private static final long serialVersionUID = 4903212580225590914L;

	private String[] mobileNumber;

	public String[] getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String[] mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
