package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExchangeRateDTO implements Serializable {

	private static final long serialVersionUID = 2104824410708603508L;

	private BigDecimal sellRate;
	private BigDecimal buyRate;
	private BigDecimal averageRate;
	private String currency;
	private String validStartingFrom;
	private Integer exchangeQuantity;
	private String market;
	private BigDecimal sellRateTransfer;
	private BigDecimal buyRateTransfer;

	public BigDecimal getSellRate() {
		return sellRate;
	}

	public void setSellRate(BigDecimal sellRate) {
		this.sellRate = sellRate;
	}

	public BigDecimal getBuyRate() {
		return buyRate;
	}

	public void setBuyRate(BigDecimal buyRate) {
		this.buyRate = buyRate;
	}

	public BigDecimal getAverageRate() {
		return averageRate;
	}

	public void setAverageRate(BigDecimal averageRate) {
		this.averageRate = averageRate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getValidStartingFrom() {
		return validStartingFrom;
	}

	public void setValidStartingFrom(String validStartingFrom) {
		this.validStartingFrom = validStartingFrom;
	}

	public Integer getExchangeQuantity() {
		return exchangeQuantity;
	}

	public void setExchangeQuantity(Integer exchangeQuantity) {
		this.exchangeQuantity = exchangeQuantity;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public BigDecimal getSellRateTransfer() {
		return sellRateTransfer;
	}

	public void setSellRateTransfer(BigDecimal sellRateTransfer) {
		this.sellRateTransfer = sellRateTransfer;
	}

	public BigDecimal getBuyRateTransfer() {
		return buyRateTransfer;
	}

	public void setBuyRateTransfer(BigDecimal buyRateTransfer) {
		this.buyRateTransfer = buyRateTransfer;
	}
}
