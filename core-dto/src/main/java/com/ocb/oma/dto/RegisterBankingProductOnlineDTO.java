package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;

import com.ocb.oma.validation.CardTypeCode;
import com.ocb.oma.validation.Digits;

public class RegisterBankingProductOnlineDTO implements Serializable {

	private static final long serialVersionUID = -4139523872534039943L;

	@CardTypeCode(message = "CARDTYPECODE_INVALID")
	private String cardTypeCode;

	@Digits(message = ValidationMessageConstant.MUST_BE_NUMBER)
	private BigDecimal amount;

	public String getCardTypeCode() {
		return cardTypeCode;
	}

	public void setCardTypeCode(String cardTypeCode) {
		this.cardTypeCode = cardTypeCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
