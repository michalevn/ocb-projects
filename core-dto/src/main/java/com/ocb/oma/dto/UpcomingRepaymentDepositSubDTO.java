/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class UpcomingRepaymentDepositSubDTO implements Serializable {

	private Date date;
	private Double amount;
	private String productId;
	private String currency;
	private Boolean infoAvailable;

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the infoAvailable
	 */
	public Boolean getInfoAvailable() {
		return infoAvailable;
	}

	/**
	 * @param infoAvailable the infoAvailable to set
	 */
	public void setInfoAvailable(Boolean infoAvailable) {
		this.infoAvailable = infoAvailable;
	}

	/**
	 * 
	 */
	public UpcomingRepaymentDepositSubDTO() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param date
	 * @param amount
	 * @param productId
	 * @param currency
	 * @param infoAvailable
	 */
	public UpcomingRepaymentDepositSubDTO(Date date, Double amount, String productId, String currency,
			Boolean infoAvailable) {
		super();
		this.date = date;
		this.amount = amount;
		this.productId = productId;
		this.currency = currency;
		this.infoAvailable = infoAvailable;
	}

	
}
