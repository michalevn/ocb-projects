/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class RecipientOCBDTO  implements Serializable{
	private String recipientId;
	private String[] recipientName;
	private String[] recipientAddress;
	private String bankName;
	private List<PaymentTemplateDTO> paymentTemplates;
	/**
	 * @return the recipientId
	 */
	public String getRecipientId() {
		return recipientId;
	}
	/**
	 * @param recipientId the recipientId to set
	 */
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}
	/**
	 * @return the recipientName
	 */
	public String[] getRecipientName() {
		return recipientName;
	}
	/**
	 * @param recipientName the recipientName to set
	 */
	public void setRecipientName(String[] recipientName) {
		this.recipientName = recipientName;
	}
	/**
	 * @return the recipientAddress
	 */
	public String[] getRecipientAddress() {
		return recipientAddress;
	}
	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(String[] recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * @return the paymentTemplates
	 */
	public List<PaymentTemplateDTO> getPaymentTemplates() {
		return paymentTemplates;
	}
	/**
	 * @param paymentTemplates the paymentTemplates to set
	 */
	public void setPaymentTemplates(List<PaymentTemplateDTO> paymentTemplates) {
		this.paymentTemplates = paymentTemplates;
	}
	public RecipientOCBDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
