package com.ocb.oma.dto;

import java.io.Serializable;

public class DepositInterestRateDTO implements Serializable {

	private static final long serialVersionUID = 8905476148705905683L;

	private Boolean accumulative;
	private Boolean autoRenewal;
	private String campaignId;
	private String currency;
	private String description;
	private Long initialAmountLimit;
	private Double interest;
	private String interestPaymentMethod;
	private String interestType;
	private Double maxAmount;
	private Double minAmount;
	private String periodUnit;
	private Long periodicAmountLimit;
	private String subProductCode;
	private Long term;

	public Boolean getAccumulative() {
		return accumulative;
	}

	public void setAccumulative(Boolean accumulative) {
		this.accumulative = accumulative;
	}

	public Boolean getAutoRenewal() {
		return autoRenewal;
	}

	public void setAutoRenewal(Boolean autoRenewal) {
		this.autoRenewal = autoRenewal;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getInitialAmountLimit() {
		return initialAmountLimit;
	}

	public void setInitialAmountLimit(Long initialAmountLimit) {
		this.initialAmountLimit = initialAmountLimit;
	}

	public Double getInterest() {
		return interest;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public String getInterestPaymentMethod() {
		return interestPaymentMethod;
	}

	public void setInterestPaymentMethod(String interestPaymentMethod) {
		this.interestPaymentMethod = interestPaymentMethod;
	}

	public String getInterestType() {
		return interestType;
	}

	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}

	public Double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Double maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(Double minAmount) {
		this.minAmount = minAmount;
	}

	public String getPeriodUnit() {
		return periodUnit;
	}

	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}

	public Long getPeriodicAmountLimit() {
		return periodicAmountLimit;
	}

	public void setPeriodicAmountLimit(Long periodicAmountLimit) {
		this.periodicAmountLimit = periodicAmountLimit;
	}

	public String getSubProductCode() {
		return subProductCode;
	}

	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}

	public Long getTerm() {
		return term;
	}

	public void setTerm(Long term) {
		this.term = term;
	}

}
