package com.ocb.oma.dto;

public class PaymentDetailDTO {
	private String clearingNetwork;
	private String bankCode;
	private String province;
	private String branchCode;
	private String cardNumber;
	private Double fee;
	
	

	/**
	 * @return the clearingNetwork
	 */
	public String getClearingNetwork() {
		return clearingNetwork;
	}



	/**
	 * @param clearingNetwork the clearingNetwork to set
	 */
	public void setClearingNetwork(String clearingNetwork) {
		this.clearingNetwork = clearingNetwork;
	}



	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}



	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}



	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}



	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}



	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}



	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}



	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}



	/**
	 * @return the fee
	 */
	public Double getFee() {
		return fee;
	}



	/**
	 * @param fee the fee to set
	 */
	public void setFee(Double fee) {
		this.fee = fee;
	}



	public PaymentDetailDTO() {
		// TODO Auto-generated constructor stub
	}

}
