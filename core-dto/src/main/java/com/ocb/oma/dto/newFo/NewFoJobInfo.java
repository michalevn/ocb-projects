package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoJobInfo implements Serializable {

	private static final long serialVersionUID = 5768933283605288584L;

	private String professionalCode;

	public String getProfessionalCode() {
		return professionalCode;
	}

	public void setProfessionalCode(String professionalCode) {
		this.professionalCode = professionalCode;
	}

}
