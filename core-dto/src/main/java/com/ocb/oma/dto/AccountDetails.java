package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

public class AccountDetails implements Serializable {

	private static final long serialVersionUID = -3370362367787747316L;

	private Long postingRestriction;
	private Date openingDate;
	private String postingRestrict;
	private AccountOwner owner;

	public Long getPostingRestriction() {
		return postingRestriction;
	}

	public void setPostingRestriction(Long postingRestriction) {
		this.postingRestriction = postingRestriction;
	}

	public Date getOpeningDate() {
		return openingDate;
	}

	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}

	public String getPostingRestrict() {
		return postingRestrict;
	}

	public void setPostingRestrict(String postingRestrict) {
		this.postingRestrict = postingRestrict;
	}

	public AccountOwner getOwner() {
		return owner;
	}

	public void setOwner(AccountOwner owner) {
		this.owner = owner;
	}

}
