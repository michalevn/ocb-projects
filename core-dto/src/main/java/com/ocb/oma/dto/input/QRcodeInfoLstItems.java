package com.ocb.oma.dto.input;

public class QRcodeInfoLstItems {
	private int quantity;
	private String note;
	private String qrInfor;
	
	

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}



	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}



	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}



	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}



	/**
	 * @return the qrInfor
	 */
	public String getQrInfor() {
		return qrInfor;
	}



	/**
	 * @param qrInfor the qrInfor to set
	 */
	public void setQrInfor(String qrInfor) {
		this.qrInfor = qrInfor;
	}



	public QRcodeInfoLstItems() {
		// TODO Auto-generated constructor stub
	}

}
