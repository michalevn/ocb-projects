package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CreditDTO implements Serializable {

	private static final long serialVersionUID = 1717604621491838010L;

	private String creditId;

	private String creditType;

	private String accountNo;

	private String currency;

	private BigDecimal creditAmount;

	private String creditName;

	private BigDecimal maturedCapitalAmount;

	private BigDecimal unmaturedCapitalAmount;

	private BigDecimal nextInstallmentAmount;

	private BigDecimal nextInstallmentDate;

	private BigDecimal overpaymentAmount;

	private String overpaymentCurrency;

	private int outstandingLiabilitiesAmount;

	private String outstandingLiabilitiesCurrency;

	private BigDecimal interestRate;

	private Date openDate;

	private Date closeDate;

	private List<OwnersDTO> ownersList;

	private Date lastTransactionBookingDate;

	private List<String> actions;

	private List<String> creditCategories;

	private String flexAgreementNo;

	private String flexProductId;

	private String principalAmount;

	private Date contractDate;

	private BigDecimal disbursedTranchesAmount;

	private BigDecimal totalAmountDue;

	private BigDecimal totalPrincipialDue;

	private BigDecimal totalInterest;

	private BigDecimal totalPenaltyInterestDue;

	private BigDecimal nextRepaymentAmount;

	private Date nextRepaymentDate;

	private String repaymentAccount;

	private Date efficientRepaymentDate;

	private Date valueDate;

	private Date finalDueDate;

	private BigDecimal wholeRepaymentBalance;

	private BigDecimal outstandingInterest;

	private BigDecimal totalInterestDue;

	private BigDecimal totalInstallment;

	private BigDecimal paidInstallments;

	private BigDecimal initialBalance;

	private BigDecimal totalPaidPrincipals;

	private BigDecimal totalPaidInterests;

	public String getCreditId() {
		return creditId;
	}

	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}

	public String getCreditType() {
		return creditType;
	}

	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public BigDecimal getMaturedCapitalAmount() {
		return maturedCapitalAmount;
	}

	public void setMaturedCapitalAmount(BigDecimal maturedCapitalAmount) {
		this.maturedCapitalAmount = maturedCapitalAmount;
	}

	public BigDecimal getUnmaturedCapitalAmount() {
		return unmaturedCapitalAmount;
	}

	public void setUnmaturedCapitalAmount(BigDecimal unmaturedCapitalAmount) {
		this.unmaturedCapitalAmount = unmaturedCapitalAmount;
	}

	public BigDecimal getNextInstallmentAmount() {
		return nextInstallmentAmount;
	}

	public void setNextInstallmentAmount(BigDecimal nextInstallmentAmount) {
		this.nextInstallmentAmount = nextInstallmentAmount;
	}

	public BigDecimal getNextInstallmentDate() {
		return nextInstallmentDate;
	}

	public void setNextInstallmentDate(BigDecimal nextInstallmentDate) {
		this.nextInstallmentDate = nextInstallmentDate;
	}

	public BigDecimal getOverpaymentAmount() {
		return overpaymentAmount;
	}

	public void setOverpaymentAmount(BigDecimal overpaymentAmount) {
		this.overpaymentAmount = overpaymentAmount;
	}

	public String getOverpaymentCurrency() {
		return overpaymentCurrency;
	}

	public void setOverpaymentCurrency(String overpaymentCurrency) {
		this.overpaymentCurrency = overpaymentCurrency;
	}

	public int getOutstandingLiabilitiesAmount() {
		return outstandingLiabilitiesAmount;
	}

	public void setOutstandingLiabilitiesAmount(int outstandingLiabilitiesAmount) {
		this.outstandingLiabilitiesAmount = outstandingLiabilitiesAmount;
	}

	public String getOutstandingLiabilitiesCurrency() {
		return outstandingLiabilitiesCurrency;
	}

	public void setOutstandingLiabilitiesCurrency(String outstandingLiabilitiesCurrency) {
		this.outstandingLiabilitiesCurrency = outstandingLiabilitiesCurrency;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public List<OwnersDTO> getOwnersList() {
		return ownersList;
	}

	public void setOwnersList(List<OwnersDTO> ownersList) {
		this.ownersList = ownersList;
	}

	public Date getLastTransactionBookingDate() {
		return lastTransactionBookingDate;
	}

	public void setLastTransactionBookingDate(Date lastTransactionBookingDate) {
		this.lastTransactionBookingDate = lastTransactionBookingDate;
	}

	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}

	public List<String> getCreditCategories() {
		return creditCategories;
	}

	public void setCreditCategories(List<String> creditCategories) {
		this.creditCategories = creditCategories;
	}

	public String getFlexAgreementNo() {
		return flexAgreementNo;
	}

	public void setFlexAgreementNo(String flexAgreementNo) {
		this.flexAgreementNo = flexAgreementNo;
	}

	public String getFlexProductId() {
		return flexProductId;
	}

	public void setFlexProductId(String flexProductId) {
		this.flexProductId = flexProductId;
	}

	public String getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(String principalAmount) {
		this.principalAmount = principalAmount;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public BigDecimal getDisbursedTranchesAmount() {
		return disbursedTranchesAmount;
	}

	public void setDisbursedTranchesAmount(BigDecimal disbursedTranchesAmount) {
		this.disbursedTranchesAmount = disbursedTranchesAmount;
	}

	public BigDecimal getTotalAmountDue() {
		return totalAmountDue;
	}

	public void setTotalAmountDue(BigDecimal totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public BigDecimal getTotalPrincipialDue() {
		return totalPrincipialDue;
	}

	public void setTotalPrincipialDue(BigDecimal totalPrincipialDue) {
		this.totalPrincipialDue = totalPrincipialDue;
	}

	public BigDecimal getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(BigDecimal totalInterest) {
		this.totalInterest = totalInterest;
	}

	public BigDecimal getTotalPenaltyInterestDue() {
		return totalPenaltyInterestDue;
	}

	public void setTotalPenaltyInterestDue(BigDecimal totalPenaltyInterestDue) {
		this.totalPenaltyInterestDue = totalPenaltyInterestDue;
	}

	public BigDecimal getNextRepaymentAmount() {
		return nextRepaymentAmount;
	}

	public void setNextRepaymentAmount(BigDecimal nextRepaymentAmount) {
		this.nextRepaymentAmount = nextRepaymentAmount;
	}

	public Date getNextRepaymentDate() {
		return nextRepaymentDate;
	}

	public void setNextRepaymentDate(Date nextRepaymentDate) {
		this.nextRepaymentDate = nextRepaymentDate;
	}

	public String getRepaymentAccount() {
		return repaymentAccount;
	}

	public void setRepaymentAccount(String repaymentAccount) {
		this.repaymentAccount = repaymentAccount;
	}

	public Date getEfficientRepaymentDate() {
		return efficientRepaymentDate;
	}

	public void setEfficientRepaymentDate(Date efficientRepaymentDate) {
		this.efficientRepaymentDate = efficientRepaymentDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public Date getFinalDueDate() {
		return finalDueDate;
	}

	public void setFinalDueDate(Date finalDueDate) {
		this.finalDueDate = finalDueDate;
	}

	public BigDecimal getWholeRepaymentBalance() {
		return wholeRepaymentBalance;
	}

	public void setWholeRepaymentBalance(BigDecimal wholeRepaymentBalance) {
		this.wholeRepaymentBalance = wholeRepaymentBalance;
	}

	public BigDecimal getOutstandingInterest() {
		return outstandingInterest;
	}

	public void setOutstandingInterest(BigDecimal outstandingInterest) {
		this.outstandingInterest = outstandingInterest;
	}

	public BigDecimal getTotalInterestDue() {
		return totalInterestDue;
	}

	public void setTotalInterestDue(BigDecimal totalInterestDue) {
		this.totalInterestDue = totalInterestDue;
	}

	public BigDecimal getTotalInstallment() {
		return totalInstallment;
	}

	public void setTotalInstallment(BigDecimal totalInstallment) {
		this.totalInstallment = totalInstallment;
	}

	public BigDecimal getPaidInstallments() {
		return paidInstallments;
	}

	public void setPaidInstallments(BigDecimal paidInstallments) {
		this.paidInstallments = paidInstallments;
	}

	public BigDecimal getInitialBalance() {
		return initialBalance;
	}

	public void setInitialBalance(BigDecimal initialBalance) {
		this.initialBalance = initialBalance;
	}

	public BigDecimal getTotalPaidPrincipals() {
		return totalPaidPrincipals;
	}

	public void setTotalPaidPrincipals(BigDecimal totalPaidPrincipals) {
		this.totalPaidPrincipals = totalPaidPrincipals;
	}

	public BigDecimal getTotalPaidInterests() {
		return totalPaidInterests;
	}

	public void setTotalPaidInterests(BigDecimal totalPaidInterests) {
		this.totalPaidInterests = totalPaidInterests;
	}

}
