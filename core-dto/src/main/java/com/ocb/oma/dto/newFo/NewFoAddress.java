package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoAddress implements Serializable {

	private static final long serialVersionUID = 5764975524100025058L;

	private String address1;
	private String ward;
	private String[] email;
	private String district;
	private String province;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String[] getEmail() {
		return email;
	}

	public void setEmail(String[] email) {
		this.email = email;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
