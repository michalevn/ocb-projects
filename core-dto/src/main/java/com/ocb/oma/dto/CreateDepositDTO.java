package com.ocb.oma.dto;

import java.io.Serializable;

import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.InterestPaymentMethod;
import com.ocb.oma.validation.InterestRateType;
import com.ocb.oma.validation.VND;

public class CreateDepositDTO implements Serializable {

	private static final long serialVersionUID = -2824193800579985223L;

	private String openingAccountId;
	@Digits
	private Long amount;
	@Digits
	private Integer period;

	private String periodUnit;
	private Boolean autoCapitalisation;
	private Boolean autoRenewal;
	private String settlementAccountId;
	private String settlementAccountIntId;
	@Digits
	private Integer numberOfDeposits;

	private String iconId;

	@VND
	private String currency;
	@Digits
	private Double interest;
	private String customName;
	@InterestRateType(message = "INTERESTRATETYPE_INVALID")
	private String interestRateType;
	@InterestPaymentMethod(message = "INTERESTPAYMENTMETHOD_INVALID")
	private String interestPaymentMethod;
	private String subProductCode;

	private String campaignId;
	// private String credentials;

	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Boolean getAutoCapitalisation() {
		return autoCapitalisation;
	}

	public void setAutoCapitalisation(Boolean autoCapitalisation) {
		this.autoCapitalisation = autoCapitalisation;
	}

	public Boolean getAutoRenewal() {
		return autoRenewal;
	}

	public void setAutoRenewal(Boolean autoRenewal) {
		this.autoRenewal = autoRenewal;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

//	public String getCredentials() {
//		return credentials;
//	}
//
//	public void setCredentials(String credentials) {
//		this.credentials = credentials;
//	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public String getIconId() {
		return iconId;
	}

	public void setIconId(String iconId) {
		this.iconId = iconId;
	}

	public Double getInterest() {
		return interest;
	}

	public void setInterest(Double interest) {
		this.interest = interest;
	}

	public String getInterestPaymentMethod() {
		return interestPaymentMethod;
	}

	public void setInterestPaymentMethod(String interestPaymentMethod) {
		this.interestPaymentMethod = interestPaymentMethod;
	}

	public String getInterestRateType() {
		return interestRateType;
	}

	public void setInterestRateType(String interestRateType) {
		this.interestRateType = interestRateType;
	}

	public Integer getNumberOfDeposits() {
		return numberOfDeposits;
	}

	public void setNumberOfDeposits(Integer numberOfDeposits) {
		this.numberOfDeposits = numberOfDeposits;
	}

	public String getOpeningAccountId() {
		return openingAccountId;
	}

	public void setOpeningAccountId(String openingAccountId) {
		this.openingAccountId = openingAccountId;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getPeriodUnit() {
		return periodUnit;
	}

	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}

	public String getSettlementAccountId() {
		return settlementAccountId;
	}

	public void setSettlementAccountId(String settlementAccountId) {
		this.settlementAccountId = settlementAccountId;
	}

	public String getSettlementAccountIntId() {
		return settlementAccountIntId;
	}

	public void setSettlementAccountIntId(String settlementAccountIntId) {
		this.settlementAccountIntId = settlementAccountIntId;
	}

	public String getSubProductCode() {
		return subProductCode;
	}

	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}

}
