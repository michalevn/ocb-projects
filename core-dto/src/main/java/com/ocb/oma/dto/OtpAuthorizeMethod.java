/**
 * 
 */
package com.ocb.oma.dto;

/**
 * @author phuhoang
 *
 */
public enum OtpAuthorizeMethod {

	NONE("NONE"), SMSOTP("SMSOTP"), HW("HW"), SOFTOTP("SOFTOTP"), PKI("PKI"), SMS_TOKEN("SMS_TOKEN"),
	HWTOKEN("HWTOKEN"), HW_TOKEN("HW_TOKEN");

	private final String code;

	/**
	 * @param code
	 */
	private OtpAuthorizeMethod(String code) {
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
}
