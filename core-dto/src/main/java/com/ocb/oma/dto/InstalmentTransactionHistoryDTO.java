package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class InstalmentTransactionHistoryDTO implements Serializable {

	private static final long serialVersionUID = 6701177314241553472L;

	private String contractNumber;
	private String currency;
	private String type;
	private String typeDesc;
	private Date eventDate;
	private BigDecimal principalIncrease;
	private Date valueDate;
	private Date bookingDate;
	private Boolean agreementFlex;

	private BigDecimal principalRepayment;
	private BigDecimal interestRepayment;
	private BigDecimal paymentAmount;
	private BigDecimal penaltyInterest;
	private BigDecimal chargeAmount;
	private BigDecimal feeAmount;
	private BigDecimal commisionAmount;
	private BigDecimal interestRate;
	private String description;
	private Date repaymentDate;

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public BigDecimal getPrincipalIncrease() {
		return principalIncrease;
	}

	public void setPrincipalIncrease(BigDecimal principalIncrease) {
		this.principalIncrease = principalIncrease;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Boolean getAgreementFlex() {
		return agreementFlex;
	}

	public void setAgreementFlex(Boolean agreementFlex) {
		this.agreementFlex = agreementFlex;
	}

	public BigDecimal getPrincipalRepayment() {
		return principalRepayment;
	}

	public void setPrincipalRepayment(BigDecimal principalRepayment) {
		this.principalRepayment = principalRepayment;
	}

	public BigDecimal getInterestRepayment() {
		return interestRepayment;
	}

	public void setInterestRepayment(BigDecimal interestRepayment) {
		this.interestRepayment = interestRepayment;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getPenaltyInterest() {
		return penaltyInterest;
	}

	public void setPenaltyInterest(BigDecimal penaltyInterest) {
		this.penaltyInterest = penaltyInterest;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public BigDecimal getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public BigDecimal getCommisionAmount() {
		return commisionAmount;
	}

	public void setCommisionAmount(BigDecimal commisionAmount) {
		this.commisionAmount = commisionAmount;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getRepaymentDate() {
		return repaymentDate;
	}

	public void setRepaymentDate(Date repaymentDate) {
		this.repaymentDate = repaymentDate;
	}

}
