/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class TransactionHistoryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 448069583895644626L;
	private String accountNo;
	private Double amount;
	private Double balanceAfterOperation;
	private String currency;
	private String transactionId;
	private String transactionType;
	private String transactionNote;
	private String transactionTypeDesc;
	private Date transactionDate;
	private String receiverName;
	private String receiverAccount;

	/**
	 * @return the balanceAfterOperation
	 */
	public Double getBalanceAfterOperation() {
		return balanceAfterOperation;
	}

	/**
	 * @param balanceAfterOperation the balanceAfterOperation to set
	 */
	public void setBalanceAfterOperation(Double balanceAfterOperation) {
		this.balanceAfterOperation = balanceAfterOperation;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the transactionNote
	 */
	public String getTransactionNote() {
		return transactionNote;
	}

	/**
	 * @param transactionNote the transactionNote to set
	 */
	public void setTransactionNote(String transactionNote) {
		this.transactionNote = transactionNote;
	}

	/**
	 * @return the transactionTypeDesc
	 */
	public String getTransactionTypeDesc() {
		return transactionTypeDesc;
	}

	/**
	 * @param transactionTypeDesc the transactionTypeDesc to set
	 */
	public void setTransactionTypeDesc(String transactionTypeDesc) {
		this.transactionTypeDesc = transactionTypeDesc;
	}

	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the receiverName
	 */
	public String getReceiverName() {
		return receiverName;
	}

	/**
	 * @param receiverName the receiverName to set
	 */
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	/**
	 * @return the receiverAccount
	 */
	public String getReceiverAccount() {
		return receiverAccount;
	}

	/**
	 * @param receiverAccount the receiverAccount to set
	 */
	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	/**
	 * 
	 */
	public TransactionHistoryDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param accountNo
	 * @param amount
	 * @param currency
	 * @param transactionId
	 * @param transactionType
	 * @param transactionNote
	 * @param transactionTypeDesc
	 * @param transactionDate
	 * @param receiverName
	 * @param receiverAccount
	 */
	public TransactionHistoryDTO(String accountNo, Double amount, String currency, String transactionId,
			String transactionType, String transactionNote, String transactionTypeDesc, Date transactionDate,
			String receiverName, String receiverAccount) {
		super();
		this.accountNo = accountNo;
		this.amount = amount;
		this.currency = currency;
		this.transactionId = transactionId;
		this.transactionType = transactionType;
		this.transactionNote = transactionNote;
		this.transactionTypeDesc = transactionTypeDesc;
		this.transactionDate = transactionDate;
		this.receiverName = receiverName;
		this.receiverAccount = receiverAccount;
	}

}
