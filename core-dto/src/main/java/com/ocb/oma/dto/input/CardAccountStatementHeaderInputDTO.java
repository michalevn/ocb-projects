/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class CardAccountStatementHeaderInputDTO implements Serializable {

	private String[] cardAccountNumber;
	private String cardNumber;
	private Date dateFrom;
	private Date dateTo;
	private int pageOffset;
	private int pageSize;
	private String productId; //�CardId� + �_� + �CARD�
	
	
	
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public CardAccountStatementHeaderInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the cardAccountNumber
	 */
	public String[] getCardAccountNumber() {
		return cardAccountNumber;
	}
	/**
	 * @param cardAccountNumber the cardAccountNumber to set
	 */
	public void setCardAccountNumber(String[] cardAccountNumber) {
		this.cardAccountNumber = cardAccountNumber;
	}
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}
	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}
	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	/**
	 * @return the dateTo
	 */
	public Date getDateTo() {
		return dateTo;
	}
	/**
	 * @param dateTo the dateTo to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	/**
	 * @return the pageOffset
	 */
	public int getPageOffset() {
		return pageOffset;
	}
	/**
	 * @param pageOffset the pageOffset to set
	 */
	public void setPageOffset(int pageOffset) {
		this.pageOffset = pageOffset;
	}
	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public CardAccountStatementHeaderInputDTO(String[] cardAccountNumber, String cardNumber, Date dateFrom,
			Date dateTo, int pageOffset, int pageSize) {
		super();
		this.cardAccountNumber = cardAccountNumber;
		this.cardNumber = cardNumber;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.pageOffset = pageOffset;
		this.pageSize = pageSize;
	}
	
	
}
