/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum  TemplateType{

	INTERNAL("INTERNAL"), EXTERNAL("EXTERNAL"), FAST("FAST");// ck trong he thong/ ngoai he thong/ nhanh
	/**
	 * 
	 */
	TemplateType(String templateType) {
		// TODO Auto-generated constructor stub
		this.templateTypeCode = templateType;
	}

	private final String templateTypeCode;

	/**
	 * @return the templateTypeCode
	 */
	public String getTemplateTypeCode() {
		return templateTypeCode;
	}

	public static void validTemplateType(String templateType) {
		boolean check = false;
		for (TemplateType configType : TemplateType.values()) {
			if (configType.getTemplateTypeCode().equals(templateType)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.TEMPLATE_TYPE_INVALID,
					"recipient type has to be on of these values :  INTERNAL,EXTERNAL,FAST");
		}
	}

}
