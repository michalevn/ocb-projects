package com.ocb.oma.dto;

import java.io.Serializable;

public class CreateDepositOutputDTO implements Serializable {

	private static final long serialVersionUID = -2824193800579985223L;

	private String resultCode;
	private String resultMsg;
	private Integer numOfSuccess;
	private Integer numOfFailure;
	private String[] lstDepositNumber;
	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultMsg
	 */
	public String getResultMsg() {
		return resultMsg;
	}
	/**
	 * @param resultMsg the resultMsg to set
	 */
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	/**
	 * @return the numOfSuccess
	 */
	public Integer getNumOfSuccess() {
		return numOfSuccess;
	}
	/**
	 * @param numOfSuccess the numOfSuccess to set
	 */
	public void setNumOfSuccess(Integer numOfSuccess) {
		this.numOfSuccess = numOfSuccess;
	}
	/**
	 * @return the numOfFailure
	 */
	public Integer getNumOfFailure() {
		return numOfFailure;
	}
	/**
	 * @param numOfFailure the numOfFailure to set
	 */
	public void setNumOfFailure(Integer numOfFailure) {
		this.numOfFailure = numOfFailure;
	}
	/**
	 * @return the lstDepositNumber
	 */
	public String[] getLstDepositNumber() {
		return lstDepositNumber;
	}
	/**
	 * @param lstDepositNumber the lstDepositNumber to set
	 */
	public void setLstDepositNumber(String[] lstDepositNumber) {
		this.lstDepositNumber = lstDepositNumber;
	}
	public CreateDepositOutputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CreateDepositOutputDTO(String resultCode, String resultMsg, Integer numOfSuccess, Integer numOfFailure,
			String[] lstDepositNumber) {
		super();
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
		this.numOfSuccess = numOfSuccess;
		this.numOfFailure = numOfFailure;
		this.lstDepositNumber = lstDepositNumber;
	}
	
	

}
