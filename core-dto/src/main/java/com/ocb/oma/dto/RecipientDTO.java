/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class RecipientDTO  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7317576818516051562L;
	private String recipientId;
	private String recipientName;
	private String recipientAddress;
	private String bankName;
	private Double amount;
	private String remitterAccountNo;
	private String toAccount;
	private String bankCode;
	private String bankCodeMA;
	private String paymentType;
	private String provinceId;
	private String branchCode;
	private String cardNumber;
	private String beneficiaryAccountNo;
	private String currency;
	
	public String getRecipientAddress() {
		return recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBeneficiaryAccountNo() {
		return beneficiaryAccountNo;
	}

	public void setBeneficiaryAccountNo(String beneficiaryAccountNo) {
		this.beneficiaryAccountNo = beneficiaryAccountNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	/**
	 * @return the remitterAccountNo
	 */
	public String getRemitterAccountNo() {
		return remitterAccountNo;
	}

	/**
	 * @param remitterAccountNo the remitterAccountNo to set
	 */
	public void setRemitterAccountNo(String remitterAccountNo) {
		this.remitterAccountNo = remitterAccountNo;
	}

	
	/**
	 * @param bankCodeMA the bankCodeMA to set
	 */
	public void setBankCodeMA(String bankCodeMA) {
		this.bankCodeMA = bankCodeMA;
	}
	/**
	 * @return the bankCodeMA
	 */
	public String getBankCodeMA() {
		return bankCodeMA;
	}

	/**
	 * @return the recipientId
	 */
	public String getRecipientId() {
		return recipientId;
	}

	/**
	 * @param recipientId the recipientId to set
	 */
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	/**
	 * @return the recipientName
	 */
	public String getRecipientName() {
		return recipientName;
	}

	/**
	 * @param recipientName the recipientName to set
	 */
	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	/**
	 * @return the toAccount
	 */
	public String getToAccount() {
		return toAccount;
	}

	/**
	 * @param toAccount the toAccount to set
	 */
	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the provinceId
	 */
	public String getProvinceId() {
		return provinceId;
	}

	/**
	 * @param provinceId the provinceId to set
	 */
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}

	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	 
	/**
	 * 
	 */
	public RecipientDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param recipientId
	 * @param recipientName
	 * @param toAccount
	 * @param bankName
	 * @param bankCode
	 * @param paymentType
	 * @param provinceId
	 * @param branchCode
	 */
	public RecipientDTO(String recipientId, String recipientName, String toAccount, String bankName, String bankCode,
			String paymentType, String provinceId, String branchCode) {
		super();
		this.recipientId = recipientId;
		this.recipientName = recipientName;
		this.toAccount = toAccount;
		this.bankName = bankName;
		this.bankCode = bankCode;
		this.paymentType = paymentType;
		this.provinceId = provinceId;
		this.branchCode = branchCode;
	}

}
