package com.ocb.oma.dto;

import java.util.Date;

public class LastInfoDTO {
	private Date date;
	private Double amount;
	private String productId;
	private String currency;
	private Boolean infoAvailable;
	
	

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}



	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}



	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}



	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}



	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}



	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}



	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}



	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}



	/**
	 * @return the infoAvailable
	 */
	public Boolean getInfoAvailable() {
		return infoAvailable;
	}



	/**
	 * @param infoAvailable the infoAvailable to set
	 */
	public void setInfoAvailable(Boolean infoAvailable) {
		this.infoAvailable = infoAvailable;
	}



	public LastInfoDTO() {
		// TODO Auto-generated constructor stub
	}

}
