/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;

/**
 * @author phuhoang
 *
 */
@DateRange(from = "realizationDateFrom", to = "realizationDateTo", message = "realizationDateFrom.less.than.or.equal.to.realizationDateTo")
public class PaymentsInFutureInputDTO implements Serializable {

	@Digits
	private int pageNumber;
	@Digits
	private int pageSize;
	private Date realizationDateFrom;
	private Date realizationDateTo;

	private String statusPaymentCriteria;

	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the realizationDateFrom
	 */
	public Date getRealizationDateFrom() {
		return realizationDateFrom;
	}

	/**
	 * @param realizationDateFrom the realizationDateFrom to set
	 */
	public void setRealizationDateFrom(Date realizationDateFrom) {
		this.realizationDateFrom = realizationDateFrom;
	}

	/**
	 * @return the realizationDateTo
	 */
	public Date getRealizationDateTo() {
		return realizationDateTo;
	}

	/**
	 * @param realizationDateTo the realizationDateTo to set
	 */
	public void setRealizationDateTo(Date realizationDateTo) {
		this.realizationDateTo = realizationDateTo;
	}

	/**
	 * @return the statusPaymentCriteria
	 */
	public String getStatusPaymentCriteria() {
		return statusPaymentCriteria;
	}

	/**
	 * @param statusPaymentCriteria the statusPaymentCriteria to set
	 */
	public void setStatusPaymentCriteria(String statusPaymentCriteria) {
		this.statusPaymentCriteria = statusPaymentCriteria;
	}

	public PaymentsInFutureInputDTO(int pageNumber, int pageSize, Date realizationDateFrom, Date realizationDateTo,
			String statusPaymentCriteria) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.realizationDateFrom = realizationDateFrom;
		this.realizationDateTo = realizationDateTo;
		this.statusPaymentCriteria = statusPaymentCriteria;
	}

	public PaymentsInFutureInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
