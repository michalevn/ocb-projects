/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum StatusesType {

	NEW("NEW"), TO_ACCEPT("TO_ACCEPT"), READY("READY");
	/**
	 * 
	 */
	StatusesType(String statusesType) {
		// TODO Auto-generated constructor stub
		this.statusesTypeCode = statusesType;
	}

	private final String statusesTypeCode;

	/**
	 * @return the statusesTypeCode
	 */
	public String getStatusesTypeCode() {
		return statusesTypeCode;
	}

	/**
	 * 
	 * @param statusesTypeCode
	 */
	public static void validStatusesType(String statusesTypeCode) {
		boolean check = false;
		for (StatusesType configType : StatusesType.values()) {
			if (configType.getStatusesTypeCode().equals(statusesTypeCode)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.STATUSES_TYPE_INVALID,
					"status type has to be on of these values :  NEW , TO_ACCEPT, READY");
		}
	}

}
