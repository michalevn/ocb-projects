package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class WebLimitDTO implements Serializable {

	private static final long serialVersionUID = 1247993301677964257L;

	private Long id;
	private String type;
	private BigDecimal limitValue;
	private BigDecimal usedLimit;
	private String companyGlobus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getLimitValue() {
		return limitValue;
	}

	public void setLimitValue(BigDecimal limitValue) {
		this.limitValue = limitValue;
	}

	public BigDecimal getUsedLimit() {
		return usedLimit;
	}

	public void setUsedLimit(BigDecimal usedLimit) {
		this.usedLimit = usedLimit;
	}

	public String getCompanyGlobus() {
		return companyGlobus;
	}

	public void setCompanyGlobus(String companyGlobus) {
		this.companyGlobus = companyGlobus;
	}

}
