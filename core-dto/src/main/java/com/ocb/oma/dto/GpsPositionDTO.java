package com.ocb.oma.dto;

import java.io.Serializable;

public class GpsPositionDTO implements Serializable {

	private static final long serialVersionUID = 7136525088361595616L;

	private Float lat;
	private Float lng;

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

}
