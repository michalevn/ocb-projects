/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

import com.ocb.oma.validation.BillCode;
import com.ocb.oma.validation.ServiceProviderCode;

/**
 * @author phuhoang
 *
 */
public class GetBillInputDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1932132925317201209L;
	private Boolean addToBeneficiary;
	@BillCode(message = "BILLCODE_INVALID")
	private String billCode;
	private String providerCode;
	private String serviceCode;

	/**
	 * @return the addToBeneficiary
	 */
	public Boolean getAddToBeneficiary() {
		return addToBeneficiary;
	}

	/**
	 * @param addToBeneficiary the addToBeneficiary to set
	 */
	public void setAddToBeneficiary(Boolean addToBeneficiary) {
		this.addToBeneficiary = addToBeneficiary;
	}

	/**
	 * @return the billCode
	 */
	public String getBillCode() {
		return billCode;
	}

	/**
	 * @param billCode the billCode to set
	 */
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	/**
	 * @return the providerCode
	 */
	public String getProviderCode() {
		return providerCode;
	}

	/**
	 * @param providerCode the providerCode to set
	 */
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * 
	 */
	public GetBillInputDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param addToBeneficiary
	 * @param billCode
	 * @param providerCode
	 * @param serviceCode
	 */
	public GetBillInputDTO(Boolean addToBeneficiary, String billCode, String providerCode, String serviceCode) {
		super();
		this.addToBeneficiary = addToBeneficiary;
		this.billCode = billCode;
		this.providerCode = providerCode;
		this.serviceCode = serviceCode;
	}

}
