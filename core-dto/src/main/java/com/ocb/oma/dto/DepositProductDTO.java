package com.ocb.oma.dto;

import java.io.Serializable;

public class DepositProductDTO implements Serializable {

	private static final long serialVersionUID = -9114314291079253124L;

	private String subProductCode;
	private String currency;
	private String categoryCode;
	private String description;

	public String getSubProductCode() {
		return subProductCode;
	}

	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
