/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class TransactionBlockedDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5024967821491409433L;
	private String accountId;
	private String accountNo;
	private Date createdDate;
	private String transactionRef;
	private String blockedBy;
	private String reason;
	private Double blockedAmount;
	
	private String blockadeId;
	private String type;
	private String blockadeDescription;
	private Date dateFrom;
	private Double amount;
	private String currency;
	private String merchant;
	private String remark;
	

	
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the blockadeId
	 */
	public String getBlockadeId() {
		return blockadeId;
	}

	/**
	 * @param blockadeId the blockadeId to set
	 */
	public void setBlockadeId(String blockadeId) {
		this.blockadeId = blockadeId;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the blockadeDescription
	 */
	public String getBlockadeDescription() {
		return blockadeDescription;
	}

	/**
	 * @param blockadeDescription the blockadeDescription to set
	 */
	public void setBlockadeDescription(String blockadeDescription) {
		this.blockadeDescription = blockadeDescription;
	}

	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the merchant
	 */
	public String getMerchant() {
		return merchant;
	}

	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the transactionRef
	 */
	public String getTransactionRef() {
		return transactionRef;
	}

	/**
	 * @param transactionRef the transactionRef to set
	 */
	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	/**
	 * @return the blockedBy
	 */
	public String getBlockedBy() {
		return blockedBy;
	}

	/**
	 * @param blockedBy the blockedBy to set
	 */
	public void setBlockedBy(String blockedBy) {
		this.blockedBy = blockedBy;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the blockedAmount
	 */
	public Double getBlockedAmount() {
		return blockedAmount;
	}

	/**
	 * @param blockedAmount the blockedAmount to set
	 */
	public void setBlockedAmount(Double blockedAmount) {
		this.blockedAmount = blockedAmount;
	}

}
