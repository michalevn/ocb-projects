/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class SendAuthorizationSmsOtpDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5326233758510073722L;
	private String language;
	private String mobileNumber;
	private String smsTemplate;
	private Long expiryTime;
	private TransactionInfo transactionInfo;
	private AuthorizationResponse authorizationResponseType;
	private TransactionInfo transactionResult;
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the smsTemplate
	 */
	public String getSmsTemplate() {
		return smsTemplate;
	}
	/**
	 * @param smsTemplate the smsTemplate to set
	 */
	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}
	/**
	 * @return the expiryTime
	 */
	public Long getExpiryTime() {
		return expiryTime;
	}
	/**
	 * @param expiryTime the expiryTime to set
	 */
	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}
	/**
	 * @return the transactionInfo
	 */
	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}
	/**
	 * @param transactionInfo the transactionInfo to set
	 */
	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}
	/**
	 * @return the authorizationResponseType
	 */
	public AuthorizationResponse getAuthorizationResponseType() {
		return authorizationResponseType;
	}
	/**
	 * @param authorizationResponseType the authorizationResponseType to set
	 */
	public void setAuthorizationResponseType(AuthorizationResponse authorizationResponseType) {
		this.authorizationResponseType = authorizationResponseType;
	}
	/**
	 * @return the transactionResult
	 */
	public TransactionInfo getTransactionResult() {
		return transactionResult;
	}
	/**
	 * @param transactionResult the transactionResult to set
	 */
	public void setTransactionResult(TransactionInfo transactionResult) {
		this.transactionResult = transactionResult;
	}
	
	
	
	 
}
