package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;

public class StudentTuitionFeeDTO implements Serializable {

	private static final long serialVersionUID = -4903692630834988711L;

	private TuitionFeeDTO tuitionFee;
	private StudentDTO student;
	private List<TuitionPaymentDTO> tuitionPayment;

	public TuitionFeeDTO getTuitionFee() {
		return tuitionFee;
	}

	public void setTuitionFee(TuitionFeeDTO tuitionFee) {
		this.tuitionFee = tuitionFee;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public List<TuitionPaymentDTO> getTuitionPayment() {
		return tuitionPayment;
	}

	public void setTuitionPayment(List<TuitionPaymentDTO> tuitionPayment) {
		this.tuitionPayment = tuitionPayment;
	}

}
