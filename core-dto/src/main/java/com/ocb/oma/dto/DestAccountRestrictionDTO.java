package com.ocb.oma.dto;

public class DestAccountRestrictionDTO {
	private Integer destCategory;
	private String destSubCategory;
	
	

	/**
	 * @return the destCategory
	 */
	public Integer getDestCategory() {
		return destCategory;
	}



	/**
	 * @param destCategory the destCategory to set
	 */
	public void setDestCategory(Integer destCategory) {
		this.destCategory = destCategory;
	}



	/**
	 * @return the destSubCategory
	 */
	public String getDestSubCategory() {
		return destSubCategory;
	}



	/**
	 * @param destSubCategory the destSubCategory to set
	 */
	public void setDestSubCategory(String destSubCategory) {
		this.destSubCategory = destSubCategory;
	}



	public DestAccountRestrictionDTO() {
		// TODO Auto-generated constructor stub
	}

}
