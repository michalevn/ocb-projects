/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class TransactionInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8039110909754097228L;
	private String cRefNum;
	private String userId;
	private String clientCode ="OMNI";
	private String branchCode ="mobilebanking";
	/**
	 * @return the cRefNum
	 */
	public String getcRefNum() {
		return cRefNum;
	}
	/**
	 * @param cRefNum the cRefNum to set
	 */
	public void setcRefNum(String cRefNum) {
		this.cRefNum = cRefNum;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the clientCode
	 */
	public String getClientCode() {
		return clientCode;
	}
	/**
	 * @param clientCode the clientCode to set
	 */
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}
	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
}
