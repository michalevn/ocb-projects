package com.ocb.oma.dto;

import java.io.Serializable;

public class OwnersDTO implements Serializable {
	private String customerId;
	private String relationType;
	private String name;
	private String addressStreetPrefix;
	private String addressStreet;
	private String houseNo;
	private String apartmentNo;
	private String postCode;
	private String town;
	private String country;
	private String fullName;
	private String residence;
	private boolean current;

	/**
	 * @return the current
	 */
	public boolean getCurrent() {
		return current;
	}

	/**
	 * @param current the current to set
	 */
	public void setCurrent(boolean current) {
		this.current = current;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param relationType the relationType to set
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the addressStreetPrefix
	 */
	public String getAddressStreetPrefix() {
		return addressStreetPrefix;
	}

	/**
	 * @param addressStreetPrefix the addressStreetPrefix to set
	 */
	public void setAddressStreetPrefix(String addressStreetPrefix) {
		this.addressStreetPrefix = addressStreetPrefix;
	}

	/**
	 * @return the addressStreet
	 */
	public String getAddressStreet() {
		return addressStreet;
	}

	/**
	 * @param addressStreet the addressStreet to set
	 */
	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	/**
	 * @return the houseNo
	 */
	public String getHouseNo() {
		return houseNo;
	}

	/**
	 * @param houseNo the houseNo to set
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	/**
	 * @return the apartmentNo
	 */
	public String getApartmentNo() {
		return apartmentNo;
	}

	/**
	 * @param apartmentNo the apartmentNo to set
	 */
	public void setApartmentNo(String apartmentNo) {
		this.apartmentNo = apartmentNo;
	}

	/**
	 * @return the postCode
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * @param postCode the postCode to set
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * @return the town
	 */
	public String getTown() {
		return town;
	}

	/**
	 * @param town the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the residence
	 */
	public String getResidence() {
		return residence;
	}

	/**
	 * @param residence the residence to set
	 */
	public void setResidence(String residence) {
		this.residence = residence;
	}

	/**
	 * 
	 * @param customerId
	 * @param relationType
	 * @param name
	 * @param addressStreetPrefix
	 * @param addressStreet
	 * @param houseNo
	 * @param apartmentNo
	 * @param postCode
	 * @param town
	 * @param country
	 * @param fullName
	 * @param residence
	 * @param current
	 */
	public OwnersDTO(String customerId, String relationType, String name, String addressStreetPrefix,
			String addressStreet, String houseNo, String apartmentNo, String postCode, String town, String country,
			String fullName, String residence, boolean current) {
		super();
		this.customerId = customerId;
		this.relationType = relationType;
		this.name = name;
		this.addressStreetPrefix = addressStreetPrefix;
		this.addressStreet = addressStreet;
		this.houseNo = houseNo;
		this.apartmentNo = apartmentNo;
		this.postCode = postCode;
		this.town = town;
		this.country = country;
		this.fullName = fullName;
		this.residence = residence;
		this.current = current;
	}

	public OwnersDTO() {
		// TODO Auto-generated constructor stub
	}

}
