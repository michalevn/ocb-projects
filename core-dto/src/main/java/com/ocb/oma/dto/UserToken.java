package com.ocb.oma.dto;

import java.util.Date;

import com.ocb.oma.oomni.dto.OldAuthenticateResponseDTO;

public class UserToken {

	private String username;
	private String deviceId;
	private String deviceToken;
	private String token;
	private Date expireTime;
	private OldAuthenticateResponseDTO oldAuthen;
	private String hashedData;
	private String firstName;
	private String lastName;
	private Boolean requireOtp;
	private Boolean isNotRegistered;

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the hashedData
	 */
	public String getHashedData() {
		return hashedData;
	}

	/**
	 * @param hashedData the hashedData to set
	 */
	public void setHashedData(String hashedData) {
		this.hashedData = hashedData;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	/**
	 * @param oldAuthen the oldAuthen to set
	 */
	public void setOldAuthen(OldAuthenticateResponseDTO oldAuthen) {
		this.oldAuthen = oldAuthen;
	}

	/**
	 * @return the oldAuthen
	 */
	public OldAuthenticateResponseDTO getOldAuthen() {
		return oldAuthen;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setRequireOtp(Boolean requireOtp) {
		this.requireOtp = requireOtp;
	}

	public Boolean getRequireOtp() {
		return requireOtp;
	}

	public Boolean getIsNotRegistered() {
		return isNotRegistered;
	}

	public void setIsNotRegistered(Boolean isNotRegistered) {
		this.isNotRegistered = isNotRegistered;
	}
}
