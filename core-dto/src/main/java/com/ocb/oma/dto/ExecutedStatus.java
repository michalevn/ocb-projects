/**
 * 
 */
package com.ocb.oma.dto;

/**
 * @author phuhoang
 *
 */
public enum ExecutedStatus {
	EXECUTED("EXECUTED"), INTERNAL_ERROR("INTERNAL_ERROR"), REJECTED("REJECTED");

	private String code;

	/**
	 * 
	 */
	private ExecutedStatus(String code) {
		// TODO Auto-generated constructor stub
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
}
