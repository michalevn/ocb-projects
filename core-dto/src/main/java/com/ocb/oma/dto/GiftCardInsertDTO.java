/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;

import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.resources.model.OmniGiftCard;
import com.ocb.oma.resources.model.OmniGiftCardReceiver;
import com.ocb.oma.validation.GiftCardInsertDTOValid;

/**
 * @author phuhoang
 *
 */

@GiftCardInsertDTOValid(message = ValidationMessageConstant.DEBITACCOUNT_AND_CREDITACCOUNT_MUST_BE_DIFFERENT)
public class GiftCardInsertDTO implements Serializable {

	private static final long serialVersionUID = 7959839032394173390L;

	private OmniGiftCard giftCard;

	private List<OmniGiftCardReceiver> receivers;
	
	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the giftCard
	 */
	public OmniGiftCard getGiftCard() {
		return giftCard;
	}

	/**
	 * @param giftCard the giftCard to set
	 */
	public void setGiftCard(OmniGiftCard giftCard) {
		this.giftCard = giftCard;
	}

	/**
	 * @return the receivers
	 */
	public List<OmniGiftCardReceiver> getReceivers() {
		return receivers;
	}

	/**
	 * @param receivers the receivers to set
	 */
	public void setReceivers(List<OmniGiftCardReceiver> receivers) {
		this.receivers = receivers;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

	
	
}
