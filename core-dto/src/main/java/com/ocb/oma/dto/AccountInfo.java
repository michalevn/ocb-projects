/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class AccountInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1305044777127689761L;
	private String accountId;
	private String accountName;
	private String accountNo;
	private String currency;

	// so du kha dung
	private Double currentBalance;
	// so du hien tai
	private Double accessibleAssets;
	// han muc thau chi
	private Double allowedLimit;
	// Ngoai te cua han muc thau chi
	private String allowedLimitCurrency;
	// ngay mo
	private Date openDate;
	// so du phong toa
	private Double blockAssets;
	// han che giao dich
	private Boolean accountRestrictFlag;
	// ten chu tai khoan
	private String customName;

	private String category;
	private String subProduct;

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the currentBalance
	 */
	public Double getCurrentBalance() {
		return currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(Double currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the accessibleAssets
	 */
	public Double getAccessibleAssets() {
		return accessibleAssets;
	}

	/**
	 * @param accessibleAssets the accessibleAssets to set
	 */
	public void setAccessibleAssets(Double accessibleAssets) {
		this.accessibleAssets = accessibleAssets;
	}

	/**
	 * @return the allowedLimit
	 */
	public Double getAllowedLimit() {
		return allowedLimit;
	}

	/**
	 * @param allowedLimit the allowedLimit to set
	 */
	public void setAllowedLimit(Double allowedLimit) {
		this.allowedLimit = allowedLimit;
	}

	/**
	 * @return the allowedLimitCurrency
	 */
	public String getAllowedLimitCurrency() {
		return allowedLimitCurrency;
	}

	/**
	 * @param allowedLimitCurrency the allowedLimitCurrency to set
	 */
	public void setAllowedLimitCurrency(String allowedLimitCurrency) {
		this.allowedLimitCurrency = allowedLimitCurrency;
	}

	/**
	 * @return the openDate
	 */
	public Date getOpenDate() {
		return openDate;
	}

	/**
	 * @param openDate the openDate to set
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	/**
	 * @return the blockAssets
	 */
	public Double getBlockAssets() {
		return blockAssets;
	}

	/**
	 * @param blockAssets the blockAssets to set
	 */
	public void setBlockAssets(Double blockAssets) {
		this.blockAssets = blockAssets;
	}

	/**
	 * @return the accountRestrictFlag
	 */
	public Boolean getAccountRestrictFlag() {
		return accountRestrictFlag;
	}

	/**
	 * @param accountRestrictFlag the accountRestrictFlag to set
	 */
	public void setAccountRestrictFlag(Boolean accountRestrictFlag) {
		this.accountRestrictFlag = accountRestrictFlag;
	}

	/**
	 * @return the customName
	 */
	public String getCustomName() {
		return customName;
	}

	/**
	 * @param customName the customName to set
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setSubProduct(String subProduct) {
		this.subProduct = subProduct;
	}

	public String getSubProduct() {
		return subProduct;
	}
}
