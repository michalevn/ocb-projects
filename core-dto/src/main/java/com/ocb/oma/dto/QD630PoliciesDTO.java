/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class QD630PoliciesDTO implements Serializable {
	// So tien- tu khoan
	private double fromAmount;
	// So tien- den khoan
	private double toAmount;
	// phuong thuc dang ky (SMSOTP, HW, SOFTOTP, PKI)
	private String registeredMethod;
	// cho phep thay doi phuong thuc
	private boolean allowChangingMethod;

	/**
	 * @return the fromAmount
	 */
	public double getFromAmount() {
		return fromAmount;
	}

	/**
	 * @param fromAmount the fromAmount to set
	 */
	public void setFromAmount(double fromAmount) {
		this.fromAmount = fromAmount;
	}

	/**
	 * @return the toAmount
	 */
	public double getToAmount() {
		return toAmount;
	}

	/**
	 * @param toAmount the toAmount to set
	 */
	public void setToAmount(double toAmount) {
		this.toAmount = toAmount;
	}

	/**
	 * @return the registeredMethod
	 */
	public String getRegisteredMethod() {
		return registeredMethod;
	}

	/**
	 * @param registeredMethod the registeredMethod to set
	 */
	public void setRegisteredMethod(String registeredMethod) {
		this.registeredMethod = registeredMethod;
	}

	/**
	 * @return the allowChangingMethod
	 */
	public boolean isAllowChangingMethod() {
		return allowChangingMethod;
	}

	/**
	 * @param allowChangingMethod the allowChangingMethod to set
	 */
	public void setAllowChangingMethod(boolean allowChangingMethod) {
		this.allowChangingMethod = allowChangingMethod;
	}

}
