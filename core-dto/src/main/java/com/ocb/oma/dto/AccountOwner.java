package com.ocb.oma.dto;

import java.io.Serializable;

public class AccountOwner implements Serializable {

	private static final long serialVersionUID = 8979837330081080504L;

	private String fullName;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
