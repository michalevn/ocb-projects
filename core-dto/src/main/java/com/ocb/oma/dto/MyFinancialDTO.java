/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class MyFinancialDTO implements Serializable {

	private String currency;
	private Double upcomingRepayment;
	private Double upcomingDepositMaturity;

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the upcomingRepayment
	 */
	public Double getUpcomingRepayment() {
		return upcomingRepayment;
	}

	/**
	 * @param upcomingRepayment the upcomingRepayment to set
	 */
	public void setUpcomingRepayment(Double upcomingRepayment) {
		this.upcomingRepayment = upcomingRepayment;
	}

	/**
	 * @return the upcomingDepositMaturity
	 */
	public Double getUpcomingDepositMaturity() {
		return upcomingDepositMaturity;
	}

	/**
	 * @param upcomingDepositMaturity the upcomingDepositMaturity to set
	 */
	public void setUpcomingDepositMaturity(Double upcomingDepositMaturity) {
		this.upcomingDepositMaturity = upcomingDepositMaturity;
	}

	/**
	 * 
	 */
	public MyFinancialDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param currency
	 * @param upcomingRepayment
	 * @param upcomingDepositMaturity
	 */
	public MyFinancialDTO(String currency, Double upcomingRepayment, Double upcomingDepositMaturity) {
		super();
		this.currency = currency;
		this.upcomingRepayment = upcomingRepayment;
		this.upcomingDepositMaturity = upcomingDepositMaturity;
	}

}
