package com.ocb.oma.dto;

import java.io.Serializable;

public class PaymentDetails implements Serializable {

	private static final long serialVersionUID = -6206389655826708147L;

	private String bankDetails;
	private String fee;
	private String feeAccount;
	private String creditAccountBankCode;
	private String creditAccountBankBranchCode;
	private String creditAccountProvinceCode;

	/**
	 * @return the bankDetails
	 */
	public String getBankDetails() {
		return bankDetails;
	}

	/**
	 * @param bankDetails the bankDetails to set
	 */
	public void setBankDetails(String bankDetails) {
		this.bankDetails = bankDetails;
	}

	/**
	 * @return the fee
	 */
	public String getFee() {
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(String fee) {
		this.fee = fee;
	}

	/**
	 * @return the feeAccount
	 */
	public String getFeeAccount() {
		return feeAccount;
	}

	/**
	 * @param feeAccount the feeAccount to set
	 */
	public void setFeeAccount(String feeAccount) {
		this.feeAccount = feeAccount;
	}

	/**
	 * @return the creditAccountBankCode
	 */
	public String getCreditAccountBankCode() {
		return creditAccountBankCode;
	}

	/**
	 * @param creditAccountBankCode the creditAccountBankCode to set
	 */
	public void setCreditAccountBankCode(String creditAccountBankCode) {
		this.creditAccountBankCode = creditAccountBankCode;
	}

	/**
	 * @return the creditAccountBankBranchCode
	 */
	public String getCreditAccountBankBranchCode() {
		return creditAccountBankBranchCode;
	}

	/**
	 * @param creditAccountBankBranchCode the creditAccountBankBranchCode to set
	 */
	public void setCreditAccountBankBranchCode(String creditAccountBankBranchCode) {
		this.creditAccountBankBranchCode = creditAccountBankBranchCode;
	}

	/**
	 * @return the creditAccountProvinceCode
	 */
	public String getCreditAccountProvinceCode() {
		return creditAccountProvinceCode;
	}

	/**
	 * @param creditAccountProvinceCode the creditAccountProvinceCode to set
	 */
	public void setCreditAccountProvinceCode(String creditAccountProvinceCode) {
		this.creditAccountProvinceCode = creditAccountProvinceCode;
	}

	public PaymentDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaymentDetails(String bankDetails, String fee, String feeAccount, String creditAccountBankCode, String creditAccountBankBranchCode,
			String creditAccountProvinceCode) {
		super();
		this.bankDetails = bankDetails;
		this.fee = fee;
		this.feeAccount = feeAccount;
		this.creditAccountBankCode = creditAccountBankCode;
		this.creditAccountBankBranchCode = creditAccountBankBranchCode;
		this.creditAccountProvinceCode = creditAccountProvinceCode;
	}

}
