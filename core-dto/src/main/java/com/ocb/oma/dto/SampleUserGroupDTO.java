package com.ocb.oma.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SampleUserGroupDTO implements Serializable {

	private static final long serialVersionUID = 1906086989689692154L;

	@NotBlank(message = "userGroup.name.required")
	@Size(min = 8, max = 16, message = "userGroup.name.size.invalid")
	private String name;

	@NotBlank(message = "userGroup.description.required")
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
