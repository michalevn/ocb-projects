/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum StatusesUpdateAvatar {

	OK("OK"), INTERNAL_ERROR("INTERNAL_ERROR"), SIZE_INVALID("SIZE_INVALID"),
	CONTENT_TYPE_INVALID("CONTENT_TYPE_INVALID");
	/**
	 * 
	 */
	StatusesUpdateAvatar(String statusesUpdateAvatar) {
		// TODO Auto-generated constructor stub
		this.statusesUpdateAvatarCode = statusesUpdateAvatar;
	}

	private final String statusesUpdateAvatarCode;

	/**
	 * @return the statusesUpdateAvatarCode
	 */
	public String getStatusesUpdateAvatarCode() {
		return statusesUpdateAvatarCode;
	}

	/**
	 * 
	 * @param statusesUpdateAvatarCode
	 */
	public static void validStatusesUpdateAvatar(String statusesUpdateAvatarCode) {
		boolean check = false;
		for (StatusesUpdateAvatar configType : StatusesUpdateAvatar.values()) {
			if (configType.getStatusesUpdateAvatarCode().equals(statusesUpdateAvatarCode)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.STATUSES_TYPE_INVALID,
					"status Avatar has to be on of these values :  OK,INTERNAL_ERROR, NO_AVATAR");
		}
	}

}
