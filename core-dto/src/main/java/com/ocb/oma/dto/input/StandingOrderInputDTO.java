/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.dto.ValidationMessageConstant;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.PeriodUnit;
import com.ocb.oma.validation.ShortName;
import com.ocb.oma.validation.VND;

/**
 * @author phuhoang
 *
 */
@DateRange(from = "startDate", to = "endDate", message = "startDate.less.than.or.equal.to.endDate")
public class StandingOrderInputDTO implements Serializable {

	@ShortName(message = "SHORTNAME_INVALID")
	private String shortName;
	@Digits(message = "AMOUNT_" + ValidationMessageConstant.MUST_BE_NUMBER)
	private Long amount;
	private String[] beneficiary;
	private String creditAccount;
	private String[] remarks;
	private String debitAccountId;
	@VND(message = ValidationMessageConstant.CURRENCY_MUST_BE_VND)
	private String currency;
	private Date endDate;
	@PeriodUnit(message = "PERIODUNIT_INVALID")
	private String periodUnit;
	@Digits(message = "PERIODCOUNT_" + ValidationMessageConstant.MUST_BE_NUMBER)
	private String periodCount;
	@Digits(message = "DAYOFMONTH_" + ValidationMessageConstant.MUST_BE_NUMBER)
	private int dayOfMonth;
	private Date startDate;

	private String customerId;
	private long remitterId;
	private String standingOrderId;
	private Date nextDate;
	private boolean addToBasket;
	private boolean transferFromTemplate;
	private String templateId;
	

	private VerifyOtpToken otpValue;
	private String otpMethod;


	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the remitterId
	 */
	public long getRemitterId() {
		return remitterId;
	}

	/**
	 * @param remitterId the remitterId to set
	 */
	public void setRemitterId(long remitterId) {
		this.remitterId = remitterId;
	}

	/**
	 * @return the standingOrderId
	 */
	public String getStandingOrderId() {
		return standingOrderId;
	}

	/**
	 * @param standingOrderId the standingOrderId to set
	 */
	public void setStandingOrderId(String standingOrderId) {
		this.standingOrderId = standingOrderId;
	}

	/**
	 * @return the nextDate
	 */
	public Date getNextDate() {
		return nextDate;
	}

	/**
	 * @param nextDate the nextDate to set
	 */
	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	/**
	 * @return the addToBasket
	 */
	public boolean isAddToBasket() {
		return addToBasket;
	}

	/**
	 * @param addToBasket the addToBasket to set
	 */
	public void setAddToBasket(boolean addToBasket) {
		this.addToBasket = addToBasket;
	}

	/**
	 * @return the transferFromTemplate
	 */
	public boolean isTransferFromTemplate() {
		return transferFromTemplate;
	}

	/**
	 * @param transferFromTemplate the transferFromTemplate to set
	 */
	public void setTransferFromTemplate(boolean transferFromTemplate) {
		this.transferFromTemplate = transferFromTemplate;
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public StandingOrderInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the amount
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Long amount) {
		this.amount = amount;
	}

	/**
	 * @return the beneficiary
	 */
	public String[] getBeneficiary() {
		return beneficiary;
	}

	/**
	 * @param beneficiary the beneficiary to set
	 */
	public void setBeneficiary(String[] beneficiary) {
		this.beneficiary = beneficiary;
	}

	/**
	 * @return the creditAccount
	 */
	public String getCreditAccount() {
		return creditAccount;
	}

	/**
	 * @param creditAccount the creditAccount to set
	 */
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	/**
	 * @return the remarks
	 */
	public String[] getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String[] remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the debitAccountId
	 */
	public String getDebitAccountId() {
		return debitAccountId;
	}

	/**
	 * @param debitAccountId the debitAccountId to set
	 */
	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the periodUnit
	 */
	public String getPeriodUnit() {
		return periodUnit;
	}

	/**
	 * @param periodUnit the periodUnit to set
	 */
	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}

	/**
	 * @return the periodCount
	 */
	public String getPeriodCount() {
		return periodCount;
	}

	/**
	 * @param periodCount the periodCount to set
	 */
	public void setPeriodCount(String periodCount) {
		this.periodCount = periodCount;
	}

	/**
	 * @return the dayOfMonth
	 */
	public int getDayOfMonth() {
		return dayOfMonth;
	}

	/**
	 * @param dayOfMonth the dayOfMonth to set
	 */
	public void setDayOfMonth(int dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}
	
	

}
