package com.ocb.oma.dto.input;

import java.io.Serializable;

public class ChangeUsernameInput implements Serializable {

	private static final long serialVersionUID = -22873648318197409L;
	
	private String newUsername;
	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the newUsername
	 */
	public String getNewUsername() {
		return newUsername;
	}

	/**
	 * @param newUsername the newUsername to set
	 */
	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}

	public ChangeUsernameInput() {
	}

	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	public String getOtpMethod() {
		return otpMethod;
	}

	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
