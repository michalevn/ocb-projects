/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class CardStatementHeader implements Serializable {
	
	private String cardAccountNumber;
	private String cycleEnd;
	private String cycleStart;
	private String id;
	private Double minimumRepaymentAmount;
	private String nextPaymentDate;
	private Double overDueAmount;
	private Double totalRepaymentAmount;
	private String downloadLink;
	
	
	/**
	 * @return the cardAccountNumber
	 */
	public String getCardAccountNumber() {
		return cardAccountNumber;
	}


	/**
	 * @param cardAccountNumber the cardAccountNumber to set
	 */
	public void setCardAccountNumber(String cardAccountNumber) {
		this.cardAccountNumber = cardAccountNumber;
	}


	/**
	 * @return the cycleEnd
	 */
	public String getCycleEnd() {
		return cycleEnd;
	}


	/**
	 * @param cycleEnd the cycleEnd to set
	 */
	public void setCycleEnd(String cycleEnd) {
		this.cycleEnd = cycleEnd;
	}


	/**
	 * @return the cycleStart
	 */
	public String getCycleStart() {
		return cycleStart;
	}


	/**
	 * @param cycleStart the cycleStart to set
	 */
	public void setCycleStart(String cycleStart) {
		this.cycleStart = cycleStart;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the minimumRepaymentAmount
	 */
	public Double getMinimumRepaymentAmount() {
		return minimumRepaymentAmount;
	}


	/**
	 * @param minimumRepaymentAmount the minimumRepaymentAmount to set
	 */
	public void setMinimumRepaymentAmount(Double minimumRepaymentAmount) {
		this.minimumRepaymentAmount = minimumRepaymentAmount;
	}


	/**
	 * @return the nextPaymentDate
	 */
	public String getNextPaymentDate() {
		return nextPaymentDate;
	}


	/**
	 * @param nextPaymentDate the nextPaymentDate to set
	 */
	public void setNextPaymentDate(String nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}


	/**
	 * @return the overDueAmount
	 */
	public Double getOverDueAmount() {
		return overDueAmount;
	}


	/**
	 * @param overDueAmount the overDueAmount to set
	 */
	public void setOverDueAmount(Double overDueAmount) {
		this.overDueAmount = overDueAmount;
	}


	/**
	 * @return the totalRepaymentAmount
	 */
	public Double getTotalRepaymentAmount() {
		return totalRepaymentAmount;
	}


	/**
	 * @param totalRepaymentAmount the totalRepaymentAmount to set
	 */
	public void setTotalRepaymentAmount(Double totalRepaymentAmount) {
		this.totalRepaymentAmount = totalRepaymentAmount;
	}


	/**
	 * @return the downloadLink
	 */
	public String getDownloadLink() {
		return downloadLink;
	}


	/**
	 * @param downloadLink the downloadLink to set
	 */
	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}


	public CardStatementHeader() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CardStatementHeader(String cardAccountNumber, String cycleEnd, String cycleStart, String id,
			Double minimumRepaymentAmount, String nextPaymentDate, Double overDueAmount, Double totalRepaymentAmount,
			String downloadLink) {
		super();
		this.cardAccountNumber = cardAccountNumber;
		this.cycleEnd = cycleEnd;
		this.cycleStart = cycleStart;
		this.id = id;
		this.minimumRepaymentAmount = minimumRepaymentAmount;
		this.nextPaymentDate = nextPaymentDate;
		this.overDueAmount = overDueAmount;
		this.totalRepaymentAmount = totalRepaymentAmount;
		this.downloadLink = downloadLink;
	}
	

}
