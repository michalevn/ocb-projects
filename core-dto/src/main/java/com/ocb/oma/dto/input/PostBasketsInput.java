/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class PostBasketsInput implements Serializable {
	/**
	 *  
	 */
	private static final long serialVersionUID = -1007563073653184781L;
	List<String> transfersIds;
	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the transfersIds
	 */
	public List<String> getTransfersIds() {
		return transfersIds;
	}

	/**
	 * @param transfersIds the transfersIds to set
	 */
	public void setTransfersIds(List<String> transfersIds) {
		this.transfersIds = transfersIds;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
