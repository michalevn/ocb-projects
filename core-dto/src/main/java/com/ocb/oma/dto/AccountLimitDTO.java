/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class AccountLimitDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Double minimumAmount;
	private Double maximumAmount;
	private Double remainingDailyLimit;

	public AccountLimitDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param minimumAmount
	 * @param maximumAmount
	 * @param remainingDailyLimit
	 */
	public AccountLimitDTO(Double minimumAmount, Double maximumAmount, Double remainingDailyLimit) {
		super();
		this.minimumAmount = minimumAmount;
		this.maximumAmount = maximumAmount;
		this.remainingDailyLimit = remainingDailyLimit;
	}

	/**
	 * @return the minimumAmount
	 */
	public Double getMinimumAmount() {
		return minimumAmount;
	}

	/**
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(Double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}

	/**
	 * @return the maximumAmount
	 */
	public Double getMaximumAmount() {
		return maximumAmount;
	}

	/**
	 * @param maximumAmount the maximumAmount to set
	 */
	public void setMaximumAmount(Double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}

	/**
	 * @return the remainingDailyLimit
	 */
	public Double getRemainingDailyLimit() {
		return remainingDailyLimit;
	}

	/**
	 * @param remainingDailyLimit the remainingDailyLimit to set
	 */
	public void setRemainingDailyLimit(Double remainingDailyLimit) {
		this.remainingDailyLimit = remainingDailyLimit;
	}

}
