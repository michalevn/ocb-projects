/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum  RecipientType{

	INTERNAL("INTERNAL"), EXTERNAL("EXTERNAL"), FAST("FAST");
	/**
	 * 
	 */
	RecipientType(String recipientType) {
		// TODO Auto-generated constructor stub
		this.recipientTypeCode = recipientType;
	}

	private final String recipientTypeCode;

	/**
	 * @return the recipientTypeCode
	 */
	public String getRecipientTypeCode() {
		return recipientTypeCode;
	}

	public static void validRecipientType(String recipientType) {
		boolean check = false;
		for (RecipientType configType : RecipientType.values()) {
			if (configType.getRecipientTypeCode().equals(recipientType)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.RECIPIENT_TYPE_INVALID,
					"recipient type has to be on of these values :  INTERNAL,EXTERNAL,FAST");
		}
	}

}
