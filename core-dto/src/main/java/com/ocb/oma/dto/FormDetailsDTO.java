package com.ocb.oma.dto;

import java.io.Serializable;

public class FormDetailsDTO implements Serializable {

	private static final long serialVersionUID = -5285474048940133408L;

	private String fieldName;

	private String fieldType;

	private String fieldValue;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

}
