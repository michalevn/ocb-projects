/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.validation.AccountNo;
import com.ocb.oma.validation.AmountRange;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;

/**
 * @author phuhoang
 *
 */
@DateRange(from = "fromDate", to = "toDate", message = "fromDate.less.than.or.equal.to.toDate")
@AmountRange(from = "fromAmount", to = "toAmount", message = "fromAmount.less.than.or.equal.to.toAmount")
public class TransHistoryRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4202314326652957036L;

	@AccountNo(message = "ACCOUNTNO_INVALID")
	private String accountNo;
	// private String transactionType;
	private Date fromDate;
	private Date toDate;
	private Double fromAmount;
	private Double toAmount;
	@Digits()
	private Integer page;
	@Digits()
	private Integer pageSize;

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the fromAmount
	 */
	public Double getFromAmount() {
		return fromAmount;
	}

	/**
	 * @param fromAmount the fromAmount to set
	 */
	public void setFromAmount(Double fromAmount) {
		this.fromAmount = fromAmount;
	}

	/**
	 * @return the toAmount
	 */
	public Double getToAmount() {
		return toAmount;
	}

	/**
	 * @param toAmount the toAmount to set
	 */
	public void setToAmount(Double toAmount) {
		this.toAmount = toAmount;
	}

	/**
	 * @return the page
	 */
	public Integer getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(Integer page) {
		this.page = page;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 
	 */
	public TransHistoryRequestDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param accountNo
	 * @param transactionType
	 * @param fromDate
	 * @param toDate
	 * @param fromAmount
	 * @param toAmount
	 * @param page
	 * @param pageSize
	 */
	public TransHistoryRequestDTO(String accountNo, String transactionType, Date fromDate, Date toDate,
			Double fromAmount, Double toAmount, Integer page, Integer pageSize) {
		super();
		this.accountNo = accountNo;
		// this.transactionType = transactionType;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.fromAmount = fromAmount;
		this.toAmount = toAmount;
		this.page = page;
		this.pageSize = pageSize;
	}

}
