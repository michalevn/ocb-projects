/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum OperationStatus {
	EXECUTED("EXECUTED"), INTERNAL_ERROR("INTERNAL_ERROR"), IN_PROCESSING("IN_PROCESSING"),
	REJECTED("REJECTED");
	
	
	/**
	 * 
	 */
	OperationStatus(String operationStatus) {
		// TODO Auto-generated constructor stub
		this.operationStatusCode = operationStatus;
	}

	private final String operationStatusCode;

	/**
	 * @return the operationStatusCode
	 */
	public String getOperationStatusCode() {
		return operationStatusCode;
	}

	public static void validOperationStatus(String operationStatus) {
		boolean check = false;
		for (OperationStatus configType : OperationStatus.values()) {
			if (configType.getOperationStatusCode().equals(operationStatus)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.OPERATIONSTATUS_INVALID,
					"operationStatus type has to be in values :  EXECUTED/INTERNAL_ERROR/IN_PROCESSING/REJECTED" + 
					"");
		}
	}

}
