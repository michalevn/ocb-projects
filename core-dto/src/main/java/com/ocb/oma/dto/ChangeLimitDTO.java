package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ocb.oma.validation.Digits;

public class ChangeLimitDTO implements Serializable {

	private static final long serialVersionUID = -8647095096898257379L;

	private String channelType;
	@Digits
	private BigDecimal value;

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
