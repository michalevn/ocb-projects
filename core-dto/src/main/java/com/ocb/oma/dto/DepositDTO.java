/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class DepositDTO implements Serializable {

	private String depositId;
	private String depositName;
	private String description;
	private String accountNo;
	private String currency;
	private Double depositBalance;
	private Date nextCapitalizationDate;
	private Date maturityDate;
	private String openingAccountNo;
	private String settlementAccountNo;
	private String settlementAccountIntNo;
	private String renewalOptionType;
	private String depositPeriodType;
	private Long period;
	private Double interest;
	private String interestRateType;
	private Double totalInterestAmount;
	private String depositMigrationName;
	private String depositMigrationContractNumber;
	private Boolean autoRolloverChangePos;
	private String interestPaymentMethod;
	private long category;
	private String subProductCode;
	private Date openDate;
	private String productType;
	private boolean isSurcharge;
	private Date nextSurchargeDate;
	private Double nextSurchargeAmount;
	private Double lastSurchargeAmount;
	private List<OwnersDTO> ownersList;
	private boolean unfinishedDisposition;
	private boolean typeCapableToBreak;
	private String lastTransactionBookingDate;
	private String depositContractNumber;
	private String customName;
	private String customIcon;
	private String instructionOnDispensation;
	private String periodFrequency;
	private Date dateFirstExecution;
	private Double workingAvailableFunds;
	private Double prematureInterestRate;
	private boolean ibRedeemDepositPos;
	private String status;
	private String depositType;
	private boolean surcharge;

	

	public DepositDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param depositId
	 * @param depositName
	 * @param description
	 * @param accountNo
	 * @param currency
	 * @param depositBalance
	 * @param nextCapitalizationDate
	 * @param maturityDate
	 * @param openingAccountNo
	 * @param settlementAccountNo
	 * @param settlementAccountIntNo
	 * @param renewalOptionType
	 * @param depositPeriodType
	 * @param period
	 * @param interest
	 * @param interestRateType
	 * @param totalInterestAmount
	 * @param depositMigrationName
	 * @param depositMigrationContractNumber
	 * @param autoRolloverChangePos
	 * @param interestPaymentMethod
	 * @param category
	 * @param subProductCode
	 * @param openDate
	 * @param productType
	 * @param isSurcharge
	 * @param nextSurchargeDate
	 * @param nextSurchargeAmount
	 * @param lastSurchargeAmount
	 * @param ownersList
	 * @param unfinishedDisposition
	 * @param typeCapableToBreak
	 * @param lastTransactionBookingDate
	 * @param depositContractNumber
	 * @param customName
	 * @param customIcon
	 * @param instructionOnDispensation
	 * @param periodFrequency
	 * @param dateFirstExecution
	 * @param workingAvailableFunds
	 * @param prematureInterestRate
	 * @param ibRedeemDepositPos
	 * @param status
	 * @param depositType
	 * @param surcharge
	 */
	public DepositDTO(String depositId, String depositName, String description, String accountNo, String currency,
			Double depositBalance, Date nextCapitalizationDate, Date maturityDate, String openingAccountNo,
			String settlementAccountNo, String settlementAccountIntNo, String renewalOptionType,
			String depositPeriodType, Long period, Double interest, String interestRateType, Double totalInterestAmount,
			String depositMigrationName, String depositMigrationContractNumber, Boolean autoRolloverChangePos,
			String interestPaymentMethod, long category, String subProductCode, Date openDate, String productType,
			boolean isSurcharge, Date nextSurchargeDate, Double nextSurchargeAmount, Double lastSurchargeAmount,
			List<OwnersDTO> ownersList, boolean unfinishedDisposition, boolean typeCapableToBreak,
			String lastTransactionBookingDate, String depositContractNumber, String customName, String customIcon,
			String instructionOnDispensation, String periodFrequency, Date dateFirstExecution,
			Double workingAvailableFunds, Double prematureInterestRate, boolean ibRedeemDepositPos, String status,
			String depositType, boolean surcharge) {
		super();
		this.depositId = depositId;
		this.depositName = depositName;
		this.description = description;
		this.accountNo = accountNo;
		this.currency = currency;
		this.depositBalance = depositBalance;
		this.nextCapitalizationDate = nextCapitalizationDate;
		this.maturityDate = maturityDate;
		this.openingAccountNo = openingAccountNo;
		this.settlementAccountNo = settlementAccountNo;
		this.settlementAccountIntNo = settlementAccountIntNo;
		this.renewalOptionType = renewalOptionType;
		this.depositPeriodType = depositPeriodType;
		this.period = period;
		this.interest = interest;
		this.interestRateType = interestRateType;
		this.totalInterestAmount = totalInterestAmount;
		this.depositMigrationName = depositMigrationName;
		this.depositMigrationContractNumber = depositMigrationContractNumber;
		this.autoRolloverChangePos = autoRolloverChangePos;
		this.interestPaymentMethod = interestPaymentMethod;
		this.category = category;
		this.subProductCode = subProductCode;
		this.openDate = openDate;
		this.productType = productType;
		this.isSurcharge = isSurcharge;
		this.nextSurchargeDate = nextSurchargeDate;
		this.nextSurchargeAmount = nextSurchargeAmount;
		this.lastSurchargeAmount = lastSurchargeAmount;
		this.ownersList = ownersList;
		this.unfinishedDisposition = unfinishedDisposition;
		this.typeCapableToBreak = typeCapableToBreak;
		this.lastTransactionBookingDate = lastTransactionBookingDate;
		this.depositContractNumber = depositContractNumber;
		this.customName = customName;
		this.customIcon = customIcon;
		this.instructionOnDispensation = instructionOnDispensation;
		this.periodFrequency = periodFrequency;
		this.dateFirstExecution = dateFirstExecution;
		this.workingAvailableFunds = workingAvailableFunds;
		this.prematureInterestRate = prematureInterestRate;
		this.ibRedeemDepositPos = ibRedeemDepositPos;
		this.status = status;
		this.depositType = depositType;
		this.surcharge = surcharge;
	}






	/**
	 * @return the depositId
	 */
	public String getDepositId() {
		return depositId;
	}



	/**
	 * @param depositId the depositId to set
	 */
	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}



	/**
	 * @return the depositName
	 */
	public String getDepositName() {
		return depositName;
	}



	/**
	 * @param depositName the depositName to set
	 */
	public void setDepositName(String depositName) {
		this.depositName = depositName;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}



	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}



	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}



	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}



	/**
	 * @return the depositBalance
	 */
	public Double getDepositBalance() {
		return depositBalance;
	}



	/**
	 * @param depositBalance the depositBalance to set
	 */
	public void setDepositBalance(Double depositBalance) {
		this.depositBalance = depositBalance;
	}



	/**
	 * @return the nextCapitalizationDate
	 */
	public Date getNextCapitalizationDate() {
		return nextCapitalizationDate;
	}



	/**
	 * @param nextCapitalizationDate the nextCapitalizationDate to set
	 */
	public void setNextCapitalizationDate(Date nextCapitalizationDate) {
		this.nextCapitalizationDate = nextCapitalizationDate;
	}



	/**
	 * @return the maturityDate
	 */
	public Date getMaturityDate() {
		return maturityDate;
	}



	/**
	 * @param maturityDate the maturityDate to set
	 */
	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}



	/**
	 * @return the openingAccountNo
	 */
	public String getOpeningAccountNo() {
		return openingAccountNo;
	}



	/**
	 * @param openingAccountNo the openingAccountNo to set
	 */
	public void setOpeningAccountNo(String openingAccountNo) {
		this.openingAccountNo = openingAccountNo;
	}



	/**
	 * @return the settlementAccountNo
	 */
	public String getSettlementAccountNo() {
		return settlementAccountNo;
	}



	/**
	 * @param settlementAccountNo the settlementAccountNo to set
	 */
	public void setSettlementAccountNo(String settlementAccountNo) {
		this.settlementAccountNo = settlementAccountNo;
	}



	/**
	 * @return the settlementAccountIntNo
	 */
	public String getSettlementAccountIntNo() {
		return settlementAccountIntNo;
	}



	/**
	 * @param settlementAccountIntNo the settlementAccountIntNo to set
	 */
	public void setSettlementAccountIntNo(String settlementAccountIntNo) {
		this.settlementAccountIntNo = settlementAccountIntNo;
	}



	/**
	 * @return the renewalOptionType
	 */
	public String getRenewalOptionType() {
		return renewalOptionType;
	}



	/**
	 * @param renewalOptionType the renewalOptionType to set
	 */
	public void setRenewalOptionType(String renewalOptionType) {
		this.renewalOptionType = renewalOptionType;
	}



	/**
	 * @return the depositPeriodType
	 */
	public String getDepositPeriodType() {
		return depositPeriodType;
	}



	/**
	 * @param depositPeriodType the depositPeriodType to set
	 */
	public void setDepositPeriodType(String depositPeriodType) {
		this.depositPeriodType = depositPeriodType;
	}



	/**
	 * @return the period
	 */
	public Long getPeriod() {
		return period;
	}



	/**
	 * @param period the period to set
	 */
	public void setPeriod(Long period) {
		this.period = period;
	}



	/**
	 * @return the interest
	 */
	public Double getInterest() {
		return interest;
	}



	/**
	 * @param interest the interest to set
	 */
	public void setInterest(Double interest) {
		this.interest = interest;
	}



	/**
	 * @return the interestRateType
	 */
	public String getInterestRateType() {
		return interestRateType;
	}



	/**
	 * @param interestRateType the interestRateType to set
	 */
	public void setInterestRateType(String interestRateType) {
		this.interestRateType = interestRateType;
	}



	/**
	 * @return the totalInterestAmount
	 */
	public Double getTotalInterestAmount() {
		return totalInterestAmount;
	}



	/**
	 * @param totalInterestAmount the totalInterestAmount to set
	 */
	public void setTotalInterestAmount(Double totalInterestAmount) {
		this.totalInterestAmount = totalInterestAmount;
	}



	/**
	 * @return the depositMigrationName
	 */
	public String getDepositMigrationName() {
		return depositMigrationName;
	}



	/**
	 * @param depositMigrationName the depositMigrationName to set
	 */
	public void setDepositMigrationName(String depositMigrationName) {
		this.depositMigrationName = depositMigrationName;
	}



	/**
	 * @return the depositMigrationContractNumber
	 */
	public String getDepositMigrationContractNumber() {
		return depositMigrationContractNumber;
	}



	/**
	 * @param depositMigrationContractNumber the depositMigrationContractNumber to set
	 */
	public void setDepositMigrationContractNumber(String depositMigrationContractNumber) {
		this.depositMigrationContractNumber = depositMigrationContractNumber;
	}



	/**
	 * @return the autoRolloverChangePos
	 */
	public Boolean getAutoRolloverChangePos() {
		return autoRolloverChangePos;
	}



	/**
	 * @param autoRolloverChangePos the autoRolloverChangePos to set
	 */
	public void setAutoRolloverChangePos(Boolean autoRolloverChangePos) {
		this.autoRolloverChangePos = autoRolloverChangePos;
	}



	/**
	 * @return the interestPaymentMethod
	 */
	public String getInterestPaymentMethod() {
		return interestPaymentMethod;
	}



	/**
	 * @param interestPaymentMethod the interestPaymentMethod to set
	 */
	public void setInterestPaymentMethod(String interestPaymentMethod) {
		this.interestPaymentMethod = interestPaymentMethod;
	}



	/**
	 * @return the category
	 */
	public long getCategory() {
		return category;
	}



	/**
	 * @param category the category to set
	 */
	public void setCategory(long category) {
		this.category = category;
	}



	/**
	 * @return the subProductCode
	 */
	public String getSubProductCode() {
		return subProductCode;
	}



	/**
	 * @param subProductCode the subProductCode to set
	 */
	public void setSubProductCode(String subProductCode) {
		this.subProductCode = subProductCode;
	}



	/**
	 * @return the openDate
	 */
	public Date getOpenDate() {
		return openDate;
	}



	/**
	 * @param openDate the openDate to set
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}



	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}



	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}



	/**
	 * @return the isSurcharge
	 */
	public boolean isSurcharge() {
		return isSurcharge;
	}



	/**
	 * @param isSurcharge the isSurcharge to set
	 */
	public void setIsSurcharge(boolean isSurcharge) {
		this.isSurcharge = isSurcharge;
	}



	/**
	 * @return the nextSurchargeDate
	 */
	public Date getNextSurchargeDate() {
		return nextSurchargeDate;
	}



	/**
	 * @param nextSurchargeDate the nextSurchargeDate to set
	 */
	public void setNextSurchargeDate(Date nextSurchargeDate) {
		this.nextSurchargeDate = nextSurchargeDate;
	}



	/**
	 * @return the nextSurchargeAmount
	 */
	public Double getNextSurchargeAmount() {
		return nextSurchargeAmount;
	}



	/**
	 * @param nextSurchargeAmount the nextSurchargeAmount to set
	 */
	public void setNextSurchargeAmount(Double nextSurchargeAmount) {
		this.nextSurchargeAmount = nextSurchargeAmount;
	}



	/**
	 * @return the lastSurchargeAmount
	 */
	public Double getLastSurchargeAmount() {
		return lastSurchargeAmount;
	}



	/**
	 * @param lastSurchargeAmount the lastSurchargeAmount to set
	 */
	public void setLastSurchargeAmount(Double lastSurchargeAmount) {
		this.lastSurchargeAmount = lastSurchargeAmount;
	}



	/**
	 * @return the ownersList
	 */
	public List<OwnersDTO> getOwnersList() {
		return ownersList;
	}



	/**
	 * @param ownersList the ownersList to set
	 */
	public void setOwnersList(List<OwnersDTO> ownersList) {
		this.ownersList = ownersList;
	}



	/**
	 * @return the unfinishedDisposition
	 */
	public boolean isUnfinishedDisposition() {
		return unfinishedDisposition;
	}



	/**
	 * @param unfinishedDisposition the unfinishedDisposition to set
	 */
	public void setUnfinishedDisposition(boolean unfinishedDisposition) {
		this.unfinishedDisposition = unfinishedDisposition;
	}



	/**
	 * @return the typeCapableToBreak
	 */
	public boolean isTypeCapableToBreak() {
		return typeCapableToBreak;
	}



	/**
	 * @param typeCapableToBreak the typeCapableToBreak to set
	 */
	public void setTypeCapableToBreak(boolean typeCapableToBreak) {
		this.typeCapableToBreak = typeCapableToBreak;
	}



	/**
	 * @return the lastTransactionBookingDate
	 */
	public String getLastTransactionBookingDate() {
		return lastTransactionBookingDate;
	}



	/**
	 * @param lastTransactionBookingDate the lastTransactionBookingDate to set
	 */
	public void setLastTransactionBookingDate(String lastTransactionBookingDate) {
		this.lastTransactionBookingDate = lastTransactionBookingDate;
	}



	/**
	 * @return the depositContractNumber
	 */
	public String getDepositContractNumber() {
		return depositContractNumber;
	}



	/**
	 * @param depositContractNumber the depositContractNumber to set
	 */
	public void setDepositContractNumber(String depositContractNumber) {
		this.depositContractNumber = depositContractNumber;
	}



	/**
	 * @return the customName
	 */
	public String getCustomName() {
		return customName;
	}



	/**
	 * @param customName the customName to set
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}



	/**
	 * @return the customIcon
	 */
	public String getCustomIcon() {
		return customIcon;
	}



	/**
	 * @param customIcon the customIcon to set
	 */
	public void setCustomIcon(String customIcon) {
		this.customIcon = customIcon;
	}



	/**
	 * @return the instructionOnDispensation
	 */
	public String getInstructionOnDispensation() {
		return instructionOnDispensation;
	}



	/**
	 * @param instructionOnDispensation the instructionOnDispensation to set
	 */
	public void setInstructionOnDispensation(String instructionOnDispensation) {
		this.instructionOnDispensation = instructionOnDispensation;
	}



	/**
	 * @return the periodFrequency
	 */
	public String getPeriodFrequency() {
		return periodFrequency;
	}



	/**
	 * @param periodFrequency the periodFrequency to set
	 */
	public void setPeriodFrequency(String periodFrequency) {
		this.periodFrequency = periodFrequency;
	}



	/**
	 * @return the dateFirstExecution
	 */
	public Date getDateFirstExecution() {
		return dateFirstExecution;
	}



	/**
	 * @param dateFirstExecution the dateFirstExecution to set
	 */
	public void setDateFirstExecution(Date dateFirstExecution) {
		this.dateFirstExecution = dateFirstExecution;
	}



	/**
	 * @return the workingAvailableFunds
	 */
	public Double getWorkingAvailableFunds() {
		return workingAvailableFunds;
	}



	/**
	 * @param workingAvailableFunds the workingAvailableFunds to set
	 */
	public void setWorkingAvailableFunds(Double workingAvailableFunds) {
		this.workingAvailableFunds = workingAvailableFunds;
	}



	/**
	 * @return the prematureInterestRate
	 */
	public Double getPrematureInterestRate() {
		return prematureInterestRate;
	}



	/**
	 * @param prematureInterestRate the prematureInterestRate to set
	 */
	public void setPrematureInterestRate(Double prematureInterestRate) {
		this.prematureInterestRate = prematureInterestRate;
	}



	/**
	 * @return the ibRedeemDepositPos
	 */
	public boolean isIbRedeemDepositPos() {
		return ibRedeemDepositPos;
	}



	/**
	 * @param ibRedeemDepositPos the ibRedeemDepositPos to set
	 */
	public void setIbRedeemDepositPos(boolean ibRedeemDepositPos) {
		this.ibRedeemDepositPos = ibRedeemDepositPos;
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}



	/**
	 * @return the depositType
	 */
	public String getDepositType() {
		return depositType;
	}



	/**
	 * @param depositType the depositType to set
	 */
	public void setDepositType(String depositType) {
		this.depositType = depositType;
	}



	/**
	 * @param surcharge the surcharge to set
	 */
	public void setSurcharge(boolean surcharge) {
		this.surcharge = surcharge;
	}
	

}
