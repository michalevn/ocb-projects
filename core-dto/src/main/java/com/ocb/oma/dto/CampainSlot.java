/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */

public class CampainSlot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7332464570478723412L;
	private String miniApplication;
	private String slotName;
	private String imageLink;

	/**
	 * 
	 */
	public CampainSlot() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @param miniApplication
	 * @param slotName
	 * @param imageLink
	 */
	public CampainSlot(String miniApplication, String slotName, String imageLink) {
		super();
		this.miniApplication = miniApplication;
		this.slotName = slotName;
		this.imageLink = imageLink;
	}


	/**
	 * @return the miniApplication
	 */
	public String getMiniApplication() {
		return miniApplication;
	}

	/**
	 * @param miniApplication the miniApplication to set
	 */
	public void setMiniApplication(String miniApplication) {
		this.miniApplication = miniApplication;
	}

	/**
	 * @return the slotName
	 */
	public String getSlotName() {
		return slotName;
	}

	/**
	 * @param slotName the slotName to set
	 */
	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}

	/**
	 * @return the imageLink
	 */
	public String getImageLink() {
		return imageLink;
	}

	/**
	 * @param imageLink the imageLink to set
	 */
	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

}