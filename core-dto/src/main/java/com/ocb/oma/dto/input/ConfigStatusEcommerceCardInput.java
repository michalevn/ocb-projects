package com.ocb.oma.dto.input;

public class ConfigStatusEcommerceCardInput {
	private String cardNumber;
	private String type;
	

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}



	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}



	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}



	public ConfigStatusEcommerceCardInput() {
		// TODO Auto-generated constructor stub
	}

}
