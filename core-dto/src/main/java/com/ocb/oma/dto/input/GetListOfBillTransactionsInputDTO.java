/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.validation.DateRange;

/**
 * @author phuhoang
 *
 */
@DateRange(from = "startDate", to = "endDate", message = "startDate.must.be.less.than.or.equal.to.endDate")
public class GetListOfBillTransactionsInputDTO implements Serializable {
	private Date startDate;
	private Date endDate;

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 */
	public GetListOfBillTransactionsInputDTO() {
		// TODO Auto-generated constructor stub
	}

	public GetListOfBillTransactionsInputDTO(Date startDate, Date endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
}
