/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class CampaignInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7394459566654865147L;
	private String name;
	private Date fromDate;
	private Date toDate;
	private Integer priority;
	private String link;
	private String description;
	private String bannerText;
	private Boolean activated;
	private List<CampainSlot> slots;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the bannerText
	 */
	public String getBannerText() {
		return bannerText;
	}

	/**
	 * @param bannerText the bannerText to set
	 */
	public void setBannerText(String bannerText) {
		this.bannerText = bannerText;
	}

	/**
	 * @return the activated
	 */
	public Boolean getActivated() {
		return activated;
	}

	/**
	 * @param activated the activated to set
	 */
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	/**
	 * @return the slots
	 */
	public List<CampainSlot> getSlots() {
		return slots;
	}

	/**
	 * @param slots the slots to set
	 */
	public void setSlots(List<CampainSlot> slots) {
		this.slots = slots;
	}

}
