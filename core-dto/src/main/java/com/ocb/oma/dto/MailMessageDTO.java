package com.ocb.oma.dto;

public class MailMessageDTO {
	
	private String subject;
	private String from;
	private String to;
	private String repllyTo;
	private String contents;
	
	
	

	/**
	 * @return the repllyTo
	 */
	public String getRepllyTo() {
		return repllyTo;
	}



	/**
	 * @param repllyTo the repllyTo to set
	 */
	public void setRepllyTo(String repllyTo) {
		this.repllyTo = repllyTo;
	}



	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}



	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}



	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}



	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}



	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}



	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}



	/**
	 * @return the contents
	 */
	public String getContents() {
		return contents;
	}



	/**
	 * @param contents the contents to set
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}



	public MailMessageDTO() {
	}

}
