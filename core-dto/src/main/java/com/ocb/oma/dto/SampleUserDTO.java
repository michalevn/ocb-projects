package com.ocb.oma.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

import com.ocb.oma.validation.CardTypeCode;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Money;
import com.ocb.oma.validation.OcbCurrentAccountNo;
import com.ocb.oma.validation.PeriodUnit;
import com.ocb.oma.validation.PhoneNumber;
import com.ocb.oma.validation.VND;

@DateRange(from = "dateFrom", to = "dateTo", message = "dateFrom.less.than.or.equal.to.dateTo")
@PeriodUnit
public class SampleUserDTO implements Serializable {

	private static final long serialVersionUID = 1906086989689692154L;

	@NotBlank(message = "user.name.required")
	@Size(min = 8, max = 16, message = "user.name.size.invalid")
	private String name;

	@Email(message = "email.invalid")
	private String email;

	@Min(value = 16, message = "user.age.min.invalid")
	@Max(value = 61, message = "user.age.max.invalid")
	private Integer age;

	@NotBlank(message = "user.phone.required")
	@PhoneNumber
	private String phone;

	@NotBlank(message = "user.accountNumber.required")
	@OcbCurrentAccountNo(message = "user.accountNumber.invalid")
	private String accountNumber;

	@VND
	private String currency;

	@Valid
	private SampleUserGroupDTO userGroup;

	@PastOrPresent(message = "dateFrom.must.be.a.past.date")
	private Date dateFrom;

	@PastOrPresent(message = "dateTo.must.be.a.past.date")
	private Date dateTo;

	@Money(message = "salary.invalid")
	private BigDecimal salary;

	@CardTypeCode
	private String cardTypeCode;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setUserGroup(SampleUserGroupDTO userGroup) {
		this.userGroup = userGroup;
	}

	public SampleUserGroupDTO getUserGroup() {
		return userGroup;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrency() {
		return currency;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setCardTypeCode(String cardTypeCode) {
		this.cardTypeCode = cardTypeCode;
	}

	public String getCardTypeCode() {
		return cardTypeCode;
	}
}
