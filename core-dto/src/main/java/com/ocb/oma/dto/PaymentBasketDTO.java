/**
 * 
 */
package com.ocb.oma.dto;

import java.util.List;

/**
 * @author phuhoang
 *
 */
public class PaymentBasketDTO {
	
	private String accountId;
	private List<BasketTransferDTO> basketTransfers;
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the basketTransfers
	 */
	public List<BasketTransferDTO> getBasketTransfers() {
		return basketTransfers;
	}
	/**
	 * @param basketTransfers the basketTransfers to set
	 */
	public void setBasketTransfers(List<BasketTransferDTO> basketTransfers) {
		this.basketTransfers = basketTransfers;
	}
	public PaymentBasketDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
