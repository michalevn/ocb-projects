package com.ocb.oma.dto.input;

import java.io.Serializable;

public class VetifyUserDeviceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String authId;
	private String otpValue;
	private String deviceId;
	private String otpMethod;

	public String getAuthId() {
		return authId;
	}

	public void setAuthId(String authId) {
		this.authId = authId;
	}

	public String getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOtpMethod() {
		return otpMethod;
	}

	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
