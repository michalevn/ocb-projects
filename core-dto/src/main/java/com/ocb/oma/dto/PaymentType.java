/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum PaymentType {

	InternalPayment("InternalPayment"), LocalPayment("LocalPayment"), FastTransfer("FastTransfer"),
	TuitionFee("TuitionFee"), BillPayment("BillPayment"), eWallet("eWallet"), TOPUPPayment("TOPUPPayment"),
	BillCardPayment("BillCardPayment"), QRCodePayment("QRCodePayment");
	/**
	 * 
	 */
	PaymentType(String paymentType) {
		// TODO Auto-generated constructor stub
		this.paymentTypeCode = paymentType;
	}

	private final String paymentTypeCode;

	/**
	 * @return the paymentTypeCode
	 */
	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	public static void validPaymentType(String paymentType) {
		boolean check = false;
		for (PaymentType configType : PaymentType.values()) {
			if (configType.getPaymentTypeCode().equals(paymentType)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.PAYMENT_TYPE_INVALID,
					"payment type has to be on of these values :  InternalPayment,LocalPayment,FastTransfer,TuitionFee,BillPayment, eWallet,TOPUPPayment,BillCardPayment");
		}
	}

}
