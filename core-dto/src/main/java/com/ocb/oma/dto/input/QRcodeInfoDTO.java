package com.ocb.oma.dto.input;

import java.util.List;

public class QRcodeInfoDTO {
	private String payType;
	private String voucherCode;
	private String mobile;
	private Long debitAmount;
	private List<QRcodeInfoLstItems> lstItems;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the payType
	 */
	public String getPayType() {
		return payType;
	}

	/**
	 * @param payType the payType to set
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}

	/**
	 * @return the voucherCode
	 */
	public String getVoucherCode() {
		return voucherCode;
	}

	/**
	 * @param voucherCode the voucherCode to set
	 */
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	/**
	 * @return the debitAmount
	 */
	public Long getDebitAmount() {
		return debitAmount;
	}

	/**
	 * @param debitAmount the debitAmount to set
	 */
	public void setDebitAmount(Long debitAmount) {
		this.debitAmount = debitAmount;
	}

	/**
	 * @return the lstItems
	 */
	public List<QRcodeInfoLstItems> getLstItems() {
		return lstItems;
	}

	/**
	 * @param lstItems the lstItems to set
	 */
	public void setLstItems(List<QRcodeInfoLstItems> lstItems) {
		this.lstItems = lstItems;
	}

	public QRcodeInfoDTO() {
		// TODO Auto-generated constructor stub
	}

}
