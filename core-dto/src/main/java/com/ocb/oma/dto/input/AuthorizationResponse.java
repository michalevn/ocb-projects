/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class AuthorizationResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8594652350557375779L;
	public static final String EXECUTED = "EXECUTED";
	public static final String IN_PROCESSING = "IN_PROCESSING";
	public static final String REJECTED = "REJECTED";
	public static final String INTERNAL_ERROR = "INTERNAL_ERROR";
	public static final String CORRECT = "CORRECT";
	public static final String INCORRECT = "INCORRECT";

	private String operationStatus;
	private String authId;
	private Date operationDate;
	private Long expiryTime;

	/**
	 * @return the operationStatus
	 */
	public String getOperationStatus() {
		return operationStatus;
	}

	/**
	 * @param operationStatus the operationStatus to set
	 */
	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}

	/**
	 * @return the authId
	 */
	public String getAuthId() {
		return authId;
	}

	/**
	 * @param authId the authId to set
	 */
	public void setAuthId(String authId) {
		this.authId = authId;
	}

	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * @return the expiryTime
	 */
	public Long getExpiryTime() {
		return expiryTime;
	}

	/**
	 * @param expiryTime the expiryTime to set
	 */
	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

}
