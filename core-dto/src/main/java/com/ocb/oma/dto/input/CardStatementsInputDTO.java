/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

import com.ocb.oma.validation.Digits;

/**
 * @author phuhoang
 *
 */
public class CardStatementsInputDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8039110909754097228L;
	@Digits
	private int pageNumber;
	@Digits
	private int pageSize;
	private String productId;

	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CardStatementsInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
