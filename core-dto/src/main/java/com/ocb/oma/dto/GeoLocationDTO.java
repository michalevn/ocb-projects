package com.ocb.oma.dto;

import java.io.Serializable;

public class GeoLocationDTO implements Serializable {

	private static final long serialVersionUID = -8514887327962119767L;

	private String id;
	private String placeType;
	private String name;
	private AddressDTO address;
	private GpsPositionDTO gpsPosition;
	private Double distance;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlaceType() {
		return placeType;
	}

	public void setPlaceType(String placeType) {
		this.placeType = placeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public GpsPositionDTO getGpsPosition() {
		return gpsPosition;
	}

	public void setGpsPosition(GpsPositionDTO gpsPosition) {
		this.gpsPosition = gpsPosition;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

}
