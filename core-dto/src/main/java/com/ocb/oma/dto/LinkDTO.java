/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

import com.ocb.oma.resources.model.OmniLink;

/**
 * @author docv
 *
 */
public class LinkDTO implements Serializable {

	private static final long serialVersionUID = -233040325634307274L;

	private Long id;
	private String imageUrl;
	private String titleVn;
	private String titleEl;
	private String descriptionVn;
	private String descriptionEl;
	private String href;
	private String target;
	private Integer displayOrder;
	private String group;
	private Boolean active;

	public LinkDTO() {
	}

	public LinkDTO(OmniLink omniLink) {
		id = omniLink.getId();
		imageUrl = omniLink.getImageUrl();
		titleVn = omniLink.getTitleVn();
		titleEl = omniLink.getTitleEl();
		descriptionVn = omniLink.getDescriptionVn();
		descriptionEl = omniLink.getDescriptionEl();
		href = omniLink.getHref();
		target = omniLink.getTarget();
		displayOrder = omniLink.getDisplayOrder();
		group = omniLink.getGroup();
		active = omniLink.getActive();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitleVn() {
		return titleVn;
	}

	public void setTitleVn(String titleVn) {
		this.titleVn = titleVn;
	}

	public String getTitleEl() {
		return titleEl;
	}

	public void setTitleEl(String titleEl) {
		this.titleEl = titleEl;
	}

	public String getDescriptionVn() {
		return descriptionVn;
	}

	public void setDescriptionVn(String descriptionVn) {
		this.descriptionVn = descriptionVn;
	}

	public String getDescriptionEl() {
		return descriptionEl;
	}

	public void setDescriptionEl(String descriptionEl) {
		this.descriptionEl = descriptionEl;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
