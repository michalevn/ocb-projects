/**
 * 
 */
package com.ocb.oma.dto;

import com.ocb.oma.exception.OmaException;

/**
 * @author phuhoang
 *
 */
public enum StatusesAvatar {

	OK("OK"), INTERNAL_ERROR("INTERNAL_ERROR"), NO_AVATAR("NO_AVATAR");
	/**
	 * 
	 */
	StatusesAvatar(String statusesAvatar) {
		// TODO Auto-generated constructor stub
		this.statusesAvatarCode = statusesAvatar;
	}

	private final String statusesAvatarCode;

	/**
	 * @return the statusesAvatarCode
	 */
	public String getStatusesAvatarCode() {
		return statusesAvatarCode;
	}

	/**
	 * 
	 * @param statusesAvatarCode
	 */
	public static void validStatusesAvatar(String statusesAvatarCode) {
		boolean check = false;
		for (StatusesAvatar configType : StatusesAvatar.values()) {
			if (configType.getStatusesAvatarCode().equals(statusesAvatarCode)) {
				check = true;
				continue;
			}
		}
		if (check == false) {
			throw new OmaException(MessageConstant.STATUSES_TYPE_INVALID,
					"status Avatar has to be on of these values :  OK,INTERNAL_ERROR, NO_AVATAR");
		}
	}

}
