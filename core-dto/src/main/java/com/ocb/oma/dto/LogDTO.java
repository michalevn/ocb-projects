/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author phuhoang
 *
 */
public class LogDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6355817997308838970L;
	public static final String RESULT_SUCCESS = "success";
	public static final String RESULT_FAILED = "failed";

	private String username;
	private String methodName;
	private Map<String, Object> params;
	private Date timeStarted;
	private Date timeEnd;
	private Long duration;
	private String result;
	private String refId;

	/**
	 * @param params the params to set
	 */
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	/**
	 * @return the params
	 */
	public Map<String, Object> getParams() {
		return params;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the timeStarted
	 */
	public Date getTimeStarted() {
		return timeStarted;
	}

	/**
	 * @param timeStarted the timeStarted to set
	 */
	public void setTimeStarted(Date timeStarted) {
		this.timeStarted = timeStarted;
	}

	/**
	 * @return the timeEnd
	 */
	public Date getTimeEnd() {
		return timeEnd;
	}

	/**
	 * @param timeEnd the timeEnd to set
	 */
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	/**
	 * @return the duration
	 */
	public Long getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Long duration) {
		this.duration = duration;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getRefId() {
		return refId;
	}

	/**
	 * 
	 */
	public LogDTO() {
		result = RESULT_SUCCESS;
	}

}
