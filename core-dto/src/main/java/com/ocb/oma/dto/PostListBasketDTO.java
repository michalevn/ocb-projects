/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class PostListBasketDTO implements Serializable {

	private String resultCode;
	private String resultMsg;
	private List<String> messages;
	private Integer readyTransactions;
	private Integer notAcceptedTransactions;
	private Integer failedTransactions;
	private Boolean transactionsSubmited;

	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return the resultMsg
	 */
	public String getResultMsg() {
		return resultMsg;
	}

	/**
	 * @param resultMsg the resultMsg to set
	 */
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	/**
	 * @return the messages
	 */
	public List<String> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * @return the readyTransactions
	 */
	public Integer getReadyTransactions() {
		return readyTransactions;
	}

	/**
	 * @param readyTransactions the readyTransactions to set
	 */
	public void setReadyTransactions(Integer readyTransactions) {
		this.readyTransactions = readyTransactions;
	}

	/**
	 * @return the notAcceptedTransactions
	 */
	public Integer getNotAcceptedTransactions() {
		return notAcceptedTransactions;
	}

	/**
	 * @param notAcceptedTransactions the notAcceptedTransactions to set
	 */
	public void setNotAcceptedTransactions(Integer notAcceptedTransactions) {
		this.notAcceptedTransactions = notAcceptedTransactions;
	}

	/**
	 * @return the failedTransactions
	 */
	public Integer getFailedTransactions() {
		return failedTransactions;
	}

	/**
	 * @param failedTransactions the failedTransactions to set
	 */
	public void setFailedTransactions(Integer failedTransactions) {
		this.failedTransactions = failedTransactions;
	}

	/**
	 * @return the transactionsSubmited
	 */
	public Boolean getTransactionsSubmited() {
		return transactionsSubmited;
	}

	/**
	 * @param transactionsSubmited the transactionsSubmited to set
	 */
	public void setTransactionsSubmited(Boolean transactionsSubmited) {
		this.transactionsSubmited = transactionsSubmited;
	}

}
