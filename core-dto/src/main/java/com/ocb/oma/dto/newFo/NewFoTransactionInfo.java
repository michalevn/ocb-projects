package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoTransactionInfo implements Serializable {

	private static final long serialVersionUID = 4327248925086085175L;

	private String cRefNum;
	private String clientCode;
	private String pRefNum;
	private String transactionReturn;
	private String transactionReturnMsg;

	public String getcRefNum() {
		return cRefNum;
	}

	public void setcRefNum(String cRefNum) {
		this.cRefNum = cRefNum;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getpRefNum() {
		return pRefNum;
	}

	public void setpRefNum(String pRefNum) {
		this.pRefNum = pRefNum;
	}

	public String getTransactionReturn() {
		return transactionReturn;
	}

	public void setTransactionReturn(String transactionReturn) {
		this.transactionReturn = transactionReturn;
	}

	public String getTransactionReturnMsg() {
		return transactionReturnMsg;
	}

	public void setTransactionReturnMsg(String transactionReturnMsg) {
		this.transactionReturnMsg = transactionReturnMsg;
	}

}
