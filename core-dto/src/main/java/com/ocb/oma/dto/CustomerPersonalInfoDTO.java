package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerPersonalInfoDTO implements Serializable {

	private static final long serialVersionUID = -3499084621755865231L;

	private Date birthDate;
	private String birthPlace;
	private String businessPhoneNumber;
	private String context;
	private CorporateInfoDTO corporateInfo;
	private AddressDTO deliveryAddress;
	private String email;
	private String faxNumber;
	private String fullName;
	private AddressDTO homeAddress;
	private String homePhoneNumber;
	private Date issuedDate1;
	private Date issuedDate2;
	private Date issuedDate3;
	private String issuedPlace1;
	private String issuedPlace2;
	private String issuedPlace3;
	private String legalIdentification1;
	private String legalIdentification2;
	private String legalIdentification3;
	private String mobilePhoneNumber;
	private String smsOtpMobileNumber;
	private String username;

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getBusinessPhoneNumber() {
		return businessPhoneNumber;
	}

	public void setBusinessPhoneNumber(String businessPhoneNumber) {
		this.businessPhoneNumber = businessPhoneNumber;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public CorporateInfoDTO getCorporateInfo() {
		return corporateInfo;
	}

	public void setCorporateInfo(CorporateInfoDTO corporateInfo) {
		this.corporateInfo = corporateInfo;
	}

	public AddressDTO getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressDTO deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public AddressDTO getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(AddressDTO homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public Date getIssuedDate1() {
		return issuedDate1;
	}

	public void setIssuedDate1(Date issuedDate1) {
		this.issuedDate1 = issuedDate1;
	}

	public Date getIssuedDate2() {
		return issuedDate2;
	}

	public void setIssuedDate2(Date issuedDate2) {
		this.issuedDate2 = issuedDate2;
	}

	public Date getIssuedDate3() {
		return issuedDate3;
	}

	public void setIssuedDate3(Date issuedDate3) {
		this.issuedDate3 = issuedDate3;
	}

	public String getIssuedPlace1() {
		return issuedPlace1;
	}

	public void setIssuedPlace1(String issuedPlace1) {
		this.issuedPlace1 = issuedPlace1;
	}

	public String getIssuedPlace2() {
		return issuedPlace2;
	}

	public void setIssuedPlace2(String issuedPlace2) {
		this.issuedPlace2 = issuedPlace2;
	}

	public String getIssuedPlace3() {
		return issuedPlace3;
	}

	public void setIssuedPlace3(String issuedPlace3) {
		this.issuedPlace3 = issuedPlace3;
	}

	public String getLegalIdentification1() {
		return legalIdentification1;
	}

	public void setLegalIdentification1(String legalIdentification1) {
		this.legalIdentification1 = legalIdentification1;
	}

	public String getLegalIdentification2() {
		return legalIdentification2;
	}

	public void setLegalIdentification2(String legalIdentification2) {
		this.legalIdentification2 = legalIdentification2;
	}

	public String getLegalIdentification3() {
		return legalIdentification3;
	}

	public void setLegalIdentification3(String legalIdentification3) {
		this.legalIdentification3 = legalIdentification3;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getSmsOtpMobileNumber() {
		return smsOtpMobileNumber;
	}

	public void setSmsOtpMobileNumber(String smsOtpMobileNumber) {
		this.smsOtpMobileNumber = smsOtpMobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
