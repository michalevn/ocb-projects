/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class BreakDepositInputDTO implements Serializable {

	private static final long serialVersionUID = 2005397816892546577L;

	private String depositId;

	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the depositId
	 */
	public String getDepositId() {
		return depositId;
	}

	/**
	 * @param depositId the depositId to set
	 */
	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
