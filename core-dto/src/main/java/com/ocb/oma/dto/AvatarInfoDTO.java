/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class AvatarInfoDTO implements Serializable {
	
	private String fileName;
	private String data;
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	
}
