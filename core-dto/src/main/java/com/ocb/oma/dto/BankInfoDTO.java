/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class BankInfoDTO implements Serializable {

	private String bankCode;
	private String bankName;
	private String codeCitad;
	private String codeNapas;
	private String provinceCode;
	private String branchCode;
	private String branchName;
	private Boolean isInternal;

	/**
	 * 
	 */
	public BankInfoDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param bankCode
	 * @param bankName
	 * @param codeCitad
	 * @param codeNapas
	 * @param provinceCode
	 * @param branchCode
	 * @param branchName
	 * @param isInternal
	 */
	public BankInfoDTO(String bankCode, String bankName, String codeCitad, String codeNapas, String provinceCode,
			String branchCode, String branchName, Boolean isInternal) {
		super();
		this.bankCode = bankCode;
		this.bankName = bankName;
		this.codeCitad = codeCitad;
		this.codeNapas = codeNapas;
		this.provinceCode = provinceCode;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.isInternal = isInternal;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the codeCitad
	 */
	public String getCodeCitad() {
		return codeCitad;
	}

	/**
	 * @param codeCitad the codeCitad to set
	 */
	public void setCodeCitad(String codeCitad) {
		this.codeCitad = codeCitad;
	}

	/**
	 * @return the codeNapas
	 */
	public String getCodeNapas() {
		return codeNapas;
	}

	/**
	 * @param codeNapas the codeNapas to set
	 */
	public void setCodeNapas(String codeNapas) {
		this.codeNapas = codeNapas;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}

	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	/**
	 * @return the isInternal
	 */
	public Boolean getIsInternal() {
		return isInternal;
	}

	/**
	 * @param isInternal the isInternal to set
	 */
	public void setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
	}

}
