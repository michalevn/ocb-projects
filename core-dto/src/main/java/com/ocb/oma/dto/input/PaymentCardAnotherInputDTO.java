/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class PaymentCardAnotherInputDTO implements Serializable {

	private String accountId;
	private String accountName;
	private Double amount;
	private String currency;
	private String remark;
	private String creditCardAccount;
	private String creditCardAccountName;
	private String cardNumber;
	private VerifyOtpToken otpValue;
	private String otpMethod;
	
	

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the creditCardAccount
	 */
	public String getCreditCardAccount() {
		return creditCardAccount;
	}

	/**
	 * @param creditCardAccount the creditCardAccount to set
	 */
	public void setCreditCardAccount(String creditCardAccount) {
		this.creditCardAccount = creditCardAccount;
	}

	/**
	 * @return the creditCardAccountName
	 */
	public String getCreditCardAccountName() {
		return creditCardAccountName;
	}

	/**
	 * @param creditCardAccountName the creditCardAccountName to set
	 */
	public void setCreditCardAccountName(String creditCardAccountName) {
		this.creditCardAccountName = creditCardAccountName;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public PaymentCardAnotherInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
