package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CardTypeDTO implements Serializable {

	private static final long serialVersionUID = -7017449398741161694L;

	private String cardTypeCode;
	private String cardTypeName;
	private String cardTypeImage;
	private String cardTypeGroup;
	private List<Map<String, String>> subCards;

	public String getCardTypeCode() {
		return cardTypeCode;
	}

	public void setCardTypeCode(String cardTypeCode) {
		this.cardTypeCode = cardTypeCode;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public List<Map<String, String>> getSubCards() {
		return subCards;
	}

	public void setSubCards(List<Map<String, String>> subCards) {
		this.subCards = subCards;
	}
	
	public void setCardTypeImage(String cardTypeImage) {
		this.cardTypeImage = cardTypeImage;
	}
	
	public String getCardTypeImage() {
		return cardTypeImage;
	}
	
	public void setCardTypeGroup(String cardTypeGroup) {
		this.cardTypeGroup = cardTypeGroup;
	}
	
	public String getCardTypeGroup() {
		return cardTypeGroup;
	}
}
