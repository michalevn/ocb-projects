/**
 * 
 */
package com.ocb.oma.dto;

/**
 * @author phuhoang
 *
 */
public class UpcomingRepaymentDepositDTO {

	private UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo;
	private UpcomingRepaymentDepositSubDTO loanRepaymentInfo;
	private UpcomingRepaymentDepositSubDTO creditCardInfo;
	private Boolean creditCardWithoutStatement;

	/**
	 * @return the minMaturityDateDepositInfo
	 */
	public UpcomingRepaymentDepositSubDTO getMinMaturityDateDepositInfo() {
		return minMaturityDateDepositInfo;
	}

	/**
	 * @param minMaturityDateDepositInfo the minMaturityDateDepositInfo to set
	 */
	public void setMinMaturityDateDepositInfo(UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo) {
		this.minMaturityDateDepositInfo = minMaturityDateDepositInfo;
	}

	/**
	 * @return the loanRepaymentInfo
	 */
	public UpcomingRepaymentDepositSubDTO getLoanRepaymentInfo() {
		return loanRepaymentInfo;
	}

	/**
	 * @param loanRepaymentInfo the loanRepaymentInfo to set
	 */
	public void setLoanRepaymentInfo(UpcomingRepaymentDepositSubDTO loanRepaymentInfo) {
		this.loanRepaymentInfo = loanRepaymentInfo;
	}

	/**
	 * @return the creditCardInfo
	 */
	public UpcomingRepaymentDepositSubDTO getCreditCardInfo() {
		return creditCardInfo;
	}

	/**
	 * @param creditCardInfo the creditCardInfo to set
	 */
	public void setCreditCardInfo(UpcomingRepaymentDepositSubDTO creditCardInfo) {
		this.creditCardInfo = creditCardInfo;
	}

	/**
	 * @return the creditCardWithoutStatement
	 */
	public Boolean getCreditCardWithoutStatement() {
		return creditCardWithoutStatement;
	}

	/**
	 * @param creditCardWithoutStatement the creditCardWithoutStatement to set
	 */
	public void setCreditCardWithoutStatement(Boolean creditCardWithoutStatement) {
		this.creditCardWithoutStatement = creditCardWithoutStatement;
	}

	/**
	 * 
	 */
	public UpcomingRepaymentDepositDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param minMaturityDateDepositInfo
	 * @param loanRepaymentInfo
	 * @param creditCardInfo
	 * @param creditCardWithoutStatement
	 */
	public UpcomingRepaymentDepositDTO(UpcomingRepaymentDepositSubDTO minMaturityDateDepositInfo,
			UpcomingRepaymentDepositSubDTO loanRepaymentInfo, UpcomingRepaymentDepositSubDTO creditCardInfo,
			Boolean creditCardWithoutStatement) {
		super();
		this.minMaturityDateDepositInfo = minMaturityDateDepositInfo;
		this.loanRepaymentInfo = loanRepaymentInfo;
		this.creditCardInfo = creditCardInfo;
		this.creditCardWithoutStatement = creditCardWithoutStatement;
	}

}
