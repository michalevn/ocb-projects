/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class CardInfoDTO implements Serializable {
	
	private static final long serialVersionUID = -6899492874158242166L;
	
	private String id;
	private String name;
	private String cardNo;
	private Double availableFunds;
	private String accountId;
	private String accountNo;
	private String cardOwnerName;
	private String cardOwnerLastName;
	private String status;
	private String cardType;
	private String cardIDType;
	private String cardSubType;
	private String currency;
	private Double blockedFunds;
	private Date dateExpirationEnd;
	private Date settlmntDate;
	private Double limitLeft;

	private Date lastOperationDate;
	private Double balance;
	private Boolean currentUserOwner;
	private Boolean currentUserHolder;
	private Boolean resume;
	private String customerName;
	private String embossedName;
	private Double limitInCycle;
	private String embossedCompanyName;
	private String cardImage;
	private Double limitUsed;
	private String cardTypeNumber;
	private Double minimumRepaymentAmount;
	private Date repaymentDueDate;
	
	

	/**
	 * @return the cardIDType
	 */
	public String getCardIDType() {
		return cardIDType;
	}

	/**
	 * @param cardIDType the cardIDType to set
	 */
	public void setCardIDType(String cardIDType) {
		this.cardIDType = cardIDType;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cardNo
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * @param cardNo the cardNo to set
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/**
	 * @return the availableFunds
	 */
	public Double getAvailableFunds() {
		return availableFunds;
	}

	/**
	 * @param availableFunds the availableFunds to set
	 */
	public void setAvailableFunds(Double availableFunds) {
		this.availableFunds = availableFunds;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the cardOwnerName
	 */
	public String getCardOwnerName() {
		return cardOwnerName;
	}

	/**
	 * @param cardOwnerName the cardOwnerName to set
	 */
	public void setCardOwnerName(String cardOwnerName) {
		this.cardOwnerName = cardOwnerName;
	}

	/**
	 * @return the cardOwnerLastName
	 */
	public String getCardOwnerLastName() {
		return cardOwnerLastName;
	}

	/**
	 * @param cardOwnerLastName the cardOwnerLastName to set
	 */
	public void setCardOwnerLastName(String cardOwnerLastName) {
		this.cardOwnerLastName = cardOwnerLastName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the cardSubType
	 */
	public String getCardSubType() {
		return cardSubType;
	}

	/**
	 * @param cardSubType the cardSubType to set
	 */
	public void setCardSubType(String cardSubType) {
		this.cardSubType = cardSubType;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the blockedFunds
	 */
	public Double getBlockedFunds() {
		return blockedFunds;
	}

	/**
	 * @param blockedFunds the blockedFunds to set
	 */
	public void setBlockedFunds(Double blockedFunds) {
		this.blockedFunds = blockedFunds;
	}

	/**
	 * @return the dateExpirationEnd
	 */
	public Date getDateExpirationEnd() {
		return dateExpirationEnd;
	}

	/**
	 * @param dateExpirationEnd the dateExpirationEnd to set
	 */
	public void setDateExpirationEnd(Date dateExpirationEnd) {
		this.dateExpirationEnd = dateExpirationEnd;
	}

	/**
	 * @return the settlmntDate
	 */
	public Date getSettlmntDate() {
		return settlmntDate;
	}

	/**
	 * @param settlmntDate the settlmntDate to set
	 */
	public void setSettlmntDate(Date settlmntDate) {
		this.settlmntDate = settlmntDate;
	}

	/**
	 * @return the limitLeft
	 */
	public Double getLimitLeft() {
		return limitLeft;
	}

	/**
	 * @param limitLeft the limitLeft to set
	 */
	public void setLimitLeft(Double limitLeft) {
		this.limitLeft = limitLeft;
	}

	/**
	 * @return the lastOperationDate
	 */
	public Date getLastOperationDate() {
		return lastOperationDate;
	}

	/**
	 * @param lastOperationDate the lastOperationDate to set
	 */
	public void setLastOperationDate(Date lastOperationDate) {
		this.lastOperationDate = lastOperationDate;
	}

	/**
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	/**
	 * @return the currentUserOwner
	 */
	public Boolean getCurrentUserOwner() {
		return currentUserOwner;
	}

	/**
	 * @param currentUserOwner the currentUserOwner to set
	 */
	public void setCurrentUserOwner(Boolean currentUserOwner) {
		this.currentUserOwner = currentUserOwner;
	}

	/**
	 * @return the currentUserHolder
	 */
	public Boolean getCurrentUserHolder() {
		return currentUserHolder;
	}

	/**
	 * @param currentUserHolder the currentUserHolder to set
	 */
	public void setCurrentUserHolder(Boolean currentUserHolder) {
		this.currentUserHolder = currentUserHolder;
	}

	/**
	 * @return the resume
	 */
	public Boolean getResume() {
		return resume;
	}

	/**
	 * @param resume the resume to set
	 */
	public void setResume(Boolean resume) {
		this.resume = resume;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the embossedName
	 */
	public String getEmbossedName() {
		return embossedName;
	}

	/**
	 * @param embossedName the embossedName to set
	 */
	public void setEmbossedName(String embossedName) {
		this.embossedName = embossedName;
	}

	/**
	 * @return the limitInCycle
	 */
	public Double getLimitInCycle() {
		return limitInCycle;
	}

	/**
	 * @param limitInCycle the limitInCycle to set
	 */
	public void setLimitInCycle(Double limitInCycle) {
		this.limitInCycle = limitInCycle;
	}

	/**
	 * @return the embossedCompanyName
	 */
	public String getEmbossedCompanyName() {
		return embossedCompanyName;
	}

	/**
	 * @param embossedCompanyName the embossedCompanyName to set
	 */
	public void setEmbossedCompanyName(String embossedCompanyName) {
		this.embossedCompanyName = embossedCompanyName;
	}

	/**
	 * @return the cardImage
	 */
	public String getCardImage() {
		return cardImage;
	}

	/**
	 * @param cardImage the cardImage to set
	 */
	public void setCardImage(String cardImage) {
		this.cardImage = cardImage;
	}

	/**
	 * @return the limitUsed
	 */
	public Double getLimitUsed() {
		return limitUsed;
	}

	/**
	 * @param limitUsed the limitUsed to set
	 */
	public void setLimitUsed(Double limitUsed) {
		this.limitUsed = limitUsed;
	}

	/**
	 * @return the cardTypeNumber
	 */
	public String getCardTypeNumber() {
		return cardTypeNumber;
	}

	/**
	 * @param cardTypeNumber the cardTypeNumber to set
	 */
	public void setCardTypeNumber(String cardTypeNumber) {
		this.cardTypeNumber = cardTypeNumber;
	}

	/**
	 * @return the minimumRepaymentAmount
	 */
	public Double getMinimumRepaymentAmount() {
		return minimumRepaymentAmount;
	}

	/**
	 * @param minimumRepaymentAmount the minimumRepaymentAmount to set
	 */
	public void setMinimumRepaymentAmount(Double minimumRepaymentAmount) {
		this.minimumRepaymentAmount = minimumRepaymentAmount;
	}

	/**
	 * @return the repaymentDueDate
	 */
	public Date getRepaymentDueDate() {
		return repaymentDueDate;
	}

	/**
	 * @param repaymentDueDate the repaymentDueDate to set
	 */
	public void setRepaymentDueDate(Date repaymentDueDate) {
		this.repaymentDueDate = repaymentDueDate;
	}

	public CardInfoDTO() {
	}

	public CardInfoDTO(String id, String name, String cardNo, Double availableFunds, String accountId, String accountNo, String cardOwnerName,
			String cardOwnerLastName, String status, String cardType, String cardSubType, String currency, Double blockedFunds,
			Date dateExpirationEnd, Date settlmntDate, Double limitLeft, Date lastOperationDate, Double balance, Boolean currentUserOwner,
			Boolean currentUserHolder, Boolean resume, String customerName, String embossedName, Double limitInCycle, String embossedCompanyName,
			String cardImage, Double limitUsed, String cardTypeNumber, Double minimumRepaymentAmount, Date repaymentDueDate) {
		super();
		this.id = id;
		this.name = name;
		this.cardNo = cardNo;
		this.availableFunds = availableFunds;
		this.accountId = accountId;
		this.accountNo = accountNo;
		this.cardOwnerName = cardOwnerName;
		this.cardOwnerLastName = cardOwnerLastName;
		this.status = status;
		this.cardType = cardType;
		this.cardSubType = cardSubType;
		this.currency = currency;
		this.blockedFunds = blockedFunds;
		this.dateExpirationEnd = dateExpirationEnd;
		this.settlmntDate = settlmntDate;
		this.limitLeft = limitLeft;
		this.lastOperationDate = lastOperationDate;
		this.balance = balance;
		this.currentUserOwner = currentUserOwner;
		this.currentUserHolder = currentUserHolder;
		this.resume = resume;
		this.customerName = customerName;
		this.embossedName = embossedName;
		this.limitInCycle = limitInCycle;
		this.embossedCompanyName = embossedCompanyName;
		this.cardImage = cardImage;
		this.limitUsed = limitUsed;
		this.cardTypeNumber = cardTypeNumber;
		this.minimumRepaymentAmount = minimumRepaymentAmount;
		this.repaymentDueDate = repaymentDueDate;
	}

}
