/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;

import com.ocb.oma.validation.CIF;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.VND;

/**
 * @author phuhoang
 *
 */
public class GetChallengeCodeInputDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5427081815386846372L;
	@CIF
	private String cif;
	private String creditAccount;
	private String debitAccount;
	@Digits
	private Double amount;
	private String paymentType;
	@VND
	private String currency;
	private String bankCode;
	private String provinceCode;

	/**
	 * @return the cif
	 */
	public String getCif() {
		return cif;
	}

	/**
	 * @param cif the cif to set
	 */
	public void setCif(String cif) {
		this.cif = cif;
	}

	/**
	 * @return the creditAccount
	 */
	public String getCreditAccount() {
		return creditAccount;
	}

	/**
	 * @param creditAccount the creditAccount to set
	 */
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	/**
	 * @return the debitAccount
	 */
	public String getDebitAccount() {
		return debitAccount;
	}

	/**
	 * @param debitAccount the debitAccount to set
	 */
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	* 
	*/
	public GetChallengeCodeInputDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cif
	 * @param creditAccount
	 * @param debitAccount
	 * @param amount
	 * @param paymentType
	 * @param currency
	 * @param bankCode
	 * @param provinceCode
	 */
	public GetChallengeCodeInputDTO(String cif, String creditAccount, String debitAccount, Double amount,
			String paymentType, String currency, String bankCode, String provinceCode) {
		super();
		this.cif = cif;
		this.creditAccount = creditAccount;
		this.debitAccount = debitAccount;
		this.amount = amount;
		this.paymentType = paymentType;
		this.currency = currency;
		this.bankCode = bankCode;
		this.provinceCode = provinceCode;
	}

}
