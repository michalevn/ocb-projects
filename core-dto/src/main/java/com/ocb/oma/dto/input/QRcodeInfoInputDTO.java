package com.ocb.oma.dto.input;

public class QRcodeInfoInputDTO {

	private QRcodeInfoDTO qrcodeInfo;
	

	/**
	 * @return the qrcodeInfo
	 */
	public QRcodeInfoDTO getQrcodeInfo() {
		return qrcodeInfo;
	}


	/**
	 * @param qrcodeInfo the qrcodeInfo to set
	 */
	public void setQrcodeInfo(QRcodeInfoDTO qrcodeInfo) {
		this.qrcodeInfo = qrcodeInfo;
	}


	public QRcodeInfoInputDTO() {
		// TODO Auto-generated constructor stub
	}

}
