package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoIDInfo implements Serializable {

	private static final long serialVersionUID = -7633373315914577286L;

	private String IDNum;
	private String IDIssuedDate;
	private String IDIssuedLocation;
	private String IDType;
	private String nationality;

	public String getIDNum() {
		return IDNum;
	}

	public void setIDNum(String iDNum) {
		IDNum = iDNum;
	}

	public String getIDIssuedDate() {
		return IDIssuedDate;
	}

	public void setIDIssuedDate(String iDIssuedDate) {
		IDIssuedDate = iDIssuedDate;
	}

	public String getIDIssuedLocation() {
		return IDIssuedLocation;
	}

	public void setIDIssuedLocation(String iDIssuedLocation) {
		IDIssuedLocation = iDIssuedLocation;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

}
