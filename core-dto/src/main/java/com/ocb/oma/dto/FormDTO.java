package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FormDTO implements Serializable {

	private static final long serialVersionUID = -5285474048940133408L;

	private String uuid;

	private String title;

	private String description;

	private String userEmail;

	private String userPhone;

	private String userName;

	private List<FormDetailsDTO> formDetails = new ArrayList<FormDetailsDTO>();

	private Date updatedDate;

	private String updatedBy;

	private Date createdDate;

	private String createdBy;
	
	private String mailSuport;
	
	

	/**
	 * @return the mailSuport
	 */
	public String getMailSuport() {
		return mailSuport;
	}

	/**
	 * @param mailSuport the mailSuport to set
	 */
	public void setMailSuport(String mailSuport) {
		this.mailSuport = mailSuport;
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setFormDetails(List<FormDetailsDTO> formDetails) {
		this.formDetails = formDetails;
	}

	public List<FormDetailsDTO> getFormDetails() {
		return formDetails;
	}

}
