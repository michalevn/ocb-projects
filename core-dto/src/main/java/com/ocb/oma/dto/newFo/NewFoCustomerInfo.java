package com.ocb.oma.dto.newFo;

import java.io.Serializable;

public class NewFoCustomerInfo implements Serializable {

	private static final long serialVersionUID = 5929395928040377896L;

	private String fullname;
	private String birthDay;
	private String gender;
	private String maritalStatus;
	private NewFoJobInfo jobInfo;
	private NewFoIDInfo IDInfo;
	private NewFoAddress contactAddress;
	private NewFoContactInfo contactInfo;
	private NewFoAddress residentialAddress;
	private NewFoAddress permanentAddress;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public NewFoJobInfo getJobInfo() {
		return jobInfo;
	}

	public void setJobInfo(NewFoJobInfo jobInfo) {
		this.jobInfo = jobInfo;
	}

	public NewFoIDInfo getIDInfo() {
		return IDInfo;
	}

	public void setIDInfo(NewFoIDInfo iDInfo) {
		IDInfo = iDInfo;
	}

	public NewFoAddress getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(NewFoAddress contactAddress) {
		this.contactAddress = contactAddress;
	}

	public NewFoContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(NewFoContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public NewFoAddress getResidentialAddress() {
		return residentialAddress;
	}

	public void setResidentialAddress(NewFoAddress residentialAddress) {
		this.residentialAddress = residentialAddress;
	}

	public NewFoAddress getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(NewFoAddress permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

}
