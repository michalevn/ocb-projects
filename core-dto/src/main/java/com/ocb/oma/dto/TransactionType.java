/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class TransactionType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6314830654825931353L;
	private String code;
	private String name;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 */
	public TransactionType() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param code
	 * @param name
	 */
	public TransactionType(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

}
