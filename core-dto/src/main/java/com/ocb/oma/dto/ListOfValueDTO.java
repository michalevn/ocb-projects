/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class ListOfValueDTO implements Serializable {

	private Long lovId;
	private String lovType;
	private String lovCode;

	private String lovNameVn;;
	private String lovNameEl;

	private Integer orderNumber;

	
	/**
	 * 
	 */
	public ListOfValueDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	/**
	 * @param lovId
	 * @param lovType
	 * @param lovCode
	 * @param lovNameVn
	 * @param lovNameEl
	 * @param orderNumber
	 */
	public ListOfValueDTO(Long lovId, String lovType, String lovCode, String lovNameVn, String lovNameEl,
			Integer orderNumber) {
		super();
		this.lovId = lovId;
		this.lovType = lovType;
		this.lovCode = lovCode;
		this.lovNameVn = lovNameVn;
		this.lovNameEl = lovNameEl;
		this.orderNumber = orderNumber;
	}



	/**
	 * @return the lovId
	 */
	public Long getLovId() {
		return lovId;
	}

	/**
	 * @param lovId the lovId to set
	 */
	public void setLovId(Long lovId) {
		this.lovId = lovId;
	}

	/**
	 * @return the lovType
	 */
	public String getLovType() {
		return lovType;
	}

	/**
	 * @param lovType the lovType to set
	 */
	public void setLovType(String lovType) {
		this.lovType = lovType;
	}

	/**
	 * @return the lovCode
	 */
	public String getLovCode() {
		return lovCode;
	}

	/**
	 * @param lovCode the lovCode to set
	 */
	public void setLovCode(String lovCode) {
		this.lovCode = lovCode;
	}

	/**
	 * @return the lovNameVn
	 */
	public String getLovNameVn() {
		return lovNameVn;
	}

	/**
	 * @param lovNameVn the lovNameVn to set
	 */
	public void setLovNameVn(String lovNameVn) {
		this.lovNameVn = lovNameVn;
	}

	/**
	 * @return the lovNameEl
	 */
	public String getLovNameEl() {
		return lovNameEl;
	}

	/**
	 * @param lovNameEl the lovNameEl to set
	 */
	public void setLovNameEl(String lovNameEl) {
		this.lovNameEl = lovNameEl;
	}

	/**
	 * @return the orderNumber
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

}
