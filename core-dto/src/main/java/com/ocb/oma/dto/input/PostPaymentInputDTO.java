/**
 * 
 */
package com.ocb.oma.dto.input;

import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.validation.PostPaymentInputValid;

/**
 * @author phuhoang
 *
 */
@PostPaymentInputValid
public class PostPaymentInputDTO {

	
	 
	private String paymentType;


	private String transferId;

	private PaymentInfoDTO paymentInfo;
	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the transferId
	 */
	public String getTransferId() {
		return transferId;
	}

	/**
	 * @param transferId the transferId to set
	 */
	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the paymentInfo
	 */
	public PaymentInfoDTO getPaymentInfo() {
		return paymentInfo;
	}

	/**
	 * @param paymentInfo the paymentInfo to set
	 */
	public void setPaymentInfo(PaymentInfoDTO paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	/**
	 * 
	 */
	public PostPaymentInputDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param paymentType
	 * @param paymentInfo
	 */
	public PostPaymentInputDTO(String paymentType, PaymentInfoDTO paymentInfo) {
		super();
		this.paymentType = paymentType;
		this.paymentInfo = paymentInfo;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

	
}
