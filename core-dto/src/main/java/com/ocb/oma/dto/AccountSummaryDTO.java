/**
 * 
 */
package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class AccountSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2134394408512308290L;
	private String accountNo;

	private String lastInComeCurrency;
	private Double lastIncome;//debit
	private Date lastIncomeDate;
	private String lastOutComeCurrency;
	private Double lastOutCome;//credit
	private Date lastOutcomeDate;
	
	
	private LastInfoDTO lastCreditInfo;
	private LastInfoDTO lastDebitInfo;
	
	

	/**
	 * @return the lastCreditInfo
	 */
	public LastInfoDTO getLastCreditInfo() {
		return lastCreditInfo;
	}

	/**
	 * @param lastCreditInfo the lastCreditInfo to set
	 */
	public void setLastCreditInfo(LastInfoDTO lastCreditInfo) {
		this.lastCreditInfo = lastCreditInfo;
	}

	/**
	 * @return the lastDebitInfo
	 */
	public LastInfoDTO getLastDebitInfo() {
		return lastDebitInfo;
	}

	/**
	 * @param lastDebitInfo the lastDebitInfo to set
	 */
	public void setLastDebitInfo(LastInfoDTO lastDebitInfo) {
		this.lastDebitInfo = lastDebitInfo;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the lastOutComeCurrency
	 */
	public String getLastOutComeCurrency() {
		return lastOutComeCurrency;
	}

	/**
	 * @param lastOutComeCurrency the lastOutComeCurrency to set
	 */
	public void setLastOutComeCurrency(String lastOutComeCurrency) {
		this.lastOutComeCurrency = lastOutComeCurrency;
	}

	/**
	 * @return the lastInComeCurrency
	 */
	public String getLastInComeCurrency() {
		return lastInComeCurrency;
	}

	/**
	 * @param lastInComeCurrency the lastInComeCurrency to set
	 */
	public void setLastInComeCurrency(String lastInComeCurrency) {
		this.lastInComeCurrency = lastInComeCurrency;
	}

	/**
	 * @return the lastIncome
	 */
	public Double getLastIncome() {
		return lastIncome;
	}

	/**
	 * @param lastIncome the lastIncome to set
	 */
	public void setLastIncome(Double lastIncome) {
		this.lastIncome = lastIncome;
	}

	/**
	 * @return the lastIncomeDate
	 */
	public Date getLastIncomeDate() {
		return lastIncomeDate;
	}

	/**
	 * @param lastIncomeDate the lastIncomeDate to set
	 */
	public void setLastIncomeDate(Date lastIncomeDate) {
		this.lastIncomeDate = lastIncomeDate;
	}

	/**
	 * @return the lastOutcomeDate
	 */
	public Date getLastOutcomeDate() {
		return lastOutcomeDate;
	}

	/**
	 * @param lastOutcomeDate the lastOutcomeDate to set
	 */
	public void setLastOutcomeDate(Date lastOutcomeDate) {
		this.lastOutcomeDate = lastOutcomeDate;
	}

	/**
	 * 
	 */
	public AccountSummaryDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the lastOutCome
	 */
	public Double getLastOutCome() {
		return lastOutCome;
	}

	/**
	 * @param lastOutCome the lastOutCome to set
	 */
	public void setLastOutCome(Double lastOutCome) {
		this.lastOutCome = lastOutCome;
	}

	/**
	 * @param accountNo
	 * @param amount
	 * @param availBalance
	 * @param lastOutComeCurrency
	 * @param lastInComeCurrency
	 * @param lastIncome
	 * @param lastIncomeDate
	 * @param lastOutcomeDate
	 */
	public AccountSummaryDTO(String accountNo, String lastOutComeCurrency, String lastInComeCurrency, Double lastIncome,
			Date lastIncomeDate, Date lastOutcomeDate, Double lastOutcome) {
		super();
		this.accountNo = accountNo;
		this.lastOutComeCurrency = lastOutComeCurrency;
		this.lastInComeCurrency = lastInComeCurrency;
		this.lastIncome = lastIncome;
		this.lastIncomeDate = lastIncomeDate;
		this.lastOutcomeDate = lastOutcomeDate;
		this.lastOutCome = lastOutcome;
	}

}
