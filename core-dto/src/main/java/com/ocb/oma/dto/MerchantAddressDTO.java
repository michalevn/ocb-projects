package com.ocb.oma.dto;

public class MerchantAddressDTO {
	
	private String town;
	private String country;
	private String postCode;
	private String street;
	
	

	public MerchantAddressDTO(String town, String country, String postCode, String street) {
		super();
		this.town = town;
		this.country = country;
		this.postCode = postCode;
		this.street = street;
	}



	/**
	 * @return the town
	 */
	public String getTown() {
		return town;
	}



	/**
	 * @param town the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}



	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}



	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}



	/**
	 * @return the postCode
	 */
	public String getPostCode() {
		return postCode;
	}



	/**
	 * @param postCode the postCode to set
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}



	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}



	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}



	public MerchantAddressDTO() {
		// TODO Auto-generated constructor stub
	}

}
