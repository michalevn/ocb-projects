package com.ocb.oma.dto;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.validation.AmountLimit;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.FinishDate;
import com.ocb.oma.validation.PaymentSetting;
import com.ocb.oma.validation.RecurringPeriod;

public class TransferAutoBillDTO implements Serializable {

	private static final long serialVersionUID = -4475721357060352217L;

	private String globusId;
	private String orderName;
	private Date firstExecutionDate;//

	@FinishDate
	private Date finishDate;//

	private String serviceCodeName;

	@PaymentSetting(message = "PAYMENTSETTING_INVALID")
	private String paymentSetting;//

	private String customerId;
	private String frequencyPeriodUnit;//
	private String orderNumber;//
	private String nextExecutionDate;
	private String owner;
	private String serviceProviderCode;
	private String serviceCode;
	private String fromAccount;

	@Digits(message = "FREQUENCYPERIODCOUNT_" + ValidationMessageConstant.MUST_BE_NUMBER)
	private int frequencyPeriodCount;//

	@AmountLimit
	private Double amountLimit;//

	@RecurringPeriod(message = "RECURRINGPERIOD_INVALID")
	private String recurringPeriod;//

	private String serviceProviderCodeName;

	/**
	 * @return the globusId
	 */
	public String getGlobusId() {
		return globusId;
	}

	/**
	 * @param globusId the globusId to set
	 */
	public void setGlobusId(String globusId) {
		this.globusId = globusId;
	}

	/**
	 * @return the orderName
	 */
	public String getOrderName() {
		return orderName;
	}

	/**
	 * @param orderName the orderName to set
	 */
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	/**
	 * @return the firstExecutionDate
	 */
	public Date getFirstExecutionDate() {
		return firstExecutionDate;
	}

	/**
	 * @param firstExecutionDate the firstExecutionDate to set
	 */
	public void setFirstExecutionDate(Date firstExecutionDate) {
		this.firstExecutionDate = firstExecutionDate;
	}

	/**
	 * @return the finishDate
	 */
	public Date getFinishDate() {
		return finishDate;
	}

	/**
	 * @param finishDate the finishDate to set
	 */
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	/**
	 * @return the serviceCodeName
	 */
	public String getServiceCodeName() {
		return serviceCodeName;
	}

	/**
	 * @param serviceCodeName the serviceCodeName to set
	 */
	public void setServiceCodeName(String serviceCodeName) {
		this.serviceCodeName = serviceCodeName;
	}

	/**
	 * @return the paymentSetting
	 */
	public String getPaymentSetting() {
		return paymentSetting;
	}

	/**
	 * @param paymentSetting the paymentSetting to set
	 */
	public void setPaymentSetting(String paymentSetting) {
		this.paymentSetting = paymentSetting;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the frequencyPeriodUnit
	 */
	public String getFrequencyPeriodUnit() {
		return frequencyPeriodUnit;
	}

	/**
	 * @param frequencyPeriodUnit the frequencyPeriodUnit to set
	 */
	public void setFrequencyPeriodUnit(String frequencyPeriodUnit) {
		this.frequencyPeriodUnit = frequencyPeriodUnit;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the nextExecutionDate
	 */
	public String getNextExecutionDate() {
		return nextExecutionDate;
	}

	/**
	 * @param nextExecutionDate the nextExecutionDate to set
	 */
	public void setNextExecutionDate(String nextExecutionDate) {
		this.nextExecutionDate = nextExecutionDate;
	}

	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @return the serviceProviderCode
	 */
	public String getServiceProviderCode() {
		return serviceProviderCode;
	}

	/**
	 * @param serviceProviderCode the serviceProviderCode to set
	 */
	public void setServiceProviderCode(String serviceProviderCode) {
		this.serviceProviderCode = serviceProviderCode;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the fromAccount
	 */
	public String getFromAccount() {
		return fromAccount;
	}

	/**
	 * @param fromAccount the fromAccount to set
	 */
	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	/**
	 * @return the frequencyPeriodCount
	 */
	public int getFrequencyPeriodCount() {
		return frequencyPeriodCount;
	}

	/**
	 * @param frequencyPeriodCount the frequencyPeriodCount to set
	 */
	public void setFrequencyPeriodCount(int frequencyPeriodCount) {
		this.frequencyPeriodCount = frequencyPeriodCount;
	}

	/**
	 * @return the amountLimit
	 */
	public Double getAmountLimit() {
		return amountLimit;
	}

	/**
	 * @param amountLimit the amountLimit to set
	 */
	public void setAmountLimit(Double amountLimit) {
		this.amountLimit = amountLimit;
	}

	/**
	 * @return the recurringPeriod
	 */
	public String getRecurringPeriod() {
		return recurringPeriod;
	}

	/**
	 * @param recurringPeriod the recurringPeriod to set
	 */
	public void setRecurringPeriod(String recurringPeriod) {
		this.recurringPeriod = recurringPeriod;
	}

	/**
	 * @return the serviceProviderCodeName
	 */
	public String getServiceProviderCodeName() {
		return serviceProviderCodeName;
	}

	/**
	 * @param serviceProviderCodeName the serviceProviderCodeName to set
	 */
	public void setServiceProviderCodeName(String serviceProviderCodeName) {
		this.serviceProviderCodeName = serviceProviderCodeName;
	}

}
