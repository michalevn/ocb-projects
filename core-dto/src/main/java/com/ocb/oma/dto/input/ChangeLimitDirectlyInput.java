/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.List;

import com.ocb.oma.dto.ChangeLimitDTO;

/**
 * @author phuhoang
 *
 */
public class ChangeLimitDirectlyInput  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5342493449424720215L;
	private List<ChangeLimitDTO> limits;
	private VerifyOtpToken otpValue;
	private String otpMethod;

	/**
	 * @return the limits
	 */
	public List<ChangeLimitDTO> getLimits() {
		return limits;
	}

	/**
	 * @param limits the limits to set
	 */
	public void setLimits(List<ChangeLimitDTO> limits) {
		this.limits = limits;
	}

	/**
	 * @return the otpValue
	 */
	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	/**
	 * @param otpValue the otpValue to set
	 */
	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	/**
	 * @return the otpMethod
	 */
	public String getOtpMethod() {
		return otpMethod;
	}

	/**
	 * @param otpMethod the otpMethod to set
	 */
	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
