package com.ocb.oma.dto.input;

public class CriteriaDTO {
	private Long deviceId;

	/**
	 * @return the deviceId
	 */
	public Long getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public CriteriaDTO() {
		// TODO Auto-generated constructor stub
	}

}
