package com.ocb.oma.dto.input;

import java.util.Date;

import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;

@DateRange(from = "dateFrom", to = "dateTo", message = "dateFrom.must.be.less.than.or.equal.to.dateTo")
public class CardRejectedInputDTO {
	private String productId;
	private String productType;
	@Digits
	private int pageNumber;
	@Digits
	private int pageSize;
	private String operationType;
	private Date dateFrom;
	private Date dateTo;

	public CardRejectedInputDTO(String productId, String productType, int pageNumber, int pageSize,
			String operationType, Date dateFrom, Date dateTo) {
		super();
		this.productId = productId;
		this.productType = productType;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.operationType = operationType;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom the dateFrom to set
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the dateTo
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo the dateTo to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public CardRejectedInputDTO() {
		// TODO Auto-generated constructor stub
	}

}
