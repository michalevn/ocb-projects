/**
 * 
 */
package com.ocb.oma.dto.input;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.VND;

/**
 * @author phuhoang
 *
 */
public class TransactionFeeInputDTO implements Serializable {

	private String debitAccount;
	private String creditAccount;
	@Digits
	private Double amount;
	@VND
	private String currency;
	private String paymentType;
	private String province;
	private Date bookingDate;

	/**
	 * @return the debitAccount
	 */
	public String getDebitAccount() {
		return debitAccount;
	}

	/**
	 * @param debitAccount the debitAccount to set
	 */
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	/**
	 * @return the creditAccount
	 */
	public String getCreditAccount() {
		return creditAccount;
	}

	/**
	 * @param creditAccount the creditAccount to set
	 */
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the bookingDate
	 */
	public Date getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate the bookingDate to set
	 */
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @param debitAccount
	 * @param creditAccount
	 * @param amount
	 * @param currency
	 * @param paymentType
	 * @param province
	 * @param bookingDate
	 */
	public TransactionFeeInputDTO(String debitAccount, String creditAccount, Double amount, String currency,
			String paymentType, String province, Date bookingDate) {
		super();
		this.debitAccount = debitAccount;
		this.creditAccount = creditAccount;
		this.amount = amount;
		this.currency = currency;
		this.paymentType = paymentType;
		this.province = province;
		this.bookingDate = bookingDate;
	}

	/**
	* 
	*/
	public TransactionFeeInputDTO() {
		// TODO Auto-generated constructor stub
	}
}
