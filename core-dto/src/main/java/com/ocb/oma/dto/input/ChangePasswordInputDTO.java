package com.ocb.oma.dto.input;

public class ChangePasswordInputDTO {
	//@Password(message = ValidationMessageConstant.INPUT_PASSWORD_FORMAT_INVALID)
	private String oldPassword;
	//@Password(message = ValidationMessageConstant.INPUT_PASSWORD_FORMAT_INVALID)
	private String newPassword;

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public ChangePasswordInputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
