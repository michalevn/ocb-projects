package com.ocb.oma.dto;

import java.util.Date;

public class PaymentDTO {
	
	private String id;
	private String accountNo;
	private String accountCurrency;
	private String accountId;
	private String accountName;
	private String[] recipientName;
	private String[] recipientAddress;
	private String[] senderName;
	private String[] senderAddress;
	private String recipientAccountNo;
	private String recipientAccountId;
	private Double amount;
	
	private String currency;
	private String[] title;
	private String transferType;
	private String paymentType;
	private String status;
	private Date realizationDate;
	private Date registrationDate;
	private PaymentDetailDTO paymentDetails;
	private boolean isCyclic;
	private Date deliveryDate;
	private String owner;
	private String operationStatus;
	private String transactionId;
	private String originalCustomerId;
	private String originalCustomerName;
	private String transactionTypeDesc;
	private String[] description;
	private String eWalletPhoneNumber;
	private boolean addToBasket;
	private boolean originalCustomerVisible;
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}







	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}







	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}







	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}







	/**
	 * @return the accountCurrency
	 */
	public String getAccountCurrency() {
		return accountCurrency;
	}







	/**
	 * @param accountCurrency the accountCurrency to set
	 */
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}







	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}







	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}







	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}







	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}







	/**
	 * @return the recipientName
	 */
	public String[] getRecipientName() {
		return recipientName;
	}







	/**
	 * @param recipientName the recipientName to set
	 */
	public void setRecipientName(String[] recipientName) {
		this.recipientName = recipientName;
	}







	/**
	 * @return the recipientAddress
	 */
	public String[] getRecipientAddress() {
		return recipientAddress;
	}







	/**
	 * @param recipientAddress the recipientAddress to set
	 */
	public void setRecipientAddress(String[] recipientAddress) {
		this.recipientAddress = recipientAddress;
	}







	/**
	 * @return the senderName
	 */
	public String[] getSenderName() {
		return senderName;
	}







	/**
	 * @param senderName the senderName to set
	 */
	public void setSenderName(String[] senderName) {
		this.senderName = senderName;
	}







	/**
	 * @return the senderAddress
	 */
	public String[] getSenderAddress() {
		return senderAddress;
	}







	/**
	 * @param senderAddress the senderAddress to set
	 */
	public void setSenderAddress(String[] senderAddress) {
		this.senderAddress = senderAddress;
	}







	/**
	 * @return the recipientAccountNo
	 */
	public String getRecipientAccountNo() {
		return recipientAccountNo;
	}







	/**
	 * @param recipientAccountNo the recipientAccountNo to set
	 */
	public void setRecipientAccountNo(String recipientAccountNo) {
		this.recipientAccountNo = recipientAccountNo;
	}







	/**
	 * @return the recipientAccountId
	 */
	public String getRecipientAccountId() {
		return recipientAccountId;
	}







	/**
	 * @param recipientAccountId the recipientAccountId to set
	 */
	public void setRecipientAccountId(String recipientAccountId) {
		this.recipientAccountId = recipientAccountId;
	}







	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}







	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}







	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}







	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}







	/**
	 * @return the title
	 */
	public String[] getTitle() {
		return title;
	}







	/**
	 * @param title the title to set
	 */
	public void setTitle(String[] title) {
		this.title = title;
	}







	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}







	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}







	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}







	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}







	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}







	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}







	/**
	 * @return the realizationDate
	 */
	public Date getRealizationDate() {
		return realizationDate;
	}







	/**
	 * @param realizationDate the realizationDate to set
	 */
	public void setRealizationDate(Date realizationDate) {
		this.realizationDate = realizationDate;
	}







	/**
	 * @return the registrationDate
	 */
	public Date getRegistrationDate() {
		return registrationDate;
	}







	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}







	/**
	 * @return the paymentDetails
	 */
	public PaymentDetailDTO getPaymentDetails() {
		return paymentDetails;
	}







	/**
	 * @param paymentDetails the paymentDetails to set
	 */
	public void setPaymentDetails(PaymentDetailDTO paymentDetails) {
		this.paymentDetails = paymentDetails;
	}







	/**
	 * @return the isCyclic
	 */
	public boolean isCyclic() {
		return isCyclic;
	}







	/**
	 * @param isCyclic the isCyclic to set
	 */
	public void setCyclic(boolean isCyclic) {
		this.isCyclic = isCyclic;
	}







	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}







	/**
	 * @param deliveryDate the deliveryDate to set
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}







	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}







	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}







	/**
	 * @return the operationStatus
	 */
	public String getOperationStatus() {
		return operationStatus;
	}







	/**
	 * @param operationStatus the operationStatus to set
	 */
	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}







	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}







	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}







	/**
	 * @return the originalCustomerId
	 */
	public String getOriginalCustomerId() {
		return originalCustomerId;
	}







	/**
	 * @param originalCustomerId the originalCustomerId to set
	 */
	public void setOriginalCustomerId(String originalCustomerId) {
		this.originalCustomerId = originalCustomerId;
	}







	/**
	 * @return the originalCustomerName
	 */
	public String getOriginalCustomerName() {
		return originalCustomerName;
	}







	/**
	 * @param originalCustomerName the originalCustomerName to set
	 */
	public void setOriginalCustomerName(String originalCustomerName) {
		this.originalCustomerName = originalCustomerName;
	}







	/**
	 * @return the transactionTypeDesc
	 */
	public String getTransactionTypeDesc() {
		return transactionTypeDesc;
	}







	/**
	 * @param transactionTypeDesc the transactionTypeDesc to set
	 */
	public void setTransactionTypeDesc(String transactionTypeDesc) {
		this.transactionTypeDesc = transactionTypeDesc;
	}







	/**
	 * @return the description
	 */
	public String[] getDescription() {
		return description;
	}







	/**
	 * @param description the description to set
	 */
	public void setDescription(String[] description) {
		this.description = description;
	}







	/**
	 * @return the eWalletPhoneNumber
	 */
	public String geteWalletPhoneNumber() {
		return eWalletPhoneNumber;
	}







	/**
	 * @param eWalletPhoneNumber the eWalletPhoneNumber to set
	 */
	public void seteWalletPhoneNumber(String eWalletPhoneNumber) {
		this.eWalletPhoneNumber = eWalletPhoneNumber;
	}







	/**
	 * @return the addToBasket
	 */
	public boolean isAddToBasket() {
		return addToBasket;
	}







	/**
	 * @param addToBasket the addToBasket to set
	 */
	public void setAddToBasket(boolean addToBasket) {
		this.addToBasket = addToBasket;
	}







	/**
	 * @return the originalCustomerVisible
	 */
	public boolean isOriginalCustomerVisible() {
		return originalCustomerVisible;
	}







	/**
	 * @param originalCustomerVisible the originalCustomerVisible to set
	 */
	public void setOriginalCustomerVisible(boolean originalCustomerVisible) {
		this.originalCustomerVisible = originalCustomerVisible;
	}







	public PaymentDTO() {
		// TODO Auto-generated constructor stub
	}

}
