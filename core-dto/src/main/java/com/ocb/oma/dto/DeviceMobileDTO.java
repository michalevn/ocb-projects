package com.ocb.oma.dto;

import java.io.Serializable;

public class DeviceMobileDTO implements Serializable {

	private static final long serialVersionUID = 6659957028949285196L;

	private String platform;
	private String version;
	private String uuid;
	private String virtual;
	private String cordova;
	private String manufacturer;
	private String model;
	private String serial;

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getVirtual() {
		return virtual;
	}

	public void setVirtual(String virtual) {
		this.virtual = virtual;
	}

	public String getCordova() {
		return cordova;
	}

	public void setCordova(String cordova) {
		this.cordova = cordova;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

}
