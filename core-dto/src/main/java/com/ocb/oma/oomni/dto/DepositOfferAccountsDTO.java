package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.List;

public class DepositOfferAccountsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<AccountSourceDTO> accountSourceList;
	private List<AccountCapitalDTO> accountCapitalList;
	private List<AccountInterestDTO> accountInterestList;
	/**
	 * @return the accountSourceList
	 */
	public List<AccountSourceDTO> getAccountSourceList() {
		return accountSourceList;
	}
	/**
	 * @param accountSourceList the accountSourceList to set
	 */
	public void setAccountSourceList(List<AccountSourceDTO> accountSourceList) {
		this.accountSourceList = accountSourceList;
	}
	/**
	 * @return the accountCapitalList
	 */
	public List<AccountCapitalDTO> getAccountCapitalList() {
		return accountCapitalList;
	}
	/**
	 * @param accountCapitalList the accountCapitalList to set
	 */
	public void setAccountCapitalList(List<AccountCapitalDTO> accountCapitalList) {
		this.accountCapitalList = accountCapitalList;
	}
	/**
	 * @return the accountInterestList
	 */
	public List<AccountInterestDTO> getAccountInterestList() {
		return accountInterestList;
	}
	/**
	 * @param accountInterestList the accountInterestList to set
	 */
	public void setAccountInterestList(List<AccountInterestDTO> accountInterestList) {
		this.accountInterestList = accountInterestList;
	}
	public DepositOfferAccountsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DepositOfferAccountsDTO(List<AccountSourceDTO> accountSourceList, List<AccountCapitalDTO> accountCapitalList,
			List<AccountInterestDTO> accountInterestList) {
		super();
		this.accountSourceList = accountSourceList;
		this.accountCapitalList = accountCapitalList;
		this.accountInterestList = accountInterestList;
	}
	
	
}
