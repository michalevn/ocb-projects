package com.ocb.oma.oomni;

import java.util.Date;

import com.ocb.oma.dto.MerchantAddressDTO;

public class CardRejectedDTO {
	
	private Date operationDate;
	private String transactionType;
	private Double transactionAmount;
	private String currency;
	private String merchantName;
	private MerchantAddressDTO merchantAddress;
	private String transactionDesc;
	private String rejectionReason;
	private String cardUser;
	
	
	
	/**
	 * @return the operationDate
	 */
	public Date getOperationDate() {
		return operationDate;
	}



	/**
	 * @param operationDate the operationDate to set
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}



	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}



	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}



	/**
	 * @return the transactionAmount
	 */
	public Double getTransactionAmount() {
		return transactionAmount;
	}



	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}



	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}



	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}



	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}



	/**
	 * @param merchantName the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}



	/**
	 * @return the merchantAddress
	 */
	public MerchantAddressDTO getMerchantAddress() {
		return merchantAddress;
	}



	/**
	 * @param merchantAddress the merchantAddress to set
	 */
	public void setMerchantAddress(MerchantAddressDTO merchantAddress) {
		this.merchantAddress = merchantAddress;
	}



	/**
	 * @return the transactionDesc
	 */
	public String getTransactionDesc() {
		return transactionDesc;
	}



	/**
	 * @param transactionDesc the transactionDesc to set
	 */
	public void setTransactionDesc(String transactionDesc) {
		this.transactionDesc = transactionDesc;
	}



	/**
	 * @return the rejectionReason
	 */
	public String getRejectionReason() {
		return rejectionReason;
	}



	/**
	 * @param rejectionReason the rejectionReason to set
	 */
	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}



	/**
	 * @return the cardUser
	 */
	public String getCardUser() {
		return cardUser;
	}



	/**
	 * @param cardUser the cardUser to set
	 */
	public void setCardUser(String cardUser) {
		this.cardUser = cardUser;
	}



	public CardRejectedDTO() {
		// TODO Auto-generated constructor stub
	}

}
