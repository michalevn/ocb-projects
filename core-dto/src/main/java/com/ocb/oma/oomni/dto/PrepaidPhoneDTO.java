/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class PrepaidPhoneDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7363944430437573995L;
	private long id;
	private String phoneName;
	private String phoneNumber;
	
	
	public PrepaidPhoneDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the phoneName
	 */
	public String getPhoneName() {
		return phoneName;
	}


	/**
	 * @param phoneName the phoneName to set
	 */
	public void setPhoneName(String phoneName) {
		this.phoneName = phoneName;
	}


	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}


	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public PrepaidPhoneDTO(long id, String phoneName, String phoneNumber) {
		super();
		this.id = id;
		this.phoneName = phoneName;
		this.phoneNumber = phoneNumber;
	}
	
	
 
}
