/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhns
 *
 */
public class GetListOfBasketInputDTO implements Serializable  {

	private static final long serialVersionUID = 8441520243550706146L;
	
	private Double amountFrom;
    private Double amountTo;

    private String customerId;
    private String realizationDateFrom;
    private String realizationDateTo;
    private String statuses;
    public Double getAmountFrom() {
        return amountFrom;
    }
    public void setAmountFrom(Double amountFrom) {
        this.amountFrom = amountFrom;
    }
    public Double getAmountTo() {
        return amountTo;
    }
    public void setAmountTo(Double amountTo) {
        this.amountTo = amountTo;
    }
    public String getCustomerId() {
        return customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getRealizationDateFrom() {
        return realizationDateFrom;
    }
    public void setRealizationDateFrom(String realizationDateFrom) {
        this.realizationDateFrom = realizationDateFrom;
    }
    public String getRealizationDateTo() {
        return realizationDateTo;
    }
    public void setRealizationDateTo(String realizationDateTo) {
        this.realizationDateTo = realizationDateTo;
    }
    public String getStatuses() {
        return statuses;
    }
    public void setStatuses(String statuses) {
        this.statuses = statuses;
    }
    public GetListOfBasketInputDTO(Double amountFrom, Double amountTo, String customerId, String realizationDateFrom,
            String realizationDateTo, String statuses) {
        super();
        this.amountFrom = amountFrom;
        this.amountTo = amountTo;
        this.customerId = customerId;
        this.realizationDateFrom = realizationDateFrom;
        this.realizationDateTo = realizationDateTo;
        this.statuses = statuses;
    }
    public GetListOfBasketInputDTO() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    public String toString() {
        return "GetListOfBasketInputDTO [amountFrom=" + amountFrom + ", amountTo=" + amountTo + ", customerId="
                + customerId + ", realizationDateFrom=" + realizationDateFrom + ", realizationDateTo="
                + realizationDateTo + ", statuses=" + statuses + "]";
    }
    
    

}