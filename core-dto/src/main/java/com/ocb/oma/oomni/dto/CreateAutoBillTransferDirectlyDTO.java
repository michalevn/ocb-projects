package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;

import com.ocb.oma.dto.ValidationMessageConstant;
import com.ocb.oma.dto.input.VerifyOtpToken;
import com.ocb.oma.validation.AmountLimit;
import com.ocb.oma.validation.CustomerId;
import com.ocb.oma.validation.DateRange;
import com.ocb.oma.validation.Digits;
import com.ocb.oma.validation.FinishDate;
import com.ocb.oma.validation.PaymentSetting;
import com.ocb.oma.validation.RecurringPeriod;
import com.ocb.oma.validation.ServiceCode;
import com.ocb.oma.validation.ServiceProviderCode;

@DateRange(from = "firstExecutionDate", to = "finishDate", message = "firstExecutionDate.less.than.or.equal.to.finishDate")
public class CreateAutoBillTransferDirectlyDTO implements Serializable {

	private static final long serialVersionUID = 2241536076368456988L;

	@Digits(message = "FREQUENCYPERIODCOUNT_" + ValidationMessageConstant.MUST_BE_NUMBER)
	private Integer frequencyPeriodCount;

	@PaymentSetting(message = "PAYMENTSETTING_INVALID")
	private String paymentSetting;

	@RecurringPeriod(message = "RECURRINGPERIOD_INVALID")
	private String recurringPeriod;

	@CustomerId(message = "CUSTOMERID_INVALID")
	private String customerId;

	@ServiceCode(message = "SERVICECODE_INVALID")
	private String serviceCode;

	@ServiceProviderCode(message = "SERVICEPROVIDERCODE_INVALID")
	private String serviceProviderCode;

	@Digits(message = "AMOUNTLIMIT_" + ValidationMessageConstant.MUST_BE_NUMBER)
	@AmountLimit(message = "AMOUNTLIMIT_INVALID")
	private Double amountLimit;

	private String fromAccount;

	private Date firstExecutionDate;
	@FinishDate(message = "FINISHDATE_INVALID")
	private Date finishDate;

	private String currency;
	private String frequencyPeriodUnit;
	private Date nextExecutionDate;
	private String serviceProviderCodeName;
	private String serviceCodeName;

	private VerifyOtpToken otpValue;
	private String otpMethod;

	public Integer getFrequencyPeriodCount() {
		return frequencyPeriodCount;
	}

	public void setFrequencyPeriodCount(Integer frequencyPeriodCount) {
		this.frequencyPeriodCount = frequencyPeriodCount;
	}

	public String getPaymentSetting() {
		return paymentSetting;
	}

	public void setPaymentSetting(String paymentSetting) {
		this.paymentSetting = paymentSetting;
	}

	public String getRecurringPeriod() {
		return recurringPeriod;
	}

	public void setRecurringPeriod(String recurringPeriod) {
		this.recurringPeriod = recurringPeriod;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceProviderCode() {
		return serviceProviderCode;
	}

	public void setServiceProviderCode(String serviceProviderCode) {
		this.serviceProviderCode = serviceProviderCode;
	}

	public Double getAmountLimit() {
		return amountLimit;
	}

	public void setAmountLimit(Double amountLimit) {
		this.amountLimit = amountLimit;
	}

	public Date getFirstExecutionDate() {
		return firstExecutionDate;
	}

	public void setFirstExecutionDate(Date firstExecutionDate) {
		this.firstExecutionDate = firstExecutionDate;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getFrequencyPeriodUnit() {
		return frequencyPeriodUnit;
	}

	public void setFrequencyPeriodUnit(String frequencyPeriodUnit) {
		this.frequencyPeriodUnit = frequencyPeriodUnit;
	}

	public Date getNextExecutionDate() {
		return nextExecutionDate;
	}

	public void setNextExecutionDate(Date nextExecutionDate) {
		this.nextExecutionDate = nextExecutionDate;
	}

	public String getServiceProviderCodeName() {
		return serviceProviderCodeName;
	}

	public void setServiceProviderCodeName(String serviceProviderCodeName) {
		this.serviceProviderCodeName = serviceProviderCodeName;
	}

	public String getServiceCodeName() {
		return serviceCodeName;
	}

	public void setServiceCodeName(String serviceCodeName) {
		this.serviceCodeName = serviceCodeName;
	}

	public VerifyOtpToken getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(VerifyOtpToken otpValue) {
		this.otpValue = otpValue;
	}

	public String getOtpMethod() {
		return otpMethod;
	}

	public void setOtpMethod(String otpMethod) {
		this.otpMethod = otpMethod;
	}

}
