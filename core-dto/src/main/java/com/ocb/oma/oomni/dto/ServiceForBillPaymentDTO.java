/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class ServiceForBillPaymentDTO implements Serializable {

	private String serviceCode;
	private String serviceName;

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
