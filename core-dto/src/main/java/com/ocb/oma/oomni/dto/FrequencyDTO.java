/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class FrequencyDTO implements Serializable {
	
	private Date nextDate;
	private String periodUnit;
	private int periodCount;
	private int dayOfMonth;
	/**
	 * @return the nextDate
	 */
	public Date getNextDate() {
		return nextDate;
	}
	/**
	 * @param nextDate the nextDate to set
	 */
	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}
	/**
	 * @return the periodUnit
	 */
	public String getPeriodUnit() {
		return periodUnit;
	}
	/**
	 * @param periodUnit the periodUnit to set
	 */
	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}
	/**
	 * @return the periodCount
	 */
	public int getPeriodCount() {
		return periodCount;
	}
	/**
	 * @param periodCount the periodCount to set
	 */
	public void setPeriodCount(int periodCount) {
		this.periodCount = periodCount;
	}
	/**
	 * @return the dayOfMonth
	 */
	public int getDayOfMonth() {
		return dayOfMonth;
	}
	/**
	 * @param dayOfMonth the dayOfMonth to set
	 */
	public void setDayOfMonth(int dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}
	
	
 
}
