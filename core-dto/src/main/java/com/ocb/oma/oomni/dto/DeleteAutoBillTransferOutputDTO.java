/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class DeleteAutoBillTransferOutputDTO implements Serializable {

	private String status;
	private String authErrorCause;
	private String referenceId;
	private String coreRefNum2;
	private String errorMsg;
	private boolean success;
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the authErrorCause
	 */
	public String getAuthErrorCause() {
		return authErrorCause;
	}
	/**
	 * @param authErrorCause the authErrorCause to set
	 */
	public void setAuthErrorCause(String authErrorCause) {
		this.authErrorCause = authErrorCause;
	}
	/**
	 * @return the referenceId
	 */
	public String getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the coreRefNum2
	 */
	public String getCoreRefNum2() {
		return coreRefNum2;
	}
	/**
	 * @param coreRefNum2 the coreRefNum2 to set
	 */
	public void setCoreRefNum2(String coreRefNum2) {
		this.coreRefNum2 = coreRefNum2;
	}
	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public DeleteAutoBillTransferOutputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
