package com.ocb.oma.oomni.dto;

import com.ocb.oma.dto.AvatarInfoDTO;

public class AvatarInfoOutputDTO {
	private AvatarInfoDTO avatarInfo;
	private String status;
	private String message;
	
	

	public AvatarInfoOutputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the avatarInfo
	 */
	public AvatarInfoDTO getAvatarInfo() {
		return avatarInfo;
	}



	/**
	 * @param avatarInfo the avatarInfo to set
	 */
	public void setAvatarInfo(AvatarInfoDTO avatarInfo) {
		this.avatarInfo = avatarInfo;
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}



	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}



	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AvatarInfoOutputDTO [avatarInfo=" + avatarInfo + ", message=" + message + ", status=" + status + "]";
	}

}
