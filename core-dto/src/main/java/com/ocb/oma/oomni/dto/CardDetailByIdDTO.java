package com.ocb.oma.oomni.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class CardDetailByIdDTO {
	
	private Date dateExpirationStart;
	private Double dailyTrxLimit;
	private Double dailyCashLimit;
	private int dailyTrxLimitCount;
	private int dailyCashLimitCount;
	private Double maxDailyTrxLimit;
	private Double maxDailyCashLimit;
	private int maxDailyTrxLimitCount;
	private int maxDailyCashLimitCount;
	private Double internetLimit;
	private Double motoLimit;
	private int internetUseLimit;
	private int motoUseLimit;
	private Double minPayment;
	private Date minPaymentDate;
	private Double limitInCycle;
	private Double debt;
	private Double debtPrevSettlmnt;
	private Double debtPrevUnsettled;
	private Double minDebtPrevSettlmnt;
	private String automaticRepayment;
	private String billingCycle;
	private Double cashTransactionsInterest;
	private Double nonCashTransactionsInterest;
	private Double accountLimit;
	private Double limitUsed;
	private Double minDebtPrevSettlmntPlain;
	private Double statementDistributionDesc;
	private List<Map<String, ?>> installments;
	
	
	
	/**
	 * @return the automaticRepayment
	 */
	public String getAutomaticRepayment() {
		return automaticRepayment;
	}



	/**
	 * @param automaticRepayment the automaticRepayment to set
	 */
	public void setAutomaticRepayment(String automaticRepayment) {
		this.automaticRepayment = automaticRepayment;
	}



	/**
	 * @return the dateExpirationStart
	 */
	public Date getDateExpirationStart() {
		return dateExpirationStart;
	}



	/**
	 * @param dateExpirationStart the dateExpirationStart to set
	 */
	public void setDateExpirationStart(Date dateExpirationStart) {
		this.dateExpirationStart = dateExpirationStart;
	}



	/**
	 * @return the dailyTrxLimit
	 */
	public Double getDailyTrxLimit() {
		return dailyTrxLimit;
	}



	/**
	 * @param dailyTrxLimit the dailyTrxLimit to set
	 */
	public void setDailyTrxLimit(Double dailyTrxLimit) {
		this.dailyTrxLimit = dailyTrxLimit;
	}



	/**
	 * @return the dailyCashLimit
	 */
	public Double getDailyCashLimit() {
		return dailyCashLimit;
	}



	/**
	 * @param dailyCashLimit the dailyCashLimit to set
	 */
	public void setDailyCashLimit(Double dailyCashLimit) {
		this.dailyCashLimit = dailyCashLimit;
	}



	/**
	 * @return the dailyTrxLimitCount
	 */
	public int getDailyTrxLimitCount() {
		return dailyTrxLimitCount;
	}



	/**
	 * @param dailyTrxLimitCount the dailyTrxLimitCount to set
	 */
	public void setDailyTrxLimitCount(int dailyTrxLimitCount) {
		this.dailyTrxLimitCount = dailyTrxLimitCount;
	}



	/**
	 * @return the dailyCashLimitCount
	 */
	public int getDailyCashLimitCount() {
		return dailyCashLimitCount;
	}



	/**
	 * @param dailyCashLimitCount the dailyCashLimitCount to set
	 */
	public void setDailyCashLimitCount(int dailyCashLimitCount) {
		this.dailyCashLimitCount = dailyCashLimitCount;
	}



	/**
	 * @return the maxDailyTrxLimit
	 */
	public Double getMaxDailyTrxLimit() {
		return maxDailyTrxLimit;
	}



	/**
	 * @param maxDailyTrxLimit the maxDailyTrxLimit to set
	 */
	public void setMaxDailyTrxLimit(Double maxDailyTrxLimit) {
		this.maxDailyTrxLimit = maxDailyTrxLimit;
	}



	/**
	 * @return the maxDailyCashLimit
	 */
	public Double getMaxDailyCashLimit() {
		return maxDailyCashLimit;
	}



	/**
	 * @param maxDailyCashLimit the maxDailyCashLimit to set
	 */
	public void setMaxDailyCashLimit(Double maxDailyCashLimit) {
		this.maxDailyCashLimit = maxDailyCashLimit;
	}



	/**
	 * @return the maxDailyTrxLimitCount
	 */
	public int getMaxDailyTrxLimitCount() {
		return maxDailyTrxLimitCount;
	}



	/**
	 * @param maxDailyTrxLimitCount the maxDailyTrxLimitCount to set
	 */
	public void setMaxDailyTrxLimitCount(int maxDailyTrxLimitCount) {
		this.maxDailyTrxLimitCount = maxDailyTrxLimitCount;
	}



	/**
	 * @return the maxDailyCashLimitCount
	 */
	public int getMaxDailyCashLimitCount() {
		return maxDailyCashLimitCount;
	}



	/**
	 * @param maxDailyCashLimitCount the maxDailyCashLimitCount to set
	 */
	public void setMaxDailyCashLimitCount(int maxDailyCashLimitCount) {
		this.maxDailyCashLimitCount = maxDailyCashLimitCount;
	}



	/**
	 * @return the internetLimit
	 */
	public Double getInternetLimit() {
		return internetLimit;
	}



	/**
	 * @param internetLimit the internetLimit to set
	 */
	public void setInternetLimit(Double internetLimit) {
		this.internetLimit = internetLimit;
	}



	/**
	 * @return the motoLimit
	 */
	public Double getMotoLimit() {
		return motoLimit;
	}



	/**
	 * @param motoLimit the motoLimit to set
	 */
	public void setMotoLimit(Double motoLimit) {
		this.motoLimit = motoLimit;
	}



	/**
	 * @return the internetUseLimit
	 */
	public int getInternetUseLimit() {
		return internetUseLimit;
	}



	/**
	 * @param internetUseLimit the internetUseLimit to set
	 */
	public void setInternetUseLimit(int internetUseLimit) {
		this.internetUseLimit = internetUseLimit;
	}



	/**
	 * @return the motoUseLimit
	 */
	public int getMotoUseLimit() {
		return motoUseLimit;
	}



	/**
	 * @param motoUseLimit the motoUseLimit to set
	 */
	public void setMotoUseLimit(int motoUseLimit) {
		this.motoUseLimit = motoUseLimit;
	}



	/**
	 * @return the minPayment
	 */
	public Double getMinPayment() {
		return minPayment;
	}



	/**
	 * @param minPayment the minPayment to set
	 */
	public void setMinPayment(Double minPayment) {
		this.minPayment = minPayment;
	}



	/**
	 * @return the minPaymentDate
	 */
	public Date getMinPaymentDate() {
		return minPaymentDate;
	}



	/**
	 * @param minPaymentDate the minPaymentDate to set
	 */
	public void setMinPaymentDate(Date minPaymentDate) {
		this.minPaymentDate = minPaymentDate;
	}



	/**
	 * @return the limitInCycle
	 */
	public Double getLimitInCycle() {
		return limitInCycle;
	}



	/**
	 * @param limitInCycle the limitInCycle to set
	 */
	public void setLimitInCycle(Double limitInCycle) {
		this.limitInCycle = limitInCycle;
	}



	/**
	 * @return the debt
	 */
	public Double getDebt() {
		return debt;
	}



	/**
	 * @param debt the debt to set
	 */
	public void setDebt(Double debt) {
		this.debt = debt;
	}



	/**
	 * @return the debtPrevSettlmnt
	 */
	public Double getDebtPrevSettlmnt() {
		return debtPrevSettlmnt;
	}



	/**
	 * @param debtPrevSettlmnt the debtPrevSettlmnt to set
	 */
	public void setDebtPrevSettlmnt(Double debtPrevSettlmnt) {
		this.debtPrevSettlmnt = debtPrevSettlmnt;
	}



	/**
	 * @return the debtPrevUnsettled
	 */
	public Double getDebtPrevUnsettled() {
		return debtPrevUnsettled;
	}



	/**
	 * @param debtPrevUnsettled the debtPrevUnsettled to set
	 */
	public void setDebtPrevUnsettled(Double debtPrevUnsettled) {
		this.debtPrevUnsettled = debtPrevUnsettled;
	}



	/**
	 * @return the minDebtPrevSettlmnt
	 */
	public Double getMinDebtPrevSettlmnt() {
		return minDebtPrevSettlmnt;
	}



	/**
	 * @param minDebtPrevSettlmnt the minDebtPrevSettlmnt to set
	 */
	public void setMinDebtPrevSettlmnt(Double minDebtPrevSettlmnt) {
		this.minDebtPrevSettlmnt = minDebtPrevSettlmnt;
	}



	/**
	 * @return the billingCycle
	 */
	public String getBillingCycle() {
		return billingCycle;
	}



	/**
	 * @param billingCycle the billingCycle to set
	 */
	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}



	/**
	 * @return the cashTransactionsInterest
	 */
	public Double getCashTransactionsInterest() {
		return cashTransactionsInterest;
	}



	/**
	 * @param cashTransactionsInterest the cashTransactionsInterest to set
	 */
	public void setCashTransactionsInterest(Double cashTransactionsInterest) {
		this.cashTransactionsInterest = cashTransactionsInterest;
	}



	/**
	 * @return the nonCashTransactionsInterest
	 */
	public Double getNonCashTransactionsInterest() {
		return nonCashTransactionsInterest;
	}



	/**
	 * @param nonCashTransactionsInterest the nonCashTransactionsInterest to set
	 */
	public void setNonCashTransactionsInterest(Double nonCashTransactionsInterest) {
		this.nonCashTransactionsInterest = nonCashTransactionsInterest;
	}



	/**
	 * @return the accountLimit
	 */
	public Double getAccountLimit() {
		return accountLimit;
	}



	/**
	 * @param accountLimit the accountLimit to set
	 */
	public void setAccountLimit(Double accountLimit) {
		this.accountLimit = accountLimit;
	}



	/**
	 * @return the limitUsed
	 */
	public Double getLimitUsed() {
		return limitUsed;
	}



	/**
	 * @param limitUsed the limitUsed to set
	 */
	public void setLimitUsed(Double limitUsed) {
		this.limitUsed = limitUsed;
	}



	/**
	 * @return the minDebtPrevSettlmntPlain
	 */
	public Double getMinDebtPrevSettlmntPlain() {
		return minDebtPrevSettlmntPlain;
	}



	/**
	 * @param minDebtPrevSettlmntPlain the minDebtPrevSettlmntPlain to set
	 */
	public void setMinDebtPrevSettlmntPlain(Double minDebtPrevSettlmntPlain) {
		this.minDebtPrevSettlmntPlain = minDebtPrevSettlmntPlain;
	}



	/**
	 * @return the statementDistributionDesc
	 */
	public Double getStatementDistributionDesc() {
		return statementDistributionDesc;
	}



	/**
	 * @param statementDistributionDesc the statementDistributionDesc to set
	 */
	public void setStatementDistributionDesc(Double statementDistributionDesc) {
		this.statementDistributionDesc = statementDistributionDesc;
	}



	/**
	 * @return the installments
	 */
	public List<Map<String, ?>> getInstallments() {
		return installments;
	}



	/**
	 * @param installments the installments to set
	 */
	public void setInstallments(List<Map<String, ?>> installments) {
		this.installments = installments;
	}



	public CardDetailByIdDTO() {
		// TODO Auto-generated constructor stub
	}

}
