/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class CardStatementsDTO implements Serializable {
	private String id;
	private Double totalRepaymentAmount;
	private Double minimumRepaymentAmount;
	private Date cycleStart;
	private Date cycleEnd;
	private Date nextPaymentDate;
	private Double overDueAmount;
	private String cardAccountNumber;
	private String downloadLink;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the totalRepaymentAmount
	 */
	public Double getTotalRepaymentAmount() {
		return totalRepaymentAmount;
	}
	/**
	 * @param totalRepaymentAmount the totalRepaymentAmount to set
	 */
	public void setTotalRepaymentAmount(Double totalRepaymentAmount) {
		this.totalRepaymentAmount = totalRepaymentAmount;
	}
	/**
	 * @return the minimumRepaymentAmount
	 */
	public Double getMinimumRepaymentAmount() {
		return minimumRepaymentAmount;
	}
	/**
	 * @param minimumRepaymentAmount the minimumRepaymentAmount to set
	 */
	public void setMinimumRepaymentAmount(Double minimumRepaymentAmount) {
		this.minimumRepaymentAmount = minimumRepaymentAmount;
	}
	/**
	 * @return the cycleStart
	 */
	public Date getCycleStart() {
		return cycleStart;
	}
	/**
	 * @param cycleStart the cycleStart to set
	 */
	public void setCycleStart(Date cycleStart) {
		this.cycleStart = cycleStart;
	}
	/**
	 * @return the cycleEnd
	 */
	public Date getCycleEnd() {
		return cycleEnd;
	}
	/**
	 * @param cycleEnd the cycleEnd to set
	 */
	public void setCycleEnd(Date cycleEnd) {
		this.cycleEnd = cycleEnd;
	}
	/**
	 * @return the nextPaymentDate
	 */
	public Date getNextPaymentDate() {
		return nextPaymentDate;
	}
	/**
	 * @param nextPaymentDate the nextPaymentDate to set
	 */
	public void setNextPaymentDate(Date nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}
	/**
	 * @return the overDueAmount
	 */
	public Double getOverDueAmount() {
		return overDueAmount;
	}
	/**
	 * @param overDueAmount the overDueAmount to set
	 */
	public void setOverDueAmount(Double overDueAmount) {
		this.overDueAmount = overDueAmount;
	}
	/**
	 * @return the cardAccountNumber
	 */
	public String getCardAccountNumber() {
		return cardAccountNumber;
	}
	/**
	 * @param cardAccountNumber the cardAccountNumber to set
	 */
	public void setCardAccountNumber(String cardAccountNumber) {
		this.cardAccountNumber = cardAccountNumber;
	}
	/**
	 * @return the downloadLink
	 */
	public String getDownloadLink() {
		return downloadLink;
	}
	/**
	 * @param downloadLink the downloadLink to set
	 */
	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}
	public CardStatementsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CardStatementsDTO(String id, Double totalRepaymentAmount, Double minimumRepaymentAmount, Date cycleStart,
			Date cycleEnd, Date nextPaymentDate, Double overDueAmount, String cardAccountNumber, String downloadLink) {
		super();
		this.id = id;
		this.totalRepaymentAmount = totalRepaymentAmount;
		this.minimumRepaymentAmount = minimumRepaymentAmount;
		this.cycleStart = cycleStart;
		this.cycleEnd = cycleEnd;
		this.nextPaymentDate = nextPaymentDate;
		this.overDueAmount = overDueAmount;
		this.cardAccountNumber = cardAccountNumber;
		this.downloadLink = downloadLink;
	}

	
	
}
