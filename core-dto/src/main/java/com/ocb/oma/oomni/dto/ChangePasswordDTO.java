/**
 * 
 */
package com.ocb.oma.oomni.dto;

/**
 * @author phuhoang
 *
 */
public class ChangePasswordDTO {

	private boolean isCorrectOldPassword;
	private boolean isChanged;
	private String message;
	private String requiredActionAfterChangePassword;

	/**
	 * @return the isCorrectOldPassword
	 */
	public boolean isCorrectOldPassword() {
		return isCorrectOldPassword;
	}

	/**
	 * @param isCorrectOldPassword the isCorrectOldPassword to set
	 */
	public void setCorrectOldPassword(boolean isCorrectOldPassword) {
		this.isCorrectOldPassword = isCorrectOldPassword;
	}

	/**
	 * @return the isChanged
	 */
	public boolean isChanged() {
		return isChanged;
	}

	/**
	 * @param isChanged the isChanged to set
	 */
	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the requiredActionAfterChangePassword
	 */
	public String getRequiredActionAfterChangePassword() {
		return requiredActionAfterChangePassword;
	}

	/**
	 * @param requiredActionAfterChangePassword the
	 *                                          requiredActionAfterChangePassword to
	 *                                          set
	 */
	public void setRequiredActionAfterChangePassword(String requiredActionAfterChangePassword) {
		this.requiredActionAfterChangePassword = requiredActionAfterChangePassword;
	}

	public ChangePasswordDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ChangePasswordDTO(boolean isCorrectOldPassword, boolean isChanged, String message,
			String requiredActionAfterChangePassword) {
		super();
		this.isCorrectOldPassword = isCorrectOldPassword;
		this.isChanged = isChanged;
		this.message = message;
		this.requiredActionAfterChangePassword = requiredActionAfterChangePassword;
	}

}
