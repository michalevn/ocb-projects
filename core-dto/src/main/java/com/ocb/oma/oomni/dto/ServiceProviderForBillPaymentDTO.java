/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class ServiceProviderForBillPaymentDTO implements Serializable {

	private String providerCode;
	private String providerName;

	/**
	 * @return the providerCode
	 */
	public String getProviderCode() {
		return providerCode;
	}

	/**
	 * @param providerCode the providerCode to set
	 */
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	/**
	 * @return the providerName
	 */
	public String getProviderName() {
		return providerName;
	}

	/**
	 * @param providerName the providerName to set
	 */
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	/**
	 * @param providerCode
	 * @param providerName
	 */
	public ServiceProviderForBillPaymentDTO(String providerCode, String providerName) {
		super();
		this.providerCode = providerCode;
		this.providerName = providerName;
	}

	/**
	 * 
	 */
	public ServiceProviderForBillPaymentDTO() {
		// TODO Auto-generated constructor stub
	}
}
