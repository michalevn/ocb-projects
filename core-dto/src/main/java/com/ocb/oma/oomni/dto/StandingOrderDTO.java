package com.ocb.oma.oomni.dto;

import java.util.Date;

public class StandingOrderDTO {

	private Double productOwner;
	private String id;
	private String shortName;
	private String orderingCustomer;
	private String debitAccount;
	private String debitAccountName;
	private String[] beneficiary;
	private String creditAccount;
	private String creditAccountBankName;
	private Double amount;
	private String amountCalculation;
	private String currency;
	private String[] remarks;
	private String clearingNetwork;
	private Date startDate;
	private Date endDate;
	private FrequencyDTO frequency;
	private long numberOfPayments;
	private int retryCount;
	private int workingDay;
	private boolean activeFlag;
	private String hUnid;
	private String hRelatedProduct;
	private String hRelatedProductOwner;
	private String expectedProcessingDate;
	private String standingOrderReferenceId;
	private boolean alreadyDeleted;

	/**
	 * @return the productOwner
	 */
	public Double getProductOwner() {
		return productOwner;
	}

	/**
	 * @param productOwner the productOwner to set
	 */
	public void setProductOwner(Double productOwner) {
		this.productOwner = productOwner;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the orderingCustomer
	 */
	public String getOrderingCustomer() {
		return orderingCustomer;
	}

	/**
	 * @param orderingCustomer the orderingCustomer to set
	 */
	public void setOrderingCustomer(String orderingCustomer) {
		this.orderingCustomer = orderingCustomer;
	}

	/**
	 * @return the debitAccount
	 */
	public String getDebitAccount() {
		return debitAccount;
	}

	/**
	 * @param debitAccount the debitAccount to set
	 */
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	/**
	 * @return the debitAccountName
	 */
	public String getDebitAccountName() {
		return debitAccountName;
	}

	/**
	 * @param debitAccountName the debitAccountName to set
	 */
	public void setDebitAccountName(String debitAccountName) {
		this.debitAccountName = debitAccountName;
	}

	/**
	 * @return the beneficiary
	 */
	public String[] getBeneficiary() {
		return beneficiary;
	}

	/**
	 * @param beneficiary the beneficiary to set
	 */
	public void setBeneficiary(String[] beneficiary) {
		this.beneficiary = beneficiary;
	}

	/**
	 * @return the creditAccount
	 */
	public String getCreditAccount() {
		return creditAccount;
	}

	/**
	 * @param creditAccount the creditAccount to set
	 */
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	/**
	 * @return the creditAccountBankName
	 */
	public String getCreditAccountBankName() {
		return creditAccountBankName;
	}

	/**
	 * @param creditAccountBankName the creditAccountBankName to set
	 */
	public void setCreditAccountBankName(String creditAccountBankName) {
		this.creditAccountBankName = creditAccountBankName;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the amountCalculation
	 */
	public String getAmountCalculation() {
		return amountCalculation;
	}

	/**
	 * @param amountCalculation the amountCalculation to set
	 */
	public void setAmountCalculation(String amountCalculation) {
		this.amountCalculation = amountCalculation;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the remarks
	 */
	public String[] getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String[] remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the clearingNetwork
	 */
	public String getClearingNetwork() {
		return clearingNetwork;
	}

	/**
	 * @param clearingNetwork the clearingNetwork to set
	 */
	public void setClearingNetwork(String clearingNetwork) {
		this.clearingNetwork = clearingNetwork;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the frequency
	 */
	public FrequencyDTO getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency the frequency to set
	 */
	public void setFrequency(FrequencyDTO frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return the numberOfPayments
	 */
	public long getNumberOfPayments() {
		return numberOfPayments;
	}

	/**
	 * @param numberOfPayments the numberOfPayments to set
	 */
	public void setNumberOfPayments(long numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}

	/**
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return retryCount;
	}

	/**
	 * @param retryCount the retryCount to set
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * @return the workingDay
	 */
	public int getWorkingDay() {
		return workingDay;
	}

	/**
	 * @param workingDay the workingDay to set
	 */
	public void setWorkingDay(int workingDay) {
		this.workingDay = workingDay;
	}

	/**
	 * @return the activeFlag
	 */
	public boolean isActiveFlag() {
		return activeFlag;
	}

	/**
	 * @param activeFlag the activeFlag to set
	 */
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	/**
	 * @return the hUnid
	 */
	public String gethUnid() {
		return hUnid;
	}

	/**
	 * @param hUnid the hUnid to set
	 */
	public void sethUnid(String hUnid) {
		this.hUnid = hUnid;
	}

	/**
	 * @return the hRelatedProduct
	 */
	public String gethRelatedProduct() {
		return hRelatedProduct;
	}

	/**
	 * @param hRelatedProduct the hRelatedProduct to set
	 */
	public void sethRelatedProduct(String hRelatedProduct) {
		this.hRelatedProduct = hRelatedProduct;
	}

	/**
	 * @return the hRelatedProductOwner
	 */
	public String gethRelatedProductOwner() {
		return hRelatedProductOwner;
	}

	/**
	 * @param hRelatedProductOwner the hRelatedProductOwner to set
	 */
	public void sethRelatedProductOwner(String hRelatedProductOwner) {
		this.hRelatedProductOwner = hRelatedProductOwner;
	}

	/**
	 * @return the expectedProcessingDate
	 */
	public String getExpectedProcessingDate() {
		return expectedProcessingDate;
	}

	/**
	 * @param expectedProcessingDate the expectedProcessingDate to set
	 */
	public void setExpectedProcessingDate(String expectedProcessingDate) {
		this.expectedProcessingDate = expectedProcessingDate;
	}

	/**
	 * @return the standingOrderReferenceId
	 */
	public String getStandingOrderReferenceId() {
		return standingOrderReferenceId;
	}

	/**
	 * @param standingOrderReferenceId the standingOrderReferenceId to set
	 */
	public void setStandingOrderReferenceId(String standingOrderReferenceId) {
		this.standingOrderReferenceId = standingOrderReferenceId;
	}

	/**
	 * @return the alreadyDeleted
	 */
	public boolean isAlreadyDeleted() {
		return alreadyDeleted;
	}

	/**
	 * @param alreadyDeleted the alreadyDeleted to set
	 */
	public void setAlreadyDeleted(boolean alreadyDeleted) {
		this.alreadyDeleted = alreadyDeleted;
	}

	public StandingOrderDTO() {
		// TODO Auto-generated constructor stub
	}

	public StandingOrderDTO(Double productOwner, String id, String shortName, String orderingCustomer,
			String debitAccount, String debitAccountName, String[] beneficiary, String creditAccount,
			String creditAccountBankName, Double amount, String amountCalculation, String currency, String[] remarks,
			String clearingNetwork, Date startDate, Date endDate, FrequencyDTO frequency, long numberOfPayments,
			int retryCount, int workingDay, boolean activeFlag, String hUnid, String hRelatedProduct,
			String hRelatedProductOwner, String expectedProcessingDate, String standingOrderReferenceId,
			boolean alreadyDeleted) {
		super();
		this.productOwner = productOwner;
		this.id = id;
		this.shortName = shortName;
		this.orderingCustomer = orderingCustomer;
		this.debitAccount = debitAccount;
		this.debitAccountName = debitAccountName;
		this.beneficiary = beneficiary;
		this.creditAccount = creditAccount;
		this.creditAccountBankName = creditAccountBankName;
		this.amount = amount;
		this.amountCalculation = amountCalculation;
		this.currency = currency;
		this.remarks = remarks;
		this.clearingNetwork = clearingNetwork;
		this.startDate = startDate;
		this.endDate = endDate;
		this.frequency = frequency;
		this.numberOfPayments = numberOfPayments;
		this.retryCount = retryCount;
		this.workingDay = workingDay;
		this.activeFlag = activeFlag;
		this.hUnid = hUnid;
		this.hRelatedProduct = hRelatedProduct;
		this.hRelatedProductOwner = hRelatedProductOwner;
		this.expectedProcessingDate = expectedProcessingDate;
		this.standingOrderReferenceId = standingOrderReferenceId;
		this.alreadyDeleted = alreadyDeleted;
	}

}
