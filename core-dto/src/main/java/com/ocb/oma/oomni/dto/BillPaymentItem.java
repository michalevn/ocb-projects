/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class BillPaymentItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7363944430437573995L;
	private String orderId;
	private String description;
	private String fromDate;
	private String toDate;
	private String formulaRates;
	private String billCodeItemNo;
	private String productType;
	private Double amountMonth;
	private String customerCode;
	private String customerName;
	private Integer qty;
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the formulaRates
	 */
	public String getFormulaRates() {
		return formulaRates;
	}
	/**
	 * @param formulaRates the formulaRates to set
	 */
	public void setFormulaRates(String formulaRates) {
		this.formulaRates = formulaRates;
	}
	/**
	 * @return the billCodeItemNo
	 */
	public String getBillCodeItemNo() {
		return billCodeItemNo;
	}
	/**
	 * @param billCodeItemNo the billCodeItemNo to set
	 */
	public void setBillCodeItemNo(String billCodeItemNo) {
		this.billCodeItemNo = billCodeItemNo;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * @return the amountMonth
	 */
	public Double getAmountMonth() {
		return amountMonth;
	}
	/**
	 * @param amountMonth the amountMonth to set
	 */
	public void setAmountMonth(Double amountMonth) {
		this.amountMonth = amountMonth;
	}
	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}
	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the qty
	 */
	public Integer getQty() {
		return qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	/**
	 * 
	 */
	public BillPaymentItem() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param orderId
	 * @param description
	 * @param fromDate
	 * @param toDate
	 * @param formulaRates
	 * @param billCodeItemNo
	 * @param productType
	 * @param amountMonth
	 * @param customerCode
	 * @param customerName
	 * @param qty
	 */
	public BillPaymentItem(String orderId, String description, String fromDate, String toDate, String formulaRates,
			String billCodeItemNo, String productType, Double amountMonth, String customerCode, String customerName,
			Integer qty) {
		super();
		this.orderId = orderId;
		this.description = description;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.formulaRates = formulaRates;
		this.billCodeItemNo = billCodeItemNo;
		this.productType = productType;
		this.amountMonth = amountMonth;
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.qty = qty;
	}
	
 
}
