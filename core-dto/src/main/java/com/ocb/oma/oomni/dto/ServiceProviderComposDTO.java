/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class ServiceProviderComposDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -297191039984169655L;
	private ServiceForBillPaymentDTO service;
	private List<ServiceProviderForBillPaymentDTO> providers;

	/**
	 * @return the service
	 */
	public ServiceForBillPaymentDTO getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(ServiceForBillPaymentDTO service) {
		this.service = service;
	}

	/**
	 * @return the providers
	 */
	public List<ServiceProviderForBillPaymentDTO> getProviders() {
		return providers;
	}

	/**
	 * @param providers the providers to set
	 */
	public void setProviders(List<ServiceProviderForBillPaymentDTO> providers) {
		this.providers = providers;
	}

}
