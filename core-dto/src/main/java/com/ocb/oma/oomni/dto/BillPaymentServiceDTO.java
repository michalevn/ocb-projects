/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author phuhoang
 *
 */
public class BillPaymentServiceDTO implements Serializable {

	private static final long serialVersionUID = -6817398392350741939L;

	private String billCode;
	private String customerName;
	private String customerCode;
	private String address;
	private String phoneNumber;
	private String meterNumber;
	private String paymentType;
	private Double amount;
	private ServiceForBillPaymentDTO service;
	private ServiceProviderForBillPaymentDTO provider;
	private List<BillPaymentItem> billItems;

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the service
	 */
	public ServiceForBillPaymentDTO getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(ServiceForBillPaymentDTO service) {
		this.service = service;
	}

	/**
	 * @return the provider
	 */
	public ServiceProviderForBillPaymentDTO getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(ServiceProviderForBillPaymentDTO provider) {
		this.provider = provider;
	}

	/**
	 * @return the billItems
	 */
	public List<BillPaymentItem> getBillItems() {
		return billItems;
	}

	/**
	 * @param billItems the billItems to set
	 */
	public void setBillItems(List<BillPaymentItem> billItems) {
		this.billItems = billItems;
	}

	/**
	 * @return the billCode
	 */
	public String getBillCode() {
		return billCode;
	}

	/**
	 * @param billCode the billCode to set
	 */
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber(String phoneNumber) {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the meterNumber
	 */
	public String getMeterNumber() {
		return meterNumber;
	}

	/**
	 * @param meterNumber the meterNumber to set
	 */
	public void setMeterNumber(String meterNumber) {
		this.meterNumber = meterNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param billCode
	 * @param billType
	 * @param customerName
	 * @param customerCode
	 * @param address
	 * @param phoneNumber
	 * @param meterNumber
	 * @param amount
	 * @param billPaymentType
	 * @param billSourceData
	 * @param service
	 * @param provider
	 * @param billItems
	 */
	public BillPaymentServiceDTO(String billCode, String billType, String customerName, String customerCode,
			String address, String phoneNumber, String meterNumber, Double amount, String billPaymentType,
			String billSourceData, ServiceForBillPaymentDTO service, ServiceProviderForBillPaymentDTO provider,
			List<BillPaymentItem> billItems) {
		super();
		this.billCode = billCode;
		this.customerName = customerName;
		this.customerCode = customerCode;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.meterNumber = meterNumber;
		this.amount = amount;
		this.service = service;
		this.provider = provider;
		this.billItems = billItems;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public BillPaymentServiceDTO() {
	}

}
