/**
 * 
 */
package com.ocb.oma.oomni.dto;

/**
 * @author phuhoang
 *
 */
public class OldTransactionInfo {
	private String cRefNum;
	private String userId;
	private String clientCode ="OMNI";
	private String branchCode ="mobilebanking";
	/**
	 * @return the cRefNum
	 */
	public String getcRefNum() {
		return cRefNum;
	}
	/**
	 * @param cRefNum the cRefNum to set
	 */
	public void setcRefNum(String cRefNum) {
		this.cRefNum = cRefNum;
	}
	 
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the clientCode
	 */
	public String getClientCode() {
		return clientCode;
	}
	/**
	 * @param clientCode the clientCode to set
	 */
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	/**
	 * @return the branchCode
	 */
	public String getBranchCode() {
		return branchCode;
	}
	/**
	 * @param branchCode the branchCode to set
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
     /**
	 * 
	 */
	public OldTransactionInfo() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param cRefNum
	 * @param userId
	 */
	public OldTransactionInfo(String cRefNum, String userId) {
		super();
		this.cRefNum = cRefNum;
		this.userId = userId;
	}
	
	
}
