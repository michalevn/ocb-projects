package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CardAccountEntryDTO implements Serializable {

	private static final long serialVersionUID = 823984038986570184L;

	private String id;
	private Date bookingDate;
	private Date transactionDate;
	private String embossedName;
	private String embossedCompanyName;
	private String transactionType;
	private BigDecimal amount;
	private String currency;
	private String transactionNameLocation;
	private String counterParty;
	private String cardNo;
	private String cardAccount;
	private String originalCurrency;
	private BigDecimal amountInOriginalCurrency;
	private String details;
	private String fullName;
	private String organizationExchangeRate;
	private String bankExchangeRate;
	private String organizationExchangeRateValue;
	private String bankExchangeRateValue;
	private BigDecimal balanceAfter;
	private Boolean detailsAvailable;
	private String billingType;

	private String accountNumber;
	private String cardId;
	private Double originalAmount;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmbossedName() {
		return embossedName;
	}

	public void setEmbossedName(String embossedName) {
		this.embossedName = embossedName;
	}

	public String getEmbossedCompanyName() {
		return embossedCompanyName;
	}

	public void setEmbossedCompanyName(String embossedCompanyName) {
		this.embossedCompanyName = embossedCompanyName;
	}

	public String getTransactionNameLocation() {
		return transactionNameLocation;
	}

	public void setTransactionNameLocation(String transactionNameLocation) {
		this.transactionNameLocation = transactionNameLocation;
	}

	public String getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(String counterParty) {
		this.counterParty = counterParty;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardAccount() {
		return cardAccount;
	}

	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
		this.accountNumber = cardAccount;
	}

	public BigDecimal getAmountInOriginalCurrency() {
		return amountInOriginalCurrency;
	}

	public void setAmountInOriginalCurrency(BigDecimal amountInOriginalCurrency) {
		this.amountInOriginalCurrency = amountInOriginalCurrency;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOrganizationExchangeRate() {
		return organizationExchangeRate;
	}

	public void setOrganizationExchangeRate(String organizationExchangeRate) {
		this.organizationExchangeRate = organizationExchangeRate;
	}

	public String getBankExchangeRate() {
		return bankExchangeRate;
	}

	public void setBankExchangeRate(String bankExchangeRate) {
		this.bankExchangeRate = bankExchangeRate;
	}

	public String getOrganizationExchangeRateValue() {
		return organizationExchangeRateValue;
	}

	public void setOrganizationExchangeRateValue(String organizationExchangeRateValue) {
		this.organizationExchangeRateValue = organizationExchangeRateValue;
	}

	public String getBankExchangeRateValue() {
		return bankExchangeRateValue;
	}

	public void setBankExchangeRateValue(String bankExchangeRateValue) {
		this.bankExchangeRateValue = bankExchangeRateValue;
	}

	public BigDecimal getBalanceAfter() {
		return balanceAfter;
	}

	public void setBalanceAfter(BigDecimal balanceAfter) {
		this.balanceAfter = balanceAfter;
	}

	public Boolean getDetailsAvailable() {
		return detailsAvailable;
	}

	public void setDetailsAvailable(Boolean detailsAvailable) {
		this.detailsAvailable = detailsAvailable;
	}

	public String getBillingType() {
		return billingType;
	}

	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}

}
