/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class PaymentItemStudentFeeDTO implements Serializable {

	private static final long serialVersionUID = -70331224139095658L;
	
	private Double amount;
	private Double discount;
	private Date dueDate;
	private Integer isrequired;
	private String itemNameDescription;
	private String itemTypeFeeType;
	private String paymentItemFeeCode;

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the discount
	 */
	public Double getDiscount() {
		return discount;
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return the isrequired
	 */
	public Integer getIsrequired() {
		return isrequired;
	}

	/**
	 * @param isrequired the isrequired to set
	 */
	public void setIsrequired(Integer isrequired) {
		this.isrequired = isrequired;
	}

	/**
	 * @return the itemNameDescription
	 */
	public String getItemNameDescription() {
		return itemNameDescription;
	}

	/**
	 * @param itemNameDescription the itemNameDescription to set
	 */
	public void setItemNameDescription(String itemNameDescription) {
		this.itemNameDescription = itemNameDescription;
	}

	/**
	 * @return the itemTypeFeeType
	 */
	public String getItemTypeFeeType() {
		return itemTypeFeeType;
	}

	/**
	 * @param itemTypeFeeType the itemTypeFeeType to set
	 */
	public void setItemTypeFeeType(String itemTypeFeeType) {
		this.itemTypeFeeType = itemTypeFeeType;
	}

	/**
	 * @return the paymentItemFeeCode
	 */
	public String getPaymentItemFeeCode() {
		return paymentItemFeeCode;
	}

	/**
	 * @param paymentItemFeeCode the paymentItemFeeCode to set
	 */
	public void setPaymentItemFeeCode(String paymentItemFeeCode) {
		this.paymentItemFeeCode = paymentItemFeeCode;
	}

}
