package com.ocb.oma.oomni;

public class ChangeUsernameOutput {
	private String status;
	private String authErrorCause;
	private String errorMsg;
	private String errorCode;
	private Boolean success;
	
	

	public ChangeUsernameOutput() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}



	/**
	 * @return the authErrorCause
	 */
	public String getAuthErrorCause() {
		return authErrorCause;
	}



	/**
	 * @param authErrorCause the authErrorCause to set
	 */
	public void setAuthErrorCause(String authErrorCause) {
		this.authErrorCause = authErrorCause;
	}



	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}



	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}



	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}



	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}



	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}



	/**
	 * @param success the success to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

}
