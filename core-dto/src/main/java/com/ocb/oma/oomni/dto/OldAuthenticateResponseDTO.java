/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.util.Date;

/**
 * @author phuhoang
 *
 */
public class OldAuthenticateResponseDTO {

	private boolean credentialsCorrect;
	private String jwtToken;
	private String customerId; // cif
	private String firstName;
	private String lastName;
	private Date lastLoginDate;
	private String authorizationMethod;
	private String smsOTPMobileNumber;
	private Boolean changePasswordRequired;
	
	
	
	

	/**
	 * @return the changePasswordRequired
	 */
	public Boolean getChangePasswordRequired() {
		return changePasswordRequired;
	}

	/**
	 * @param changePasswordRequired the changePasswordRequired to set
	 */
	public void setChangePasswordRequired(Boolean changePasswordRequired) {
		this.changePasswordRequired = changePasswordRequired;
	}

	/**
	 * @return the authorizationMethod
	 */
	public String getAuthorizationMethod() {
		return authorizationMethod;
	}

	/**
	 * @param authorizationMethod the authorizationMethod to set
	 */
	public void setAuthorizationMethod(String authorizationMethod) {
		this.authorizationMethod = authorizationMethod;
	}

	/**
	 * @return the smsOTPMobileNumber
	 */
	public String getSmsOTPMobileNumber() {
		return smsOTPMobileNumber;
	}

	/**
	 * @param smsOTPMobileNumber the smsOTPMobileNumber to set
	 */
	public void setSmsOTPMobileNumber(String smsOTPMobileNumber) {
		this.smsOTPMobileNumber = smsOTPMobileNumber;
	}

	/**
	 * @return the credentialsCorrect
	 */
	public boolean isCredentialsCorrect() {
		return credentialsCorrect;
	}

	/**
	 * @param credentialsCorrect the credentialsCorrect to set
	 */
	public void setCredentialsCorrect(boolean credentialsCorrect) {
		this.credentialsCorrect = credentialsCorrect;
	}

	/**
	 * @return the jwtToken
	 */
	public String getJwtToken() {
		return jwtToken;
	}

	/**
	 * @param jwtToken the jwtToken to set
	 */
	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the lastLoginDate
	 */
	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * @param lastLoginDate the lastLoginDate to set
	 */
	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * 
	 */
	public OldAuthenticateResponseDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param credentialsCorrect
	 * @param jwtToken
	 * @param customerId
	 * @param firstName
	 * @param lastName
	 * @param lastLoginDate
	 * @param authorizationMethod
	 * @param smsOTPMobileNumber
	 */
	public OldAuthenticateResponseDTO(boolean credentialsCorrect, String jwtToken, String customerId, String firstName,
			String lastName, Date lastLoginDate, String authorizationMethod, String smsOTPMobileNumber) {
		super();
		this.credentialsCorrect = credentialsCorrect;
		this.jwtToken = jwtToken;
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastLoginDate = lastLoginDate;
		this.authorizationMethod = authorizationMethod;
		this.smsOTPMobileNumber = smsOTPMobileNumber;
	}

	 
}
