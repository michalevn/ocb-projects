package com.ocb.oma.oomni.dto;

public class QRcodeInfoOutputDTO {
	private String resultCode;
	private String resultMsg;
	private BillInfoDTO billInfo;
	

	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}



	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}



	/**
	 * @return the resultMsg
	 */
	public String getResultMsg() {
		return resultMsg;
	}



	/**
	 * @param resultMsg the resultMsg to set
	 */
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}



	/**
	 * @return the billInfo
	 */
	public BillInfoDTO getBillInfo() {
		return billInfo;
	}



	/**
	 * @param billInfo the billInfo to set
	 */
	public void setBillInfo(BillInfoDTO billInfo) {
		this.billInfo = billInfo;
	}



	public QRcodeInfoOutputDTO() {
		// TODO Auto-generated constructor stub
	}

}
