package com.ocb.oma.oomni.dto;

public class SendGiftCardOutputDTO {
	private PostPaymentOutputDTO postPaymentOutputDTO;
	// nguoi gui
	private String debitAccountId;
	// so tai khoan nguoi nhan
	private String beneficiary;

	public PostPaymentOutputDTO getPostPaymentOutputDTO() {
		return postPaymentOutputDTO;
	}

	public void setPostPaymentOutputDTO(PostPaymentOutputDTO postPaymentOutputDTO) {
		this.postPaymentOutputDTO = postPaymentOutputDTO;
	}

	public String getDebitAccountId() {
		return debitAccountId;
	}

	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

}
