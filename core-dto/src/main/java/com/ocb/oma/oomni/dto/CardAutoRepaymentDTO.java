package com.ocb.oma.oomni.dto;

public class CardAutoRepaymentDTO {

	private String id;
	private String accountId;
	private String accountNo;
	private String accountName;
	private String cardAccountId;
	private String cardAccountName;
	private String cardAccountNo;
	private String cardNo;
	private String cardId;
	private String cardRepaymentType;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}
	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}
	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	/**
	 * @return the cardAccountId
	 */
	public String getCardAccountId() {
		return cardAccountId;
	}
	/**
	 * @param cardAccountId the cardAccountId to set
	 */
	public void setCardAccountId(String cardAccountId) {
		this.cardAccountId = cardAccountId;
	}
	/**
	 * @return the cardAccountName
	 */
	public String getCardAccountName() {
		return cardAccountName;
	}
	/**
	 * @param cardAccountName the cardAccountName to set
	 */
	public void setCardAccountName(String cardAccountName) {
		this.cardAccountName = cardAccountName;
	}
	/**
	 * @return the cardAccountNo
	 */
	public String getCardAccountNo() {
		return cardAccountNo;
	}
	/**
	 * @param cardAccountNo the cardAccountNo to set
	 */
	public void setCardAccountNo(String cardAccountNo) {
		this.cardAccountNo = cardAccountNo;
	}
	/**
	 * @return the cardNo
	 */
	public String getCardNo() {
		return cardNo;
	}
	/**
	 * @param cardNo the cardNo to set
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}
	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	/**
	 * @return the cardRepaymentType
	 */
	public String getCardRepaymentType() {
		return cardRepaymentType;
	}
	/**
	 * @param cardRepaymentType the cardRepaymentType to set
	 */
	public void setCardRepaymentType(String cardRepaymentType) {
		this.cardRepaymentType = cardRepaymentType;
	}
	public CardAutoRepaymentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CardAutoRepaymentDTO(String id, String accountId, String accountNo, String accountName, String cardAccountId,
			String cardAccountName, String cardAccountNo, String cardNo, String cardId, String cardRepaymentType) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.accountNo = accountNo;
		this.accountName = accountName;
		this.cardAccountId = cardAccountId;
		this.cardAccountName = cardAccountName;
		this.cardAccountNo = cardAccountNo;
		this.cardNo = cardNo;
		this.cardId = cardId;
		this.cardRepaymentType = cardRepaymentType;
	}

	
}
