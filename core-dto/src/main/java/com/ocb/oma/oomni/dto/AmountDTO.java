package com.ocb.oma.oomni.dto;

public class AmountDTO {
	private Double value;
	

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}


	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}


	public AmountDTO() {
		// TODO Auto-generated constructor stub
	}

}
