package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ocb.oma.dto.OwnersDTO;

public class AccountCapitalDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountId;
	private String productType;
	private String accountName;
	private String accountNo;
	private Double accessibleAssets;
	private Double currentBalance;
	private Double allowedLimit;
	private String allowedLimitCurrency;
	private String description;
	private Date openDate;
	private Double blockAssets;
	private Double arrearAmount;
	private String ownerName;
	private Double accountInterest;
	private Double creditInterest;
	private String currency;
	private String settlementPeriodDtType;
	private String settlementPeriodCtType;
	private long settlementPeriodDt;
	private long settlementPeriodCt;
	private Date nextCapitalizationDtDate;
	private Date nextCapitalizationCtDate;
	private Date lastTransactionBookingDate;
	private List<Map<String, ?>> accountStatementDetails;
	private List<OwnersDTO> ownersList;
	private Integer category;
	private String subProduct;
	private String relation;
	private String statementDistributionType;
	private String[] actions;
	private String[] accountCategories;
	private boolean accountRestrictFlag;
	private String[] destAccountRestrictions;
	private String ownerContext;
	private String accountModifyStatus;
	private boolean executiveRestriction;
	private long ownerGlobusId;
	private String openBranch;
	private String customName;
	private String customIcon;

	public AccountCapitalDTO(String accountId, String productType, String accountName, String accountNo,
			Double accessibleAssets, Double currentBalance, Double allowedLimit, String allowedLimitCurrency,
			String description, Date openDate, Double blockAssets, Double arrearAmount, String ownerName,
			Double accountInterest, Double creditInterest, String currency, String settlementPeriodDtType,
			String settlementPeriodCtType, long settlementPeriodDt, long settlementPeriodCt,
			Date nextCapitalizationDtDate, Date nextCapitalizationCtDate, Date lastTransactionBookingDate,
			List<Map<String, ?>> accountStatementDetails, List<OwnersDTO> ownersList, Integer category, String subProduct,
			String relation, String statementDistributionType, String[] actions, String[] accountCategories,
			boolean accountRestrictFlag, String[] destAccountRestrictions, String ownerContext,
			String accountModifyStatus, boolean executiveRestriction, long ownerGlobusId, String openBranch,
			String customName, String customIcon) {
		super();
		this.accountId = accountId;
		this.productType = productType;
		this.accountName = accountName;
		this.accountNo = accountNo;
		this.accessibleAssets = accessibleAssets;
		this.currentBalance = currentBalance;
		this.allowedLimit = allowedLimit;
		this.allowedLimitCurrency = allowedLimitCurrency;
		this.description = description;
		this.openDate = openDate;
		this.blockAssets = blockAssets;
		this.arrearAmount = arrearAmount;
		this.ownerName = ownerName;
		this.accountInterest = accountInterest;
		this.creditInterest = creditInterest;
		this.currency = currency;
		this.settlementPeriodDtType = settlementPeriodDtType;
		this.settlementPeriodCtType = settlementPeriodCtType;
		this.settlementPeriodDt = settlementPeriodDt;
		this.settlementPeriodCt = settlementPeriodCt;
		this.nextCapitalizationDtDate = nextCapitalizationDtDate;
		this.nextCapitalizationCtDate = nextCapitalizationCtDate;
		this.lastTransactionBookingDate = lastTransactionBookingDate;
		this.accountStatementDetails = accountStatementDetails;
		this.ownersList = ownersList;
		this.category = category;
		this.subProduct = subProduct;
		this.relation = relation;
		this.statementDistributionType = statementDistributionType;
		this.actions = actions;
		this.accountCategories = accountCategories;
		this.accountRestrictFlag = accountRestrictFlag;
		this.destAccountRestrictions = destAccountRestrictions;
		this.ownerContext = ownerContext;
		this.accountModifyStatus = accountModifyStatus;
		this.executiveRestriction = executiveRestriction;
		this.ownerGlobusId = ownerGlobusId;
		this.openBranch = openBranch;
		this.customName = customName;
		this.customIcon = customIcon;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName the accountName to set
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the accessibleAssets
	 */
	public Double getAccessibleAssets() {
		return accessibleAssets;
	}

	/**
	 * @param accessibleAssets the accessibleAssets to set
	 */
	public void setAccessibleAssets(Double accessibleAssets) {
		this.accessibleAssets = accessibleAssets;
	}

	/**
	 * @return the currentBalance
	 */
	public Double getCurrentBalance() {
		return currentBalance;
	}

	/**
	 * @param currentBalance the currentBalance to set
	 */
	public void setCurrentBalance(Double currentBalance) {
		this.currentBalance = currentBalance;
	}

	/**
	 * @return the allowedLimit
	 */
	public Double getAllowedLimit() {
		return allowedLimit;
	}

	/**
	 * @param allowedLimit the allowedLimit to set
	 */
	public void setAllowedLimit(Double allowedLimit) {
		this.allowedLimit = allowedLimit;
	}

	/**
	 * @return the allowedLimitCurrency
	 */
	public String getAllowedLimitCurrency() {
		return allowedLimitCurrency;
	}

	/**
	 * @param allowedLimitCurrency the allowedLimitCurrency to set
	 */
	public void setAllowedLimitCurrency(String allowedLimitCurrency) {
		this.allowedLimitCurrency = allowedLimitCurrency;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the openDate
	 */
	public Date getOpenDate() {
		return openDate;
	}

	/**
	 * @param openDate the openDate to set
	 */
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	/**
	 * @return the blockAssets
	 */
	public Double getBlockAssets() {
		return blockAssets;
	}

	/**
	 * @param blockAssets the blockAssets to set
	 */
	public void setBlockAssets(Double blockAssets) {
		this.blockAssets = blockAssets;
	}

	/**
	 * @return the arrearAmount
	 */
	public Double getArrearAmount() {
		return arrearAmount;
	}

	/**
	 * @param arrearAmount the arrearAmount to set
	 */
	public void setArrearAmount(Double arrearAmount) {
		this.arrearAmount = arrearAmount;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the accountInterest
	 */
	public Double getAccountInterest() {
		return accountInterest;
	}

	/**
	 * @param accountInterest the accountInterest to set
	 */
	public void setAccountInterest(Double accountInterest) {
		this.accountInterest = accountInterest;
	}

	/**
	 * @return the creditInterest
	 */
	public Double getCreditInterest() {
		return creditInterest;
	}

	/**
	 * @param creditInterest the creditInterest to set
	 */
	public void setCreditInterest(Double creditInterest) {
		this.creditInterest = creditInterest;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the settlementPeriodDtType
	 */
	public String getSettlementPeriodDtType() {
		return settlementPeriodDtType;
	}

	/**
	 * @param settlementPeriodDtType the settlementPeriodDtType to set
	 */
	public void setSettlementPeriodDtType(String settlementPeriodDtType) {
		this.settlementPeriodDtType = settlementPeriodDtType;
	}

	/**
	 * @return the settlementPeriodCtType
	 */
	public String getSettlementPeriodCtType() {
		return settlementPeriodCtType;
	}

	/**
	 * @param settlementPeriodCtType the settlementPeriodCtType to set
	 */
	public void setSettlementPeriodCtType(String settlementPeriodCtType) {
		this.settlementPeriodCtType = settlementPeriodCtType;
	}

	/**
	 * @return the settlementPeriodDt
	 */
	public long getSettlementPeriodDt() {
		return settlementPeriodDt;
	}

	/**
	 * @param settlementPeriodDt the settlementPeriodDt to set
	 */
	public void setSettlementPeriodDt(long settlementPeriodDt) {
		this.settlementPeriodDt = settlementPeriodDt;
	}

	/**
	 * @return the settlementPeriodCt
	 */
	public long getSettlementPeriodCt() {
		return settlementPeriodCt;
	}

	/**
	 * @param settlementPeriodCt the settlementPeriodCt to set
	 */
	public void setSettlementPeriodCt(long settlementPeriodCt) {
		this.settlementPeriodCt = settlementPeriodCt;
	}

	/**
	 * @return the nextCapitalizationDtDate
	 */
	public Date getNextCapitalizationDtDate() {
		return nextCapitalizationDtDate;
	}

	/**
	 * @param nextCapitalizationDtDate the nextCapitalizationDtDate to set
	 */
	public void setNextCapitalizationDtDate(Date nextCapitalizationDtDate) {
		this.nextCapitalizationDtDate = nextCapitalizationDtDate;
	}

	/**
	 * @return the nextCapitalizationCtDate
	 */
	public Date getNextCapitalizationCtDate() {
		return nextCapitalizationCtDate;
	}

	/**
	 * @param nextCapitalizationCtDate the nextCapitalizationCtDate to set
	 */
	public void setNextCapitalizationCtDate(Date nextCapitalizationCtDate) {
		this.nextCapitalizationCtDate = nextCapitalizationCtDate;
	}

	/**
	 * @return the lastTransactionBookingDate
	 */
	public Date getLastTransactionBookingDate() {
		return lastTransactionBookingDate;
	}

	/**
	 * @param lastTransactionBookingDate the lastTransactionBookingDate to set
	 */
	public void setLastTransactionBookingDate(Date lastTransactionBookingDate) {
		this.lastTransactionBookingDate = lastTransactionBookingDate;
	}

	/**
	 * @return the accountStatementDetails
	 */
	public List<Map<String, ?>> getAccountStatementDetails() {
		return accountStatementDetails;
	}

	/**
	 * @param accountStatementDetails the accountStatementDetails to set
	 */
	public void setAccountStatementDetails(List<Map<String, ?>> accountStatementDetails) {
		this.accountStatementDetails = accountStatementDetails;
	}

	/**
	 * @return the ownersList
	 */
	public List<OwnersDTO> getOwnersList() {
		return ownersList;
	}

	/**
	 * @param ownersList the ownersList to set
	 */
	public void setOwnersList(List<OwnersDTO> ownersList) {
		this.ownersList = ownersList;
	}

	/**
	 * @return the category
	 */
	public Integer getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(Integer category) {
		this.category = category;
	}

	/**
	 * @return the subProduct
	 */
	public String getSubProduct() {
		return subProduct;
	}

	/**
	 * @param subProduct the subProduct to set
	 */
	public void setSubProduct(String subProduct) {
		this.subProduct = subProduct;
	}

	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return the statementDistributionType
	 */
	public String getStatementDistributionType() {
		return statementDistributionType;
	}

	/**
	 * @param statementDistributionType the statementDistributionType to set
	 */
	public void setStatementDistributionType(String statementDistributionType) {
		this.statementDistributionType = statementDistributionType;
	}

	/**
	 * @return the actions
	 */
	public String[] getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(String[] actions) {
		this.actions = actions;
	}

	/**
	 * @return the accountCategories
	 */
	public String[] getAccountCategories() {
		return accountCategories;
	}

	/**
	 * @param accountCategories the accountCategories to set
	 */
	public void setAccountCategories(String[] accountCategories) {
		this.accountCategories = accountCategories;
	}

	/**
	 * @return the accountRestrictFlag
	 */
	public boolean isAccountRestrictFlag() {
		return accountRestrictFlag;
	}

	/**
	 * @param accountRestrictFlag the accountRestrictFlag to set
	 */
	public void setAccountRestrictFlag(boolean accountRestrictFlag) {
		this.accountRestrictFlag = accountRestrictFlag;
	}

	/**
	 * @return the destAccountRestrictions
	 */
	public String[] getDestAccountRestrictions() {
		return destAccountRestrictions;
	}

	/**
	 * @param destAccountRestrictions the destAccountRestrictions to set
	 */
	public void setDestAccountRestrictions(String[] destAccountRestrictions) {
		this.destAccountRestrictions = destAccountRestrictions;
	}

	/**
	 * @return the ownerContext
	 */
	public String getOwnerContext() {
		return ownerContext;
	}

	/**
	 * @param ownerContext the ownerContext to set
	 */
	public void setOwnerContext(String ownerContext) {
		this.ownerContext = ownerContext;
	}

	/**
	 * @return the accountModifyStatus
	 */
	public String getAccountModifyStatus() {
		return accountModifyStatus;
	}

	/**
	 * @param accountModifyStatus the accountModifyStatus to set
	 */
	public void setAccountModifyStatus(String accountModifyStatus) {
		this.accountModifyStatus = accountModifyStatus;
	}

	/**
	 * @return the executiveRestriction
	 */
	public boolean isExecutiveRestriction() {
		return executiveRestriction;
	}

	/**
	 * @param executiveRestriction the executiveRestriction to set
	 */
	public void setExecutiveRestriction(boolean executiveRestriction) {
		this.executiveRestriction = executiveRestriction;
	}

	/**
	 * @return the ownerGlobusId
	 */
	public long getOwnerGlobusId() {
		return ownerGlobusId;
	}

	/**
	 * @param ownerGlobusId the ownerGlobusId to set
	 */
	public void setOwnerGlobusId(long ownerGlobusId) {
		this.ownerGlobusId = ownerGlobusId;
	}

	/**
	 * @return the openBranch
	 */
	public String getOpenBranch() {
		return openBranch;
	}

	/**
	 * @param openBranch the openBranch to set
	 */
	public void setOpenBranch(String openBranch) {
		this.openBranch = openBranch;
	}

	/**
	 * @return the customName
	 */
	public String getCustomName() {
		return customName;
	}

	/**
	 * @param customName the customName to set
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}

	/**
	 * @return the customIcon
	 */
	public String getCustomIcon() {
		return customIcon;
	}

	/**
	 * @param customIcon the customIcon to set
	 */
	public void setCustomIcon(String customIcon) {
		this.customIcon = customIcon;
	}

	public AccountCapitalDTO() {
		// TODO Auto-generated constructor stub
	}

}
