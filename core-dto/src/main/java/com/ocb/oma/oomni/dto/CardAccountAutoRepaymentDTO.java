package com.ocb.oma.oomni.dto;

import java.io.Serializable;

public class CardAccountAutoRepaymentDTO implements Serializable {

	private static final long serialVersionUID = 8920490078102671475L;

	private String cardId;
	private String remitterAccountId;
	private String autoRepaymentType;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getRemitterAccountId() {
		return remitterAccountId;
	}

	public void setRemitterAccountId(String remitterAccountId) {
		this.remitterAccountId = remitterAccountId;
	}

	public String getAutoRepaymentType() {
		return autoRepaymentType;
	}

	public void setAutoRepaymentType(String autoRepaymentType) {
		this.autoRepaymentType = autoRepaymentType;
	}

}
