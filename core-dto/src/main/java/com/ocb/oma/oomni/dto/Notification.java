/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author docv
 *
 */

public class Notification implements Serializable {

	private static final long serialVersionUID = 4153288101534690922L;

	private String titleTemplate;
	private Map<String, String> titleParams;
	private String bodyTemplate;
	private Map<String, String> bodyParams;
	private List<String> deviceTokens;
	private List<String> userIds;
	private Boolean maskAsRead = false;
	private List<NotificationType> notificationTypes = new ArrayList<Notification.NotificationType>();

	public static enum NotificationType {
		SMS, FIREBASE, DB
	}

	public List<String> getDeviceTokens() {
		return deviceTokens;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	public void setUserIds(String... userIds) {
		this.userIds = Arrays.asList(userIds);
	}

	public List<String> getUserIds() {
		return userIds;
	}

	public void setDeviceTokens(List<String> deviceTokens) {
		this.deviceTokens = deviceTokens;
	}

	public void setDeviceTokens(String... deviceTokens) {
		this.deviceTokens = Arrays.asList(deviceTokens);
	}

	public String getTitleTemplate() {
		return titleTemplate;
	}

	public Map<String, String> getTitleParams() {
		return titleParams;
	}

	public void setTitleTemplate(String titleTemplate, Map<String, String> titleParams) {
		this.titleTemplate = titleTemplate;
		this.titleParams = titleParams;
	}

	public String getBodyTemplate() {
		return bodyTemplate;
	}

	public void setBodyTemplate(String bodyTemplate, Map<String, String> bodyParams) {
		this.bodyTemplate = bodyTemplate;
		this.bodyParams = bodyParams;
	}

	public Map<String, String> getBodyParams() {
		return bodyParams;
	}

	public void setMaskAsRead(Boolean maskAsRead) {
		this.maskAsRead = maskAsRead;
	}

	public Boolean getMaskAsRead() {
		return maskAsRead;
	}

	public void setNotificationTypes(NotificationType... notificationTypes) {
		this.notificationTypes = Arrays.asList(notificationTypes);
	}

	public List<NotificationType> getNotificationTypes() {
		return notificationTypes;
	}
}
