package com.ocb.oma.oomni.dto;

public class ConfigStatusEcommerceCardOutput {
	private String status;
	private String errorMsg;
	private Boolean success;
	

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}


	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}


	/**
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}


	/**
	 * @param success the success to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}


	public ConfigStatusEcommerceCardOutput() {
		// TODO Auto-generated constructor stub
	}

}
