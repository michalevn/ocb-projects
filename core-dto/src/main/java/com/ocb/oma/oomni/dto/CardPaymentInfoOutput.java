package com.ocb.oma.oomni.dto;

import java.util.Date;

public class CardPaymentInfoOutput {

	private String accountNumber;
	private String accountStatus;
	private String cardNumber;
	private String customerId;
	private String customerName;
	private String customerIdNo;
	private Double currentDebt;
	private Double totalBillDebt;
	private Double minBillDebt;
	private Double totalPaymentAfterBilling;
	private Date billCycleDate;
	
	

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus() {
		return accountStatus;
	}

	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the customerIdNo
	 */
	public String getCustomerIdNo() {
		return customerIdNo;
	}

	/**
	 * @param customerIdNo the customerIdNo to set
	 */
	public void setCustomerIdNo(String customerIdNo) {
		this.customerIdNo = customerIdNo;
	}

	/**
	 * @return the currentDebt
	 */
	public Double getCurrentDebt() {
		return currentDebt;
	}

	/**
	 * @param currentDebt the currentDebt to set
	 */
	public void setCurrentDebt(Double currentDebt) {
		this.currentDebt = currentDebt;
	}

	/**
	 * @return the totalBillDebt
	 */
	public Double getTotalBillDebt() {
		return totalBillDebt;
	}

	/**
	 * @param totalBillDebt the totalBillDebt to set
	 */
	public void setTotalBillDebt(Double totalBillDebt) {
		this.totalBillDebt = totalBillDebt;
	}

	/**
	 * @return the minBillDebt
	 */
	public Double getMinBillDebt() {
		return minBillDebt;
	}

	/**
	 * @param minBillDebt the minBillDebt to set
	 */
	public void setMinBillDebt(Double minBillDebt) {
		this.minBillDebt = minBillDebt;
	}

	/**
	 * @return the totalPaymentAfterBilling
	 */
	public Double getTotalPaymentAfterBilling() {
		return totalPaymentAfterBilling;
	}

	/**
	 * @param totalPaymentAfterBilling the totalPaymentAfterBilling to set
	 */
	public void setTotalPaymentAfterBilling(Double totalPaymentAfterBilling) {
		this.totalPaymentAfterBilling = totalPaymentAfterBilling;
	}

	/**
	 * @return the billCycleDate
	 */
	public Date getBillCycleDate() {
		return billCycleDate;
	}

	/**
	 * @param billCycleDate the billCycleDate to set
	 */
	public void setBillCycleDate(Date billCycleDate) {
		this.billCycleDate = billCycleDate;
	}

	public CardPaymentInfoOutput() {
		// TODO Auto-generated constructor stub
	}

}
