/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;

/**
 * @author phuhoang
 *
 */
public class StandingOrderOutputDTO implements Serializable {

	private StandingOrderDTO standingOrder;
	private String bankName;

	/**
	 * @return the standingOrder
	 */
	public StandingOrderDTO getStandingOrder() {
		return standingOrder;
	}

	/**
	 * @param standingOrder the standingOrder to set
	 */
	public void setStandingOrder(StandingOrderDTO standingOrder) {
		this.standingOrder = standingOrder;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public StandingOrderOutputDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StandingOrderOutputDTO(StandingOrderDTO standingOrder, String bankName) {
		super();
		this.standingOrder = standingOrder;
		this.bankName = bankName;
	}

	
	

}
