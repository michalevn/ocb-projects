package com.ocb.oma.oomni.dto;

public class ServiceProviderDTO {
	private String providerCode;
	private ServiceDTO service;

	/**
	 * @return the providerCode
	 */
	public String getProviderCode() {
		return providerCode;
	}

	/**
	 * @param providerCode the providerCode to set
	 */
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	/**
	 * @return the service
	 */
	public ServiceDTO getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(ServiceDTO service) {
		this.service = service;
	}

	public ServiceProviderDTO() {
		// TODO Auto-generated constructor stub
	}

}
