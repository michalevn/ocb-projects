package com.ocb.oma.oomni.dto;

import java.io.Serializable;

public class AuthActivateCardDTO implements Serializable {

	private static final long serialVersionUID = -6324518490873552997L;

	private String cardId;

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public AuthActivateCardDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
