package com.ocb.oma.oomni.dto;

public class BillInfoDTO {
	private ServiceProviderDTO serviceProvider;
	private String customerName;
	private String address;
	private AmountDTO amount;
	private Double promotionValue;
	private String additionalData;
	
	

	/**
	 * @return the serviceProvider
	 */
	public ServiceProviderDTO getServiceProvider() {
		return serviceProvider;
	}



	/**
	 * @param serviceProvider the serviceProvider to set
	 */
	public void setServiceProvider(ServiceProviderDTO serviceProvider) {
		this.serviceProvider = serviceProvider;
	}



	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}



	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}



	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}



	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}



	/**
	 * @return the amount
	 */
	public AmountDTO getAmount() {
		return amount;
	}



	/**
	 * @param amount the amount to set
	 */
	public void setAmount(AmountDTO amount) {
		this.amount = amount;
	}



	/**
	 * @return the promotionValue
	 */
	public Double getPromotionValue() {
		return promotionValue;
	}



	/**
	 * @param promotionValue the promotionValue to set
	 */
	public void setPromotionValue(Double promotionValue) {
		this.promotionValue = promotionValue;
	}



	/**
	 * @return the additionalData
	 */
	public String getAdditionalData() {
		return additionalData;
	}



	/**
	 * @param additionalData the additionalData to set
	 */
	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}



	public BillInfoDTO() {
		// TODO Auto-generated constructor stub
	}

}
