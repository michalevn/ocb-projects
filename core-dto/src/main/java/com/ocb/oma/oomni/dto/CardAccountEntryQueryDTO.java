package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.ocb.oma.validation.AmountRange;
import com.ocb.oma.validation.DateRange;

@DateRange(from = "dateFrom", to = "dateTo", message = "dateFrom.must.be.less.than.or.equal.to.dateTo")
@AmountRange(from = "operationAmountFrom", to = "operationAmountTo", message = "operationAmountFrom.must.be.less.than.or.equal.to.operationAmountTo")
public class CardAccountEntryQueryDTO implements Serializable {

	private static final long serialVersionUID = -9138730241649555754L;

	private String productId;
	private String operationType;
	private Date dateFrom;
	private Date dateTo;
	private Integer pageNumber = 0;
	private Integer pageSize = 20;
	private BigDecimal operationAmountFrom;
	private BigDecimal operationAmountTo;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public BigDecimal getOperationAmountFrom() {
		return operationAmountFrom;
	}

	public void setOperationAmountFrom(BigDecimal operationAmountFrom) {
		this.operationAmountFrom = operationAmountFrom;
	}

	public BigDecimal getOperationAmountTo() {
		return operationAmountTo;
	}

	public void setOperationAmountTo(BigDecimal operationAmountTo) {
		this.operationAmountTo = operationAmountTo;
	}

}
