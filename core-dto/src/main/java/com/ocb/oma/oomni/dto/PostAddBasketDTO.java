/**
 * 
 */
package com.ocb.oma.oomni.dto;

/**
 * @author phuhns
 *
 */
public class PostAddBasketDTO {
	
	private String resultCode;
	private String resultMsg;
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

}
