/**
 * 
 */
package com.ocb.oma.oomni.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.PositiveOrZero;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.input.QRcodeInfoLstItems;

/**
 * @author phuhoang
 *
 */
public class PaymentInfoDTO implements Serializable {

	private static final long serialVersionUID = -5501115356066209628L;

	private String accountId;
	private String accountNo;
	@PositiveOrZero(message = MessageConstant.VALIDATION_ERROR)
	private Long amount;
	private String currency;
	private String creditAccount;

	private String cardNumber;
	private String recipientCardNumber;
	private String creditAccountBankCode;
	private String creditAccountBankBranchCode;

	private String creditAccountProvinceCode;
	private String recipient;
	private String remarks;
	private Date executionDate;
	private String cRefNum;
	private String serviceCode;
	private String serviceProviderCode;
	private String billCode;
	private String[] billCodeItemNo;
	private String studentCode;
	private String universityCode;
	private List<PaymentItemStudentFeeDTO> paymentItemStudentFee;

	private String mobilePhoneNumber;
	private String eWalletPhoneNumber;

	// them 26/12/18
	private String promotionCode;
	private Double realAmount;
	private String additionalData;
	private List<QRcodeInfoLstItems> lstQRCodeItems;

	/**
	 * @return the promotionCode
	 */
	public String getPromotionCode() {
		return promotionCode;
	}

	/**
	 * @param promotionCode the promotionCode to set
	 */
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	/**
	 * @return the realAmount
	 */
	public Double getRealAmount() {
		return realAmount;
	}

	/**
	 * @param realAmount the realAmount to set
	 */
	public void setRealAmount(Double realAmount) {
		this.realAmount = realAmount;
	}

	/**
	 * @return the additionalData
	 */
	public String getAdditionalData() {
		return additionalData;
	}

	/**
	 * @param additionalData the additionalData to set
	 */
	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	/**
	 * @return the lstQRCodeItems
	 */
	public List<QRcodeInfoLstItems> getLstQRCodeItems() {
		return lstQRCodeItems;
	}

	/**
	 * @param lstQRCodeItems the lstQRCodeItems to set
	 */
	public void setLstQRCodeItems(List<QRcodeInfoLstItems> lstQRCodeItems) {
		this.lstQRCodeItems = lstQRCodeItems;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the universityCode
	 */
	public String getUniversityCode() {
		return universityCode;
	}

	/**
	 * @param universityCode the universityCode to set
	 */
	public void setUniversityCode(String universityCode) {
		this.universityCode = universityCode;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the amount
	 */
	public Long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Long amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the creditAccount
	 */
	public String getCreditAccount() {
		return creditAccount;
	}

	/**
	 * @param creditAccount the creditAccount to set
	 */
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	/**
	 * @return the recipientCardNumber
	 */
	public String getRecipientCardNumber() {
		return recipientCardNumber;
	}

	/**
	 * @param recipientCardNumber the recipientCardNumber to set
	 */
	public void setRecipientCardNumber(String recipientCardNumber) {
		this.recipientCardNumber = recipientCardNumber;
	}

	/**
	 * @return the creditAccountBankCode
	 */
	public String getCreditAccountBankCode() {
		return creditAccountBankCode;
	}

	/**
	 * @param creditAccountBankCode the creditAccountBankCode to set
	 */
	public void setCreditAccountBankCode(String creditAccountBankCode) {
		this.creditAccountBankCode = creditAccountBankCode;
	}

	/**
	 * @return the creditAccountBankBranchCode
	 */
	public String getCreditAccountBankBranchCode() {
		return creditAccountBankBranchCode;
	}

	/**
	 * @param creditAccountBankBranchCode the creditAccountBankBranchCode to set
	 */
	public void setCreditAccountBankBranchCode(String creditAccountBankBranchCode) {
		this.creditAccountBankBranchCode = creditAccountBankBranchCode;
	}

	/**
	 * @return the creditAccountProvinceCode
	 */
	public String getCreditAccountProvinceCode() {
		return creditAccountProvinceCode;
	}

	/**
	 * @param creditAccountProvinceCode the creditAccountProvinceCode to set
	 */
	public void setCreditAccountProvinceCode(String creditAccountProvinceCode) {
		this.creditAccountProvinceCode = creditAccountProvinceCode;
	}

	/**
	 * @return the recipient
	 */
	public String getRecipient() {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * @return the cRefNum
	 */
	public String getcRefNum() {
		return cRefNum;
	}

	/**
	 * @param cRefNum the cRefNum to set
	 */
	public void setcRefNum(String cRefNum) {
		this.cRefNum = cRefNum;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the serviceProviderCode
	 */
	public String getServiceProviderCode() {
		return serviceProviderCode;
	}

	/**
	 * @param serviceProviderCode the serviceProviderCode to set
	 */
	public void setServiceProviderCode(String serviceProviderCode) {
		this.serviceProviderCode = serviceProviderCode;
	}

	/**
	 * @return the billCode
	 */
	public String getBillCode() {
		return billCode;
	}

	/**
	 * @param billCode the billCode to set
	 */
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	/**
	 * @return the billCodeItemNo
	 */
	public String[] getBillCodeItemNo() {
		return billCodeItemNo;
	}

	/**
	 * @param billCodeItemNo the billCodeItemNo to set
	 */
	public void setBillCodeItemNo(String[] billCodeItemNo) {
		this.billCodeItemNo = billCodeItemNo;
	}

	/**
	 * @return the studentCode
	 */
	public String getStudentCode() {
		return studentCode;
	}

	/**
	 * @param studentCode the studentCode to set
	 */
	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	/**
	 * @return the paymentItemStudentFee
	 */
	public List<PaymentItemStudentFeeDTO> getPaymentItemStudentFee() {
		return paymentItemStudentFee;
	}

	/**
	 * @param paymentItemStudentFee the paymentItemStudentFee to set
	 */
	public void setPaymentItemStudentFee(List<PaymentItemStudentFeeDTO> paymentItemStudentFee) {
		this.paymentItemStudentFee = paymentItemStudentFee;
	}

	/**
	 * @return the mobilePhoneNumber
	 */
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	/**
	 * @param mobilePhoneNumber the mobilePhoneNumber to set
	 */
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	/**
	 * @return the eWalletPhoneNumber
	 */
	public String geteWalletPhoneNumber() {
		return eWalletPhoneNumber;
	}

	/**
	 * @param eWalletPhoneNumber the eWalletPhoneNumber to set
	 */
	public void seteWalletPhoneNumber(String eWalletPhoneNumber) {
		this.eWalletPhoneNumber = eWalletPhoneNumber;
	}

	/**
	 * 
	 * @param accountId
	 * @param amount
	 * @param currency
	 * @param creditAccount
	 * @param cardNumber
	 * @param recipientCardNumber
	 * @param creditAccountBankCode
	 * @param creditAccountBankBranchCode
	 * @param creditAccountProvinceCode
	 * @param recipient
	 * @param remarks
	 * @param executionDate
	 * @param cRefNum
	 * @param serviceCode
	 * @param serviceProviderCode
	 * @param billCode
	 * @param billCodeItemNo
	 * @param studentCode
	 * @param universityCode
	 * @param paymentItemStudentFee
	 * @param mobilePhoneNumber
	 * @param eWalletPhoneNumber
	 */
	public PaymentInfoDTO(String accountId, Long amount, String currency, String creditAccount, String cardNumber,
			String recipientCardNumber, String creditAccountBankCode, String creditAccountBankBranchCode,
			String creditAccountProvinceCode, String recipient, String remarks, Date executionDate, String cRefNum,
			String serviceCode, String serviceProviderCode, String billCode, String[] billCodeItemNo,
			String studentCode, String universityCode, List<PaymentItemStudentFeeDTO> paymentItemStudentFee,
			String mobilePhoneNumber, String eWalletPhoneNumber) {
		super();
		this.accountId = accountId;
		this.amount = amount;
		this.currency = currency;
		this.creditAccount = creditAccount;
		this.cardNumber = cardNumber;
		this.recipientCardNumber = recipientCardNumber;
		this.creditAccountBankCode = creditAccountBankCode;
		this.creditAccountBankBranchCode = creditAccountBankBranchCode;
		this.creditAccountProvinceCode = creditAccountProvinceCode;
		this.recipient = recipient;
		this.remarks = remarks;
		this.executionDate = executionDate;
		this.cRefNum = cRefNum;
		this.serviceCode = serviceCode;
		this.serviceProviderCode = serviceProviderCode;
		this.billCode = billCode;
		this.billCodeItemNo = billCodeItemNo;
		this.studentCode = studentCode;
		this.universityCode = universityCode;
		this.paymentItemStudentFee = paymentItemStudentFee;
		this.mobilePhoneNumber = mobilePhoneNumber;
		this.eWalletPhoneNumber = eWalletPhoneNumber;
	}

	/**
	 * 
	 */
	public PaymentInfoDTO() {
	}

}
