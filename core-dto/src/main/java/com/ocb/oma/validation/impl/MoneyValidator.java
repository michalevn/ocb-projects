package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.Money;

public class MoneyValidator implements ConstraintValidator<Money, Number> {

	public boolean isValid(Number value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		float floatValue = value.floatValue();
		return floatValue == Math.round(floatValue);
	}
}
