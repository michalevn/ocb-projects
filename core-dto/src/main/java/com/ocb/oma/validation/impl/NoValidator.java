package com.ocb.oma.validation.impl;

import java.lang.annotation.Annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NoValidator implements ConstraintValidator<Annotation, Object> {

	private static final Log LOG = LogFactory.getLog(NoValidator.class);

	public void initialize(Annotation annotation) {
		LOG.info(annotation.getClass().getName());
	}

	public boolean isValid(Object value, ConstraintValidatorContext context) {
		return true;
	}
}
