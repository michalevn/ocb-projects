package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.PeriodUnitValidator;

/**
 * don vi tan suat. Enum:
 *
 * <ul>
 * <li>D: ngay</li>
 * <li>W: tuan</li>
 * <li>X: > 360 ngay</li>
 * <li>Y: nam</li>
 * <li>M: thang</li>
 * </ul>
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = { PeriodUnitValidator.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface PeriodUnit {

	String message() default "periodUnit.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
