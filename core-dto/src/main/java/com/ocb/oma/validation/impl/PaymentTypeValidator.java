package com.ocb.oma.validation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.PaymentType;

public class PaymentTypeValidator implements ConstraintValidator<PaymentType, String> {

	public static final List<String> PAYMENT_TYPES = new ArrayList<String>();
	static {
		// chuyển tiền trong hệ thống
		PAYMENT_TYPES.add("InternalPayment");
		// chuyển tiền ngoài hệ thống
		PAYMENT_TYPES.add("LocalPayment");
		// chuyển tiền 24/7
		PAYMENT_TYPES.add("FastTransfer");
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return PAYMENT_TYPES.contains(value);
	}

}
