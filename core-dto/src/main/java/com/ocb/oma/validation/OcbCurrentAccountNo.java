package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.OcbCurrentAccountNoValidator;

/**
 * Tai khoan thanh toan OCB: Chi chua cac ky tu so, chieu dai 16 ky tu.
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = OcbCurrentAccountNoValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OcbCurrentAccountNo {

	String message() default "ocbCurrentAccountNo.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
