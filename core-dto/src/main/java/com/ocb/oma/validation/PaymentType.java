package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.PaymentTypeValidator;

/**
 * Loai thanh toan: 
 * 
 * <ul>
 * <li>InternalPayment: chuyển tiền trong hệ thống</li>
 * <li>LocalPayment: chuyển tiền ngoài hệ thống</li>
 * <li>FastTransfer: chuyển tiền 24/7</li>
 * </ul>
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = PaymentTypeValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface PaymentType {

	String message() default "paymentType.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
