package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.NoValidator;

@Documented
@Constraint(validatedBy = { NoValidator.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface RecurringPeriod {

	String message() default "recurringPeriod.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
