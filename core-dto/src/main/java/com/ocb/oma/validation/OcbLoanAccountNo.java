package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.OcbLoanAccountNoValidator;

/**
 * Tai khoan loan
 * 
 * <ul>
 * 	<li>12 ky tu, bat dau la LD, cac ky tu con lai la ky tu so</li>
 * 	<li>14 ky tu, bat dau la PDLD, cac ky tu con lai la ky tu so</li>
 * </ul>
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = OcbLoanAccountNoValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OcbLoanAccountNo {

	String message() default "ocbLoanAccountNo.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
