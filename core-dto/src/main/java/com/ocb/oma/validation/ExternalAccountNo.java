package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.ExternalAccountNoValidator;

/**
 * Tai khoan cac ngan hang khac OCB
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = ExternalAccountNoValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExternalAccountNo {

	String message() default "externalAccountNo.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
