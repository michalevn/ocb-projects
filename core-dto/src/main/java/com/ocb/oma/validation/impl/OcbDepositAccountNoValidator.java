package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.OcbDepositAccountNo;

public class OcbDepositAccountNoValidator implements ConstraintValidator<OcbDepositAccountNo, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.isEmpty()) {
			return false;
		}
		if ((value.length() == 16) && value.matches("[0-9]+")) {
			return true;
		}
		if ((value.length() == 12) && value.matches("(LD)[0-9]+")) {
			return true;
		}
		return false;
	}
}
