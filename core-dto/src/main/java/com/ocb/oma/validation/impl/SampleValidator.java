package com.ocb.oma.validation.impl;

import com.ocb.oma.dto.SampleUserDTO;

public interface SampleValidator extends Validator<SampleUserDTO> {

}
