package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.MoneyValidator;

/**
 * so tien, phai la so duong khong dau
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = MoneyValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Money {

	String message() default "money.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
