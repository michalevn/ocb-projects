package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.BranchCode;

public class BranchCodeValidator implements ConstraintValidator<BranchCode, String> {
	
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return (value.length() == 9) && value.startsWith("VN001");
	}

}
