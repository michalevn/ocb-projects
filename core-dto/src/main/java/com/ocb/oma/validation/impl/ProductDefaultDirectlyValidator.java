package com.ocb.oma.validation.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.validation.ProductDefaultDirectlyValid;

public class ProductDefaultDirectlyValidator
		implements ConstraintValidator<ProductDefaultDirectlyValid, Map<String, String>> {

	private static final List<String> PRODUCT_LIST_IDS = new ArrayList<String>();
	static {
		PRODUCT_LIST_IDS.add("INTERNAL_TRANSFER_FROM_LIST");
		PRODUCT_LIST_IDS.add("FAST_INTERBANK_TRANSFER_FROM_LIST");
		PRODUCT_LIST_IDS.add("MOBILE_TOPUP_FROM_LIST");
		PRODUCT_LIST_IDS.add("BILL_PAYMENT_FROM_LIST");
		PRODUCT_LIST_IDS.add("TRANSFER_OTHER_FROM_LIST");
	}

	public boolean isValid(Map<String, String> value, ConstraintValidatorContext context) {
		if (value == null || value.isEmpty()) {
			return true;
		}
		for (Entry<String, String> entry : value.entrySet()) {
			if (!PRODUCT_LIST_IDS.contains(entry.getKey())) {
				throw new OmaException(MessageConstant.VALIDATION_ERROR,
						"The key " + entry.getKey() + " is not allowed.");
			}
			if (!entry.getValue().matches("[0-9]+")) {
				throw new OmaException(MessageConstant.VALIDATION_ERROR,
						"The value of " + entry.getKey() + " must only contain numeric characters.");
			}
		}
		return true;
	}
}
