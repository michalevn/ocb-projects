package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.RecurringPeriod;

public class RecurringPeriodValidator implements ConstraintValidator<RecurringPeriod, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return value.equals("NOLIMIT") || value.equals("LIMITED");
	}
}
