package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.AccountNunberValidator;

@Documented
@Constraint(validatedBy = { AccountNunberValidator.class })
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountNumber {

	String message() default "accountNumber.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
