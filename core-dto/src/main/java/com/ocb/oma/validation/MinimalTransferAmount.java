package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.MinimalTransferAmountValidator;

/**
 * So tien toi thieu duoc phep chuyen khoan
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = MinimalTransferAmountValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MinimalTransferAmount {

	String message() default "minimalTransferAmount.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
