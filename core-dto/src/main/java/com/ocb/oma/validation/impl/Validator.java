package com.ocb.oma.validation.impl;

public interface Validator<T> {

	/**
	 * @param value
	 * @return ma loi hay null neu khong loi
	 */
	String validate(T value);

}
