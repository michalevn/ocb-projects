package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.PhoneNumber;

public class PhoneValidator implements ConstraintValidator<PhoneNumber, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return value.matches("[0-9]+") && (value.length() > 8) && (value.length() < 14);
	}

}
