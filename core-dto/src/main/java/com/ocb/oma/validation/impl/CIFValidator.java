package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.CIF;

public class CIFValidator implements ConstraintValidator<CIF, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return (value.length() > 0) && value.matches("[0-9]+");
	}

}
