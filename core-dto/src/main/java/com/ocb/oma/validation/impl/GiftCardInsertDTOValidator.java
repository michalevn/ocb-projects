package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.dto.GiftCardInsertDTO;
import com.ocb.oma.validation.GiftCardInsertDTOValid;

public class GiftCardInsertDTOValidator implements ConstraintValidator<GiftCardInsertDTOValid, GiftCardInsertDTO> {

	public boolean isValid(GiftCardInsertDTO value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return true;
	}
}
