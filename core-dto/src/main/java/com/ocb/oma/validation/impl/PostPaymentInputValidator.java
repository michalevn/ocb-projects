package com.ocb.oma.validation.impl;

import static com.ocb.oma.constants.PostPaymentErrors.PAYMENTINFO_ACCOUNTID_ONLY_ACCEPT_NUMBERIC_CHARACTERS;
import static com.ocb.oma.constants.PostPaymentErrors.PAYMENTINFO_ACCOUNTID_REQUIRED;
import static com.ocb.oma.constants.PostPaymentErrors.PAYMENTINFO_REQUIRED;
import static com.ocb.oma.constants.PostPaymentErrors.PAYMENT_TYPE_INVALID;
import static com.ocb.oma.constants.PostPaymentErrors.REQUEST_BODY_MISSING;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.dto.input.PostPaymentInputDTO;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.oomni.dto.PaymentInfoDTO;
import com.ocb.oma.validation.PostPaymentInputValid;

public class PostPaymentInputValidator implements ConstraintValidator<PostPaymentInputValid, PostPaymentInputDTO> {

	private static final String PAYMENT_TYPE_INTERNAL_PAYMENT = "InternalPayment";
	private static final String PAYMENT_TYPE_LOCAL_PAYMENT = "LocalPayment";
	private static final String PAYMENT_TYPE_FAST_TRANSFER = "FastTransfer";
	private static final String PAYMENT_TYPE_TUITION_FEE = "TuitionFee";
	private static final String PAYMENT_TYPE_BILL_PAYMENT = "BillPayment";
	private static final String PAYMENT_TYPE_EWALLET = "eWallet";
	private static final String PAYMENT_TYPE_TOPUP = "TOPUPPayment";
	private static final String PAYMENT_TYPE_BILLCARD = "BillCardPayment";

	public static final List<String> PAYMENT_TYPES = new ArrayList<String>();
	static {
		PAYMENT_TYPES.add(PAYMENT_TYPE_INTERNAL_PAYMENT);// chuyển tiền trong hệ thống
		PAYMENT_TYPES.add(PAYMENT_TYPE_LOCAL_PAYMENT); // chuyển tiền ngoài hệ thống
		PAYMENT_TYPES.add(PAYMENT_TYPE_FAST_TRANSFER); // chuyển tiền 24/7
		PAYMENT_TYPES.add(PAYMENT_TYPE_TUITION_FEE); // thanh toán học phí
		PAYMENT_TYPES.add(PAYMENT_TYPE_BILL_PAYMENT); // thanh toán hóa đơn
		PAYMENT_TYPES.add(PAYMENT_TYPE_EWALLET); // ví điện tử
		PAYMENT_TYPES.add(PAYMENT_TYPE_TOPUP); // nạp tiền điện thoại
		PAYMENT_TYPES.add(PAYMENT_TYPE_BILLCARD); // thanh toán hóa đơn sử dụng credit card
	}

	public boolean isValid(PostPaymentInputDTO paymentInput, ConstraintValidatorContext context) {
		if (paymentInput == null) {
			throw new OmaException(REQUEST_BODY_MISSING.name(), REQUEST_BODY_MISSING.message());
		}
		String paymentType = paymentInput.getPaymentType();
		if (!PAYMENT_TYPES.contains(paymentType)) {
			throw new OmaException(PAYMENT_TYPE_INVALID.name(), PAYMENT_TYPE_INVALID.message());
		}
		PaymentInfoDTO paymentInfo = paymentInput.getPaymentInfo();
		if (paymentInfo == null) {
			throw new OmaException(PAYMENTINFO_REQUIRED.name(), PAYMENTINFO_REQUIRED.message());
		}
		String accountId = paymentInfo.getAccountId();
		if (accountId == null || (accountId = accountId.trim()).isEmpty()) {
			throw new OmaException(PAYMENTINFO_ACCOUNTID_REQUIRED.name(), PAYMENTINFO_ACCOUNTID_REQUIRED.message());
		}
		if (!accountId.matches("[0-9]+")) {
			throw new OmaException(PAYMENTINFO_ACCOUNTID_ONLY_ACCEPT_NUMBERIC_CHARACTERS.name(),
					PAYMENTINFO_ACCOUNTID_ONLY_ACCEPT_NUMBERIC_CHARACTERS.message());
		}

		if (paymentType.equals(PAYMENT_TYPE_BILL_PAYMENT)) {
			validateBillPayment(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_BILLCARD)) {
			validateBillcard(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_EWALLET)) {
			validateEwallet(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_FAST_TRANSFER)) {
			validateFastTransfer(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_INTERNAL_PAYMENT)) {
			validateInternalPayment(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_LOCAL_PAYMENT)) {
			validateLocalPayment(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_TOPUP)) {
			validateTopup(paymentInput);
		}

		if (paymentType.equals(PAYMENT_TYPE_TUITION_FEE)) {
			validateTuitionFee(paymentInput);
		}

		return true;
	}

	private void validateTuitionFee(PostPaymentInputDTO paymentInput) {

	}

	private void validateTopup(PostPaymentInputDTO paymentInput) {

	}

	private void validateFastTransfer(PostPaymentInputDTO paymentInput) {

	}

	private void validateEwallet(PostPaymentInputDTO paymentInput) {

	}

	private void validateBillcard(PostPaymentInputDTO paymentInput) {

	}

	private void validateBillPayment(PostPaymentInputDTO paymentInput) {

	}

	private void validateLocalPayment(PostPaymentInputDTO paymentInput) {

	}

	private void validateInternalPayment(PostPaymentInputDTO paymentInput) {
		
		PaymentInfoDTO paymentInfo = paymentInput.getPaymentInfo();
		
		// Tai khoan nguon va tai khoon đich phai la VND
		
		if(!"VND".equalsIgnoreCase(paymentInfo.getCurrency())) {
			throw new OmaException(MessageConstant.VALIDATION_ERROR, "Tai khoan nguon va tai khoon đich phai la VND");
		}
		
		
	}

}
