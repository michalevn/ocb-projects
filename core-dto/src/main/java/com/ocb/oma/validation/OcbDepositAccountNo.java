package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.OcbDepositAccountNoValidator;

/**
 * Tai khoan deposit: 
 * <ul>
 * 	<li>16 ky tu so</li>
 *  <li>12 ky tu voi bat dau la LD, va con lai la ky tu so</li>
 * </ul>
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = OcbDepositAccountNoValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OcbDepositAccountNo {

	String message() default "ocbDepositAccount.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
