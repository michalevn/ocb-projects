package com.ocb.oma.validation;

import javax.validation.Payload;
//phai la IN_ADVANCE/IN_ARREARS/DAILY
public @interface InterestPaymentMethod {
	String message() default "only.digits.allowed";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
