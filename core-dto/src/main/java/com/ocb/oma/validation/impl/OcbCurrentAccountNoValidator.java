package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.OcbCurrentAccountNo;

public class OcbCurrentAccountNoValidator implements ConstraintValidator<OcbCurrentAccountNo, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return value.matches("[0-9]+") && (value.length() == 16);
	}

}
