package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.NoValidator;

/**
 * 
 * 2 properties hay parameters phai duoc matched voi nhau.
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = { NoValidator.class })
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldsMatched {

	String message() default "{src}.must.be.matched.with.{dest}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String src() default "src";

	String dest() default "dest";
}
