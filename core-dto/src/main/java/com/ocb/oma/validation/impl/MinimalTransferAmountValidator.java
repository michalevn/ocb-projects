package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.MinimalTransferAmount;

public class MinimalTransferAmountValidator implements ConstraintValidator<MinimalTransferAmount, Object> {

	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value instanceof Number) {
			Number numberValue = (Number) value;
			return numberValue.floatValue() >= 20000;
		}
		return false;
	}

}
