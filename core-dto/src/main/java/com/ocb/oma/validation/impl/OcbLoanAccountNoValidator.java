package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.OcbDepositAccountNo;

public class OcbLoanAccountNoValidator implements ConstraintValidator<OcbDepositAccountNo, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.isEmpty()) {
			return false;
		}
		if ((value.length() == 12) && value.matches("(LD)[0-9]+")) {
			return true;
		}
		if ((value.length() == 14) && value.matches("(PDLD)[0-9]+")) {
			return true;
		}
		return false;
	}
}
