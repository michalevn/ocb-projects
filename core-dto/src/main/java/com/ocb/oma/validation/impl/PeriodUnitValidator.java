package com.ocb.oma.validation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.PaymentType;

public class PeriodUnitValidator implements ConstraintValidator<PaymentType, String> {

	public static final List<String> VALUES = new ArrayList<String>();
	static {
		VALUES.add("D");
		VALUES.add("W");
		VALUES.add("M");
		VALUES.add("Y");
		VALUES.add("X");
	}

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return VALUES.contains(value);
	}

}
