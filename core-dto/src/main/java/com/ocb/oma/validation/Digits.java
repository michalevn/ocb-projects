package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.DigitsValidator;

/**
 * 
 * chi nhan cac ky tu so [0-9]
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = DigitsValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface Digits {

	String message() default "only.digits.allowed";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
