package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.RangeValidator;

@Documented
@Constraint(validatedBy = { RangeValidator.class })
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface PostPaymentInputValid {

	String message() default "{from}.must.be.less.than.or.equal.to.{to}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
