package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.PhoneValidator;

/**
 * So dien thoai: chi chua cac ky tu so, 9-13 ky tu.
 * 
 * @author docv
 *
 */
@Documented
@Constraint(validatedBy = PhoneValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface PhoneNumber {

	String message() default "phoneNumber.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
