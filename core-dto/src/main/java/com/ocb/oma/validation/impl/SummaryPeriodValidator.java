package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.validation.SummaryPeriod;

public class SummaryPeriodValidator implements ConstraintValidator<SummaryPeriod, Integer> {

	private SummaryPeriod summaryPeriod;

	public void initialize(SummaryPeriod summaryPeriod) {
		this.summaryPeriod = summaryPeriod;
	}

	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		int[] values = summaryPeriod.values();
		for (int v : values) {
			if (v == value) {
				return true;
			}
		}
		throw new OmaException(MessageConstant.VALIDATION_ERROR, "summaryPeriod must be in " + values.toString());
	}

}
