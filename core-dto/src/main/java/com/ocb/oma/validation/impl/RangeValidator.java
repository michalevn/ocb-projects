package com.ocb.oma.validation.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ocb.oma.dto.MessageConstant;
import com.ocb.oma.exception.OmaException;
import com.ocb.oma.validation.DateRange;

public class RangeValidator implements ConstraintValidator<DateRange, Object> {

	private static final Log LOG = LogFactory.getLog(RangeValidator.class);

	private DateRange dateRange;

	public void initialize(DateRange dateRange) {
		this.dateRange = dateRange;
	}

	public boolean isValid(Object bean, ConstraintValidatorContext context) {

		String from = dateRange.from();
		String to = dateRange.to();

		try {

			Object fromValue = PropertyUtils.getProperty(bean, from);
			Object toValue = PropertyUtils.getProperty(bean, to);

			if (fromValue == null || toValue == null) {
				return true;
			}

			Number fromNumber = null;
			if (fromValue instanceof Number) {
				fromNumber = (Number) fromValue;
			} else if (fromValue instanceof Date) {
				fromNumber = ((Date) fromValue).getTime();
			}

			Number toNumber = null;
			if (toValue instanceof Number) {
				toNumber = (Number) toValue;
			} else if (toValue instanceof Date) {
				toNumber = ((Date) toValue).getTime();
			}

			if (fromNumber == null) {
				throw new OmaException(MessageConstant.VALIDATION_ERROR,
						fromValue + " cannot be cast to java.lang.Number");
			}

			if (toNumber == null) {
				throw new OmaException(MessageConstant.VALIDATION_ERROR,
						toValue + " cannot be cast to java.lang.Number");
			}

			if (fromNumber.floatValue() <= toNumber.floatValue()) {
				return true;
			} else {
				String message = dateRange.message();
				message = message.replace("{from}", from).replace("{to}", to);
				throw new OmaException(MessageConstant.VALIDATION_ERROR, message);
			}
		} catch (IllegalAccessException e) {
			LOG.error(e, e);
			throw new OmaException(MessageConstant.VALIDATION_ERROR, e.getMessage());
		} catch (InvocationTargetException e) {
			LOG.error(e, e);
			throw new OmaException(MessageConstant.VALIDATION_ERROR, e.getMessage());
		} catch (NoSuchMethodException e) {
			LOG.error(e, e);
			throw new OmaException(MessageConstant.VALIDATION_ERROR, e.getMessage());
		}
	}

}
