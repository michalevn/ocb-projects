package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.PaymentTypeValidator;

//chi cho nhap : InternalPayment/INTERNAL/LocalPayment/EXTERNAL/FastTransfer/FAST

@Documented
@Constraint(validatedBy = PaymentTypeValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface RecipientTypeVal {
	String message() default "paymentType.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
