package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.AccountIdValidator;

/**
 * 
 * ID cua so tai khoan
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = AccountIdValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountId {

	String message() default "accountId.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
