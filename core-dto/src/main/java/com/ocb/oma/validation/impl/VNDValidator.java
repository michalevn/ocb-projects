package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.OcbCurrentAccountNo;

public class VNDValidator implements ConstraintValidator<OcbCurrentAccountNo, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return value.equals("VND");
	}

}
