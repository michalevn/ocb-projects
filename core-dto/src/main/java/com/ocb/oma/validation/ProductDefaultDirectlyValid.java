package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.NoValidator;
import com.ocb.oma.validation.impl.ProductDefaultDirectlyValidator;

/**
 * 
 * ID cua so tai khoan
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = { ProductDefaultDirectlyValidator.class })
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ProductDefaultDirectlyValid {

	String message() default "productDefaultDirectly.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
