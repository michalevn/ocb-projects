package com.ocb.oma.validation;

import javax.validation.Payload;
// se la FIXED/FLOATING

public @interface InterestRateType {
	String message() default "only.digits.allowed";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
