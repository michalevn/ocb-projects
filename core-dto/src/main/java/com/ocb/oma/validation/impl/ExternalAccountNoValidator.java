package com.ocb.oma.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ocb.oma.validation.OcbCurrentAccountNo;

public class ExternalAccountNoValidator implements ConstraintValidator<OcbCurrentAccountNo, String> {

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return value.matches("([0-9A-Z]){,30}");
	}
}
