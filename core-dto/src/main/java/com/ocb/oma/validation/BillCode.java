package com.ocb.oma.validation;

import javax.validation.Payload;

public @interface BillCode {
	String message() default "accountId.invalid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
