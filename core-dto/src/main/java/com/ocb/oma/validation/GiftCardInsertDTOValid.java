package com.ocb.oma.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ocb.oma.validation.impl.GiftCardInsertDTOValidator;

/**
 * 
 * OmniGiftCard.debitAccount PHAI khac voi moi OmniGiftCardReceiver.recipient
 * trong List<OmniGiftCardReceiver>
 * 
 * @author docv
 *
 */

@Documented
@Constraint(validatedBy = { GiftCardInsertDTOValidator.class })
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface GiftCardInsertDTOValid {

	String message() default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
