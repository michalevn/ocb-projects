package com.ocb.oma.exception;

public class OmaException extends RuntimeException {

	private String desc;

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8875046077719654118L;

	public OmaException(String s) {
		super(s);
		this.desc = s;
	}

	public OmaException(String s, String desc) {
		super(s);
		this.desc = desc;
	}

	public OmaException(String message, Throwable cause) {
		super(message, cause);
	}

}
