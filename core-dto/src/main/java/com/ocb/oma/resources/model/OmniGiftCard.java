/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ocb.oma.dto.ValidationMessageConstant;
import com.ocb.oma.entity.AbstractEntity;
import com.ocb.oma.validation.VND;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "PMDT_GIFT_CARD")
public class OmniGiftCard extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "PMDT_GIFT_CARD_ID", unique = true)
	@Id
	@SequenceGenerator(name = "PMDT_GIFT_CARD_ID", sequenceName = "DEQ_PMDT_GIFT_CARD_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMDT_GIFT_CARD_ID")
	private Long giftCardId;

	@Column(name = "full_message")
	private String fullMessage;

	@Column(name = "short_message")
	private String shortMessage;

	// vd: thiep sinh nhat, ...
	@Column(name = "cart_type")
	private String cardType;

	@Column(name = "card_image")
	private String cardImage;

	@Column(name = "debitAccountId")
	private String debitAccountId;

	@Column(name = "debitAccount")
	private String debitAccount;

	@Column(name = "total_amount")
	private String totalAmount;

	@Column(name = "user_id")
	private String userId;

	@VND(message = ValidationMessageConstant.CURRENCY_MUST_BE_VND)
	@Column(name = "currency")
	private String currency;

	@Transient
	private String short_message;

	@Transient
	private String locale;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @param cardImage the cardImage to set
	 */
	public void setCardImage(String cardImage) {
		this.cardImage = cardImage;
	}

	/**
	 * @return the cardImage
	 */
	public String getCardImage() {
		return cardImage;
	}

	/**
	 * @return the giftCardId
	 */
	public Long getGiftCardId() {
		return giftCardId;
	}

	/**
	 * @param giftCardId the giftCardId to set
	 */
	public void setGiftCardId(Long giftCardId) {
		this.giftCardId = giftCardId;
	}

	/**
	 * @return the fullMessage
	 */
	public String getFullMessage() {
		return fullMessage;
	}

	/**
	 * @param fullMessage the fullMessage to set
	 */
	public void setFullMessage(String fullMessage) {
		this.fullMessage = fullMessage;
	}

	/**
	 * @return the shortMessage
	 */
	public String getShortMessage() {
		return shortMessage;
	}

	/**
	 * @param shortMessage the shortMessage to set
	 */
	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the debitAccount
	 */
	public String getDebitAccount() {
		return debitAccount;
	}

	/**
	 * @param debitAccount the debitAccount to set
	 */
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	/**
	 * @return the totalAmount
	 */
	public String getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDebitAccountId() {
		return debitAccountId;
	}

	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	public void setShort_message(String short_message) {
		this.short_message = short_message;
		this.shortMessage = short_message;
	}

	public String getShort_message() {
		return short_message;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getLocale() {
		return locale;
	}
}
