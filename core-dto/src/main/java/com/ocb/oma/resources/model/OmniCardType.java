/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_CARD_TYPE")
public class OmniCardType extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -5725631260086460215L;

	@Column(name = "OMDT_CARD_TYPE_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_CARD_TYPE_ID", sequenceName = "SEQ_OMDT_CARD_TYPE_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_CARD_TYPE_ID")
	private Long id;

	/**
	 * <ul>
	 * <li>C: CREDIT = The tin dung</li>
	 * <li>D: DEBIT = The ghi no</li>
	 * </ul>
	 */
	@Column(name = "CARD_TYPE_GROUP")
	private String cardTypeGroup;

	@Column(name = "CARD_TYPE_CODE")
	private String cardTypeCode;

	@Column(name = "CARD_TYPE_NAME")
	private String cardTypeName;

	@Column(name = "CARD_TYPE_IMAGE")
	private String cardTypeImage;

	//@Lob
	@Column(name = "SUB_CARDS")
	private String subCards;

	@Column(name = "ACTIVE")
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardTypeCode() {
		return cardTypeCode;
	}

	public void setCardTypeCode(String cardTypeCode) {
		this.cardTypeCode = cardTypeCode;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getSubCards() {
		return subCards;
	}

	public void setSubCards(String subCards) {
		this.subCards = subCards;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}

	public void setCardTypeImage(String cardTypeImage) {
		this.cardTypeImage = cardTypeImage;
	}

	public String getCardTypeImage() {
		return cardTypeImage;
	}

	public void setCardTypeGroup(String cardTypeGroup) {
		this.cardTypeGroup = cardTypeGroup;
	}

	public String getCardTypeGroup() {
		return cardTypeGroup;
	}
}
