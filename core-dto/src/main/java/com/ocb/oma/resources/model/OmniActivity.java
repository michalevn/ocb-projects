/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_ACTIVITY")
public class OmniActivity extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -6774717308565457913L;
	
	public static final Integer TARGET_TYPE_NOTIFICATION = 1;
	
	public static final Integer ACTION_VIEW = 1;

	@Column(name = "OMDT_ACTIVITY_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_ACTIVITY_ID", sequenceName = "SEQ_OMDT_ACTIVITY_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_ACTIVITY_ID")
	private Long id;

	@Column(name = "TARGET_TYPE")
	private Integer targetType;

	@Column(name = "TARGET")
	private Long target;

	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "ACTION")
	private Integer action;

	@Column(name = "VALUE")
	private Double value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTargetType() {
		return targetType;
	}

	public void setTargetType(Integer targetType) {
		this.targetType = targetType;
	}

	public Long getTarget() {
		return target;
	}

	public void setTarget(Long target) {
		this.target = target;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAction() {
		return action;
	}

	public void setAction(Integer action) {
		this.action = action;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
