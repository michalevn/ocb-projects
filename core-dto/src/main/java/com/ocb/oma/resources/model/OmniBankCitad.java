/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_BANK_CITAD")
public class OmniBankCitad extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 1860641324464054229L;

	@Column(name = "OMDT_BANK_CITAD_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_BANK_CITAD_ID", sequenceName = "SEQ_OMDT_BANK_CITAD_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_BANK_CITAD_ID")
	private Long bankCitadId;

	// BANK_ID BANK_BRANCH_CODE BANK_BRANCH_NAME PROVINCE_ID CITAD_CODE_MAPPING
	// BANKID

	@Column(name = "BANK_CODE")
	private String bankCode;

	@Column(name = "BRANCH_CODE")
	private String bankBranchCode;

	@Column(name = "branch_name")
	private String bankBranchName;

	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "citad_code")
	private String citadCode;

	@Column(name = "BANK_CITAD_ID")
	private Long bankCidtadId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BANK_CODE", nullable = false, updatable = false, insertable = false)
	private OmniBankInfo bank;

	/**
	 * @return the bankCitadId
	 */
	public Long getBankCitadId() {
		return bankCitadId;
	}

	/**
	 * @param bankCitadId the bankCitadId to set
	 */
	public void setBankCitadId(Long bankCitadId) {
		this.bankCitadId = bankCitadId;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * @return the bankBranchCode
	 */
	public String getBankBranchCode() {
		return bankBranchCode;
	}

	/**
	 * @param bankBranchCode the bankBranchCode to set
	 */
	public void setBankBranchCode(String bankBranchCode) {
		this.bankBranchCode = bankBranchCode;
	}

	/**
	 * @return the bankBranchName
	 */
	public String getBankBranchName() {
		return bankBranchName;
	}

	/**
	 * @param bankBranchName the bankBranchName to set
	 */
	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the citadCode
	 */
	public String getCitadCode() {
		return citadCode;
	}

	/**
	 * @param citadCode the citadCode to set
	 */
	public void setCitadCode(String citadCode) {
		this.citadCode = citadCode;
	}

	/**
	 * @return the bank
	 */
	public OmniBankInfo getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(OmniBankInfo bank) {
		this.bank = bank;
	}

}
