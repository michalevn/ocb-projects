/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "PMDT_GIFT_CARDMSG_TPL")
public class OmniGiftCardMsgTemplate extends AbstractEntity implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "PMDT_GIFT_CARDMSG_TPL", unique = true)
	@Id
	@SequenceGenerator(name = "PMDT_GIFT_CARDMSG_TPL", sequenceName = "SEQ_PMDT_GIFT_CARDMSG_TPL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMDT_GIFT_CARDMSG_TPL")
	private Long cardMsgTemplateId;

	@Column(name = "full_message_vn")
	private String fullMessageVn;

	@Column(name = "full_message_el")
	private String fullMessageEl;

	@Column(name = "short_message_vn")
	private String shortMessageVn;

	@Column(name = "short_message_el")
	private String shortMessageEl;

	@Column(name = "card_type")
	private String cardType;

	@Column(name = "order_number")
	private Integer orderNumber;

	/**
	 * @return the cardMsgTemplateId
	 */
	public Long getCardMsgTemplateId() {
		return cardMsgTemplateId;
	}

	/**
	 * @param cardMsgTemplateId the cardMsgTemplateId to set
	 */
	public void setCardMsgTemplateId(Long cardMsgTemplateId) {
		this.cardMsgTemplateId = cardMsgTemplateId;
	}

	/**
	 * @return the fullMessageVn
	 */
	public String getFullMessageVn() {
		return fullMessageVn;
	}

	/**
	 * @param fullMessageVn the fullMessageVn to set
	 */
	public void setFullMessageVn(String fullMessageVn) {
		this.fullMessageVn = fullMessageVn;
	}

	/**
	 * @return the fullMessageEl
	 */
	public String getFullMessageEl() {
		return fullMessageEl;
	}

	/**
	 * @param fullMessageEl the fullMessageEl to set
	 */
	public void setFullMessageEl(String fullMessageEl) {
		this.fullMessageEl = fullMessageEl;
	}

	/**
	 * @return the shortMessageVn
	 */
	public String getShortMessageVn() {
		return shortMessageVn;
	}

	/**
	 * @param shortMessageVn the shortMessageVn to set
	 */
	public void setShortMessageVn(String shortMessageVn) {
		this.shortMessageVn = shortMessageVn;
	}

	/**
	 * @return the shortMessageEl
	 */
	public String getShortMessageEl() {
		return shortMessageEl;
	}

	/**
	 * @param shortMessageEl the shortMessageEl to set
	 */
	public void setShortMessageEl(String shortMessageEl) {
		this.shortMessageEl = shortMessageEl;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the orderNumber
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	 
}
