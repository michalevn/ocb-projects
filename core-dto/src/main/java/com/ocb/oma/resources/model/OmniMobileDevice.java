/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_MOBILE_DEVICE")
public class OmniMobileDevice extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@Column(name = "ID", unique = true)
	@Id
	@SequenceGenerator(name = "MOBILE_DEVICE_ID", sequenceName = "SEQ_MOBILE_DEVICE_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOBILE_DEVICE_ID")
	private Long id;

	@Column(name = "DEVICE_ID")
	private String deviceId;

	@Column(name = "DEVICE_TOKEN")
	private String deviceToken;

	@Column(name = "DEVICE_NAME")
	private String deviceName;

	@Column(name = "DEVICE_MODAL")
	private String deviceModal;

	@Column(name = "DEVICE_BRAND")
	private String deviceBrand;

	@Column(name = "OS_TYPE")
	private String osType;

	@Column(name = "ACTIVE")
	private Boolean active;

	@Column(name = "VERSION")
	private String version;

	/**
	 * @return the osType
	 */
	public String getOsType() {
		return osType;
	}

	/**
	 * @param osType the osType to set
	 */
	public void setOsType(String osType) {
		this.osType = osType;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceModal() {
		return deviceModal;
	}

	public void setDeviceModal(String deviceModal) {
		this.deviceModal = deviceModal;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return version;
	}
}
