/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_NOTIFICATION")
public class OmniNotification extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -6774717308565457913L;

	public final static int DISTRIBUTED_TYPE_APP = 0x1; // 00000001
	public final static int DISTRIBUTED_TYPE_FIREBASE = 0x2; // 00000010
	public final static int DISTRIBUTED_TYPE_SMS = 0x4; // 00000100

	public final static Integer TYPE_THONG_BAO = 1;
	public final static Integer TYPE_KHUYEN_MAI = 2;

	@Column(name = "OMDT_NOTIFICATION_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_NOTIFICATION_ID", sequenceName = "SEQ_OMDT_NOTIFICATION_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_NOTIFICATION_ID")
	private Long id;

	@Column(name = "DISTRIBUTED_TYPE")
	private Integer distributedType = 3;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "TITLE_VN")
	private String titleVn;

	@Column(name = "TITLE_EL")
	private String titleEl;

	@Column(name = "DESCRIPTION_VN")
	private String descriptionVn;

	@Column(name = "DESCRIPTION_EL")
	private String descriptionEl;

	@Column(name = "BODY_VN")
	private String bodyVn;

	@Column(name = "BODY_EL")
	private String bodyEl;

	@Column(name = "IMAGE")
	private String image;

	@Transient
	private Boolean read;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitleVn() {
		return titleVn;
	}

	public void setTitleVn(String titleVn) {
		this.titleVn = titleVn;
	}

	public String getTitleEl() {
		return titleEl;
	}

	public void setTitleEl(String titleEl) {
		this.titleEl = titleEl;
	}

	public String getDescriptionVn() {
		return descriptionVn;
	}

	public void setDescriptionVn(String descriptionVn) {
		this.descriptionVn = descriptionVn;
	}

	public String getDescriptionEl() {
		return descriptionEl;
	}

	public void setDescriptionEl(String descriptionEl) {
		this.descriptionEl = descriptionEl;
	}

	public String getBodyVn() {
		return bodyVn;
	}

	public void setBodyVn(String bodyVn) {
		this.bodyVn = bodyVn;
	}

	public String getBodyEl() {
		return bodyEl;
	}

	public void setBodyEl(String bodyEl) {
		this.bodyEl = bodyEl;
	}

	/**
	 * 
	 * @return kenh thong bao, la so nguyen tu 1 den 7.
	 *         <ul>
	 *         <li>1: APP</li>
	 *         <li>2: FIREBASE</li>
	 *         <li>4: SMS</li>
	 *         <li>3: APP + FIREBASE</li>
	 *         <li>5: APP + SMS</li>
	 *         <li>6: FIREBASE + SMS</li>
	 *         <li>7: APP + FIREBASE + SMS</li>
	 *         </ul>
	 */
	public Integer getDistributedType() {
		return distributedType;
	}

	/**
	 * 
	 * @param distributedType kenh thong bao, la so nguyen tu 1 den 7.
	 *                        <ul>
	 *                        <li>1: APP</li>
	 *                        <li>2: FIREBASE</li>
	 *                        <li>4: SMS</li>
	 *                        <li>3: APP + FIREBASE</li>
	 *                        <li>5: APP + SMS</li>
	 *                        <li>6: FIREBASE + SMS</li>
	 *                        <li>7: APP + FIREBASE + SMS</li>
	 *                        </ul>
	 * 
	 */
	public void setDistributedType(Integer distributedType) {
		this.distributedType = distributedType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean getRead() {
		return read;
	}
}
