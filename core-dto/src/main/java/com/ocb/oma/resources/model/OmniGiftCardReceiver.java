/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "PMDT_GIFT_CARD_RECEIVER")
public class OmniGiftCardReceiver extends AbstractEntity implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "PMDT_GIFT_CARD_RECEIVER_ID", unique = true)
	@Id
	@SequenceGenerator(name = "PMDT_GIFT_CARD_RECEIVER_ID", sequenceName = "SEQ_PMDT_GIFT_CARD_RECEIVER_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PMDT_GIFT_CARD_RECEIVER_ID")
	private Long giftCardReceiverId;

	@Column(name = "beneficiary")
	private String beneficiary;

	@Transient
	private String cif;

	@Column(name = "amount")
	@Min(value = 20000)
	private Double amount;

	@Column(name = "GIFT_CARD_ID")
	private Long giftCardId;

	@Column(name = "recipient")
	private String recipient;

	@Column(name = "received")
	private Boolean received;

	/**
	 * @param received the received to set
	 */
	public void setReceived(Boolean received) {
		this.received = received;
	}

	/**
	 * @return the received
	 */
	public Boolean getReceived() {
		return received;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the giftCardReceiverId
	 */
	public Long getGiftCardReceiverId() {
		return giftCardReceiverId;
	}

	/**
	 * @param giftCardReceiverId the giftCardReceiverId to set
	 */
	public void setGiftCardReceiverId(Long giftCardReceiverId) {
		this.giftCardReceiverId = giftCardReceiverId;
	}

	/**
	 * @return the beneficiary
	 */
	public String getBeneficiary() {
		return beneficiary;
	}

	/**
	 * @param beneficiary the beneficiary to set
	 */
	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the giftCardId
	 */
	public Long getGiftCardId() {
		return giftCardId;
	}

	/**
	 * @param giftCardId the giftCardId to set
	 */
	public void setGiftCardId(Long giftCardId) {
		this.giftCardId = giftCardId;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getCif() {
		return cif;
	}
}
