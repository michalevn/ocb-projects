/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_BANK_INFO")
public class OmniBankInfo extends AbstractEntity implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692120808220820440L;

	 

	@Column(name = "image_url")
	private String imageUrl;
	@Id
	@Column(name = "bank_code",unique=true)
	private String bankCode;

	@Column(name = "bank_name")
	private String bankName;
	@Column(name = "napas_code")
	private String napasCode;
	@Column(name = "citad_code")
	private String citadCode;
	@Column(name = "order_number")
	private Integer orderNumber;

	@Column(name = "internal_bank")
	private Boolean internalBank;

	@OneToMany(cascade = CascadeType.DETACH,
            fetch = FetchType.LAZY,
            mappedBy = "bank")
    private List<OmniBankCitad> bankCitads ;
	
	/**
	 * @param bankCitads the bankCitads to set
	 */
	public void setBankCitads(List<OmniBankCitad> bankCitads) {
		this.bankCitads = bankCitads;
	}
	/**
	 * @return the bankCitads
	 */
	public List<OmniBankCitad> getBankCitads() {
		return bankCitads;
	}
	/**
	 * @return the internalBank
	 */
	public Boolean getInternalBank() {
		return internalBank;
	}

	/**
	 * @param internalBank the internalBank to set
	 */
	public void setInternalBank(Boolean internalBank) {
		this.internalBank = internalBank;
	}

	/**
	 * @return the orderNumber
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
 
	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the napasCode
	 */
	public String getNapasCode() {
		return napasCode;
	}

	/**
	 * @param napasCode the napasCode to set
	 */
	public void setNapasCode(String napasCode) {
		this.napasCode = napasCode;
	}

	/**
	 * @return the citadCode
	 */
	public String getCitadCode() {
		return citadCode;
	}

	/**
	 * @param citadCode the citadCode to set
	 */
	public void setCitadCode(String citadCode) {
		this.citadCode = citadCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
