package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

@Entity
@Table(name = "OMDT_TUITION_FEE_GROUP")
public class TuitionFeeGroup extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 6695737960151662053L;

	@Column(name = "ID", unique = true)
	@Id
	@SequenceGenerator(name = "TUITION_FEE_GROUP_ID", sequenceName = "SEQ_TUITION_FEE_GROUP_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TUITION_FEE_GROUP_ID")
	private Long id;

	@Column(name = "CODE")
	private String code;

	@Column(name = "NAME")
	private String name;

	//@Lob
	@Column(name = "IMAGE")
	private String image;

	@Column(name = "ACTIVE")
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
