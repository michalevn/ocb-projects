/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_LOV")
public class OmniLOV extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -6855069041284934332L;

	@Column(name = "OMDT_LOV_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_LOV_ID", sequenceName = "SEQ_OMDT_LOV_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_LOV_ID")
	private Long lovId;

	// BANK_ID BANK_BRANCH_CODE BANK_BRANCH_NAME PROVINCE_ID CITAD_CODE_MAPPING
	// BANKID

	@Column(name = "LOV_TYPE")
	private String lovType;
	
	@Column(name = "LOV_CODE")
	private String lovCode;

	@Column(name = "LOV_NAME_VN")
	private String lovNameVn;;
	
	@Column(name = "LOV_NAME_EL")
	private String lovNameEl;

	@Column(name = "order_number")
	private Integer orderNumber;

	/**
	 * @return the lovId
	 */
	public Long getLovId() {
		return lovId;
	}

	/**
	 * @param lovId the lovId to set
	 */
	public void setLovId(Long lovId) {
		this.lovId = lovId;
	}

	/**
	 * @return the lovCode
	 */
	public String getLovCode() {
		return lovCode;
	}

	/**
	 * @param lovCode the lovCode to set
	 */
	public void setLovCode(String lovCode) {
		this.lovCode = lovCode;
	}

	/**
	 * @return the lovNameVn
	 */
	public String getLovNameVn() {
		return lovNameVn;
	}

	/**
	 * @param lovNameVn the lovNameVn to set
	 */
	public void setLovNameVn(String lovNameVn) {
		this.lovNameVn = lovNameVn;
	}

	/**
	 * @return the lovNameEl
	 */
	public String getLovNameEl() {
		return lovNameEl;
	}

	/**
	 * @param lovNameEl the lovNameEl to set
	 */
	public void setLovNameEl(String lovNameEl) {
		this.lovNameEl = lovNameEl;
	}

	/**
	 * @return the orderNumber
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the lovType
	 */
	public String getLovType() {
		return lovType;
	}

	/**
	 * @param lovType the lovType to set
	 */
	public void setLovType(String lovType) {
		this.lovType = lovType;
	}

}
