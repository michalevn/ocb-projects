/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_PROVINCE_INFO")
public class OmniProvinceInfo extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 2598877629319448210L;

	@Column(name = "OMDT_PROVINCE_INFO_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_PROVINCE_INFO_ID", sequenceName = "SEQ_OMDT_PROVINCE_INFO_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_PROVINCE_INFO_ID")
	private Long provinceId;

	// BANK_ID BANK_BRANCH_CODE BANK_BRANCH_NAME PROVINCE_ID CITAD_CODE_MAPPING
	// BANKID
//
	@Column(name = "PROVINCE_CODE")
	private String provinceCode;

	@Column(name = "PROVINCE_NAME")
	String provinceName;
	@Column(name = "order_number")
	private Integer orderNumber;

	/**
	 * @return the provinceId
	 */
	public Long getProvinceId() {
		return provinceId;
	}

	/**
	 * @param provinceId the provinceId to set
	 */
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * @return the provinceCode
	 */
	public String getProvinceCode() {
		return provinceCode;
	}

	/**
	 * @param provinceCode the provinceCode to set
	 */
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	/**
	 * @return the provinceName
	 */
	public String getProvinceName() {
		return provinceName;
	}

	/**
	 * @param provinceName the provinceName to set
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	/**
	 * @return the orderNumber
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

}
