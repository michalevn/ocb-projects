/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_FORM")
public class OmniForm extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 6197320115641158730L;

	@Column(name = "FORM_ID", unique = true)
	@Id
	@SequenceGenerator(name = "FORM_ID", sequenceName = "SEQ_FORM_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FORM_ID")
	private Long formId;

	@Column(name = "UUID")
	private String uuid;

	@Column(name = "TITLE")
	private String title;

	// @Lob
	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "USER_EMAIL")
	private String userEmail;

	@Column(name = "USER_PHONE")
	private String userPhone;

	@Column(name = "USER_NAME")
	private String userName;

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
