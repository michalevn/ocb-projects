/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_LINK")
public class OmniLink extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "OMDT_LINK_ID", unique = true)
	@Id
	@SequenceGenerator(name = "OMDT_LINK_ID", sequenceName = "SEQ_OMDT_LINK_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OMDT_LINK_ID")
	private Long id;

	@Column(name = "TITLE_VN")
	private String titleVn;

	@Column(name = "TITLE_EL")
	private String titleEl;

	@Column(name = "IMAGE_URL")
	private String imageUrl;

	@Column(name = "DESCRIPTION_VN")
	private String descriptionVn;

	@Column(name = "DESCRIPTION_EL")
	private String descriptionEl;

	@Column(name = "HREF")
	private String href;

	@Column(name = "TARGET")
	private String target;

	@Column(name = "DISPLAY_ORDER")
	private Integer displayOrder;

	@Column(name = "LINK_GROUP")
	private String group;

	@Column(name = "ACTIVE")
	private Boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitleVn() {
		return titleVn;
	}

	public void setTitleVn(String titleVn) {
		this.titleVn = titleVn;
	}

	public String getTitleEl() {
		return titleEl;
	}

	public void setTitleEl(String titleEl) {
		this.titleEl = titleEl;
	}

	public String getDescriptionVn() {
		return descriptionVn;
	}

	public void setDescriptionVn(String descriptionVn) {
		this.descriptionVn = descriptionVn;
	}

	public String getDescriptionEl() {
		return descriptionEl;
	}

	public void setDescriptionEl(String descriptionEl) {
		this.descriptionEl = descriptionEl;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
