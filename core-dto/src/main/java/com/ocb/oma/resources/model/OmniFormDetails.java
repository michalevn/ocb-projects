/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author docv
 *
 */

@Entity
@Table(name = "OMDT_FORM_DETAILS")
public class OmniFormDetails extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = 6197320115641158730L;

	@Column(name = "FORM_DETAILS_ID", unique = true)
	@Id
	@SequenceGenerator(name = "FORM_DETAILS_ID", sequenceName = "SEQ_FORM_DETAILS_ID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FORM_DETAILS_ID")
	private Long formDetailsId;

	@Column(name = "FORM_ID")
	private Long formId;

	@Column(name = "FIELD_NAME")
	private String fieldName;

	@Column(name = "FIELD_TYPE")
	private String fieldType;

	// @Lob
	@Column(name = "FIELD_VALUE")
	private String fieldValue;

	public Long getFormDetailsId() {
		return formDetailsId;
	}

	public void setFormDetailsId(Long formDetailsId) {
		this.formDetailsId = formDetailsId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

}
