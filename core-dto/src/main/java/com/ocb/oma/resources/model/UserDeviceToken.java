/**
 * 
 */
package com.ocb.oma.resources.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ocb.oma.entity.AbstractEntity;

/**
 * @author phuhoang
 *
 */

@Entity
@Table(name = "OMDT_USDV_TOKEN")
public class UserDeviceToken extends AbstractEntity implements Serializable, Cloneable {

	private static final long serialVersionUID = -4692120808220820440L;

	@Column(name = "USDV_TOKEN_ID", unique = true)
	@Id
	@SequenceGenerator(name = "UserDeviceTokenID", sequenceName = "SEQ_UserDeviceTokenID", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserDeviceTokenID")
	private Long userDeviceTokenId;

	@Column(name = "CIF")
	private String cif;

	@Column(name = "DEVICE_ID")
	private String deviceId;

	@Column(name = "DEVICE_TOKEN")
	private String deviceToken;

	@Column(name = "ACTIVE")
	private Boolean active;

	@Column(name = "USER_DEVICE_STATUS")
	private Integer status = 0;

	@Column(name = "LAST_LOGIN_DATE")
	private Date lastLoginDate;

	@Transient
	private OmniMobileDevice mobileDevice;

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the userDeviceTokenId
	 */
	public Long getUserDeviceTokenId() {
		return userDeviceTokenId;
	}

	/**
	 * @param userDeviceTokenId the userDeviceTokenId to set
	 */
	public void setUserDeviceTokenId(Long userDeviceTokenId) {
		this.userDeviceTokenId = userDeviceTokenId;
	}

	/**
	 * @return the cif
	 */
	public String getCif() {
		return cif;
	}

	/**
	 * @param cif the cif to set
	 */
	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public void setMobileDevice(OmniMobileDevice mobileDevice) {
		this.mobileDevice = mobileDevice;
	}

	public OmniMobileDevice getMobileDevice() {
		return mobileDevice;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}
}
